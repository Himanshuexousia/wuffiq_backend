(function($) {
    "use strict";

    var windowWidth = $(window).width();

    //var windowHeight = $(window).height();
    var body = document.body,
        html = document.documentElement;

    var windowHeight = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight );

    if(windowHeight < 600){
        windowHeight = 600;
    }

    var contentHeight = 0;
    $("#content").children().each(function(){
        contentHeight += $(this).outerHeight(true); // true = include margins
    });

    var footerHeight = $("footer").height();

    //console.log(windowHeight + ' ' + contentHeight + ' ' + footerHeight);
    var heightDocument = windowHeight + contentHeight + footerHeight + 150;


    //fixed parallax for header and footer
    $('#scroll-animate, #scroll-animate-main').css({
        'height' :  heightDocument + 'px'
    });

    $('header').css({
        'height' : windowHeight + 'px'
    });

    $('.wrapper-parallax').css({
        'margin-top' : windowHeight + 'px'
    });
    //end of fixed parallax for header and footer

    $("#infinity").css({
        height:windowHeight +'px'
    });

    $(".infinity-content, .parallax-container").css({
        height:windowHeight/2 + 'px'
    });

    //hide mobile menu on click
    $( ".nav a" ).click(function() {
        if($('.navbar-toggler').css('display') !='none' && !$(this).hasClass('dropdown-toggle')){
            $( ".navbar-toggler" ).click();
        }
    });

    $("h1").fitText(
        1.2, {
            minFontSize: '35px',
            maxFontSize: '65px'
        }
    );

    //menu scroll on click
    $('a.page-scroll').bind('click', function(event) {
        var $ele = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($ele.attr('href')).offset().top - menu_offset(windowHeight))
        }, 1450, 'easeInOutExpo');
        event.preventDefault();
    });

    //submenu features scroll on click
    $('a.features-scroll').bind('click', function(event) {
        var $ele = $(this);
        var elemId = $ele.attr('href');
        var bgColor = $(elemId).data('background');
        //console.log('bgElement: '+ $(elemId).position().top);
        //console.log('feature height: '+ $('#features').position().top);

        $('html, body').animate({scrollTop: ($('#features').offset().top - menu_offset(windowHeight))}, 1000, 'easeInOutExpo');
        //$("#features").delay( 1000 ).animate({ scrollTop: ($($bgElement).offset().top) }, 2000);
        $("#features").delay( 1000 ).animate({ scrollTop: $(elemId).parent().scrollTop() + $(elemId).offset().top - $(elemId).parent().offset().top}, 1000);
        $('.slide').css('background-color', bgColor);

        event.preventDefault();
    });

    //$(".preview").sticky({topSpacing:100});

    $(".loader").delay(500).fadeOut();
    $("#mask").delay(1000).fadeOut("slow");

    scrollFooter(window.scrollY, footerHeight);

    /*
     $('.slide').height(windowHeight).scrollie({
     scrollOffset : -100,
     scrollingInView : function(elem) {
     console.log('scrolled');
     var bgColor = elem.data('background');
     $('.fill').css('background-color', bgColor);
     }
     });
     */

    //parallax
    $('.parallax-holder').scrollie({
        direction : 'both', //can be  "both", "down", or "up"
        scrollOffset : -100,
        scrollingInView : function(elem, offset, direction, coords, scrollRatio, thisTop, winPos) {
            $(".parallax-holder").animate({ scrollTop: coords/4}, 0);

        }
    });

    //animations
    $('.animate').scrollie({
        direction : 'up', //can be  "both", "down", or "up"
        scrollOffset : -50,
        scrollingInView : function(elem) {
            //console.log('scrolled');
            if(!elem.hasClass( "no-animation" )){
                var animation = elem.data('animation');
                var delay = elem.data('delay');
                if(!delay){
                    delay = 0;
                }
                $(elem).animateCss(animation, delay);
            }
        }
    });

    $('.gallery-box').magnificPopup({type:'image'});

    loadsuperslides();


    $(window).resize(function() {
        if(windowWidth != $(window).width()){
            location.reload();
            return;
        }
    });

    //highlight links on scroll (bootstrap feature)
    $('body').scrollspy({
        target: '#collapsingNavbar',
        offset: 100
    })

    $(window).scroll(function(){
        var scroll = window.scrollY;
        $('#scroll-animate-main').css({
            'top' : '-' + scroll + 'px'
        });
        $('header').css({
            'background-position-y' : 50 - (scroll * 100 / heightDocument) + '%'
        });
        scrollFooter(scroll, footerHeight);
    });

    //extend animate.js
    $.fn.extend({
        animateCss: function (animationName, delay) {
            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            if(delay){
                $(this).css('animation-delay', delay + 's');
            }
            $(this).addClass('animated ' + animationName).one(animationEnd, function() {
                $(this).removeClass('animated ' + animationName);
                $(this).addClass('no-animation');//this will be used to prevent animation repetition
            });
        }
    });

    function menu_offset(windowHeight){
        if(windowHeight >= 767){
            return 60;
        }
        if(windowHeight < 767){
            return 20;
        }
    }

    function loadsuperslides(){
        $('#slider').superslides({
            hashchange: false,
            animation: 'fade',
            play: 10000,
            pagination: false
        });
    }

    function scrollFooter(scrollY, heightFooter)
    {
        //console.log(scrollY);
        //console.log(heightFooter);

        if(scrollY >= heightFooter)
        {
            $('footer').css({
                'bottom' : '0px'
            });
        }
        else
        {
            $('footer').css({
                'bottom' : '-' + heightFooter + 'px'
            });
        }
    }


})(jQuery); // End of use strict
