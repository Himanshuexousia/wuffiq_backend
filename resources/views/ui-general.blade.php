@extends('app')

@section('page-title')
    General
@endsection

@section('page-css')
    <style>
        .border-white [class^=col-] {
            padding-top: 10px;
            padding-bottom: 10px;
            border: 1px solid #FFF;
        }
        #bg_text_markup p {
            padding: 5px;
            text-align: center;
            font-weight: bold;
        }
    </style>
@endsection
@section('content-header')
    <h1>
        General
    </h1>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Jumbotron
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-9">
                    <div class="jumbotron p10">
                        <h1>Title Jumbotron</h1>
                        <p>A lightweight, flexible component that can optionally extend the entire viewport to showcase key content on your site.</p>
                        <p><a role="button" class="btn btn-primary btn-lg">Learn more</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <h5>Code:</h5>
                    <p>
                        <code>
                            &lt;div class=&quot;jumbotron&quot;&gt;<br />
                            &lt;h1&gt;...&lt;/h1&gt;<br />
                            &lt;p&gt;...&lt;/p&gt;<br />
                            &lt;/div&gt;
                        </code>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Color Palettes
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-2">
                    <ul class="list-group">
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Darken" class="list-group-item light-blue darker">.darker</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Normal" class="list-group-item light-blue">.light-blue</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Lighten" class="list-group-item light-blue lighter">.lighter</li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul class="list-group">
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Darken" class="list-group-item aqua darker">.darker</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Normal" class="list-group-item aqua">.aqua</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Lighten" class="list-group-item aqua lighter">.lighter</li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul class="list-group">
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Darken" class="list-group-item yellow darker">.darker</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Normal" class="list-group-item yellow">.yellow</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Lighten" class="list-group-item yellow lighter">.lighter</li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul class="list-group">
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Darken" class="list-group-item blue darker">.darker</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Normal" class="list-group-item blue">.blue</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Lighten" class="list-group-item blue lighter">.lighter</li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul class="list-group">
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Darken" class="list-group-item navy darker">.darker</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Normal" class="list-group-item navy">.navy</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Lighten" class="list-group-item navy lighter">.lighter</li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul class="list-group">
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Darken" class="list-group-item teal darker">.darker</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Normal" class="list-group-item teal">.teal</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Lighten" class="list-group-item teal lighter">.lighter</li>
                    </ul>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-2">
                    <ul class="list-group">
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Darken" class="list-group-item olive darker">.darker</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Normal" class="list-group-item olive">.olive</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Lighten" class="list-group-item olive lighter">.lighter</li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul class="list-group">
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Darken" class="list-group-item lime darker">.darker</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Normal" class="list-group-item lime">.lime</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Lighten" class="list-group-item lime lighter">.lighter</li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul class="list-group">
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Darken" class="list-group-item orange darker">.darker</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Normal" class="list-group-item orange">.orange</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Lighten" class="list-group-item orange lighter">.lighter</li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul class="list-group">
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Darken" class="list-group-item fuchsia darker">.darker</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Normal" class="list-group-item fuchsia">.fuchsia</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Lighten" class="list-group-item fuchsia lighter">.lighter</li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul class="list-group">
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Darken" class="list-group-item maroon darker">.darker</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Normal" class="list-group-item maroon">.marron</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Lighten" class="list-group-item maroon lighter">.lighter</li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul class="list-group">
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Darken" class="list-group-item gray darker">.darker</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Normal" class="list-group-item gray">.gray</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Lighten" class="list-group-item gray lighter">.lighter</li>
                    </ul>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-2">
                    <ul class="list-group">
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Darken" class="list-group-item black darker">.darker</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Normal" class="list-group-item black">.black</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Lighten" class="list-group-item black lighter">.lighter</li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul class="list-group">
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Darken" class="list-group-item red darker">.darker</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Normal" class="list-group-item red">.red</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Lighten" class="list-group-item red lighter">.lighter</li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul class="list-group">
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Darken" class="list-group-item purple darker">.darker</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Normal" class="list-group-item purple">.purple</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Lighten" class="list-group-item purple lighter">.lighter</li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul class="list-group">
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Darken" class="list-group-item green darker">.darker</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Normal" class="list-group-item green">.green</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Lighten" class="list-group-item green lighter">.lighter</li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul class="list-group">
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Darken" class="list-group-item pink darker">.darker</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Normal" class="list-group-item pink">.pink</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Lighten" class="list-group-item pink lighter">.lighter</li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul class="list-group">
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Darken" class="list-group-item light-gray darker">.darker</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Normal" class="list-group-item light-gray">.light-gray</li>
                        <li data-toggle="tooltip" data-placement="left" title="" data-original-title="Lighten" class="list-group-item light-gray lighter">.lighter</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Miscellaneous
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <h2 class="page-header">Typography</h2>
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#headlines" data-toggle="tab" role="tab">Headlines</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#display" data-toggle="tab" role="tab">Display headings</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#markup" data-toggle="tab" role="tab">Markup</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#block_quotes" data-toggle="tab" role="tab">Block Quotes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#lists" data-toggle="tab" role="tab">Lists</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#dvlists" data-toggle="tab" role="tab">Description Vertical</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#dhlists" data-toggle="tab" role="tab">Description Horizontal</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="headlines" role="tabpanel">
                            <h1>h1. Bootstrap heading</h1>
                            <h2>h2. Bootstrap heading</h2>
                            <h3>h3. Bootstrap heading</h3>
                            <h4>h4. Bootstrap heading</h4>
                            <h5>h5. Bootstrap heading</h5>
                            <h6>h6. Bootstrap heading</h6>
                        </div>
                        <div class="tab-pane" id="display" role="tabpanel">
                            <h1 class="display-1">Display 1</h1>
                            <h1 class="display-2">Display 2</h1>
                            <h1 class="display-3">Display 3</h1>
                            <h1 class="display-4">Display 4</h1>
                            <h1 class="display-5">Display 5</h1>
                        </div>
                        <div class="tab-pane" id="markup" role="tabpanel">
                            <div class="row">
                                <div class="col-md-4">
                                    <h5 class="page-header text-md-center">Text</h5>
                                    <p class="lead">Text markup to Lead</p>
                                    <p class="text-green">Text markup to green (success)</p>
                                    <p class="text-aqua">Text markup to aqua (info light)</p>
                                    <p class="text-light-blue">Text markup to light blue (info dark)</p>
                                    <p class="text-red">Text markup to red (danger)</p>
                                    <p class="text-yellow">Text markup to yellow (warning)</p>
                                    <p class="text-muted">Text markup to muted</p>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="page-header text-md-center">Background</h5>
                                        <p class="bg-primary">Primary</p>
                                        <p class="bg-success">Success</p>
                                        <p class="bg-info">Info</p>
                                        <p class="bg-warning">Warning</p>
                                        <p class="bg-danger">Danger</p>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="page-header text-md-center">Others</h5>
                                    <div class="col-md-6">
                                        <p><mark>Highlight</mark> text.</p>
                                        <p><del>Deleted text.</del></p>
                                        <p><s>Striked text.</s></p>
                                        <p><ins>Underlined.</ins></p>
                                        <p><u>Underlined.</u> (Deprecated)</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><small>Small text.</small></p>
                                        <p><strong>Bold text.</strong></p>
                                        <p><em>Italicized text.</em></p>
                                        <p><abbr title="attribute">attr</abbr></p>
                                        <p><abbr title="HyperText Markup Language" class="initialism">HTML</abbr></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="block_quotes" role="tabpanel">
                            <h5 class="page-header text-md-center">Blockquote Left</h5>
                            <blockquote class="blockquote">
                                <p>Fusce fermentum odio nec arcu. Quisque rutrum. Praesent nonummy mi in odio. Praesent venenatis metus at tortor pulvinar varius. Proin magna.</p>
                                <footer class="blockquote-footer">Someone in <cite title="Checkmate Digital">Checkmate Digital</cite></footer>
                            </blockquote>
                            <hr />
                            <h5 class="page-header text-md-center">Blockquote Right</h5>
                            <blockquote class="blockquote blockquote-reverse">
                                <p>Fusce fermentum odio nec arcu. Quisque rutrum. Praesent nonummy mi in odio. Praesent venenatis metus at tortor pulvinar varius. Proin magna.</p>
                                <footer class="blockquote-footer">Someone in <cite title="Checkmate Digital">Checkmate Digital</cite></footer>
                            </blockquote>
                        </div>
                        <div class="tab-pane" id="lists" role="tabpanel">
                            <div class="row mh5">
                                <div class="col-md-3">
                                    <h5 class="page-header text-md-center">Unordered List</h5>
                                    <ul>
                                        <li>Lorem ipsum dolor sit amet</li>
                                        <li>Consectetur adipiscing elit</li>
                                        <li>Integer molestie lorem at massa
                                            <ul>
                                                <li>Lorem</li>
                                                <li>Consectetur</li>
                                                <li>Integer</li>
                                                <li>Facilisis</li>
                                            </ul></li>
                                        <li>Facilisis in pretium nisl aliquet</li>
                                    </ul>
                                </div>
                                <div class="col-md-3 bg-gray disabled">
                                    <h5 class="page-header text-md-center">Ordered List</h5>
                                    <ol>
                                        <li>Lorem ipsum dolor sit amet</li>
                                        <li>Consectetur adipiscing elit</li>
                                        <li>Integer molestie lorem at massa
                                            <ol>
                                                <li>Lorem</li>
                                                <li>Consectetur</li>
                                                <li>Integer</li>
                                                <li>Facilisis</li>
                                            </ol></li>
                                        <li>Facilisis in pretium nisl aliquet</li>
                                    </ol>
                                </div>
                                <div class="col-md-3">
                                    <h5 class="page-header text-md-center">Unstyled List</h5>
                                    <ul class="list-unstyled">
                                        <li>Lorem ipsum dolor sit amet</li>
                                        <li>Consectetur adipiscing elit</li>
                                        <li>Integer molestie lorem at massa corpione
                                            <ul>
                                                <li>Lorem</li>
                                                <li>Consectetur</li>
                                                <li>Integer</li>
                                                <li>Facilisis</li>
                                            </ul></li>
                                        <li>Facilisis in pretium nisl aliquet</li>
                                    </ul>
                                </div>
                                <div class="col-md-3 bg-gray disabled">
                                    <h5 class="page-header text-md-center">Mixed List</h5>
                                    <ol>
                                        <li>Lorem ipsum dolor sit amet</li>
                                        <li>Consectetur adipiscing elit</li>
                                        <li>Integer molestie lorem at massa
                                            <ul>
                                                <li>Lorem</li>
                                                <li>Consectetur</li>
                                                <li>Integer</li>
                                                <li>Facilisis</li>
                                            </ul></li>
                                        <li>Facilisis in pretium nisl aliquet</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane p10" id="dvlists" role="tabpanel">
                            <dl>
                                <dt>Description lists</dt>
                                <dd>A description list is perfect for defining terms.</dd>
                                <dt>Euismod</dt>
                                <dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
                                <dd>Donec id elit non mi porta gravida at eget metus.</dd>
                                <dt>Malesuada porta</dt>
                                <dd>Etiam porta sem malesuada magna mollis euismod.</dd>
                            </dl>
                        </div>
                        <div class="tab-pane p10" id="dhlists" role="tabpanel">
                            <dl class="dl-horizontal">
                                <dt class="col-sm-3">Description lists</dt>
                                <dd class="col-sm-9">A description list is perfect for defining terms.</dd>

                                <dt class="col-sm-3">Euismod</dt>
                                <dd class="col-sm-9">Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
                                <dd class="col-sm-9 col-sm-offset-3">Donec id elit non mi porta gravida at eget metus.</dd>

                                <dt class="col-sm-3">Malesuada porta</dt>
                                <dd class="col-sm-9">Etiam porta sem malesuada magna mollis euismod.</dd>

                                <dt class="col-sm-3 text-truncate">Truncated term is truncated</dt>
                                <dd class="col-sm-9">Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Collapsible Accordion
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-6">
                    <div id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a  id="statusCollapsible" class="btn btn-flat btn-block btn-primary" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Collapsible Opened
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <p>Velit dignissim et dictumst in quis sagittis enim montes, augue vut dolor tortor, vel sit risus enim mid? Nisi nec est ac amet pid dictumst risus! Eros risus adipiscing magnis nec magna nec phasellus sit ut, purus quis massa dis, arcu odio, porta! Proin integer pulvinar aliquet aenean eu ac.</p>

                                <p>Ac nec pulvinar et. Pellentesque ut pellentesque pellentesque purus augue integer, purus hac in hac. Ac arcu, in amet egestas auctor! Ac nascetur sociis est est, aliquam? Ultricies platea, montes? Lectus ut pulvinar, enim sit est scelerisque proin vel? Proin. Nec, aenean, pid ac, et. Ac sed magna augue.</p>

                                <p>Rhoncus natoque phasellus tortor, sit rhoncus, pid. Cum magna phasellus lacus, sit tristique tincidunt tincidunt mid nascetur a, lorem, tempor et urna nunc dis, integer lectus sed integer turpis mauris parturient elementum? Ac odio, penatibus, amet arcu, enim habitasse? Enim rhoncus diam. A a, tincidunt quis urna enim turpis.</p>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed btn btn-block btn-flat btn-danger" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Collapsible Two
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed btn btn-block btn-flat btn-success" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Collapsible Three
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a class="collapsed btn btn-flat btn-block btn-secondary" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                        Collapsible Four
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                <p>Velit dignissim et dictumst in quis sagittis enim montes, augue vut dolor tortor, vel sit risus enim mid? Nisi nec est ac amet pid dictumst risus! Eros risus adipiscing magnis nec magna nec phasellus sit ut, purus quis massa dis, arcu odio, porta! Proin integer pulvinar aliquet aenean eu ac.</p>

                                <p>Ac nec pulvinar et. Pellentesque ut pellentesque pellentesque purus augue integer, purus hac in hac. Ac arcu, in amet egestas auctor! Ac nascetur sociis est est, aliquam? Ultricies platea, montes? Lectus ut pulvinar, enim sit est scelerisque proin vel? Proin. Nec, aenean, pid ac, et. Ac sed magna augue.</p>

                                <p>Rhoncus natoque phasellus tortor, sit rhoncus, pid. Cum magna phasellus lacus, sit tristique tincidunt tincidunt mid nascetur a, lorem, tempor et urna nunc dis, integer lectus sed integer turpis mauris parturient elementum? Ac odio, penatibus, amet arcu, enim habitasse? Enim rhoncus diam. A a, tincidunt quis urna enim turpis.</p>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed btn btn-block btn-flat btn-secondary" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                        Collapsible Five
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSix">
                                <h4 class="panel-title">
                                    <a class="collapsed btn btn-block btn-flat btn-secondary" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                        Collapsible Six
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Carousel
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-12">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <img src="http://placehold.it/1750x325/6fe263/ffffff&amp;text=Bootstrap+Carousel" alt="Slide #1">
                                <div class="carousel-caption">
                                    First Slide
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="http://placehold.it/1750x325/ca070f/ffffff&amp;text=Checkmate+Digital" alt="Slide #2">
                                <div class="carousel-caption">
                                    Second Slide
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="http://placehold.it/1750x325/341eb6/ffffff&amp;text=Bootstrap+Carousel" alt="Slide #3">
                                <div class="carousel-caption">
                                    Third Slide
                                </div>
                            </div>
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="icon-prev" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="icon-next" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script>
        $('#statusCollapsible').on('click', function () {
            if($('#statusCollapsible').hasClass('collapsed'))
            {
                $('#statusCollapsible')[0].innerText = 'Collapsible Opened';
            } else
            {
                $('#statusCollapsible')[0].innerText = 'Collapsible Closed';
            }
        });
    </script>
@endsection