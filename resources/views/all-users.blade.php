@extends('app')

@section('page-title')
    User Tables
@endsection

@section('page-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap4.min.css">
     <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet">

@endsection
@section('content-header')
    <h1>
       User Tables
    </h1>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Users
                    <div class="pull-md-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-block">
                    <table id="example1" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" data-page-length='100'>
                        <thead>
                        <tr class="table-active">
                            <th></th>
                            <th>Name</th>
                            <th>User id</th>
                            <th>User gender</th>
                            <th>Phone</th>
                            <th>D.O.J</th>
							<th>Registered</th>
                            <th>Status</th>
							 <th>VIP</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr class="table-active">
                            <th></th>
                            <th>Name</th>
                            <th>User id</th>
                            <th>User gender</th>
                            <th>Phone</th>
                            <th>D.O.J</th>
                            <th>Registered</th>
                            <th>Status</th>
                            <th>VIP</th>
							<th>Action</th>
                        </tr>
                        </tfoot>
                        <tbody>
						@foreach($users as $user)
                        <tr>
                            <td id="color_{{$user->id}}" onclick="readStatus({{$user->id}})" @if($user->read_admin=='0') style="background-color:red;cursor:pointer;" @else style="background-color:green;cursor:pointer;" @endif>
							@if($user->read_admin=='0')
							<img src="{{URL('../images/envelope.png')}}">
							@else
							<img src="{{URL('../images/message.png')}}">
							@endif
							</td>
                            <td>{{$user->username}}</td>
                            <td>{{$user->id}}</td>
                            <td>{{$user->gender}}</td>
                            <td>{{$user->phone}}</td>
                            <td>{{date('M-d-Y',$user->regDate)}}</td>
                            <td>@if($user->user_registration==2)
							Half Registered	
							@else
							Full Registered		
							@endif								
							</td>
                            <td>
							@if($user->banAdmin=='0')
							Activated
							@else
							<span style="color:red;">Suspeded</span>	
							@endif
							</td>
							<td>
							@if($user->vip=='0')
							No
							@else
							Yes	
							@endif
							</td>
                            <td><div class="btn-group">
                                <button @if(!empty($user->user_registration)&& ($user->user_registration==1)) onclick="return viewUser({{$user->id}})" @else onclick="viewmsg()" @endif type="button" class="btn btn-primary" style="background-color:#3f51b5;"><i class="fa fa-rocket"></i>&nbsp;&nbsp;View</button>
                                <button type="button" class="btn btn-primary dropdown-toggle" style="background-color:#3f51b5;" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
@if($user->vip=='0')                                    
<li><a href="#" onclick="return action({{$user->id}},'3')" >&nbsp;&nbsp;&nbsp;vip&nbsp;&nbsp;&nbsp;</a></li>
@else
<li><a href="#" onclick="return action({{$user->id}},'5')" >&nbsp;&nbsp;&nbsp;Remove vip&nbsp;&nbsp;&nbsp;</a></li>
@endif	
@if($user->banAdmin=='0')  
<li><a href="#" onclick="return action({{$user->id}},'1')" >&nbsp;&nbsp;&nbsp;Suspend&nbsp;&nbsp;&nbsp;</a></li>
@else
<li><a href="#" onclick="return action({{$user->id}},'4')" >&nbsp;&nbsp;&nbsp;Active&nbsp;&nbsp;&nbsp;</a></li>
@endif
<li><a href="#" onclick="return action({{$user->id}},'2')">&nbsp;&nbsp;&nbsp;Delete&nbsp;&nbsp;&nbsp;</a></li>
                                  
                                </ul>
                            </div></td>
                        </tr>
                       @endforeach 
                        
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

   
@endsection

@section('page-scripts')
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('/js/table-data.js')}}"></script>
    <script src="{{asset('js/toastr.min.js')}}"></script>

    <script>
        // $(function() {

            // $('#example1').DataTable();

        // });

function readStatus(id){
	
	$('#color_'+id).css('background-color','green');
	$('#color_'+id).html('<img src=\'{{URL("../images/message.png")}}\'>');
	$.ajax({
		url:"{{URL('API/dbReadStatus')}}",
		type:"POST",
		data:{id:id}
	}).done(function(data){
	})
	return false;
}

		
function action(id,type){
	if(type=='3')
	{
		var x=confirm('are you sure you want to make this user vip');
	if(x){
	$.ajax({
		url:"{{URL('API/dbUserAction')}}",
		type:"POST",
		data:{id:id,type:type}
	}).done(function(data){
		location.reload();
		
	})
	}
	}
	else if(type=='2')
	{
			var x=confirm('are you sure you want to delete this user');
	if(x){
	$.ajax({
		url:"{{URL('API/dbUserAction')}}",
		type:"POST",
		data:{id:id,type:type}
	}).done(function(data){
		location.reload();
		
	})
	}
	}
	else if(type=='1')
	{
		var x=confirm('are you sure you want to suspend this user');
	if(x){
	$.ajax({
		url:"{{URL('API/dbUserAction')}}",
		type:"POST",
		data:{id:id,type:type}
	}).done(function(data){
		location.reload();
		
	})
	}
	}
	else if(type=='5')
	{
			var x=confirm('are you sure you want to remove this user from vip list');
	if(x){
	$.ajax({
		url:"{{URL('API/dbUserAction')}}",
		type:"POST",
		data:{id:id,type:type}
	}).done(function(data){
		location.reload();
		
	})
	}
	}
	else if(type=='4')
	{
	var x=confirm('are you sure you want make this user active');
	if(x){
	$.ajax({
		url:"{{URL('API/dbUserAction')}}",
		type:"POST",
		data:{id:id,type:type}
	}).done(function(data){
		location.reload();
		
	})
	}	
	}
	
	return false;
}
	
function viewUser(id){
var url="all-users/view-id-"+id;
window.location.href="http://www.wuffiq.com/admin/all-users/view-id-"+id;
	
}		

function viewmsg(){
	Command: toastr['warning']("This user has not complete it's profile.")

toastr.options = {
  "closeButton": true,
  "debug": false,
  "progressBar": true,
  "preventDuplicates": false,
  "positionClass": "toast-top-right",
  "onclick": null,
  "showDuration": "400",
  "hideDuration": "1000",
  "timeOut": "7000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
}
		$(document).ready(function() {
    $('#example').DataTable( {
	
        "order": [[ 'id', "desc" ]],
	      "pageLength": 100
		
    } );

} );
    </script>
@endsection