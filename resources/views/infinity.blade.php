<?php if(!config('infinity.use_minified')){$minified = true;}else{$minified = false;} ?>

        <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="index, follow">
    <meta name="keywords" content="app, admin, dashboard, laravel, php, html5, theme, template"/>
    <meta name="description" content="Infinity Admin, a fully responsive web app admin theme with charts, dashboards, landing pages and components. Fully customized with more than 140.000 layout combinations." />

    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/img/favicon//ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <title>Infinity Premium Admin Theme</title>

    @if ($minified)

            <!--Normalize-->
    <link rel="stylesheet" href="{{asset('vendor/normalize-css/normalize.css')}}">

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" type="text/css">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="{{asset('vendor/animate.css/animate.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('vendor/superslides/stylesheets/superslides.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/magnific-popup/magnific-popup.css')}}" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('css/infinity.css')}}" type="text/css">

    @else

        <link rel="stylesheet" href="{{asset('dist/main-infinity.css')}}" type="text/css">

    @endif

                <!-- Custom Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>
<section id="home"></section><!--needed only to highlighting the link-->
<body>
<div id="mask">
    <div class="loader">
        <img src="{{asset('img/infinity/loading.gif')}}" alt='loading'>
    </div>
</div>
<div id="scroll-animate">
    <div id="scroll-animate-main">
        <div class="wrapper-parallax">
            <nav id="mainNav" class="navbar navbar-default navbar-fixed-top sps sps--abv">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand page-scroll" href="#home"><div class="brand-img"></div> </a>
                        <button class="navbar-toggler hidden-md-up pull-right" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>
                    <div class="collapse navbar-toggleable-sm" id="collapsingNavbar">
                        <ul class="nav navbar-nav pull-md-right">
                            <li class="nav-item">
                                <a class="nav-link page-scroll" href="#home">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link page-scroll" href="#infinity">Infinity</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" href="#" aria-haspopup="true" aria-expanded="false">Features</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item features-scroll" href="#feature1">Layout</a>
                                    <a class="dropdown-item features-scroll" href="#feature2">Responsive</a>
                                    <a class="dropdown-item features-scroll" href="#feature3">Laravel and Html5</a>
                                    <a class="dropdown-item features-scroll" href="#feature4">Authentication</a>
                                    <a class="dropdown-item features-scroll" href="#feature5">Widgets</a>
                                    <a class="dropdown-item features-scroll" href="#feature6">Drag and Drop</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link page-scroll" href="#gallery" data-src-base='{{asset('img/infinity/slider/')}}/' data-src='<768:01-small.jpg,<992:01-medium.jpg,<1199:01-large.jpg,>1200:01.jpg'>Gallery</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link page-scroll" href="#differentials" data-src-base='{{asset('img/infinity/slider/')}}/' data-src='<768:02-small.jpg,<992:02-medium.jpg,<1199:02-large.jpg,>1200:02.jpg'>Differentials</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link page-scroll" href="#about" data-src-base='{{asset('img/infinity/slider/')}}/' data-src='<768:03-small.jpg,<992:03-medium.jpg,<1199:03-large.jpg,>1200:03.jpg'>About</a>
                            </li>
                            <li class="nav-item nav-btn">
                                <a href="{{route('responsive')}}" target="_blank">Preview</a>
                            </li>
                        </ul>

                    </div>
                </div>
            </nav>

            <header>
                <div class="slider-top-content">
                    <img src="{{asset('img/infinity/infinity_logo_white.png')}}">
                    <hr>
                    <p>Laravel admin template <br>with front-end and more than 140.000 layout combinations!</p>
                    <a href="{{route('responsive')}}" class="btn btn-primary btn-xl page-scroll" target="_blank">Live Preview</a>
                    <a href="#infinity" class="btn btn-secondary btn-xl page-scroll">Features <i class="fa fa-long-arrow-down"></i></a>
                </div>
                <div id="slider">
                    <div class="overlay"></div>
                    <ul class="slides-container">
                        <li>
                            <img src="{{asset('img/infinity/slider/01.jpg')}}" alt="">
                        </li>
                        <li>
                            <img src="{{asset('img/infinity/slider/02.jpg')}}" alt="">
                        </li>
                    </ul>
                    <nav class="slides-navigation">
                        <a href="#" class="next"></a>
                        <a href="#" class="prev"></a>
                    </nav>
                </div>
            </header>
            <div id="content">
                <section class="bg-bs4" id="infinity">
                    <div class="container-fluid pdtop infinity-content">
                        <div class="row animate" data-animation="fadeInUp">
                            <div class="col-lg-8 col-lg-offset-2 text-xs-center">
                                <h2 class="section-heading">Premium fully responsive admin template</h2>
                                <hr class="light">
                                <p class="text-faded">
                                    The ultimate theme made in Bootstrap 4 <br>and Laravel 5 with a fully customized widgets.
                                </p>
                                <a href="#feature1" class="btn btn-secondary btn-xl page-scroll">Features</a>
                            </div>
                        </div>
                        <div class="parallax-content">
                            <div class="row">
                                <div class="col-lg-2 col-xs-4 text-xs-center">
                                    <img src="{{asset('img/infinity/logos/bootstrap-logo-white.png')}}" class="animate" data-animation="fadeIn" data-delay=".5">
                                </div>
                                <div class="col-lg-2 col-xs-4 text-xs-center">
                                    <img src="{{asset('img/infinity/logos/laravel-logo-white.png')}}" class="animate" data-animation="fadeIn" data-delay=".6">
                                </div>
                                <div class="col-lg-2 col-xs-4 text-xs-center">
                                    <img src="{{asset('img/infinity/logos/html5-logo-white.png')}}" class="animate" data-animation="fadeIn" data-delay=".7">
                                </div>
                                <div class="col-lg-2 col-xs-4 text-xs-center">
                                    <img src="{{asset('img/infinity/logos/css3-logo-white.png')}}" class="animate" data-animation="fadeIn" data-delay=".8">
                                </div>
                                <div class="col-lg-2 col-xs-4 text-xs-center">
                                    <img src="{{asset('img/infinity/logos/sass-logo-white.png')}}" class="animate" data-animation="fadeIn" data-delay=".9">
                                </div>
                                <div class="col-lg-2 col-xs-4 text-xs-center">
                                    <img src="{{asset('img/infinity/logos/gulp-logo-white.png')}}" class="animate" data-animation="fadeIn" data-delay="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="parallax-holder hidden-md-down">
                        <img data-src-base='{{asset('img/infinity/')}}/' data-src='<768:bootstrap4-small.jpg,<992:bootstrap4-medium.jpg,<1199:bootstrap4-large.jpg,>1200:bootstrap4.jpg'/>
                    </div>
                </section>
                <section id="features">
                    <div class="slide slide-one" data-background="#3498db" id="feature1">
                        <div class="container-fluid pdtop">
                            <div class="row">
                                <div class="col-md-4 feature text-md-right">
                                    <h2>More than 140.000 <br>layout combinations</h2>
                                    <p>With Infinity you can customize window type, <br>background color, header and sidebars with ease.</p>
                                </div>
                                <div class="col-md-8 text-md-left">
                                    <img src="{{asset('img/infinity/videos/colors.gif')}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="#feature2" class="btn btn-secondary btn-xl features-scroll">More features <i class="fa fa-long-arrow-down"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slide slide-two" data-background="#27ae60" id="feature2">
                        <div class="container-fluid pdtop">
                            <div class="row">
                                <div class="col-md-4 feature text-md-right">
                                    <h2>Responsive Layout</h2>
                                    <p>Fit in any screen, adapting the layout to <br>most of desktops and mobile devices.</p>
                                </div>
                                <div class="col-md-8 text-md-left">
                                    <img src="{{asset('img/infinity/videos/responsive.gif')}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="#feature3" class="btn btn-secondary btn-xl features-scroll">More features <i class="fa fa-long-arrow-down"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slide slide-three" data-background="#e74c3c" id="feature3">
                        <div class="container-fluid pdtop">
                            <div class="row">
                                <div class="col-md-4 feature text-md-right">
                                    <h2>Laravel Framework and <br>HTML5 versions included</h2>
                                    <p>Infinity comes integrated with Laravel 5, <br>the most used php framework in the world. <br>You can use the Html5 version also.</p>
                                </div>
                                <div class="col-md-8 text-md-left">
                                    <img src="{{asset('img/infinity/videos/laravel.gif')}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="#feature4" class="btn btn-secondary btn-xl features-scroll">More features <i class="fa fa-long-arrow-down"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slide slide-four" data-background="#e67e22" id="feature4">
                        <div class="container-fluid pdtop">
                            <div class="row">
                                <div class="col-md-4 feature text-md-right">
                                    <h2>Robust Authentication <br>and authorization system</h2>
                                    <p>Infinity comes with CheckAuth, Laravel's <br>premium package from CheckMate Digital. <br>With this you can manage users, roles, <br>permissions and allow social authentication with ease.</p>
                                </div>
                                <div class="col-md-8 text-md-left">
                                    <img src="{{asset('img/infinity/videos/login.gif')}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="#feature5" class="btn btn-secondary btn-xl features-scroll">More features <i class="fa fa-long-arrow-down"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slide slide-five" data-background="#9b59b6" id="feature5">
                        <div class="container-fluid pdtop">
                            <div class="row">
                                <div class="col-md-4 feature text-md-right">
                                    <h2>Hundreds of widgets</h2>
                                    <p>Infinity comes with 5 dashboard <br>options, 3 chart libraries, <br>9 page templates and hundreds <br>of elements and widgets. <br>All ready to use.</p>
                                </div>
                                <div class="col-md-8 text-md-left">
                                    <img src="{{asset('img/infinity/videos/widjets.gif')}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="#feature6" class="btn btn-secondary btn-xl features-scroll">More features <i class="fa fa-long-arrow-down"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slide slide-six" data-background="#4c0671" id="feature6">
                        <div class="container-fluid pdtop pdbottom">
                            <div class="row">
                                <div class="col-md-4 feature text-md-right">
                                    <h2>Drag and drop panels</h2>
                                    <p>Arrange your layout dragging pannels arround.</p>
                                </div>
                                <div class="col-md-8 text-md-left">
                                    <img src="{{asset('img/infinity/videos/drag.gif')}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="#gallery" class="btn btn-secondary btn-xl page-scroll">Gallery</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="gallery" class="">
                    <div class="container-fluid">
                        <div class="row no-gutter">
                            <div class="col-lg-4 col-sm-6">
                                <a href="{{asset('img/infinity/gallery/social_dashboard.png')}}" class="gallery-box">
                                    <img src="{{asset('img/infinity/gallery/social_dashboard_t.png')}}" class="img-responsive" alt="">
                                    <div class="gallery-box-caption">
                                        <div class="gallery-box-caption-content">
                                            <div class="project-category text-faded">
                                                Dashboard
                                            </div>
                                            <div class="project-name">
                                                Social Media
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <a href="{{asset('img/infinity/gallery/profile.png')}}" class="gallery-box">
                                    <img src="{{asset('img/infinity/gallery/profile_t.png')}}" class="img-responsive" alt="">
                                    <div class="gallery-box-caption">
                                        <div class="gallery-box-caption-content">
                                            <div class="project-category text-faded">
                                                Page
                                            </div>
                                            <div class="project-name">
                                                User profile
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <a href="{{asset('img/infinity/gallery/mailbox.png')}}" class="gallery-box">
                                    <img src="{{asset('img/infinity/gallery/mailbox_t.png')}}" class="img-responsive" alt="">
                                    <div class="gallery-box-caption">
                                        <div class="gallery-box-caption-content">
                                            <div class="project-category text-faded">
                                                Sample app interface
                                            </div>
                                            <div class="project-name">
                                                Mailbox
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <a href="{{asset('img/infinity/gallery/login.png')}}" class="gallery-box">
                                    <img src="{{asset('img/infinity/gallery/login_t.png')}}" class="img-responsive" alt="">
                                    <div class="gallery-box-caption">
                                        <div class="gallery-box-caption-content">
                                            <div class="project-category text-faded">
                                                Authentication
                                            </div>
                                            <div class="project-name">
                                                Login with social authentication included
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <a href="{{asset('img/infinity/gallery/contacts.png')}}" class="gallery-box">
                                    <img src="{{asset('img/infinity/gallery/contacts_t.png')}}" class="img-responsive" alt="">
                                    <div class="gallery-box-caption">
                                        <div class="gallery-box-caption-content">
                                            <div class="project-category text-faded">
                                                Page
                                            </div>
                                            <div class="project-name">
                                                Contacts
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <a href="{{asset('img/infinity/gallery/calendar.png')}}" class="gallery-box">
                                    <img src="{{asset('img/infinity/gallery/calendar_t.png')}}" class="img-responsive" alt="">
                                    <div class="gallery-box-caption">
                                        <div class="gallery-box-caption-content">
                                            <div class="project-category text-faded">
                                                Sample app interface
                                            </div>
                                            <div class="project-name">
                                                Calendar
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="differentials">
                    <div class="container-fluid pdtop pdbottom">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 text-xs-center">
                                <div>
                                    <i class="fa fa-2x fa-diamond animate" data-animation="bounceIn" data-delay=".5"></i>
                                    <h2>Premium support</h2>
                                    <p>You can count with our team<br> to solve problems and bugs<br> and help along the process</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 text-xs-center">
                                <div>
                                    <i class="fa fa-2x fa-file-text-o animate" data-animation="bounceIn" data-delay=".6"></i>
                                    <h2>Well documented</h2>
                                    <p>Complete documentation<br> to help you use 100%<br> of Infinity's potential</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 text-xs-center">
                                <div>
                                    <i class="fa fa-2x fa-star-o animate" data-animation="bounceIn" data-delay=".7"></i>
                                    <h2>Free Updates</h2>
                                    <p>We constantly update Infinity, <br>keeping all libraryes <br>up-to-date and bug free.</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 text-xs-center">
                                <div>
                                    <i class="fa fa-2x fa-heart-o animate" data-animation="bounceIn" data-delay=".8"></i>
                                    <h2>Free produtcs</h2>
                                    <p>$30.00 worth in products! <br>Infinity comes bundled with <br><a href="{{asset('http://www.checkmatedigital.com')}}" target="_blank">Checkauth</a> and <a href="{{asset('http://www.checkmatedigital.com')}}" target="_blank">AppTheme</a>. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="about" class="bg-foreground fill">
                    <div class="parallax-holder">
                        <img data-src-base='{{asset('img/infinity/')}}/' data-src='<768:infinity-devices-small.jpg,<992:infinity-devices-medium.jpg,<1199:infinity-devices-large.jpg,>1200:infinity-devices.jpg'/>
                    </div>
                    <div class="container-fluid ">
                        <div class="row">
                            <div class="col-lg-8 col-lg-offset-2 text-xs-center">
                                <h2 class="section-heading animate" data-animation="bounceIn" data-delay="1">Buying Infinity <br>you get:</h2>
                                <hr class="primary">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <ul>
                                    <li>Laravel 5.2 and Bootstrap 4</li>
                                    <li>Latest jQuery and jQuery UI</li>
                                    <li>Authentication and authorization system</li>
                                    <li>User management</li>
                                    <li>Backoffice and Front-end</li>
                                    <li>Plugins for extended functionality</li>
                                    <li>Intuitive and user-friendly interface</li>
                                    <li>Easy to use customize</li>
                                    <li>Responsive layout (desktops, tablets, mobile devices)</li>
                                    <li>Semantic and clean HTML5 and CSS3 code</li>
                                    <li>Fluid 12 column grid</li>
                                    <li>Form Validation</li>
                                    <li>Optimized for fast loading</li>
                                    <li>Cross-browser compatible</li>
                                    <li>Build with Sass CSS (Scss files are included)</li>
                                </ul>
                            </div>
                            <div class="col-lg-4">
                                <ul>
                                    <li>Assembled via Gulp</li>
                                    <li>Well structured code</li>
                                    <li>Retina Ready</li>
                                    <li>Off-canvas menu at smaller screens</li>
                                    <li>Complete User Interface elements and componenets</li>
                                    <li>Variety of widgets for content and sidebars</li>
                                    <li>Dynamic data tables</li>
                                    <li>50+ widgets & charts</li>
                                    <li>14+ unique pages</li>
                                    <li>Front End Template</li>
                                    <li>5 Dashboards</li>
                                    <li>Over 1000 icons</li>
                                    <li>Unlimited colors</li>
                                    <li>Multiple Colors Support</li>
                                    <li>Fluid, Rounded and Boxed Views</li>
                                    <li>3 different charts libraries</li>
                                    <li>Drag and drop panels</li>
                                </ul>
                            </div>
                            <div class="col-lg-4">
                                <ul>
                                    <li>Animations CSS3</li>
                                    <li>Invoice, Inbox, ToDo & Timeline pages</li>
                                    <li>404 and 500 error pages</li>
                                    <li>Login, Register, LockScreen, Profile</li>
                                    <li>Buttons, Notifications, Form Elements</li>
                                    <li>Static Tables, DataTables, Calendar</li>
                                    <li>Maps, Invoice, Grid</li>
                                    <li>Custom Scroll bars</li>
                                    <li>Typography, Charts</li>
                                    <li>Alerts & Notifications</li>
                                    <li>Tabs & Accordions</li>
                                    <li>Buttons & Icons</li>
                                    <li>Basic and advanced Form Elements</li>
                                    <li>Constant upgrades</li>
                                    <li>After sales support</li>
                                    <li>Documentation included</li>
                                    <li>More to come...!</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row text-xs-center" style="padding-bottom: 50px;"><a href="http://codecanyon.net/item/infinity-laravel-admin-theme-front-end-advanced-authentication-social-authentication/16849532?s_rank=1&ref=CheckmateDigital" class="btn btn-default btn-xl animate" data-animation="tada" target="_blank">Buy on Envato</a></div>
                    </div>
                </section>
            </div>
            <footer>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 text-xs-center pdtop">
                            Thanks for the visit!
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>
@if ($minified)

    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/jquery-ui/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('vendor/tether/js/tether.min.js') }}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('vendor/responsive-img.js/responsive-img.min.js')}}"></script>
    <script src="{{asset('js/jquery.easing.min.js')}}"></script>
    <script src="{{asset('vendor/superslides/jquery.superslides.min.js')}}"></script>
    <script src="{{asset('vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('js/jquery.fittext.js')}}"></script>
    <script src="{{asset('js/scrollPosStyler.js')}}"></script>
    <script src="{{asset('js/jquery.scrollie.min.js')}}"></script>
    <script src="{{asset('js/infinity.js')}}"></script>

@else

    <script src="{{asset('dist/main-infinity.js')}}"></script>

@endif

</body>

</html>