<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Infinity Admin - Lockscreen</title>
    <meta name="robots" content="index, follow">
    <meta name="keywords" content="app, admin, dashboard, laravel, php, html5, theme, template"/>
    <meta name="description" content="Infinity Admin, a fully responsive web app admin theme with charts, dashboards, landing pages and components. Fully customized with more than 140.000 layout combinations." />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="{{asset('vendor/tether/css/tether.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('vendor/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('vendor/ionicons/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Main css -->
    <link href="{{asset('css/main.css')}}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        html, body {
            overflow: hidden;
        }
        body {
            min-height: 1200px;
            height: 100%;
        }
        .lockscreen-wrapper
        {
            position: absolute;
            top: 100px;
            left: 50%;
            width: 400px;
            margin: 0 0 0 -200px;
        }
        .lockscreen-wrapper {
            animation-timing-function: linear;
        }
        a, a:visited, a:active {
            color: #39CCCC !important;
        }
    </style>
</head>
<body class="content-light lockscreen navy">
<div class="lockscreen-wrapper">
    <div class="lockscreen-logo">
        <a href="{{route('home')}}" class="logo">
            <img src="{{asset('img/infinity/infinity_logo_white.png')}}" height="80px">
        </a>
    </div>
    <div class="clearfix"></div>
    <p class="fs20 fw600 text-uppercase text-sm-center text-teal">
        Checkmate Digital
    </p>
    <Br />
    <div class="clearfix"></div>
    <div class="lockscreen-item">
        <div class="lockscreen-image">
            <img src="{{asset('img/avatars/user1.jpg')}}" alt="User Image">
        </div>

        <form class="lockscreen-credentials">
            <div class="input-group">
                <input type="password" class="form-control" placeholder="Your Password">
                <div class="input-group-btn">
                    <button class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
                </div>
            </div>
        </form>
    </div>
    <br />
    <br />
    <div class="clearfix"></div>
    <div class="lead text-sm-center">
        <p>Enter your password to retrieve your session.</p>
        <p><a href="{{route('checkauth.login')}}">Or sign in as a different user</a></p>
        <div class="alert alert-info" role="alert">
            <strong>Heads up!</strong> This is just the interface design, a working sample will be presented in the next release.
        </div>
    </div>
</div>

<script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('vendor/tether/js/tether.min.js')}}"></script>
<script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/jquery.particleground.min.js')}}"></script>
<script>
    $(function () {
        $('.lockscreen-credentials').hide();
        $('.lockscreen-image').animate({
            'left': '50%',
            'margin-left': '-45px'
        }, 500);
        $('.lockscreen-image').on('mouseover',function(){
            $('.lockscreen-credentials').show("slow");
            $('.lockscreen-image').animate({
                'left': '-10px',
                'margin-left': '0'
            }, 500);
        });
        var timeout;
        document.onmousemove = function(){
            clearTimeout(timeout);
            timeout = setTimeout(function(){
                $('.lockscreen-credentials').hide("slow");
                $('.lockscreen-image').animate({
                    'left': '50%',
                    'margin-left': '-45px'
                }, 500);
            }, 30000);
        };
        $('.content-light').particleground({
            dotColor: '#FFFFFF',
            lineColor: '#6d6d6d'
        });
    });
</script>
</body>
</html>
