@extends('app')

@section('page-title')
    Form Editors
@endsection

@section('page-css')
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/bootstrap3-wysihtml5/bootstrap3-wysihtml5.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/bootstrap-markdown/css/bootstrap-markdown.min.css')}}" />
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
@endsection
@section('content-header')
    <h1>
        Form Editors
    </h1>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            BS3 Wysihtml5
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <textarea id="wysihtml5" class="form-control" placeholder="Enter text ..."></textarea>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Summernote
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <textarea name="summernote" class="form-control" id="summernote" rows="10" cols="80"></textarea>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            CKEditor
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <textarea name="ckeditor1" class="form-control" id="ckeditor1" rows="10" cols="80">
                This is my textarea to be replaced with CKEditor.
            </textarea>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Markdown
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <textarea name="content" class="form-control" data-provide="markdown" rows="10"></textarea>
        </div>
    </div>

@endsection

@section('page-scripts')
    <script src="{{asset('vendor/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap3-wysihtml5/templates.js')}}"></script>
    <script src="{{asset('vendor/bootstrap3-wysihtml5/commands.js')}}"></script>
    <script src="{{asset('vendor/bootstrap-markdown/js/bootstrap-markdown.js')}}"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script>
    <script src="//cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script type="text/javascript">
        $('#wysihtml5').wysihtml5();
        $('#summernote').summernote({
            height: 200,
            codemirror: {
                theme: 'monokai'
            }
        });
        // Turn off automatic editor initilization.
        // Used to prevent conflictions with multiple text
        // editors being displayed on the same page.
        CKEDITOR.disableAutoInline = true;

        // Init Ckeditor
        CKEDITOR.replace('ckeditor1', {
            height: 200,
        });
    </script>
@endsection