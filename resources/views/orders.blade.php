@extends('app')

@section('page-title')
    User Tables
@endsection

@section('page-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap4.min.css">
@endsection
@section('content-header')
    <h1>
       Orders
    </h1>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Orders
                    <div class="pull-md-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-block">
                    <table id="example1" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" data-page-length='100'>
                        <thead>
                        <tr class="table-active">
                            <th></th>                          
                            <th>Order Id</th>
                            <th>Boy Id</th>
                            <th>Girl Id</th>
                            <th>Marriage State</th>
                            <th>Bank Name</th>                                                     
                            <th>Payment State</th>                                                     
                            <th>Preacher Name</th>                                                     
                            <th>Preacher State</th>                                                     
                            <th>Day</th>                                                     
                            <th>Time</th>
                            <th>Date</th>							
                        </tr>
                        </thead>
						
						<tbody>
					
								@foreach($json as $data)
								<tr>
								<td></td>
							    <td>500{{$data['orderId']}}</td>
								<td>{{$data['boyId']}}</td>
								<td>{{$data['girlId']}}</td>
								<td>
								@if($data['marriageStatus']==1)
									Accpted
								@elseif($data['marriageStatus']==2)
									Rejected
								@elseif($data['marriageStatus']==3)
									Hold
								@else
									Pending
								@endif
								</td>
								
								<td>{{$data['bankName']}}</td>
								<td>
								@if($data['payment']==1)
									Accpted
								@elseif($data['payment']==2)
									Rejected
								@else	
									Pending
								@endif
								
								</td>
								<td>{{$data['preacher']}}</td> 
								<td>
								@if($data['preacherStatus']==1)
									Accpted
								@elseif($data['preacherStatus']==2)
									Rejected
								@elseif($data['preacherStatus']==0)
									Pending
								@endif</td>
								<td>{{date('l',$data['time'])}}</td>
							     <td>{{date('h:ia',$data['time'])}}</td>
								 <td>{{date('d-m-y',$data['time'])}}</td>
								</tr>
								
								
								@endforeach
						</tbody>
						
                        <tfoot>
                        <tr class="table-active">
                             <th></th>
                            <th>Order Id</th>
                            <th>Boy Id</th>
                            <th>Girl Id</th>
                            <th>Marriage State</th>
                            <th>Bank Name</th>                                                     
                            <th>Payment State</th>                                                     
                            <th>Preacher Name</th>                                                     
                            <th>Preacher State</th>                                                     
                            <th>Day</th>                                                     
                            <th>Time</th> 
							<th>Date</th>
                        </tr>
                        </tfoot>

                    </table>
                </div>
            </div>
        </div>
    </div>

   
@endsection

@section('page-scripts')
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('/js/table-data.js')}}"></script>
	
@endsection
