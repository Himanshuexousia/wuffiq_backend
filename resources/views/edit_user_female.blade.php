@extends('app')

@section('page-title')
    General Form
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{asset('vendor/select2/css/select2.min.css')}}">

@endsection

@section('content-header')
    <h1>
        General Form
    </h1>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Edit User
                    <div class="pull-md-right">
                        
                    </div>
                </div>
                <div class="card-block">
                    <form  id='form2' method="POST"  onsubmit="return updateData()">
                        <fieldset class="form-group">
                            <label for="userName">User Name</label>
                            <input type="text" value="{{$data['userName']}}" class="form-control" id="userName" placeholder="User Name">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="userEmail">User Email</label>
                            <input type="email" value="{{$data['userEmail']}}" class="form-control" id="userEmail" placeholder="User Email">
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userPhone">User Phone</label>
                            <input readonly value="{{$data['userPhone']}}" type="text" class="form-control" id="userPhone" placeholder="User Phone">
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userGender">User Gender</label>
                            <select readonly class="form-control" id="userGender" placeholder="Another input">
							
							<option @if($data['userGender']=='Female') selected @endif  value="Female">Female</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="marriedStatus">Married Status</label>
                            <select class="form-control" id="marriedStatus" placeholder="Another input">
							<option @if($data['marriedStatus']=='GS1') selected @endif  value="GS1">Single-Virgin</option>
							<option @if($data['marriedStatus']=='GS2') selected @endif  value="GS2">Divorce</option>
							<option @if($data['marriedStatus']=='GS3') selected @endif  value="GS3">Widoed</option>
							</select>
                        </fieldset>
					
						<fieldset class="form-group">
                            <label for="qubelahStatus">Qubelah Status</label>
                            <select onchange="qStatusChange()" class="form-control" id="qubelahStatus" placeholder="Another input">
							<option @if($data['qubelahStatus']=='GQ1') selected @endif value="GQ1">Yes</option>
							<option @if($data['qubelahStatus']=='GQ2') selected @endif value="GQ2">No</option>
							</select>
                        </fieldset>
					
						<fieldset id="qName" class="form-group">
                            <label for="qubelahName">Qubelah Name</label>
                           <select class="form-control" id="qubelahName" placeholder="Another input">
							@foreach($qubelahName as $quName)
							<option @if($data['qubelahName']==$quName->QabelahName) selected @endif value="{{$quName->QabelahName}}">{{$quName->QabelahName}}</option>
							@endforeach
							</select>
                        </fieldset>
						
						<fieldset class="form-group">
                            <label for="userAge">User Age</label>
                            <input type="number" style="width:100%" min="18" max="100" value="{{$data['userAge']}}" class="form-control" id="userAge" placeholder="Another input">
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userEducation">User Education</label>
                           <select class="form-control" id="userEducation" placeholder="Another input">
							<option @if($data['userEducation']=='GE1') selected @endif value="GE1">no Education</option>
							<option @if($data['userEducation']=='GE2') selected @endif value="GE2">Elementry & Middle</option>
							<option @if($data['userEducation']=='GE3') selected @endif value="GE3">High School</option>
							<option @if($data['userEducation']=='GE4') selected @endif  value="GE4">Bachelor</option>
							<option @if($data['userEducation']=='GE5') selected @endif value="GE5">Master</option>
							<option @if($data['userEducation']=='GE6') selected @endif value="GE6">Doctor</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userHeight">User Height</label>
                      			<input type="number" step="0.02" style="width:100%" min="1" max="3" value="{{$data['userHeight']}}" class="form-control" id="userHeight" placeholder="Another input">
                            <!-- <input type="text" value="{{$data['userHeight']}}" class="form-control" id="userHeight" placeholder="Another input"> -->
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userWeight">User Weight</label>
                            <input type="number" style="width:100%" min="40" max="300" value="{{$data['userWeight']}}" class="form-control" id="userWeight" placeholder="Another input">
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userNationality">User Nationality</label>
                            <select class="form-control" id="userNationality" placeholder="Another input">
							@foreach($country as $userCountry)
							<option @if($data['userNationality']==$userCountry->girl) selected @endif value="{{$userCountry->girl}}">{{$userCountry->country_name}}</option>
							@endforeach
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userCity">User City</label>
                            <select class="form-control" id="userCity" placeholder="Another input">
							@foreach($city as $userCity)
							<option @if($data['userCity']==$userCity->girl) selected @endif  value="{{$userCity->girl}}">{{$userCity->status}}</option>
							@endforeach
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userReligion">User Religion</label>
                            <select class="form-control" id="userReligion" placeholder="Another input">
							<option @if($data['userReligion']=='GR1') selected @endif value="GR1">Suni</option>
							<option @if($data['userReligion']=='GR2') selected @endif value="GR2">Shiai</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label  for="hairColor">Hair Color</label>
                            <select class="form-control" id="hairColor" placeholder="Another input">
							<option @if($data['hairColor']=='GH1') selected @endif value="GH1">Black</option>
							<option @if($data['hairColor']=='GH2') selected @endif value="GH2">Brown</option>
							<option @if($data['hairColor']=='GH3') selected @endif value="GH3">White</option>
							<option @if($data['hairColor']=='GH4') selected @endif value="GH4">Black and White</option>
							<option @if($data['hairColor']=='GH5') selected @endif value="GH5">Little Hair</option>
							<option @if($data['hairColor']=='GH6') selected @endif value="GH6">No Hair</option>
							<option @if($data['hairColor']=='GH7') selected @endif value="GH7">Other</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userProfession">User Profession</label>
                            <select class="form-control" id="userProfession" placeholder="Another input">
							<option @if($data['userProfession']=='GJ1') selected @endif value="GJ1">Company</option>
							<option @if($data['userProfession']=='GJ2') selected @endif value="GJ2">Teacher</option>
							<option @if($data['userProfession']=='GJ3') selected @endif value="GJ3">Govremant</option>
							<option @if($data['userProfession']=='GJ4') selected @endif value="GJ4">Doctor</option>
							<option @if($data['userProfession']=='GJ5') selected @endif value="GJ5">Businesswoman</option>
							<option @if($data['userProfession']=='GJ6') selected @endif value="GJ6">Student</option>
							<option @if($data['userProfession']=='GJ7') selected @endif value="GJ7">Retired</option>
							<option @if($data['userProfession']=='GJ8') selected @endif value="GJ8">No Job</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="childrenStatus">Children Status</label>
                            <select class="form-control" id="childrenStatus" placeholder="Another input">
							<option @if($data['childrenStatus']=='GI1') selected @endif value="GI1">No</option>
							<option @if($data['childrenStatus']=='GI2') selected @endif value="GI2">Yes but the don't live with me</option>
							<option @if($data['childrenStatus']=='GI3') selected @endif value="GI3">Yes live with me 1 children</option>
							<option @if($data['childrenStatus']=='GI4') selected @endif value="GI4">Yes live with me 2 children</option>
							<option @if($data['childrenStatus']=='GI5') selected @endif value="GI5">Yes live with me 3 children</option>
							<option @if($data['childrenStatus']=='GI6') selected @endif value="GI6">Yes live with more then 3 children</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userHobbies">Hobbies</label>
                            <select id="userHobbies" class="form-control select2" multiple="multiple" data-placeholder="Select Hobbies" style="width: 100%;">
						@php ($userHobbies=json_decode($data['userHobbies']))
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='GO1')
selected
@endif
@endforeach	
							value="GO1">Sport</option>
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='GO2')
selected
@endif
@endforeach	
							value="GO2">Traval</option>
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='GO3')
selected
@endif
@endforeach	
							value="GO3">Watch TV</option>
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='GO4')
selected
@endif
@endforeach	
							value="GO4">Internet</option>
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='GO5')
selected
@endif
@endforeach	
							value="GO5">Read book</option>
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='GO6')
selected
@endif
@endforeach	
							value="GO6">Go To resturant & Mall</option>
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='GO7')
selected
@endif
@endforeach	
							value="GO7">Go to desert</option>
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='GO8')
selected
@endif
@endforeach	
							value="GO8">Go to sea</option>
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='GO9')
selected
@endif
@endforeach	
							value="GO9">Cook</option>
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='GO10')
selected
@endif
@endforeach	
							value="GO10">Alistrahah</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userIncome">Income</label>
                            <select class="form-control" id="userIncome" placeholder="Another input">
							<option @if($data['userIncome']=='GM1') selected @endif  value="GM1">Average</option>
							<option @if($data['userIncome']=='GM2') selected @endif value="GM2">Above average</option>
							<option @if($data['userIncome']=='GM3') selected @endif value="GM3">Below Averge</option>
							<option @if($data['userIncome']=='GM4') selected @endif value="GM4">Rich</option>
							<option @if($data['userIncome']=='GM5') selected @endif value="GM5">No Answer</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userReligious">Religious</label>
                            <select class="form-control" id="userReligious" placeholder="Another input">
							<option @if($data['userReligion']=='GU1') selected @endif value="GU1">Not Religious</option>
							<option @if($data['userReligion']=='GU2') selected @endif value="GU2">low Religious</option>
							<option @if($data['userReligion']=='GU3') selected @endif value="GU3">Religious</option>
							<option @if($data['userReligion']=='GU4') selected @endif value="GU4">very religious</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userTraditional">Traditional</label>
                            <select class="form-control" id="userTraditional" placeholder="Another input">
							<option  @if($data['userTraditional']=='GD1') selected @endif value="GD1">Not Traditions</option>
							<option @if($data['userTraditional']=='GD2') selected @endif value="GD2">Normal</option>
							<option @if($data['userTraditional']=='GD3') selected @endif value="GD3">Very Traditions</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userLook">Look</label>
                            <select class="form-control" id="userLook" placeholder="Another input">
							<option @if($data['lookStatus']=='GL1') selected @endif value="GL1">Normal</option>
							<option @if($data['lookStatus']=='GL2') selected @endif  value="GL2">Beautiful</option>
							<option @if($data['lookStatus']=='GL3') selected @endif  value="GL3">Very Beautiful</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userBody">Body</label>
                            <select class="form-control" id="userBody" placeholder="Another input">
							<option @if($data['bodyStatus']=='GY1') selected @endif  value="GY1">Thin</option>
							<option @if($data['bodyStatus']=='GY2') selected @endif  value="GY2">Normal</option>
							<option @if($data['bodyStatus']=='GY3') selected @endif  value="GY3">Little Big</option>
							<option @if($data['bodyStatus']=='GY4') selected @endif  value="GY4">Sport</option>
							<option @if($data['bodyStatus']=='GY5') selected @endif  value="GY5">Big</option>
							<option @if($data['bodyStatus']=='GY6') selected @endif  value="GY6">Huge</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userHealth">Health</label>
                            <select onchange='uHealthChange()' class="form-control" id="userHealth" placeholder="Another input">
							<option @if($data['healthStatus']=='GF1') selected @endif value="GF1">Good</option>
							<option @if($data['healthStatus']=='GF2') selected @endif value="GF2">Little Sick</option>
							<option @if($data['healthStatus']=='GF3') selected @endif value="GF3">Disabled</option>
							</select>
                        </fieldset>
						
					
						<fieldset id="lSick" style="display:none;" class="form-group">
                            <label for="sickType">Sick Type</label>
                            <input type="text" class="form-control" id="sickType" placeholder="Another input">
                        </fieldset>
						
						<fieldset id="dType" style="display:none;" class="form-group">
                            <label for="disableType">Disable Type</label>
                            <input type="text" class="form-control" id="disableType" placeholder="Another input">
                        </fieldset>
				
						<fieldset class="form-group">
                            <label for="otherInformation">Other Information</label>
                            <input type="text" value="{{$data['otherInformation']}}" class="form-control" id="otherInformation" placeholder="Another input">
                        </fieldset>
						<fieldset class="form-group">
                            <label for="privateInformation">Private Information</label>
                            <input type="text" value="{{$data['privateInformation']}}"  class="form-control" id="privateInformation" placeholder="Another input">
                        </fieldset>
					<fieldset class="form-group">
                            <label for="preacherStatus">Preature Account</label>
                            <select class="form-control" id="preacherStatus" placeholder="Another input">
							<option @if($data['preatureStatus']=='No') selected @endif value="No">No</option>
							<option @if($data['preatureStatus']=='Yes') selected @endif value="Yes">Yes</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="preacherAccount">Preature Account</label>
                            <input type="text" value="{{$data['preatureAccount']}}" class="form-control" id="preacherAccount" placeholder="Another input">
                        </fieldset>
						<fieldset class="form-group">
                            <label for="arabiaStatus">Fertile</label>
                            <select class="form-control" id="arabiaStatus" placeholder="Another input">
							<option @if($data['arabiaStatus']=='No') selected @endif value="No">No</option>
							<option @if($data['arabiaStatus']=='Yes') selected @endif value="Yes">Yes</option>
							</select>
                        </fieldset>
						
@if($data['userGender']=="Male")						
						<fieldset class="form-group">
                            <label for="beardStatus">Beard Status</label>
                            <select class="form-control" id="beardStatus" placeholder="Another input">
							<option @if($data['beardStatus']=='GK1') selected @endif value="GK1">Little Beard</option>
							<option @if($data['beardStatus']=='GK2') selected @endif value="GK2">Tall Beard</option>
							<option @if($data['beardStatus']=='GK3') selected @endif value="GK3">No Beard</option>
							</select>
                        </fieldset>
@else						
						<fieldset class="form-group">
                            <label for="hairLook">Hair Look Status</label>
                            <select class="form-control" id="hairLook" placeholder="Another input">
							<option @if($data['hairLookStatus']=='GZ1') selected @endif  value="GZ1">Black</option>
							<option @if($data['hairLookStatus']=='GZ2') selected @endif  value="GZ2">Brown</option> 
							</select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="bodycolor">Body Color</label>
                            <select class="form-control" id="bodycolor" placeholder="Another input">
							<option @if($data['bodyColor']=='GB1') selected @endif  value="GB1">very white</option>
							<option @if($data['bodyColor']=='GB2') selected @endif  value="GB2">White</option> 
							<option @if($data['bodyColor']=='GB3') selected @endif  value="GB3">Brown</option>
							<option @if($data['bodyColor']=='GB4') selected @endif  value="GB4">Dark Brown</option> 
							<option @if($data['bodyColor']=='GB5') selected @endif  value="GB5">little Black</option>
							<option @if($data['bodyColor']=='GB6') selected @endif  value="GB6">very Black</option> 
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="veilStatus">Veil Status</label>
                            <select class="form-control" id="veilStatus" placeholder="Another input">
							<option @if($data['veilStatus']=='GK1') selected @endif  value="GK1">Monagbah</option>
							<option @if($data['veilStatus']=='GK2') selected @endif  value="GK2">Cover Every Thing</option>
							<option @if($data['veilStatus']=='GK3') selected @endif  value="GK3">Tarhah</option>
							<option @if($data['veilStatus']=='GK4') selected @endif  value="GK4">Cover hair</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="virginStatus">Virgin Status</label>
                            <select class="form-control" id="virginStatus" placeholder="Another input">
							<option @if($data['virginStatus']=='No') selected @endif  value="No">No</option>
							<option @if($data['virginStatus']=='Yes') selected @endif  value="Yes">Yes</option>
							</select>
                        </fieldset>
	<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">				
						<fieldset class="form-group">
                            <label for="smsNumber">SMS Number</label>
                            <input type="Number" value="{{$data['smsNumber']}}" class="form-control" id="smsNumber" placeholder="Another input">
                        </fieldset>
@endif				
<center><button type="submit"     class="btn btn-success">Update</button></center>
		
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
@endsection

@section('page-scripts')
    <script src="{{asset('vendor/select2/js/select2.full.min.js')}}"></script>
    <script src="{{asset('js/form-advanced.js')}}"></script>


<script>
var userId ="{{$data['userId']}}";
var qStatus ="{{$data['qubelahStatus']}}";
var uGender ="{{$data['userGender']}}";
var uHealth ="{{$data['healthStatus']}}";
if(uHealth=='GF2'){
$('#lSick').css('display','block');		
}
else if(uHealth=='GF3'){
$('#dType').css('display','block');	
}

if(qStatus=="GQ"){	
$('#qName').css('display','block');
}
function qStatusChange(){
qStatus=document.getElementById('qubelahStatus').value; 
if(qStatus=='GQ1'){
$('#qName').css('display','block');	
}
else{
$('#qName').css('display','none');
}	
}


function uHealthChange(){
	
	uHealth=document.getElementById('userHealth').value;
	if(uHealth=='GF2'){
	$('#lSick').css('display','block');
	$('#dType').css('display','none');		
	}
	else if(uHealth=='GF3'){
	$('#dType').css('display','block');	
	$('#lSick').css('display','none');	
	}
}


function updateData(){

	var userId ="{{$data['userId']}}";
	var userName=document.getElementById('userName').value;
	var userEmail=document.getElementById('userEmail').value;
	var userGender=document.getElementById('userGender').value;
	var marriedStatus=document.getElementById('marriedStatus').value;
	var qubelahStatus=document.getElementById('qubelahStatus').value;
	if(qubelahStatus=='GQ1'){		
	var	qubelahName=document.getElementById('qubelahName').value;
	}
	else{
	var	qubelahName='';	
	}
	var userAge=document.getElementById('userAge').value;
	var userEducation=document.getElementById('userEducation').value;
	var userHeight=document.getElementById('userHeight').value;
	var userWeight=document.getElementById('userWeight').value;
	var userNationality=document.getElementById('userNationality').value;
	var userCity=document.getElementById('userCity').value;
	var userReligion=document.getElementById('userReligion').value;
	var hairColor=document.getElementById('hairColor').value;
	var userProfession=document.getElementById('userProfession').value;
	var childrenStatus=document.getElementById('childrenStatus').value;
	var userHobbies=$('#userHobbies').val();
	var userIncome=document.getElementById('userIncome').value;
	var userReligious=document.getElementById('userReligious').value;
	var userTraditional=document.getElementById('userTraditional').value;
	var userLook=document.getElementById('userLook').value;
	var userBody=document.getElementById('userBody').value;
	var userSmoke='No'
	var userHealth=document.getElementById('userHealth').value;
	var otherInformation=document.getElementById('otherInformation').value;
	var privateInformation=document.getElementById('privateInformation').value;
	var preacherStatus=document.getElementById('preacherStatus').value;
	var preacherAccount=document.getElementById('preacherAccount').value;
	var arabiaStatus=document.getElementById('arabiaStatus').value;
	var hairLook=document.getElementById('hairLook').value;
	var bodycolor=document.getElementById('bodycolor').value;
	var veilStatus=document.getElementById('veilStatus').value;
	var virginStatus=document.getElementById('virginStatus').value;
	var smsNumber=document.getElementById('smsNumber').value;
	var sickType='';
	var disableType='';
	if(userHealth=='GF2'){		
	 sickType=document.getElementById('sickType').value;	
	}
	else if(userHealth=='GF3'){
	 disableType=document.getElementById('disableType').value;		
	}
	
	var _token=document.getElementById('_token').value;
 
	$.ajax({
		url:"{{URL('API/updateUser')}}",
		type:"POST",
		data:{userId:userId,userName:userName,userEmail:userEmail,userGender:userGender,marriedStatus:marriedStatus,bodycolor:bodycolor,qubelahStatus:qubelahStatus,qubelahName:qubelahName,userAge:userAge,userEducation:userEducation,userHeight:userHeight,userWeight:userWeight,userNationality:userNationality,userCity:userCity,userReligion:userReligion,hairColor:hairColor,userProfession:userProfession,childrenStatus:childrenStatus,userHobbies:userHobbies,userIncome:userIncome,userReligious:userReligious,userTraditional:userTraditional,userLook:userLook,userBody:userBody,userSmoke:userSmoke,userHealth:userHealth,disableType:disableType,sickType:sickType,otherInformation:otherInformation,privateInformation:privateInformation,preacherStatus:preacherStatus,preacherAccount:preacherAccount,arabiaStatus:arabiaStatus,hairLook:hairLook,veilStatus:veilStatus,virginStatus:virginStatus,smsNumber:smsNumber,_token:_token}
	}).done(function(data){
		data=JSON.parse(data);
		if(data.status=='1'){
			Command: toastr['success']("Update Successfully")
			toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "progressBar": true,
			  "preventDuplicates": false,
			  "positionClass": "toast-top-right",
			  "onclick": null,
			  "showDuration": "400",
			  "hideDuration": "1000",
			  "timeOut": "7000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
		}
	})
	return false;
}
</script>
@endsection