@extends('app')

@section('page-title')
    Marketing Dashboard
@endsection

@section('page-css')

@endsection

@section('content-header')
    <h1>
        Marketing Dashboard
    </h1>
    @endsection

    @section('content')

    <div class="row">
        <section class="col-xl-3 col-lg-6 connectedSortable">
            <div class="card card-graphic">
                <div class="card-block">
                    <div class="inner">
                        <h4 class="mt5 mbn pull-left">Bounce Rate</h4>
                        <h4 class="text-system pull-right pr10">52.96%</h4>
                    </div>
                    <div class="text-xs-center graphic">
                        <canvas id="lineChart1" height="60" width="300"></canvas>
                    </div>

                </div>
                <div class="card-footer text-xs-center br-t">
                    <span>
                        <i class="fa fa-arrow-up" style="color: palegreen;"></i>
                        -6.2% INCREASE
                        <b>vs $56.49% (prev)</b>
                    </span>
                </div>
            </div>
        </section>
        <section class="col-xl-3 col-lg-6 connectedSortable">
            <div class="card card-graphic">
                <div class="card-block">
                    <div class="inner">
                        <h4 class="mt5 mbn pull-left">Pageviews</h4>
                        <h4 class="text-system pull-right pr10">2.33</h4>
                    </div>
                    <div class="text-xs-center graphic">
                        <canvas id="lineChart2" height="60" width="300"></canvas>
                    </div>

                </div>
                <div class="card-footer text-xs-center br-t">
                    <span>
                    <i class="fa fa-arrow-up" style="color: palegreen;"></i>
                    9.77% INCREASE
                    <b>vs 2.13 (prev)</b>
                    </span>
                </div>
            </div>
        </section>
        <section class="col-xl-3 col-lg-6 connectedSortable">
            <div class="card card-graphic">
                <div class="card-block">
                    <div class="inner">
                        <h4 class="mt5 mbn pull-left">New sessions</h4>
                        <h4 class="text-system pull-right pr10">65.0%</h4>
                    </div>
                    <div class="text-xs-center graphic">
                        <canvas id="lineChart3" height="60" width="300"></canvas>
                    </div>
                </div>
                <div class="card-footer text-xs-center br-t">
                    <span>
                        <i class="fa fa-arrow-down" style="color: palevioletred;"></i>
                        -5.5% INCREASE
                        <b>vs 68.8% (prev)</b>
                    </span>
                </div>
            </div>
        </section>
        <section class="col-xl-3 col-lg-6 connectedSortable">
            <div class="card card-graphic">
                <div class="card-block">
                    <div class="inner">
                        <h4 class="mt5 mbn pull-left">Time on site</h4>
                        <h4 class="text-system pull-right pr10">67s</h4>
                    </div>
                    <div class="text-xs-center graphic">
                        <canvas id="lineChart4" height="60" width="300"></canvas>
                    </div>
                </div>
                <div class="card-footer text-xs-center br-t">
                    <span>
                        <i class="fa fa-arrow-up" style="color: palegreen;"></i>
                        15.2% INCREASE
                        <b>vs 1m:60s (prev)</b>
                    </span>
                </div>
            </div>
        </section>
    </div>
    <div class="row">
        <section class="col-xl-12 connectedSortable">
            <!--midia ads -->
            <div class="card bg-gray-gradient" id="divTimeline">
                <div class="card-header">
                    <i class="fa fa-calendar"></i>

                    <h3 class="card-title">Midia ads schedule this month</h3>

                    <div class="card-tools pull-right">
                        <div class="btn-group" data-toggle="btn-toggle">
                            <button type="button" class="btn btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-block">
                    <div id="timelineChart" style="height: 300px;"></div>
                </div>
            </div>
            <!--./midia ads-->
        </section>
    </div>
    <div class="row">
        <section class="col-xl-6 connectedSortable">
            <!--Cost-per-conversion-->
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-user-plus"></i>

                    <h3 class="card-title">Cost-per-conversion (las 30 days)</h3>

                    <div class="card-tools pull-right">
                        <div class="btn-group" data-toggle="btn-toggle">
                            <button type="button" class="btn btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-block">
                    <div class="goal-progress">
                        <div class="progress-holder">
                            <p class="label-pill label-default white raised"><strong>Facebook Goal $7.80</strong></p>
                            <p class="label-pill label-default white goal"><strong>Actual $35.84</strong></p>
                            <progress class="progress progress-striped" value="80" max="100"></progress>
                        </div>
                    </div>
                    <div class="goal-progress">
                        <div class="progress-holder">
                            <p class="label-pill label-default white raised"><strong>AdWords Goal $7.80</strong></p>
                            <p class="label-pill label-default white goal"><strong>Actual $21.50</strong></p>
                            <progress class="progress progress-striped progress-danger" value="55" max="100"></progress>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.Cost-per-conversion -->
        </section>
        <section class="col-xl-6 connectedSortable">
            <!-- media followers-->
            <div class="card">
                <div class="card-header">
                    <!-- tools card -->
                    <div class="pull-right card-tools">
                        <button type="button" class="btn btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                    <!-- /. tools -->

                    <i class="fa fa-dot-circle-o"></i>

                    <h3 class="card-title">
                        Social Media Followers
                    </h3>
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-xl-2 col-xs-4 text-xs-center">
                            <i class="fa fa-facebook facebook alert fs30"></i>
                            <p class="mb5">1450</p>
                        </div>
                        <div class="col-xl-2 col-xs-4 text-xs-center">
                            <i class="fa fa-google google alert fs30"></i>
                            <p class="mb5">180</p>
                        </div>
                        <div class="col-xl-2 col-xs-4 text-xs-center">
                            <i class="fa fa-twitter twitter alert fs30"></i>
                            <p class="mb5">1020</p>
                        </div>
                        <div class="col-xl-2 col-xs-4 text-xs-center">
                            <i class="fa fa-instagram instagram alert fs30"></i>
                            <p class="mb5">800</p>
                        </div>
                        <div class="col-xl-2 col-xs-4 text-xs-center">
                            <i class="fa fa-linkedin linkedin alert fs30"></i>
                            <p class="mb5">302</p>
                        </div>
                        <div class="col-xl-2 col-xs-4 text-xs-center">
                            <i class="fa fa-foursquare foursquare alert fs30"></i>
                            <p class="mb5">211</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.media followers-->
        </section>
    </div>
    <div class="row">
        <section class="col-xl-6 col-lg-12 connectedSortable">
            <!--ROI AdWords-->
            <div class="card bg-red-gradient">
                <div class="card-header">
                    <i class="fa fa-google"></i>

                    <h3 class="card-title">AdWords ROI and CPC</h3>

                    <div class="card-tools pull-right">
                        <div class="btn-group" data-toggle="btn-toggle">
                            <button type="button" class="btn btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-block">
                    <div class="col-md-6">
                        <div class="card card-graphic">
                            <div class="card-block">
                                <div class="inner">
                                    <h4 class="mt5 mbn pull-left">ROI</h4>
                                    <h4 class="text-system pull-right pr10">33%</h4>
                                </div>

                            </div>
                            <div class="card-footer text-xs-center br-t">
                                <span>
                                    <i class="fa fa-arrow-up" style="color: palegreen;"></i>
                                    10% INCREASE
                                    <b>vs 30% (prev)</b>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-graphic">
                            <div class="card-block">
                                <div class="inner">
                                    <h4 class="mt5 mbn pull-left">CPC</h4>
                                    <h4 class="text-system pull-right pr10">$2,40</h4>
                                </div>

                            </div>
                            <div class="card-footer text-xs-center br-t">
                                <span>
                                    <i class="fa fa-arrow-down" style="color: palegreen;"></i>
                                    50% INCREASE
                                    <b>vs $1,20 (prev)</b>
                                </span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!--./ROI-->
        </section>
        <section class="col-xl-6 col-lg-12 connectedSortable">
            <!--ROI Facebook-->
            <div class="card bg-blue-gradient">
                <div class="card-header">
                    <i class="fa fa-facebook"></i>

                    <h3 class="card-title">AdWords ROI and CPC</h3>

                    <div class="card-tools pull-right">
                        <div class="btn-group" data-toggle="btn-toggle">
                            <button type="button" class="btn btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-block">
                    <div class="col-md-6">
                        <div class="card card-graphic">
                            <div class="card-block">
                                <div class="inner">
                                    <h4 class="mt5 mbn pull-left">ROI</h4>
                                    <h4 class="text-system pull-right pr10">66%</h4>
                                </div>

                            </div>
                            <div class="card-footer text-xs-center br-t">
                                <span>
                                    <i class="fa fa-arrow-up" style="color: palegreen;"></i>
                                    10% INCREASE
                                    <b>vs 60% (prev)</b>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-graphic">
                            <div class="card-block">
                                <div class="inner">
                                    <h4 class="mt5 mbn pull-left">CPC</h4>
                                    <h4 class="text-system pull-right pr10">$1,20</h4>
                                </div>

                            </div>
                            <div class="card-footer text-xs-center br-t">
                                <span>
                                    <i class="fa fa-arrow-down" style="color: palevioletred;"></i>
                                    50% DECREASE
                                    <b>vs $2,40 (prev)</b>
                                </span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!--./ROI-->

        </section>
        <!-- right col -->
    </div>
    <!-- /.row (main row) -->

@endsection

@section('page-scripts')
    <script src="{{asset('vendor/moment/js/moment.min.js')}}"></script>
    <script src="{{asset('/vendor/chart-js/Chart.min.js')}}"></script>
    <script src="{{asset('https://www.gstatic.com/charts/loader.js')}}"></script>

    <script>
        $(function () {

            "use strict";

            //Make the dashboard widgets sortable Using jquery UI
            $(".connectedSortable").sortable({
                placeholder: "sort-highlight",
                connectWith: ".connectedSortable",
                handle: ".card-header, .nav-tabs",
                forcePlaceholderSize: true,
                zIndex: 999999
            });
            $(".connectedSortable .card-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");

            google.charts.load('current', {packages: ['timeline']});
            google.charts.setOnLoadCallback(drawChart);
            //drawChart();

            //event listener to div resizing
            var chartDiv = document.getElementById("divTimeline");
            //css-element-queries library
            new ResizeSensor(chartDiv, function() {
                drawChart();
            });


            var data = {
                labels: ["01", "02", "03", "04", "05", "06", "07"],
                datasets: [
                    {

                        fillColor: "rgba(209,231,255,1)",
                        strokeColor: "rgba(97,154,234,1)",
                        pointColor: "rgba(97,154,234,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [65, 59, 120, 81, 15, 55, 40]
                    }
                ]
            };

            var lineChart = document.getElementById("lineChart1").getContext("2d");
            new Chart(lineChart).Line(data, {
                bezierCurve: false,
                showScale: false
            });

            var data = {
                labels: ["01", "02", "03", "04", "05", "06", "07"],
                datasets: [
                    {

                        fillColor: "rgba(209,231,255,1)",
                        strokeColor: "rgba(97,154,234,1)",
                        pointColor: "rgba(97,154,234,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [45, 90, 120, 15, 100, 55, 40]
                    }
                ]
            };

            var lineChart = document.getElementById("lineChart2").getContext("2d");
            new Chart(lineChart).Line(data, {
                bezierCurve: false,
                showScale: false
            });

            var data = {
                labels: ["01", "02", "03", "04", "05", "06", "07"],
                datasets: [
                    {

                        fillColor: "rgba(209,231,255,1)",
                        strokeColor: "rgba(97,154,234,1)",
                        pointColor: "rgba(97,154,234,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [32, 67, 10, 20, 90, 110, 20]
                    }
                ]
            };

            var lineChart = document.getElementById("lineChart3").getContext("2d");
            new Chart(lineChart).Line(data, {
                bezierCurve: false,
                showScale: false
            });

            var data = {
                labels: ["01", "02", "03", "04", "05", "06", "07"],
                datasets: [
                    {
                        fillColor: "rgba(209,231,255,1)",
                        strokeColor: "rgba(97,154,234,1)",
                        pointColor: "rgba(97,154,234,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [12, 32, 42, 124, 10, 65, 40]
                    }
                ]
            };

            var lineChart = document.getElementById("lineChart4").getContext("2d");
            new Chart(lineChart).Line(data, {
                bezierCurve: false,
                showScale: false
            });

            function drawChart() {

                var container = document.getElementById('timelineChart');
                var chart = new google.visualization.Timeline(container);
                var dataTable = new google.visualization.DataTable();
                dataTable.addColumn({ type: 'string', id: 'Term' });
                dataTable.addColumn({ type: 'string', id: 'Name' });
                dataTable.addColumn({ type: 'date', id: 'Start' });
                dataTable.addColumn({ type: 'date', id: 'End' });
                dataTable.addRows([
                    [ '1', 'Facebook', new Date(2016, 3, 4), new Date(2016, 3, 22) ],
                    [ '2', 'Adwords',        new Date(2016, 3, 15),  new Date(2016, 3, 25) ],
                    [ '3', 'Yahoo',  new Date(2016, 3, 1),  new Date(2016, 3, 15) ],
                    [ '4', 'Youtube', new Date(2016, 3, 1), new Date(2016, 3, 30) ],
                    [ '5', 'Wired mag.',        new Date(2016, 3, 1),  new Date(2016, 3, 30) ],
                    [ '6', 'Trivago',  new Date(2016, 3, 23),  new Date(2016, 3, 28) ]
                ]);

                var options = {
                    timeline: { showRowLabels: false }
                };

                chart.draw(dataTable, options);

            }
        });
    </script>
@endsection