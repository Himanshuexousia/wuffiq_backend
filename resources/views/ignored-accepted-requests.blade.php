@extends('app')

@section('page-title')
    User Tables
@endsection

@section('page-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap4.min.css">
@endsection
@section('content-header')
    <h1>
       Marriage Requests
    </h1>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Marriage Requests
                    <div class="pull-md-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-block">
                    <table id="example1" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                        <thead>
                        <tr class="table-active">
                            <th></th>
                            <th>Request Id</th> 
                            <th>Order Id</th> 
                            <th>Boy Id</th> 
                            <th>Girl Id</th>
                            <th>Boy Name</th>
                            <th>Girl Name</th>
                            <th>Status</th>
							<th>Payments</th>
                            <th>Action</th>
                        </tr>
                        </thead>
						<tbody>
						@if(count($da!=0))
						 @foreach($da as $data)
						<tr>
						  <td></td>
						  <td>{{$data['request_id']}}</td>
						  <td>500{{$data['request_id']}}</td>
						  <td>{{$data['user_id']}}</td>
						  <td>{{$data['requested_id']}}</td>
						  <td>{{$data['username']}}</td>
						  <td>{{$data['requestedName']}}</td>
						  <td>
							@if($data['status']==0)
								Pending
							@elseif($data['status']==1)
								Accepted	
							@elseif($data['status']==2)
								Rejected
							@endif
						  </td>
						  <td>
						  	@if($data['payment']==0)
								Pending
							@elseif($data['payment']==1)
								Accepted	
							@elseif($data['payment']==2)
								Rejected
							@endif
						  </td>
						  <td>
						  @if($data['payment']==0)
						  <div class="btn-group">
                                <button onclick="return action3({{$data['request_id']}})" type="button" class="btn btn-primary" style="background-color:#3f51b5;border-radius:10px;width:120"><i class="fa fa-rocket"></i>&nbsp;&nbsp;Cancel Request</button>
						   </div>
						   @endif
						   </td>
						</tr>
						@endforeach
						@endif
						</tbody>
                        <tfoot> 
                        <tr class="table-active">
                             <th></th> 
                            <th>Request Id</th>
							<th>Order Id</th> 							
                            <th>Boy Id</th> 
                            <th>Girl Id</th>
                            <th>Boy Name</th>
                            <th>Girl Name</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>

                    </table>
                </div>
            </div>
        </div>
    </div>

   
@endsection

@section('page-scripts')
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('/js/table-data.js')}}"></script>
    <script>
        $(function() {

            $('#example1').DataTable();

        });

function readStatus(id){
	
	$('#color_'+id).css('background-color','green');
	$('#color_'+id).html('<img src=\'{{URL("../images/message.png")}}\'>');
	$.ajax({
		url:"{{URL('API/dbReadStatus')}}",
		type:"POST",
		data:{id:id}
	}).done(function(data){
	})
	return false;
}

		
function action2(requestId){
	
	// alert(requestId);
	$.ajax({
		url:"{{URL('API/requestAction')}}",
		type:"POST",
		data:{requestId:requestId,action:2}
	}).done(function(data){
		location.reload();
		
	})
	return false;
}
	
function viewUser(id){
var url="all-users/view-id-"+id;
window.location.href="http://www.wuffiq.com/admin/all-users/view-id-"+id;
	 
}		
		
function action3(requestId)
{
	$.ajax({
		url:"{{URL('API/cancelIgnoredRequest')}}",
		type:"POST",
		data:{requestId:requestId}
	}).done(function(data){
		location.reload();
		
	})
	return false;
}

		
		
    </script>
@endsection