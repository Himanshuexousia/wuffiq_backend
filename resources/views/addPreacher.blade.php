@extends('app')

@section('page-title')
    Preacher Form
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{asset('vendor/select2/css/select2.min.css')}}">

@endsection

@section('content-header')
    <h1>
        Preacher Form
    </h1>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Add Preacher
                    <div class="pull-md-right">
                        
                    </div>
                </div>
                <div class="card-block">
                    <form method="POST" onsubmit="return saveData();">
                        <fieldset class="form-group">
                            <label for="name">* Name</label>
                            <input required type="text" value="" class="form-control" id="name" placeholder="User Name">
                        </fieldset>
						<fieldset class="form-group">
                            <label for="phoneCode">* Phone Code</label>
                            <input required value="" type="text" class="form-control" id="phoneCode" placeholder="User Phone">
                        </fieldset>	
						<fieldset class="form-group">
                            <label for="phone">* Phone</label>
                            <input required value="" type="text" class="form-control" id="phone" placeholder="User Phone">
                        </fieldset>						
                        <fieldset class="form-group">
                            <label for="email">Email</label>
                            <input  type="email" value="" class="form-control" id="email" placeholder="User Email">
                        </fieldset>

						<fieldset class="form-group">
                            <label for="marriageType">* Marriage Type</label>
                            <select required class="form-control" id="marriageType" placeholder="Another input">
							<option   value='["TT1"]'>Normal</option>
							<option  value='["TT2"]'>Msyar</option>
							<option  value='["TT3"]'>not Multi</option>
							<option  value='["TT1","TT2"]'>I don't care</option>
							</select>
                        </fieldset>
						
						<fieldset class="form-group">
                            <label for="religion">* Religion</label>
                            <select required class="form-control" id="religion" placeholder="Another input">
							<option  value="MR1">Suni</option>
							<option  value="MR2">Shiai</option>
							</select>
                        </fieldset>
						
						<fieldset class="form-group">
                            <label for="cityLive">* City Live</label>
                            <select required class="form-control" id="cityLive" placeholder="Another input">
							<option  value="Riyadh">Riyadh</option>
							<option  value="Jeddah">Jeddah</option>
							<option  value="Mecca">Mecca</option>
							<option  value="Medina">Medina</option>
							<option  value="Dammam">Dammam</option>
							<option  value="Ta'if">Ta'if</option>
							<option  value="Hofuf">Hofuf</option>
							<option  value="Khamis Mushait">Khamis Mushait</option>
							<option  value="Buraidah">Buraidah</option>
							<option  value="Khobar">Khobar</option>
							<option  value="Ha'il">Ha'il</option>
							<option  value="Hafar Al-Batin">Hafar Al-Batin</option>
							<option  value="Jubail">Jubail</option>
							<option  value="Al-Kharj">Al-Kharj</option>
							<option  value="Qatif">Qatif</option>
							<option  value="Abha">Abha</option>
							<option  value="Najran">Najran</option>
							<option  value="Yanbu">Yanbu</option>
							<option  value="Al Qunfudhah">Al Qunfudhah</option>
							<option  value="Tabuk">Tabuk</option>
							</select>
                        </fieldset>
						
						<fieldset class="form-group">
                            <label for="service">* District Service</label>
                            <select required id="service" class="form-control select2" name="service[]" multiple="multiple" data-placeholder="District Service" style="width: 100%;">
							<option value="1">Riyadh</option>
							<option value="2">Jeddah</option>
							<option value="3">Mecca</option>
							<option value="4">Medina</option>
							<option value="5">Dammam</option>
							<option value="6">Ta'if</option>
							<option value="7">Hofuf</option>
							<option value="8">Khamis Mushait</option>
							<option value="9">Buraidah</option>
							<option value="10">Khobar</option>
							<option value="11">Ha'il</option>
							<option value="12">Hafar Al-Batin</option>
							<option value="13">Jubail</option>
							<option value="14">Al-Kharj</option>
							<option value="15">Qatif</option>
							<option value="16">Abha</option>
							<option value="17">Najran</option>
							<option value="18">Yanbu</option>
							<option value="19">Al Qunfudhah</option>
							<option value="20">Tabuk</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="bankName">* Bank Name</label>
                            <select required class="form-control" id="bankName" placeholder="Another input">
							<option value="alahli">alahli</option>
							<option value="albritani">albritani</option>
							<option value="alfransi">alfransi</option>
							<option value="alholandi">alholandi</option>
							<option value="alistthmar">alistthmar</option>
							<option value="alwatani">alwatani</option>
							<option value="albilad">albilad</option>
							<option value="aljazirah">aljazirah</option>
							<option value="alriyadh">alriyadh</option>
							<option value="samba">samba</option>
							<option value="alrajhi">alrajhi</option>
							<option value="alinma">alinma</option>
							</select>
                        </fieldset>
	<input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
						<fieldset class="form-group">
                            <label for="bankAccountName">* Account Holder Name</label>
                            <input required type="text" value="" class="form-control" id="bankAccountName" placeholder="Account Holder Name">
                        </fieldset>
						
						<fieldset class="form-group">
                            <label for="bankAccount">* Bank Account</label>
                            <input required type="text" value="" class="form-control" id="bankAccount" placeholder="Bank Account">
                        </fieldset>
	<center><button type="submit" class="btn btn-success">Save</button></center>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script src="{{asset('vendor/select2/js/select2.full.min.js')}}"></script>
    <script src="{{asset('js/form-advanced.js')}}"></script>
@endsection

<script>
function saveData(){

	var name=document.getElementById('name').value;
	var phoneCode=document.getElementById('phoneCode').value;
	var phone=document.getElementById('phone').value;
	var email=document.getElementById('email').value;
	var marriageType=document.getElementById('marriageType').value;
	var religion=document.getElementById('religion').value;
	var cityLive=document.getElementById('cityLive').value;
	var service=$('#service').val();
	var bankName=document.getElementById('bankName').value;
	var bankAccountName=document.getElementById('bankAccountName').value;
	var bankAccount=document.getElementById('bankAccount').value;
	var _token=document.getElementById('_token').value;

	$.ajax({
		url:"{{URL('addDbPreacher')}}",
		type:"POST",
		data:{name:name,phoneCode:phoneCode,phone:phone,email:email,marriageType:marriageType,religion:religion,cityLive:cityLive,service:service,bankName:bankName,bankAccountName:bankAccountName,bankAccount:bankAccount,_token:_token}
	}).done(function(data){
			data=JSON.parse(data);
			if(data.status=='0'){
				alert('mobile no already exist');
			}
			else{
				window.location.href="{{URL('preacher-all')}}";
			}
	})
	return false;
}
</script>