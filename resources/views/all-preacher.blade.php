@extends('app')

@section('page-title')
    User Tables
@endsection

@section('page-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap4.min.css">
@endsection
@section('content-header')
    <h1>
       User Tables
    </h1>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Users
                    <div class="pull-md-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-block">
                    <table id="example1" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" data-page-length='100'>
                        <thead>
                        <tr class="table-active">
                            <th>Preacher name</th>
							<th>Preacher id</th>
                            <th>Preacher Code</th>
                            <th>Preacher phone</th>
                            <th>D.O.J</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr class="table-active">
                            <th>Preacher name</th>
                            <th>Preacher id</th>
                            <th>Preacher Code</th>
                            <th>Preacher phone</th>
                            <th>D.O.J</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                        <tbody>
						@foreach($preacher as $user)
                        <tr>
                            <td>{{$user->username}}</td>
                            <td>{{$user->id}}</td>
                            <td>{{$user->preacherCode}}</td>
                            <td>{{$user->phone}}</td>
                            <td>{{date('M-d-Y',$user->regDate)}}</td>
                            <td>
							@if($user->banAdmin=='0')
							Activated
							@else
							Suspended	
							@endif
							</td>
                            <td><div class="btn-group">
                                <button onclick="return viewMarriageRequests({{$user->id}})" type="button" class="btn btn-primary" style="background-color:#3f51b5;"><i class="fa fa-rocket"></i>&nbsp;&nbsp;View Requests</button>
                                <button type="button" class="btn btn-primary dropdown-toggle" style="background-color:#3f51b5;" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                             <li><a href="#" onclick="return viewUser({{$user->id}},'1')" >&nbsp;&nbsp;&nbsp;Edit Details&nbsp;&nbsp;&nbsp;</a></li>       
@if($user->banAdmin=='0')  
<li><a href="#" onclick="return action({{$user->id}},'1')" >&nbsp;&nbsp;&nbsp;Suspend&nbsp;&nbsp;&nbsp;</a></li>
@else
<li><a href="#" onclick="return action({{$user->id}},'4')" >&nbsp;&nbsp;&nbsp;Active&nbsp;&nbsp;&nbsp;</a></li>
@endif
                                    <li><a href="{{URL('deletePreacher'.$user->id)}}">&nbsp;&nbsp;&nbsp;Delete&nbsp;&nbsp;&nbsp;</a></li>
                                  
                                </ul>
                            </div></td>
                        </tr>
                       @endforeach 
                        
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

   
@endsection

@section('page-scripts')
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('/js/table-data.js')}}"></script>
    <script>
        $(function() {

            $('#example1').DataTable();

        });
function action(id,type){
	//alert(type);
	$.ajax({
		url:"{{URL('API/dbUserAction')}}",
		type:"POST",
		data:{id:id,type:type}
	}).done(function(data){
		location.reload()
		
	})
	return false;
}		
function viewUser(id){
window.location.href="{{URL('preacher-edit-')}}"+id;
}		
function viewMarriageRequests(id){
window.location.href="{{URL('preacher-requests-')}}"+id;
}
		
    </script>
@endsection