@extends('app')

@section('page-title')
    General Form
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{asset('vendor/select2/css/select2.min.css')}}">

@endsection

@section('content-header')
    <h1>
        General Form
    </h1>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Edit User
                    <div class="pull-md-right">
                 				       <?php 
						// $userId=$data['id'];
						// dd($data['userProfession']);
						?>
                    </div>
                </div>
                <div class="card-block">
                    <form method="POST" onsubmit="return updateData()">
                        <fieldset class="form-group">
                            <label for="userName">User Name</label>
                            <input type="text" value="{{$data['userName']}}" class="form-control" id="userName" placeholder="User Name">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="userEmail">User Email</label>
                            <input type="email" value="{{$data['userEmail']}}" class="form-control" id="userEmail" placeholder="User Email">
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userPhone">User Phone</label>
                            <input readonly value="{{$data['userPhone']}}" type="text" class="form-control" id="userPhone" placeholder="User Phone">
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userGender">User Gender</label>
                            <select readonly class="form-control" id="userGender" placeholder="Another input">
							
							<option @if($data['userGender']=='Male') selected @endif  value="Male">Male</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="marriedStatus">Married Status</label>
                            <select class="form-control" onchange="changeMarryReason()" id="marriedStatus" placeholder="Another input">
							<option @if($data['marriedStatus']=='MS1') selected @endif  value="MS1">Single</option>
							<option @if($data['marriedStatus']=='MS2') selected @endif  value="MS2">Married</option>
							<option @if($data['marriedStatus']=='MS3') selected @endif  value="MS3">Divorce</option>
							<option @if($data['marriedStatus']=='MS4') selected @endif  value="MS4">Widoed</option>
							</select>
                        </fieldset>
					
						<fieldset id="mReason" @if($data['marriedStatus']!='MS2') style="display:none;" @endif class="form-group">
                            <label for="marryReason">Marry Reason</label>
                            <input type="text" value="{{$data['marryReason']}}" class="form-control" id="marryReason" placeholder="Another input">
                        </fieldset>
					
					
						<fieldset class="form-group">
                            <label for="qubelahStatus">Qubelah Status</label>
                            <select onchange="qStatusChange()" class="form-control" id="qubelahStatus" placeholder="Another input">
							<option @if($data['qubelahStatus']=='MQ1') selected @endif value="MQ1">Yes</option>
							<option @if($data['qubelahStatus']=='MQ2') selected @endif value="MQ2">No</option>
							</select>
                        </fieldset>
					
						<fieldset id="qName" @if($data['qubelahStatus']!='MQ1')   @endif class="form-group">
                            <label for="qubelahName">Qubelah Name</label>
                           <select class="form-control" id="qubelahName" placeholder="Another input">
							@foreach($qubelahName as $quName)
							<option @if($data['qubelahName']==$quName->QabelahName) selected @endif value="{{$quName->QabelahName}}">{{$quName->QabelahName}}</option>
							@endforeach
							</select>
                        </fieldset>
						
						<fieldset class="form-group">
                            <label for="userAge">User Age</label>
                            <input type="number" style="width:100%" min="18" max="100" value="{{$data['userAge']}}" class="form-control" id="userAge" placeholder="Another input">
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userEducation">User Education</label>
                           <select class="form-control" id="userEducation" placeholder="Another input">
							<option @if($data['userEducation']=='ME1') selected @endif value="ME1">no Education</option>
							<option @if($data['userEducation']=='ME2') selected @endif value="ME2">Elementry & Middle</option>
							<option @if($data['userEducation']=='ME3') selected @endif value="ME3">High School</option>
							<option @if($data['userEducation']=='ME4') selected @endif  value="ME4">Bachelor</option>
							<option @if($data['userEducation']=='ME5') selected @endif value="ME5">Master</option>
							<option @if($data['userEducation']=='ME6') selected @endif value="ME6">Doctor</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userHeight">User Height</label>
                            
					<input type="number" step="0.02" style="width:100%" min="1" max="3" value="{{$data['userHeight']}}" class="form-control" id="userHeight" placeholder="Another input">
					   <!--<input type="text" value="{{$data['userHeight']}}" class="form-control" id="userHeight" placeholder="Another input"> -->
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userWeight">User Weight</label>
                            <input type="number" style="width:100%" min="40" max="300" value="{{$data['userWeight']}}" class="form-control" id="userWeight" placeholder="Another input">
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userNationality">User Nationality</label>
                            <select class="form-control" id="userNationality" placeholder="Another input">
							@foreach($country as $userCountry)
							<option @if($data['userNationality']==$userCountry->man) selected @endif value="{{$userCountry->man}}">{{$userCountry->country_name}}</option>
							@endforeach
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userCity">User City</label>
                            <select class="form-control" id="userCity" placeholder="Another input">
							@foreach($city as $userCity)
							<option @if($data['userCity']==$userCity->man) selected @endif  value="{{$userCity->man}}">{{$userCity->status}}</option>
							@endforeach
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userReligion">User Religion</label>
                            <select class="form-control" id="userReligion" placeholder="Another input">
							<option @if($data['userReligion']=='MR1') selected @endif value="MR1">Suni</option>
							<option @if($data['userReligion']=='MR2') selected @endif value="MR2">Shiai</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label  for="hairColor">Hair Color</label>
                            <select class="form-control" id="hairColor" placeholder="Another input">
							<option @if($data['hairColor']=='MH1') selected @endif value="MH1">Black</option>
							<option @if($data['hairColor']=='MH2') selected @endif value="MH2">Brown</option>
							<option @if($data['hairColor']=='MH3') selected @endif value="MH3">White</option>
							<option @if($data['hairColor']=='MH4') selected @endif value="MH4">Black and White</option>
							<option @if($data['hairColor']=='MH5') selected @endif value="MH5">Little Hair</option>
							<option @if($data['hairColor']=='MH6') selected @endif value="MH6">No Hair</option>
							<option @if($data['hairColor']=='MH7') selected @endif value="MH7">Other</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userProfession">User Profession</label>
			
                            <select class="form-control" id="userProfession" placeholder="Another input">
							<option @if($data['userProfession']=='MJ1') selected @endif value="MJ1">Company</option>
							<option @if($data['userProfession']=='MJ2') selected @endif value="MJ2">Teacher</option>
							<option @if($data['userProfession']=='MJ3') selected @endif value="MJ3">Govremant</option>
							<option @if($data['userProfession']=='MJ4') selected @endif value="MJ4">Soldier</option>
							<option @if($data['userProfession']=='MJ5') selected @endif value="MJ5">Doctor</option>
							<option @if($data['userProfession']=='MJ6') selected @endif value="MJ6">Businessman</option>
							<option @if($data['userProfession']=='MJ7') selected @endif value="MJ7">Student</option>
							<option @if($data['userProfession']=='MJ8') selected @endif value="MJ8">Retired</option>
							<option @if($data['userProfession']=='MJ9') selected @endif value="MJ9">No Job</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="childrenStatus">Children Status</label>
                            <select class="form-control" id="childrenStatus" placeholder="Another input">
							<option @if($data['childrenStatus']=='MI1') selected @endif value="MI1">No</option>
							<option @if($data['childrenStatus']=='MI2') selected @endif value="MI2">Yes but the don't live with me</option>
							<option @if($data['childrenStatus']=='MI3') selected @endif value="MI3">Yes live with me 1 children</option>
							<option @if($data['childrenStatus']=='MI4') selected @endif value="MI4">Yes live with me 2 children</option>
							<option @if($data['childrenStatus']=='MI5') selected @endif value="MI5">Yes live with me 3 children</option>
							<option @if($data['childrenStatus']=='MI6') selected @endif value="MI6">Yes live with more then 3 children</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userHobbies">Hobbies</label>
                            <select id="userHobbies" class="form-control select2" multiple="multiple" data-placeholder="Select Hobbies" style="width: 100%;">
						@php ($userHobbies=json_decode($data['userHobbies']))
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='MO1')
selected
@endif
@endforeach	
							value="MO1">Sport</option>
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='MO2')
selected
@endif
@endforeach	
							value="MO2">Traval</option>
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='MO3')
selected
@endif
@endforeach	
							value="MO3">Watch TV</option>
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='MO4')
selected
@endif
@endforeach	
							value="MO4">Internet</option>
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='MO5')
selected
@endif
@endforeach	
							value="MO5">Read book</option>
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='MO6')
selected
@endif
@endforeach	
							value="MO6">Go To resturant & Mall</option>
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='MO7')
selected
@endif
@endforeach	
							value="MO7">Go to desert</option>
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='MO8')
selected
@endif
@endforeach	
							value="MO8">Go to sea</option>
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='MO9')
selected
@endif
@endforeach	
							value="MO9">Cook</option>
							<option
@foreach($userHobbies as $hobby)
@if($hobby=='MO10')
selected
@endif
@endforeach	
							value="MO10">Alistrahah</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userIncome">Income</label>
                            <select class="form-control" id="userIncome" placeholder="Another input">
							<option @if($data['userIncome']=='MM1') selected @endif  value="MM1">Average</option>
							<option @if($data['userIncome']=='MM2') selected @endif value="MM2">Above average</option>
							<option @if($data['userIncome']=='MM3') selected @endif value="MM3">Below Averge</option>
							<option @if($data['userIncome']=='MM4') selected @endif value="MM4">Rich</option>
							<option @if($data['userIncome']=='MM5') selected @endif value="MM5">No Answer</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userReligious">Religious</label>
                            <select class="form-control" id="userReligious" placeholder="Another input">
							<option @if($data['userReligion']=='MU1') selected @endif value="MU1">Not Religious</option>
							<option @if($data['userReligion']=='MU2') selected @endif value="MU2">low Religious</option>
							<option @if($data['userReligion']=='MU3') selected @endif value="MU3">Religious</option>
							<option @if($data['userReligion']=='MU4') selected @endif value="MU4">very religious</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userTraditional">Traditional</label>
                            <select class="form-control" id="userTraditional" placeholder="Another input">
							<option  @if($data['userTraditional']=='MD1') selected @endif value="MD1">Not Traditions</option>
							<option @if($data['userTraditional']=='MD2') selected @endif value="MD2">Normal</option>
							<option @if($data['userTraditional']=='MD3') selected @endif value="MD3">Very Traditions</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userLook">Look</label>
                            <select class="form-control" id="userLook" placeholder="Another input">
							<option @if($data['lookStatus']=='ML1') selected @endif value="ML1">Normal</option>
							<option @if($data['lookStatus']=='ML2') selected @endif  value="ML2">Beautiful</option>
							<option @if($data['lookStatus']=='ML3') selected @endif  value="ML3">Very Beautiful</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userBody">Body</label>
                            <select class="form-control" id="userBody" placeholder="Another input">
							<option @if($data['bodyStatus']=='MY1') selected @endif  value="MY1">Thin</option>
							<option @if($data['bodyStatus']=='MY2') selected @endif  value="MY2">Normal</option>
							<option @if($data['bodyStatus']=='MY3') selected @endif  value="MY3">Little Big</option>
							<option @if($data['bodyStatus']=='MY4') selected @endif  value="MY4">Sport</option>
							<option @if($data['bodyStatus']=='MY5') selected @endif  value="MY5">Big</option>
							<option @if($data['bodyStatus']=='MY6') selected @endif  value="MY6">Huge</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userSmoke">Smoke</label>
                            <select class="form-control" id="userSmoke" placeholder="Another input">
							<option @if($data['smokeStatus']=='No') selected @endif  value="No">No</option>
							<option @if($data['smokeStatus']=='Yes') selected @endif  value="Yes">Yes</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="userHealth">Health</label>
                            <select onchange='uHealthChange()' class="form-control" id="userHealth" placeholder="Another input">
							<option @if($data['healthStatus']=='MF1') selected @endif value="MF1">Good</option>
							<option @if($data['healthStatus']=='MF2') selected @endif value="MF2">Little Sick</option>
							<option @if($data['healthStatus']=='MF3') selected @endif value="MF3">Disabled</option>
							</select>
                        </fieldset>
						
					
						<fieldset id="lSick" style="display:none;" class="form-group">
                            <label for="sickType">Sick Type</label>
                            <input type="text" class="form-control" id="sickType" placeholder="Another input">
                        </fieldset>
						
						<fieldset id="dType" style="display:none;" class="form-group">
                            <label for="disableType">Disable Type</label>
                            <input type="text" class="form-control" id="disableType" placeholder="Another input">
                        </fieldset>
				
						<fieldset class="form-group">
                            <label for="otherInformation">Other Information</label>
                            <input type="text" value="{{$data['otherInformation']}}" class="form-control" id="otherInformation" placeholder="Another input">
                        </fieldset>
						<fieldset class="form-group">
                            <label for="privateInformation">Private Information</label>
                            <input type="text" value="{{$data['privateInformation']}}"  class="form-control" id="privateInformation" placeholder="Another input">
                        </fieldset>
						<fieldset class="form-group">
                            <label for="preacherStatus">Preature Account</label>
                            <select class="form-control" id="preacherStatus" placeholder="Another input">
							<option @if($data['preatureStatus']=='No') selected @endif value="No">No</option>
							<option @if($data['preatureStatus']=='Yes') selected @endif value="Yes">Yes</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="preacherAccount">Preature Account</label>
                            <input type="text" value="{{$data['preatureAccount']}}" class="form-control" id="preacherAccount" placeholder="Another input">
                        </fieldset> 
						<fieldset class="form-group">
                            <label for="arabiaStatus">Fertile</label>
                            <select class="form-control" id="arabiaStatus" placeholder="Another input">
							<option @if($data['arabiaStatus']=='No') selected @endif value="No">No</option>
							<option @if($data['arabiaStatus']=='Yes') selected @endif value="Yes">Yes</option>
							</select>
                        </fieldset>
												
						<fieldset class="form-group">
                            <label for="beardStatus">Beard Status</label>
                            <select class="form-control" id="beardStatus" placeholder="Another input">
							<option @if($data['beardStatus']=='MK1') selected @endif value="MK1">Little Beard</option>
							<option @if($data['beardStatus']=='MK2') selected @endif value="MK2">Normal Beard</option>
							<option @if($data['beardStatus']=='MK3') selected @endif value="MK3">Tall Beard</option>
							<option @if($data['beardStatus']=='MK4') selected @endif value="MK4">Goatee</option>
							<option @if($data['beardStatus']=='MK5') selected @endif value="MK5">Mustache</option>
							<option @if($data['beardStatus']=='MK6') selected @endif value="MK6">No Beard</option>
							<option @if($data['beardStatus']=='MK') selected @endif value="MK">I don't care</option>
							</select>
                        </fieldset>
					
						
	<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">				
						
			
<center><button type="submit" class="btn btn-success">Udate</button></center>
		
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script src="{{asset('vendor/select2/js/select2.full.min.js')}}"></script>
    <script src="{{asset('js/form-advanced.js')}}"></script>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
var userId ="{{$data['userId']}}";
var qStatus ="{{$data['qubelahStatus']}}";
var uGender ="{{$data['userGender']}}";
var uHealth ="{{$data['healthStatus']}}";
if(uHealth=='MF2'){
$('#lSick').css('display','block');
$('#dType').css('display','none');			
}
else if(uHealth=='MF3'){
$('#dType').css('display','block');	
$('#lSick').css('display','none');	
}



if(qStatus=="MQ1"){	
// alert('ds');
$('#qName').css('display','block');	
}
function qStatusChange(){

qStatus=document.getElementById('qubelahStatus').value; 
if(qStatus=='MQ1'){

$('#qName').css('display','block');	
}
else{
$('#qName').css('display','none');
}	
}


function uHealthChange(){
	
	uHealth=document.getElementById('userHealth').value;
	if(uHealth=='MF2'){
	$('#lSick').css('display','block');
	$('#dType').css('display','none');		
	}
	else if(uHealth=='MF3'){
	$('#dType').css('display','block');	
	$('#lSick').css('display','none');	
	} 
}

function changeMarryReason(){
	var mStatus=$('#marriedStatus').val();
	if(mStatus=='MS2'){
		$('#mReason').css('display','block');
	}
	else{
		$('#mReason').css('display','none');
	}
}


function updateData(){
    var userId ="{{$data['userId']}}";
	var userName=document.getElementById('userName').value;
	var userEmail=document.getElementById('userEmail').value;
	var userGender=document.getElementById('userGender').value;
	var marriedStatus=document.getElementById('marriedStatus').value;
	if(marriedStatus=='MS2'){		
	var	marryReason=document.getElementById('marryReason').value;
	}
	else{
	var	marryReason='';	
	}
	var qubelahStatus=document.getElementById('qubelahStatus').value;
	if(qubelahStatus=='GQ1'){		
	var	qubelahName=document.getElementById('qubelahName').value;
	}
	else{
	var	qubelahName='';	
	}
	var userAge=document.getElementById('userAge').value;
	var userEducation=document.getElementById('userEducation').value;
	var userHeight=document.getElementById('userHeight').value;
	var userWeight=document.getElementById('userWeight').value;
	var userNationality=document.getElementById('userNationality').value;
	var userCity=document.getElementById('userCity').value;
	var userReligion=document.getElementById('userReligion').value;
	var hairColor=document.getElementById('hairColor').value;
	var userProfession=document.getElementById('userProfession').value;
	var childrenStatus=document.getElementById('childrenStatus').value;
	var userHobbies=$('#userHobbies').val();
	var userIncome=document.getElementById('userIncome').value;
	var userReligious=document.getElementById('userReligious').value;
	var userTraditional=document.getElementById('userTraditional').value;
	var userLook=document.getElementById('userLook').value;
	var userBody=document.getElementById('userBody').value;
	var userSmoke=document.getElementById('userSmoke').value;
	var userHealth=document.getElementById('userHealth').value;
	var otherInformation=document.getElementById('otherInformation').value;
	var privateInformation=document.getElementById('privateInformation').value;
	var preacherStatus=document.getElementById('preacherStatus').value;
	var preacherAccount=document.getElementById('preacherAccount').value;
	var arabiaStatus=document.getElementById('arabiaStatus').value;
	var beardStatus=document.getElementById('beardStatus').value; 
	var sickType='';
	var disableType='';
	if(userHealth=='GF2'){		
	 sickType=document.getElementById('sickType').value;	
	}
	else if(userHealth=='GF3'){
	 disableType=document.getElementById('disableType').value;		
	}
	
	var _token=document.getElementById('_token').value;
	
	$.ajax({
		url:"{{URL('API/updateUser')}}",
		type:"POST",
		data:{userId:userId,userName:userName,userEmail:userEmail,userGender:userGender,marriedStatus:marriedStatus,marryReason:marryReason,qubelahStatus:qubelahStatus,qubelahName:qubelahName,userAge:userAge,userEducation:userEducation,userHeight:userHeight,userWeight:userWeight,userNationality:userNationality,userCity:userCity,userReligion:userReligion,hairColor:hairColor,userProfession:userProfession,childrenStatus:childrenStatus,userHobbies:userHobbies,userIncome:userIncome,userReligious:userReligious,userTraditional:userTraditional,userLook:userLook,userBody:userBody,userSmoke:userSmoke,userHealth:userHealth,disableType:disableType,sickType:sickType,otherInformation:otherInformation,privateInformation:privateInformation,preacherStatus:preacherStatus,preacherAccount:preacherAccount,arabiaStatus:arabiaStatus,beardStatus:beardStatus,_token:_token}
	}).done(function(data){
		data=JSON.parse(data);
		if(data.status=='1'){
			alert('Update Successfully');
		}
	})
	return false;
}
</script>
