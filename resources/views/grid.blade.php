@extends('app')

@section('page-title')
    Grid System
@endsection

@section('page-css')
    <style>
        .show-grid {
            margin: 15px 0;
        }
        .show-grid [class^="col-"] {
            padding-top: 10px;
            padding-bottom: 10px;
            border: 1px solid #ddd;
            background-color: #eee !important;
        }
    </style>
@endsection
@section('content-header')
    <h1>
        Grid System
    </h1>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Grid options
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <p>See how aspects of the Bootstrap grid system work across multiple devices with a handy table.</p>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th>
                            Extra small devices
                            <small>Phones (&lt;768px)</small>
                        </th>
                        <th>
                            Small devices
                            <small>Tablets (≥768px)</small>
                        </th>
                        <th>
                            Medium devices
                            <small>Desktops (≥992px)</small>
                        </th>
                        <th>
                            Large devices
                            <small>Desktops (≥1200px)</small>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>Grid behavior</th>
                        <td>Horizontal at all times</td>
                        <td colspan="3">Collapsed to start, horizontal above breakpoints</td>
                    </tr>
                    <tr>
                        <th>Max container width</th>
                        <td>None (auto)</td>
                        <td>750px</td>
                        <td>970px</td>
                        <td>1170px</td>
                    </tr>
                    <tr>
                        <th>Class prefix</th>
                        <td>
                            <code>.col-xs-</code>
                        </td>
                        <td>
                            <code>.col-sm-</code>
                        </td>
                        <td>
                            <code>.col-md-</code>
                        </td>
                        <td>
                            <code>.col-lg-</code>
                        </td>
                    </tr>
                    <tr>
                        <th># of columns</th>
                        <td colspan="4">12</td>
                    </tr>
                    <tr>
                        <th>Max column width</th>
                        <td class="text-muted">Auto</td>
                        <td>60px</td>
                        <td>78px</td>
                        <td>95px</td>
                    </tr>
                    <tr>
                        <th>Gutter width</th>
                        <td colspan="4">30px (15px on each side of a column)</td>
                    </tr>
                    <tr>
                        <th>Nestable</th>
                        <td colspan="4">Yes</td>
                    </tr>
                    <tr>
                        <th>Offsets</th>
                        <td colspan="4">Yes</td>
                    </tr>
                    <tr>
                        <th>Column ordering</th>
                        <td colspan="4">Yes</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <p>Grid classes apply to devices with screen widths greater than or equal to the breakpoint sizes, and override grid classes targeted at smaller devices. Therefore, applying any
                <code>.col-md-</code> class to an element will not only affect its styling on medium devices but also on large devices if a
                <code>.col-lg-</code> class is not present.</p>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Stacked-to-horizontal
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <p>Using a single set of
                <code>.col-md-*</code> grid classes, you can create a default grid system that starts out stacked on mobile devices and tablet devices (the extra small to small range) before becoming horizontal on desktop (medium) devices. Place grid columns in any
                <code>.row</code>.</p>
            <div class="row show-grid">
                <div class="col-md-1">.col-md-1</div>
                <div class="col-md-1">.col-md-1</div>
                <div class="col-md-1">.col-md-1</div>
                <div class="col-md-1">.col-md-1</div>
                <div class="col-md-1">.col-md-1</div>
                <div class="col-md-1">.col-md-1</div>
                <div class="col-md-1">.col-md-1</div>
                <div class="col-md-1">.col-md-1</div>
                <div class="col-md-1">.col-md-1</div>
                <div class="col-md-1">.col-md-1</div>
                <div class="col-md-1">.col-md-1</div>
                <div class="col-md-1">.col-md-1</div>
            </div>
            <div class="row show-grid">
                <div class="col-md-8">.col-md-8</div>
                <div class="col-md-4">.col-md-4</div>
            </div>
            <div class="row show-grid">
                <div class="col-md-4">.col-md-4</div>
                <div class="col-md-4">.col-md-4</div>
                <div class="col-md-4">.col-md-4</div>
            </div>
            <div class="row show-grid">
                <div class="col-md-6">.col-md-6</div>
                <div class="col-md-6">.col-md-6</div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Responsive - Mobile, Tablet and Desktop
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <p>Don't want your columns to simply stack in smaller devices? Use the extra small, small and medium device grid classes by adding
                <code>.col-xs-*</code>
                <code>.col-sm-*</code>
                <code>.col-md-*</code> to your columns. See the example below for a better idea of how it all works.</p>
            <div class="row show-grid">
                <div class="col-xs-12 col-sm-6 col-md-8">.col-xs-12 .col-sm-6 .col-md-8</div>
                <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
            </div>
            <div class="row show-grid">
                <div class="col-xs-6 col-sm-4">.col-xs-6 .col-sm-4</div>
                <div class="col-xs-6 col-sm-4">.col-xs-6 .col-sm-4</div>
                <div class="clearfix visible-xs"></div>
                <div class="col-xs-6 col-sm-4">.col-xs-6 .col-sm-4</div>
            </div>
            <div class="row show-grid">
                <div class="col-xs-6">.col-xs-6</div>
                <div class="col-xs-6">.col-xs-6</div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Nesting columns
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <p>To nest your content with the default grid, add a new
                <code>.row</code> and set of
                <code>.col-md-*</code> columns within an existing
                <code>.col-md-*</code> column. Nested rows should include a set of columns that add up to 12.</p>
            <div class="row show-grid">
                <div class="col-md-9">
                    Level 1: .col-md-9
                    <div class="row show-grid">
                        <div class="col-md-6">
                            Level 2: .col-md-6
                        </div>
                        <div class="col-md-6">
                            Level 2: .col-md-6
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    Level 1: .col-md-3
                    <div class="row show-grid">
                        <div class="col-md-12">
                            Level 2: .col-md-12
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('page-scripts')

@endsection