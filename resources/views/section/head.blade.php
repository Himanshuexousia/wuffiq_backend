    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>تطبيق وفّق</title>
    <meta name="robots" content="index, follow">
    <meta name="keywords" content="app, admin, dashboard, laravel, php, html5, theme, template"/>
    <meta name="description" content="Infinity Admin, a fully responsive web app admin theme with charts, dashboards, landing pages and components. Fully customized with more than 140.000 layout combinations." />

    <title>Infinity Admin :: @yield('page-title', 'Your title here') </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="apple-touch-icon" sizes="57x57" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="manifest" href="{{asset('img/favicon/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{URL('../images/web/ic_launcher.png')}}">
    <meta name="theme-color" content="#ffffff">

    <link href="{{asset('vendor/tether/css/tether.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/flaticon/flaticon.css')}}" rel="stylesheet">
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('vendor/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('vendor/ionicons/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Main css -->
    <link href="{{asset('css/main.css')}}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
