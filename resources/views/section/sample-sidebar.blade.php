<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar" style="background-color:#23415f;border-right:1px solid #999999;padding-top: 85px;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- search form
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
           
			<li class="treeview {{ ( Request::is( '/')  ? ' active' : '') }}">
                <a href="{{ route('/')  }}"  style="background: #23415f !important;border-left: 0px solid transparent;">
                    <img src="{{URL('../images/speedometer.png')}}" >
                    <span style="color:#20e7ff;line-height:29px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dashboard</span>
                   
                </a> 
            </li>
		     <li class="treeview{{ (Request::is( 'all-*')  ? ' active' : '') }}">
                <a href="#" style="background: #23415f !important;border-left: 0px solid transparent;">
                     <i class="fa fa-users" style="color:#20e7ff"></i>
                    <span style="color:#20e7ff;line-height:29px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Users</span>
                    <i style="color:#20e7ff;" class="fa fa-angle-left pull-md-right"></i>   
                </a>
                <ul class="treeview-menu"  style="background: #23415f;">
                    <li {{ (Request::is( 'all-users') ? 'class="active"' : '') }}><a href="{{route('all-users')}}"><i class="fa fa-circle-o{{ (Request::is( 'all-users') ? ' text-yellow' : '') }}"></i>All Users</a></li>
                    <li {{ (Request::is( 'all-suspended-user') ? 'class="active"' : '') }}><a href="{{route('all-suspended-user')}}"><i class="fa fa-circle-o{{ (Request::is( 'all-suspended-user') ? ' text-yellow' : '') }}"></i>All Suspended Users</a></li>
                    <li {{ (Request::is( 'all-vip-user') ? 'class="active"' : '') }}><a href="{{route('all-vip-user')}}"><i class="fa fa-circle-o{{ (Request::is( 'all-vip-user') ? ' text-yellow' : '') }}"></i>All VIP Users</a></li>
                </ul>
            </li>
			
			           
		     <li class="treeview{{ (Request::is( 'all-*')  ? ' active' : '') }}">
                <a href="#" style="background: #23415f !important;border-left: 0px solid transparent;">
                     <img src="{{URL('../images/gender.png')}}" >
                    <span style="color:#20e7ff;line-height:29px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Marriage Requests</span>
                    <i style="color:#20e7ff;" class="fa fa-angle-left pull-md-right"></i>
                </a>
                <ul class="treeview-menu"  style="background: #23415f;">
                    <li {{ (Request::is( 'all-marriage-requests') ? 'class="active"' : '') }}><a href="{{URL::to('all-marriage-requests')}}"><i class="fa fa-circle-o{{ (Request::is( 'all-marriage-requests') ? ' text-yellow' : '') }}"></i>All Marriage Requests</a></li>
					<li {{ (Request::is( 'pending-marriage-requests') ? 'class="active"' : '') }}><a href="{{URL::to('pending-marriage-requests')}}"><i class="fa fa-circle-o{{ (Request::is( 'pending-marriage-requests') ? ' text-yellow' : '') }}"></i>Pending Marriage Requests</a></li>
                   <li {{ (Request::is( 'all-rejected-requests') ? 'class="active"' : '') }}><a href="{{URL::to('all-rejected-requests')}}"><i class="fa fa-circle-o{{ (Request::is( 'all-rejected-requests') ? ' text-yellow' : '') }}"></i>Rejected Marriage Requests</a></li>
				   <li {{ (Request::is( 'all-accepted-requests') ? 'class="active"' : '') }}><a href="{{URL::to('all-accepted-requests')}}"><i class="fa fa-circle-o{{ (Request::is( 'all-accepted-requests') ? ' text-yellow' : '') }}"></i>Accepted Marriage Requests</a></li>
				   <li {{ (Request::is( 'ignored-accepted-requests') ? 'class="active"' : '') }}><a href="{{URL::to('ignored-accepted-requests')}}"><i class="fa fa-circle-o{{ (Request::is( 'ignored-accepted-requests') ? ' text-yellow' : '') }}"></i>Ignored Accepted Requests</a></li>
                </ul>
            </li>
			
			
			
			
			<li class="treeview{{ (Request::is( 'preacher-*')  ? ' active' : '') }}">
                <a href="#" style="background: #23415f !important;border-left: 0px solid transparent;">
                   <img src="{{URL('../images/praying-hands.png')}}" >
                    <span style="color:#20e7ff;line-height:29px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Preacher</span>
                    <i style="color:#20e7ff;" class="fa fa-angle-left pull-md-right"></i>
                </a>
                <ul class="treeview-menu"  style="background: #23415f;">
                    <li {{ (Request::is( 'preacher-add') ? 'class="active"' : '') }}><a href="{{route('preacher-add')}}"><i class="fa fa-circle-o{{ (Request::is( 'preacher-add') ? ' text-yellow' : '') }}"></i>&nbsp;Add Preacher</a></li>
                    <li {{ (Request::is( 'preacher-all') ? 'class="active"' : '') }}><a href="{{route('preacher-all')}}"><i class="fa fa-circle-o{{ (Request::is( 'preacher-all') ? ' text-yellow' : '') }}"></i> All Preacher</a></li>
                    <li {{ (Request::is( 'preacher-suspended') ? 'class="active"' : '') }}><a href="{{route('preacher-suspended')}}"><i class="fa fa-circle-o{{ (Request::is( 'preacher-suspended') ? ' text-yellow' : '') }}"></i> Suspended Preacher</a></li>
					<li {{ (Request::is( 'request-action') ? 'class="active"' : '') }}><a href="{{route('request-action')}}"><i class="fa fa-circle-o{{ (Request::is( 'request-action') ? ' text-yellow' : '') }}"></i>Request Action</a></li> 
                    <li {{ (Request::is( 'preacher-requests') ? 'class="active"' : '') }}><a href="{{route('preacher-requests')}}"><i class="fa fa-circle-o{{ (Request::is( 'preacher-requests') ? ' text-yellow' : '') }}"></i>Preacher Requests</a></li>
					 
					<li {{ (Request::is( 'approval') ? 'class="active"' : '') }}><a href="{{route('approval')}}"><i class="fa fa-circle-o{{ (Request::is( 'approval') ? ' text-yellow' : '') }}"></i>Approval</a></li>
                    
                </ul>
            </li>
			<li class="treeview {{ ( Request::is( 'pictures')  ? ' active' : '') }}">
                <a href="{{ route('pictures')  }}"  style="background: #23415f !important;border-left: 0px solid transparent;">
                    <img src="{{URL('../images/photograph.png')}}" >
                    <span style="color:#20e7ff;line-height:29px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All Pictures</span>
                   
                </a>
            </li>
			<li class="treeview {{ ( Request::is( 'banks')  ? ' active' : '') }}">
                <a href="{{ route('banks')  }}"  style="background: #23415f !important;border-left: 0px solid transparent;">
                    <img src="{{URL('../images/credit-card.png')}}" >
                    <span style="color:#20e7ff;line-height:29px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All Banks</span>
                   
                </a>
            </li>
			<li class="treeview{{ (Request::is( 'payments-*')  ? ' active' : '') }}">
                <a href="#" style="background: #23415f !important;border-left: 0px solid transparent;">
                   <img src="{{URL('../images/credit-card.png')}}" >
                    <span style="color:#20e7ff;line-height:29px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Payments</span>
                    <i style="color:#20e7ff;" class="fa fa-angle-left pull-md-right"></i>
                </a>
                <ul class="treeview-menu"  style="background: #23415f;">
                    <li {{ (Request::is( 'preacher-add') ? 'class="active"' : '') }}><a href="{{route('payments-pending')}}"><i class="fa fa-circle-o{{ (Request::is( 'payments-pending') ? ' text-yellow' : '') }}"></i>&nbsp;Pending Payments</a></li>
                    <li {{ (Request::is( 'preacher-all') ? 'class="active"' : '') }}><a href="{{route('payments-done')}}"><i class="fa fa-circle-o{{ (Request::is( 'payments-done') ? ' text-yellow' : '') }}"></i> Accepted Payments</a>
					</li>  
                    <li {{ (Request::is( 'preacher-all') ? 'class="active"' : '') }}><a href="{{route('payments-cancel')}}"><i class="fa fa-circle-o{{ (Request::is( 'payments-cancel') ? ' text-yellow' : '') }}"></i> Rejected Payments</a>
					</li>					
                </ul>
            </li>
			<li class="treeview {{ ( Request::is( 'orders')  ? ' active' : '') }}">
                <a href="{{ route('orders')  }}"  style="background: #23415f !important;border-left: 0px solid transparent;">
                    <img src="{{URL('../images/credit-card.png')}}" >
                    <span style="color:#20e7ff;line-height:29px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All Orders</span>
                   
                </a>
            </li>
    </section>
    <!-- /.sidebar -->
</aside>
	<div id="logConfirm" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <center><h4 class="modal-title">Are You Sure want to Logout !!</h4><center>
      </div>
      <div class="modal-footer">
       <center> <button type="button" style="width:200px;" class="btn btn-warning" data-dismiss="modal">Disagree</button>
        <a href="{{URL('logout')}}"><button type="button" style="width:200px;" class="btn btn-success" >Agree</button></a></center>
      </div>
    </div>

  </div>
</div>