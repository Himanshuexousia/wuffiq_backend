<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="login-form" style="background: #FFF;padding:20px;">
	 <div class="row">
	 <div class="col-sm-5">
	 <h6><a href="" style="color:#373e4a;font-size:12px;">Privacy Policy</a>&nbsp;|&nbsp;<a target="_blank" href="{{URL('../terms.html')}}" style="color:#373e4a;font-size:12px;">Terms of Service</a></h6>
	 <br>
	 <h6 style="margin-top:-22px;color:#373e4a;font-size:12px;">Copyright ©2017</h6>
	 <br>
	 <a href="{{URL('../')}}"><h6 style="margin-top:-22px;color:#373e4a;font-size:12px;">WUFFIQ</h6></a>
	 </div>
	 
	  <div class="col-sm-7">
	  <div class="pull-right">
	  <img src="{{URL('../images/2.png')}}" width="40" height="30" />&nbsp;&nbsp;&nbsp;&nbsp;
	  <img src="{{URL('../images/3.jpg')}}" width="90" height="30" />&nbsp;&nbsp;&nbsp;&nbsp;
	 <span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=dzugI5wPCYKmNj9QP86dNnMqZADpF0v7ynpee94wVE6txdCph63sRZnFsUkF"></script></span>&nbsp;&nbsp;&nbsp;&nbsp;
	  <img src="{{URL('../images/5.png')}}" width="90" height="30" />&nbsp;&nbsp;&nbsp;&nbsp;
	  <a href="https://www.exousia.tech"><img src="{{URL('../images/exousia-tech.png')}}" width="127" height="30" /></a>
	  </div>
	  </div>
	 
	 </div>
	 </div>
</footer>