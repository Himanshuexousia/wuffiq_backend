<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{URL('')}}" style="background-color: #ffffff;border-right: 0px solid #999999;height:80px;" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><span class="symbol">&infin;</span></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg" style="color:#d95f63;font-size:34px;" dir="rtl"><img src="{{URL('../images/web/ic_launcher.png')}}"  height="80">تطبيق وفّق</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation" style="background-color: #fff;border-bottom: 1px solid #999999;
">
        <!-- Sidebar toggle button-->
        <!--<a href="#" class="sidebar-toggle" style="border-right: 0px solid #000;" data-toggle="offcanvas" role="button">
            <span style="color:black !important;" class="sr-only">Toggle navigation</span>
        </a>-->
        <!--<a href="#" class="fullscreen-toggle hidden-sm-down" style="border-right: 0px solid #000;" data-toggle="fullscreen" role="button">
            <i style="color:#666666;font-size:37px;" class="ion-arrow-expand"></i>
        </a>-->

        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                
          

                <li class="nav-item dropdown notifications-menu" onclick="readNotification()">
                    <a href="#" class="nav-link dropdown-toggle" id="area" style="background-color:#fff !important;border-left: 0px solid #000;" data-toggle="dropdown">
                        <img src="{{URL('../images/group.png')}}" style="margin-top: 36%;">
                        <span  class="label label-danger" style="position: relative;
    top: -10px;
    left: 0;
    text-align: center;
    border-radius: 50%;
    font-size: 9px;
    width: 15px;
    height: 15px;
    padding: 2px;" id="count">0</span>
                    </a>
                    <ul class="dropdown-menu" style="margin-top:15px;border:1px solid #999999">
                        <li class="header" id="countMessage">You have 0 notifications</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu" id="menu">

                            </ul>
                        </li>
                        <!--<li class="footer"><a href="#">View all</a></li>-->
                    </ul>
                </li>
 &nbsp;&nbsp;&nbsp; 
<li class="nav-item dropdown notifications-menu">
<a href="#" class="nav-link dropdown-toggle" onclick="logoutConfirm()" style="background-color:#fff !important;border-left: 0px solid #000;" >
 <img  src="{{URL('../images/logout.png')}}" style="margin-top: 54%;">
</a>
 </li>
				

				
                <!-- Control Sidebar Toggle Button -->
             
            </ul>
        </div>

    </nav>
	

	
</header><!-- /header -->

<script>
function getNotifications(){
	var x = document.getElementById("area"); 
	if(x.hasAttribute('aria-expanded')){
	if($('#area').attr('aria-expanded')=='false'){
		
	$.ajax({
		url:"{{URL('getNotifications')}}",
		type:"get"
	}).done(function(data){
		var json = JSON.parse(data);
		$('#menu').html(json.array);
		$('#count').html(json.count);
		$('#countMessage').html('You have '+json.count+' notifications');
	})

	}
	}
	else{
		
	$.ajax({
		url:"{{URL('getNotifications')}}",
		type:"get"
	}).done(function(data){
		var json = JSON.parse(data);
		$('#menu').html(json.array);
		$('#count').html(json.count);
		$('#countMessage').html('You have '+json.count+' notifications');
	})
	
	}
	// if($('#area').attr('aria-expanded')==false){
	// alert('d');

	// }
}


setInterval(function(){ getNotifications() }, 3000);

function logoutConfirm(){
	
	$('#logConfirm').modal('show');
	
}

function readNotification(){
	$.ajax({
		url:"{{URL('readNotification')}}",
		type:"get"
	}).done(function(data){
	})
}
</script>

<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>