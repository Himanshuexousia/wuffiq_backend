@extends('app')

@section('page-title')
    Pace
@endsection

@section('page-css')
    <link id="paceTheme" href="{{ asset('vendor/pace/themes/blue/pace-theme-center-simple.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .colorPace {
            padding-top: 30px;
        }
        .colorPace > .active:after, .colorPace > .active:before {
            bottom: 100%;
            left: 50%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
            -webkit-transform: rotate(180deg);
            -moz-transform: rotate(180deg);
            -o-transform: rotate(180deg);
            -ms-transform: rotate(180deg);
            transform: rotate(180deg);
        }

        .colorPace > .active:after {
            border-color: rgba(136, 183, 213, 0);
            border-bottom-color: #88b7d5;
            border-width: 10px;
            margin-left: -10px;
        }
        .colorPace > .active:before {
            border-color: rgba(194, 225, 245, 0);
            border-bottom-color: #c2e1f5;
            border-width: 16px;
            margin-left: -16px;
        }
        .typePace {
            padding-top: 30px;
        }
        .typePace > .active:after, .typePace > .active:before {
            bottom: 100%;
            left: 50%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
            -webkit-transform: rotate(180deg);
            -moz-transform: rotate(180deg);
            -o-transform: rotate(180deg);
            -ms-transform: rotate(180deg);
            transform: rotate(180deg);
        }

        .typePace > .active:after {
            border-color: rgba(136, 183, 213, 0);
            border-bottom-color: #88b7d5;
            border-width: 10px;
            margin-left: -10px;
        }
        .typePace > .active:before {
            border-color: rgba(194, 225, 245, 0);
            border-bottom-color: #c2e1f5;
            border-width: 16px;
            margin-left: -16px;
        }
    </style>
@endsection

@section('content-header')
    <h1>
        Pace
    </h1>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Pace
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-xs-12 text-md-center">
                <div class="btn-group colorPace">
                    <button type="button" data-color="0" class="btn setColor btn-sm black">
                        Black
                    </button>
                    <button type="button" data-color="1" class="btn setColor btn-sm blue active">
                        Blue
                    </button>
                    <button type="button" data-color="2" class="btn setColor btn-sm green">
                        Green
                    </button>
                    <button type="button" data-color="3" class="btn setColor btn-sm orange">
                        Orange
                    </button>
                    <button type="button" data-color="4" class="btn setColor btn-sm pink">
                        Pink
                    </button>
                    <button type="button" data-color="5" class="btn setColor btn-sm purple">
                        Purple
                    </button>
                    <button type="button" data-color="6" class="btn setColor btn-sm red">
                        Red
                    </button>
                    <button type="button" data-color="7" class="btn setColor btn-sm gray">
                        Silver
                    </button>
                    <button type="button" data-color="8" class="btn setColor btn-sm white">
                        White
                    </button>
                    <button type="button" data-color="9" class="btn setColor btn-sm yellow">
                        Yellow
                    </button>
                </div>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-xs-12 text-md-center">
                <div class="btn-group typePace">
                    <button type="button" data-type="barber" class="btn btn-secondary btn-sm setType">
                        Barber Shop
                    </button>
                    <button type="button" data-type="big" class="btn btn-secondary btn-sm setType">
                        Big Counter
                    </button>
                    <button type="button" data-type="bounce" class="btn btn-secondary btn-sm setType">
                        Bounce
                    </button>
                    <button type="button" data-type="atom" class="btn btn-secondary btn-sm setType">
                        Center Atom
                    </button>
                    <button type="button" data-type="circle" class="btn btn-secondary btn-sm setType">
                        Center Circle
                    </button>
                    <button type="button" data-type="radar" class="btn btn-secondary btn-sm setType">
                        Center Radar
                    </button>
                    <button type="button" data-type="simple" class="btn btn-secondary btn-sm setType active">
                        Center Simple
                    </button>
                    <button type="button" data-type="corner" class="btn btn-secondary btn-sm setType">
                        Corner Indicator
                    </button>
                    <button type="button" data-type="flash" class="btn btn-secondary btn-sm setType">
                        Flash
                    </button>
                    <button type="button" data-type="loading" class="btn btn-secondary btn-sm setType">
                        Loading Bar
                    </button>
                    <button type="button" data-type="mac" class="btn btn-secondary btn-sm setType">
                        Mac Osx
                    </button>
                    <button type="button" data-type="minimal" class="btn btn-secondary btn-sm setType">
                        Minimal
                    </button>
                </div>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-xs-12 text-md-center">
                    <button type="button" class="btn btn-default btn-lrg ajax" title="Ajax Request">
                        Force False Ajax / Run It
                    </button>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('page-scripts')
    <script src="{{asset('vendor/pace/pace.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ajaxStart(function() { Pace.restart(); });
        $(function () {

            "use strict";
            Pace.options = {
                ajax: true, // disabled
                document: false, // disabled
                eventLag: false, // disabled
                elements: false
            };

        });
        $('.ajax').click(function(){
            $.ajax({url: '#', success: function(result){
                setTimeout(function(){
                    toastr.info('Ajax Request Completed !');
                }, 500);
            }});
        });
        $('.setColor').click(function(){
            $( ".setColor" ).each(function() {
                if($( this).hasClass('active'))
                {
                    $( this).removeClass('active');
                }
            });
            $( this).addClass('active');
            var color = $( this)[0].dataset.color;
            paceChangeTheme(color, '');
        });
        $('.setType').click(function(){
            $( ".setType" ).each(function() {
                if($( this).hasClass('active'))
                {
                    $( this).removeClass('active');
                }
            });
            $( this).addClass('active');
            var type = $( this)[0].dataset.type;
            paceChangeTheme('', type);
        });
        function paceChangeTheme(cColor, cType)
        {
            var colors = {
                0: 'black',
                1: 'blue',
                2: 'green',
                3: 'orange',
                4: 'pink',
                5: 'purple',
                6: 'red',
                7: 'silver',
                8: 'white',
                9: 'yellow'
            };
            var types = {
                'barber': 'pace-theme-barber-shop.css',
                'big': 'pace-theme-big-counter.css',
                'bounce': 'pace-theme-bounce.css',
                'atom': 'pace-theme-center-atom.css',
                'circle': 'pace-theme-center-circle.css',
                'radar': 'pace-theme-center-radar.css',
                'simple': 'pace-theme-center-simple.css',
                'corner': 'pace-theme-corner-indicator.css',
                'fill': 'pace-theme-fill-left.css',
                'flash': 'pace-theme-flash.css',
                'flat': 'pace-theme-flat-top.css',
                'loading': 'pace-theme-loading-bar.css',
                'mac': 'pace-theme-mac-osx.css',
                'minimal': 'pace-theme-minimal.css'
            };
            var paceTheme = $(' #paceTheme ').attr('href');
            var parts = paceTheme.split('themes/');
            var url = parts[0]+'themes/';
            var parts = parts[1].split('/');
            if(cColor == '')
            {
                var color = parts[0];
            } else
            {
                var color = colors[cColor];
            }
            if(cType == '')
            {
                var type = parts[1];
            } else
            {
                var type = types[cType];
            }
            $(' #paceTheme ').attr('href', url+color+'/'+type);
            toastr.options = {
                "positionClass": "toast-bottom-right",
            };
            setTimeout(function(){
                $(' .ajax ').click();
            }, 1000);
        }
    </script>
@endsection