@extends('app')

@section('page-title')
    User Profile
@endsection

@section('page-css')

@endsection

@section('content-header')
    <h1>
        User Profile
    </h1>
@endsection

@section('content')
    <div class="jumbotron instagram p10" style="background-color:#3f51b5 !important;">
        <div class="row">
            <div class="col-md-2 text-md-center">
			
                <img class="mv10 rounded" @if($users['userImage']=='' || $users['userImage']=='No image') src="{{URL('../images/web/ic_launcher.png')}}" @else src="{{URL('../storage/app/userImage/'.$users['userImage'])}}" @endif style="height:128px;width:127px;" alt="User Image">
                <div class="btn-group mv5">
                    <label class="btn btn-secondary btn-sm btn-circle" data-toggle="tooltip" data-placement="bottom" title="{{ucfirst($users['userEmail'])}}">
                        <a onclick="sendMessage()" href="#"><span class="fa fa-envelope"></span></a>
                    </label>
					@if($users['userImage']=='' || $users['userImage']=='No image')
					 @else 
					<label class="btn btn-secondary btn-sm btn-circle" style="width:60px;" data-toggle="tooltip" data-placement="bottom" title="Delete Pic">
                        <a href="#" onclick="deletePic()">Delete Pic</span></a>
                    </label>
					@endif
					
                    <label class="btn btn-secondary btn-sm btn-circle" data-toggle="tooltip" data-placement="bottom" title="{{ucfirst($users['userCity'])}}">
                        <a href="#" ><span class="fa fa-map-marker"></span></a>
                    </label>
                    <label class="btn btn-secondary btn-sm btn-circle" data-toggle="tooltip" data-placement="bottom" title="{{$users['userPhone']}}">
                        <a href="tel:+552123456789"><span class="fa fa-phone"></span></a>
                    </label>
                </div>
            </div>
            <div class="col-md-10">
                <h2>{{ucfirst($users['userName'])}} , {{ucfirst($users['userGender'])}} , {{$users['userAge']}}
@if($users['userRegistration']==1)	<span class="pull-md-right fs18"><a style="color:white!important;" href="{{URL('all-users/edit-id-'.$users['userId'])}}">Edit</a></span><span class="pull-md-right fs18" style="padding-right: 15px;"><a style="color:white!important;" href="{{URL('all-users/man-request-id-'.$users['userId'])}}">Request</a></span>			</h2>@endif
                <blockquote class="blockquote fs14">
                    <p>@if($users['userRegistration']==1)
					{{ucfirst($users['privateInformation'])}}
					@endif</p>
                </blockquote>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" @if($users['userImage']=='' || $users['userImage']=='No image') src="{{URL('../images/web/5.png')}}" @else src="{{URL('../storage/app/userImage/'.$users['userImage'])}}" @endif style="height: 180px; width: 100%; display: block;"  alt="Card image cap">
                <div class="card-header">
                    About
                </div>
                <div class="card-block">
<strong><i class="fa fa-phone margin-r-5"></i>  Phone</strong>
                    <p class="text-muted">
					{{$users['userPhone']}}
                    </p>				
<hr>
<strong><i class="fa fa-envelope margin-r-5"></i>  Email</strong>
                    <p class="text-muted">
					{{$users['userEmail']}}
                    </p>				
<hr>				
                    <strong><i class="fa fa-book margin-r-5"></i>  Education</strong>
                    <p class="text-muted">
					{{ucfirst($users['userEducation'])}}
                    </p>

                    <hr>

                    <strong><i class="fa fa-venus-mars"></i> Married Status</strong>
                    <p class="text-muted">{{ucfirst($users['marriedStatus'])}}</p>

                    <hr>
					
					<strong><i class="fa fa-puzzle-piece"></i> Qubelah Status</strong>
                    <p class="text-muted">{{ucfirst($users['qubelahStatus'])}}</p>
					
					<hr>
					@if( ucfirst($users['qubelahStatus']=='Yes'))
					<strong><i class="fa fa-puzzle-piece"></i> Qubelah Name</strong>
                    <p class="text-muted">{{ucfirst($users['qubelahName'])}}</p>
					  <hr>
                    @endif
                  
						

					<strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                    <p class="text-muted">{{ucfirst($users['userNationality'])}} , {{ucfirst($users['userCity'])}}</p>

                    <hr>
					
					<strong><i class="fa fa-line-chart margin-r-5"></i> Height And Weight</strong>
                    <p class="text-muted">{{$users['userHeight']}} And {{$users['userWeight']}}</p>

                    <hr>
					
<strong><i class="fa fa-rebel margin-r-5"></i>Religious</strong>
@if (empty($users['userReligious']))
	<p class="text-muted"></p>
@else
<p class="text-muted">{{$users['userReligious']}}</p>
@endif
<hr>
<strong><i class="fa fa-user-secret margin-r-5"></i>Profession</strong>
<p class="text-muted">{{$users['userProfession']}}</p>
<hr>
<strong><i class="fa fa-child margin-r-5"></i>Children</strong>
<p class="text-muted">{{$users['childrenStatus']}}</p>
<hr>
<strong><i class="fa fa-trophy margin-r-5"></i>Hobbies</strong>
@if(count(($users['userHobbies'])==0))
	<p class="text-muted">
<br></p>
@else
<p class="text-muted">  @php ($hobbies=json_decode($users['userHobbies']))
@foreach($hobbies as $hobby)
@php( $ans=DB::table('codes')->where('code',$hobby)->first() )

{{$ans->answer}}

@endphp
<br>
@endforeach
</p>
@endif
<hr>
@if($users['userRegistration']==1)
<strong><i class="fa fa-money margin-r-5"></i>Income</strong>
<p class="text-muted">{{$users['userIncome']}}</p>
<hr> 
<strong><i class="fa fa-renren margin-r-5"></i>Religion</strong>
<p class="text-muted">{{$users['userReligion']}}</p>
<hr>
<strong><i class="fa fa-cube margin-r-5"></i>Traditional</strong>
<p class="text-muted">{{$users['userTraditional']}}</p>
<hr>
<strong><span class="flaticon-looking-through-binoculars"></span>&nbsp;&nbsp;Look</strong>
<p class="text-muted">{{$users['lookStatus']}}</p>
<hr>
<strong><span class="flaticon-musculous-arm-silhouette"></span>&nbsp;&nbsp;Body</strong>
<p class="text-muted">{{$users['bodyStatus']}}</p>
<hr> 
<strong><span class="flaticon-woman-standing-up"></span>&nbsp;&nbsp;Body Color</strong>
<p class="text-muted">{{$users['bodyColor']}}</p>
<hr>
<strong><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;Hair Color</strong>
<p class="text-muted">{{$users['hairColor']}}</p>
<hr>
@if($users['userGender']=='Male')
<strong><span class="flaticon-cigarette-with-smoke"></span>&nbsp;&nbsp;Smoke</strong>
<p class="text-muted">{{$users['smokeStatus']}}</p>
<hr>  
@endif            
<strong><span class="flaticon-medical-kit"></span>&nbsp;&nbsp;Health</strong>
<p class="text-muted">{{$users['healthStatus']}}</p>
<hr>
<strong><span class="flaticon-medical-drug-pill"></span>&nbsp;&nbsp;Sick Type</strong>
<p class="text-muted">{{$users['littleSickType']}}</p>
<hr>
<strong><span class="flaticon-disabled"></span>&nbsp;&nbsp;Disable Type</strong>
<p class="text-muted">{{$users['disableType']}}</p>
<hr>
<strong><span class="flaticon-information-button"></span>&nbsp;&nbsp;Other Information</strong>
<p class="text-muted">{{$users['otherInformation']}}</p>
<hr>
<strong><span class="flaticon-information-button"></span>&nbsp;&nbsp;Private Information</strong>
<p class="text-muted">{{$users['privateInformation']}}</p>
<hr>
<strong><span class="flaticon-praying-hands"></span>&nbsp;&nbsp;Preature Status</strong>
<p class="text-muted">{{$users['preatureStatus']}}</p>
<hr>
<strong><span class="flaticon-transfer"></span>&nbsp;&nbsp;Preature Account</strong>
<p class="text-muted">{{$users['preatureAccount']}}</p>
<hr>

<strong><span class="flaticon-mosque"></span>&nbsp;&nbsp;Fertile Status</strong>
@if($users['userGender']=='Male')
<p class="text-muted">{{$users['arabiaStatus']}}</p>
@else
<p class="text-muted">{{$users['smokeStatus']}}</p>
@endif
<hr>
@if($users['userGender']=='Male')
<strong><span class="flaticon-man-with-low-beard"></span>&nbsp;&nbsp;Beard Status</strong>
<p class="text-muted">{{$users['beardStatus']}}</p>
<hr>
@else
<strong><span class="flaticon-female-hair-shape-and-face-silhouette"></span>&nbsp;&nbsp;Hair Look Status</strong>
<p class="text-muted">{{$users['lookStatus']}}</p>
<hr>
<strong><span class="flaticon-woman-with-veil"></span>&nbsp;&nbsp;Veil Status</strong>
<p class="text-muted">{{$users['veilStatus']}}</p>
<hr>
<strong><span class="flaticon-woman-standing-up"></span>&nbsp;&nbsp;Virgin Status</strong>
<p class="text-muted">{{$users['virginStatus']}}</p>
<hr>
<strong><i class="fa fa-text-width"></i>&nbsp;&nbsp;SMS Number</strong>
<p class="text-muted">{{$users['smsNumber']}}</p>
@endif
@endif

                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-block">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#activity" role="tab">Activity</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#poke" role="tab">Pokes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#fav" role="tab">Favourites</a>
                         </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#marriage" role="tab">Marriage Requests</a>
                         </li>
                        
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane p10 active" id="activity" role="tabpanel">
						@foreach($requests as $requ )
                            <div class="media">
                                <a class="pull-sm-left" href="#"> <img class="media-object mn thumbnail rounded mw50" @if($users['userImage']=='' || $users['userImage']=='No image') src="{{URL('../images/web/ic_launcher.png')}}" @else src="{{URL('../storage/app/userImage/'.$users['userImage'])}}" @endif alt="..."> </a>
                                <div class="media-body">
                                    <h5 class="media-heading ml10 mb20">{{ucfirst($users['userName'])}} {{$requ['activityType']}} {{$requ['requestedName']}}
                                        <small> - {{date('M/d/Y',$requ['timestamp'])}}</small> <small> - Status @if($requ['status']==0)
										Pending
										@elseif($requ['status']==1)
										Accepted
										@elseif($requ['status']==2)
										Rejected
										@endif
										</small>
                                    </h5>
                                                                     
                                </div>
                            </div>
                          @endforeach 
                        </div>
                        <div class="tab-pane p10" id="poke" role="tabpanel">
                          @foreach($requests as $requ )
						  @if($requ['type']=='poke')
                            <div class="media">
                                <a class="pull-sm-left" href="#"> <img class="media-object mn thumbnail rounded mw50" @if($users['userImage']=='' || $users['userImage']=='No image') src="{{URL('../images/web/ic_launcher.png')}}" @else src="{{URL('../storage/app/userImage/'.$users['userImage'])}}" @endif alt="..."></a>
                                <div class="media-body">
                                    <h5 class="media-heading ml10 mb20">{{ucfirst($users['userName'])}} {{$requ['activityType']}} {{$requ['requestedName']}}
                                        <small> - {{date('M/d/Y',$requ['timestamp'])}}</small><small> - Status @if($requ['status']==0)
										Pending
										@elseif($requ['status']==1)
										Accepted
										@elseif($requ['status']==2)
										Rejected
										@endif
										</small>
                                    </h5>
                                                                     
                                </div>
                            </div>
							@endif
                          @endforeach  
                        </div>
						<div class="tab-pane p10" id="fav" role="tabpanel">
                          @foreach($requests as $requ )
						 
						  @if($requ['type']=='fav')
                            <div class="media">
                                <a class="pull-sm-left" href="#"> <img class="media-object mn thumbnail rounded mw50" @if($users['userImage']=='' || $users['userImage']=='No image') src="{{URL('../images/web/ic_launcher.png')}}" @else src="{{URL('../storage/app/userImage/'.$users['userImage'])}}" @endif alt="..."></a>
                                <div class="media-body">
                                    <h5 class="media-heading ml10 mb20">{{ucfirst($users['userName'])}} {{$requ['activityType']}} {{$requ['requestedName']}}
                                        <small> - {{date('M/d/Y',$requ['timestamp'])}}</small>
										<small> - Status @if($requ['status']==0)
										Pending
										@elseif($requ['status']==1)
										Accepted
										@elseif($requ['status']==2)
										Rejected
										@endif
										</small>
                                    </h5>
                                                                     
                                </div>
                            </div>
							@endif
                          @endforeach  
                        </div>
						<div class="tab-pane p10" id="marriage" role="tabpanel">
                          @foreach($requests as $requ )
						 
						  @if($requ['type']=='marriageRequest')
                            <div class="media">
                                <a class="pull-sm-left" href="#"> <img class="media-object mn thumbnail rounded mw50" @if($users['userImage']=='' || $users['userImage']=='No image') src="{{URL('../images/web/ic_launcher.png')}}" @else src="{{URL('../storage/app/userImage/'.$users['userImage'])}}" @endif alt="..."></a>
                                <div class="media-body">
                                    <h5 class="media-heading ml10 mb20">{{ucfirst($users['userName'])}} {{$requ['activityType']}} {{$requ['requestedName']}}
                                        <small> - {{date('M/d/Y',$requ['timestamp'])}}</small>
										<small> - Status @if($requ['status']==0)
										Pending
										@elseif($requ['status']==1)
										Accepted
										@elseif($requ['status']==2)
										Rejected
										@endif
										</small>
                                    </h5>
                                                                     
                                </div>
                            </div>
							@endif
                          @endforeach  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalContact" tabindex="-1" role="dialog" aria-labelledby="modalContact">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d235190.9986726896!2d-43.183683466015616!3d-22.918557911027275!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spt-BR!2sbr!4v1458066791334" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Send Message<span id="ajaxMsg"></span></h4>
      </div>
      <div class="modal-body">
      <textarea class="form-control" id="message" rows="5"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="sendSuccess()">Send</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $(function(){
        "use strict";
        });
		
		function sendSuccess(){
		var userId='{{$users["userId"]}}';
		alert(userId);
		var message=$('#message').val();
		if(message==''){
		$('#ajaxMsg').html('&nbsp;<p style="color:red;font-size:14px;">Field can not be left blank!!</p>');				
		}  
		else{
			alert(message);

			$.ajax({
				url:"https://www.wuffiq.com/webs/sendMessage.php",
				type:"GET"
			}).success(function(data){
				alert(data);		
			});
			
		}	
		}
		
		function sendMessage(){
			
			$('#myModal').modal('show');
			return false;
		}
		
		
		function deletePic(){
			
			var type="6";
			var id='{{$users["userId"]}}';
	$.ajax({
		url:"{{URL('API/dbUserAction')}}",
		type:"POST",
		data:{id:id,type:type}
	}).done(function(data){
		location.reload();
		
	})
	return false;
			
			
		}
		
    </script>
@endsection