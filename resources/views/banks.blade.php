@extends('app')

@section('page-title')
    User Tables
@endsection

@section('page-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap4.min.css">
@endsection
@section('content-header')
    <h1>
       Payments<a href="{{URL('banks-add')}}"><button style="background-color:#3a89c9;border-radius:10px;width:120;color:#fff" class="btn btn-primary pull-right">Add Banks</button></a>
    </h1>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Payments
                    <div class="pull-md-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-block">
                    <table id="example1" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" data-page-length='100'>
                        <thead>
                        <tr class="table-active">
                            <th></th>
                            <th>Bank Name</th>
                            <th>Name</th>
							<th>Account No</th>
                            <th>IBAN</th>
                            <th>Arabic Bank Name</th>
                            <th>Arabic Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
						<tbody>
						@if(count($json!=0))
						 @foreach($json as $data)
						<tr>
						  <td></td>
						  <td>{{$data->bankName}}</td>
						  <td>{{$data->name}}</td>
						  <td>{{$data->accountNumber}}</td>
						  <td>{{$data->IBAN}}</td>
						  <td>{{$data->arabic_bankName}}</td>
						  <td>{{$data->arabic_name}}</td>

						  <td>
								<div class="btn-group">
									<a href="{{URL('banks/edit/'.$data->id)}}"><button type="button" class="btn btn-primary" style="background-color:#3a89c9;border-radius:10px;width:120"><i class="fa fa-rocket"></i>&nbsp;&nbsp;Edit Bank</button></a>
								</div>
						   </td>
						  
						</tr>
						@endforeach
						@endif
						</tbody>
                        <tfoot>
							<tr class="table-active">
                            <th></th>
                            <th>Bank Name</th>
                            <th>Name</th>
							<th>Account No</th>
                            <th>IBAN</th>
                            <th>Arabic Bank Name</th>
                            <th>Arabic Name</th>
                            <th>Action</th>
							</tr>
                        </tfoot>

                    </table>
                </div>
            </div>
        </div>
    </div>

   
@endsection

@section('page-scripts')
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('/js/table-data.js')}}"></script>
    <script>
        $(function() {

            $('#example1').DataTable();

        });

function readStatus(id){
	
	$('#color_'+id).css('background-color','green');
	$('#color_'+id).html('<img src=\'{{URL("../images/message.png")}}\'>');
	$.ajax({
		url:"{{URL('API/dbReadStatus')}}",
		type:"POST",
		data:{id:id}
	}).done(function(data){
	})
	return false;
}

		
function action2(requestId,orderId,action){
	// alert(requestId);
	// alert(orderId);
	// alert(action);
	$.ajax({
		url:"{{URL('API/paymentAction')}}",
		type:"POST",
		data:{requestId:requestId,orderId:orderId,action:action}
	}).done(function(data){
		// alert(data);
		location.reload();
		
	})
	return false;
}
	
function viewUser(id){
var url="all-users/view-id-"+id;
window.location.href="http://www.wuffiq.com/admin/all-users/view-id-"+id;
	
}		
		
    </script>
@endsection