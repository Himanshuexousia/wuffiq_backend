@extends('app')

@section('page-title')
    All-Pictures
@endsection

@section('page-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap4.min.css">
@endsection
@section('content-header')
    <h1>
       All-Pictures
    </h1>
@endsection

@section('content')
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Users
                    <div class="pull-md-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-block">
	@foreach($images as $allImage)			
	<div class="col-sm-3">			
    <a href="{{URL('all-users/view-id-'.$allImage->id)}}"><div class="panel panel-default">
      <div class="panel-heading">Name : {{$allImage->username}}</div>
      <div class="panel-body" style="background:URL({{URL('../storage/app/userImage/'.$allImage->image)}});background-size:100% 100%;height:200px;"></div>
	  <div class="panel-footer">Email : @if($allImage->email!='')
	  {{$allImage->email}}
  @else
	  N/A
  @endif
  </div>
    </div></a>
    </div>
	@endforeach
	
                </div>
            </div>
        </div>
    </div>

   
@endsection

@section('page-scripts')
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('/js/table-data.js')}}"></script>

@endsection