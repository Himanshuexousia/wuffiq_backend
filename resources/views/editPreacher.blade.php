@extends('app')

@section('page-title')
    Preacher Form
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{asset('vendor/select2/css/select2.min.css')}}">

@endsection

@section('content-header')
    <h1>
        Preacher Form
    </h1>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Edit Preacher
                    <div class="pull-md-right">
                        
                    </div>
                </div>
                <div class="card-block">
                    <form method="POST" onsubmit="return saveData();">
                        <fieldset class="form-group">
                            <label for="name">* Name</label>
                            <input required type="text" value="{{$data->username}}" class="form-control" id="name" placeholder="User Name">
                        </fieldset>
						<fieldset class="form-group">
                            <label for="phone">* Phone</label>
                            <input readonly required value="{{$data->phone}}" type="text" class="form-control" id="phone" placeholder="User Phone">
                        </fieldset>						
                        <fieldset class="form-group">
                            <label for="email">Email</label>
                            <input  type="email" value="{{$data->email}}" class="form-control" id="email" placeholder="User Email">
                        </fieldset>

						<fieldset class="form-group">
                            <label for="marriageType">* Marriage Type</label>
                            <select required class="form-control" id="marriageType" placeholder="Another input">
							<option @if($data->marriage_type=='["TT1"]') selected @endif  value='["TT1"]'>Normal</option>
							<option @if($data->marriage_type=='["TT2"]') selected @endif  value='["TT2"]'>Msyar</option>
							<option @if($data->marriage_type=='["TT3"]') selected @endif  value='["TT3"]'>not Multi</option>
							<option @if($data->marriage_type=='["TT1","TT2"]') selected @endif  value='["TT1","TT2"]'>I don't care</option>
							</select>
                        </fieldset>
						
						<fieldset class="form-group">
                            <label for="religion">* Religion</label>
                            <select required class="form-control" id="religion" placeholder="Another input">
							<option @if($data->religion=='MR1') selected @endif    value="MR1">Suni</option>
							<option @if($data->religion=='MR2') selected @endif  value="MR2">Shiai</option>
							</select>
                        </fieldset>
						
						<fieldset class="form-group">
                            <label for="cityLive">* City Live</label>
                            <select required class="form-control" id="cityLive" placeholder="Another input">
							<option @if($data->city=='Riyadh') selected @endif   value="Riyadh">Riyadh</option>
							<option @if($data->city=='Jeddah') selected @endif   value="Jeddah">Jeddah</option>
							<option @if($data->city=='Mecca') selected @endif  value="Mecca">Mecca</option>
							<option @if($data->city=='Medina') selected @endif  value="Medina">Medina</option>
							<option @if($data->city=='Dammam') selected @endif  value="Dammam">Dammam</option>
							<option @if($data->city=='Ta\'if') selected @endif  value="Ta'if">Ta'if</option>
							<option @if($data->city=='Hofuf') selected @endif  value="Hofuf">Hofuf</option>
							<option @if($data->city=='Khamis Mushait') selected @endif  value="Khamis Mushait">Khamis Mushait</option>
							<option @if($data->city=='Buraidah') selected @endif  value="Buraidah">Buraidah</option>
							<option @if($data->city=='Khobar') selected @endif  value="Khobar">Khobar</option>
							<option @if($data->city=='Ha\'il') selected @endif  value="Ha'il">Ha'il</option>
							<option @if($data->city=='Hafar Al-Batin') selected @endif  value="Hafar Al-Batin">Hafar Al-Batin</option>
							<option @if($data->city=='Jubail') selected @endif  value="Jubail">Jubail</option>
							<option @if($data->city=='Al-Kharj') selected @endif  value="Al-Kharj">Al-Kharj</option>
							<option @if($data->city=='Qatif') selected @endif  value="Qatif">Qatif</option>
							<option @if($data->city=='Abha') selected @endif  value="Abha">Abha</option>
							<option @if($data->city=='Najran') selected @endif  value="Najran">Najran</option>
							<option @if($data->city=='Yanbu') selected @endif  value="Yanbu">Yanbu</option>
							<option @if($data->city=='Al Qunfudhah') selected @endif  value="Al Qunfudhah">Al Qunfudhah</option>
							<option @if($data->city=='Tabuk') selected @endif  value="Tabuk">Tabuk</option>
							</select>
                        </fieldset>
						
						<fieldset class="form-group">
                            <label for="service">* District Service</label>
                            <select required id="service" class="form-control select2" name="service[]" multiple="multiple" data-placeholder="District Service" style="width: 100%;">	
							@php ($service=json_decode($data->service))
							<option
@foreach($service as $ser)
@if($ser==1)
selected
@endif
@endforeach	
	
							value="1">Riyadh</option>
							<option
@foreach($service as $ser)
@if($ser==2)
selected
@endif
@endforeach	
							value="2">Jeddah</option>
							<option
@foreach($service as $ser)
@if($ser==3)
selected
@endif
@endforeach	
							value="3">Mecca</option>
							<option
@foreach($service as $ser)
@if($ser==4)
selected
@endif
@endforeach	
							value="4">Medina</option>
							<option
@foreach($service as $ser)
@if($ser==5)
selected
@endif
@endforeach	
							value="5">Dammam</option>
							<option
@foreach($service as $ser)
@if($ser==6)
selected
@endif
@endforeach	
							value="6">Ta'if</option>
							<option
@foreach($service as $ser)
@if($ser==7)
selected
@endif
@endforeach	
							value="7">Hofuf</option>
							<option
@foreach($service as $ser)
@if($ser==8)
selected
@endif
@endforeach	
							value="8">Khamis Mushait</option>
							<option
@foreach($service as $ser)
@if($ser==9)
selected
@endif
@endforeach	
							value="9">Buraidah</option>
							<option
@foreach($service as $ser)
@if($ser==10)
selected
@endif
@endforeach	
							value="10">Khobar</option>
							<option
@foreach($service as $ser)
@if($ser==11)
selected
@endif
@endforeach	
							value="11">Ha'il</option>
							<option
@foreach($service as $ser)
@if($ser==12)
selected
@endif
@endforeach	
							value="12">Hafar Al-Batin</option>
							<option
@foreach($service as $ser)
@if($ser==13)
selected
@endif
@endforeach	
							value="13">Jubail</option>
							<option
@foreach($service as $ser)
@if($ser==14)
selected
@endif
@endforeach	
							value="14">Al-Kharj</option>
							<option
@foreach($service as $ser)
@if($ser==15)
selected
@endif
@endforeach	
							value="15">Qatif</option>
							<option
@foreach($service as $ser)
@if($ser==16)
selected
@endif
@endforeach	
							value="16">Abha</option>
							<option
@foreach($service as $ser)
@if($ser==17)
selected
@endif
@endforeach	
							value="17">Najran</option>
							<option
@foreach($service as $ser)
@if($ser==18)
selected
@endif
@endforeach	
							value="18">Yanbu</option>
							<option
@foreach($service as $ser)
@if($ser==19)
selected
@endif
@endforeach	
							value="19">Al Qunfudhah</option>
							<option
@foreach($service as $ser)
@if($ser==20)
selected
@endif
@endforeach	
							value="20">Tabuk</option>
							</select>
                        </fieldset>
						<fieldset class="form-group">
                            <label for="bankName">* Bank Name</label>
                            <select required class="form-control" id="bankName" placeholder="Another input">
							<option @if($data->bank_name=='alahli') selected @endif value="alahli">alahli</option>
							<option @if($data->bank_name=='albritani') selected @endif value="albritani">albritani</option>
							<option @if($data->bank_name=='alfransi') selected @endif value="alfransi">alfransi</option>
							<option @if($data->bank_name=='alholandi') selected @endif value="alholandi">alholandi</option>
							<option @if($data->bank_name=='alistthmar') selected @endif value="alistthmar">alistthmar</option>
							<option @if($data->bank_name=='alwatani') selected @endif value="alwatani">alwatani</option>
							<option @if($data->bank_name=='albilad') selected @endif value="albilad">albilad</option>
							<option @if($data->bank_name=='aljazirah') selected @endif value="aljazirah">aljazirah</option>
							<option @if($data->bank_name=='alriyadh') selected @endif value="alriyadh">alriyadh</option>
							<option @if($data->bank_name=='samba') selected @endif value="samba">samba</option>
							<option @if($data->bank_name=='alrajhi') selected @endif value="alrajhi">alrajhi</option>
							<option @if($data->bank_name=='alinma') selected @endif value="alinma">alinma</option>
							</select>
                        </fieldset>
	<input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
						<fieldset class="form-group">
                            <label for="bankAccountName">* Account Holder Name</label>
                            <input required type="text" value="{{$data->ac_holder_name}}" class="form-control" id="bankAccountName" placeholder="Account Holder Name">
                        </fieldset>
						
						<fieldset class="form-group">
                            <label for="bankAccount">* Bank Account</label>
                            <input required type="text" value="{{$data->account_no}}" class="form-control" id="bankAccount" placeholder="Bank Account">
                        </fieldset>
	<center><button type="submit" class="btn btn-success">Save</button></center>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script src="{{asset('vendor/select2/js/select2.full.min.js')}}"></script>
    <script src="{{asset('js/form-advanced.js')}}"></script>
@endsection

<script>
function saveData(){

	var name=document.getElementById('name').value;
	var phone=document.getElementById('phone').value;
	var email=document.getElementById('email').value;
	var marriageType=document.getElementById('marriageType').value;
	var religion=document.getElementById('religion').value;
	var cityLive=document.getElementById('cityLive').value;
	var service=$('#service').val();
	var bankName=document.getElementById('bankName').value;
	var bankAccountName=document.getElementById('bankAccountName').value;
	var bankAccount=document.getElementById('bankAccount').value;
	var _token=document.getElementById('_token').value;
    var preacherId="{{$data->preacher_id}}";
	
	$.ajax({
		url:"{{URL('updateDbPreacher')}}",
		type:"POST",
		data:{name:name,phone:phone,email:email,marriageType:marriageType,religion:religion,cityLive:cityLive,service:service,bankName:bankName,bankAccountName:bankAccountName,bankAccount:bankAccount,preacherId:preacherId,_token:_token}
	}).done(function(data){
			data=JSON.parse(data);
			if(data.status=='1'){
				location.reload();
			}

	})
	return false;
}
</script>