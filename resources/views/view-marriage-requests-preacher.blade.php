@extends('app')

@section('page-title')
    User Tables
@endsection

@section('page-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap4.min.css">
@endsection
@section('content-header')
    <h1>
       Preacher Requests
    </h1>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Preacher Requests
                    <div class="pull-md-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-block">
                    <table id="example1" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" data-page-length='100'>  
                        <thead>
                        <tr class="table-active">
                            <th></th>
                            <th>Request Id</th>
                            <th>Order Id</th>
                            <th>Boy Id</th>
							<th> Boy Preacher Code</th>
                            <th>Girl Id</th>   
                            <th>Girl Preacher Code</th>							
                            <th>Time</th>
							<th>Preacher Id</th>							
                            <th>Action</th>
                        </tr>
                        </thead>
						
						<tbody>
						@if(count($select!=0))
						 @foreach($select as $data)
						<tr>
						  <td></td>
						 <?php if(DB::table('userDetailsFull')->where('user_id',$data->user_id)->first())
						 {
							 $da=DB::table('userDetailsFull')->where('user_id',$data->user_id)->first(); 
						  
						    if($da->preature_status=='Yes' || $da->preature_status=='نعم')
							{
								$n=$da->preature_account; 
							}
							else
							{
								$n='';
							
							}
						 }
							 
							  if(DB::table('userDetailsFull')->where('user_id',$data->request_id)->first())
							  {
							$l= DB::table('userDetailsFull')->where('user_id',$data->request_id)->first(); 
						     if($l->preature_status=='Yes'|| $l->preature_status=='نعم')
							 {
								$m=$l->preature_account; 
							 }
							 else
							 {
								$m='';
							 }
							
							
							  }
							  ?>
							  <?php $preach=DB::table('preacher_requests')->select('id')->where('request_id',$data->id)->first(); ?>
							  
							  
						  <td>{{$data->id}}</td>
						  <td>500{{$data->id}}</td>
						  <td>{{$data->user_id}}</td>
						  <td>{{$n}}</td>
						  <td>{{$data->request_id}}</td>
						  <td>{{$m}}</td>
						  <td>{{date('M-d-Y',$data->time)}}</td>						  
						  <td>{{$data->preachId}}</td>						  
						  <td> 
						  @if($data->astatus==0)
						  <div class="btn-group"> 
                                <button onclick="return action3({{$preach->id}},1)" type="button" class="btn btn-primary" style="background-color:#27AE60;border-radius:10px;width:120;border:none"><i class="fa fa-rocket"></i>&nbsp;&nbsp;Accept</button>
								<button onclick="return action3({{$preach->id}},2)" type="button" class="btn btn-primary" style="background-color:#E74C3C;border-radius:10px;width:120; border:none;"><i class="fa fa-rocket"></i>&nbsp;&nbsp;Reject</button>
								<button onclick="return action4({{$data->id}})" type="button" class="btn btn-primary" style="background-color:#3a89c9;border-radius:10px;width:120; border:none;"><i class="fa fa-rocket"></i>&nbsp;&nbsp;Transfer</button>
						   </div>
						   @elseif($data->astatus==1)
							Success
						  @elseif($data->astatus==2)
							Rejected
						  @endif
						   </td>
						</tr>
						@endforeach
						@endif
						</tbody>
						
                        <tfoot>
                        <tr class="table-active">
                             <th></th>
                            <th>Request Id</th>
                            <th>Order Id</th>
                            <th>Boy Id</th>
                            <th> Boy Preacher Code</th>
                            <th>Girl Id</th>   
                            <th>Girl Preacher Code</th>			                         
                            <th>Time</th>
							<th>Preacher Id</th>							
                            <th>Action</th>
                        </tr>
                        </tfoot>

                    </table>
                </div>
            </div>
        </div>
    </div>

   
@endsection

@section('page-scripts')
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('/js/table-data.js')}}"></script>
    <script>
        $(function() {

            $('#example1').DataTable();

        });

function readStatus(id){
	
	$('#color_'+id).css('background-color','green');
	$('#color_'+id).html('<img src=\'{{URL("../images/message.png")}}\'>');
	$.ajax({
		url:"{{URL('API/dbReadStatus')}}",
		type:"POST",
		data:{id:id}
	}).done(function(data){
	})
	return false;
}

		
function action3(preachId,requestId){
	
	// alert(preachId);
	// alert(requestId);
	$.ajax({
		url:"{{URL('API/actionByPreacher')}}",
		type:"POST",
		data:{id:preachId,action:requestId}
	}).done(function(data){
			location.reload();
	})
	return false;
}
	
function viewUser(id){
var url="all-users/view-id-"+id;
window.location.href="http://www.wuffiq.com/admin/all-users/view-id-"+id;
	
}
var rrr='';
function action4(reqId){
	rrr=reqId;
	$('#transfer').modal('show');
	return false;
}	

function transfer(){
	
	var preacherToTransfer=$('#preacherId').val();
	$.ajax({
		url:"{{URL('API/exchangeRequest')}}",
		type:"POST",
		data:{preacherCode:preacherToTransfer,requestId:rrr}
	}).done(function(data){
		alert('Transfer done');
		location.reload();
	})
	return false;
}	
		
    </script>
	
@endsection

<div id="transfer" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Transfer Requests</h4>
      </div>
	  <form onSubmit="return transfer()">
      <div class="modal-body">
      <input type="text" required class="form-control" id="preacherId" placeholder="Preacher Number">
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-Primary">Transfer</button>
        <button class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
	  </form>
    </div>

  </div>
</div>