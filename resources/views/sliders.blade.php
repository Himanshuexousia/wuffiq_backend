@extends('app')

@section('page-title')
    Sliders
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{url('/vendor/bootstrap-slider/css/bootstrap-slider.min.css')}}">
    <style>
        #example1 div div.slider-selection {
            background: #4d0000;
        }
        #example1 div div.slider-handle {
            background: #670000;
        }
        #example2 div div.slider-selection {
            background: #800080;
        }
        #example2 div div.slider-handle {
            background: #340034;
        }
        #RGB {
            height: 120px;
            background: rgb(128, 128, 128);
        }
        #RC .slider-selection {
            background: #FF8282;
        }
        #RC .slider-handle {
            background: red;
        }
        #GC .slider-selection {
            background: #428041;
        }
        #GC .slider-handle {
            background: green;
        }
        #BC .slider-selection {
            background: #8283FF;
        }
        #BC .slider-handle {
            border-bottom-color: blue;
        }
        #R, #G, #B {
            width: 300px;
        }
        #example5 div div.slider-selection {
            background: #ffa500;
        }
        #example5 div div.slider-handle {
            background: #b37400;
        }
    </style>
@endsection
@section('content-header')
    <h1>
        Sliders
    </h1>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Bootstrap Slider
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="row margin">
                <div class="col-sm-1">
                    <strong>Vertical</strong>
                    <p><input id="example5" type="text" class="slider form-control" data-slider-id='example5' data-slider-handle="square" data-slider-min="1" data-slider-max="20" data-slider-step="1" data-slider-value="12" data-slider-orientation="vertical" /></p>
                </div>
                <div class="col-sm-5">
                    <strong>Basic</strong>
                    <p><input id="example1" data-slider-id='example1' type="text" class="slider form-control" data-slider-handle="square" data-slider-min="0" data-slider-max="20" data-slider-step="1" data-slider-value="14" /></p>
                    <br />
                    <strong>Range</strong>
                    <p>
                        <button class="btn btn-secondary">&#36; 10</button>
                        <input id="example2" type="text" data-slider-id='example2' class="slider form-control" data-slider-handle="round" value="" data-slider-min="10" data-slider-max="1000" data-slider-step="5" data-slider-value="[250,450]" />
                        <button class="btn btn-secondary">&#36; 1000</button>
                    </p>
                    <br />
                    <strong>Current Range</strong>
                    <p>
                        <input id="example4" type="text" data-slider-id='example4' class="slider form-control" data-slider-handle="round" value="" data-slider-tooltip="hide" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="0" />
                        <button id="example4Val" class="btn btn-secondary">0</button>
                    </p>
                </div>
                <div class="col-sm-6">
                    <strong>Color Range</strong>
                    <br />
                    <div class="row">
                        <div class="col-md-6">
                            <p>
                                <b>R</b> <input type="text" class="slider form-control" value="" data-slider-min="0" data-slider-max="255" data-slider-step="1" data-slider-value="128" data-slider-id="RC" id="R" data-slider-tooltip="hide" data-slider-handle="square" />
                            </p>
                            <p>
                                <b>G</b> <input type="text" class="slider form-control" value="" data-slider-min="0" data-slider-max="255" data-slider-step="1" data-slider-value="128" data-slider-id="GC" id="G" data-slider-tooltip="hide" data-slider-handle="round" />
                            </p>
                            <p>
                                <b>B</b> <input type="text" class="slider form-control" value="" data-slider-min="0" data-slider-max="255" data-slider-step="1" data-slider-value="128" data-slider-id="BC" id="B" data-slider-tooltip="hide" data-slider-handle="triangle" />
                            </p>
                        </div>
                        <div class="col-md-6">
                            <div id="RGB"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <p>*You can customize the style according to the <a href="http://seiyria.com/bootstrap-slider/" target="_blank">Bootstrap Slider documentation</a>.</p>
    </div>

@endsection

@section('page-scripts')
    <script src="{{url('/vendor/bootstrap-slider/js/bootstrap-slider.min.js')}}"></script>
    <script>
        $(function() {
            $("input.slider").bootstrapSlider();
        });
        $("#example1").slider({
            formater: function(value) {
                return 'Current value: ' + value;
            }
        });

        /* Example 2 */
        $("#example2").slider({});

        /* Example 3 */
        var RGBChange = function() {
            $('#RGB').css('background', 'rgb('+$('#R')[0].value+','+$('#G')[0].value+','+$('#B')[0].value+')')
        };

        var r = $('#R').slider({})
                .on('slide', RGBChange)
                .data('slider');
        var g = $('#G').slider({})
                .on('slide', RGBChange)
                .data('slider');
        var b = $('#B').slider({})
                .on('slide', RGBChange)
                .data('slider');

        /* Example 4 */
        $("#example4").slider();
        $("#example4").on('slide', function(slideEvt) {
            $("#example4Val").text(slideEvt.value);
        });

        /* Example 5 */
        $("#example5").slider({
            reversed: true
        });

        /* Example 7 */
        $("#example7").slider();
        $("#example7-enabled").click(function() {
            if (this.checked) {
                $("#example7").slider("enable");
            } else {
                $("#example7").slider("disable");
            }
        });

        /* Example 8 */
        $("#example8").slider({
            tooltip: 'always'
        });

        /* Example 9 */
        $("#example9").slider({
            step: 0.01,
            value: 8.115
        });
    </script>
@endsection