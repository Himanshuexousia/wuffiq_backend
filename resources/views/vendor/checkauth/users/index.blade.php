@extends('app')
{{-- Web site Title --}}
@section('page-title')
    {{ trans_choice('checkauth::pages.users', 2) }}
@endsection

{{-- Content --}}
@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header h70">
                    <h3 class="card-title pull-left">{{ trans_choice('checkauth::pages.users', 2) }}</h3>
                    <a class='btn btn-primary pull-right' href="{{ route('checkauth.users.create') }}">{{ trans('checkauth::pages.create') }} {{ trans_choice('checkauth::pages.users', 1) }}</a>
                </div>
                <div class="card-block">

                    <table class="table table-striped table-hover">
                        <thead>
                        <th>{{ trans_choice('checkauth::pages.users', 1) }}</th>
                        <th>{{ trans('checkauth::pages.status') }}</th>
                        <th>{{ trans('checkauth::pages.options') }}</th>
                        </thead>
                        <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td><a href="{{ route('checkauth.users.show', array($user->hash)) }}">{{ $user->first_name }} {{ $user->last_name }} ({{ $user->email }})</a></td>
                                <td><span class="label @if (Activation::completed($user)) label-success @else label-default @endif">@if (Activation::completed($user)) {{ trans('checkauth::pages.active') }} @else {{ trans('checkauth::pages.notactive') }} @endif</span> </td>
                                <td>
                                    <button class="btn btn-secondary" type="button" onClick="location.href='{{ route('checkauth.users.show', array($user->hash)) }}'"><i class="fa fa-eye" alt="Show" title="Show"></i></button>
                                    <button class="btn btn-secondary" type="button" onClick="location.href='{{ route('checkauth.users.edit', array($user->hash)) }}'"><i class="fa fa-edit" alt="Edit" title="Edit"></i></button>
                                    @if (Activation::completed($user))
                                        <button class="btn btn-secondary" type="button" onClick="location.href='{{ route('checkauth.users.deactivate', array($user->hash)) }}'"><i class="fa fa-toggle-off" alt="Show" title="Show"></i></button>
                                    @else
                                        <button class="btn btn-success" type="button" onClick="location.href='{{ route('checkauth.users.activate', array($user->hash)) }}'"><i class="fa fa-toggle-on" alt="Show" title="Show"></i></button>
                                    @endif
                                    <button class="btn action_confirm btn-danger" href="{{ route('checkauth.users.destroy', array($user->hash)) }}" data-token="{{ Session::getToken() }}" data-method="delete"><i class="fa fa-trash fa" alt="Delete" title="Delete"></i></button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <div class='pull-right'>
                        {!! $users->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


@section('page-scripts')
    <script src="{{ asset('vendor/checkauth/js/restfulizer.js') }}"></script>
@endsection
@stop
