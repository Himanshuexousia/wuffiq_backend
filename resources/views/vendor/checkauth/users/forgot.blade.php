@extends('checkauth::sessions.logoff')
{{-- Web site Title --}}
@section('page-title')
    Forgot my Password
@endsection

{{-- Content --}}
@section('checkauth:grid-class')
    @parent
@show

@section('checkauth:card-title')
Provide your registration e-mail so we can send you a reset password link
@stop

@section('checkauth::card-block')
    <form method="POST" action="{{ route('checkauth.reset.request') }}" accept-charset="UTF-8">
            @include('checkauth::partials.notifications')
            <div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
                <input class="form-control" placeholder="E-mail" autofocus="autofocus" name="email" type="text" value="{{ Input::old('name') }}">
                {{ ($errors->has('email') ? $errors->first('email') : '') }}
            </div>
            <div class="form-group">
                {!! app('captcha')->display() !!}
                {{ ($errors->has('g-recaptcha-response') ? $errors->first('g-recaptcha-response') : '') }}
            </div>
            <div class="form-group">
                <div class='btn-group'>
                    <input class="btn btn-primary" value="Send Instructions" type="submit">
                    <input name="_token" value="{{ csrf_token() }}" type="hidden">
                </div>
            </div>
    </form>
@stop

@section('checkauth::card-footer')
@stop