<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="utf-8">
</head>
<body style="background-color: rgb(48, 48, 48);" bgcolor="#303030">
<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
	<tbody><tr>
		<td align="center" style="-webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; background-position: center center; background-repeat: no-repeat;">
			<div>
				<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
					<tbody><tr>
						<td width="100%" align="center">

							<div class="sortable_inner ui-sortable">

								<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" >
									<tbody><tr>
										<td width="352" height="80"></td>
									</tr>
									</tbody></table>

								<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" >
									<tbody><tr>
										<td width="352" valign="middle" align="center">

											<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
												<tbody><tr>
													<td width="352" valign="middle" object="text-editable" style="text-align: center; font-family: Helvetica,Arial,sans-serif; font-size: 67px; color: rgb(255, 255, 255); line-height: 72px; font-weight: 100; word-break: break-all;">
															<span style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: normal;">
																 Hello!
															</span>
													</td>
												</tr>
												</tbody></table>
										</td>
									</tr>
									</tbody></table>

								<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" >
									<tbody><tr>
										<td width="352" valign="middle" align="center">

											<table width="265" cellspacing="0" cellpadding="0" border="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">

												<tbody><tr>
													<td width="100%" height="25"></td>
												</tr>
												</tbody></table>
										</td>
									</tr>
									</tbody></table>

								<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" >
									<tbody><tr>
										<td width="352" valign="middle" align="center">

											<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
												<tbody><tr>
													<td width="100%" valign="middle" object="text-editable" style="text-align: center; font-family: Helvetica,Arial,sans-serif; font-size: 19px; color: rgb(255, 255, 255); line-height: 26px; font-weight: bold; text-transform: uppercase;">
															<span style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: normal;">
																 Password Reset requested for
															</span>
													</td>
												</tr>
												<tr>
													<td width="100%" height="50"></td>
												</tr>
												</tbody></table>
										</td>
									</tr>
									</tbody></table>

							</div>
						</td>
					</tr>
					</tbody></table>

				<table width="392" cellspacing="0" cellpadding="0" border="0" align="center" >
					<tbody><tr>
						<td width="20" valign="middle" align="center"></td>
						<td width="352" valign="middle" align="center">

							<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" >
								<tbody><tr>
									<td width="352" valign="middle" bgcolor="#f56954" align="center" style="background-color: rgb(245, 105, 84);">

										<div class="sortable_inner ui-sortable">

											<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" >
												<tbody><tr>
													<td width="352" valign="middle" align="center">

														<table width="300" cellspacing="0" cellpadding="0" border="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
															<tbody><tr>
																<td width="100%" height="30"></td>
															</tr>
															</tbody></table>
													</td>
												</tr>
												</tbody></table>

											<!-- Avatar -->
											<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" >
												<tbody><tr>
													<td width="352" valign="middle" align="center" class="avatar125">

														<table width="265" cellspacing="0" cellpadding="0" border="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
															<tbody><tr>
																<td width="100%"><span object="image-editable"><img width="80" border="0" alt="" src="{{ asset('vendor/checkauth/img/key_icon.png')  }}" editable="true"></span></td>
															</tr>
															</tbody></table>
													</td>
												</tr>
												</tbody></table>

											<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" >
												<tbody><tr>
													<td width="352" valign="middle" align="center">
														<table width="265" cellspacing="0" cellpadding="0" border="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
															<tbody><tr>
																<td width="100%" height="30"></td>
															</tr>
															</tbody></table>
													</td>
												</tr>
												</tbody></table>

											<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" >
												<tbody><tr>
													<td width="352" valign="middle" align="center">

														<table width="265" cellspacing="0" cellpadding="0" border="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">

															<tbody><tr>
																<td width="100%" valign="middle" object="text-editable" style="text-align: center; font-family: Helvetica,Arial,sans-serif; font-size: 34px; color: rgb(255, 255, 255); line-height: 44px; font-weight: bold;">
																	<span style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;; font-weight: normal;"> {{ $first_name }} {{ $last_name }}  </span>
																</td>
															</tr>
															</tbody></table>
													</td>
												</tr>
												</tbody></table>

											<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" >
												<tbody><tr>
													<td width="352" valign="middle" align="center">

														<table width="265" cellspacing="0" cellpadding="0" border="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
															<tbody><tr>
																<td width="100%" height="30"></td>
															</tr>
															</tbody></table>
													</td>
												</tr>
												</tbody></table>

											<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" >
												<tbody><tr>
													<td width="352" valign="middle" align="center">

														<table width="265" cellspacing="0" cellpadding="0" border="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
															<tbody><tr>
																<td width="100%" valign="middle" object="text-editable" style="text-align: center; font-family: Helvetica,Arial,sans-serif; font-size: 14px; color: rgb(255, 255, 255); line-height: 24px;">
																	<span style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: normal;">To reset your password, click at the button bellow.</span>
																</td>
															</tr>
															</tbody></table>
													</td>
												</tr>
												</tbody></table>

											<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" >
												<tbody><tr>
													<td width="352" valign="middle" align="center">

														<table width="265" cellspacing="0" cellpadding="0" border="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
															<tbody><tr>
																<td width="100%" height="40"></td>
															</tr>
															</tbody></table>
													</td>
												</tr>
												</tbody></table>

											<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" >
												<tbody><tr>
													<td width="352" valign="middle" align="center">

														<table width="300" cellspacing="0" cellpadding="0" border="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">

															<tbody><tr>
																<td align="center">
																	<table cellspacing="0" cellpadding="0" border="0" align="center">
																		<tbody><tr>
																			<td bgcolor="#ffffff" height="45" align="center" style="border-radius: 5px; padding-left: 30px; padding-right: 30px; font-weight: bold; font-family: Helvetica,Arial,sans-serif; color: rgb(56, 56, 56); text-transform: uppercase; background-color: rgb(255, 255, 255);" c-style="not4ButButton">
																				<multiline> <span style=" font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: normal;">
																			<a style="color: rgb(56, 56, 56); font-size: 15px; text-decoration: none; line-height: 35px; width: 100%;" href="{{ route('checkauth.reset.form', [$hash, urlencode($code)]) }}">Reset my password</a>
																		 </span></multiline>
																			</td>
																		</tr>
																		</tbody></table>
																</td>
															</tr>

															</tbody></table>
													</td>
												</tr>
												</tbody></table>

											<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" >
												<tbody><tr>
													<td width="352" valign="middle">

														<table width="265" cellspacing="0" cellpadding="0" border="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
															<tbody><tr>
																<td width="100%" height="40"></td>
															</tr>
															</tbody></table>
													</td>
												</tr>
												</tbody></table>

											<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" >
												<tbody><tr>
													<td width="352" valign="middle" align="center">

														<table width="265" cellspacing="0" cellpadding="0" border="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
															<tbody><tr>
																<td width="100%" valign="middle" object="text-editable" style="text-align: center; font-family: Helvetica,Arial,sans-serif; font-size: 15px; color: rgb(255, 255, 255); line-height: 24px;">
																	<span style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: normal;">Or point your browser to this address: <br /> {{ route('checkauth.reset.form', [$hash, urlencode($code)]) }}</span>
																</td>
															</tr>
															</tbody></table>
													</td>
												</tr>
												</tbody></table>

											<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" >
												<tbody><tr>
													<td width="352" valign="middle">

														<table width="265" cellspacing="0" cellpadding="0" border="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
															<tbody><tr>
																<td width="100%" height="50"></td>
															</tr>
															</tbody></table>

													</td>
												</tr>
												</tbody></table>

										</div>
									</td>
								</tr>
								</tbody></table>

						</td>
						<td width="20" valign="middle" align="center"></td>
					</tr>
					</tbody></table>

				<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" class="mobile2">
					<tbody><tr>
						<td width="100%" align="center">

							<div class="sortable_inner ui-sortable">

								<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" >
									<tbody><tr>
										<td width="352" height="40"></td>
									</tr>
									</tbody></table>

								<table width="352" cellspacing="0" cellpadding="0" border="0" align="center" >
									<tbody><tr>
										<td width="352" height="60"></td>
									</tr>
									<tr>
										<td width="352" height="1" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
									</tr>
									</tbody></table>
							</div>

						</td>
					</tr>
					</tbody></table>

			</div>
		</td>
	</tr>
	</tbody></table>

</body>
</html>