<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>تطبيق وفّق - Login</title>
    <meta name="robots" content="index, follow">
    <meta name="keywords" content="app, admin, dashboard, laravel, php, html5, theme, template"/>
    <meta name="description" content="Infinity Admin, a fully responsive web app admin theme with charts, dashboards, landing pages and components. Fully customized with more than 140.000 layout combinations." />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="{{asset('vendor/tether/css/tether.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('vendor/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('vendor/ionicons/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/icheck/skins/flat/blue.css')}}" />
    <!-- Main css -->
	 <link rel="apple-touch-icon" sizes="57x57" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{URL('../images/web/ic_launcher.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{URL('../images/web/ic_launcher.png')}}">
    <link href="{{asset('css/main.css')}}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        html, body {
            overflow: hidden;
        }
        body {
            min-height: 1200px;
            height: 100%;
        }
        .login-page {
            background: #474747;
        }
        .login-box {
            position: absolute;
            top: 50px;
            left: 50%;
            margin-left: -180px;
        }
        .login-form .card-info {
            border-top: 6px solid #003872;
        }
    </style>
</head>
<body class="content-light login-page">
<div class="login-box white br12 p12">
    <div class="text-sm-center">
        <a href="{{URL('../')}}" class="logo">
            <img src="{{URL('../images/web/ic_launcher.png')}}" height="80px">
        </a>
    </div>
    <div class="login-box-body br24">
        <p class="login-box-msg" id="msgs">Sign in</p>

        <form method="POST" onsubmit="return userLogin()" accept-charset="UTF-8" class="form-signin">

            <div class="form-group">
                <input type="email" class="form-control" required placeholder="Email" id="email" autofocus="autofocus" type="text"   name="email">

            </div>
			<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
            <div class="form-group has-feedback">
                <input class="form-control" placeholder="Password" required type="password" id="password"  name="password">

            </div>
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block" id="sub"  style="background-color:#3f51b5;">{{ trans('checkauth::pages.login') }}</button>
                    
                </div>
                <!-- /.col -->
            </div>

        </form>


    </div>
</div>

<script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('vendor/tether/js/tether.min.js')}}"></script>
<script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('vendor/icheck/icheck.min.js') }}"></script>
<script src="{{asset('js/jquery.particleground.min.js')}}"></script>
<script>
    $(function () {
        $('.content-light').particleground({
            dotColor: '#6d6d6d',
            lineColor: '#6d6d6d'
        });
        $('input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });
    });
	var email='';
	var password='';
	var _token='';
	function userLogin(){
		$('#sub').attr('disabled',true);
		$('#sub').html('<i style="font-size:18px;" class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>');
		 email=$('#email').val();
		 password=$('#password').val();
		 _token=$('#_token').val();
		
		$.ajax({
			url:"{{URL('dbAdminLogin')}}",
			method:"POST",
			data:{email:email,password:password,_token:_token}
		}).done(function(data){
			data=JSON.parse(data);
			// alert(data.status);
			if(data.status==1){
				location.reload();
			}
			else if(data.status==2){
				
				$('#msgs').html('<span style="color:red">Email Not Exist</span>');
				$('#sub').html("{{ trans('checkauth::pages.login') }}");
				$('#sub').removeAttr('disabled');
				
			}
			else if(data.status==3){
				
				$('#msgs').html('<span style="color:red">Email & Password Not Match</span>');
				$('#sub').html("{{ trans('checkauth::pages.login') }}");
				$('#sub').removeAttr('disabled');
				
			}
		})
		return false;
	}
</script>
</body>
</html>
