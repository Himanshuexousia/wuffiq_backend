<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>
        @section('checkauth::page-title')
            Logoff from app
        @show
    </title>

    @include('checkauth::partials.styles')

</head>

<body>

<div class="container">

    <div class="row">
        <div class="brand">
            <h2><a href="{{ route('checkauth.app') }}">CheckAuth</a></h2>
        </div>
    </div>
    <div class="row">
        @include('checkauth::partials.notifications')
    </div>
    <div class="row">
        <div class="@section('checkauth:grid-class') col-md-offset-2 col-md-8 col-md-offset-2 @show">
            <div class="card">
                <div class="card-header">@yield('checkauth:card-title', 'your box title')</div>
                <div class="card-block">
                    @yield('checkauth::card-block', 'your box content')
                </div>
                <div class="card-footer">
                    @yield('checkauth::card-footer', 'your box content')
                </div>
            </div>
        </div>
    </div>

</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
@include('checkauth::partials.scripts')
@yield('checkauth::scripts')
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="{{ asset('vendor/bootstrap/js/ie10-viewport-bug-workaround.js') }}"></script>
</body>
</html>