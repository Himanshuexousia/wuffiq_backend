@extends('app')

@section('page-title')
    User Tables
@endsection

@section('page-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap4.min.css">
@endsection
@section('content-header')
    <h1>
       Payments
    </h1>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Payments
                    <div class="pull-md-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-block">
                    <table id="example1" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                        <thead>
                        <tr class="table-active">
                            <th></th>
                            <th>Request Id</th>
                            <th>Order Id</th>
                            <th>Bank Name</th>
                            <th>Pay By</th>
                            <th>Boy Id</th>
							<th>Girl Id</th>
                            <th>Customer Name</th>
                            <th>Account No</th>
                            <th>Amount</th>
                            <th>Optional</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
						<tbody>
						@if(count($json!=0))
						 @foreach($json as $data)
						<tr>
						  <td></td>
						  <td>{{$data['request_id']}}</td>
						  <td>500{{$data['request_id']}}</td>
						  <td>{{$data['bankName']}}</td>
						  <td>
						  @if($data['pay_by']==0)
								Today
						  @else	
								Tomarrow
						  @endif
						  </td>
						  <td>{{$data['boyId']}}</td>
						  <td>{{$data['girlId']}}</td>
						  <td>{{$data['customer_name']}}</td>
						  <td>{{$data['account_no']}}</td>
						  <td>{{$data['amount']}}</td>
						  <td>{{$data['optional']}}</td>
						  <td>{{date('m-d-y',$data['created_at'])}}</td>
						 
						  <td>
						   @if($data['status']==0)
							<div class="btn-group">
                                <button onclick="return action2({{$data['request_id']}},{{$data['order_id']}},'2')" type="button" class="btn btn-danger" style="background-color:#c13321;border-radius:10px;">&nbsp;&nbsp;Cancel Request</button>
								<button onclick="return action2({{$data['request_id']}},{{$data['order_id']}},1)" type="button" class="btn btn-primary" style="background-color:#3f51b5;border-radius:10px;">&nbsp;&nbsp;Accept Request</button>
						   </div>
						  @elseif($data['status']==1) 
							  Success
						  @elseif($data['status']==2)
							  Cancelled
						  @endif
						  </td>
						</tr>
						@endforeach
						@endif
						</tbody>
                        <tfoot>
							<tr class="table-active">
								<th></th>
								<th>Request Id</th>
								<th>Order Id</th>
								<th>Bank Name</th>
								<th>Pay By</th>
								<th>Boy Id</th>
								<th>Girl Id</th>
								<th>Customer Name</th>
								<th>Account No</th>
								<th>Amount</th>
								<th>Optional</th>
								<th>Date</th>
								<th>Action</th>
							</tr>
                        </tfoot>

                    </table>
                </div>
            </div>
        </div>
    </div>

   
@endsection

@section('page-scripts')
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('/js/table-data.js')}}"></script>
    <script>
        $(function() {

            $('#example1').DataTable();

        });

function readStatus(id){
	
	$('#color_'+id).css('background-color','green');
	$('#color_'+id).html('<img src=\'{{URL("../images/message.png")}}\'>');
	$.ajax({
		url:"{{URL('API/dbReadStatus')}}",
		type:"POST",
		data:{id:id}
	}).done(function(data){
	})
	return false;
}

		
function action2(requestId,orderId,action){
	// alert(requestId);
	// alert(orderId);
	// alert(action);
	$.ajax({
		url:"{{URL('API/paymentAction')}}",
		type:"POST",
		data:{requestId:requestId,orderId:orderId,action:action}
	}).done(function(data){
		// alert(data);
		location.reload();
		
	})
	return false;
}
	
function viewUser(id){
var url="all-users/view-id-"+id;
window.location.href="http://www.wuffiq.com/admin/all-users/view-id-"+id;
	
}		
		
    </script>
@endsection