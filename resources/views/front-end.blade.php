<?php $develop = true; ?>

        <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="index, follow">
    <meta name="keywords" content="app, admin, dashboard, laravel, php, html5, theme, template"/>
    <meta name="description" content="Infinity Admin, a fully responsive web app admin theme with charts, dashboards, landing pages and components. Fully customized with more than 140.000 layout combinations." />

    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/img/favicon//ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <title>Infinity Premium Admin Theme</title>

    @if ($develop)

            <!--Normalize-->
    <link rel="stylesheet" href="{{asset('vendor/normalize-css/normalize.css')}}">

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" type="text/css">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="{{asset('vendor/animate.css/animate.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('vendor/superslides/stylesheets/superslides.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/magnific-popup/magnific-popup.css')}}" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('css/front-end.css')}}" type="text/css">

    @else

        <link rel="stylesheet" href="{{asset('dist/main-front.css')}}" type="text/css">

        @endif

                <!-- Custom Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>
<section id="home"></section><!--needed only to highlighting the link-->
<body>
<div id="mask">
    <div class="loader">
        <img src="{{asset('img/front-end/loading.gif')}}" alt='loading'>
    </div>
</div>
<div id="scroll-animate">
    <div id="scroll-animate-main">
        <div class="wrapper-parallax">
            <nav id="mainNav" class="navbar navbar-default navbar-fixed-top sps sps--abv">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand page-scroll" href="#home"><div class="brand-img"></div> </a>
                        <button class="navbar-toggler hidden-md-up pull-right" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>
                    <div class="collapse navbar-toggleable-sm" id="collapsingNavbar">
                        <ul class="nav navbar-nav pull-md-right">
                            <li class="nav-item">
                                <a class="nav-link page-scroll" href="#home">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link page-scroll" href="#app4">App4</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" href="#" aria-haspopup="true" aria-expanded="false">Features</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item features-scroll" href="#feature1">Feature 1</a>
                                    <a class="dropdown-item features-scroll" href="#feature2">Feature 2</a>
                                    <a class="dropdown-item features-scroll" href="#feature3">Feature 3</a>
                                    <a class="dropdown-item features-scroll" href="#feature4">Feature 4</a>
                                    <a class="dropdown-item features-scroll" href="#feature5">Feature 5</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link page-scroll" href="#gallery" data-src-base='{{asset('img/front-end/slider/')}}/' data-src='<768:01-small.jpg,<992:01-medium.jpg,<1199:01-large.jpg,>1200:01.jpg'>Gallery</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link page-scroll" href="#differentials" data-src-base='{{asset('img/front-end/slider/')}}/' data-src='<768:02-small.jpg,<992:02-medium.jpg,<1199:02-large.jpg,>1200:02.jpg'>Differentials</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link page-scroll" href="#about" data-src-base='{{asset('img/front-end/slider/')}}/' data-src='<768:03-small.jpg,<992:03-medium.jpg,<1199:03-large.jpg,>1200:03.jpg'>About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link page-scroll" href="#contact">Contact</a>
                            </li>
                        </ul>

                    </div>
                </div>
            </nav>

            <header>
                <div class="slider-top-content">
                    <img src="{{asset('img/front-end/app4_logo_white.png')}}">
                    <hr>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,<br> sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                    <a href="#app4" class="btn btn-primary btn-xl page-scroll">More</a>
                </div>
                <div id="slider">
                    <div class="overlay"></div>
                    <ul class="slides-container">
                        <li>
                            <img src="{{asset('img/front-end/slider/01.jpg')}}" alt="">
                        </li>
                        <li>
                            <img src="{{asset('img/front-end/slider/02.jpg')}}" alt="">
                        </li>
                    </ul>
                    <nav class="slides-navigation">
                        <a href="#" class="next"></a>
                        <a href="#" class="prev"></a>
                    </nav>
                </div>
            </header>
            <div id="content">
                <section class="bg-app4" id="app4">
                    <div class="container-fluid pdtop infinity-content">
                        <div class="row animate" data-animation="fadeInUp">
                            <div class="col-lg-8 col-lg-offset-2 text-xs-center">
                                <h2 class="section-heading">We are App4, a App develop start up</h2>
                                <hr class="light">
                                <p class="text-faded">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,<br> sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
                                </p>
                                <a href="#feature1" class="btn btn-secondary btn-xl page-scroll">Features</a>
                            </div>
                        </div>
                        <div class="parallax-content">
                            <div class="row">
                                <div class="col-lg-2 col-xs-4 text-xs-center">
                                    <img src="{{asset('img/front-end/logos/bootstrap-logo-white.png')}}" class="animate" data-animation="fadeIn" data-delay=".5">
                                </div>
                                <div class="col-lg-2 col-xs-4 text-xs-center">
                                    <img src="{{asset('img/front-end/logos/laravel-logo-white.png')}}" class="animate" data-animation="fadeIn" data-delay=".6">
                                </div>
                                <div class="col-lg-2 col-xs-4 text-xs-center">
                                    <img src="{{asset('img/front-end/logos/html5-logo-white.png')}}" class="animate" data-animation="fadeIn" data-delay=".7">
                                </div>
                                <div class="col-lg-2 col-xs-4 text-xs-center">
                                    <img src="{{asset('img/front-end/logos/css3-logo-white.png')}}" class="animate" data-animation="fadeIn" data-delay=".8">
                                </div>
                                <div class="col-lg-2 col-xs-4 text-xs-center">
                                    <img src="{{asset('img/front-end/logos/sass-logo-white.png')}}" class="animate" data-animation="fadeIn" data-delay=".9">
                                </div>
                                <div class="col-lg-2 col-xs-4 text-xs-center">
                                    <img src="{{asset('img/front-end/logos/gulp-logo-white.png')}}" class="animate" data-animation="fadeIn" data-delay="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="parallax-holder hidden-md-down">
                        <img data-src-base='{{asset('img/front-end/')}}/' data-src='<768:app4-small.jpg,<992:app4-medium.jpg,<1199:app4-large.jpg,>1200:app4.jpg'/>
                    </div>
                </section>
                <section id="features">
                    <div class="slide slide-one" data-background="#ffa800" id="feature1">
                        <div class="container-fluid pdtop">
                            <div class="row">
                                <div class="col-md-4 feature text-md-right">
                                    <h2>Lorem ipsum dolor</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                </div>
                                <div class="col-md-8 text-md-left">
                                    <img class="img-thumbnail" src="{{asset('img/front-end/features/1.jpg')}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="#feature2" class="btn btn-xl features-scroll">More features <i class="fa fa-long-arrow-down"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slide slide-two" data-background="#ff9600" id="feature2">
                        <div class="container-fluid pdtop">
                            <div class="row">
                                <div class="col-md-4 feature text-md-right">
                                    <h2>Lorem ipsum dolor</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                </div>
                                <div class="col-md-8 text-md-left">
                                    <img class="img-thumbnail" src="{{asset('img/front-end/features/2.jpg')}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="#feature3" class="btn btn-xl features-scroll">More features <i class="fa fa-long-arrow-down"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slide slide-three" data-background="#ff6c00" id="feature3">
                        <div class="container-fluid pdtop">
                            <div class="row">
                                <div class="col-md-4 feature text-md-right">
                                    <h2>Lorem ipsum dolor</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                </div>
                                <div class="col-md-8 text-md-left">
                                    <img class="img-thumbnail" src="{{asset('img/front-end/features/3.jpg')}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="#feature4" class="btn btn-xl features-scroll">More features <i class="fa fa-long-arrow-down"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slide slide-four" data-background="#ff3000" id="feature4">
                        <div class="container-fluid pdtop">
                            <div class="row">
                                <div class="col-md-4 feature text-md-right">
                                    <h2>Lorem ipsum dolor</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                </div>
                                <div class="col-md-8 text-md-left">
                                    <img class="img-thumbnail" src="{{asset('img/front-end/features/4.jpg')}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="#feature5" class="btn btn-xl features-scroll">More features <i class="fa fa-long-arrow-down"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slide slide-five" data-background="#ff0000" id="feature5">
                        <div class="container-fluid pdtop">
                            <div class="row">
                                <div class="col-md-4 feature text-md-right">
                                    <h2>Lorem ipsum dolor</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                </div>
                                <div class="col-md-8 text-md-left">
                                    <img class="img-thumbnail" src="{{asset('img/front-end/features/5.jpg')}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="#gallery" class="btn btn-xl page-scroll">Gallery <i class="fa fa-long-arrow-down"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="gallery" class="">
                    <div class="container-fluid">
                        <div class="row no-gutter">
                            <div class="col-lg-4 col-sm-6">
                                <a href="{{asset('img/front-end/gallery/1.jpg')}}" class="gallery-box">
                                    <img src="{{asset('img/front-end/gallery/1.jpg')}}" class="img-responsive" alt="">
                                    <div class="gallery-box-caption">
                                        <div class="gallery-box-caption-content">
                                            <div class="project-category text-faded">
                                                Image
                                            </div>
                                            <div class="project-name">
                                                Description
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <a href="{{asset('img/front-end/gallery/2.jpg')}}" class="gallery-box">
                                    <img src="{{asset('img/front-end/gallery/2.jpg')}}" class="img-responsive" alt="">
                                    <div class="gallery-box-caption">
                                        <div class="gallery-box-caption-content">
                                            <div class="project-category text-faded">
                                                Image
                                            </div>
                                            <div class="project-name">
                                                Description
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <a href="{{asset('img/front-end/gallery/3.jpg')}}" class="gallery-box">
                                    <img src="{{asset('img/front-end/gallery/3.jpg')}}" class="img-responsive" alt="">
                                    <div class="gallery-box-caption">
                                        <div class="gallery-box-caption-content">
                                            <div class="project-category text-faded">
                                                Image
                                            </div>
                                            <div class="project-name">
                                                Description
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <a href="{{asset('img/front-end/gallery/4.jpg')}}" class="gallery-box">
                                    <img src="{{asset('img/front-end/gallery/4.jpg')}}" class="img-responsive" alt="">
                                    <div class="gallery-box-caption">
                                        <div class="gallery-box-caption-content">
                                            <div class="project-category text-faded">
                                                Image
                                            </div>
                                            <div class="project-name">
                                                Description
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <a href="{{asset('img/front-end/gallery/5.jpg')}}" class="gallery-box">
                                    <img src="{{asset('img/front-end/gallery/5.jpg')}}" class="img-responsive" alt="">
                                    <div class="gallery-box-caption">
                                        <div class="gallery-box-caption-content">
                                            <div class="project-category text-faded">
                                                Image
                                            </div>
                                            <div class="project-name">
                                                Description
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <a href="{{asset('img/front-end/gallery/6.jpg')}}" class="gallery-box">
                                    <img src="{{asset('img/front-end/gallery/6.jpg')}}" class="img-responsive" alt="">
                                    <div class="gallery-box-caption">
                                        <div class="gallery-box-caption-content">
                                            <div class="project-category text-faded">
                                                Image
                                            </div>
                                            <div class="project-name">
                                                Description
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="differentials">
                    <div class="container-fluid pdtop pdbottom">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 text-xs-center">
                                <div>
                                    <i class="fa fa-2x fa-diamond animate" data-animation="bounceIn" data-delay=".5"></i>
                                    <h2>Premium support</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur<br> adipiscing elit, sed do eiusmod tempor<br> incididunt ut labore et dolore magna aliqua</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 text-xs-center">
                                <div>
                                    <i class="fa fa-2x fa-file-text-o animate" data-animation="bounceIn" data-delay=".6"></i>
                                    <h2>Well documented</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur<br> adipiscing elit, sed do eiusmod tempor<br> incididunt ut labore et dolore magna aliqua</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 text-xs-center">
                                <div>
                                    <i class="fa fa-2x fa-star-o animate" data-animation="bounceIn" data-delay=".7"></i>
                                    <h2>Free Updates</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur<br> adipiscing elit, sed do eiusmod tempor<br> incididunt ut labore et dolore magna aliqua</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 text-xs-center">
                                <div>
                                    <i class="fa fa-2x fa-heart-o animate" data-animation="bounceIn" data-delay=".8"></i>
                                    <h2>Free produtcs</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur<br> adipiscing elit, sed do eiusmod tempor<br> incididunt ut labore et dolore magna aliqua</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="about" class="bg-foreground fill">
                    <div class="parallax-holder">
                        <img data-src-base='{{asset('img/front-end/')}}/' data-src='<768:app4-devices-small.jpg,<992:app4-devices-medium.jpg,<1199:app4-devices-large.jpg,>1200:app4-devices.jpg'/>
                    </div>
                    <div class="container-fluid ">
                        <div class="row">
                            <div class="col-lg-8 col-lg-offset-2 text-xs-center">
                                <h2 class="section-heading animate" data-animation="bounceIn" data-delay="1">About App4</h2>
                                <hr class="primary">
                            </div>
                        </div>
                        <div class="row text-xs-center">
                            <div class="col-xs-12">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<br>
                                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. <br>
                                    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                            </div>
                        </div>
                        <div class="row text-xs-center" style="padding-bottom: 50px;"><a href="#" class="btn btn-default btn-xl animate" data-animation="tada">Buy now</a></div>
                    </div>
                </section>
                <section id="contact" class="bg-foreground fill">
                    <div class="container pdtop pdbottom">
                        <div class="row">
                            <div class="col-lg-8 col-lg-offset-2 text-xs-center">
                                <h2 class="section-heading animate" data-animation="bounceIn" data-delay="1">Contact</h2>
                                <hr class="primary">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-7">
                                <form role="form" id="contactForm" data-toggle="validator">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="prepend-icon">
                                                        <input type="text" id="name" name="name" class="form-control input-lg" placeholder="Your name" required>
                                                    </div>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="prepend-icon">
                                                        <input type="email" id="email" name="email" class="form-control input-lg" placeholder="Email" required>
                                                    </div>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <input id="subject" name="subject" class="form-control input-lg" type="text" required="required" placeholder="Subject">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea id="message" name="message" class="form-control" rows="10" placeholder="Message"></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <button type="submit" id="form-submit" class="btn btn-primary btn-lg icon-left-effect"><i class="nc-icon-outline ui-1_email-85"></i><span>Send message</span></button>
                                    <div id="msgSubmit" class="hidden"></div>
                                </form>
                            </div>
                            <div class="col-md-5">
                                <div id="map"></div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <footer>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 text-xs-center pdtop">
                        <img src="{{asset('img/front-end/app4_logo_white.png')}}" width="150px">
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
</div>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    // When the window has finished loading create our google map below
    google.maps.event.addDomListener(window, 'load', init);

    function init() {
        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 11,

            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(40.6700, -73.9400), // New York

            // How you would like to style the map.
            // This is where you would paste any style found on Snazzy Maps.
            styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}]
        };

        // Get the HTML DOM element that will contain your map
        // We are using a div with id="map" seen below in the <body>
        var mapElement = document.getElementById('map');

        // Create the Google Map using our element and options defined above
        var map = new google.maps.Map(mapElement, mapOptions);

        // Let's also add a marker while we're at it
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(40.6700, -73.9400),
            map: map,
            title: 'Snazzy!'
        });
    }
</script>


@if ($develop)

    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/jquery-ui/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('vendor/tether/js/tether.min.js') }}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('vendor/responsive-img.js/responsive-img.min.js')}}"></script>
    <script src="{{asset('js/jquery.easing.min.js')}}"></script>
    <script src="{{asset('vendor/superslides/jquery.superslides.min.js')}}"></script>
    <script src="{{asset('vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('js/jquery.fittext.js')}}"></script>
    <script src="{{asset('js/scrollPosStyler.js')}}"></script>
    <script src="{{asset('js/jquery.scrollie.min.js')}}"></script>
    <script src="{{asset('js/front-end.js')}}"></script>

@else

    <script src="{{asset('dist/main-front.js')}}"></script>

@endif

</body>

</html>