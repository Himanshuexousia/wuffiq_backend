@extends('app')

@section('page-title')
    Timeline
@endsection

@section('page-css')
    
@endsection
@section('content-header')
    <h1>
        Timeline
    </h1>
@endsection

@section('content')

    <div class="card card-default">
        <div class="card-block">
            <div id="timeline" class="mt30">
                <div class="timeline-divider mtn">
                    <div class="divider-label">{{date('F Y')}}</div>
                </div>

                <div class="row">
                    <div class="col-sm-6 left-column">
                        <div class="timeline-item">
                            <div class="timeline-icon">
                                <span class="glyphicon glyphicon-user text-primary"></span>
                            </div>
                            <div class="card">
                                <div class="card-header">
                    <span class="card-title">
                      <span class="glyphicon glyphicon-comment"></span> User Posting </span>
                                    <div class="card-header-menu pull-sm-right mr10 text-muted fs12"> {{date('F')}} {{date('d')}}, {{date('Y')}} </div>
                                </div>
                                <div class="card-block">
                                    <div class="media">
                                        <a class="pull-sm-left" href="#"> <img class="media-object thumbnail mw50 rounded" src="{{asset('img/avatars/user1.jpg')}}" alt="..."> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading mb20">Checkmate Digital Posted
                                                <small> - 3 hours ago</small>
                                            </h5>
                                            <img src="{{asset('img/photo1.png')}}" class="mw140 mr25 mb20" /> <img src="{{asset('img/photo2.png')}}" class="mw140 mb20" />
                                            <div class="media-links">
                          <span class="text-light fs12 mr10">
                            <span class="fa fa-thumbs-o-up text-primary mr5"></span> Like </span>
                          <span class="text-light fs12 mr10">
                            <span class="fa fa-share text-primary mr5"></span> Share </span>
                          <span class="text-light fs12 mr10">
                            <span class="glyphicon glyphicon-floppy-save text-primary mr5"></span> Save </span>
                          <span class="text-light fs12 mr10">
                            <span class="fa fa-comments text-primary mr5"></span> Comment </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="media mt20">
                                        <a class="pull-sm-left" href="#"> <img class="media-object thumbnail thumbnail-sm rounded mw40" src="{{asset('img/avatars/user6.jpg')}}" alt="..."> </a>
                                        <div class="media-body mb5">
                                            <h5 class="media-heading mbn">Sarah Rose
                                                <small> - 3 hours ago</small>
                                            </h5>
                                            <p> Omg so freaking sweet dude.</p>
                                        </div>
                                    </div>
                                    <div class="media mt15">
                                        <a class="pull-sm-left" href="#"> <img class="media-object thumbnail thumbnail-sm rounded mw40" src="{{asset('img/avatars/user5.jpg')}}" alt="..."> </a>
                                        <div class="media-body mb5">
                                            <h5 class="media-heading mbn">Alex Moon
                                                <small> - 3 hours ago</small>
                                            </h5>
                                            <p>Omgosh I'm in love</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer p15">
                                    <div class="admin-form">
                                        <label for="reply1" class="field prepend-icon">
                                            <input type="text" name="reply1" id="reply1" class="event-name gui-input" placeholder="Respond with a comment.">
                                            <label for="reply1" class="field-icon">
                                                <i class="fa fa-pencil"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="timeline-item">
                            <div class="timeline-icon">
                                <span class="fa fa-video-camera text-primary"></span>
                            </div>
                            <div class="card">
                                <div class="card-header">
                    <span class="card-title">
                      <span class="glyphicon glyphicon-facetime-video"></span> Timeline Video! </span>
                                    <div class="card-header-menu pull-sm-right mr10 text-muted fs12"> January 27, 2016 </div>
                                </div>
                                <div class="card-block">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/eX_iASz1Si8" frameborder="0" allowfullscreen=""></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 right-column">
                        <div class="timeline-item">
                            <div class="timeline-icon">
                                <span class="fa fa-picture-o text-info"></span>
                            </div>
                            <div class="card">
                                <div class="card-block">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a class="gallery-item" href="{{asset('img/photo4.jpg')}}"><img src="{{asset('img/photo4.jpg')}}" class="img-responsive" alt="">
                                            </a>
                                        </div>
                                        <div class="col-xs-6">
                                            <a class="gallery-item" href="{{asset('img/photo5.jpg')}}"><img src="{{asset('img/photo5.jpg')}}" class="img-responsive" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="timeline-item">
                            <div class="timeline-icon">
                                <span class="fa fa-compass text-success"></span>
                            </div>
                            <div class="card">
                                <div class="card-header">
                    <span class="card-title">
                      <span class="glyphicon glyphicon-camera"></span> Map Posting</span>
                                    <div class="card-header-menu pull-sm-right mr10 text-muted fs12"> February 2, 2016 </div>
                                </div>
                                <div class="card-block">
                                    <iframe width="100%" height="275" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=Rio%20de%20Janeiro&amp;key=AIzaSyBqf9cjsPAh3rGJPKLCsYL2IeAveGhGJTw&amp;zoom=14"></iframe>
                                </div>
                            </div>
                        </div>
                        <div class="timeline-item">
                            <div class="timeline-icon">
                                <span class="fa fa-paperclip text-danger"></span>
                            </div>
                            <div class="card">
                                <div class="card-block p10">
                                    <blockquote class="mbn ml10">
                                        <p>Penatibus, duis! Ridiculus platea, augue in urna, nunc ultricies. Adipiscing aenean, pellentesque turpis ac magna dignissim parturient cras odio. Massa sit amet et, in, sociis, ac magnis adipiscing! Tortor! Velit porta magna tempor sagittis egestas. Diam phasellus parturient vel, cras porttitor nunc dapibus? Penatibus ut? Pellentesque phasellus non mattis. ;)</p>
                                        <small>Checkmate Digital</small>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="timeline-divider">
                    <div class="divider-label">2015</div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script>
    </script>
@endsection