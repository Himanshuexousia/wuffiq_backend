@extends('app')

@section('page-title')
    General Form
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{asset('vendor/select2/css/select2.min.css')}}">

@endsection

@section('content-header')
    <h1>
        Bank Edit 
    </h1>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
						@if(Session::has('message'))
			            <div class="alert alert-success"> 
						  <center>Successfully Updated</center>
						</div>
						@endif
                <div class="card-header">
                    Edit bank
                    <div class="pull-md-right">

                    </div>
                </div>
                <div class="card-block">
                    <form method="POST" action="{{URL('update-bank')}}" >
					    <fieldset class="form-group">
                            <label for="bankName">Bank Name</label>
                            <input type="text" value="{{$bankDetail->bankName}}" name="bankName" required class="form-control" id="bankName" placeholder="Bank Name">
                        </fieldset>
					    <fieldset class="form-group">
                            <label for="name">Name</label>
                            <input type="text" value="{{$bankDetail->name}}" required name="name" class="form-control" id="name" placeholder="Name">
                        </fieldset>	
					    <fieldset class="form-group">
                            <label for="accountNumber">Account Number</label>
                            <input type="text" value="{{$bankDetail->accountNumber}}" required name="accountNumber" class="form-control" id="accountNumber" placeholder="Account Number">
                        </fieldset>
					    <fieldset class="form-group">
                            <label for="iban">IBAN</label>
                            <input type="text" value="{{$bankDetail->IBAN}}" required name="IBAN" class="form-control" id="iban" placeholder="IBAN">
                        </fieldset>	
					    <fieldset class="form-group">
                            <label for="arabicBankName">Arabic BankName</label>
                            <input type="text" dir='rtl' value="{{$bankDetail->arabic_bankName}}" required name="arabicBankName" class="form-control" id="arabicBankName" placeholder="Arabic BankName">
                        </fieldset>	
					    <fieldset class="form-group">
                            <label for="arabicName">Arabic Name</label>
                            <input type="text" dir='rtl' value="{{$bankDetail->arabic_name}}" required name="arabicName" class="form-control" id="arabicName" placeholder="Arabic Name">
                        </fieldset>								
						<input type="hidden" name="bankId" id="bankId" value="{{$bankDetail->id}}">				
						<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">				
						<center><button type="submit" class="btn btn-success form-control">Update</button></center>		
					</form>
				</div>
			</div>
		</div>
	</div>	 
@endsection

@section('page-scripts')
    <script src="{{asset('vendor/select2/js/select2.full.min.js')}}"></script>
    <script src="{{asset('js/form-advanced.js')}}"></script>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>	