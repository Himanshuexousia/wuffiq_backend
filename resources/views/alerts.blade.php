@extends('app')

@section('page-title')
    Alerts, Tooltips and Popovers
@endsection

@section('page-css')
    
@endsection
@section('content-header')
    <h1>
        Alerts, Tooltips and Popovers
    </h1>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Notification features
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-6">
                    <h4>Alerts</h4>
                    <div class="alert alert-success" role="alert"><strong>Well done!</strong> You successfully read this important alert message. </div>
                    <div class="alert alert-info" role="alert"><strong>Heads up!</strong> This alert needs your attention, but it's not super important. </div>
                    <div class="alert alert-warning" role="alert"><strong>Warning!</strong> Better check yourself, you're not looking too good. </div>
                    <div class="alert alert-danger" role="alert"> <strong>Oh snap!</strong> Change a few things up and try submitting again. </div>
                    <div class="alert bg-navy" role="alert"> <strong>Hell yeah!</strong> Change a few things up and try submitting again. </div>
                </div>
                <div class="col-md-6">
                    <h4>Closeable Alerts</h4>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span></button>
                        <strong>Well done!</strong> Now, close me!
                    </div>
                    <div class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span></button>
                        <strong>Heads up!</strong> Now, close me!
                    </div>
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span></button>
                        <strong>Warning!</strong> Now, close me!
                    </div>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span></button>
                        <strong>Oh snap!</strong> Now, close me!
                    </div>
                    <div class="alert bg-navy alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span></button>
                        <strong>Hell yeah!</strong> Now, close me!
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Callouts
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-12">
                    <div class="callout callout-danger">
                        <h4>I am a danger callout!</h4>

                        <p>There is a problem that we need to fix. A wonderful serenity has taken possession of my entire soul,
                            like these sweet mornings of spring which I enjoy with my whole heart.</p>
                    </div>
                    <div class="callout callout-info">
                        <h4>I am an info callout!</h4>

                        <p>Follow the steps to continue to payment.</p>
                    </div>
                    <div class="callout callout-warning">
                        <h4>I am a warning callout!</h4>

                        <p>This is a yellow callout.</p>
                    </div>
                    <div class="callout callout-success">
                        <h4>I am a success callout!</h4>

                        <p>This is a green callout.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Tooltips and Popovers
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-6">
                    <h4>Tooltips</h4>
                    <h5 class="text-md-center">In Button</h5>
                    <div class="row">
                        <div class="col-md-3">
                            <button type="button" class="btn btn-block btn-secondary" data-toggle="tooltip" data-placement="left" title="Tooltip on left">Left</button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn-block btn-secondary" data-toggle="tooltip" data-placement="top" title="Tooltip on top">Top</button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn-block btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">Bottom</button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn-block btn-secondary" data-toggle="tooltip" data-placement="right" title="Tooltip on right">Right</button>
                        </div>
                    </div>
                    <hr />
                    <h5 class="text-md-center">In Text</h5>
                    <p class="text-justify">Nullam sagittis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque posuere. <a href="#" data-toggle="tooltip" title="" data-original-title="Default tooltip">Vivamus</a> euismod mauris. Suspendisse non nisl sit amet velit hendrerit rutrum.</p>

                    <p class="text-justify">Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Fusce pharetra convallis urna. Etiam rhoncus. Praesent blandit laoreet nibh. <a href="#" data-toggle="tooltip" title="Another tooltip">Etian</a> rhoncus.</p>

                    <p class="text-justify">Nunc nec neque. Donec mollis hendrerit risus. Vivamus elementum semper nisi. Donec elit libero, sodales nec, volutpat a, suscipit non, <a href="#" data-toggle="tooltip" title="Another one here too">turpis</a>. Donec orci lectus, aliquam ut, faucibus <a href="#" data-toggle="tooltip" title="The last tip!">viral</a>, euismod id, nulla.</p>
                </div>
                <div class="col-md-6 text-md-center">
                    <h4>Popover</h4>
                    <h5 class="text-md-center">In Button</h5>
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-block btn-secondary" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                                Top
                            </button>
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-block btn-secondary" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                                Right
                            </button>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-block btn-secondary" data-container="body" data-toggle="popover" data-placement="left" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                                Left
                            </button>
                        </div>
                        <div class="col-md-2">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-block btn-secondary" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus
sagittis lacus vel augue laoreet rutrum faucibus.">
                                Bottom
                            </button>
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                        <hr />
                        <h5 class="text-md-center">In Text</h5>
                        <p class="text-justify">Nullam sagittis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque posuere. <a data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus
sagittis lacus vel augue laoreet rutrum faucibus.">Vivamus</a> euismod mauris. Suspendisse non nisl sit amet velit hendrerit rutrum.</p>

                        <p class="text-justify">Aenean tellus metus, <a data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus
sagittis lacus vel augue laoreet rutrum faucibus.">Etian</a> rhoncus. Nunc nec neque. Donec elit libero, sodales nec, volutpat a, suscipit non, <a data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus
sagittis lacus vel augue laoreet rutrum faucibus.">turpis</a>. Donec orci lectus, aliquam ut, faucibus <a data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus
sagittis lacus vel augue laoreet rutrum faucibus.">viral</a>, euismod id, nulla.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('page-scripts')
    <script>
        $(function() {
            $('[data-toggle="popover"]').popover()
        });
    </script>
@endsection