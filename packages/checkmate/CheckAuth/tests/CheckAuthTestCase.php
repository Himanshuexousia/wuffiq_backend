<?php

class CheckAuthTestCase extends Orchestra\Testbench\TestCase
{
    /*
     * These tests make use of the Orchestra Test Bench Package: https://github.com/orchestral/testbench
     */


    // The class being tested
    protected $repo;

    /**
     * Destroy the test environment
     */
    public function tearDown()
    {
        parent::tearDown();
        \Mockery::close();
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     *
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {

        $app['config']->set('database.default', 'testbench');
        //config cipher and key otherwise it'll fire this error: RuntimeException: No supported encrypter found. The cipher and / or key length are invalid.
        //if problem remains you have to change fixture/config/app.php inside vendor/orchestral/workbench
        $app['config']->set('key', '6xa78rnOudUWugfhx6qkQG6h7PMlrrcw');
        $app['config']->set('cipher', 'AES-256-CBC');


        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => __DIR__ . '/_data/test.sqlite',
            'prefix'   => '',
        ]);
        $app['config']->set('mail.pretend', true);
        $app['config']->set('mail.from', ['from' => 'noreply@example.com', 'name' => null]);

        // Prepare the sqlite database
        // http://www.chrisduell.com/blog/development/speeding-up-unit-tests-in-php/
        //exec('cp ' . __DIR__ . '/_data/prep.sqlite ' . __DIR__ . '/_data/db.sqlite');
        $path = __DIR__ . '/_data/';
        File::copy( $path . 'database.sqlite', $path . 'test.sqlite' );
    }

    /**
     * Get package providers.
     *
     * @param  \Illuminate\Foundation\Application  $app
     *
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            'Checkmate\CheckAuth\Providers\CheckAuthServiceProvider',
        ];
    }

    /**
     * Get package alias.
     *
     * @param  \Illuminate\Foundation\Application  $app
     *
     * @return array
     */
    protected function getPackageAliases($app)
    {
        return [
            //'Acme' => 'Acme\Facade',
            'Activation' => Cartalyst\Sentinel\Laravel\Facades\Activation::class,
            'Reminder'   => Cartalyst\Sentinel\Laravel\Facades\Reminder::class,
            'Sentinel'   => Cartalyst\Sentinel\Laravel\Facades\Sentinel::class,
            'CheckAuth' => Checkmate\CheckAuth\Facades\CheckAuth::class
        ];
    }
}
