<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CheckAuthDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call('UserTableSeeder');
        $this->call('SentinelRolesSeeder');
        $this->call('SentinelUserSeeder');
        $this->call('SentinelUserRolesSeeder');
    }
}
