<?php
use Illuminate\Database\Seeder;

class SentinelUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        Sentinel::registerAndActivate(array(
            'email'    => 'admin@auth.com',
            'password' => 'authadmin',
            'first_name' => 'John',
            'last_name' => 'Doe'
        ));

        Sentinel::registerAndActivate(array(
            'email'    => 'user@auth.com',
            'password' => 'authuser',
            'first_name' => 'Jane',
            'last_name' => 'Doe'
        ));
    }
}
