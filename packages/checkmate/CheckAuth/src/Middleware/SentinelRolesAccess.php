<?php

namespace Checkmate\CheckAuth\Middleware;

use Closure;
use Checkmate\CheckAuth\Repositories\User\CheckAuthUserRepositoryInterface;
use Checkmate\CheckAuth\Repositories\Roles\CheckAuthRolesRepositoryInterface;
use Session;

/**
 * middleware provide a convenient mechanism for filtering HTTP requests entering your application.
 * SentinelRolesAccess middleware allows only authenticated users with a certain role to access.
 *
 * @license http://www.checkmate.com/licenses/checkauth
 *
 * @version 1.0.0
 */
class SentinelRolesAccess
{
    /**
     * Constructor
     * @param CheckAuthUserRepositoryInterface $userManager
     * @param CheckAuthRolesRepositoryInterface $rolesManager
     */
    public function __construct(CheckAuthUserRepositoryInterface $userManager, CheckAuthRolesRepositoryInterface $rolesManager)
    {
        $this->user = $userManager;
        $this->roles = $rolesManager;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param string $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $user = $this->user->getUser();
        if (!$user) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest(route('checkauth.login'));
            }
        }else{
            // Find the specified group
            $role = $this->roles->retrieveByName($role);

            // Now check to see if the current user is a member of the specified group
            if (!$user->inRole($role)) {
                if ($request->ajax()) {
                    return response('Unauthorized.', 401);
                } else {
                    Session::flash('error', trans('checkauth::users.noaccess'));

                    return back();
                }
            }
        }

        return $next($request);
    }
}
