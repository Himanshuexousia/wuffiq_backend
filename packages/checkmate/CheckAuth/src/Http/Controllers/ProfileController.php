<?php

namespace Checkmate\CheckAuth\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

use Checkmate\CheckAuth\FormRequests\ChangePasswordRequest;
use Checkmate\CheckAuth\FormRequests\UserUpdateRequest;
use Checkmate\CheckAuth\Repositories\Roles\CheckAuthRolesRepositoryInterface;
use Checkmate\CheckAuth\Repositories\User\CheckAuthUserRepositoryInterface;
use Checkmate\CheckAuth\Traits\CheckAuthRedirectionTrait;
use Checkmate\CheckAuth\Traits\CheckAuthViewfinderTrait;

use Session;
use Input;
use Response;
use Redirect;

/**
 * User Profile controller
 *
 * @license http://www.checkmate.com/licenses/checkauth
 *
 * @version 1.0.0
 */
class ProfileController extends BaseController
{
    /**
     * Traits
     */
    use CheckAuthRedirectionTrait;
    use CheckAuthViewfinderTrait;

    /**
     * @param CheckAuthUserRepositoryInterface $userRepository
     * @param CheckAuthRolesRepositoryInterface $roleRepository
     * Constructor
     */
    public function __construct(
        CheckAuthUserRepositoryInterface $userRepository,
        CheckAuthRolesRepositoryInterface $roleRepository
    ) {
        // DI Member assignment
        $this->userRepository  = $userRepository;
        $this->roleRepository = $roleRepository;

        // You must have an active session to proceed
        //$this->middleware('sentinel.auth');//middleware is in route now
    }

    /**
     * Display the specified resource.
     *
     * @return $this->viewFinder
     */
    public function show()
    {

        $user = $this->userRepository->getUser();
        $roles = $this->roleRepository->all();

        return $this->viewFinder('checkauth::users.show', ['user' => $user, 'roles' => $roles]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @return $this->viewFinder
     */
    public function edit()
    {
        $user = $this->userRepository->getUser();
        $roles = $this->roleRepository->all();

        $permissions = config('checkauth.default_permissions');

        $userFields = array(
            'first_name' => 'alpha',
            'last_name' => 'alpha',
            'avatar' => 'image'
        );

        if(config('checkauth.additional_user_fields'))
        $userFields = array_merge($userFields, config('checkauth.additional_user_fields'));

        return $this->viewFinder('checkauth::users.edit', [
            'user' => $user,
            'roles' => $roles,
            'userFields' => $userFields,
            'permissions' => $permissions
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateRequest $request
     *
     * @return $this->redirectViaResponse
     */
    public function update(UserUpdateRequest $request)
    {
        // Gather Input
        $data       = $request->all();
        $data['id'] = $this->userRepository->getUser()->id;

        // Attempt to update the user
        $result = $this->userRepository->store($data);

        // Done!
        return $this->redirectViaResponse('profile_update', $result);
    }

    /**
     * Process a password change request
     *
     * @param ChangePasswordRequest $request
     *
     * @return Redirect::back()
     */
    public function changePassword(ChangePasswordRequest $request)
    {

        $user = $this->userRepository->getUser();

        $data       = $request->all();
        $data['id'] = $user->id;

        // Change the User's password
        $result = ($user->hasAccess('admin') ? $this->userRepository->changePasswordWithoutCheck($data) : $this->userRepository->changePassword($data));

        // Was the change successful?
        if (!$result->isSuccessful()) {
            Session::flash('error', $result->getMessage());

            return Redirect::back();
        }

        return $this->redirectViaResponse('profile_change_password', $result);
    }
}
