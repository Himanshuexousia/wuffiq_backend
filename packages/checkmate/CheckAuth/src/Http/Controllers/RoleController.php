<?php

namespace Checkmate\CheckAuth\Http\Controllers;

use Vinkla\Hashids\HashidsManager;
use Illuminate\Routing\Controller as BaseController;
use Checkmate\CheckAuth\FormRequests\RoleUpdateRequest;
use Checkmate\CheckAuth\FormRequests\RoleCreateRequest;
use Checkmate\CheckAuth\Repositories\Roles\CheckAuthRolesRepositoryInterface;
use Checkmate\CheckAuth\Traits\CheckAuthRedirectionTrait;
use Checkmate\CheckAuth\Traits\CheckAuthViewfinderTrait;
use Checkmate\CheckAuth\Models\Role;
use View;
use Input;
use Redirect;

/**
 * Roles controller
 *
 * @license http://www.checkmate.com/licenses/checkauth
 *
 * @version 1.0.0
 */
class RoleController extends BaseController
{
    /**
     * Traits
     */
    use CheckAuthRedirectionTrait;
    use CheckAuthViewfinderTrait;

    /**
     * Constructor
     *
     * @param CheckAuthRolesRepositoryInterface $roleRepository
     * @param HashidsManager $hashids
     */
    public function __construct(
        CheckAuthRolesRepositoryInterface $roleRepository,
        HashidsManager $hashids
    ) {
        $this->roleRepository = $roleRepository;
        $this->hashids         = $hashids;

        // You must have admin access to proceed
        //$this->middleware('sentinel.admin'); //midleware is in routes now, can be used like this alson
    }

    /**
     * Display a paginated list of all current roles
     *
     * @return $this->viewFinder
     */
    public function index()
    {
        $roles = Role::paginate(15);
        return $this->viewFinder('checkauth::roles.index', ['roles' => $roles]);
    }

    /**
     * Show the form for creating a role
     *
     * @return $this->viewFinder
     */
    public function create()
    {
        return $this->viewFinder('checkauth::roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoleCreateRequest $request
     *
     * @return $this->redirectViaResponse
     */
    public function store(RoleCreateRequest $request)
    {

        $data = $request->all();

        $result = $this->roleRepository->store($data);

        return $this->redirectViaResponse('roles_store', $result);
    }

    /**
     * Display the specified role
     *
     * @param string $hash
     *
     * @return  $this->viewFinder
     */
    public function show($hash)
    {
        // Decode the hashid
        $id = $this->hashids->decode($hash)[0];

        $role = $this->roleRepository->retrieveById($id);

        return $this->viewFinder('checkauth::roles.show', ['role' => $role]);
    }

    /**
     * Show the form for editing the specified role.
     *
     * @param string $hash
     *
     * @return  $this->viewFinder
     */
    public function edit($hash)
    {
        // Decode the hashid
        $id = $this->hashids->decode($hash)[0];

        // Pull the role from storage
        $role = $this->roleRepository->retrieveById($id);

        return $this->viewFinder('checkauth::roles.edit', [
            'role' => $role,
            'permissions' => $role->getPermissions()
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param RoleUpdateRequest $request
     * @param string $hash
     *
     * @return $this->redirectViaResponse
     */
    public function update(RoleUpdateRequest $request, $hash)
    {
        // Gather Input
        $data = $request->all();

        // Decode the hashid
        $data['id'] = $this->hashids->decode($hash)[0];

        // Update the role
        $result = $this->roleRepository->update($data);

        return $this->redirectViaResponse('roles_update', $result);
    }

    /**
     * Remove the specified role from storage.
     *
     * @return $this->redirectViaResponse
     */
    public function destroy($hash)
    {
        // Decode the hashid
        $id = $this->hashids->decode($hash)[0];

        // Remove the role from storage
        $result = $this->roleRepository->destroy($id);

        return $this->redirectViaResponse('roles_destroy', $result);
    }
}
