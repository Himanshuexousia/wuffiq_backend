<?php

namespace Checkmate\CheckAuth\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Checkmate\CheckAuth\FormRequests\ChangePasswordRequest;
use Checkmate\CheckAuth\FormRequests\UserCreateRequest;
use Checkmate\CheckAuth\FormRequests\UserUpdateRequest;
use Checkmate\CheckAuth\Repositories\Roles\CheckAuthRolesRepositoryInterface;
use Checkmate\CheckAuth\Repositories\User\CheckAuthUserRepositoryInterface;
use Checkmate\CheckAuth\Traits\CheckAuthRedirectionTrait;
use Checkmate\CheckAuth\Traits\CheckAuthViewfinderTrait;
use Vinkla\Hashids\HashidsManager;
use Checkmate\CheckAuth\Models\User;
use View;
use Input;
use Event;
use Redirect;
use Session;
use Config;

/**
 * User controller
 *
 * @license http://www.checkmate.com/licenses/checkauth
 *
 * @version 1.0.0
 */
class UserController extends BaseController
{
    /**
     * Traits
     */
    use CheckAuthRedirectionTrait;
    use CheckAuthViewfinderTrait;

    /**
     * Constructor
     * @param CheckAuthUserRepositoryInterface $userRepository
     * @param CheckAuthRolesRepositoryInterface $roleRepository
     * @param HashidsManager $hashids
     */
    public function __construct(
        CheckAuthUserRepositoryInterface $userRepository,
        CheckAuthRolesRepositoryInterface $roleRepository,
        HashidsManager $hashids
    ) {
        $this->userRepository  = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->hashids         = $hashids;

        // You must have admin access to proceed
        //$this->middleware('sentinel.admin'); //midleware is now through routing
    }

    /**
     * Display a paginated index of all current users, with throttle data
     *
     * @return $this->viewFinder
     */
    public function index()
    {
        $users = User::paginate(10);
        return $this->viewFinder('checkauth::users.index', ['users' => $users]);
    }


    /**
     * Show the "Create new User" form
     *
     * @return $this->viewFinder
     */
    public function create()
    {
        return $this->viewFinder('checkauth::users.create');
    }

    /**
     * Create a new user account manually
     *
     * @param UserCreateRequest $request
     *
     * @return $this->redirectTo
     */
    public function insert(UserCreateRequest $request)
    {
        if(config('checkauth.disableSave')){
            $message = trans('checkauth::pages.savingDisabled');
            return $this->redirectTo('users_store', ['warning' => $message]);
        }else{
            // Create and store the new user
            $result = $this->userRepository->store($request->all());

            // Determine response message based on whether or not the user was activated
            $message = ($result->getPayload()['activated'] ? trans('checkauth::users.addedactive') : trans('checkauth::users.added'));
        }

        // Finished!
        return $this->redirectTo('users_store', ['success' => $message]);
    }


    /**
     * Show the profile of a specific user account
     *
     * @param $hash
     *
     * @return $this->viewFinder
     */
    public function show($hash)
    {
        // Decode the hashid
        $id = $this->hashids->decode($hash)[0];

        // Get the user
        $user = $this->userRepository->retrieveById($id);
        $roles = $this->roleRepository->all();

        return $this->viewFinder('checkauth::users.show', ['user' => $user, 'roles' => $roles]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $hash
     *
     * @return $this->viewFinder
     */
    public function edit($hash)
    {

        // Decode the hashid
        $id = $this->hashids->decode($hash)[0];

        // Get the user
        $user = $this->userRepository->retrieveById($id);

        // Get all available roles
        $roles = $this->roleRepository->all();

        $permissions = config('checkauth.default_permissions');

        $userFields = array(
            'first_name' => 'alpha',
            'last_name' => 'alpha',
            'avatar' => 'image'
        );

        if(config('checkauth.additional_user_fields'))
        $userFields = array_merge($userFields, config('checkauth.additional_user_fields'));

        return $this->viewFinder('checkauth::users.edit', [
            'user' => $user,
            'roles' => $roles,
            'userFields' => $userFields,
            'permissions' => $permissions
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  string $hash
     *
     * @param UserUpdateRequest $request
     *
     * @return $this->redirectViaResponse
     */
    public function update(UserUpdateRequest $request, $hash)
    {
        if(config('checkauth.disableSave')){
            $message = trans('checkauth::pages.savingDisabled');
            return $this->redirectTo('users_update', ['warning' => $message]);
        }else {
            // Gather Input
            $data = $request->all();

            // Decode the hashid
            $data['id'] = $this->hashids->decode($hash)[0];

            // Attempt to update the user
            $result = $this->userRepository->store($data);

            // Done!
            return $this->redirectViaResponse('users_update', $result);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  string $hash
     *
     * @return $this->redirectViaResponse
     */
    public function destroy($hash)
    {
        if(config('checkauth.disableSave')){
            $message = trans('checkauth::pages.savingDisabled');
            return $this->redirectTo('users_destroy', ['warning' => $message]);
        }else {
            // Decode the hashid
            $id = $this->hashids->decode($hash)[0];

            // Remove the user from storage
            $result = $this->userRepository->destroy($id);

            return $this->redirectViaResponse('users_destroy', $result);
        }
    }

    /**
     * Change the role memberships for a given user
     *
     * @param $hash
     *
     * @return $this->redirectViaResponse
     */
    public function updateRoleMemberships($hash)
    {
        if(config('checkauth.disableSave')){
            $message = trans('checkauth::pages.savingDisabled');
            return $this->redirectTo('users_change_memberships', ['warning' => $message]);
        }else {
            // Decode the hashid
            $id = $this->hashids->decode($hash)[0];

            // Gather input
            $roles = Input::get('roles');

            // Change memberships
            $result = $this->userRepository->changeRoleMemberships($id, $roles);

            return $this->redirectViaResponse('users_change_memberships', $result);
        }
    }

    /**
     * Change the permissions for a given user
     *
     * @param $hash
     *
     * @return $this->redirectViaResponse
     */
    public function updatePermissions($hash)
    {
        if(config('checkauth.disableSave')){
            $message = trans('checkauth::pages.savingDisabled');
            return $this->redirectTo('users_change_permissions', ['warning' => $message]);
        }else {
            // Decode the hashid
            $id = $this->hashids->decode($hash)[0];

            // Gather input
            $permissions = Input::get('permissions');

            // Change memberships
            $result = $this->userRepository->changePermissions($id, $permissions);

            return $this->redirectViaResponse('users_change_permissions', $result);
        }
    }

    /**
     * Process a password change request
     *
     * @param  string $hash
     * @param ChangePasswordRequest $request
     *
     * @return $this->redirectViaResponse
     */
    public function changePassword(ChangePasswordRequest $request, $hash)
    {

        // Gather input
        $data = $request->all();
        $data['id'] = $this->hashids->decode($hash)[0];

        // Grab the current user
        $user = $this->userRepository->getUser();

        // Change the User's password
        $result = ($user->hasAccess('admin') ? $this->userRepository->changePasswordWithoutCheck($data) : $this->userRepository->changePassword($data));

        // Was the change successful?
        if (!$result->isSuccessful()) {
            Session::flash('error', $result->getMessage());

            return Redirect::back();
        }

        return $this->redirectViaResponse('users_change_password', $result);

    }


    /**
     * deactivate a user
     *
     * @param  string $hash
     *
     * @return $this->redirectViaResponse
     */
    public function deactivateUser($hash)
    {
            $id = $this->hashids->decode($hash)[0];

            $result = $this->userRepository->deactivateUser($id);

            return $this->redirectViaResponse('users_deactivated', $result);
    }

    /**
     * activate a user
     *
     * @param string $hash
     *
     * @return $this->redirectViaResponse
     */
    public function activateUser($hash)
    {
            $id = $this->hashids->decode($hash)[0];

            $result = $this->userRepository->activateUser($id);

            return $this->redirectViaResponse('users_activated', $result);
    }
}