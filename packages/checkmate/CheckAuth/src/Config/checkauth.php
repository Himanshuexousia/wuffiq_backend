<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Registration
    |--------------------------------------------------------------------------
    |
    | Users can register on your app. If false, users will only be created for another logged user
    |
    */

    'registration' => true,

    /*
    |--------------------------------------------------------------------------
    | Recapcha
    |--------------------------------------------------------------------------
    |
    | Use recapcha on login form
    |
    */

    'recapcha' => true,


    /*
    |--------------------------------------------------------------------------
    | Activation
    |--------------------------------------------------------------------------
    |
    | By default, new accounts must be activated via email (exception made for
    | oauth with facebook, google+, etc).  Setting this to
    | false will allow users to login immediately after signing up.
    |
    */

    'require_activation' => true,


    /*
    |--------------------------------------------------------------------------
    | Default User Roles
    |--------------------------------------------------------------------------
    |
    | When a new user is created, they will automatically be added to the
    | roles in this array.
    |
    */

    'default_user_roles' => ['Users'],


    /*
    |--------------------------------------------------------------------------
    | Default Roles Permissions
    |--------------------------------------------------------------------------
    |
    | Permission types allowed in you application
    | roles in this array.
    |
    */

    'default_permissions' => ['users.list', 'users.edit', 'roles.edit'],

    /*
    |--------------------------------------------------------------------------
    | Oath providers
    |--------------------------------------------------------------------------
    |
    | Oauth authentication is not allowed in your application by default
    | You can add an array of the providers you'll want to use.
    | CheckAuth supports: facebook, twitter, linkedin, google, github or bitbucket
    | You can specify an array of the providers you want like that:
    | ['facebook', 'twitter', 'linkedin', 'google', 'github', 'bitbucket']
    */

    'oauth_providers' => ['facebook', 'google'],

    /*
   |--------------------------------------------------------------------------
   | Default Oauth User Roles
   |--------------------------------------------------------------------------
   |
   | If the client was registered by Oauth, they will automatically be added to the
   | roles in this array.
   |
   */

    'default_oauth_user_roles' => null, //user will just be logged in, treated as a guest

    /*
    |--------------------------------------------------------------------------
    | Remember oauth users after authenticate
    |--------------------------------------------------------------------------
    |
    | By default, users authenticated through Oauth (social authentication like google) will be remembered.
    |
    */

    'remember_oauth' => true,

    /*
    |--------------------------------------------------------------------------
    | Default oauth Roles Permissions
    |--------------------------------------------------------------------------
    |
    | Permission types allowed in you application through Oauth (social authentication like google) will be remembered.
    | roles in this array.
    |
    */

    'default_oauth_permissions' => null,

    /*
    |--------------------------------------------------------------------------
    | Custom User Fields
    |--------------------------------------------------------------------------
    |
    | If you want to add additional fields to your user model you can specify
    | their validation needs here (can be alpha spaces or image).  You must update your db tables and add
    | the fields to your 'create' and 'edit' views before this will work.
    | value must be a key pair like: 'middle_name' => 'alpha'
    |
    */

    'additional_user_fields' => null,

    /*
    |--------------------------------------------------------------------------
    | path for user profile images inside public folder
    |--------------------------------------------------------------------------
    |
    */

    'imagePath' => 'img/avatars/',


    /*
    |--------------------------------------------------------------------------
    | E-Mail Subject Lines
    |--------------------------------------------------------------------------
    |
    | When using the "Eloquent" authentication driver, we need to know which
    | Eloquent model should be used to retrieve your users. Of course, it
    | is often just the "User" model but you may use whatever you like.
    |
    */

    'subjects' => [
        'activate'        => 'Account Registration Confirmation',
        'welcome'        => 'Account Registration Confirmed',
        'reset_password' => 'Password Reset Confirmation'
    ],

    /*
    |--------------------------------------------------------------------------
    | Default Routing
    |--------------------------------------------------------------------------
    |
    | CheckAuth provides default routes for its sessions, users and roles.
    | You can use them as is, or you can disable them entirely.
    |
    */
    'routes_enabled' => true,

    /*
    |--------------------------------------------------------------------------
    | Publish Routes on app/http
    |--------------------------------------------------------------------------
    |
    | CheckAuth will publish its routes on a file called checkauth_routes.php on app/http folder.
    |
    */
    'publish_routes' => true,
    

    /*
    |--------------------------------------------------------------------------
    | URL Redirection for Method Completion
    |--------------------------------------------------------------------------
    |
    | Upon completion of their tasks, controller methods will look-up their
    | return destination here. You can specify a route, action or URL.
    | If no action is specified a JSON response will be returned.
    |
    */

    'routing' => [
        'session_store'                => ['route' => 'checkauth.app'],
        'session_destroy'              => ['action' => '\\Checkmate\CheckAuth\Http\Controllers\SessionController@create'],
        //'session_destroy'                => ['route' => 'checkauth.login'],
        'registration_complete'        => ['route' => 'checkauth.app'],
        'registration_activated'       => ['route' => 'checkauth.app'],
        'registration_resend'          => ['route' => 'checkauth.app'],
        'registration_reset_triggered' => ['route' => 'checkauth.app'],
        'registration_reset_invalid'   => ['route' => 'checkauth.app'],
        'registration_reset_complete'  => ['route' => 'checkauth.app'],
        'users_invalid'                => ['route' => 'checkauth.app'],
        'users_store'                  => ['route' => 'checkauth.users.index'],
        'users_update'                 => ['route' => 'checkauth.users.show', 'parameters' => ['user' => 'hash']],
        'users_destroy'                => ['route' => 'checkauth.users.index'],
        'users_change_password'        => ['route' => 'checkauth.users.show', 'parameters' => ['user' => 'hash']],
        'users_change_memberships'     => ['route' => 'checkauth.users.show', 'parameters' => ['user' => 'hash']],
        'users_change_permissions'     => ['route' => 'checkauth.users.show', 'parameters' => ['user' => 'hash']],
        'users_deactivated'             => ['route' => 'checkauth.users.index'],
        'users_activated'               => ['route' => 'checkauth.users.index'],
        'roles_store'                 => ['route' => 'checkauth.roles.index'],
        'roles_update'                => ['route' => 'checkauth.roles.index'],
        'roles_destroy'               => ['route' => 'checkauth.roles.index'],
        'profile_change_password'      => ['route' => 'checkauth.profile.show'],
        'profile_update'               => ['route' => 'checkauth.profile.show'],
        'no_permission'                => ['route' => 'checkauth.app'],
    ],

    /*
    |--------------------------------------------------------------------------
    | Guest Middleware Redirection
    |--------------------------------------------------------------------------
    |
    | The SentinelGuest middleware will redirect users with active sessions to
    | the route you specify here.  If left blank, the user will be taken
    | to the home route.
    |
    */

    'redirect_if_authenticated' => 'checkauth.app',

    /*
    |--------------------------------------------------------------------------
    | Enable HTML Views
    |--------------------------------------------------------------------------
    |
    | There are situations in which you may not want to display any views
    | when interacting with CheckAuth.  To return JSON instead of HTML,
    | turn this setting off. This cannot be done selectively.
    |
    */
    'views_enabled' => true,

    /*
    |--------------------------------------------------------------------------
    | Email Views
    |--------------------------------------------------------------------------
    |
    | String or array of views to use for emails
    |
    */

    'emails' => [
        'views' => [
            'welcome' => 'checkauth::emails.welcome',
            'activate' => 'checkauth::emails.activate',
            'reset' => 'checkauth::emails.reset'
        ]
    ]

];
