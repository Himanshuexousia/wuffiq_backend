<?php
namespace Checkmate\CheckAuth\Command;

use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Filesystem\Filesystem;


/**
 * Command class for CheckAuth
 *
 * @license http://www.checkmatedigital.com/licenses/checkauth
 *
 * @version 1.0.0
 */
class CheckAuthPublishCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkauth:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish views, routes, config file and public asset for CheckAuth';

    /**
     * The base path of the parent application
     *
     * @var string
     */
    private $appPath;

    /**
     * The path to the CheckAuth's src directory
     *
     * @var string
     */
    private $packagePath;

    /**
     * Create a new command instance.
     *
     * @param Filesystem $file
     */
    public function __construct(Filesystem $file)
    {
        parent::__construct();

        // DI Member Assignment
        $this->file = $file;

        // Set Application Path
        $this->appPath = app_path();

        // Set the path to the  Package namespace root
        $this->packagePath = __DIR__ . '/../..';
    }

    /**
     * This trait allows us to easily check the current environment
     */
    use ConfirmableTrait;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // warn the user if the app is in production!
        if (!$this->confirmToProceed('Application In Production!')) {
            return;
        }

        $this->cleanLaravelAuth();

        $this->publishCheckAuthConfig();

        $this->publishSentinelConfig();

        $this->publishHashidsConfig();

        if (config('checkauth.publish_routes')) {
            $this->publishRoutes();
        }

        $this->publishViews();

        $this->publishAssets();

        $this->publishTranslations();

        $this->publishMigrationsSeeds();

        $this->publishDb();

        $this->info('CheckAuth is now ready to use!');
    }

    private function cleanLaravelAuth(){

        $answer = $this->confirm('CheckAuth will erase Laravel basic authentication (migrations and controllers) to avoid conflict, proceed?');

        if (!$answer) {
            return;
        }

        $files = [
            '/database/migrations/2014_10_12_000000_create_users_table.php',
            '/database/migrations/2014_10_12_100000_create_password_resets_table.php',
            '/app/Http/Controllers/Auth/AuthController.php',
            '/app/Http/Controllers/Auth/PasswordController.php',
        ];
        foreach ($files as $file) {
            if (file_exists(base_path($file))) {
                $this->file->delete(base_path($file));
                $this->info('Removed File: ' . $file);
            }
        }

        $this->info('Auth migrations and controllers deleted');
    }

    /**
     * Publish the CheckAuth Config file
     */
    private function publishCheckAuthConfig()
    {

        $source      = $this->packagePath . '/src/config/checkauth.php';
        $destination = base_path() . '/config/checkauth.php';

        if ($this->file->isFile($destination)) {
            $answer = $this->confirm('CheckAuth config has already been published. Do you want to overwrite?');

            if (!$answer) {
                return;
            }
        }

        $this->file->copy($source, $destination);

        $this->info('CheckAuth configuration file published.');
    }

    /**
     * Publish the Sentinel Config file
     */
    private function publishSentinelConfig()
    {

        $source      = $this->packagePath . '/src/config/cartalyst.sentinel.php';
        $destination = base_path() . '/config/cartalyst.sentinel.php';

        if ($this->file->isFile($destination)) {
            $answer = $this->confirm('Sentinel config has already been published. Do you want to overwrite?');

            if (!$answer) {
                return;
            }
        }

        $this->file->copy($source, $destination);

        $this->info('Cartalyst/Sentinel configuration file published.');
    }

    /**
     * Publish the config file for Vinkla/Hashids
     */
    public function publishHashidsConfig()
    {
        $source      = $this->packagePath . '/src/config/hashids.php';
        $destination       = base_path() . '/config/hashids.php';

        if ($this->file->isFile($destination)) {
            $answer = $this->confirm('Hashid Config file has already been published. Do you want to overwrite?');

            if (!$answer) {
                return;
            }
        }

        $this->file->copy($source, $destination);

        $this->info('Vinkla/Hashids configuration file published.');
    }

    /**
     * Publish routes on app/http
     */
    public function publishRoutes()
    {
        $source      = $this->packagePath . '/src/Http/routes.php';
        $destination       = base_path() . '/app/Http/authentication_routes.php';

        if ($this->file->isFile($destination)) {
            $answer = $this->confirm('authentication_routes.php already been published in app/Http. Do you want to overwrite?');

            if (!$answer) {
                return;
            }
        }

        $this->file->copy($source, $destination);

        $this->info('Checkauth routes routes file published.');
    }

    /**
     * Publish the Views
     */
    private function publishViews()
    {
        $source      = $this->packagePath . '/resources/views';
        $destination = base_path() . '/resources/views/vendor/checkauth';

        if ($this->file->isDirectory($destination)) {
            $answer = $this->confirm('Views have already been published. Do you want to overwrite?');

            if (!$answer) {
                return;
            }
        }

        $this->file->copyDirectory($source, $destination);

        $this->info('Views published.');
    }

    /**
     * Publish the assets needed for a specified theme.
     */
    private function publishAssets()
    {

        $source      = $this->packagePath . '/resources/vendor';
        $destination = public_path('vendor');

        if ($this->file->isDirectory($destination)) {
            $answer = $this->confirm('Public Assets have already been published at laravel public/vendor folder. Some of they can be overwrited, Do you want to proceed?');

            if (!$answer) {
                return;
            }
        }

        $this->file->copyDirectory($source, $destination);

        $this->info('Assets published.');
    }

    /**
     * Publish language files
     */
    private function publishTranslations()
    {

        $source      = $this->packagePath . '/resources/lang/';
        $destination = base_path('/resources/lang/vendor/checkauth/');

        if ($this->file->isDirectory($destination)) {
            $answer = $this->confirm('Translations have already been published in resources/lang folder. Do you want to overwrite?');

            if (!$answer) {
                return;
            }
        }

        $this->file->copyDirectory($source, $destination);

        $this->info('CheckAuth language files published.');
    }


    /**
     * Copy and run migration and seeder files
     */
    private function publishMigrationsSeeds()
    {
        if($this->confirm('Would you like to publish the migration and seeder files?')){
            $source      = $this->packagePath . '/database/migrations/';
            $destination = base_path('/database/migrations');
            $this->file->copyDirectory($source, $destination);

            $source      = $this->packagePath . '/database/seeds/';
            $destination = base_path('/database/seeds');
            $this->file->copyDirectory($source, $destination);

            $this->info('Migration and seed files published.');
        }

    }

    /**
     * Copy and run migration and seeder files
     */
    private function publishDb()
    {

        if ($this->confirm('Would you like to update your database with checkauth tables?')) {
            $this->call('migrate:install');
            //$this->call('migrate:reset');
            //$this->call('migrate', ['--seed' => true]);
            $this->call('migrate:refresh'); //reset and re-run all your migrations
            $this->call('db:seed');

            // Notify action completion
            $this->info('database updated.');
        }

    }
}