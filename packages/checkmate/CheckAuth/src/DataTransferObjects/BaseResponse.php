<?php

namespace Checkmate\CheckAuth\DataTransferObjects;

/**
 * BaseResponse is a Data transfer object that carries data between processes.
 *
 * @license http://www.checkmate.com/licenses/checkauth
 *
 * @version 1.0.0
 */
class BaseResponse
{
    protected $payload;
    protected $message;
    protected $success;
    protected $error = false;

    /**
     * @param       $message
     * @param array $payload
     */
    public function __construct($message, array $payload = null)
    {
        $this->message = $message;
        $this->payload = $payload;
    }

    /**
     * @return mixed
     */
    public function isSuccessful()
    {
        return $this->success;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return array
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @return boolean
     */
    public function isError()
    {
        return $this->error;
    }
}
