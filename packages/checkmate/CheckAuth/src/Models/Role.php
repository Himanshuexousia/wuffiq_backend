<?php

namespace Checkmate\CheckAuth\Models;

use Cartalyst\Sentinel\Roles\EloquentRole;
use Hashids;


/**
 * Role Class
 *
 * @license http://www.checkmate.com/licenses/checkauth
 *
 * @property $name
 * @property $permissions
 * @property $slug
 * @property $id

 *
 * @version 1.0.0
 */
class Role extends EloquentRole
{
    /**
     * Use a mutator to derive the appropriate hash for this group
     *
     * @return mixed
     */
    public function getHashAttribute()
    {
        //return Hashids::encode($this->attributes['id']);
        return Hashids::encode($this->getKey());
    }
}
