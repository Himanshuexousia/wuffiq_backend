<?php

namespace Checkmate\CheckAuth\Repositories\Session;

use Checkmate\CheckAuth\DataTransferObjects\BaseResponse;

interface CheckAuthSessionRepositoryInterface
{
    /**
     * Store a newly created resource in storage.
     *
     * @param array $data
     *
     * @return BaseResponse
     */
    public function store($data);

    /**
     * Store a newly created oauth session in storage.
     *
     * @param array $data
     *
     * @return BaseResponse
     */
    public function oauthStore($data);

    /**
     * Remove the specified resource from storage.
     *
     * @return BaseResponse
     */
    public function destroy();

}
