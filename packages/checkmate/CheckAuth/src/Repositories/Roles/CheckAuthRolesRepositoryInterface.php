<?php
namespace Checkmate\CheckAuth\Repositories\Roles;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Response;
use Checkmate\CheckAuth\Models\User;

interface CheckAuthRolesRepositoryInterface
{
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($data);
    
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id);

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id);

    /**
     * Return a specific role by a given id
     * 
     * @param  integer $id
     * @return Role
     */
    public function retrieveById($id);

    /**
     * Return a specific role by a given name
     * 
     * @param  string $name
     * @return Role
     */
    public function retrieveByName($name);

    /**
     * Return all the registered roles
     *
     * @return Collection
     */
    public function all();

}
