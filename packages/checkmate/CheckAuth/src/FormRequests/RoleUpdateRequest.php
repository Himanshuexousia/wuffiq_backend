<?php

namespace Checkmate\CheckAuth\FormRequests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Checks if user have authorization and if the input value given is according the rules.
 *
 * @license http://www.checkmate.com/licenses/checkauth
 *
 * @version 1.0.0
 */
class RoleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:4'
        ];
    }
}
