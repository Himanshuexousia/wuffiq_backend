<?php

namespace Checkmate\CheckAuth\FormRequests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Checks if user have authorization and if the input values given are according the rules.
 *
 * @license http://www.checkmate.com/licenses/checkauth
 *
 * @version 1.0.0
 */
class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $additional_fields_rules = config('checkauth.additional_user_fields');
        $rules = [
            'first_name' => 'alpha|required|min:2|max:254',
            'last_name' => 'alpha|required|min:2|max:254',
            'avatar' => 'image',
            'email' => 'required|unique:users|min:4|max:254|email',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required'
        ];
        if(is_array($additional_fields_rules))
        $rules = array_merge($rules, $additional_fields_rules);

        return $rules;
    }
}
