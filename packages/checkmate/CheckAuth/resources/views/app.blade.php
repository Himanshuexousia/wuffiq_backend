<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>
        @section('htmlHeaderTitle')
            Backoffice
        @show
    </title>

    @include('checkauth::partials.styles')

    <style>
        body {
            padding-top: 5rem;
        }
        .starter-template {
            padding: 3rem 1.5rem;
            text-align: center;
        }
    </style>
</head>

<body>

<nav class="navbar navbar-fixed-top navbar-dark bg-inverse">
    <div class="container">
        <div class="pull-xs-left">
            <a class="navbar-brand pull-left" href="{{route('checkauth.app')}}">Auth</a>
        </div>
        <div class="pull-xs-right">
            <ul class="nav navbar-nav pull-right">
                <li class="nav-item {{ Request::is('checkauth-app')? 'active': '' }}">
                    <a class="nav-link" href="{{route('checkauth.app')}}">Home</a>
                </li>
                @if($user = CheckAuth::getUser())
                    @if(CheckAuth::inRole('admins') || CheckAuth::inRole('tester') )
                        <li class="nav-item {{ Request::is('users')? 'active': '' }}">
                            <a class="nav-link" href="{{ route('checkauth.users.index') }}">{{trans_choice('checkauth::pages.users', 10)}}</a>
                        </li>
                        <li class="nav-item {{ Request::is('roles')? 'active': '' }}">
                            <a class="nav-link" href="{{ route('checkauth.roles.index') }}">{{trans_choice('checkauth::pages.roles', 2)}}</a>
                        </li>
                    @endif
                    <li class="nav-item {{ Request::is('profile')? 'active': '' }}">
                        <a class="nav-link" href="{{ route('checkauth.profile.show') }}">{{trans('checkauth::pages.profile')}}</a>
                    </li>
                    <li class="nav-item {{ Request::is('logout')? 'active': '' }}">
                        <a class="nav-link" href="{{ route('checkauth.logout') }}">{{trans('checkauth::pages.logout')}}</a>
                    </li>
                @else
                    <li class="nav-item {{ Request::is('register')? 'active': '' }}">
                        <a class="nav-link" href="{{route('checkauth.register.form')}}">{{ trans('checkauth::pages.register') }}</a>
                    </li>
                    <li class="nav-item {{ Request::is('login')? 'active': '' }}">
                        <a class="nav-link" href="{{route('checkauth.login')}}">{{ trans('checkauth::pages.login') }}</a>
                    </li>
                @endif
                <li class="nav-item {{ Request::is('documentation/checkauth')? 'active': '' }}">
                    <a class="nav-link" href="{{route('checkauth.documentation')}}" target="_blank">{{ trans('checkauth::pages.documentation') }}</a>
                </li>

            </ul>
        </div>
    </div>
</nav>



<div class="container">
    <div class="row">
        @include('checkauth::partials.notifications')
    </div>

    <!-- Your Page Content Here -->
    @section('checkauth::main-content')
        <div class="starter-template">
            <h1>Auth</h1>
            <p class="lead">{{ trans('checkauth::pages.helloworld') }}</p>
            @if($user = CheckAuth::getUser())
                <p>{{ trans('checkauth::pages.hello') }} {{$user['first_name']}}!</p>
                <a href="{{route('checkauth.logout')}}"><button type="button" class="btn btn-secondary">{{ trans('checkauth::pages.logout') }}</button></a>
            @else
                <div class="alert alert-warning alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Warning!</strong> No user is logged in
                </div>
                <a href="{{route('checkauth.login')}}"><button type="button" class="btn btn-secondary">{{ trans('checkauth::pages.login') }}</button></a>
            @endif

        </div>
    @show

</div><!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
@include('checkauth::partials.scripts')

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="{{ asset('vendor/bootstrap/js/ie10-viewport-bug-workaround.js') }}"></script>
@yield('checkauth::page-scripts')
</body>
</html>