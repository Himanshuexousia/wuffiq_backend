@extends('checkauth::sessions.logoff')
{{-- Web site Title --}}
@section('htmlHeaderTitle')
    Reset your password
@endsection

{{-- Content --}}
@section('checkauth:grid-class')
    col-md-offset-4 col-md-4 col-md-offset-4
@stop

@section('checkauth:card-title')
    Reset Your Password
@stop

@section('checkauth::card-block')
    @include('checkauth::partials.notifications')
    <form method="POST" action="{{ route('checkauth.reset.password', [$hash, $code]) }}" accept-charset="UTF-8">
        <div class="card-block">
            <div class="form-group {{ ($errors->has('password')) ? 'has-error' : '' }}">
                <input class="form-control" placeholder="New Password" name="password" type="password" />
                {{ ($errors->has('password') ? $errors->first('password') : '') }}
            </div>

            <div class="form-group {{ ($errors->has('password_confirmation')) ? 'has-error' : '' }}">
                <input class="form-control" placeholder="Confirm Password" name="password_confirmation" type="password" />
                {{ ($errors->has('password_confirmation') ? $errors->first('password_confirmation') : '') }}
            </div>
        </div>
        <div class="card-footer">
            <input name="_token" value="{{ csrf_token() }}" type="hidden">
            <div class='btn-group pull-right padding'>
                <input class="btn btn-primary" value="Reset Password" type="submit">
            </div>

        </div>
    </form>
@stop

@section('checkauth::card-footer')
@stop

@include('checkauth::partials.scripts')
@section('checkauth::scripts')
@stop