<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Session Repository Messages
    |--------------------------------------------------------------------------
    */

    "invalid"   => "Usuário ou Senha inválidos.",

    "notactive" =>  "Você ainda não ativou sua conta. <a href=':url' class='alert-link'>Reenviar Email de Ativação?</a>",

    "suspended" => "Sua conta foi suspensa temporariamente.",

    "banned"    => "Você está banido.",

    "oathRegister" => "Confirme suas informações e forneça uma senha. Você poderá se logar com o :provider ou com seu e-mail e senha no futuro",

    "change_providers" => "Este e-mail já está associado a um outro serviço de autenticação Oauth. Você gostaria de alterar para o :provider? <a href=':url' class='alert-link'>Clique aqui para continuar</a>",

    "add_provider" => "Você gostaria de se logar com o :provider? <a href=':url' class='alert-link'>Clique aqui para continuar</a>",

    "register_provider" => "Confirme seus dados e escolha uma senha. Você poderá logar com o :provider ou com seu e-mail e senha no futuro",

    "provider_changed" => "O provedor de autenticação foi alterado para o :provider! Agora você pode acessar clicando no botão 'Logar com :provider'",

);