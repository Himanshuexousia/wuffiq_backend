<!doctype html>
<html lang="en-us">
<head>
    <meta charset="utf-8">
    <title>CrudGen - Crud generator for Laravel with Interface and Command options</title>

    <meta name="description" content="CrudGen - Crud generator for Laravel with Interface and Command options">
    <meta name="author" content="checkmate digital">
    <meta name="copyright" content="checkmatedigital.com">

    <link rel="stylesheet" href="{{ asset('vendor/documenter/css/documenter_style.css') }}" media="all">

    <script src="{{ asset('vendor/jquery/jquery.js') }}"></script>
    <script src="{{ asset('vendor/jquery.scrollTo/jquery.scrollTo.min.js') }}"></script>

    <script src="{{ asset('vendor/documenter/js/documenter_script.js') }}"></script>
    <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js?autoload=true&amp;skin=sons-of-obsidian&amp;lang=html" defer="defer"></script>
</head>
<body>
<div id="documenter_sidebar">
    <a href="#start" id="logo"><img src="{{ asset('vendor/checkauth/img/CheckMateLogo.png') }}"></a>
    <ul id="documenter_nav">
        <li><a class="current" href="#start" title="Start">Start</a></li>
        <li><a href="#introduction" title="Introduction">What's in the box</a></li>
        <li><a href="#features" title="Features">Features</a></li>
        <li><a href="#server" title="Server requirements">Server requirements</a></li>
        <li><a href="#installation" title="Installation">Installation</a></li>
        <li><a href="#configuration" title="Configuration">Configuration</a></li>
        <li><a href="#interface" title="Interface">Interface</a></li>
        <li><a href="#command" title="Command line">Command Line</a></li>
        <li><a href="#changelog" title="Changelog">Changelog</a></li>
        <li><a href="#support" title="Support">Support</a></li>
        <li><a href="#license" title="License">License</a></li>
    </ul>
    <div id="documenter_copyright">Copyright<br><a href="http://www.checkmatedigital.com">Checkmate Digital</a>
    </div>
</div>
<div id="documenter_content">
    <section id="start">
        <img src="{{asset('vendor/checkauth/img/authLogo.png')}}">
        <h1>CrudGen</h1>
        <h2>Crud generator for Laravel with Interface and Command options</h2>
        <hr>
        <ul>
            <li>Created: 01/01/2016</li>
            <li>latest Update: 08/08/2016</li>
            <li>Developer: <a href="{{asset('http://www.checkmatedigital.com')}}" target="_blank">Checkmate Digital</a></li>
        </ul>
        <p></p>
    </section>

    <section id="introduction">
        <div class="page-header"><h3>What's in the box</h3><hr class="notop"></div>
        <p>Crudgen is a CRUD (CREATE, UPDATE and DELETE) package for Laravel 5 that creates Controlers, Views, Routes, Migrations and Menu for your application with very simple steps.</p>
    </section>
    <section id="features">
        <div class="page-header"><h3>Features</h3><hr class="notop"></div>
        <ul>
            <li>Easy to use interface</li>
            <li>Command line options</li>
        </ul>
    </section>
    <section id="server">
        <div class="page-header"><h3>Server requirements</h3><hr class="notop"></div>
        <ul>
            <li>PHP >= 5.5.9</li>
            <li>OpenSSL PHP Extension</li>
            <li>PDO PHP Extension</li>
            <li>Mbstring PHP Extension</li>
            <li>Tokenizer PHP Extension</li>
            <li>FileInfo PHP Extension</li>
            <li>MySql or SQLite database</li>
        </ul>
    </section>
    <section id="installation">
        <div class="page-header"><h3>Installation</h3><hr class="notop"></div>
        <p>	<strong>Step 1, extract files:&nbsp;</strong>Extract 'packages' folder from the checkauth.zip file to your Laravel's app root folder. Add dependencies to your composer.json file in your app root folder.</p>
        <pre class="prettyprint linenums:14">
"require": {
    ...
    "cartalyst/sentinel": "2.0.*",
    "vinkla/hashids": "~2.0",
    "laravel/socialite": "~2.0",
    "intervention/image": "~2.3.5",
    "anhskohbo/no-captcha": "2.*"
},
      </pre>
        <p>and in psr-4 add:</p>
        <pre class="prettyprint linenums:36">
"psr-4": {
    "App\\": "app/",
    "Checkmate\\CheckAuth\\": "packages/checkmate/CheckAuth/src"
}
        </pre>
        <p>Run the command above in your shell command prompt and wait untill required libraries are installed:</p>
        <pre class="prettyprint">
composer update
        </pre>

        <p>Go to: {{url('https://www.google.com/recaptcha/intro/index.html')}} and Get reCaptcha secret and site keys, this will allow recapcha to protect your login form</p>
        <p>Then you just need to add to your .env file the following parameters (dont forget to replace your secret-key and site-key from the website above.</p>
        <pre class="prettyprint">
NOCAPTCHA_SECRET=[secret-key]
NOCAPTCHA_SITEKEY=[site-key]
        </pre>
        <p><strong>Important:</strong> After any change on config or env file you must use the command above to clear Laravel's cache</p>
        <pre class="prettyprint">
php artisan config:cache
        </pre>

        <p>	<strong>Step 2, Configure environment:&nbsp;</strong>Open file app.php in the folder 'web/config', and change 'http://localhost' to your domain URL (ex: http://www.mydomain.com) </p>
        <!--cannot use indentation inside pre because it will mass with code-prettify-->
        <pre class="prettyprint linenums:31">
/*
|--------------------------------------------------------------------------
| Application URL
|--------------------------------------------------------------------------
|
| This URL is used by the console to properly generate URLs when using
| the Artisan command line tool. You should set this to the root of
| your application so that it is used when running Artisan tasks.
|
*/

'url' => 'http://localhost',
        </pre>
        <p>	<strong>Step 3, Setup database:&nbsp;</strong>Open 'config/database.php' and choose your database connection, for example: 'sqlite' or 'mysql'
        <pre class="prettyprint linenums::18">
/*
|--------------------------------------------------------------------------
| Default Database Connection Name
|--------------------------------------------------------------------------
|
| Here you may specify which of the database connections below you wish
| to use as your default connection for all database work. Of course
| you may use many connections at once using the Database library.
|
*/

'default' => env('DB_CONNECTION', 'sqlite'),
        </pre>

        <p>Change database parameters to mach your database environment. For example:</p>

        <pre class="prettyprint linenums:55">
'mysql' => [
    'driver'    => 'mysql',
    'host'      => env('DB_HOST', 'localhost'),
    'database'  => env('DB_DATABASE', 'forge'),
    'username'  => env('DB_USERNAME', 'forge'),
    'password'  => env('DB_PASSWORD', ''),
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
    'strict'    => false,
],
        </pre>
        <p>if you use mySql and have Cpanel you can check this tutorial on <a href="{{asset('https://www.youtube.com/watch?v=nfM0xNwkAMA')}}" target="_blank"> how to create a MySql database</a></p>
        <p>	<strong>Step 4:&nbsp;</strong> Open 'config/mail.php' and configure your e-mail provider parameters. Auth uses this to send registration confirmation, resend passwords, etc.</p>
        <p>	<strong>Step 5:&nbsp;</strong>In 'config/app.php' include Checkauth service provider at the end of the providers list</p>
        <pre class="prettyprint linenums:124">
/*
|--------------------------------------------------------------------------
| Autoloaded Service Providers
|--------------------------------------------------------------------------
|
| The service providers listed here will be automatically loaded on the
| request to your application. Feel free to add your own services to
| this array to grant expanded functionality to your applications.
|
*/

'providers' => [
        ...
        Checkmate\CheckAuth\Providers\CheckAuthServiceProvider::class,
        </pre>
        <p>	<strong>Step 6, publish checkauth:</strong>Publish Checkauth's views, assets, config files and migrations into your laravels app. In your shell type:</p>
        <pre class="prettyprint">
php artisan checkauth:publish
        </pre>
        <p>	<strong>Step 7, optional database update (if you skip this on step 6) :</strong>If you chose to not update your database during step 6, you can update ir now using the above command:</p>
        <pre class="prettyprint">
php artisan migrate:refresh --seed
        </pre>
        <p>Follow the installation instructions untill the end.</p>
        <p>	<strong>Step 8, file permissions:&nbsp;</strong> You may need to configure some permissions. Directories within the storage and the bootstrap/cache directories should be writable by your web server or Laravel will not run.</p>
        <p>	<strong>Last step, Security: </strong> The next thing you should do after installing checkauth is set your application key to a random string. Typically, this string should be 32 characters long. The key can be set in the .env environment file located in 'web' folder. If your not familiar with Artisan command line 'php artisan key:generate', you can go on this site and generate a key <a href="{{asset('https://darkvoice.dyndns.org/wlankeygen/')}}" target="_blank">here.</a> Important! Character set has to be 0-9, A-Z, a-z (ASCII 48-57, 65-90, 97-122), Lenght 32. Then click generate button, then copy it to your file. <strong>If the application key is not set, your user sessions and other encrypted data will not be secure!</strong> </p>

    </section>

    <section id="configuration">
        <div class="page-header"><h3>Configuration</h3><hr class="notop"></div>
        <p>Crudgen have a config file located at 'config/crudgen.php', that allow tou to customize options to suit your needs.</p>
    </section>
    <section id="interface">
        <div class="page-header"><h3>Interface</h3><hr class="notop"></div>
        <p>You can use command line to generate your cruds (see bellow) or use the interface.</p>
        <p>Interface can be accessed by entering this url: yoursiteurl.com/admin/crudgen/generator</p>
        <b>Important: the url will be defined by the config file parameter 'prefix'. By default prefix is 'admin'. If you change prefix in configuration, the url will change. See example bellow:</b>
        <p>if you change the parameter 'prefix' in 'config/crudgen.php' from 'admin' to 'private'. Your Interface address will be: yoursiteurl.com/<b>private</b>/crudgen/generator</p>
        <p>Access to generator's interface will depend on middleware permission. The default permission is <b>'sentinel.role:admin'</b>. It means that only users loged in with admin privileges will access this interface.</p>
        <img src="{{asset('vendor/crudgen/images/documentation/Crud_Generator_Interface.png')}}" style="border: 1px solid #000;">
        <p><b>Class name:</b> Your class name. Ex: 'post'</p>
        <p><b>Route Group:</b> Route prefix for your class. Ex: if you choose 'admin' prefix, the class url will be: yourwebsite.com/admin/className</p>
        <p><b>MiddleWare:</b> The permissions necessary to access the class CRUD. If your using Infinity or CheckAuth, check  <a href="http://infinity.checkmatedigital.com/documentation#routing" target="_blank">Infinity documentation</a> for available middleware. If you are using standard Laravel authentication, <a href="https://laravel.com/docs/master/middleware" target="_blank">read this</a>  </p>
        <p><b>Field name:</b> Class property. ex: name, phone, address...</p>
        <p><b>Field type:</b> Class type. ex: string, varchar, integer...</p>
        <p>You can add fields by clicking on 'Add field' button</p>
        <p>After you fill the form and press 'Generate', Crudgen will create all files necessary to your crud to work.</p>
    </section>
    <section id="command">
        <div class="page-header"><h3>Command Line</h3><hr class="notop"></div>
        <p>You can use CMD to run Crudgen commands:</p>
        <pre class="prettyprint">
php artisan crud:generate Posts --fields="title#string, body#text"
        </pre>
        <p>You can also easily include route, set primary key, set views directory etc through options --route, --pk, --view-path as belows:</p>
        <pre class="prettyprint">
php artisan crud:generate Posts --fields="title#string#required, body#text#required_with:title|alpha_num" --route=yes --pk=id --view-path="admin" --namespace=Admin --route-group=admin
        </pre>
        <p>Options:</p>

        --fields : Fields name for the form & model.<br>
        --route : Include Crud route to routes.php? yes or no.<br>
        --pk : The name of the primary key.<br>
        --view-path : The name of the view path.<br>
        --namespace : Namespace of the controller.<br>
        --route-group : Prefix of the route group.<br>

        <p>Other commands</p>
        <p>controller generator:</p>
        <pre class="prettyprint">
php artisan crud:controller PostsController --crud-name=posts --model-name=Post --view-path="directory" --route-group=admin
        </pre>

        <p>model generator:</p>
        <pre class="prettyprint">
php artisan crud:model Post --fillable="['title', 'body']"
        </pre>

        <p>migration generator:</p>
        <pre class="prettyprint">
php artisan crud:migration posts --schema="title#string, body#text"
        </pre>

        <p>view generator:</p>
        <pre class="prettyprint">
php artisan crud:view posts --fields="title#string, body#text" --view-path="directory" --route-group=admin
        </pre>

        <p>By default, the generator will attempt to append the crud route to your routes.php file. If you don't want the route added, you can use the option '--route=no'.</p>

        <h3>Supported Field Types</h3>

        <p>These fields are supported for migration and view's form:</p>

        <p>string, char, varchar, password, email, date, datetime, time, timestamp, text, mediumtext, longtext, json, jsonb, binary, number, integer, bigint, mediumint, tinyint,
        smallint, boolean, decimal, double, float, enum</p>

        <h3>Enum Type Field</h3>

        <p>For generating enum type field follow the instructions:</p>

        Write a command like below.

        <pre class="prettyprint">
php artisan crud:generate Posts --fields="title#string#required, body#text, category#enum"
        </pre>

        <p>Modify your migration like below.</p>

        <pre class="prettyprint">
$table->enum('category', ['technology', 'tips', 'health']);
        </pre>

        <p>Add EnumTrait to your model.</p>

        <pre class="prettyprint">
class Post extends Model
{
use EnumTrait;
        </pre>

        <h3>Custom Generator's Stub Template</h3>

        <p>You can customize the generator's stub files/templates to achieve your need. Make sure you've published package's assets.</p>

        <pre class="prettyprint">
php artisan crudgen:publish
        </pre>

        <p>
            Turn on custom_template support on /config/crudgenerator.php
        </p>

        <pre class="prettyprint">
'custom_template' => true,
        </pre>

        <p>
            From the directory /resources/crud-generator/ you can modify or customize the stub files.
        </p>

    </section>
    <section id="roles">
        <div class="page-header"><h3>Roles and permissions</h3><hr class="notop"></div>
        <h3>Permissions</h3>
        <p>Permissions can be broken down into two types and two implementations. Depending on the used implementation, these permission types will behave differently.</p>
        <ul>
            <li>Role Permissions</li>
            <li>User Permissions</li>
        </ul>
        <p>
            <em>Standard</em>
            - This implementation will give the user-based permissions a higher priority and will override role-based permissions. Any permissions granted/rejected on the user will always take precendece over any role-based permissions assigned.
        </p>
        <p>
            <em>Strict</em>
            - This implementation will reject a permission as soon as one rejected permission is found on either the user or any of the assigned roles. Granting a user a permission that is rejected on a role he is assigned to will not grant that user this permission.
        </p>
        <p>Role-based permissions that define the same permission with different access rights will be rejected in case of any rejections on any role.</p>
        <p>If a user is not assigned a permission, the user will inherit permissions from the role. If a user is assigned a permission of false or true, then the user's permission will override the role permission.</p>
        <blockquote>
            <p>
                <strong>Important:</strong> The permission type is set to <strong>StandardPermissions</strong> by default. It can be changed on the <strong>config</strong>
                file located at 'config/checkauth.php'.
            </p>
        </blockquote>
    </section>


    <section id="changelog">
        <div class="page-header"><h3>Changelog</h3><hr class="notop"></div>
        <p>
            <strong>v1.0 (08/08/2016) :</strong></p>
        <ul>
            <li>
                initial release</li>
        </ul>
    </section>
    <section id="support">
        <div class="page-header"><h3>Support</h3><hr class="notop"></div>
        <p>	If you need any help regarding the installation of the script please create a support&nbsp; ticket from here with help topic &quot;CodeCanyon Support &quot;</p>
        <p>	<a href="{{asset('http://support.checkmatedigital.com/checkauth')}}" target="_blank">Checkmate Support Center</a></p>
        <p>	Our tech support team will get back to you within the nex 48 hours.</p>
    </section>

    <section id="license">
        <div class="license_list">
            <div class="page-header"><h3>License</h3><hr class="notop"></div>
            <p>When you buy this project (or item) you're agreeing with the license above:</p>
            <h3>General terms</h3>
            <ul>
                <li>
                    <p><span >1.</span>This license grants you, the purchaser, an ongoing, non-exclusive, worldwide license to make use of the digital work you have purchased. Read the rest of this license for the details that apply to your use of the Item.</p>
                </li>
                <li>
                    <p><span>2.</span>You are licensed to use the Item to create one single End Product for yourself or for one client (a "single application"), and the End Product can be distributed for Free.</p>
                </li>
                <li>
                    <p><span>3.</span>An End Product is one of the following things, both requiring an application of skill and effort.</p>

                    <ul>
                        <li>
                            <p><span>(a)</span>the End Product is a customised implementation of this project.</p>
                        </li>
                        <li>
                            <p><span>(b)</span> the End Product is a work that incorporates the Item as well as other things, so that it is larger in scope and different in nature than the Item.</p>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <h3>Things you can do with this item</h3>

        <ul>
            <li>
                <p><span>4.</span>You can create one End Product for a client, and you can transfer that single End Product to your client for any fee. This license is then transferred to your client.</p>
            </li>
            <li>
                <p><span>5.</span>You can modify or manipulate the Item. You can combine the Item with other works and make a derivative work from it. The resulting works are subject to the terms of this license. You can do these things as long as the End Product you then create is one that's permitted under clause 3.</p>
            </li>
        </ul>

        <h3>Things you can&#39;t do with the item</h3>

        <ul>
            <li>
                <p><span>6.</span>You can't Sell the End Product, except to one client. (If you or your client want to Sell the End Product, you will need the Extended License.)</p>
            </li>
            <li>
                <p><span>7.</span>You can't re-distribute the Item as stock, in a tool or template, or with source files. You can't do this with an Item either on its own or bundled with other items, and even if you modify the Item. You can't re-distribute or make available the Item as-is or with superficial modifications. These things are not allowed even if the re-distribution is for Free.</p>
                <p class="license-bubble--below">For example: You can't purchase an HTML template, convert it to a WordPress theme and sell or give it to more than one client. You can't license an item and then make it available as-is on your website for your users to download.</p>
            </li>
            <li>
                <p><span>8.</span>You can't use the Item in any application allowing an end user to customise a digital or physical product to their specific needs, such as an "on demand", "made to order" or "build it yourself" application. You can use the Item in this way only if you purchase a separate license for each final product incorporating the Item that is created using the application.</p>

                <p class="license-bubble--below">Examples of "on demand", "made to order" or "build it yourself" applications: website builders, "create your own" slideshow apps, and e-card generators.</p>
            </li>
            <li>
                <p><span>9.</span>Although you can modify the Item and therefore delete unwanted components before creating your single End Product, you can't extract and use a single component of an Item on a stand-alone basis.</p>

                <p class="license-bubble--below">For example: You license a website theme containing icons. You can delete unwanted icons from the theme. But you can&#39;t extract an icon to use outside of the theme.</p>
            </li>
            <li>
                <p><span>10.</span>You must not permit an end user of the End Product to extract the Item and use it separately from the End Product.</p>
            </li>
            <li>
                <p><span>11.</span>You can't use an Item in a logo, trademark, or service mark.</p>
            </li>
        </ul>

        <h3>Other license terms</h3>

        <ul>

            <li>
                <p><span>12.</span>You can only use the Item for lawful purposes. Also, if an Item contains an image of a person, even if the Item is model-released you can't use it in a way that creates a fake identity, implies personal endorsement of a product by the person, or in a way that is defamatory, obscene or demeaning, or in connection with sensitive subjects.</p>
            </li>
            <li>
                <p><span>16.</span>Items that contain digital versions of real products, trademarks or other intellectual property owned by others have not been property released. These Items are licensed on the basis of editorial use only. It is your responsibility to consider whether your use of these Items requires a clearance and if so, to obtain that clearance from the intellectual property rights owner.</p>
            </li>
            <li>
                <p><span>17.</span>This license applies in conjunction with the <a href="http://codecanyon.net/legal/market">Envato Market Terms</a> for your use of Envato Market. If there is an inconsistency between this license and the Envato Market Terms, this license will apply to the extent necessary to resolve the inconsistency.</p>
            </li>
            <li>
                <p><span>18.</span>This license can be terminated if you breach it. If that happens, you must stop making copies of or distributing the End Product until you remove the Item from it.</p>
            </li>
            <li>
                <p><span>19.</span>The author of the Item retains ownership of the Item but grants you the license on these terms. This license is between the author of the Item and you.</p>
            </li>

        </ul>

        <h3>Credits</h3>

        <p>We built Crudgen on top of appzcoder/crud-generator project as a reference and with the help of other great gitHub projects above (Thanks for all!):</p>

        <h3>MIT License:</h3>

        <ul>
            <li>laravel/framework</li>
            <li>bootstrap/bootstrap</li>
            <li>laravelcollective/html</li>

        </ul>

        <h3>BSD3 License:</h3>
        <ul>
            <li>cartalyst/sentinel &copy;  2011-2015 Cartalyst LLC, All rights reserved.</li>
        </ul>

    </section>

</div>

</body>
</html>