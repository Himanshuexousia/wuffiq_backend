<?php

namespace App\Http\Controllers;
 
use illuminate\Http\Request;
 
use App\Http\Requests;
 
use DB;

use File;

use Hash;

use Auth;

use Response;


class AbcController extends Controller
{
	public function store123()
	{
		echo 'hg';
	}
	
	public function getToken()
	{
		return Response::json(array("status"=>"1","message"=>"CSRF Token","response"=>csrf_token()));
	}
	
	public function getHome(Request $request)
	{
		$userId=$this->sanitizeInput($request->input('userId'));
		$postsFrom=$this->sanitizeInput($request->input('postsFrom'));
		$countFrom=$this->sanitizeInput($request->input('count'));
		$latitude=$this->sanitizeInput($request->input('latitude'));
		$longitude=$this->sanitizeInput($request->input('longitude'));
		$distance=$this->sanitizeInput($request->input('distance'));
		$countTo=$countFrom+15;
		$var=array();
		$data=DB::table('uploads')->orderBy('time', 'desc')->get();
		$count=0;
		foreach($data as $dat)
		{
			if($this->isPostVisible($userId,$postsFrom,$dat->privacy,$dat->userId,$dat->id) && $this->postVisibleByDistance($latitude,$longitude,floatval($dat->latitude),floatval($dat->longitude),$distance) && $this->checkIfBlocked($userId,$dat->userId) && $this->checkIfprofilevalid($dat->userId) && $this->checkIfContentReported($userId,$dat->id))
			{
				$name=$this->getUserNameFromId($dat->userId);
				$var[]=array("imageId"=>$dat->id,"userId"=>$dat->userId,"userName"=>$name,"location"=>$dat->location,"latitude"=>floatval($dat->latitude),"longitude"=>floatval($dat->longitude),"caption"=>$dat->caption,"category"=>json_decode($dat->category),"time"=>intval(time()-$dat->time),"likes"=>$this->getLikesCount($dat->id),"views"=>$this->getviewsCount($dat->id),"liked"=>$this->checkIfLiked($userId,$dat->id),"comments"=>$this->getCommentsCount($dat->id));
			}
		}
		if($postsFrom=='1')
		{
			usort($var, function($a, $b) 
			{
                return $a['likes'] > $b['likes'] ? -1 : 1;
            }); 
		}
		$homeData=array();
		foreach($var as $row)
		{
			if($countFrom<=$count && $countTo>$count)
			{
			    $homeData[]=$row;
			}
			$count++;
		}
		$json=array("status"=>"1","message"=>"Home Page","response"=>$homeData);
		return Response::json($json);
	}
	
	public function checkIfContentReported($userid,$id)
	{
		if(empty($userid))
		{
			return 1;
		}
		else
		{
			$data=DB::table('reportedContent')->where('reportedBy',$userid)->where('uploadId',$id)->get();
			if(count($data))
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
	}
	
	public function checkIfprofilevalid($id)
	{
		$data=DB::table('users')->where('id',$id)->first();
		if(count($data))
		{
			if($data->deactivate=='1' || $data->banAdmin=='1' || $data->status=='0')
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}
	}
	
	public function checkIfBlocked($userId,$userId1)
	{
		$data=DB::Table("blockedUsers")->where('blockedBy',$userId)->where('blocked',$userId1)->get();
		if(count($data))
		{
			return 0;
		}
		else
		{
			$data1=DB::Table("blockedUsers")->where('blockedBy',$userId1)->where('blocked',$userId)->get();
		    if(count($data1))
		    {
			    return 0;
		    }
			else
			{
				return 1;
			}
		}
	}
	
	public function postVisibleByDistance($latitude,$longitude,$latitude1,$longitude1,$distance)
	{
		if(!empty($latitude) && !empty($longitude))
		{
		   $theta=$longitude - $longitude1;
           $dist=sin(deg2rad($latitude)) * sin(deg2rad($latitude1)) +  cos(deg2rad($latitude)) * cos(deg2rad($latitude1)) * cos(deg2rad($theta));
           $dist=acos($dist);
           $dist=rad2deg($dist);
           $miles=$dist * 60 * 1.1515;
           $dist=$miles * 1.609344; 
		   if($distance=='0')
		   {
			   return 1;
		   }
		   else if($dist<=$distance)
		   {
			   return 1;
		   }
		   else
		   {
			   return 0;
		   }
		}
		else
		{
			return 1;
		}
	}
	
	public function checkIfLiked($userId,$id)
	{
		if($userId!='')
		{
			$data=DB::table('uploadLikes')->where('userId',$userId)->where('uploadId',$id)->get();
			if(count($data))
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}
	
	public function getCommentsCount($id)
	{
		$data=DB::table('uploadComments')->where('uploadId',$id)->get();
		return count($data);
	}
	
	public function getLikesCount($id)
	{
		$data=DB::table('uploadLikes')->where('uploadId',$id)->get();
		return count($data);
	}
	
	public function getviewsCount($id)
	{
		$data=DB::table('uploadViews')->where('uploadId',$id)->get();
		return count($data);
	}
	
	public function getUserNameFromId($id)
	{
		$check=DB::table('users')->where('id',$id)->first();
		return $check->username;
	}
	
	public function likeUpload(Request $request)
	{
		$accessToken =  $this->sanitizeInput($request->header('Authorization'));
		$sessionTime =  $this->sanitizeInput($request->input('sessionTime'));
		$userId =  $this->sanitizeInput($request->input('userId'));
		$uploadId =  $this->sanitizeInput($request->input('uploadId'));
		if(!empty($accessToken) && !empty($sessionTime) && !empty($userId) && !empty($uploadId))
		{
			if($this->checkAuthorization($accessToken,$sessionTime,$userId))
			{
				$data=DB::table('uploadLikes')->where('userId',$userId)->where('uploadId',$uploadId)->get();
				if(count($data))
				{
					$check=DB::table('uploadLikes')->where('userId',$userId)->where('uploadId',$uploadId)->delete();
					if($check)
					{
						$json=array("status"=>"1","message"=>"Action Successful","response"=>array());
					}
					else
					{
						$json=array("status"=>"0","message"=>"Some Error Occured","response"=>array());
					}
				}
				else
				{
					$check=DB::table('uploadLikes')->insert(["userId"=>$userId,"uploadId"=>$uploadId,"time"=>time()]);
					if($check)
					{
						$json=array("status"=>"1","message"=>"Action Successful","response"=>array());
					}
					else
					{
						$json=array("status"=>"0","message"=>"Some Error Occured","response"=>array());
					}
				}
			}
			else
			{
				$json=array("status"=>"10","message"=>"Authorization Failed","response"=>array());
			}
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	public function viewsUpload(Request $request)
	{
		$accessToken =  $this->sanitizeInput($request->header('Authorization'));
		$sessionTime =  $this->sanitizeInput($request->input('sessionTime'));
		$userId =  $this->sanitizeInput($request->input('userId'));
		$uploadId =  json_decode($this->sanitizeInput($request->input('uploadId')));
		if(!empty($accessToken) && !empty($sessionTime) && !empty($userId) && !empty($uploadId))
		{
			if($this->checkAuthorization($accessToken,$sessionTime,$userId))
			{
				foreach($uploadId as $id)
				{
				    $data=DB::table('uploadViews')->where('userId',$userId)->where('uploadId',$id)->get();
				    if(!count($data))
				    {
					    $check=DB::table('uploadViews')->insert(['userId'=>$userId,'uploadId'=>$id]);
				    }
				}
				 $json=array("status"=>"1","message"=>"Action Successful","response"=>array());
			}
			else
			{
				$json=array("status"=>"10","message"=>"Authorization Failed","response"=>array());
			}
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	public function isPostVisible($userId,$postsFrom,$privacy,$posteduserId,$imageID)
	{
		if($postsFrom=='1' || $postsFrom=='5' || $postsFrom=='7')
		{
			if($privacy=='2')
			{
				return 1;
			}
			else if($privacy=='1')
			{
				if($this->checkIfFollowed($userId,$posteduserId))
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
			else
			{
				if($userId==$posteduserId)
			    {
				    return 1;
			    }
			    else
			    {
				    return 0;
			    }
			}
		}
		else if($postsFrom=='2')
		{
			if($userId==$posteduserId)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else if($postsFrom=='3')
		{
			if($this->checkIfFollowed($posteduserId,$userId))
			{
				if($privacy=='2')
				{
					return 1;
				}
				else if($privacy=='1')
				{
					if($this->checkIfFollowed($userId,$posteduserId))
				    {
					    return 1;
				    }
				    else
				    {
					    return 0;
				    }
				}
				else
				{
					return 0;
				}
			}
			else
			{
				return 0;
			}
		}
		else if($postsFrom=='4')
		{
			if($privacy=='2')
			{
				if($this->checkIfFavourite($userId,$imageID))
			    {
				    return 1;
			    }
				else
				{
					return 0;
				}
			}
			else if($privacy=='1')
			{
				if($this->checkIfFollowed($userId,$posteduserId))
				{
					if($this->checkIfFavourite($userId,$imageID))
			        {
				        return 1;
			        }
				    else
				    {
					    return 0;
				    }
				}
				else
				{
					return 0;
				}
			}
			else
			{
				if($userId==$posteduserId)
			    {
				    if($this->checkIfFavourite($userId,$imageID))
			        {
				        return 1;
			        }
				    else
				    {
					    return 0;
				    }
			    }
			    else
			    {
				    return 0;
			    }
			}
		}
		else if($postsFrom=='6')
		{
			if($privacy=='2')
			{
				if($this->checkIfLiked($userId,$imageID))
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
			else if($privacy=='1')
			{
				if($this->checkIfFollowed($userId,$posteduserId))
				{
					if($this->checkIfLiked($userId,$imageID))
				    {
					    return 1;
				    }
				    else
				    {
					    return 0;
				    }
				}
				else
				{
					return 0;
				}
			}
			else
			{
				if($userId==$posteduserId)
			    {
				    if($this->checkIfLiked($userId,$imageID))
				    {
					    return 1;
				    }
				    else
				    {
					    return 0;
				    }
			    }
			    else
			    {
				    return 0;
			    }
			}
		}
		else
		{
			return 0;
		}			
	}
	
	public function checkIfFavourite($userId,$imageId)
	{
		$data=DB::table("favouriteUploads")->where('userId',$userId)->where('imageId',$imageId)->get();
		if(count($data))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	public function registerUser(Request $request)
	{
		// $user = DB::table('users')->where('id',27)->first();

	// echo $user->username;
		
		$username=$this->sanitizeInput($request->input('username'));
		$gender=$this->sanitizeInput($request->input('gender'));
		
		$email=$this->sanitizeInput($request->input('email'));
		
		$phone=$this->sanitizeInput($request->input('phone'));
		$password=$this->sanitizeInput($request->input('password'));
		$registerType=$this->sanitizeInput($request->input('registerType'));
		$loginPlatform=$this->sanitizeInput($request->input('loginPlatform'));
		$gcmToken=$this->sanitizeInput($request->input('gcmToken'));
		if(!empty($username) && !empty($email) && !empty($password) && !empty($gender) && !empty($phone) && !empty($registerType) && !empty($loginPlatform) && !empty($gcmToken))
		{
		if($this->checkUniqueEmail($email))
			{
				if($this->checkUniquePhone($phone) || empty($phone))
				{
					if($this->checkUniqueUserName($username))
				    {
		
			
				
					$authorization=sha1('exousia'.uniqid());
					$sessiontime=sha1(date('Y-m-d H:i:s'));
					   
	$check=DB::table('users')->insertGetId(["email"=>$email,"username"=>$username,"gender"=>$gender,"phone"=>$phone,"password"=>bcrypt($password),"registerType"=>$registerType,"loginPlatform"=>$loginPlatform,"gcmToken"=>$gcmToken,"regDate"=>time(),"sessiontime"=>$sessiontime,"authorization"=>$authorization]);
					    if($check!='' || $check!='0')
					    {
							
						    
						    $json=array("status"=>"1","message"=>"Registration Success","response"=>array("user_id"=>$check,"email"=>$email,"phone"=>$phone,"username"=>$username));
					    }
					    else
					    {
						    $json=array("status"=>"0","message"=>"Registration Failure","response"=>array());
					    }
										
				
			}
                    else
					{
						$json=array("status"=>"5","message"=>"Username Already Registered","response"=>array());
					}						
				}
				else
				{
					$json=array("status"=>"4","message"=>"Contact Already Registered","response"=>array());
				}
			}
			else
			{
				$json=array("status"=>"3","message"=>"Email Already Registered","response"=>array());
			}
			
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	
	
	
	
	
	
	// public function registerUser(Request $request)
	// {
		// $name=$this->sanitizeInput($request->input('name'));
		// $username=$this->sanitizeInput($request->input('username'));
		// $profileType=$this->sanitizeInput($request->input('profileType'));
		// $imageName=$this->sanitizeInput($request->input('imageName'));
		// $email=$this->sanitizeInput($request->input('email'));
		// $description=$this->sanitizeInput($request->input('description'));
		// $phone=$this->sanitizeInput($request->input('phone'));
		// $password=$this->sanitizeInput($request->input('password'));
		// $latitude=$this->sanitizeInput($request->input('latitude'));
		// $longitude=$this->sanitizeInput($request->input('longitude'));
		// $loginPlatform=$this->sanitizeInput($request->input('loginPlatform'));
		// $registerMethod=$this->sanitizeInput($request->input('registerMethod'));
		// $notificationToken=$this->sanitizeInput($request->input('notificationToken'));
		// if(!empty($username) && !empty($email) && !empty($password) && !empty($loginPlatform)  && !empty($registerMethod) && (!empty($profileType)))
		// {
			// if($this->checkUniqueEmail($email))
			// {
				// if($this->checkUniquePhone($phone) || empty($phone))
				// {
					// if($this->checkUniqueUserName($username))
				    // {
					    // $authorization=sha1('exousia'.uniqid());
					    // $sessionTime=sha1(date('Y-m-d H:i:s'));
						// if(empty($name))
						// {
							// $name=$username;
						// }
					    // $check=DB::table('users')->insertGetId(["email"=>$email,"username"=>$username,"phone"=>$phone,"password"=>bcrypt($password),"lastLogin"=>time(),"name"=>$name,"createdAt"=>time(),"updatedAt"=>time(),"emailVerificationCode"=>uniqid(),"description"=>$description,"latitude"=>$latitude,"longitude"=>$longitude,"sessionTime"=>$sessionTime,"authorization"=>$authorization,"loginPlatform"=>$loginPlatform,"registerMethod"=>$registerMethod,"notificationToken"=>$notificationToken,"profileType"=>$profileType]);
					    // if($check!='' || $check!='0')
					    // {
							// if(empty($imageName))
							// {
								// File::copy(storage_path('../demo/uploads/user_images/Default-avatar.jpg'),storage_path('../demo/uploads/user_images/smartchef_'.$check.'.png'));
							// }
							// else
							// {
								 // File::move(storage_path('../demo/uploads/user_images/'.$imageName),storage_path('../demo/uploads/user_images/smartchef_'.$check.'.png'));
							// }
						    // DB::table('userSettings')->insert(['userId'=>$check]);
						    // $json=array("status"=>"1","message"=>"Registration Success","response"=>array("user_id"=>$check,"email"=>$email,"phone"=>$phone,"username"=>$username,"name"=>$name,"emailVerified"=>0,"description"=>$description,"latitude"=>floatval($latitude),"longitude"=>floatval($longitude),"sessionTime"=>$sessionTime,"authorization"=>$authorization,"findMe"=>intval(1),"postsPrivacy"=>intval(2),"ghostMode"=>intval(0),"receiveMessages"=>intval(1),"newFollower"=>intval(1),"newLike"=>intval(1),"profileType"=>$profileType));
					    // }
					    // else
					    // {
						    // $json=array("status"=>"0","message"=>"Registration Failure","response"=>array());
					    // }
					// }
                    // else
					// {
						// $json=array("status"=>"5","message"=>"Username Already Registered","response"=>array());
					// }						
				// }
				// else
				// {
					// $json=array("status"=>"4","message"=>"Contact Already Registered","response"=>array());
				// }
			// }
			// else
			// {
				// $json=array("status"=>"3","message"=>"Email Already Registered","response"=>array());
			// }
			
		// }
		// else
		// {
			// $json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		// }
		// return Response::json($json);
	// }
	public function userDetails(Request $request)
	{
		
		
		$user_id=$this->sanitizeInput($request->input('user_id'));
		$Preacher=$this->sanitizeInput($request->input('Preacher'));
		$beard=$this->sanitizeInput($request->input('beard'));
		
		$virgin=$this->sanitizeInput($request->input('virgin'));
		$veil=$this->sanitizeInput($request->input('veil'));
		$healthStatus=$this->sanitizeInput($request->input('healthStatus'));
		$smoke=$this->sanitizeInput($request->input('smoke'));
		$body=$this->sanitizeInput($request->input('body'));
		$look=$this->sanitizeInput($request->input('look'));
		$traditional=$this->sanitizeInput($request->input('traditional'));
		$religious=$this->sanitizeInput($request->input('religious'));
		$incomeLevel=$this->sanitizeInput($request->input('incomeLevel'));
		$liveOutside=$this->sanitizeInput($request->input('liveOutside'));
		$hobbies=$this->sanitizeInput($request->input('hobbies'));
		$children=$this->sanitizeInput($request->input('children'));
		$profession=$this->sanitizeInput($request->input('profession'));
		$bodyColor=$this->sanitizeInput($request->input('bodyColor'));
		$hairLook=$this->sanitizeInput($request->input('hairLook'));
		$hairColor=$this->sanitizeInput($request->input('hairColor'));
		$religion=$this->sanitizeInput($request->input('religion'));
		$city=$this->sanitizeInput($request->input('city'));
		$nationality=$this->sanitizeInput($request->input('nationality'));
		$height=$this->sanitizeInput($request->input('height'));
		$weight=$this->sanitizeInput($request->input('weight'));
		$education=$this->sanitizeInput($request->input('education'));
		$age=$this->sanitizeInput($request->input('age'));
		$status=$this->sanitizeInput($request->input('status'));
		
		
		if(!empty($user_id) && !empty($Preacher) && !empty($beard) && !empty($virgin) && !empty($veil) && !empty($healthStatus) && !empty($smoke) && !empty($body) && !empty($look) && !empty($traditional) && !empty($religious) && !empty($incomeLevel) && !empty($liveOutside) && !empty($hobbies) && !empty($children) && !empty($profession) && !empty($bodyColor) && !empty($bodyColor) && !empty($hairLook) && !empty($hairColor) && !empty($religion) && !empty($city) && !empty($nationality) && !empty($height) && !empty($weight) && !empty($education) && !empty($age) && !empty($status))
		{
		
		
			
				
					
					   
	$check=DB::table('userDetails')->insertGetId(["user_id"=>$user_id,"Preacher"=>$Preacher,"beard"=>$beard,"virgin"=>$virgin,"veil"=>$veil,"healthStatus"=>$healthStatus,"smoke"=>$smoke,"body"=>$body,"look"=>$look,"traditional"=>$traditional,"religious"=>$religious,"incomeLevel"=>$incomeLevel,"liveOutside"=>$liveOutside,"hobbies"=>$hobbies,"children"=>$children,"profession"=>$profession,"bodyColor"=>$bodyColor,"hairLook"=>$hairLook,"hairColor"=>$hairColor,"religion"=>$religion,"city"=>$city,"nationality"=>$nationality,"height"=>$height,"weight"=>$weight,"education"=>$education,"age"=>$age,"status"=>$status]);
					    if($check!='' || $check!='0')
					    {
							
						    
						    $json=array("status"=>"1","message"=>"Success","response"=>array("id"=>$check,"Preacher"=>$Preacher,"beard"=>$beard,"virgin"=>$virgin));
					    }
					    else
					    {
						    $json=array("status"=>"0","message"=>"Registration Failure","response"=>array());
					    }
										
				
			
			
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	
	public function sanitizeInput($var) 
	{
	   $var = trim($var);
	   $var = stripslashes($var);
	   $var = htmlspecialchars($var);
	   $var = htmlentities($var);
	   return $var;
	}
	
	public function checkUniqueEmail($email)
	{
		$check=DB::table('users')->select('*')->where('email',$email)->get();
		if(count($check))
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	
	public function checkUniqueUserName($username)
	{
		$check=DB::table('users')->select('*')->where('username',$username)->get();
		if(count($check))
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	
	public function checkUniquePhone($phone)
	{
		$check=DB::table('users')->select('*')->where('phone',$phone)->get();
		if(count($check))
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	public function qabelahName(Request $request)
	{
		$json1=array();
		 $users = DB::table('Qabelah_Name')->get();
		foreach ($users as $user) {
			
			
    $json1[]=array("id"=>$user->id,"qabelah_name"=>$user->QabelahName);
	
		}
		$json=array("status"=>"1","message"=>"Qabelah Names","response"=>$json1);
		return Response::json($json);
	}
	public function loginUser(Request $request)
	{
		$email=$this->sanitizeInput($request->input('email'));
		$password=$this->sanitizeInput($request->input('password'));
		
		if(!empty($email) && !empty($password))
		{
			
			$user=DB::table('users')->where('email',$email)->orWhere('username',$email)->first();
			
			if(count($user))
			{	
		
			    if(Hash::check($password,$user->password))
				{
					
						$authorization=sha1('exousia'.uniqid());
						$sessiontime=sha1(date('Y-m-d H:i:s'));
						$update=DB::Table('users')->where('email',$email)->orWhere('username',$email)->update(["sessiontime"=>$sessiontime,"authorization"=>$authorization]);
						$json=array("status"=>"1","message"=>"Login Success","response"=>array("user_id"=>$user->id,"email"=>$user->email,"phone"=>$user->phone,"username"=>$user->username,"sessiontime"=>$sessiontime,"authorization"=>$authorization));
					
				}
				else
				{
					$json=array("status"=>"3","message"=>"Invalid Credentials","response"=>array());
				}
			}
			else
			{
				$json=array("status"=>"0","message"=>"Invalid Credentials","response"=>array());
			}
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	public function updatePrivacySettings(Request $request)
	{
		$accessToken =  $this->sanitizeInput($request->header('Authorization'));
		$sessionTime =  $this->sanitizeInput($request->input('sessionTime'));
		$userId =  $this->sanitizeInput($request->input('userId'));
		$data['findMe'] =  $this->sanitizeInput($request->input('findMe'));
		$data['ghostMode'] =  $this->sanitizeInput($request->input('ghostMode'));
		$data['postsPrivacy'] =  $this->sanitizeInput($request->input('postsPrivacy'));
		if(!empty($accessToken) && !empty($sessionTime) && !empty($userId) && $data['findMe']!='' && $data['ghostMode']!='' && $data['postsPrivacy']!='')
		{
			if($this->checkAuthorization($accessToken,$sessionTime,$userId))
			{
				DB::table('userSettings')->where('userId',$userId)->update($data);
				$json=array("status"=>"1","message"=>"Privacy Settings updated","response"=>array());
			}
			else
			{
				$json=array("status"=>"10","message"=>"Authorization Failed","response"=>array());
			}
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	
	public function checkAuthorization($accessToken,$sessionTime,$userId)
	{
		$check=DB::table('users')->select('*')->where('id',$userId)->where('sessionTime',$sessionTime)->where('authorization',$accessToken)->get();
		if(count($check))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	public function updateNotifications(Request $request)
	{
		$accessToken =  $this->sanitizeInput($request->header('Authorization'));
		$sessionTime =  $this->sanitizeInput($request->input('sessionTime'));
		$userId =  $this->sanitizeInput($request->input('userId'));
		$data['receiveMessages'] =  $this->sanitizeInput($request->input('receiveMessages'));
		$data['newFollower'] =  $this->sanitizeInput($request->input('newFollower'));
		$data['newLike'] =  $this->sanitizeInput($request->input('newLike'));
		if(!empty($accessToken) && !empty($sessionTime) && !empty($userId) && $data['receiveMessages']!='' && $data['newFollower']!='' && $data['newLike']!='')
		{
			if($this->checkAuthorization($accessToken,$sessionTime,$userId))
			{
				DB::table('userSettings')->where('userId',$userId)->update($data);
				$json=array("status"=>"1","message"=>"Privacy Settings updated","response"=>array());
			}
			else
			{
				$json=array("status"=>"10","message"=>"Authorization Failed","response"=>array());
			}
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	public function changePassword(Request $request)
	{
		$accessToken =  $this->sanitizeInput($request->header('Authorization'));
		$sessionTime =  $this->sanitizeInput($request->input('sessionTime'));
		$userId =  $this->sanitizeInput($request->input('userId'));
		$oldPassword =  $this->sanitizeInput($request->input('oldPassword'));
		$newPassword =  bcrypt($this->sanitizeInput($request->input('newPassword')));
		if(!empty($accessToken) && !empty($sessionTime) && !empty($userId) && !empty($oldPassword) && !empty($newPassword))
		{
			if($this->checkAuthorization($accessToken,$sessionTime,$userId))
			{
				$user=DB::table('users')->select('*')->where('id',$userId)->first();
				if(Hash::check($oldPassword,$user->password))
				{
					if(DB::table('users')->where('id',$userId)->update(['password'=>$newPassword]))
					{
						$json=array("status"=>"1","message"=>"Password updated","response"=>array());
					}
					else
					{
						$json=array("status"=>"0","message"=>"Password update Failed","response"=>array());
					}
				}
				else
				{
					$json=array("status"=>"3","message"=>"Incorrect Password","response"=>array());
				}
			}
			else
			{
				$json=array("status"=>"10","message"=>"Authorization Failed","response"=>array());
			}
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	public function deactivateProfile(Request $request)
	{
		$accessToken =  $this->sanitizeInput($request->header('Authorization'));
		$sessionTime =  $this->sanitizeInput($request->input('sessionTime'));
		$userId =  $this->sanitizeInput($request->input('userId'));
		if(!empty($accessToken) && !empty($sessionTime) && !empty($userId))
		{
			if($this->checkAuthorization($accessToken,$sessionTime,$userId))
			{
				if(DB::table('users')->where('id',$userId)->update(['deactivate'=>'1']))
				{
					$json=array("status"=>"1","message"=>"Profile Deactivated","response"=>array());
				}
				else
				{
					$json=array("status"=>"0","message"=>"Profile Deactivation failed","response"=>array());
				}
			}
			else
			{
				$json=array("status"=>"10","message"=>"Authorization Failed","response"=>array());
			}
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	public function contactForm(Request $request)
	{
		$accessToken =  $this->sanitizeInput($request->header('Authorization'));
		$sessionTime =  $this->sanitizeInput($request->input('sessionTime'));
		$userId =  $this->sanitizeInput($request->input('userId'));
		$title =  $this->sanitizeInput($request->input('title'));
		$message =  $this->sanitizeInput($request->input('message'));
		if(!empty($accessToken) && !empty($sessionTime) && !empty($userId) && !empty($title) && !empty($message))
		{
			if($this->checkAuthorization($accessToken,$sessionTime,$userId))
			{
				if(DB::table('contact')->insert(['userId'=>$userId,'title'=>$title,'message'=>$message,'time'=>time()]))
				{
					$json=array("status"=>"1","message"=>"Contact Request Submitted","response"=>array());
				}
				else
				{
					$json=array("status"=>"0","message"=>"Contact Request sending failed","response"=>array());
				}
			}
			else
			{
				$json=array("status"=>"10","message"=>"Authorization Failed","response"=>array());
			}
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	public function getFaqs()
	{
		$js=array();
		$data=DB::table('faqs')->select('*')->where('status','1')->get();
		foreach($data as $row)
		{
			$js[]=array("id"=>$row->id,"title"=>$row->title,"description"=>$row->description);
		}
		$json=array("status"=>"1","message"=>"Smartchef Faqs","response"=>$js);
		return Response::json($json);
	}
	
	public function uploadProfileImageAndroid(Request $request)
	{
		$image=$request->input('image');
		if(!empty($image))
		{
			$check=uniqid();
			if(File::put(storage_path('../demo/uploads/user_images/'.$check.'.png'), $image))
			{
				if($this->compressImage('../demo/uploads/user_images/'.$check.'.png','../demo/uploads/user_images/'.$check.'.png'))
				{
				    $json=array("status"=>"1","message"=>"Image uploaded Successfully","response"=>$check.'.png');
				}
				else
				{
					File::delete('../demo/uploads/user_images/'.$check.'.png');
					$json=array("status"=>"3","message"=>"Invalid Image Format","response"=>$check.'.png');
				}
			}
			else
			{
				$json=array("status"=>"0","message"=>"Image uploading Failed","response"=>array());
			}
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	public function uploadProfileImageIos(Request $request)
	{
		$file = $request->file('image');
		$check=uniqid();
		if($file->move(storage_path('../demo/uploads/user_images/'),$check.'.png'))
		{
			if($this->compressImage('../demo/uploads/user_images/'.$check.'.png','../demo/uploads/user_images/'.$check.'.png'))
			{
				$json=array("status"=>"1","message"=>"Image uploaded Successfully","response"=>$check.'.png');
			}
			else
			{
				$file->delete('../demo/uploads/user_images/'.$check.'.png');
				$json=array("status"=>"3","message"=>"Invalid Image Format","response"=>array());
			}
		}
		else
		{
			$json=array("status"=>"0","message"=>"Image uploading Failed","response"=>array());
		}
		return Response::json($json);
	}
	
	public function uploadImageIos(Request $request)
	{
		$file = $request->file('image');
		$location=$this->sanitizeInput($request->input('location'));
		$latitude=$this->sanitizeInput($request->input('latitude'));
		$longitude=$this->sanitizeInput($request->input('longitude'));
		$caption=$this->sanitizeInput($request->input('caption'));
		$category=$this->sanitizeInput($request->input('category'));
		$userId=$this->sanitizeInput($request->input('userId'));
		$privacy=$this->sanitizeInput($request->input('privacy'));
		if(!empty($userId) && !empty($location) && $privacy!='' && !empty($latitude) && !empty($longitude))
		{
			$check=DB::table('uploads')->insertGetId(["userId"=>$userId,"location"=>$location,"caption"=>$caption,"category"=>$category,"privacy"=>$privacy,"time"=>time(),'latitude'=>$latitude,'longitude'=>$longitude]);
			if($check!='' && $check!='0')
			{
		        if($file->move(storage_path('../demo/uploads/posted_images/'),'smartchefUpload_'.$check.'.png'))
				{
					if($this->compressImage('../demo/uploads/posted_images/smartchefUpload_'.$check.'.png','../demo/uploads/posted_images/smartchefUpload_'.$check.'.png'))
					{
						$json=array("status"=>"1","message"=>"Image uploaded Successfully","response"=>array("imageId"=>$check,"imageName"=>'smartchefUpload_'.$check.'.png'));
					}
					else
					{
						DB::table('uploads')->where('id',$check)->delete();
						$file->delete('../demo/uploads/posted_images/smartchefUpload_'.$check.'.png');
						$json=array("status"=>"3","message"=>"Invalid Image Format","response"=>array());
					}
				}
				else
				{
					DB::table('uploads')->where('id',$check)->delete();
					$json=array("status"=>"4","message"=>"Image Upload failed","response"=>array());
				}
			}
			else
			{
				$json=array("status"=>"0","message"=>"Action Failed","response"=>array());	
			}
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	public function uploadImageAndroid(Request $request)
	{
		$image=$request->input('image');
		$location=$this->sanitizeInput($request->input('location'));
		$latitude=$this->sanitizeInput($request->input('latitude'));
		$longitude=$this->sanitizeInput($request->input('longitude'));
		$caption=$this->sanitizeInput($request->input('caption'));
		$category=$this->sanitizeInput($request->input('category'));
		$userId=$this->sanitizeInput($request->input('userId'));
		$privacy=$this->sanitizeInput($request->input('privacy'));
		if(!empty($userId) && !empty($image) && $privacy!='')
		{
			$check=DB::table('uploads')->insertGetId(["userId"=>$userId,"location"=>$location,"caption"=>$caption,"category"=>$category,"privacy"=>$privacy,"time"=>time(),'latitude'=>$latitude,'longitude'=>$longitude]);
			if($check!='' && $check!='0')
			{
				if(File::put(storage_path('../demo/uploads/posted_images/smartchefUpload_'.$check.'.png'), $image))
				{
					if($this->compressImage('../demo/uploads/posted_images/smartchefUpload_'.$check.'.png','../demo/uploads/posted_images/smartchefUpload_'.$check.'.png'))
					{
						$json=array("status"=>"1","message"=>"Image uploaded Successfully","response"=>array("imageId"=>$check,"imageName"=>'smartchefUpload_'.$check.'.png'));
					}
					else
					{
						DB::table('uploads')->where('id',$check)->delete();
						File::delete('../demo/uploads/posted_images/smartchefUpload_'.$check.'.png');
						$json=array("status"=>"3","message"=>"Invalid Image Format","response"=>array());
					}
				}
				else
				{
					DB::table('uploads')->where('id',$check)->delete();
					$json=array("status"=>"4","message"=>"Image Upload failed","response"=>array());
				}
			}
			else
			{
				$json=array("status"=>"0","message"=>"Image uploading Failed","response"=>array());
			}
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	public function getCategories()
	{
		$check=DB::table('categories')->where('status','1')->get();
		$json=array();
		foreach($check as $row)
		{
			$json[]=array("id"=>$row->category_id,"name"=>$row->title);
		}
		$js=array("status"=>"1","message"=>"All Categories","response"=>$json);
		return Response::json($js);
	}
	
	public function compressImage($src,$dest)
    {
		$info = getimagesize($src);
        if ($info['mime'] == 'image/jpeg') 
        {
            $image = imagecreatefromjpeg($src);
        }    
		elseif ($info['mime'] == 'image/gif')
		{
            $image = imagecreatefromgif($src);
		}
        elseif ($info['mime'] == 'image/png')
		{
			$image = imagecreatefrompng($src);    
		}
		else
		{
            return 0;
		}
		imagejpeg($image, $dest, 30);
		return 1;
	}
	
	public function getTerms()
	{
		$check=DB::table('settings')->where('field','terms')->first();
		return view('viewTerms',compact('check'));
	}
	
	public function getAbout()
	{
		$check=DB::table('settings')->where('field','about')->first();
		return view('viewTerms',compact('check'));
	}
	
	public function getPrivacy()
	{
		$check=DB::table('settings')->where('field','privacy')->first();
		return view('viewTerms',compact('check'));
	}
	
	public function getProfileInfoImages(Request $request)
	{
		$viewer=$request->input('viewer');
		$profile=$request->input('profile');
		$countFrom=$this->sanitizeInput($request->input('count'));
		if(!empty($profile))
		{
		    $countTo=$countFrom+15;
		    $data=DB::table('uploads')->where('userId',$profile)->orderBy('id', 'desc')->get();
		    $count=0;
		    $var=array();
		    foreach($data as $dat)
		    {
			    if($this->isPostVisibleProfile($viewer,$dat->privacy,$dat->userId))
			    {
				    if($countFrom<=$count && $countTo>$count)
				    {
				        $name=$this->getUserNameFromId($dat->userId);
				        $var[]=array("imageId"=>$dat->id,"userId"=>$dat->userId,"userName"=>$name,"location"=>$dat->location,"latitude"=>floatval($dat->latitude),"longitude"=>floatval($dat->longitude),"caption"=>$dat->caption,"category"=>json_decode($dat->category),"time"=>intval(time()-$dat->time),"likes"=>$this->getLikesCount($dat->id),"views"=>$this->getviewsCount($dat->id),"liked"=>$this->checkIfLiked($viewer,$dat->id),"comments"=>$this->getCommentsCount($dat->id));				
				    }
					$count++;	
			    }
		    }
		    $json=array("status"=>"1","message"=>"Profile Data","response"=>$var);
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);	
	}
	
	public function isPostVisibleProfile($viewer,$privacy,$id)
	{
		if($viewer!='')
		{
			if($privacy=='2')
			{
				return 1;
			}
			else if($privacy=='1')
			{
				if($this->checkIfFollowed($viewer,$id))
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
			else
			{
				if($viewer==$id)
			    {
				    return 1;
			    }
			    else
			    {
				    return 0;
			    }
			}
		}
		else
		{
			if($privacy=='2')
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}
	
	public function getProfileInfo(Request $request)
	{
		$viewer=$request->input('viewer');
		$profile=$request->input('profile');
		if(!empty($profile))
		{
			$data=DB::table('users')->where('id',$profile)->first();
			if(count($data))
			{
				if(!empty($data->country) && (!empty($data->city)))
				{
					$location=$data->city.','.$data->country;
				}
				else
				{
					$location='';
				}
				$json=array("status"=>"1","message"=>"Profile Data","response"=>array("name"=>$data->name,"username"=>$data->username,"location"=>$location,"website"=>$data->website,"photos"=>$this->uploadedPhotosCount($profile),"followers"=>$this->getFollowersCount($profile),"following"=>$this->getFollowingCount($profile),"followed"=>$this->checkIfFollowed($viewer,$profile),"description"=>$data->description,"name"=>$data->name,"likes"=>$this->getTotalImageLikes($profile),"rating"=>$this->getAverageRating($profile),"ratingsCount"=>$this->getRatingsCount($profile),"myRating"=>$this->getMyRating($viewer,$profile),"myReview"=>$this->getMyReview($viewer,$profile)));
			}
			else
			{
				$json=array("status"=>"3","message"=>"Account Does Not Exists","response"=>(object)array());
			}
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>(object)array());
		}
		return Response::json($json);	
	}
	
	public function getMyReview($viewer,$profile)
	{
		$data=DB::table('reviews')->where('userId',$viewer)->where('profileId',$profile)->first();
		if(count($data))
		{
			return $data->review;
		}
		else
		{
			return '0';
		}
	}
	
	public function getMyRating($viewer,$profile)
	{
		$data=DB::table('reviews')->where('userId',$viewer)->where('profileId',$profile)->first();
		if(count($data))
		{
			return round($data->rating,2);
		}
		else
		{
			return 0;
		}
	}
	
	public function getAverageRating($profile)
	{
		$total=0;
		$data=DB::table('reviews')->where('profileId',$profile)->get();
		foreach($data as $dat)
		{
			$total+=$dat->rating;
		}
		$count=$this->getRatingsCount($profile);
		if($count!=0)
		{
		    return round($total/$count,2);
		}
		else
		{
			return 0;
		}
	}
	
	public function getRatingsCount($profile)
	{
		$data=DB::table('reviews')->where('profileId',$profile)->get();
		return count($data);
	}
	
	public function getTotalImageLikes($profile)
	{
		$count=0;
		$data=DB::table('uploads')->where('userId',$profile)->get();
		foreach($data as $dat)
		{
			$var=Db::table('uploadLikes')->where('uploadId',$dat->id)->get();
			$count+=count($var);
		}
		return $count;
	}
	
	public function checkIfFollowed($viewer,$profile)
	{
		if($viewer!='')
		{
		    $data=DB::table('followers')->where('follower',$viewer)->where('following',$profile)->get();
			if(count($data))
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}
	
	public function getFollowersCount($profile)
	{
		$data=DB::table('followers')->where('following',$profile)->get();
		return count($data);
	}
	
	public function getFollowingCount($profile)
	{
		$data=DB::table('followers')->where('follower',$profile)->get();
		return count($data);
	}
	
	public function uploadedPhotosCount($profile)
	{
		$data=DB::table('uploads')->where('userId',$profile)->get();
		return count($data);
	}
	
	public function giveReview(Request $request)
	{
		$viewer=$this->sanitizeInput($request->input('viewer'));
		$profile=$this->sanitizeInput($request->input('profile'));
		$rating=$this->sanitizeInput($request->input('rating'));
		$review=$this->sanitizeInput($request->input('review'));
		$accessToken =  $this->sanitizeInput($request->header('Authorization'));
		$sessionTime =  $this->sanitizeInput($request->input('sessionTime'));
		if(!empty($accessToken) && !empty($sessionTime) && !empty($viewer) && !empty($profile) && !empty($rating))
		{
			if($this->checkAuthorization($accessToken,$sessionTime,$viewer))
			{
				$data=DB::table('reviews')->where('userId',$viewer)->where('profileId',$profile)->get();
				if(count($data))
				{
					$var=DB::table('reviews')->where('userId',$viewer)->where('profileId',$profile)->update(["rating"=>$rating,"review"=>$review,"time"=>time()]);
					if($var)
					{
						$json=array("status"=>"1","message"=>"Review Submitted","response"=>array("rating"=>$this->getAverageRating($profile),"ratingsCount"=>$this->getRatingsCount($profile)));
					}
					else
					{
						$json=array("status"=>"0","message"=>"Review submission failed","response"=>array());
					}
				}
				else
				{
					$var=DB::table('reviews')->insert(["userId"=>$viewer,"profileId"=>$profile,"rating"=>$rating,"review"=>$review,"time"=>time()]);
					if($var)
					{
						$json=array("status"=>"1","message"=>"Review Submitted","response"=>array("rating"=>$this->getAverageRating($profile),"ratingsCount"=>$this->getRatingsCount($profile)));
					}
					else
					{
						$json=array("status"=>"0","message"=>"Review submission failed","response"=>array());
					}
				}
			}
			else
			{
				$json=array("status"=>"10","message"=>"Authorization Failed","response"=>array());
			}
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	public function getReviews(Request $request)
	{
		$profile=$this->sanitizeInput($request->input('profile'));
		$countFrom=$this->sanitizeInput($request->input('count'));
		if(!empty($profile))
		{
		    $data=Db::table('reviews')->where('profileID',$profile)->limit(15)->skip($countFrom)->get();
            $var=array();	
			foreach($data as $dat)
			{
				$var[]=array('id'=>$dat->userId,"name"=>$this->getUserNameFromId($dat->userId),"rating"=>round($dat->rating,2),"review"=>$dat->review,"time"=>time()-$dat->time,"reviews"=>$this->getRatingsCount($profile),"followers"=>$this->getFollowersCount($profile));
			}
            $json=array("status"=>"1","message"=>"All reviews","response"=>$var);			
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	public function addComment(Request $request)
	{
		$userId=$this->sanitizeInput($request->input('userId'));
		$uploadId=$this->sanitizeInput($request->input('uploadId'));
		$comment=$this->sanitizeInput($request->input('comment'));
		if(!empty($userId) && !empty($uploadId) && !empty($comment))
		{
			$var=DB::table('uploadComments')->insert(["userId"=>$userId,"uploadId"=>$uploadId,"comment"=>$comment,"time"=>time()]);
			if($var)
			{
				$json=array("status"=>"1","message"=>"Comment has been added Successfully","response"=>$var);
			}
			else
			{
				$json=array("status"=>"0","message"=>"Action Failed","response"=>$var);
			}
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	public function getComments(Request $request)
	{
		$uploadId=$this->sanitizeInput($request->input('uploadId'));
		$countFrom=$this->sanitizeInput($request->input('count'));
		if(!empty($uploadId))
		{
			$var=array();
			$data=DB::table('uploadComments')->where('uploadId',$uploadId)->orderBy('id', 'desc')->limit(20)->skip($countFrom)->get();
			foreach($data as $dat)
			{
				$var[]=array("userId"=>$dat->userId,"username"=>$this->getUserNameFromId($dat->userId),"comment"=>$dat->comment,"time"=>time()-$dat->time);
			}
			$json=array("status"=>"1","message"=>"All Comments","response"=>$var);
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	public function getHomePeople(Request $request)
	{
		$userId=$this->sanitizeInput($request->input('userId'));
		$name=$this->sanitizeInput($request->input('name'));
		$profiles=json_decode($this->sanitizeInput($request->input('profiles')));
		$sort=$this->sanitizeInput($request->input('sort'));
		$countFrom=$this->sanitizeInput($request->input('count'));
		if(!empty($profiles) && !empty($sort))
		{
			$countTo=$countFrom+15;
			$var=array();
			$data=DB::table('users')->where('deactivate','0')->where('banAdmin','0')->where('status','1')->where('username','LIKE','%'.$name.'%')->get();
			foreach($data as $dat)
			{
				if((in_array($dat->profileType,$profiles) || !count($profiles))&& $this->checkIfBlocked($userId,$dat->id))
				{
					$var[]=array("id"=>$dat->id,"name"=>$dat->username,"likes"=>$this->getLikesCount($dat->id),"followers"=>$this->getFollowersCount($dat->id),"posts"=>$this->getPostsCount($dat->id),"rating"=>$this->getAverageRating($dat->id));
				}
			}
			$row=array();
			if($sort=='1')
		    {
			    usort($var, function($a, $b) 
			    {
                    return $a['likes'] > $b['likes'] ? -1 : 1;
                }); 
		    }
			else if($sort=='2')
			{
			    usort($var, function($a, $b) 
			    {
                    return $a['followers'] > $b['followers'] ? -1 : 1;
                }); 
			}
			else if($sort=='3')
			{
			    usort($var, function($a, $b) 
			    {
                    return $a['posts'] > $b['posts'] ? -1 : 1;
                }); 
			}
			else if($sort=='4')
			{
			    usort($var, function($a, $b) 
			    {
                    return $a['rating'] > $b['rating'] ? -1 : 1;
                }); 
			}
			$count=0;
			foreach($var as $dat)
			{
				if($countFrom<=$count && $countTo>$count)
			    {
				    $row[]=array("id"=>$dat['id'],"name"=>$dat['name']);
				}
				$count++;
			}
			$json=array("status"=>"1","message"=>"All Home users","response"=>$row);
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	public function getPostsCount($id)
	{
		$data=DB::table('uploads')->where('userId',$id)->get();
		return count($data);
	}
	
	public function getHomePosts(Request $request)
	{
		$userId=$this->sanitizeInput($request->input('userId'));
		$name=$this->sanitizeInput($request->input('name'));
		$profiles=json_decode($this->sanitizeInput($request->input('profiles')));
		$categories=json_decode($this->sanitizeInput($request->input('categories')));
		$sort=$this->sanitizeInput($request->input('sort'));
		$countFrom=$this->sanitizeInput($request->input('count'));
		$latitude=$this->sanitizeInput($request->input('latitude'));
		$longitude=$this->sanitizeInput($request->input('longitude'));
		$distance=$this->sanitizeInput($request->input('distance'));
		if(!empty($profiles) && !empty($sort))
		{
			$countTo=$countFrom+15;
			$var=array();
			$data=DB::table('uploads')->get();
			foreach($data as $dat)
			{
				if($this->checkIfBlocked($userId,$dat->id) && $this->isPostVisibleProfile($userId,$dat->privacy,$dat->userId) &&  $this->postVisibleByDistance($latitude,$longitude,floatval($dat->latitude),floatval($dat->longitude),$distance) && $this->checkIfprofilevalid($dat->userId) && $this->checkIfContentReported($userId,$dat->id) && $this->postVisibleByCategory($categories,json_decode($dat->category)))
				{
					$check=DB::table('users')->where('id',$dat->userId)->where('username','LIKE','%'.$name.'%')->first();
					if(count($check))
					{ 
				        if(in_array($check->profileType,$profiles) || !count($profiles))
						{
							$var[]=array("imageId"=>$dat->id,"userId"=>$dat->userId,"userName"=>$check->username,"location"=>$dat->location,"latitude"=>floatval($dat->latitude),"longitude"=>floatval($dat->longitude),"caption"=>$dat->caption,"category"=>json_decode($dat->category),"time"=>intval(time()-$dat->time),"likes"=>$this->getLikesCount($dat->id),"views"=>$this->getviewsCount($dat->id),"liked"=>$this->checkIfLiked($userId,$dat->id),"comments"=>$this->getCommentsCount($dat->id));
						}
					}
				}
			}
			$row=array();
			if($sort=='1')
		    {
			    usort($var, function($a, $b) 
			    {
                    return $a['imageId'] > $b['imageId'] ? -1 : 1;
                }); 
		    }
			else if($sort=='2')
		    {
			    usort($var, function($a, $b) 
			    {
                    return $a['likes'] > $b['likes'] ? -1 : 1;
                }); 
		    }
			else if($sort=='4')
			{
			    usort($var, function($a, $b) 
			    {
                    return $a['views'] > $b['views'] ? -1 : 1;
                }); 
			}
			else if($sort=='5')
			{
			    usort($var, function($a, $b) 
			    {
                    return $a['time'] > $b['time'] ? -1 : 1;
                }); 
			}
			else if($sort=='6')
			{
			    usort($var, function($a, $b) 
			    {
                    return $a['rating'] > $b['rating'] ? -1 : 1;
                }); 
			}
			$count=0;
			foreach($var as $dat)
			{
				if($countFrom<=$count && $countTo>$count)
			    {
				    $row[]=$dat;
				}
				$count++;
			}
			$json=array("status"=>"1","message"=>"All Home Posts","response"=>$row);
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
	
	public function postVisibleByCategory($cat1,$cat2)
	{
		if(count($cat1))
		{
			$var=0;
			foreach($cat1 as $cat)
			{
				foreach($cat2 as $ca)
				{
					if($cat==$ca)
					{
						$var=1;
						break;
					}
				}
				if($var==1)
					break;
			}
			return $var;
		}
		else
		{
			return 1;
		}
	}
	
	public function followUser(Request $request)
	{
		$profile=$this->sanitizeInput($request->input('profile'));
		$viewer=$this->sanitizeInput($request->input('viewer'));
		$accessToken =  $this->sanitizeInput($request->header('Authorization'));
		$sessionTime =  $this->sanitizeInput($request->input('sessionTime'));
		if(!empty($profile) && !empty($viewer) && !empty($accessToken) && !empty($sessionTime))
		{
			if($this->checkAuthorization($accessToken,$sessionTime,$viewer))
			{
				$data=DB::table('followers')->where('follower',$viewer)->where('following',$profile)->first();
				if(count($data))
				{
					$var=DB::table('followers')->where('follower',$viewer)->where('following',$profile)->delete();
					if($var)
					{
						$json=array("status"=>"1","message"=>"Success","response"=>array());
					}
					else
					{
						$json=array("status"=>"0","message"=>"Some Error Occured","response"=>array());
					}
				}
				else
				{
					$var=DB::table('followers')->insert(["follower"=>$viewer,"following"=>$profile,"time"=>time()]);
					if($var)
					{
						$json=array("status"=>"1","message"=>"Success","response"=>array());
					}
					else
					{
						$json=array("status"=>"0","message"=>"Some Error Occured","response"=>array());
					}
				}
			}
			else
			{
				$json=array("status"=>"10","message"=>"Authorization Failed","response"=>array());
			}
		}
		else
		{
			$json=array("status"=>"2","message"=>"Invalid Parameters","response"=>array());
		}
		return Response::json($json);
	}
}
?>