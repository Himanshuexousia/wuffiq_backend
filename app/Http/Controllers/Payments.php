<?php

namespace App\Http\Controllers;

use App\User;

use App\Requ;

use Twilio;

use illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use App\Http\Requests;

use Illuminate\Support\Facades\Cache;
 
use DB;

use app;

use File;

use Hash;

use Auth;

use Input;

use Route;

use Response;

use Services_Twilio;

use Eloquent;

use Validator;

class Payments extends Controller
{

	public function pendingPayments()
	{
		$json=array();
		$data=DB::table('payments')->select('payments.*','requests.user_id as boyId','requests.request_id as girlId','bankDetails.bankName')->join('requests','requests.id','=','payments.request_id')->join('bankDetails','bankDetails.id','=','payments.bank_id')->where('payments.status','0')->orderBy('payments.request_id','DESC')->get();
		foreach($data as $allData)
		{
			$boy=DB::table('users')->select('username')->where('id',$allData->boyId)->first();
			$girl=DB::table('users')->select('username')->where('id',$allData->girlId)->first();
			$json[]=array(
							'boyId'=>$allData->boyId,
							'girlId'=>$allData->girlId,
							// 'girlName'=>$girl->username,
							// 'boyName'=>$boy->username,
							'bankName'=>$allData->bankName,
							'request_id'=>$allData->request_id,
							'order_id'=>$allData->order_id,
							'bank_id'=>$allData->bank_id,
							'pay_by'=>$allData->pay_by,
							'amount'=>$allData->amount,
							'customer_name'=>$allData->customer_name,
							'account_no'=>$allData->account_no,
							'optional'=>$allData->optional,
							'created_at'=>$allData->created_at,
							'status'=>$allData->status
						);	
		}
		return view('payments',compact('json'));
		// return $json;
	}
	
	public function donePayments()
	{
		$json=array();
		// echo 's';
		$data=DB::table('payments')->select('payments.*','requests.user_id as boyId','requests.request_id as girlId','bankDetails.bankName')->join('requests','requests.id','=','payments.request_id')->join('bankDetails','bankDetails.id','=','payments.bank_id')->where('payments.status','1')->orderBy('payments.request_id','DESC')->get();
		// dd($data);
		// $data=DB::table('payments')->select('payments.*','payments')
		// print_R($data);
		// if(!empty($data))
		// {
		foreach($data as $allData)
		{
			// if(!empty(allData))
			// {
			$boy=DB::table('users')->select('username')->where('id',$allData->boyId)->first();
			if(count($boy))
			{
				$boy->username='';
			}
			$girl=DB::table('users')->select('username')->where('id',$allData->girlId)->first();
			if(count($girl)!=0)
			{
				$girl->username='0';
			}
			// dd($girl);
			$json[]=array(
							'boyId'=>$allData->boyId,
							'girlId'=>$allData->girlId,
							// 'girlName'=>$girl->username,
							// 'boyName'=>$boy->username,
							'bankName'=>$allData->bankName,
							'request_id'=>$allData->request_id,
							'order_id'=>$allData->order_id,
							'bank_id'=>$allData->bank_id,
							'pay_by'=>$allData->pay_by,
							'amount'=>$allData->amount,
							'customer_name'=>$allData->customer_name,
							'account_no'=>$allData->account_no,
							'optional'=>$allData->optional,
							'created_at'=>$allData->created_at,
							'status'=>$allData->status
						);	
			}
		
	
		return view('payments',compact('json'));
		
	}
	
	public function cancelPayments()
	{
		$json=array();
		// echo 's';
		$data=DB::table('payments')->select('payments.*','requests.user_id as boyId','requests.request_id as girlId','bankDetails.bankName')->join('requests','requests.id','=','payments.request_id')->join('bankDetails','bankDetails.id','=','payments.bank_id')->where('payments.status','2')->where('requests.deactivateRequest','0')->orderBy('payments.request_id','DESC')->get();
		// print_R($data);
		foreach($data as $allData)
		{
			$boy=DB::table('users')->select('username')->where('id',$allData->boyId)->first();
			$girl=DB::table('users')->select('username')->where('id',$allData->girlId)->first();
			$json[]=array(
							'boyId'=>$allData->boyId,
							'girlId'=>$allData->girlId,
							// 'girlName'=>$girl->username,
							// 'boyName'=>$boy->username,
							'bankName'=>$allData->bankName,
							'request_id'=>$allData->request_id,
							'order_id'=>$allData->order_id,
							'bank_id'=>$allData->bank_id,
							'pay_by'=>$allData->pay_by,
							'amount'=>$allData->amount,
							'customer_name'=>$allData->customer_name, 
							'account_no'=>$allData->account_no,
							'optional'=>$allData->optional,
							'created_at'=>$allData->created_at,
							'status'=>$allData->status
						);	
		}
		return view('payments',compact('json'));
		
	}
	
	public function sendNotification($token,$message)
	{	
	
			$url='https://fcm.googleapis.com/fcm/send';
			$fields=array('registration_ids'=>array($token),'data'=>array("message"=>$message));
			$fields=json_encode($fields);
			$headers=array('Authorization: key='."AIzaSyDd5MLRfVWxJCUIDjQK6-Alkhtyo9eDAcM",'Content-Type:application/json');
			$ch=curl_init();
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch,CURLOPT_POST,true);
			curl_setopt($ch,CURLOPT_HTTPHEADER,$headers); 
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch,CURLOPT_POSTFIELDS,$fields);
			curl_exec($ch);
			curl_close($ch);
		
	}

	public function preacherMessage($preacherId,$message)
	{
		$preacher=DB::table('users')->where('id',$preacherId)->first();
		$this->messages($preacher->phone_code,$preacher->phone,$message);		
	}
	
	public function paymentAction()
	{
		$data=Input::all();
		$data['orderId']='500'.$data['requestId'];
		DB::table('payments')->where([['request_id',$data['requestId']],['order_id',$data['orderId']]])->update(['status'=>$data['action']]);
		$userData=DB::table('requests')->where('id',$data['requestId'])->first();
		
		if($data['action']=='1')
		{
			DB::table('requests')->where('id',$data['requestId'])->where('deactivateRequest','0')->update(['payment'=>'1']);			
			if(count($userData)>0)
			{
				$userLoginPlatform=DB::table('users')->select('loginPlatform','gcmToken')->where('id',$userData->user_id)->first();
				$userLoginPlatform2=DB::table('users')->select('loginPlatform','gcmToken')->where('id',$userData->request_id)->first();
				$message1=array('type'=>'notification1','sent'=>'admin','action'=>'1','username'=>'');
				if($userLoginPlatform->loginPlatform=='Android')
				{
				      $this->sendNotification($userLoginPlatform->gcmToken,$message1);
			    }
			    else
                {
                	$message2=array('title'=>'','body'=>'لقد تم استلام المبلغ وتم تحويل الطلب للخطابة, خلال الساعات القادمه ستقوم الخطابه بالاتصال عليك , شكرا','messageData'=>$message1);
                       $this->send_notification_ios($userLoginPlatform->gcmToken,$message2);
                }
                if($userLoginPlatform2->loginPlatform=='Android')
				{
					// dd($userLoginPlatform2->loginPlatform);
				    $this->sendNotification($userLoginPlatform2->gcmToken,$message1);
				}
				else
				{
					$message2=array('title'=>'','body'=>'لقد تم استلام المبلغ وتم تحويل الطلب للخطابة, خلال الساعات القادمه ستقوم الخطابه بالاتصال عليك , شكرا','messageData'=>$message1);
                       $this->send_notification_ios($userLoginPlatform2->gcmToken,$message2);
				}
			}
			
			$boyData=DB::table('users')->join('userDetailsFull','users.id','=','userDetailsFull.user_id')->join('userDetails','users.id','=','userDetails.user_id')->where('users.id',$userData->user_id)->first();
			$girlData=DB::table('users')->join('userDetailsFull','users.id','=','userDetailsFull.user_id')->join('userDetails','users.id','=','userDetails.user_id')->where('users.id',$userData->request_id)->first();
			
			if(count($boyData) && $girlData)
			{
				
				// $boyPreacher=DB::table('users')->where('loginPlatform',$boyData->preature_account)->where('preacher',1)->where('banAdmin',0)->first();
				
				// $girlPreacher=DB::table('users')->where('loginPlatform',$girlData->preature_account)->where('preacher',1)->where('banAdmin',0)->first();
				
				// $message="تم ارسال طلب زواج لكِ"."\n". "Man Name"."\n".$boyData->username."\n"."Man Number"."\n".$boyData->phone."\n"."Women Name"."\n".$girlData->username."\n"."Woman Number"."\n".$girlData->phone;
				
				// if($girlData->preature_status=='نعم' && count($girlPreacher)>0)
				// {	
					// $messa=array('type'=>'notification1','sent'=>'admin','action'=>'3','username'=>'');			
					// $this->preacherMessage($girlPreacher->id,$message);
					// $this->sendNotification($girlPreacher->id,$messa);
					// DB::table('preacher_requests')->insert(['preacher_id'=>$girlPreacher->id,'request_id'=>$data['requestId'],'time'=>time(),'order_id'=>$data['orderId']]);	
				// }
				// else if($boyData->preature_status=='نعم' && count($boyPreacher)>0)
				// {	
					// $messa=array('type'=>'notification1','sent'=>'admin','action'=>'3','username'=>'');			
					// $this->sendNotification($boyPreacher->id,$messa);
					// $this->preacherMessage($boyPreacher->id,$message);				
					// DB::table('preacher_requests')->insert(['preacher_id'=>$boyPreacher->id,'request_id'=>$data['requestId'],'time'=>time(),'order_id'=>$data['orderId']]);
				// }
				
				// else
				// {
				$userLoginPlatform3=DB::table('users')->select('loginPlatform','gcmToken')->where('id','913')->first();
				
					$messa=array('type'=>'notification1','sent'=>'admin','action'=>'3','username'=>'');	
                     if($userLoginPlatform3->loginPlatform=='Android')
                     {
					$this->sendNotification($userLoginPlatform3->gcmToken,$messa);		
					}
					else
					{
						$message4=array('title'=>'','body'=>'لديكِ طلب زواج جديد','messageData'=>$messa);
						$this->send_notification_ios($userLoginPlatform3->gcmToken,$message4);
					}		
					$check=DB::table('preacher_requests')->where('preacher_id',913)->where('request_id',$data['requestId'])->where('order_id',$data['orderId'])->first();
					if(count($check)==0)
					{
					     DB::table('preacher_requests')->insert(['preacher_id'=>913,'request_id'=>$data['requestId'],'time'=>time(),'order_id'=>$data['orderId']]);
					}
						// $manReqdata=DB::table('man_request')->where('user_id',$girlData->user_id)->first();
						// $preacherData=DB::table('users')->where([['users.preacher','=','1'],['users.banAdmin','=','0']])->join('preacherDetails','users.id','=','preacherDetails.preacher_id')->get();
						// if(count($manReqdata))
						// {
						// foreach($preacherData as $preacher)
						// {
						// 		$count=0;	
						// 		$marriageType=json_decode($preacher->marriage_type);
						// 		foreach($marriageType as $type)
						// 		{
						// 			if($type==$manReqdata->marriage_type){
						// 				$count++;					
						// 			}
						// 		}
								
						// 		$service=json_decode($preacher->service);
						// 		foreach($service as $ser)
						// 		{
						// 			if('GC'.$ser==$girlData->user_city)
						// 			{
						// 				$count++;					
						// 			}
						// 		}
						// 		$religion=str_replace('M','G',$preacher->religion);
								
						// 		if($religion==$girlData->user_religion)
						// 		{
						// 			$count++;				
						// 		}
								
						// 	$preacherJson[]=array('preacherId'=>$preacher->preacher_id,'count'=>$count);	
						// }	
						// }
						// else
						// {
						// 	$manReqdata2=DB::table('man_request')->where('user_id',$boyData->user_id)->first();
						// 	$preacherData=DB::table('users')->where([['users.preacher','=','1'],['users.banAdmin','=','0']])->join('preacherDetails','users.id','=','preacherDetails.preacher_id')->get();
						// 	if(count($manReqdata2))
						// 	{
						// 	foreach($preacherData as $preacher)
						//     {
						// 		$count=0;	
						// 		$marriageType=json_decode($preacher->marriage_type);
						// 		foreach($marriageType as $type)
						// 		{
						// 			if($type==$manReqdata2->marriage_type){
						// 				$count++;					
						// 			}
						// 		}
								
						// 		$service=json_decode($preacher->service);
						// 		foreach($service as $ser)
						// 		{
						// 			if('GC'.$ser==$boyData->user_city)
						// 			{
						// 				$count++;					
						// 			}
						// 		}
						// 		$religion=str_replace('G','M',$preacher->religion);
								
						// 		if($religion==$boyData->user_religion)
						// 		{
						// 			$count++;				
						// 		}
								
						// 	$preacherJson[]=array('preacherId'=>$preacher->preacher_id,'count'=>$count);	
						//     }	
						// 	}
						// }
					
						// 	usort($preacherJson, array($this,'my_sort1'));
						// 	$json=current($preacherJson);
						// 	$messa=array('type'=>'notification1','sent'=>'admin','action'=>'3','username'=>'');			
						// 	$this->sendNotification($json['preacherId'],$messa);
						// 	$this->preacherMessage($json['preacherId'],$message);
						// 	// echo $json['preacherId'];
						// 	DB::table('preacher_requests')->insert(['preacher_id'=>$json['preacherId'],'request_id'=>$data['requestId'],'time'=>time(),'order_id'=>$data['orderId']]);
				
					
				// }			
			}
		}
		else if($data['action']=='2')
		{
			$userLoginPlatform4=DB::table('users')->select('loginPlatform','gcmToken')->where('id',$userData->user_id)->first();
			$userLoginPlatform5=DB::table('users')->select('loginPlatform','gcmToken')->where('id',$userData->request_id)->first();
			$message1=array('type'=>'notification1','sent'=>'admin','action'=>'0','username'=>'');
            if($userLoginPlatform4->loginPlatform=='Android')
            {
			$this->sendNotification($userLoginPlatform4->gcmToken,$message1);
		}
		else
		{
			$message6=array('title'=>'','body'=>'تم رفض الطلب بسبب عدم السداد','messageData'=>$message1);
			$this->send_notification_ios($userLoginPlatform4->gcmToken,$message6);
		}
		   if($userLoginPlatform5->loginPlatform=='Android')
            {
			$this->sendNotification($userLoginPlatform5->gcmToken,$message1);
		}
			else
		      {
		$message7=array('title'=>'','body'=>'تم رفض الطلب بسبب عدم السداد','messageData'=>$message1);
			$this->send_notification_ios($userLoginPlatform5->gcmToken,$message7);
		     }
			DB::table('requests')->where('id',$data['requestId'])->where('deactivateRequest','0')->update(['payment'=>'3']);
			if(count($userData)>0)
			{
					$this->userMessage($userData->user_id,'تم إلغاء طلبك من إداره وفًق لعدم السداد'.'\n'.'www.wuffiq.com');
					$this->userMessage($userData->request_id,'تم إلغاء طلبك من إداره وفًق لعدم السداد'.'\n'.'www.wuffiq.com');				
			}
			$check=DB::table('requests')->where('id',$data['requestId'])->where('deactivateRequest','0')->first();
			if(count($check))
			{
				$change1=DB::table('users')->where('id',$userData->user_id)->update(['deactivate'=>'0']);
				$change2=DB::table('users')->where('id',$userData->request_id)->update(['deactivate'=>'0']);
				DB::table('requests')->where('id',$data['requestId'])->update(['deactivateRequest'=>'1']);
			}
			
			
		}
		return Response::json(array('status'=>'1'));
	}
	
	
	public function userMessage($userId,$message)
	{
		$data=DB::table('users')->where('id',$userId)->first();
		$this->messages($data->phone_code,$data->phone,$message);
		
	}
	
	public function messages($phoneCode,$phoneNumber,$message){
	
	
	$number=$phoneCode.$phoneNumber;
	$client = new Services_Twilio('AC2d33b9dfdf71f2987d4541a0dbb4a2c0', '9812ac0f92962b4665adf79d2477e3f4');

	 try {
		// Use the Twilio REST API client to send a text message
		$m = $client->account->messages->sendMessage(
		  '+1 765-626-3110', // the text will be sent from your Twilio number
		  $number, // the phone number the text will be sent to
		  $message // the body of the text message
		);
	  } catch(Services_Twilio_RestException $e) {
		// Return and render the exception object, or handle the error as needed
		//  return $e;
	  };
		
		// return $m;
	}
	
	
	public function my_sort1($a, $b)
	{
		if ($a['count'] > $b['count']) {
			return -1;
		} else if ($a['count'] < $b['count']) {
			return 1;
		} else {
			return 0; 
		}
	}


	public function send_notification_ios($deviceToken,$message){


 // Put your private key's passphrase here:
  	    $passphrase = '123456';
		$nottype='123';
 ////////////////////////////////////////////////////////////////////////////
 		$ctx = stream_context_create();
 		stream_context_set_option($ctx, 'ssl', 'local_cert','/var/www/vhosts/wuffiq.com/httpdocs/webs/WuffiqPushCertificate.pem');
 		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
 // dd($ctx);
 // Open a connection to the APNS server
 		$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$fp)
		// exit("Failed to connect: $err $errstr" . PHP_EOL);
		// echo 'Connected to APNS' . PHP_EOL;
		ob_flush();
		flush();
 // Encode the payload as JSON
        $body['aps'] = array(
            // 'alert' => json_encode($message),
			'alert' =>array(
			"title"=>$message['title'],
			"body"=>$message['body']),
            'sound' => 'default',
            'badge' => 1,
			'content-available' => 1,
			'message'=>json_encode($message['messageData']),
        );

 		$payload = json_encode($body);
 // Build the binary notification
 		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
	
 // Send it to the server
 		stream_set_blocking ($fp,0);
 		$result = fwrite($fp, $msg, strlen($msg));
 		if (!$result)
 		{
  		// echo json_encode(array('status'=>'0')) . PHP_EOL;
 		 // echo $fp;
 		}
 		else
 		{
 		// echo json_encode(array('status'=>'1')) .PHP_EOL;
  		  ob_flush();
  		  flush();
 		}
 		// self::checkAppleErrorResponse($fp);
 		fclose($fp);

}



}