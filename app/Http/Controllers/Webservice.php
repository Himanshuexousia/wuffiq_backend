<?php

namespace App\Http\Controllers;

use App\User;

use App\Requ;

use Twilio;

use illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use App\Http\Requests;

use Illuminate\Support\Facades\Cache;
// use Aloha\Twilio\TwilioInterface;
 
use DB;

use app;

use File;

use Hash;

use Auth;

use Input;

use Route;

use Response;

use Services_Twilio;

use Eloquent;

use Validator;
// use Jose\Factory\JWSFactory;
class Req extends Eloquent{
	
	protected $table='requests';
	public $timestamps = false;
}

class Webservice extends Controller
{
	public function sanitizeInput($var) 
	{
	   $var = trim($var);
	   $var = stripslashes($var);
	   $var = htmlspecialchars($var);
	   $var = htmlentities($var);
	   return $var;
	}
	
	public function cityStatus(Request $request){
		$data = Input::all();
		if(!empty($data['data'])&&$data['data']=='Request')
		{
			$data=DB::table('city_status')->where('id','!=',120)->where('id','!=',121)->orderBy('id','Asc')->get();
			$users1 = DB::table('city_status')->where('id',121)->first();	
			array_unshift($data,$users1);		
		}
		else
		{	
			$data=DB::table('city_status')->where('id','!=',121)->orderBy('id','Asc')->get();
		}
		
		
		return Response::json(array('status'=>'0','allResponse'=>$data));
	
	}
	
	public function qabelahName(Request $request)
	{
		$data = Input::all();
		if(!empty($data['data'])&&$data['data']=='Request')
		{
			$users = DB::table('Qabelah_Name')->where('id','!=',97)->where('id','!=',98)->select('id','QabelahName as qabelah_name')->get();
			// dd($users);
			$users1 = DB::table('Qabelah_Name')->where('id',98)->select('id','QabelahName as qabelah_name')->first();	
			array_unshift($users,$users1);		
		}
		else
		{	
			$users = DB::table('Qabelah_Name')->where('id','!=',98)->select('id','QabelahName as qabelah_name')->get();
		}
		$json=array("status"=>"1","message"=>"Qabelah Names","response"=>$users);
		return Response::json($json);
	}
	
	public function sendOtp(){

// $account_sid = "AC2d33b9dfdf71f2987d4541a0dbb4a2c0"; // Your Twilio account sid
// $auth_token = "9812ac0f92962b4665adf79d2477e3f4"; // Your Twilio auth token
// $fromNumber='+1 765-626-3110';
// Twilio::message('+919996968968', 'ds');
// $twilio = new Aloha\Twilio\Twilio($account_sid,$auth_token,$fromNumber);
	// $sdk = $twilio->getTwilio();
	// Twilio::message('+919996968968', 'Pink Elephants and Happy Rainbows');	
	
	}
	
	
	
public function userRegister(Request $request){
		

$userPhone=$this->validError($this->sanitizeInput($request->input('userPhone')),'userPhone');

$phoneCode=$this->validError($this->sanitizeInput($request->input('phoneCode')),'phoneCode');
		
$userGender=$this->validError($this->sanitizeInput($request->input('userGender')),'userGender');

$userName=$this->validError($this->sanitizeInput($request->input('userName')),'userName');

$userEmail=$this->sanitizeInput($request->input('userEmail'));

$marriedStatus=$this->validError($this->sanitizeInput($request->input('marriedStatus')),'marriedStatus');

$qabelahStatus=$this->validError($this->sanitizeInput($request->input('qabelahStatus')),'qabelahStatus');

if($userGender=='Male' && $qabelahStatus=='MQQ')
{
	$qabelahStatus='MQ1';
}
else if($userGender=='Female' && $qabelahStatus=='GQQ')
{
	$qabelahStatus='GQ1';
}
 
// if($qabelahStatus=='MQQ' || $qabelahStatus=='GQQ'){
// $qabelahName=$this->validError($this->sanitizeInput($request->input('qabelahName')),'qabelahName');		
// }
// else{
// $qabelahName='';	
// } 

$qabelahName=$this->sanitizeInput($request->input('qabelahName'));	

$userAge=$this->validError($this->sanitizeInput($request->input('userAge')),'userAge');

$userEducation=$this->validError($this->sanitizeInput($request->input('userEducation')),'userEducation');

$userHeight=$this->validError($this->sanitizeInput($request->input('userHeight')),'userHeight');

$userWeight=$this->validError($this->sanitizeInput($request->input('userWeight')),'userWeight');

$userNationality=$this->validError($this->sanitizeInput($request->input('userNationality')),'userNationality');

$userCity=$this->validError($this->sanitizeInput($request->input('userCity')),'userCity');

$userReligion=$this->validError($this->sanitizeInput($request->input('userReligion')),'userReligion');

$userHairColor=$this->validError($this->sanitizeInput($request->input('userHairColor')),'userHairColor');

if($userGender=='Male'){
$hairLook='';	
}
else{
$hairLook=$this->validError($this->sanitizeInput($request->input('hairLookStatus')),'hairLookStatus');
}
$userBodyColor=$this->validError($this->sanitizeInput($request->input('userBodyColor')),'userBodyColor');

$userProfession=$this->validError($this->sanitizeInput($request->input('userProfession')),'userProfession');

$childernStatus=0;	


if($marriedStatus=='MS2'){
$marryReason=$this->validError($this->sanitizeInput($request->input('marryReason')),'marryReason');		
}
else{
$marryReason='';	
}
	
$token=$this->validError($this->sanitizeInput($request->input('token')),'token');

$devicePlatform=$this->validError($this->sanitizeInput($request->input('devicePlatform')),'devicePlatform');

$session=sha1(date('Y-m-d H:i:s'));

$authorization='exousia'.uniqid();

$dataExists=DB::table('users')->where('phone',$userPhone)->get();
if(count($dataExists)>0){
return Response::json(array('status'=>'0'));	
}
else{
$userId=DB::table('users')->insertGetId(['username' =>$userName , 'email' => $userEmail,'phone_code'=>$phoneCode,'phone'=> $userPhone,'gender'=>$userGender,'gcmToken'=>$token,'registerType'=>'mobile','loginPlatform'=>$devicePlatform,'regDate'=>time(),'sessiontime'=>$session,'authorization'=>$authorization]);	
	
$data=DB::table('userDetails')->insert(['user_id'=>$userId,'married_status'=>$marriedStatus,'marry_reason'=>$marryReason,'qubelah_status'=>$qabelahStatus,'qubelah_name'=>$qabelahName,'user_age'=>$userAge,'user_education'=>$userEducation,'user_height'=>$userHeight,'user_weight'=>$userWeight,'user_nationality'=>$userNationality,'user_city'=>$userCity,'user_religion'=>$userReligion,'hair_color'=>$userHairColor,'hair_look'=>$hairLook,'body_color'=>$userBodyColor,'user_profession'=>$userProfession,'children_status'=>$childernStatus]);
return Response::json(array('status'=>'1','userId'=>$userId));
}

}
	
	
public function validError($data,$type){
	
if($data==''){	
$array=array('errorData'=>$type,'errorStatus'=>'blank');
echo json_encode(array('status'=>'2','response'=>$array,'errorData'=>$type));
die();
}
else{		
return $data;	
}

}
	
public function countryPhoneCode(Request $request){
		
$data=DB::table('user_country')->get();		
	
return Response::json(array('status'=>'0','message'=>'phoneCode','response'=>$data));
	
}
	
	
public function countryFlags(Request $request){
		
$type=$request->input('type');
if($type=='register'){		
$data=DB::table('country_flag')->where([['country_id','!=','24'],['country_id','!=','22']])->orderBy('country_id','ASC')->get();
}
elseif($type='request'){
$data=DB::table('country_flag')->where([['country_id','!=','23'],['country_id','!=','22']])->orderBy('country_id','ASC')->get();
$newData=(object)array('flag_id'=> 29,
      'country_name'=> 'Kaligi',
      'arabic_country_name'=>'خليجي',
      'country_id'=>22,
      'country_flag'=>'khaleej-min.png',
      'man'=>'',
      'girl'=>'',
	  'girl_arabic_country_name'=>'خليجية'
);
array_unshift($data,$newData);	
}
	
return Response::json(array('status'=>'0','message'=>'countryFlags','response'=>$data));	
		
}
	
public function userRegisterFull(Request $request){

$userId=$this->validError($this->sanitizeInput($request->input('userId')),'userId');

$userGender=$this->validError($this->sanitizeInput($request->input('userGender')),'userGender');

$userImage=$this->sanitizeInput($request->input('userImage'));

$userName=$this->validError($this->sanitizeInput($request->input('userName')),'userName');

$userEmail=$this->sanitizeInput($request->input('userEmail'));

$marriedStatus=$this->validError($this->sanitizeInput($request->input('marriedStatus')),'marriedStatus');

$qabelahStatus=$this->validError($this->sanitizeInput($request->input('qabelahStatus')),'qabelahStatus');

// if($qabelahStatus=='MQQ' || $qabelahStatus=='GQQ'){
// $qabelahName=$this->validError($this->sanitizeInput($request->input('qabelahName')),'qabelahName');		
// }
// else{
// $qabelahName='';	
// }
if($userGender=='Male' && $qabelahStatus=='MQQ')
{
	$qabelahStatus='MQ1';
}
else if($userGender=='Female' && $qabelahStatus=='GQQ')
{
	$qabelahStatus='GQ1';
}

$qabelahName=$this->sanitizeInput($request->input('qabelahName'));


$userAge=$this->validError($this->sanitizeInput($request->input('userAge')),'userAge');

$userEducation=$this->validError($this->sanitizeInput($request->input('userEducation')),'userEducation');

$userHeight=$this->validError($this->sanitizeInput($request->input('userHeight')),'userHeight');

$userWeight=$this->validError($this->sanitizeInput($request->input('userWeight')),'userWeight');

$userNationality=$this->validError($this->sanitizeInput($request->input('userNationality')),'userNationality');

$userCity=$this->validError($this->sanitizeInput($request->input('userCity')),'userCity');

$userReligion=$this->validError($this->sanitizeInput($request->input('userReligion')),'userReligion');

$userHairColor=$this->validError($this->sanitizeInput($request->input('userHairColor')),'userHairColor');

$userBodyColor=$this->validError($this->sanitizeInput($request->input('userBodyColor')),'userBodyColor');

$userProfession=$this->validError($this->sanitizeInput($request->input('userProfession')),'userProfession');
 
// if($marriedStatus=='MS1' || $marriedStatus=='GS1'){
// $childernStatus=0;
// }
// else{
$childernStatus=$this->validError($this->sanitizeInput($request->input('childernStatus')),'childernStatus');	
// }

if($marriedStatus=='MS2'){
$marryReason=$this->validError($this->sanitizeInput($request->input('marryReason')),'marryReason');		
}
else{
$marryReason='';	
}
	
$token=$this->validError($this->sanitizeInput($request->input('token')),'token');

$devicePlatform=$this->validError($this->sanitizeInput($request->input('devicePlatform')),'devicePlatform');

/* new fields*/

$userHobbies=$this->validError($request->input('userHobbies'),'userHobbies');

$userIncomeLevel=$this->validError($this->sanitizeInput($request->input('userIncomeLevel')),'userIncomeLevel');

$religiousStatus=$this->validError($this->sanitizeInput($request->input('religiousStatus')),'religiousStatus');

$traditionalStatus=$this->validError($this->sanitizeInput($request->input('traditionalStatus')),'traditionalStatus');

$lookStatus=$this->validError($this->sanitizeInput($request->input('lookStatus')),'lookStatus');

$bodyStatus=$this->validError($this->sanitizeInput($request->input('bodyStatus')),'bodyStatus');

$smokeStatus=$this->validError($this->sanitizeInput($request->input('smokeStatus')),'smokeStatus');

$healthStatus=$this->validError($this->sanitizeInput($request->input('healthStatus')),'healthStatus');

if($healthStatus=='MF2' || $healthStatus=='GF2'){
$littleSickType=$this->validError($this->sanitizeInput($request->input('littleSickType')),'littleSickType');		
}
else{
$littleSickType='';	
}

if($healthStatus=='MF3' || $healthStatus=='GF3'){
$disableType=$this->validError($this->sanitizeInput($request->input('disableType')),'disableType');		
}
else{
$disableType='';	
}

$otherInformation=$this->validError($this->sanitizeInput($request->input('otherInformation')),'otherInformation');

$privateInformation=$this->sanitizeInput($request->input('privateInformation'));

$preatureStatus=$this->validError($this->sanitizeInput($request->input('preatureStatus')),'preatureStatus');

if($preatureStatus=='Yes'||$preatureStatus=='نعم'){
	
$preatureAccount=$this->validError($this->sanitizeInput($request->input('preatureAccount')),'preatureAccount');	

}
else{

$preatureAccount='';	
	
}
		


if($userGender=='Male'){
	
$beardStatus=$this->validError($this->sanitizeInput($request->input('beardStatus')),'beardStatus');

$arabiaStatus=$this->validError($this->sanitizeInput($request->input('arabiaStatus')),'arabiaStatus');

$hairLookStatus='';

$veilStatus='';

$virginStatus='';

$smsNumber='';


}
else{
	
$beardStatus='';
	
$arabiaStatus='';	

$hairLookStatus=$this->validError($this->sanitizeInput($request->input('hairLookStatus')),'hairLookStatus');

$virginStatus=$this->validError($this->sanitizeInput($request->input('virginStatus')),'virginStatus');

$veilStatus=$this->validError($this->sanitizeInput($request->input('veilStatus')),'veilStatus');

$smsNumber=$this->validError($this->sanitizeInput($request->input('smsNumber')),'smsNumber');
	
}

$session=sha1(date('Y-m-d H:i:s'));

$user=DB::table('users')->where('id',$userId)->first();
// if($user->user_registration==1){
	
// return Response::json(array('status'=>'0'));	
	
// }
// else{
	
$data=DB::table('users')->where('id',$userId)->update(['username' =>$userName , 'email' => $userEmail,'gender'=>$userGender,'image'=>$userImage,'gcmToken'=>$token,'registerType'=>'mobile','loginPlatform'=>$devicePlatform,'sessiontime'=>$session,'user_registration'=>'1']);	
	
$data1=DB::table('userDetails')->where('user_id',$userId)->update(['married_status'=>$marriedStatus,'marry_reason'=>$marryReason,'qubelah_status'=>$qabelahStatus,'qubelah_name'=>$qabelahName,'user_age'=>$userAge,'user_education'=>$userEducation,'user_height'=>$userHeight,'user_weight'=>$userWeight,'user_nationality'=>$userNationality,'user_city'=>$userCity,'user_religion'=>$userReligion,'hair_color'=>$userHairColor,'body_color'=>$userBodyColor,'hair_look'=>$hairLookStatus,'user_profession'=>$userProfession,'children_status'=>$childernStatus]);

$allData=DB::table('userDetailsFull')->where('user_id',$userId)->get();
if(count($allData)==0){
$data2=DB::table('userDetailsFull')->insert(['user_id'=>$userId,'user_hobbies'=>$userHobbies,'user_income'=>$userIncomeLevel,'user_religious'=>$religiousStatus,'user_traditional'=>$traditionalStatus,'look_status'=>$lookStatus,'body_status'=>$bodyStatus,'smoke_status'=>$smokeStatus,'health_status'=>$healthStatus,'little_sick_type'=>$littleSickType,'disable_type'=>$disableType,'other_information'=>$otherInformation,'private_information'=>$privateInformation,'preature_status'=>$preatureStatus,'preature_account'=>$preatureAccount,'beard_status'=>$beardStatus,'arabia_status'=>$arabiaStatus,'veil_status'=>$veilStatus,'virgin_status'=>$virginStatus,'sms_number'=>$smsNumber]);

DB::table('notification')->insert(['type'=>'registered','text'=>'New user registered','url'=>'all-users','logo'=>'fa fa-user text-aqua']);
}
else{
	
$data2=DB::table('userDetailsFull')->where('user_id',$userId)->update(['user_hobbies'=>$userHobbies,'user_income'=>$userIncomeLevel,'user_religious'=>$religiousStatus,'user_traditional'=>$traditionalStatus,'look_status'=>$lookStatus,'body_status'=>$bodyStatus,'smoke_status'=>$smokeStatus,'health_status'=>$healthStatus,'little_sick_type'=>$littleSickType,'disable_type'=>$disableType,'other_information'=>$otherInformation,'private_information'=>$privateInformation,'preature_status'=>$preatureStatus,'preature_account'=>$preatureAccount,'beard_status'=>$beardStatus,'arabia_status'=>$arabiaStatus,'veil_status'=>$veilStatus,'virgin_status'=>$virginStatus,'sms_number'=>$smsNumber]);	
	
}
$userData=DB::table('users')->where('id',$userId)->first();

echo json_encode(array('status'=>'1','response'=>$userData));

// }

		
}

public function manRequest(Request $request){

$userId=$this->validError($request->input('userId'),'userId');

$marriageType=$this->validError($request->input('marriageType'),'marriageType');
	
$marriageTypePrefer=$this->validError($request->input('marriageTypePrefer'),'marriageTypePrefer');

$qabelahStatus=$this->validError($request->input('qabelahStatus'),'qabelahStatus');

// if($qabelahStatus=='MQQ' || $qabelahStatus=='GQQ'){
$qabelahName=$request->input('qabelahName');		
// }
// else{
// $qabelahName='';	
// }

$userAgeMin=$this->validError($request->input('userAgeMin'),'userAgeMin');

$userAgeMax=$this->validError($request->input('userAgeMax'),'userAgeMax');

$userEducation=$this->validError($request->input('userEducation'),'userEducation');

$userHeightMin=$this->validError($request->input('userHeightMin'),'userHeightMin');
$userHeightMax=$this->validError($request->input('userHeightMax'),'userHeightMax');

$userWeightMin=$this->validError($request->input('userWeightMin'),'userWeightMin');
$userWeightMax=$this->validError($request->input('userWeightMax'),'userWeightMax');
	
$userNationality=$this->validError($request->input('userNationality'),'userNationality');

$userCity=$this->validError($request->input('userCity'),'userCity');

$userReligion=$this->validError($request->input('userReligion'),'userReligion');

$userHairColor=$this->validError($request->input('userHairColor'),'userHairColor');

$userHairLook=$this->validError($request->input('userHairLook'),'userHairLook');

$userBodyColor=$this->validError($request->input('userBodyColor'),'userBodyColor');

$userProfession=$this->validError($request->input('userProfession'),'userProfession');

$manRequests=DB::table('man_request')->where('user_id',$userId)->get();
if(count($manRequests)>0){

return Response::json(array('status'=>'0'));
	
}
else
{
$data=DB::table('man_request')->insert(['user_id'=>$userId,'marriage_type'=>$marriageType,'marriage_type_prefer'=>$marriageTypePrefer,'qubelah_status'=>$qabelahStatus,'qubelah_name'=>$qabelahName,'user_age_min'=>$userAgeMin,'user_age_max'=>$userAgeMax,'user_education'=>$userEducation,'user_height_min'=>$userHeightMin,'user_height_max'=>$userHeightMax,'user_weight_min'=>$userWeightMin,'user_weight_max'=>$userWeightMax,'user_nationality'=>$userNationality,'user_city'=>$userCity,'user_religion'=>$userReligion,'user_hair_color'=>$userHairColor,'user_hair_look'=>$userHairLook,'user_body_color'=>$userBodyColor,'user_profession'=>$userProfession]);

return Response::json(array('status'=>'1'));
}
}

public function manRequestFull(Request $request){


$userId=$this->validError($request->input('userId'),'userId');

$marriageType=$this->validError($request->input('marriageType'),'marriageType');
	
$marriageTypePrefer=$this->validError($request->input('marriageTypePrefer'),'marriageTypePrefer');

$qabelahStatus=$this->validError($request->input('qabelahStatus'),'qabelahStatus');

// if($qabelahStatus=='MQQ' || $qabelahStatus=='GQQ'){
$qabelahName=$request->input('qabelahName');		
// }
// else{
// $qabelahName='';	
// }

$userAgeMin=$this->validError($request->input('userAgeMin'),'userAgeMin');

$userAgeMax=$this->validError($request->input('userAgeMax'),'userAgeMax');

$userEducation=$this->validError($request->input('userEducation'),'userEducation');

$userHeightMin=$this->validError($request->input('userHeightMin'),'userHeightMin');

$userHeightMax=$this->validError($request->input('userHeightMax'),'userHeightMax');

$userWeightMin=$this->validError($request->input('userWeightMin'),'userWeightMin');

$userWeightMax=$this->validError($request->input('userWeightMax'),'userWeightMax');
	
$userNationality=$this->validError($request->input('userNationality'),'userNationality');

$userCity=$this->validError($request->input('userCity'),'userCity');

$userReligion=$this->validError($request->input('userReligion'),'userReligion');

$userHairColor=$this->validError($request->input('userHairColor'),'userHairColor');

$userHairLook=$this->validError($request->input('userHairLook'),'userHairLook');

$userBodyColor=$this->validError($request->input('userBodyColor'),'userBodyColor');

$userProfession=$this->validError($request->input('userProfession'),'userProfession');
	
$userHobbies=$this->validError($request->input('userHobbies'),'userHobbies');

$userIncomelevel=$this->validError($request->input('userIncomelevel'),'userIncomelevel');

$fertileStatus=$request->input('fertileStatus');

$homeAfterMarriage=$this->validError($request->input('homeAfterMarriage'),'homeAfterMarriage');

$userReligious=$this->validError($request->input('userReligious'),'userReligious');

$traditionalStatus=$this->validError($request->input('traditionalStatus'),'traditionalStatus');

$lookStatus=$this->validError($request->input('lookStatus'),'lookStatus');

$bodyStatus=$this->validError($request->input('bodyStatus'),'bodyStatus');

$smokeStatus=$this->validError($request->input('smokeStatus'),'smokeStatus');

$healthStatus=$this->validError($request->input('healthStatus'),'healthStatus');

$veilStatus=$request->input('veilBeardStatus');

$wifeJob=$request->input('wifeJob');

$childernStatus=$this->validError($request->input('childernStatus'),'childernStatus') ;

$manRequest=DB::table('man_request')->where('user_id',$userId)->first();
if($manRequest->man_request_status==1){

$data=DB::table('man_request')->where('user_id',$userId)->update(['marriage_type'=>$marriageType,'marriage_type_prefer'=>$marriageTypePrefer,'qubelah_status'=>$qabelahStatus,'qubelah_name'=>$qabelahName,'user_age_min'=>$userAgeMin,'user_age_max'=>$userAgeMax,'user_education'=>$userEducation,'user_height_min'=>$userHeightMin,'user_height_max'=>$userHeightMax,'user_weight_min'=>$userWeightMin,'user_weight_max'=>$userWeightMax,'user_nationality'=>$userNationality,'user_city'=>$userCity,'user_religion'=>$userReligion,'user_hair_color'=>$userHairColor,'user_hair_look'=>$userHairLook,'user_body_color'=>$userBodyColor,'user_profession'=>$userProfession,'man_request_status'=>'1']);

$data1=DB::table('man_request_full')->where('user_id',$userId)->update(['user_hobbies'=>$userHobbies,'user_income'=>$userIncomelevel,'fertile_status'=>$fertileStatus,'home_after_marriage'=>$homeAfterMarriage,'user_religious'=>$userReligious,'traditional_status'=>$traditionalStatus,'look_status'=>$lookStatus,'body_status'=>$bodyStatus,'smoke_status'=>$smokeStatus,'health_status'=>$healthStatus,'veil_status'=>$veilStatus,'wife_job'=>$wifeJob,'children_status'=>$childernStatus]);

return Response::json(array('status'=>'1'));

}
else{
$data=DB::table('man_request')->where('user_id',$userId)->update(['marriage_type'=>$marriageType,'marriage_type_prefer'=>$marriageTypePrefer,'qubelah_status'=>$qabelahStatus,'qubelah_name'=>$qabelahName,'user_age_min'=>$userAgeMin,'user_age_max'=>$userAgeMax,'user_education'=>$userEducation,'user_height_min'=>$userHeightMin,'user_height_max'=>$userHeightMax,'user_weight_min'=>$userWeightMin,'user_weight_max'=>$userWeightMax,'user_nationality'=>$userNationality,'user_city'=>$userCity,'user_religion'=>$userReligion,'user_hair_color'=>$userHairColor,'user_hair_look'=>$userHairLook,'user_body_color'=>$userBodyColor,'user_profession'=>$userProfession,'man_request_status'=>'1']);

$data1=DB::table('man_request_full')->insert(['user_id'=>$userId,'user_hobbies'=>$userHobbies,'user_income'=>$userIncomelevel,'fertile_status'=>$fertileStatus,'home_after_marriage'=>$homeAfterMarriage,'user_religious'=>$userReligious,'traditional_status'=>$traditionalStatus,'look_status'=>$lookStatus,'body_status'=>$bodyStatus,'smoke_status'=>$smokeStatus,'health_status'=>$healthStatus,'veil_status'=>$veilStatus,'wife_job'=>$wifeJob,'children_status'=>$childernStatus]);
	
return Response::json(array('status'=>'1'));
}	
}




public function getUserById($id){
		
$data=DB::table('users')->where('id',$id)->first();

$json['userId']=$data->id;
$json['userName']=$data->username;
$json['userEmail']=$data->email;
$json['userPhone']=$data->phone;
$json['userGender']=$data->gender;
$json['userImage']=$data->image;
$json['banAdmin']=$data->banAdmin;
$json['deActive']=$data->deactivate;
$json['deleteStatus']=$data->deleteStatus;
$json['registerType']=$data->registerType;
$json['loginPlatform']=$data->loginPlatform;
$json['regDate']=$data->regDate;
$json['token']=$data->gcmToken;
$json['sessionTime']=$data->sessiontime;
$json['authorization']=$data->authorization;
$json['userRegistration']=$data->user_registration;
if($data->user_registration==1){
	
	$data1=DB::table('userDetails')->where('user_id',$id)->first();
	$json['marriageStatus']=$data1->married_status;
	$json['marriageReason']=$data1->marry_reason;
	$json['qubelahStatus']=$data1->qubelah_status;
	$json['qubelahName']=$data1->qubelah_name;
	$json['userAge']=$data1->user_age;
	$json['userEducation']=$data1->user_education;
	$json['userHeight']=$data1->user_height;
	$json['userWeight']=$data1->user_weight;
	$json['userNationality']=$this->getCodeById($data1->user_nationality);
	$flagName=DB::table('country_flag')->where('man',$data1->user_nationality)->orWhere('girl',$data1->user_nationality)->first();
	$json['flagName']=$flagName->country_flag;
	$city=$this->getCodeById($data1->user_city);
	$data5=DB::table('city_status')->select('arabic_status')->where('status',$city)->first();
	$json['userCity']=$data5->arabic_status;
	$json['userReligion']=$data1->user_religion;
	$json['userHairColor']=$data1->hair_color;
	$json['userBodyColor']=$data1->body_color;
	$json['userProfession']=$data1->user_profession;
	if($data1->children_status!='0'){
	$json['childernStatus']=$data1->children_status;
	}
	else{
	$json['childernStatus']='No Children';			
	}
	$data2=DB::table('userDetailsFull')->where('user_id',$id)->first();
	
	$json['userHobbies']=json_decode($data2->user_hobbies);
	$json['userIncome']=$data2->user_income;
	$json['userReligious']=$data2->user_religious;
	$json['userTraditional']=$data2->user_traditional;
	$json['lookStatus']=$data2->look_status;
	$json['bodyStatus']=$data2->body_status;
	$json['smokeStatus']=$data2->smoke_status;
	$json['healthStatus']=$data2->health_status;
	$json['littleSickType']=$data2->little_sick_type;
	$json['disableType']=$data2->disable_type;
	$json['otherInformation']=$data2->other_information;
	$json['privateInformation']=$data2->private_information;
	$json['preatureStatus']=$data2->preature_status;
	$json['preatureAccount']=$data2->preature_account;
	
	$json['arabiaStatus']=$data2->arabia_status;
	
	if($data->gender=='Female'){
	$json['hairLookStatus']=$data1->hair_look;
	$json['veilStatus']=$data2->veil_status;
	$json['virginStatus']=$data2->virgin_status;
	$json['smsNumber']=$data2->sms_number;
	}
	else{		
	$json['beardStatus']=$data2->beard_status;	
	}
}
return $json;
}


public function manRequestProfile(Request $request){
	
$userId=$this->validError($this->sanitizeInput($request->input('userId')),'userId');
$jsonFlag=array();
$jsonCity=array();
$data=DB::table('man_request')->where('user_id',$userId)->first();
if(count($data)>0){
	
$json['marriageType']=$data->marriage_type;
$json['marriageTypePrefer']=json_decode($data->marriage_type_prefer,true);
$json['qubelahStatus']=json_decode($data->qubelah_status,true);
$json['qubelahName']=json_decode($data->qubelah_name,true);
$json['minAge']=$data->user_age_min;
$json['maxAge']=$data->user_age_max;
$json['lookWomenEducationList']=json_decode($data->user_education,true);
$json['heightMin']=$data->user_height_min;
$json['heightMax']=$data->user_height_max;
$json['weightMin']=$data->user_weight_min;
$json['weightMax']=$data->user_weight_max;
$nation=json_decode($data->user_nationality,true);

if(count($nation))
{
foreach($nation as $nations){
	
// $flag=DB::table('country_flag')->where('man',$nations)->orwhere('girl',$nations)->first();
if(DB::table('country_flag')->where('man',$nations)->first())
{
	 $flag=DB::table('country_flag')->select('arabic_country_name','country_flag')->where('man',$nations)->first();	
	 $jsonFlag[]=array('countryName'=>$flag->arabic_country_name,'countryFlag'=>$flag->country_flag);
}

else if(DB::table('country_flag')->where('girl',$nations)->first())
{
   $flag=DB::table('country_flag')->select('girl_arabic_country_name','country_flag')->where('girl',$nations)->first();	
   $jsonFlag[]=array('countryName'=>$flag->girl_arabic_country_name,'countryFlag'=>$flag->country_flag);
}


	
}
}

$json['nationality']=$jsonFlag;
$city=json_decode($data->user_city);
if(count($city))
{
foreach($city as $cities){
	
$cty=DB::table('city_status')->where('man',$cities)->orwhere('girl',$cities)->first();

if(count($cty))
{
$jsonCity[]=$cty->arabic_status;	
}

	
}
}

$json['city']=$jsonCity;
$json['religion']=json_decode($data->user_religion);
$json['hairColor']=json_decode($data->user_hair_color);
$json['hairLook']=json_decode($data->user_hair_look);
$json['bodyColor']=json_decode($data->user_body_color);
$json['job']=json_decode($data->user_profession);
$data1=DB::table('man_request_full')->where('user_id',$userId)->first();
$json['hobbies']=json_decode($data1->user_hobbies);	
$json['incomeLevel']=json_decode($data1->user_income);	
$json['fertileStatus']=$data1->fertile_status;	
$json['homeAfterMarriage']=json_decode($data1->home_after_marriage);	
$json['religious']=json_decode($data1->user_religious);	
$json['traditional']=json_decode($data1->traditional_status);	
$json['look']=json_decode($data1->look_status);	
$json['body']=json_decode($data1->body_status);	
$json['smoke']=$data1->smoke_status;	
$json['health']=json_decode($data1->health_status);	
$json['veil']=json_decode($data1->veil_status);		
$json['wifeJobAccept']=json_decode($data1->wife_job);	
$json['children']=json_decode($data1->children_status);	
return Response::json(array('status'=>'1','preferenceResponse'=>$json));	
	
}
else{
	
return Response::json(array('status'=>'0'));
	
}
	
	
}

public function getCodeById($code){
	$codeData=DB::table('codes')->where('code',$code)->first();
	
	return $codeData->answer;
}


public function userRegisteredProfile(Request $request){
	
	$ownId=$this->sanitizeInput($request->input('ownId'));
	$userId=$this->validError($this->sanitizeInput($request->input('userId')),'userId');
	$json=$this->getUserById($userId);
	if($ownId!=''){
		
	$json['pokeCount']=$this->requestCount($ownId,$userId,'poke');
	$json['marriageRequestCount']=$this->requestCount($ownId,$userId,'marriageRequest');
	$json['pokeStatus']='0';
	$json['favStatus']='0';
	$json['marriageRequestStatus']='0';
	$json['status']='0';
	$json['pstatus']='0';
	
	$status=DB::table('requests')->where([['user_id','=',$ownId],['request_id','=',$userId],['deactivateRequest','=','0']])->get();
	foreach($status as $stat){
		if($stat->type=='fav'){
		$json['favStatus']='1';	
		$json['status']='0';	
		}
		elseif($stat->type=='poke'){
		$json['pokeStatus']='1';	
		$json['pstatus']=$stat->status;	
		}
		elseif($stat->type=='marriageRequest'){
		$json['marriageRequestStatus']='1';	
		$json['status']=$stat->status;	
		}		
	}
	}	
	return Response::json($json);
}


public function filterTest(){
$json=array();	
$userId=Input::get('userId');	
$count=0;	
$myProfileData=DB::table('users')->join('userDetails','userDetails.user_id','=','users.id')->join('userDetailsFull','userDetailsFull.user_id','=','users.id')->where('users.id',$userId)->first();
$myRequestData=DB::table('users')->join('man_request','man_request.user_id','=','users.id')->join('man_request_full','man_request_full.user_id','=','users.id')->where('users.id',$userId)->first();

 // dd($myRequestData);

 
 
if(count($myProfileData)>0 && count($myRequestData)>0)
{
	$profileOf=($myProfileData->gender=='Male')? "Female" : "Male";
	
	// $userFullData=DB::table('users')->select('users.*','userDetails.*','userDetailsFull.*','man_request.marriage_type as marriage_type_req','man_request.marriage_type_prefer as marriage_type_prefer_req','man_request.qubelah_status as qubelah_status_req','man_request.qubelah_name as qubelah_name_req','man_request.user_age_min as user_age_min_req','man_request.user_age_max as user_age_max_req','man_request.user_education as user_education_req','man_request.user_height_min as user_height_min_req','man_request.user_height_max as user_height_max_req','man_request.user_weight_min as user_weight_min_req','man_request.user_weight_max as user_weight_max_req','man_request.user_nationality as user_nationality_req','man_request.user_city as user_city_req','man_request.user_religion as user_religion_req','man_request.user_hair_color as user_hair_color_req','man_request.user_hair_look as user_hair_look_req','man_request.user_body_color as user_body_color_req','man_request.user_profession as user_profession_req','man_request_full.user_hobbies as user_hobbies_req','man_request_full.user_income as user_income_req','man_request_full.fertile_status as fertile_status_req','man_request_full.home_after_marriage as home_after_marriage_req','man_request_full.user_religious as user_religious_req','man_request_full.traditional_status as traditional_status_req','man_request_full.look_status as look_status_req','man_request_full.body_status as body_status_req','man_request_full.smoke_status as smoke_status_req','man_request_full.health_status as health_status_req','man_request_full.veil_status as veil_status_req','man_request_full.wife_job as wife_job_req','man_request_full.children_status as children_status_req')->join('userDetails','userDetails.user_id','=','users.id')->join('userDetailsFull','userDetailsFull.user_id','=','users.id')->join('man_request','man_request.user_id','=','users.id')->join('man_request_full','man_request_full.user_id','=','users.id')->where([['users.user_registration','=','1'],['users.preacher','=','0'],['banAdmin','=','0'],['deactivate','=','0'],['man_request.man_request_status','=','1']])->where(DB::raw('CASE WHEN users.vip="'.$myProfileData->vip.'" THEN 1 ELSE 0 END'),1)->where(DB::raw('CASE WHEN users.gender="'.$myProfileData->gender.'" THEN 0 ELSE 1 END'),1)->get();	
	$userFullData=DB::table('users')->select('users.*','userDetails.*','userDetailsFull.*','man_request.marriage_type as marriage_type_req','man_request.marriage_type_prefer as marriage_type_prefer_req','man_request.qubelah_status as qubelah_status_req','man_request.qubelah_name as qubelah_name_req','man_request.user_age_min as user_age_min_req','man_request.user_age_max as user_age_max_req','man_request.user_education as user_education_req','man_request.user_height_min as user_height_min_req','man_request.user_height_max as user_height_max_req','man_request.user_weight_min as user_weight_min_req','man_request.user_weight_max as user_weight_max_req','man_request.user_nationality as user_nationality_req','man_request.user_city as user_city_req','man_request.user_religion as user_religion_req','man_request.user_hair_color as user_hair_color_req','man_request.user_hair_look as user_hair_look_req','man_request.user_body_color as user_body_color_req','man_request.user_profession as user_profession_req','man_request_full.user_hobbies as user_hobbies_req','man_request_full.user_income as user_income_req','man_request_full.fertile_status as fertile_status_req','man_request_full.home_after_marriage as home_after_marriage_req','man_request_full.user_religious as user_religious_req','man_request_full.traditional_status as traditional_status_req','man_request_full.look_status as look_status_req','man_request_full.body_status as body_status_req','man_request_full.smoke_status as smoke_status_req','man_request_full.health_status as health_status_req','man_request_full.veil_status as veil_status_req','man_request_full.wife_job as wife_job_req','man_request_full.children_status as children_status_req')->join('userDetails','userDetails.user_id','=','users.id')->join('userDetailsFull','userDetailsFull.user_id','=','users.id')->join('man_request','man_request.user_id','=','users.id')->join('man_request_full','man_request_full.user_id','=','users.id')->where([['users.user_registration','=','1'],['users.preacher','=','0'],['banAdmin','=','0'],['deactivate','=','0'],['man_request.man_request_status','=','1']])->where(DB::raw('CASE WHEN users.gender="'.$myProfileData->gender.'" THEN 0 ELSE 1 END'),1)->get();

// dd($userFullData);
		foreach($userFullData as $userAllData){
			$point=0;
             
			 
			$point=($myRequestData->marriage_type==$userAllData->marriage_type_req)? $point+1 : $point;
			
			
			// $point=in_array($userAllData->user_religion,$userReligion)? $point+1 : $point;
            // $userReligionReq=json_decode($userAllData->user_religion_req);
			// $point=in_array($myProfileData->user_religion,$userReligionReq)? $point+1 : $point;
			if($profileOf=='Male')
			{
               $userReligionReq=str_replace('G','M',$userAllData->user_religion);
			   $healthStatusReq=str_replace('G','M',json_decode($userAllData->health_status_req));
			   $userMarriageTypeReq=str_replace('G','M',json_decode($userAllData->marriage_type_prefer_req));
			   $childernStatusReq=str_replace('M','G',json_decode($userAllData->children_status_req));
			   $fertileStatusReq=str_replace('G','M',json_decode($userAllData->fertile_status_req));
			   $lookStatusReq=str_replace('G','M',json_decode($userAllData->look_status_req));
			   $childernStatus=json_decode(str_replace('G','M',$myRequestData->children_status));
			   
			   
			}
			else
			{
                $userReligionReq=str_replace('M','G',$userAllData->user_religion);
				$healthStatusReq=str_replace('M','G',json_decode($userAllData->health_status_req));
				$userMarriageTypeReq=str_replace('M','G',json_decode($userAllData->marriage_type_prefer_req));
			    $childernStatusReq=str_replace('G','M',json_decode($userAllData->children_status_req));
				$fertileStatusReq=str_replace('M','G',json_decode($userAllData->fertile_status_req));
				$lookStatusReq=str_replace('G','M',json_decode($userAllData->look_status_req));
				$childernStatus=json_decode(str_replace('M','G',$myRequestData->children_status));
			
			}
	
			
			 $userReligion=json_decode($myRequestData->user_religion);//MR1 array
			 $usersReligionReq=json_decode($userAllData->user_religion_req);//GR1 array
			$point=(in_array($userReligionReq,$userReligion) && in_array($myProfileData->user_religion,$usersReligionReq))? $point+1 : $point;
		
			// $var[]=array('points'=>$point,'userid'=>$userAllData->user_id,'userReligion'=>$userAllData->user_religion,'userReligionRequested'=>$usersReligionReq,'requestedByGirl'=>$myRequestData->user_religion,'rgirlReligion'=>$myProfileData->user_religion);
			
			$healthStatus=json_decode($myRequestData->health_status);
		    // $point=in_array($userAllData->health_status,$healthStatus)? $point+1 : $point;
            // $point=in_array($myProfileData->health_status,$healthStatusReq)? $point+1 : $point;                            
		    $result=array_intersect($healthStatus,$healthStatusReq);
			$point=(count($result)>0)? $point+1: $point;
				
			
			$userMarriageType=json_decode($myRequestData->marriage_type_prefer);
			// $point=in_array($userAllData->married_status,$userMarriageType)? $point+1 : $point;
            // $point=in_array($myProfileData->married_status,$userMarriageTypeReq)? $point+1 : $point;
			$result=array_intersect($userMarriageType,$userMarriageTypeReq);
			$point=(count($result)>0)? $point+1 : $point;
			
			// dd($point);
			// dd($point);
						
			if($profileOf=='Male')
			{				
		       // dd($userAllData->arabic_status);
		       	$smok=($userAllData->arabia_status=='Yes' || $userAllData->arabia_status=='نعم')? 1 : 0 ;
				$smo=($myRequestData->fertile_status=='Yes' || $myRequestData->fertile_status=='نعم')? 1 :($myRequestData->fertile_status=='لا يهمني'? 2 : 0);
				$smor=($userAllData->fertile_status_req=='Yes' || $userAllData->fertile_status_req=='نعم')? 1 :($userAllData->fertile_status_req=='لا يهمني'? 2 : 0);
				$smokr=($myProfileData->smoke_status=='Yes' || $myProfileData->smoke_status=='نعم')? 1 : 0 ;
				$point=(($smo==$smok || $smo==2) && ($smor==$smokr || $smor==2)) ? $point+1 : $point; 
				
			}
			else
			{		
				$smok=($userAllData->smoke_status=='Yes' || $userAllData->smoke_status=='نعم')? 1 : 0 ;
				$smo=($myRequestData->fertile_status=='Yes' || $myRequestData->fertile_status=='نعم')? 1 :($myRequestData->fertile_status=='لا يهمني'? 2 : 0);
				$smor=($userAllData->fertile_status_req=='Yes' || $userAllData->fertile_status_req=='نعم')? 1 : ($userAllData->fertile_status_req=='لا يهمني'? 2 : 0);
				$smokr=($myProfileData->smoke_status=='Yes' || $myProfileData->smoke_status=='نعم')? 1 : 0 ;
				$point=(($smo==$smok || $smo==2) && ($smor==$smokr || $smor==2))? $point+1 : $point; 
			}
			// dd($smor);
			// dd($smokr);
		
			// $var[]=$userAllData->arabia_status;
		    $point=(in_array($userAllData->children_status,$childernStatus) && in_array($myProfileData->children_status,$childernStatusReq)) ? $point+1 : $point;
	
 // dd($myProfileData);
              // $var[]=array('userAllData'=>$userAllData->fertile_status_req,'myRequestData'=>$myRequestData->fertile_status);
			// $point=$userAllData->fertile_status_req==$myRequestData->fertile_status && $myProfileData->arabia_status==$myRequestData->fertile_status ? $point+1 : $point;
			

			 
	
			 
          
		
			$homeAfterMarriage=json_decode($myRequestData->home_after_marriage);
			$homeAfterMarriageReq=json_decode($userAllData->home_after_marriage_req);
			$result=array_intersect($homeAfterMarriage,$homeAfterMarriageReq);
			$point=(count($result)>0)? $point+1 : $point;
			// $var[]=$point; 
		
			$lookStatus=json_decode($myRequestData->look_status);
            // $result=array_intersect($lookStatus,$lookStatusReq);
			// $point=(count($result)>0)? $point+1 : $point;
			// $point=in_array($myProfileData->look_status,$lookStatusReq)? $point+1 : $point;
			$point=in_array($userAllData->look_status,$lookStatus)? $point+1 : $point;
				
			$bodyStatus=json_decode($myRequestData->body_status);
			$point=in_array($userAllData->body_status,$bodyStatus)? $point+1 : $point;
		 // $var[]=array('id'=>$userAllData->user_id,'points'=>$point,'name'=>$userAllData->username);		
			// $bodyStatusReq=json_decode($userAllData->body_status_req);
			// $point=in_array($myProfileData->body_status,$bodyStatusReq)? $point+1 : $point; 
			
			
			
				
			if($point==9)
			{
				$userEducation=json_decode($myRequestData->user_education);
			    $point=in_array($userAllData->user_education,$userEducation)? $point+50 : $point;
                // $userEducationReq=json_decode($userAllData->user_education_req);
			    // $point=in_array($myProfileData->user_education,$userEducationReq)? $point+1 : $point;
				if($myProfileData=='Male'):
				
				$veilStatus=json_decode($myRequestData->veil_status);
				$point=in_array($userAllData->veil_status,$veilStatus)? $point+50 : $point;		
				
				$userBodyColor=json_decode($myRequestData->user_body_color);
			    // $userBodyColorReq=json_decode($userAllData->user_body_color_req);
			    $point=in_array($userAllData->body_color,$userBodyColor)? $point+50 : $point;
			    // $point=in_array($myProfileData->body_color,$userBodyColorReq)? $point+1 : $point;
				
				$point=($myRequestData->user_weight_min >= $userAllData->user_weight &&  $myRequestData->user_weight_max>=$userAllData->user_weight)? $point+50 : $point;
				
				$userNationality=json_decode($myRequestData->user_nationality);
			    $point=in_array($userAllData->user_nationality,$userNationality)? $point+50 : $point;
				
				$userHairLook=json_decode($myRequestData->user_hair_look);
				$point=in_array($userAllData->user_hair_look,$userHairLook)? $point+1 : $point;
				
				$wifeJob=json_decode($myRequestData->wife_job);
				$point=in_array($userAllData->wife_job,$wifeJob)? $point+40 : $point;
				
				$point=($userAllData->user_age_min_req >= $myProfileData->user_age &&  $userAllData->user_age_max_req>=$myProfileData->user_age)? $point+40 : $point;
				
				$userHobbies=json_decode($myRequestData->user_hobbies);
				$point=in_array($userAllData->user_hobbies_req,$userHobbies)? $point+40 : $point;
				
				
				$userReligious=json_decode($myRequestData->user_religious);
			    $point=in_array($userAllData->user_religious,$userReligious)? $point+30 : $point;
				
				
				$userCity=json_decode($myRequestData->user_city);
			    $point=in_array($userAllData->user_city,$userCity)? $point+30 : $point;
			// $userHobbies=json_decode($myProfileData->user_hobbies);
			// $result1=array_intersect($userHobbiesReq,$userHobbies);
			// $point=(count($result1)>0)? $point+1 : $point;
			
			   $point=($myRequestData->user_height_min >= $userAllData->user_height &&  $myRequestData->user_height_max>=$userAllData->user_height)? $point+20 : $point;
			   
			   $qubelahStatus=json_decode($myRequestData->qubelah_status);
			   $point=in_array($userAllData->qubelah_status,$qubelahStatus)? $point+10 : $point;
            // $qubelahStatusReq=json_decode($userAllData->qubelah_status_req);
			// $point=in_array($myProfileData->qubelah_status,$qubelahStatusReq)? $point+1 : $point;
            // $result=array_intersect($qubelahStatus,$qubelahStatusReq);
			// $point=(count($result)>0)? $point+1 : $point;
			
			$qabelahName=json_decode($myRequestData->qubelah_name);
			$point=in_array($userAllData->qubelah_name,$qabelahName)? $point+10 :$point;
			
			$userIncome=json_decode($myRequestData->user_income);
			$point=in_array($userAllData->user_income,$userIncome)? $point+10 : $point;
			   
			$hairLook=json_decode($myRequestData->user_hair_look);
            $point=in_array($userAllData->user_hair_look,$hairLook)? $point+5 : $point;

            $hairColor=json_decode($myRequestData->user_hair_color);
            $point=in_array($userAllData->user_hair_color_req,$hairColor)? $point+5 :$point;
			
			$userTraditional=json_decode($myRequestData->traditional_status);
			$point=in_array($userAllData->user_traditional,$userTraditional)? $point+5 : $point;
            // $var[]=$point;
  			
				
			else:
			
			   
			
			    $userCity=json_decode($myRequestData->user_city);
			    $point=in_array($userAllData->user_city,$userCity)? $point+50 : $point;
 
			
			    $userNationality=json_decode($myRequestData->user_nationality);
			    $point=in_array($userAllData->user_nationality,$userNationality)? $point+50 : $point;
				
				$userProfessions=json_decode($myRequestData->user_profession);
				$point=in_array($userAllData->user_profession,$userProfessions)? $point+30 : $point;
				
				
			
				$veilStatus=json_decode($myRequestData->veil_status);
				$point=in_array($userAllData->beard_status,$veilStatus)? $point+20 : $point;			
				
				$lookStatus=json_decode($myRequestData->look_status);
                // $result=array_intersect($lookStatus,$lookStatusReq);
			    // $point=(count($result)>0)? $point+1 : $point;
			    // $point=in_array($myProfileData->look_status,$lookStatusReq)? $point+1 : $point;
			    $point=in_array($userAllData->look_status,$lookStatus)? $point+40 : $point;
			
			    $bodyStatus=json_decode($myRequestData->body_status);
			    $point=in_array($userAllData->body_status,$bodyStatus)? $point+40 : $point;
				
				$userEducation=json_decode($myRequestData->user_education);
			    $point=in_array($userAllData->user_education,$userEducation)? $point+40 : $point;
				
				$userBodyColor=json_decode($myRequestData->user_body_color);
			    // $userBodyColorReq=json_decode($userAllData->user_body_color_req);
			    $point=in_array($userAllData->body_color,$userBodyColor)? $point+40 : $point;
				
				$userIncome=json_decode($myRequestData->user_income);
			    $point=in_array($userAllData->user_income,$userIncome)? $point+40 : $point;
				
				$point=($myRequestData->user_height_min >= $userAllData->user_height &&  $myRequestData->user_height_max>=$userAllData->user_height)? $point+30 : $point;
				
				$point=($userAllData->user_age_min_req >= $myProfileData->user_age &&  $userAllData->user_age_max_req>=$myProfileData->user_age)? $point+30 : $point;
				
				$userReligious=json_decode($myRequestData->user_religious);
			    $point=in_array($userAllData->user_religious,$userReligious)? $point+20 : $point;
				
				$userHobbies=json_decode($myRequestData->user_hobbies);
				$point=in_array($userAllData->user_hobbies_req,$userHobbies)? $point+20 : $point;
				
				$point=($myProfileData->smoke_status==$userAllData->smoke_status)? $point+50 : $point;
				
				// $beardStatus=json_decode($myRequestData->beard_status);
				// $point=in_array($userAllData->beard_status,$beardStatus)? $point+20 : $point;
				
				$qubelahStatus=json_decode($myRequestData->qubelah_status);
			    $point=in_array($userAllData->qubelah_status,$qubelahStatus)? $point+10 : $point;
				
			    $qabelahName=json_decode($myRequestData->qubelah_name);
			    $point=in_array($userAllData->qubelah_name,$qabelahName) && in_array($qubelahStatus,['Yes']) ? $point+10 :$point;
			
			    $userIncome=json_decode($myRequestData->user_income);
			    $point=in_array($userAllData->user_income,$userIncome)? $point+10 : $point;
               
			    $hairColor=json_decode($myRequestData->user_hair_color);
                $point=in_array($userAllData->user_hair_color_req,$hairColor)? $point+5 :$point;
			
			    $userTraditional=json_decode($myRequestData->traditional_status);
			    $point=in_array($userAllData->traditional_status_req,$userTraditional)? $point+5 : $point;
				
			
			
			endif;
					$json[]=array('id'=>strval($userAllData->user_id),'point'=>strval($point));
			}
			
		}
		// dd($var);
			usort($json, array($this,'my_sort'));
			$array=array_pluck($json, 'id');
			return Response::json(array('status'=>'1','idslist'=>array_slice($array,$count,15),'profileOf'=>$profileOf),200);
}
			return Response::json(array('status'=>'0'),200);
}


public function filterMainData(Request $request){
$json=array();	
$userId=$request->input('userId');	
$userData=DB::table('users')->where('id',$userId)->first();
if(count($userData)==0){
	return Response::json(array('status'=>'0'));
	die();
}
if($userData->vip=='0'){

if($userData->gender=='Male'){
	$profileOf='Female';
	$allData=DB::table('users')->where([['users.gender','=','Female'],['users.user_registration','=','1'],['users.preacher','=','0'],['banAdmin','=','0'],['deactivate','=','0'],['vip','=','0']])->join('userDetails','users.id','=','userDetails.user_id')->join('userDetailsFull','users.id','=','userDetailsFull.user_id')->get();
}
else{
	$profileOf='Male';
	$allData=DB::table('users')->where([['users.gender','=','Male'],['users.user_registration','=','1'],['users.preacher','=','0'],['banAdmin','=','0'],['deactivate','=','0'],['vip','=','0']])->join('userDetails','users.id','=','userDetails.user_id')->join('userDetailsFull','users.id','=','userDetailsFull.user_id')->get();	
	
}
}
else{
	
if($userData->gender=='Male'){
	$profileOf='Female';
	$allData=DB::table('users')->join('userDetails','users.id','=','userDetails.user_id')->join('userDetailsFull','users.id','=','userDetailsFull.user_id')->join('man_request','man_request.user_id','=','user.id')->where([['users.gender','=','Female'],['users.user_registration','=','1'],['users.preacher','=','0'],['banAdmin','=','0'],['deactivate','=','0'],['vip','=','1']])->where(DB::raw('CASE when man_request.man_request_status=1 THEN 1 ELSE 0 END'),1)->get();
	
}
else{
	$profileOf='Male';
	$allData=DB::table('users')->join('userDetails','users.id','=','userDetails.user_id')->join('userDetailsFull','users.id','=','userDetailsFull.user_id')->join('man_request','man_request.user_id','=','user.id')->where([['users.gender','=','Male'],['users.user_registration','=','1'],['users.preacher','=','0'],['banAdmin','=','0'],['deactivate','=','0'],['vip','=','1']])->where(DB::raw('CASE when man_request.man_request_status=1 THEN 1 ELSE 0 END'),1)->get();	

}	
	
}

$manRequest=DB::table('man_request')->where('user_id',$userId)->first();
if(count($manRequest)>0){
	if($manRequest->man_request_status==1){
		
	$mainUserData=DB::table('man_request')->where('man_request.user_id',$userId)->join('man_request_full','man_request.user_id','=','man_request_full.user_id')->first();
		
	}
	else{
		
	return Response::json(array('status'=>'0'));	
	die();	
	}
}


foreach($allData as $data){
$apply=0;
$point=0;
$mainPoint=0;

$mrg=DB::table('man_request')->where('user_id',$data->user_id)->first();
if(count($mrg)>0){
	
if($mainUserData->marriage_type==$mrg->marriage_type && $mrg->man_request_status==1){
	$apply=1;
	$point++;
	$mainPoint++;		
}	
	
}

$userMarriageType=json_decode($mainUserData->marriage_type_prefer);
foreach($userMarriageType as $marriageType){
if($marriageType==$data->married_status){
	$apply=1;
	$point++;
	$mainPoint++;
}		
}
$qubelah_status=json_decode($mainUserData->qubelah_status);
foreach($qubelah_status as $qubelahStatus){
if($qubelahStatus==$data->qubelah_status){
	$apply=1;
	$point++;
	$mainPoint++;
}
}

// if($mainUserData->qubelah_status==$data->qubelah_status){
	// $apply=1;
	// $point++;
// }
if($mainUserData->user_age_min>=$data->user_age && $mainUserData->user_age_max<=$data->user_age){
	$apply=1;
	$point++;
}
$userEducation=json_decode($mainUserData->user_education);
foreach($userEducation as $edu){
if($edu==$data->user_education){
	$apply=1;
	$point++;
}		
}

if($mainUserData->user_height_min>=$data->user_height && $mainUserData->user_height_max<=$data->user_height ){
	$apply=1;
	$point++;
}

if($mainUserData->user_weight_min>=$data->user_weight && $mainUserData->user_weight_max<=$data->user_weight ){
	$apply=1;
	$point++;
}

$userNationality=json_decode($mainUserData->user_nationality);
foreach($userNationality as $nation){
if($nation==$data->user_nationality){
	$apply=1;
	$mainPoint++;
	$point++;
}
}

$userCity=json_decode($mainUserData->user_city);
foreach($userCity as $city){
if($city==$data->user_city){
	$apply=1;
	$point++;
}
}


$userReligion=json_decode($mainUserData->user_religion);
foreach($userReligion as $religion){
if($religion==$data->user_religion){
	$apply=1;
	$mainPoint++;
	$point++;
}
}

if($userData->gender=='Male'){
$userHairLook=json_decode($mainUserData->user_hair_look);
foreach($userHairLook as $hairLook){
if($hairLook==$data->hair_look){
	$apply=1;
	$point++;	
}	
}

$wifeJob=json_decode($mainUserData->wife_job);
foreach($wifeJob as $wife){
if($wife==$data->user_profession){
	$apply=1;
	$point++;	
}	
}

$veilStatus=json_decode($mainUserData->veil_status);
foreach($veilStatus as $veil){
if($veil==$data->veil_status){
	$apply=1;
	$point++;	
}	
}

}
else{

$veilStatus=json_decode($mainUserData->veil_status);
foreach($veilStatus as $veil){
if($veil==$data->beard_status){
	$apply=1;
	$point++;	
}	
}

	
}

$userBodyColor=json_decode($mainUserData->user_body_color);
foreach($userBodyColor as $bodyColor){
if($bodyColor==$data->body_color){
	$apply=1;
	$point++;	
}	
}

$userProfession=json_decode($mainUserData->user_profession);
foreach($userProfession as $profession){
if($userProfession==$data->user_profession){
	$apply=1;
	$point++;	
}	
}

$userHobbies=json_decode($mainUserData->user_hobbies);
foreach($userHobbies as $hobbies){	
// $hobby=json_decode($data->user_hobbies);
// foreach($hobby as $hob){
// if($hob==$hobbies){
	// $apply=1;
	// $point++;
	// break;	
// }	
// }
}

$userIncome=json_decode($mainUserData->user_income);
foreach($userIncome as $income){
if($income==$data->user_income){
	$apply=1;
	$point++;	
}	
}

// $homeAfterMarriage=json_decode($mainUserData->home_after_marriage);
// foreach($homeAfterMarriage as $home){
// if($income==$data->user_income){
	// $apply=1;
	// $point++;	
// }	
// }

$userReligious=json_decode($mainUserData->user_religious);
foreach($userReligious as $religious){
if($religious==$data->user_religious){
	$apply=1;
	$point++;	
}	
}

$userTraditional=json_decode($mainUserData->traditional_status);
foreach($userTraditional as $traditional){
if($traditional==$data->user_traditional){
	$apply=1;
	$point++;	
}	
}

$lookStatus=json_decode($mainUserData->look_status);
foreach($lookStatus as $look){
if($look==$data->look_status){
	$apply=1;
	$point++;	
}	
}

$bodyStatus=json_decode($mainUserData->body_status);
foreach($bodyStatus as $body){
if($body==$data->body_status){
	$apply=1;
	$point++;	
}	
}

if($mainUserData->smoke_status==$data->smoke_status){
	$apply=1;
	$point++;	
}

$healthStatus=json_decode($mainUserData->health_status);
foreach($healthStatus as $health){
if($health==$data->health_status){
	$apply=1;
	$point++;	
}	
}

$childernStatus=json_decode($mainUserData->children_status);
foreach($childernStatus as $children){
if($childernStatus==$data->children_status){
	$apply=1;
	$point++;	
}	
}

// echo $mainPoint;
if($mainPoint>1){
$json[]=array('id'=>$data->user_id,'point'=>$point);
}	

}
usort($json, array($this,'my_sort'));

$array=array();

foreach($json as $js){
	
	$array[]=strval($js['id']);	
	
}

return Response::json(array('status'=>'1','idslist'=>$array,'profileOf'=>$profileOf));

}


	public function my_sort($a, $b)
{
    if ($a['point'] > $b['point']) {
        return -1;
    } else if ($a['point'] < $b['point']) {
        return 1;
    } else {
        return 0; 
    }
}

public function uploadImage(Request $request){
	
	$image=$request->input('image');
	$check=uniqid().'.png';
	if(Storage::put('userImage/'.$check, $image))
    {
		return Response::json(array('status'=>'1','imageName'=>$check));
		
	}
	else{
		
		return Response::json(array('status'=>'0','imageName'=>''));
		
	}
	
}



public function phoneCode(){
	
	$data=DB::table('phone_code')->get();
	$json=array();
	foreach($data as $code){
		$json[]=$code->code;		
	}
	return Response::json(array('status'=>'1','codeList'=>$json));
}


public function userRequest(Request $request){
	
	$userId=$this->validError($this->sanitizeInput($request->input('userId')),'userId');
	$requestId=$this->validError($this->sanitizeInput($request->input('requestId')),'requestId');
	$requestType=$this->validError($this->sanitizeInput($request->input('requestType')),'requestType');
	
	$requestIdData=DB::table('users')->where('id',$requestId)->first();
	$userIdData=DB::table('users')->where('id',$userId)->first();
	 $message1=array('type'=>$requestType,'username'=>$userIdData->username,'sent'=>'1','action'=>'0');
// if($requestIdData->loginPlatform=='Android'){	

if($requestType!='fav')
{
	if($requestIdData->loginPlatform=='Android'){
		// dd('data');
		$message1=array('type'=>$requestType,'username'=>$userIdData->username,'sent'=>'1','action'=>'0');
		Self::sendNotification($requestIdData->gcmToken,$message1);
	}
	else{
		if($requestType=='poke')
		{
		    $message2=array('title'=>$userIdData->username,'body'=>'ارسل اعجاب','messageData'=>$message1);
		}
		else
		{
			$message2=array('title'=>$userIdData->username,'body'=>' قام بارسال طلب زواج','messageData'=>$message1);
		}
		// dd($requestIdData->gcmToken);
		Self::send_notification_ios($requestIdData->gcmToken,$message2);
	}
}


// }	

// if($requestType=='marriedRequest'){
	// DB::table('married_requests')->insert(['user_id'=>$userId,'request_id'=>$requestId,'type'=>$requestType,'timestamp'=>time()]);
// }
// else
// {
	DB::table('requests')->insert(['user_id'=>$userId,'request_id'=>$requestId,'type'=>$requestType,'timestamp'=>time()]);
// }
	return Response::json(array('status'=>'1'));
}
 

public function sendNotification($token,$message){
	
$url='https://fcm.googleapis.com/fcm/send';
$fields=array('registration_ids'=>array($token),'data'=>array("message"=>$message));
$fields=json_encode($fields);
$headers=array('Authorization: key='."AIzaSyDd5MLRfVWxJCUIDjQK6-Alkhtyo9eDAcM",'Content-Type:application/json');
$ch=curl_init();
curl_setopt($ch,CURLOPT_URL,$url);
curl_setopt($ch,CURLOPT_POST,true);
curl_setopt($ch,CURLOPT_HTTPHEADER,$headers); 
curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
curl_setopt($ch,CURLOPT_POSTFIELDS,$fields);
curl_exec($ch);
curl_close($ch);
}

public function getRequestByType(Request $request){
	$json=array();
	$userId=$this->validError($this->sanitizeInput($request->input('userId')),'userId');	
	$requestType=$this->sanitizeInput($request->input('requestType'));	
	if($requestType=='mrg'){
	$data=Req::where([['request_id','=',$userId],['type','=','marriageRequest'],['status','!=','2'],['deactivateRequest','=','0']])->orderBy('id','Desc')->get();		
	}
	else
	{
		$data=DB::table('requests')->where('type','poke')->where('request_id',$userId)->where('status','0')->where('deactivateRequest','0')->get();
	}
	


	foreach($data as $allData){
		
		if(count($allData)!=0)
		{
			
			$check=DB::table('users')->where('id',$allData->user_id)->first();
		
	
			if(count($check))
			{
				$data=DB::table('requests')->where([['request_id',$check->id],['user_id',$userId],['status','1'],['deactivateRequest','0']])->orWhere([['request_id',$userId],['user_id',$check->id],['status','1'],['deactivateRequest','0']])->count();

				$pstatus=$data? 1 : 0;
					
				$userData=$this->getUserById($allData->user_id);
					
				$flag=DB::table('codes')->where('answer',$userData['userNationality'])->first();
				
				$flagName=DB::table('country_flag')->where('man',$flag->code)->orWhere('girl',$flag->code)->first();
			
				
				
				$json[]=array('requestId'=>$allData->id,
				'requestUserId'=>$allData->user_id,
				'type'=>$allData->type, 
				'status'=>$allData->status,
				'requestUserName'=>$userData['userName'],
				'flagName'=>$flagName->country_flag,
				'userNationality'=>$userData['userNationality'],
				'userCity'=>$userData['userCity'],
				'userAge'=>$userData['userAge'],
				'userHeight'=>$userData['userHeight'],
				'userWeight'=>$userData['userWeight'],
				'userImage'=>$userData['userImage'],
				'timestamp'=>$allData->timestamp,
				'pstatus'=>$pstatus);	 
			}
			
		}
      		
	} 
	return Response::json(array('status'=>'1','requestList'=>$json));
	
}


public function requestAction(Request $request){
	$all=$request->all();
	Req::where('id',$all['requestId'])->update(['status'=>$all['action']]);
	
	$data=Req::Find($all['requestId']);
	// dd($data);
	if($data->type=='marriageRequest' || $data->type=='poke')
	{	
		if($data->type=='marriageRequest' && $all['action']=='1')
		{
			
			DB::table('users')->where('id',$data->user_id)->update(['deactivate'=>'1']);
			DB::table('users')->where('id',$data->request_id)->update(['deactivate'=>'1']);
			DB::table('requests')->where([['user_id','=',$data->user_id],['id','!=',$all['requestId']],['deactivateRequest','=','0'],['request_id','=',$data->request_id]])->update(['status'=>'1','timestamp'=>time()]);
			// DB::table('requests')->where([['request_id','=',$data->request_id],['id','!=',$all['requestId']]])->update(['status'=>'1','timestamp'=>time()]);	
			
			$dat=DB::table('requests')->where([['id','!=',$all['requestId']],['type','marriageRequest'],['user_id',$data->user_id],['deactivateRequest','=','0']])->orWhere([['request_id',$data->request_id],['id','!=',$all['requestId']],['type','marriageRequest']])->get();
			// print_r($dat);
			foreach ($dat as $d)
			{
				DB::table('requests')->where('id',$d->id)->update(['deactivateRequest'=>'1']);
			}
		}	
		else if($data->type=='poke' && $all['action']==1) 
		{
			$request=DB::table('requests')->where('id',$data->id)->where('user_id',$data->user_id)->where('deactivateRequest','0')->first();
			
			if($request->status=='1')
			 {
				$query=DB::table('requests')->where('user_id',$request->user_id)->where('request_id',$data->request_id)->where('type','poke')->where('deactivateRequest','0')->update(['status'=>$all['action'],'timestamp'=>time()]);
			 }
		 }
		 else if($data->type=='poke' && $all['action']==2)
		 {
			  $request=DB::table('requests')->where('id',$data->id)->where('user_id',$data->user_id)->where('deactivateRequest','0')->first();
			 
			  if($request->status=='2')
			  {
				   $query=DB::table('requests')->where('user_id',$request->user_id)->where('request_id',$data->request_id)->where('deactivateRequest','0')->where('type','poke')->update(['status'=>$all['action'],'timestamp'=>time()]);
			  }
			 
		 }
		 
		 else if($data->type=='marriageRequest' && $all['action']==2)
		 {
			 $request=DB::table('requests')->where('id',$all['requestId'])->where('user_id',$data->user_id)->where('status','2')->first();
		 }
		
		$userIdData=User::Find($data->user_id);	
		$requestIdData=$this->getUserById($data->request_id);
		 //dd($requestIdData);
		$message=array('type'=>$data->type,'action'=>$all['action'],'userId'=>$requestIdData['userId'],'username'=>$requestIdData['userName'],'userHeight'=>$requestIdData['userHeight'],'userWeight'=>$requestIdData['userWeight'],'flagName'=>$requestIdData['flagName'],'userCity'=>$requestIdData['userCity'],'userAge'=>$requestIdData['userWeight'],'sent'=>'0','action'=>$all['action'],'req_id'=>$data->request_id);
		// $this->sendNotification($userIdData->gcmToken,$message);
		if($userIdData->loginPlatform=='Android'){
			Self::sendNotification($userIdData->gcmToken,$message);
		}
		else
		{
			if($data->type=='poke' && $all['action']==1)
			{
				if($requestIdData=='Female')
				{
                        $message1=array('title'=>'رائع! '.$requestIdData['userName'].'','body'=>' تم قبول طلب ألاعجاب','messageData'=>$message);
				}
                else
                {
                       $message1=array('title'=>'رائع! '.$requestIdData['userName'].'','body'=>' قبل طلب الاعجاب هل تريد ارسال طلب زواج له ؟ ','messageData'=>$message);
                }
			}
			else if($data->type=='poke' && $all['action']==2)
			{
                   $message1=array('title'=>'اسف, '.$requestIdData['userName'].'','body'=>'لم يقبل طلب الاعجاب','messageData'=>$message);
			}
			else if($data->type=='marriageRequest' && $all['action']==1)
			{
                   $message1=array('title'=>'رائع!'.$requestIdData['userName'].'','body'=>'  تم قبول طلب الزواج, يرجاء العوده للتطبيق الان','messageData'=>$message);
			}
			else if($data->type=='marriageRequest' && $all['action']==2)
			{
                      $message1=array('title'=>'اسف,' .$requestIdData['userName'].'','body'=>'رفضت طلب الزواج','messageData'=>$message);
			}
			Self::send_notification_ios($userIdData->gcmToken,$message1);
		}	
	 }
	return Response::json(array('status'=>'1'));	
}


	public function my_sort1($a, $b)
{
    if ($a['count'] > $b['count']) {
        return -1;
    } else if ($a['count'] < $b['count']) {
        return 1;
    } else {
        return 0; 
    }
}





public function getPreacher(Request $request){
	$all=$request->all();
	// dd($all);
	$preacher=DB::table('users')->where([['preacher','=','1'],['preacherCode','=',$all['preacherCode']]])->first();
	if(count($preacher)>0){
		$json=array('userName'=>$preacher->username,'preacherId'=>$preacher->id);
		return Response::json(array('status'=>'1','data'=>$json));
	}
	else{
		
		return Response::json(array('status'=>'0'));
	}	
}


public function marrigeRequests(Request $request){
	$all=$request->all();
	$json=array();
	$data=DB::table('preacher_requests')->where([['preacher_id','=',$all['preacherId']],['status','=','0']])->get();
	// print_r($data);
	// dd($data);
	foreach($data as $allData){
		$requestId=$allData->request_id;
		$id=$allData->id;
		$timestamp=$allData->time;
		$status=$allData->status;
		$requestData=DB::table('requests')->where('id',$requestId)->where('deactivateRequest','0')->first();
		if($requestData!=null)
		{
		       $json[]=array(
		      'personBoy'=>$requestData->user_id,
		      'personGirl'=>$requestData->request_id,
		      'requestId'=>$requestId,
		      'id'=>$id,
		      'timestamp'=>$timestamp
		       );
		}
		
	}
	return Response::json(array('status'=>'1','allData'=>$json));
	
}

public function userfavList(Request $request){
	$all=$request->all();
	$json=array();
	$data=Req::where([['user_id','=',$all['userId']],['type','=','fav']])->orderBy('id','Desc')->get();
	

	
	foreach($data as $allData){
	
		$userData=$this->getUserById($allData->request_id);
	
		$flag=DB::table('codes')->where('answer',$userData['userNationality'])->first();
		
		$flagName=DB::table('country_flag')->where('man',$flag->code)->orWhere('girl',$flag->code)->first();
		if(DB::table('requests')->where('type','poke')->where('user_id',$all['userId'])->where('request_id',$allData->request_id)->where('status','0')->where('deactivateRequest','0')->first())
	    {
		    $pstatus='0';
	    }
	    else if(DB::table('requests')->where('type','poke')->where('user_id',$all['userId'])->where('request_id',$allData->request_id)->where('status','1')->where('deactivateRequest','0')->first())
	    {
		   $pstatus='1';
	    }
	    else
	    {
		    $pstatus='';
	    }
		$json[]=array('requestId'=>$allData->id,
		'requestUserId'=>$allData->request_id,
		'type'=>$allData->type,
		'status'=>$allData->status,
		'requestUserName'=>$userData['userName'],
		'flagName'=>$flagName->country_flag,
		'userNationality'=>$userData['userNationality'],
		'userCity'=>$userData['userCity'],
		'userAge'=>$userData['userAge'],
		'userHeight'=>$userData['userHeight'],
		'userWeight'=>$userData['userWeight'],
		'userImage'=>$userData['userImage'],
		'timestamp'=>$allData->timestamp,
		'pstatus'=>$pstatus
		);
		
	} 
	
	
	return Response::json(array('status'=>'1','requestList'=>$json));
	
}


public function preacherProfile(Request $request){
	$all=$request->all();
	$preacherData=DB::table('users')->where([['users.preacher','=','1'],['users.id','=',$all['preacherId']]])->join('preacherDetails','users.id','=','preacherDetails.preacher_id')->first();
	//dd($preacherData);
$array=array(
			'preacherId'=>$preacherData->preacher_id,
			'preacherName'=>$preacherData->username,
			'preacherPhone'=>$preacherData->phone,
			'preacherEmail'=>$preacherData->email,
			'preacherCode'=>$preacherData->preacherCode,
			'preacherReligion'=>$preacherData->religion,
			'preacherCity'=>$preacherData->city,
			// 'preacherService'=>json_decode($preacherData->service),
			'bankName'=>$preacherData->bank_name,
			'acHolderName'=>$preacherData->ac_holder_name,
			'accountNo'=>$preacherData->account_no,
			'marriageType'=>json_decode($preacherData->marriage_type)
);
$json=array();
$service=json_decode($preacherData->service);
foreach($service as $serviceCity){
	$json[]=$this->getCodeById('GC'.$serviceCity);
}
	$array['preacherService']=$json;
	return Response::json($array);
	
}
public function requestCount($userId,$requestId,$type){
	
	$data=DB::table('requests')->where([['user_id','=',$userId],['request_id','=',$requestId],['type','=',$type],['deactivateRequest','0']])->get();
	return count($data);
}
 
public function exchangeRequest(Request $request){
	$all=$request->all();
	$data=DB::table('users')->where([['preacher','=','1'],['preacherCode','=',$all['preacherCode']]])->first();
// dd($data);
	if(count($data)>0){
	$manData=DB::table('preacher_requests')->select('users.username','users.phone_code','users.phone')->join('requests','preacher_requests.request_id','=','requests.id')->join('users','users.id','=','requests.user_id')->where('preacher_requests.request_id',$all['requestId'])->first();	
	$womanData=DB::table('preacher_requests')->select('users.username','users.phone_code','users.phone')->join('requests','preacher_requests.request_id','=','requests.id')->join('users','users.id','=','requests.request_id')->where('preacher_requests.request_id',$all['requestId'])->first();	
	$preacherData=DB::table('preacher_requests')->join('users','users.id','=','preacher_requests.preacher_id')->where('preacher_requests.request_id',$all['requestId'])->first();
	// dd($manData);
		if($manData!=null && $womanData!=null)
		{
		$notification=array('type'=>'notification1','sent'=>'admin','action'=>'4','username'=>'');
		$message='تم ارسال طلب زواج لكِ'."\n".$manData->username."\n".$manData->phone_code.$manData->phone."\n".$womanData->username."\n".$womanData->phone_code.$womanData->phone;
		Self::messages($data->phone_code,$data->phone,$message);
		// if($data->loginPlatform=='Android')
			Self::sendNotification($data->gcmToken,$notification);
		// else{
		// 	Self::send_notification_ios($data->gcmToken,$notification);
		// }
		DB::table('preacher_requests')->where([['request_id','=',$all['requestId']]])->update(['preacher_id'=>$data->id,'time'=>time()]);
		return Response::json(array('status'=>'1'));
		}
		else
		{
			return Response::json(array('status'=>'0'));
		}
	}
	else{
		return Response::json(array('status'=>'0'));
		
	}
}

public function actionByPreacher2(Request $request)
{
	   $all=$request->all();
	   $data=DB::table('preacher_requests')->where('id',$all['id'])->first();
	  
	   $manWomanData=DB::table('requests')->where('id',$data->request_id)->first();
	   $manData=$this->getSmallInfo($manWomanData->user_id);
	$womenData=$this->getSmallInfo($manWomanData->request_id);
	   $preacherData=DB::table('users')->where('id',$data->preacher_id)->first();
	    	DB::table('requests')->where('id',$data->request_id)->update(['approved'=>'2']);
	   if($womenData->sms_number!=''){
		$manMessage='بالتوفيق يارب'."\n".$womenData->username."\n".$womenData->sms_number;	
	}
	else{
		$manMessage='بالتوفيق يارب'."\n".$womenData->username."\n".$womenData->phone;	
	}
	
	
	$womenMessage='بالتوفيق يارب'."\n".$manData->username."\n".$manData->phone;
	
	
	$this->messages($manData->phone_code,$manData->phone,$manMessage);
	$this->messages($womenData->phone_code,$womenData->phone,$womenMessage);
	
		
	$message=array('type'=>'notification1','sent'=>'preacher','action'=>'1','username'=>'');
	if($womenData->loginPlatform=='Android'){
		Self::sendNotification($womenData->gcmToken,$message);
	}
	else{
		$message1=array('title'=>'الخطابه قبلت طلب الزواج','body'=>'','messageData'=>$message);
		Self::send_notification_ios($womenData->gcmToken,$message1);
	}

	if($manData->loginPlatform=='Android'){
		Self::sendNotification($manData->gcmToken,$message);
	}
	else{
		$message1=array('title'=>'الخطابه قبلت طلب الزواج','body'=>'','messageData'=>$message);
		Self::send_notification_ios($manData->gcmToken,$message1);
	}
		return Response::json(array('status'=>'1'));
}


public function actionByPreacher(Request $request)
{		
    $all=$request->all();
    $data=DB::table('preacher_requests')->where('id',$all['id'])->first();	
	$manWomanData=DB::table('requests')->where('id',$data->request_id)->where('deactivateRequest','0')->first();
	$preacherData=DB::table('users')->where('id',$data->preacher_id)->first();
	$manData=$this->getSmallInfo($manWomanData->user_id);
	$womenData=$this->getSmallInfo($manWomanData->request_id);
    // $check=DB::table('userDetailsFull')->where('preature_status','Yes')->where('user_id',$manWomanData->user_id)->orWhere('user_id',$manWomanData->request_id)->get();
	// if(count($check)
	// {
		// dd($all['action']);
	if($all['action']=='1'){

	// $preacherMessage='Congratulations! Man name='.$manData->username.', Man Number='.$manData->phone.',Women name='.$womenData->username.', Woman Number='.$womenData->phone;
	
	// if($womenData->sms_number!=''){
		// $manMessage='بالتوفيق يارب'."\n".$womenData->username."\n".$womenData->sms_number;	
	// }
	// else{
		// $manMessage='بالتوفيق يارب'."\n".$womenData->username."\n".$womenData->phone;	
	// }
	
	
	// $womenMessage='بالتوفيق يارب'."\n".$manData->username."\n".$manData->phone;

	// if($data->preacher_id!=913)
	// {	
		// $this->messages($preacherData->phone_code,$preacherData->phone);
	// }
	// $this->messages($manData->phone_code,$manData->phone,$manMessage);
	// $this->messages($womenData->phone_code,$womenData->phone,$womenMessage);
	DB::table('preacher_requests')->where('id',$all['id'])->update(['status'=>'1','time'=>time()]);
	DB::table('requests')->where('id',$data->request_id)->update(['approved'=>'1']);
	DB::table('users')->where('id',$manWomanData->user_id)->update(['deactivate'=>'2']);	
	DB::table('users')->where('id',$manWomanData->request_id)->update(['deactivate'=>'2']);	
	
	
	// $message=array('type'=>'notification1','sent'=>'preacher','action'=>'1','username'=>'');
	// if($womenData->loginPlatform=='Android'){
		// Self::sendNotification($womenData->gcmToken,$message);
	// }
	// else{
		// Self::send_notification_ios($womenData->gcmToken,$message);
	// }

	// if($womenData->loginPlatform=='Android'){
		// Self::sendNotification($manData->gcmToken,$message);
	// }
	// else{
		// Self::send_notification_ios($manData->gcmToken,$message);
	// }
	
	}
	elseif($all['action']=='2'){
		
	DB::table('preacher_requests')->where('id',$all['id'])->update(['status'=>'2']);	
	DB::table('requests')->where('id',$data->request_id)->where('deactivateRequest','0')->update(['status'=>'3']);	
	$payments=DB::table('payments')->where('request_id',$data->request_id)->where('status','1')->get();
	// if(count($payments)>0){
		// DB::table('users')->where('id',$data->request_id)->update(['pay_chance'=>'1']);
	// }
	// DB::table('users')->where('id',$manWomanData->user_id)->update(['deactivate'=>'0']);	
	// DB::table('users')->where('id',$manWomanData->request_id)->update(['deactivate'=>'0']);
	// if(DB::table('users')->where('id',$manWomanData->user_id)->where('deactivate','0')->first())
	// {
		// $m=DB::table('requests')->where('user_id',$manWomanData->user_id)->get();
		// if(count($m)!=0)
		// {
			// DB::table('requests')->where('user_id',$namWomanData->user_id)->delete();
		// }
	// }
	// else if(DB::table('requests')->where('request_id',$manWomanData->request_id)->where('deactivate','0')->first())
	// {
		  // DB::table('requests')->where('request_id',$manWomanData->request_id)->delete();
	// }
	
	$check=DB::table('requests')->where('id',$data->request_id)->first();
	$message=array('type'=>'notification1','sent'=>'preacher','action'=>'0','username'=>'');

	if($womenData->loginPlatform=='Android'){
		Self::sendNotification($womenData->gcmToken,$message);
	}
	else{
		$message1=array('title'=>'"قامت الخطابه بإلغاء طلب الزواج بموافقه احد الطرفين','body'=>'','messageData'=>$message);
		Self::send_notification_ios($womenData->gcmToken,$message1);
	}

	if($manData->loginPlatform=='Android'){
		Self::sendNotification($manData->gcmToken,$message);
	}
	else{
		$message1=array('title'=>'"قامت الخطابه بإلغاء طلب الزواج بموافقه احد الطرفين','body'=>'','messageData'=>$message);
		Self::send_notification_ios($manData->gcmToken,$message1);
	}

	if(count($check))
	{
		
		$change1=DB::table('users')->where('id',$check->user_id)->orWhere('id',$check->request_id)->update(['deactivate'=>'0']);
		DB::table('requests')->where('user_id',$check->user_id)->where('type','marriageRequest')->where('request_id',$check->request_id)->where('deactivateRequest','0')->update(['deactivateRequest'=>'1']);
	}
	// $message=array('type'=>'notification1','sent'=>'preacher','action'=>'0','username'=>'');
	// Self::sendNotification($womenData->gcmToken,$message);
	// Self::sendNotification($manData->gcmToken,$message);	
	
	}
	elseif($all['action']=='3'){
		
	DB::table('preacher_requests')->where('id',$all['id'])->update(['status'=>'3','time'=>time()]);	
		
	}
	

	
	return Response::json(array('status'=>'1'));
	
}


public function getSmallInfo($id){
	
	return $data=DB::table('users')->join('userDetailsFull','userDetailsFull.user_id','=','users.id')->where('users.id',$id)->first();
	
}

public function getSmallInfo1($id){
	
	return $data=DB::table('users')->where('id',$id)->first();
	
}

public function messages($phoneCode,$phoneNumber,$message){
	
	
	$number=$phoneCode.$phoneNumber;
	$client = new Services_Twilio('AC2d33b9dfdf71f2987d4541a0dbb4a2c0', '9812ac0f92962b4665adf79d2477e3f4');

  try {
    // Use the Twilio REST API client to send a text message
    $m = $client->account->messages->sendMessage(
      '+1 765-626-3110', // the text will be sent from your Twilio number
      $number, // the phone number the text will be sent to
      $message // the body of the text message
    );
  } catch(Services_Twilio_RestException $e) {
    // Return and render the exception object, or handle the error as needed
	//  return $e;
  };
	//dd($m);
// return $m;
}


public function preacherNotificationList(Request $request){
	$array=array();
	$all=$request->all();
	$data=DB::table('preacher_requests')->where([['preacher_id','=',$all['preacherId']],['status','!=','0']])->get();
	foreach($data as $allData){
		
		$getData=DB::table('requests')->where('id',$allData->request_id)->where('deactivateRequest','0')->first();
		
		$array[]=array(
		'timestamp'=>$allData->time,
		'status'=>$allData->status,
		'personBoy'=>$getData->user_id,
		'personGirl'=>$getData->request_id,
		'requestId'=>$allData->request_id
		
		);
		
		
	}
	
	return Response::json(array('status'=>'1','notificationList'=>$array));
	
}

public function searchService(Request $request){
	
$userId=$this->validError($request->input('userId'),'userId');

$marriageType=$this->validError($request->input('marriageType'),'marriageType');
	
$marriageTypePrefer=$this->validError($request->input('marriageTypePrefer'),'marriageTypePrefer');

$qabelahStatus=$this->validError($request->input('qabelahStatus'),'qabelahStatus');

// if($qabelahStatus=='MQQ' || $qabelahStatus=='GQQ'){
$qabelahName=$request->input('qabelahName');		
// }
// else{
// $qabelahName='';	
// }

$userAgeMin=$this->validError($request->input('userAgeMin'),'userAgeMin');

$userAgeMax=$this->validError($request->input('userAgeMax'),'userAgeMax');

$userEducation=$this->validError($request->input('userEducation'),'userEducation');

$userHeightMin=$this->validError($request->input('userHeightMin'),'userHeightMin');

$userHeightMax=$this->validError($request->input('userHeightMax'),'userHeightMax');

$userWeightMin=$this->validError($request->input('userWeightMin'),'userWeightMin');

$userWeightMax=$this->validError($request->input('userWeightMax'),'userWeightMax');
	
$userNationality=$this->validError($request->input('userNationality'),'userNationality');

$userCity=$this->validError($request->input('userCity'),'userCity');

$userReligion=$this->validError($request->input('userReligion'),'userReligion');

$userHairColor=$this->validError($request->input('userHairColor'),'userHairColor');

$userHairLook=$this->validError($request->input('userHairLook'),'userHairLook');

$userBodyColor=$this->validError($request->input('userBodyColor'),'userBodyColor');

$userProfession=$this->validError($request->input('userProfession'),'userProfession');
	
$userHobbies=$this->validError($request->input('userHobbies'),'userHobbies');

$userIncomelevel=$this->validError($request->input('userIncomelevel'),'userIncomelevel');

$homeAfterMarriage=$this->validError($request->input('homeAfterMarriage'),'homeAfterMarriage');

$userReligious=$this->validError($request->input('userReligious'),'userReligious');

$traditionalStatus=$this->validError($request->input('traditionalStatus'),'traditionalStatus');

$lookStatus=$this->validError($request->input('lookStatus'),'lookStatus');

$bodyStatus=$this->validError($request->input('bodyStatus'),'bodyStatus');

$smokeStatus=$this->validError($request->input('smokeStatus'),'smokeStatus');

$healthStatus=$this->validError($request->input('healthStatus'),'healthStatus');

$veilStatus=$request->input('veilBeardStatus');

$wifeJob=$request->input('wifeJob');

$childernStatus=$this->validError($request->input('childernStatus'),'childernStatus') ;

$userData=DB::table('users')->where('id',$userId)->first();
if(count($userData)==0){
return Response::json(array('status'=>'0'));
die();
}
	
if($userData->vip=='0'){

if($userData->gender=='Male'){ 
	$profileOf='Female';
	$allData=DB::table('users')->where([['users.gender','=','Female'],['users.user_registration','=','1'],['users.preacher','=','0'],['banAdmin','=','0'],['deactivate','=','0'],['vip','=','0']])->join('userDetails','users.id','=','userDetails.user_id')->join('userDetailsFull','users.id','=','userDetailsFull.user_id')->get();
}
else{
	$profileOf='Male';
	$allData=DB::table('users')->where([['users.gender','=','Male'],['users.user_registration','=','1'],['users.preacher','=','0'],['banAdmin','=','0'],['deactivate','=','0'],['vip','=','0']])->join('userDetails','users.id','=','userDetails.user_id')->join('userDetailsFull','users.id','=','userDetailsFull.user_id')->get();	
}
}
else{
	
if($userData->gender=='Male'){
	$profileOf='Female';
	$allData=DB::table('users')->where([['users.gender','=','Female'],['users.user_registration','=','1'],['users.preacher','=','0'],['banAdmin','=','0'],['deactivate','=','0'],['vip','=','1']])->join('userDetails','users.id','=','userDetails.user_id')->join('userDetailsFull','users.id','=','userDetailsFull.user_id')->get();
}
else{
	$profileOf='Male';
	$allData=DB::table('users')->where([['users.gender','=','Male'],['users.user_registration','=','1'],['users.preacher','=','0'],['banAdmin','=','0'],['deactivate','=','0'],['vip','=','1']])->join('userDetails','users.id','=','userDetails.user_id')->join('userDetailsFull','users.id','=','userDetailsFull.user_id')->get();	
}	
	
}	
	
foreach($allData as $data){
	
$apply=0;
$point=0;
$mainPoint=0;	
	
$mrg=DB::table('man_request')->where('user_id',$data->user_id)->first();
if(count($mrg)>0){
	
if($marriageType==$mrg->marriage_type ){
	$apply=1;
	$point++;
	$mainPoint++;		
}	
	
}

$userMarriageType=json_decode($marriageTypePrefer);
foreach($userMarriageType as $marriageTypee){
if($marriageTypee==$data->married_status){
	$apply=1;
	$point++;
	$mainPoint++;
}		
}

//var_dump($qabelahStatus); 
// $qabelahStatus = array(); 
 $qabelahstatus = json_decode($qabelahStatus,true); 
 // dd($qabelahStatus); 

// $qubelah_status=json_decode($mainUserData->qubelah_status);
foreach($qabelahstatus as $qubelah_status){
if($qubelah_status==$data->qubelah_status){
	$apply=1;
	$point++;
	// $mainPoint++;
}
}
 
if($userAgeMin>=$data->user_age && $userAgeMax<=$data->user_age){
	$apply=1;
	$point++;
}
// dd(json_decode($userEducation,true)); 
 //$userEducation=;
 //dd($userEducation); 
 //dd('here');
//dd (is_array($userEducation));
foreach(json_decode($userEducation,true) as $edu){
if($edu==$data->user_education){
	$apply=1;
	$point++;
}		
}

if($userHeightMin>=$data->user_height && $userHeightMax<=$data->user_height ){
	$apply=1;
	$point++;
}

if($userWeightMin>=$data->user_weight && $userWeightMax<=$data->user_weight ){
	$apply=1;
	$point++;
}

//$userNationality=;
foreach(json_decode($userNationality,true) as $nation){
if($nation==$data->user_nationality){
	$apply=1;
	$mainPoint++;
	$point++;
}
}

//$userCity=;
foreach(json_decode($userCity,true) as $city){
if($city==$data->user_city){
	$apply=1;
	$point++;
}
}

//$userReligion=;
foreach(json_decode($userReligion,true) as $religion){
if($religion==$data->user_religion){
	$apply=1;
	$mainPoint++;
	$point++;
}
}

if($userData->gender=='Male'){
//$userHairLook=;
foreach(json_decode($userHairLook,true) as $hairLook){
if($hairLook==$data->hair_look){
	$apply=1;
	$point++;	
}	
}

//$wifeJob=;
foreach(json_decode($wifeJob,true) as $wife){
if($wife==$data->user_profession){
	$apply=1;
	$point++;	
}	
}

//$veilStatus=;
foreach(json_decode($veilStatus,true) as $veil){
if($veil==$data->veil_status){
	$apply=1;
	$point++;	
}	
}

}
else{

//$veilStatus=;
foreach(json_decode($veilStatus,true) as $veil){
if($veil==$data->beard_status){
	$apply=1;
	$point++;	
}	
}
	
}


//$userBodyColor=;
foreach(json_decode($userBodyColor,true) as $bodyColor){
if($bodyColor==$data->body_color){
	$apply=1;
	$point++;	
}	
}

//$userProfession=;
foreach(json_decode($userProfession,true) as $profession){
if($userProfession==$data->user_profession){
	$apply=1;
	$point++;	
}	
}

//$userHobbies=;
foreach(json_decode($userHobbies,true) as $hobbies){	
$hobby=json_decode($data->user_hobbies);
foreach($hobby as $hob){
if($hob==$hobbies){
	$apply=1;
	$point++;
	break;	
}	
}
}

//$userIncome=;
foreach(json_decode($userIncomelevel,true) as $income){
if($income==$data->user_income){
	$apply=1;
	$point++;	
}	
}
//$userReligious=;
foreach(json_decode($userReligious,true) as $religious){
if($religious==$data->user_religious){
	$apply=1;
	$point++;	
}	
}

//$userTraditional=;
foreach(json_decode($traditionalStatus,true) as $traditional){
if($traditional==$data->user_traditional){
	$apply=1;
	$point++;	
}	
}

//$lookStatus=;
foreach(json_decode($lookStatus,true) as $look){
if($look==$data->look_status){
	$apply=1;
	$point++;	
}	
}

//$bodyStatus=;
foreach(json_decode($bodyStatus,true) as $body){
if($body==$data->body_status){
	$apply=1;
	$point++;	
}	
}

if($smokeStatus==$data->smoke_status){
	$apply=1;
	$point++;	
}

//$healthStatus=;
foreach(json_decode($healthStatus,true) as $health){
if($health==$data->health_status){
	$apply=1;
	$point++;	
}	
}

//$childernStatus=;
foreach(json_decode($childernStatus,true) as $children){
if($childernStatus==$data->children_status){
	$apply=1;
	$point++;	
}	
}

if($mainPoint>1){
$json[]=array('id'=>$data->user_id,'point'=>$point);
}	
}
usort($json, array($this,'my_sort'));
$array=array();

foreach($json as $js){
	
	$array[]=strval($js['id']);	
	
}

return Response::json(array('status'=>'1','idslist'=>$array,'profileOf'=>$profileOf));	
	
}


public function delUserTesting(Request $request){
	
	$userPhone=$this->validError($this->sanitizeInput($request->input('userPhone')),'userPhone');
	$data=DB::table('users')->where('phone',$userPhone)->get();
	if(count($data)){		
	DB::table('users')->where('phone',$userPhone)->delete();
	echo json_encode(array('status'=>'delete successfully'));	
	}
	else{		
	echo json_encode(array('status'=>'Not Exists'));	
	}
	
}




public function bankName(){
	$db=DB::table('bankDetails')->select('id','bankName','arabic_bankName')->get();
	// var_dump($db);
	return Response::json(array('status'=>'1','message'=>'Bank Names','response'=>$db));	
 }


public function bankDetails(Request $request)
{
	$id=$this->sanitizeInput($request->input('id'));
	
	$db=DB::table('bankDetails')->where('id',$id)->first();
	// var_dump($db);
	if(count($db)>0)
	{
		$data=DB::table('bankDetails')->where('id',$id)->first();
		return Response::json(array('status'=>'1','message'=>'Bank Detais','response'=>$data));
	}
	else
	{
		return Response::json(array('status'=>'0','message'=>'Id does not natch','response'=>array()));
	}
}

public function manMarriageRequestType(Request $request)
{
	$userId=$this->validError($this->sanitizeInput($request->input('userId')),'userId');
	
	$data=DB::table('requests')->where('type','marriageRequest')->where('user_id',$userId)->orderBy('id','DESC')->where('deactivateRequest','0')->get();
	// dd($data);
	// var_dump($data);
	if(count($data)!=0)
	{
	   foreach ($data as $dat)
	   {
		$requestId=$dat->request_id;
		$select=DB::table('users')->leftJoin('userDetails','userDetails.user_id','=','users.id')->where('users.id',$requestId)->first();
		
		$userData=$this->getUserById($dat->request_id);
		$flag=DB::table('codes')->where('answer',$userData['userNationality'])->first();
		
		$flagName=DB::table('country_flag')->where('man',$flag->code)->orWhere('girl',$flag->code)->first();
		// var_dump($userData);
		$var[]=array('id'=>$requestId,'userName'=>$select->username,'email'=>$select->email,'preacher'=>$select->preacher,'phone_code'=>$select->phone_code,'phone'=>$select->phone,'gender'=>$select->gender,'vip'=>$select->gender,'image'=>$select->image,'banAdmin'=>$select->banAdmin,'deactivate'=>$select->banAdmin,'read_admin'=>$select->read_admin,'deleteStatus'=>$select->deleteStatus,'registerType'=>$select->registerType,'loginPlatform'=>$select->loginPlatform,'regDate'=>$select->regDate,'gcmToken'=>$select->gcmToken,'sessiontime'=>$select->sessiontime,'authorization'=>$select->authorization,'user_registration'=>$select->user_registration,'type'=>'mrg','timestamp'=>$dat->timestamp,'userAge'=>$select->user_age,'userHeight'=>$select->user_height,'userWeight'=>$select->user_weight,'flagName'=>$flagName->country_flag,'userCity'=>$userData['userCity'],'mrgStatus'=>(string)$dat->status,'deactivateStatus'=>(string)$dat->deactivateRequest);
	   }
	    return Response::json(array('status'=>'1','message'=>'Man marriage request ','requestList'=>$var));
    }
	else
	{
		return Response::json(array('status'=>'0','message'=>'No request'));
	}

		
}

//webservice for cancel request...
public function cancelRequest(Request $request)
{
	  $requestUserId=$this->validError($this->sanitizeInput($request->input('requestUserId')),'requestUserId');
	  $userId=$this->validError($this->sanitizeInput($request->input('userId')),'userId');
	  
	  $data=DB::table('requests')->where('user_id',$userId)->where('request_id',$requestUserId)->where('type','marriageRequest')->where('deactivateRequest','0')->first();
	  if(count($data))
	  {
	  $query=DB::table('requests')->where('user_id',$userId)->where('request_id',$requestUserId)->where('type','marriageRequest')->where('deactivateRequest','0')->update(['deactivateRequest'=>'1']);
	   return Response::json(array('status'=>'1','message'=>'Request Canceled'));
	  }
	  else
	  {
		   return Response::json(array('status'=>'0','message'=>'Request does not exist'));
	  }
	  
}


//wbservice for ios image upload...
	// public function uploadProfileImageIos(Request $request)
	// {
		
		// $image=$request->input('image');
		// $imgData=base64_encode($image);
	    // $check=uniqid().'.png';
		
		// if(Storage::put('userImage/'.$check, $imgData))
		// {
				// $json=array("status"=>"1","message"=>"Image uploaded Successfully","response"=>$check);
		// }
		// else
		// {
		
		    // return Response::json(array('status'=>'0','imageName'=>''));
		
	    // }
		
	// }
		public function compressImage($src,$dest)
    {
		$info = getimagesize($src);
        if ($info['mime'] == 'image/jpeg') 
        {
            $image = imagecreatefromjpeg($src);
        }    
		elseif ($info['mime'] == 'image/gif')
		{
            $image = imagecreatefromgif($src);
		}
        elseif ($info['mime'] == 'image/png')
		{
			$image = imagecreatefrompng($src);    
		}
		else
		{
            return 0;
		}
		imagejpeg($image, $dest, 30);
		return 1;
	}

	public function uploadProfileImageIos(Request $request)
		{
			$data['image_data']=Input::get('image');
			$check=uniqid().'.png';
			$data=explode("base64,",$data['image_data'],2);
			$file_content = base64_decode($data[0]);
			if( Storage::put('/userImage/'.$check, $file_content))
			{
				// if($this->compressImage("../../../userImage/".$check,"../../../userImage/".$check))
				// {
				$json=array("status"=>"1","message"=>"Image uploaded Successfully","response"=>$check);
				// }
				// else
				// {
					// $json=array("status"=>"1","message"=>"Invalid size","response"=>$check);
				// }
			}
			else
			{
				$json=array("status"=>"0","message"=>"Failure","response"=>'');
			}
			return Response::json($json);
		}

	public static $add_client_rule=array(
		'userId' => 'required|numeric',
	);	
		
	public function payNow()
	{
		$data=Input::all();
		$rules = Webservice::$add_client_rule;
		$validation = Validator::make($data,$rules);
		if($validation->passes())
		{
			$data=DB::table('requests')->select('requests.*','users.username','users.phone','users.pay_chance')->join('users','users.id','=','requests.user_id')->where([['user_id',$data['userId']],['status',1],['type','marriageRequest'],['payment',0],['deactivateRequest','=','0']])->first();
			if(count($data))
			{	
				$womanData=DB::table('users')->where('id',$data->request_id)->first();
				if(count($womanData))
				{
					$json=array('status'=>'1','orderId'=>'500'.$data->id,'manName'=>$data->username,'womanName'=>$womanData->username,'phoneNo'=>$data->phone,'amount'=>'500','payChance'=>$data->pay_chance,'requestId'=>$data->id);
				}
				else
				{
					$json=array('status'=>'0','errorData'=>'Woman Data Not Found');					
				}
			}
			else
			{
				$json=array('status'=>'0','errorData'=>'Man Data Not Found');
			}
		}
		else
		{
			$json=array('status'=>'2','errorData'=>$validation->getMessageBag()->first());
		}	
		return Response::json($json,200);
	}
		
		
	public static $paymentNowValidation=array(
			'requestId'=>'required|numeric|digits_between:1,11',
			'orderId'=>'required|numeric|digits_between:1,11',
			'bankId'=>'required|numeric|digits_between:1,11',
			'payBy'=>'required|numeric|digits_between:1,11',
			'amount'=>'required|numeric|digits_between:1,11',
			'customerName'=>'required',
			'accountNo'=>'required|numeric|digits_between:1,30',
	);	
		
	public function paymentNow()
	{
		$data=Input::all();
		$rules=Webservice::$paymentNowValidation;
		$validation=Validator::make($data,$rules);
		if($validation->passes())
		{
			$dataToInsert['request_id']=$data['requestId'];
			$dataToInsert['order_id']=$data['orderId'];
			$dataToInsert['bank_id']=$data['bankId'];
			$dataToInsert['pay_by']=$data['payBy'];
			$dataToInsert['amount']=$data['amount'];
			$dataToInsert['customer_name']=$data['customerName'];
			$dataToInsert['account_no']=$data['accountNo'];
			if(Input::has('optional')):
				$dataToInsert['optional']=Input::get('optional');
			else:
				$dataToInsert['optional']="";
			endif;
			$dataToInsert['created_at']=time();
			$dataToInsert['updated_at']=time();
			DB::table('payments')->insert($dataToInsert);
			DB::table('requests')->where('id',$data['requestId'])->where('deactivateRequest','0')->update(['payment'=>'2']);
			
			$bankData=DB::table('bankDetails')->where('id',$data['bankId'])->first();
			
			
			$userData=DB::table('requests')->select('users.username','users.gcmToken','users.phone_code','users.phone')->join('users','users.id','=','requests.user_id')->where('requests.id',$data['requestId'])->where('requests.deactivateRequest','0')->first();
			
			$userDataWoman=DB::table('requests')->select('users.username','users.gcmToken','users.loginPlatform')->join('users','users.id','=','requests.request_id')->where('requests.id',$data['requestId'])->where('requests.deactivateRequest','0')->first();
			
			if(count($userData) && count($userDataWoman)):
				$message=array('type'=>'notification1','username'=>$userData->username,'sent'=>'man','action'=>'0','pay_by'=>$data['payBy']);
				Self::messages($userData->phone_code,$userData->phone,'مؤسسه وفّق العالميه للتقنية المعلومات'."\n".$bankData->arabic_bankName."\n".$bankData->accountNumber."\n".'ايبان'."\n".$bankData->IBAN."\n".'المبلغ'.$data['amount'].'ريال'."\n".'www.wuffiq.com');
				// Self::sendNotification($userDataWoman->gcmToken,$message);
				
				if($userDataWoman->loginPlatform=='Android')
				{
					Self::sendNotification($userDataWoman->gcmToken,$message);
				}
				else
				{
					$message1=array('title'=>$userData->username,'body'=>' سيقوم الخطابه بالاتصال بك قريبا','messageData'=>$message);
				    Self::send_notification_ios($userDataWoman->gcmToken,$message1);
				}

			endif;			
			$json=array('status'=>'1');
		} 
		else
		{
			$json=array('status'=>'2','errorData'=>$validation->getMessageBag()->first());
		}
		return Response::json($json,200);
	}	
	
	
	public static $wuffiqLoginRule=array(
		'userPhone' => 'required|numeric',
		'phoneCode' => 'required|numeric',
		'token' => 'required',
		'otp' => 'required|numeric',
		'loginPlatform' =>'required'
	);	
		
	
	public function wuffiqLogin(){
		
		$da=Input::all();
		$json=array();
		$rules = Webservice::$wuffiqLoginRule;
		$validation = Validator::make($da,$rules);
		if($validation->passes())
		{
			$data=DB::table('users')->where('phone',$da['userPhone'])->first();
			Self::messages($da['phoneCode'],$da['userPhone'],'الرمز السري'."\n" .$da['otp'] ."\n".'www.wuffiq.com');
			if(count($data)>0)
			{

				DB::table('users')->where('phone',$da['userPhone'])->update(['gcmToken'=>$da['token'],'loginPlatform'=>$da['loginPlatform']]);
				
				if($data->preacher==0){
					
					$data1=DB::table('users')->join('userDetails','userDetails.user_id','=','users.id')->where('users.id',$data->id)->first();

					$array=array('userId'=>$data1->user_id,'userName'=>$data1->username,'email'=>$data1->email,'userPhone'=>$data1->phone,'userGender'=>$data1->gender,'userImage'=>$data1->image,'marriedStatus'=>$data1->married_status,'marryReason'=>$data1->marry_reason,'qubelahStatus'=>$data1->qubelah_status,'qubelahName'=>$data1->qubelah_name,'userAge'=>$data1->user_age,'userEducation'=>$data1->user_education,'userHeight'=>$data1->user_height,'userWeight'=>$data1->user_weight,'userNationality'=>$data1->user_nationality,'userCity'=>$data1->user_city,'userReligion'=>$data1->user_religion,'hairColor'=>$data1->hair_color,'hairLook'=>$data1->hair_look,'bodyColor'=>$data1->body_color,'userProfession'=>$data1->user_profession,'childrenStatus'=>$data1->children_status);
					
					if($data->user_registration==1){
						$manRequest=DB::table('man_request')->where('user_id',$data->id)->first();
						$d=DB::table('userDetails')->where('user_id',$data->id)->first();
						if(count($manRequest)>0){
							if($manRequest->man_request_status==1){
								$manRequestStatus='0';	
								$manRequestData=(object)array();
							}
							else{
								$manRequestStatus='1';
								$manRequestData=array(
								'hairLook'=>$manRequest->user_hair_look=='NO HAIR' ? array() : json_decode($manRequest->user_hair_look) ,
									'marriageType'=>$manRequest->marriage_type,'marriageTypePrefer'=>json_decode($manRequest->marriage_type_prefer),'qubelahStatus'=>json_decode($manRequest->qubelah_status),'minAge'=>$manRequest->user_age_min,'maxAge'=>$manRequest->user_age_max,'lookWomenEducationList'=>json_decode($manRequest->user_education),
									'heightMin'=>$manRequest->user_height_min,'heightMax'=>$manRequest->user_height_max,'weightMin'=>$manRequest->user_weight_min,'weightMax'=>$manRequest->user_weight_max,'nationalityLogin'=>json_decode($manRequest->user_nationality),'city'=>json_decode($manRequest->user_city),'religion'=>json_decode($manRequest->user_religion),'hairColor'=>json_decode($manRequest->user_hair_color),'bodyColor'=>json_decode($manRequest->user_body_color),'job'=>json_decode($manRequest->user_profession)
									,'qubelahName'=>json_decode($manRequest->qubelah_name)
									);
							}				
							
						}
						else{
							
							$manRequestStatus='2';	
							$manRequestData=(object)array();							
						}
													
						if($data->gender=='Male'){
							$requestData=DB::table('requests')->where([['user_id',$data->id],['type','marriageRequest'],['status','1'],['deactivaterequest','0']])->first();
						}
						else{
							$requestData=DB::table('requests')->where([['request_id',$data->id],['type','marriageRequest'],['status','1'],['deactivateRequest','0']])->first();
						}
						
						if(count($requestData)>0)
						{
							$array['marriageRequestTime']=intval($requestData->timestamp);
							if($requestData->payment==0){
								$data->deactivate=3;					// payment is still pending
							}
							elseif($requestData->payment==1){
																			//status 4 means fully cancelled
								$preacherRequest=DB::table('preacher_requests')->where('request_id',$requestData->id)->where('status','!=','3')->orderBy('id','desc')->first();
								if(count($preacherRequest)>0){
									if($preacherRequest->status==0){
											$data->deactivate=4;
									}
									elseif($preacherRequest->status==1){
											$data->deactivate=2;
											DB::table('users')->where('id',$data->id)->update(['deactivate'=>2]);
									}
									elseif($preacherRequest->status==3){
											$data->deactivate=4;
									}
									else{
											$data->deactivate=0;
									}
											
								}
								else{
								$data->deactivate=4;	
								}
													// payment done and approvedd by admin
							}
							elseif($requestData->payment==2){
								$data->deactivate=5;					// payment done but pendng from admin
							}
							elseif($requestData->payment==3){ 
								$data->deactivate=0;					// payment cancel by admin
							}
							$pay=DB::table('payments')->join('bankDetails','bankDetails.id','=','payments.bank_id')->where('payments.request_id',$requestData->id)->orderBy('payments.id','Desc')->first();
							if(count($pay)>0){
									$array['paymentTime']=$pay->created_at;
									$array['selectedBank']=$pay->bankName;
									$array['requestId']=(string)$pay->request_id;
							}
							
						}
						$json=array('status'=>'1','message'=>'fullData','banAdmin'=>$data->banAdmin,'deactivate'=>(string)$data->deactivate,'preacher'=>$data->preacher,'manRequestStatus'=>$manRequestStatus,'preferenceResponse'=>$manRequestData,'responsePreacher'=>(object)array(),'response'=>$array);
						
					}
					else{
						
						$json=array('status'=>'1','message'=>'halfData','banAdmin'=>$data->banAdmin,'deactivate'=>(string)$data->deactivate,'preacher'=>$data->preacher,'manRequestStatus'=>'2','preferenceResponse'=>(object)array(),'response'=>$array,'responsePreacher'=>(object)array());
						
					}
				}
				else{
					$arrayData=DB::table('users')->join('preacherDetails','users.id','=','preacherDetails.preacher_id')->join('city_status','city_status.status','=','preacherDetails.city')->where('users.id',$data->id)->first();
					$array=array(
						'preacherId'=>$arrayData->preacher_id,
						'preacherName'=>$arrayData->username,
						'preacherPhone'=>$arrayData->phone,
						'preacherEmail'=>$arrayData->email,
						'preacherCode'=>$arrayData->preacherCode,
						'preacherReligion'=>$arrayData->religion,
						'preacherCity'=>$arrayData->arabic_status,
						'preacherService'=>json_decode($arrayData->service),
						'bankName'=>$arrayData->bank_name,
						'acHolderName'=>$arrayData->ac_holder_name,
						'accountNo'=>$arrayData->account_no,
						'marriageType'=>json_decode($arrayData->marriage_type)
					
					);
					
					
					$json=array('status'=>'1','message'=>'fullData','banAdmin'=>$arrayData->banAdmin,'deactivate'=>(string)$arrayData->deactivate,'preacher'=>$data->preacher,'manRequestStatus'=>'2','preferenceResponse'=>(object)array(),'response'=>(object)array(),'responsePreacher'=>$array);  
				}
				
			}
			else{
				$json=array('status'=>'0','message'=>'notExist','banAdmin'=>'0','deactivate'=>'0','response'=>(object)array());
			}
		}
		else
		{
			$json=array('status'=>'2','errorData'=>$validation->getMessageBag()->first());
		}	
		return Response::json($json,200);
		
		
	}
	
	
	public function regularHit(){
		
		$userData=DB::table('preacher_requests')->where('status','0')->orWhere('status','3')->get();
		foreach($userData as $data){
			
			if(time()-$data->time>84600)
			{
				DB::table('requests')->where('id',$data->request_id)->where('deactivateRequest','0')->first();
			}
			
			
		}
		
	}
	
	
	public function getAllPokeRequests(Request $request)
	{
		$userId=$this->sanitizeInput($request->Input('userId'));
		
			$check=DB::table('requests')->where('type','poke')->where('user_id',$userId)->get();
			if(count($check))
			{
				foreach ($check as $ch)
				{

				$userData=$this->getUserById($ch->request_id);
					
				$flag=DB::table('codes')->where('answer',$userData['userNationality'])->first();
				
				$flagName=DB::table('country_flag')->where('man',$flag->code)->orWhere('girl',$flag->code)->first();
				$status=DB::table('requests')->select('status')->where('user_id',$userId)->where('request_id',$ch->request_id)->where('type','marriageRequest')->first();
			    $marriageRequest=$this->requestCount($userId,$ch->request_id,'marriageRequest');
				$json[]=array(
				'requestUserId'=>$ch->request_id,
				'requestId'=>$ch->id,
			    'type'=>'pokeSent', 
				'status'=>count($status)==1 ? $status->status : 0,
				'requestUserName'=>$userData['userName'],
				'flagName'=>$flagName->country_flag,
				'userNationality'=>$userData['userNationality'],
				'userCity'=>$userData['userCity'],
				'userAge'=>$userData['userAge'],
				'userHeight'=>$userData['userHeight'],
				'userWeight'=>$userData['userWeight'],
				'userImage'=>$userData['userImage'],
				'timestamp'=>$ch->timestamp,
				'pstatus'=>$ch->status,
				'marriageRequestCount'=>$marriageRequest);	
				
				}
				$json=array('status'=>'1','message'=>'Poke request exists','requestList'=>$json);
			}
			else
			{
				$json=array('status'=>'0','message'=>'No Request','requestList'=>array());
			}
		
		
		return Response::json($json,200);
	}
		


public function cancelIgnoredRequest()
{
	$all=Input::all();
	$check=DB::table('requests')->where('id',$all['requestId'])->first();
	if(count($check))
	{
	     $data=DB::table('requests')->where('id',$all['requestId'])->where('type','marriageRequest')->update(['deactivateRequest'=>'1']);
	     $da1=DB::table('users')->where('id',$check->user_id)->update(['deactivate'=>'0']);
	     $da2=DB::table('users')->where('id',$check->request_id)->update(['deactivate'=>'0']);
	     $user=DB::table('users')->where('id',$check->user_id)->first();
	     $user2=DB::table('users')->where('id',$check->request_id)->first();
	     $message1=array('type'=>'notification1','username'=>$user->username,'sent'=>'admin','action'=>'5');

		if($user->loginPlatform=='Android'){
			Self::sendNotification($user->gcmToken,$message1);
		}
		else{
			$message2=array('title'=>'لقد تم الغاء الطلب بسبب عدم دفع الرسوم في الوقت المحدد.','body'=>'','messageData'=>$message1);
			Self::send_notification_ios($user->gcmToken,$message2);
		}

		if($user2->loginPlatform=='Android'){
			Self::sendNotification($user2->gcmToken,$message1);
		}
		else{
			$message2=array('title'=>'لقد تم الغاء الطلب بسبب عدم دفع الرسوم في الوقت المحدد.','body'=>'','messageData'=>$message1);
			Self::send_notification_ios($user2->gcmToken,$message2);
		}


	     return 1;
	}
}


public static $validatorSearch=array('userId'=>'required|exists:users,id',
'marriageType'=>'required',
'marriageTypePrefer'=>'required',
'qabelahStatus'=>'required',
'qabelahName'=>'required',
'userAgeMin'=>'required',
'userAgeMax'=>'required',
'userEducation'=>'required',
'userHeightMin'=>'required',
'userHeightMax'=>'required',
'userWeightMin'=>'required',
'userWeightMax'=>'required',
'userNationality'=>'required',
'userCity'=>'required',
'userReligion'=>'required',
'userHairColor'=>'required',
'userHairLook'=>'required',
'userBodyColor'=>'required',
'userProfession'=>'required',
'userHobbies'=>'required',
'userIncomelevel'=>'required',
'homeAfterMarriage'=>'required',
'userReligious'=>'required',
'traditionalStatus'=>'required',
'lookStatus'=>'required',
'bodyStatus'=>'required',
'smokeStatus'=>'required',
'healthStatus'=>'required',
'veilBeardStatus'=>'required',
'wifeJob'=>'required',
'childernStatus'=>'required',
'gender'=>'required');
public function search()
{
	    $all=Input::all();
		$rules=Webservice::$validatorSearch;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
			  $json=array();	
              $count=0;	
		      $all=Input::all();
	          $profile=$all['gender'];
			  // echo $profile;
	       if($profile=='Male')
		   {
			   // dd($profile);
			   $profileOf='Female';
		   }
		   else
		   {
			   $profileOf='Male';
		   }
		   // dd($profileOf);
	          $userFullData=DB::table('users')->select('users.*','userDetails.*','userDetailsFull.*','man_request.marriage_type as marriage_type_req','man_request.marriage_type_prefer as marriage_type_prefer_req','man_request.qubelah_status as qubelah_status_req','man_request.qubelah_name as qubelah_name_req','man_request.user_age_min as user_age_min_req','man_request.user_age_max as user_age_max_req','man_request.user_education as user_education_req','man_request.user_height_min as user_height_min_req','man_request.user_height_max as user_height_max_req','man_request.user_weight_min as user_weight_min_req','man_request.user_weight_max as user_weight_max_req','man_request.user_nationality as user_nationality_req','man_request.user_city as user_city_req','man_request.user_religion as user_religion_req','man_request.user_hair_color as user_hair_color_req','man_request.user_hair_look as user_hair_look_req','man_request.user_body_color as user_body_color_req','man_request.user_profession as user_profession_req','man_request_full.user_hobbies as user_hobbies_req','man_request_full.user_income as user_income_req','man_request_full.fertile_status as fertile_status_req','man_request_full.home_after_marriage as home_after_marriage_req','man_request_full.user_religious as user_religious_req','man_request_full.traditional_status as traditional_status_req','man_request_full.look_status as look_status_req','man_request_full.body_status as body_status_req','man_request_full.smoke_status as smoke_status_req','man_request_full.health_status as health_status_req','man_request_full.veil_status as veil_status_req','man_request_full.wife_job as wife_job_req','man_request_full.children_status as children_status_req')->join('userDetails','userDetails.user_id','=','users.id')->join('userDetailsFull','userDetailsFull.user_id','=','users.id')->join('man_request','man_request.user_id','=','users.id')->join('man_request_full','man_request_full.user_id','=','users.id')->where([['users.user_registration','=','1'],['users.preacher','=','0'],['banAdmin','=','0'],['deactivate','=','0'],['man_request.man_request_status','=','1']])->where('users.gender','!=',$profile)->get();
			// dd($userFullData);
			  	foreach($userFullData as $userAllData)
				{
				
					if(count(json_decode($all['qabelahStatus']))==2):
						$qstatus=3;
					elseif(in_array('MQ1',json_decode($all['qabelahStatus'])) || in_array('GQ1',json_decode($all['qabelahStatus']))):
						$qstatus=1;
					else:
						$qstatus=3;
					endif;
					
                          if($profile=='Female'&& in_array($userAllData->user_nationality,json_decode($all['userNationality'])) && in_array($userAllData->user_religion,json_decode($all['userReligion'])) && $all['userAgeMax']>=$userAllData->user_age && $all['userAgeMin']<=$userAllData->user_age && in_array($userAllData->beard_status,json_decode($all['veilBeardStatus'])) && in_array($userAllData->user_profession,json_decode($all['userProfession'])) && in_array($userAllData->married_status,json_decode($all['marriageTypePrefer'],true)) && in_array($userAllData->qubelah_status,json_decode($all['qabelahStatus'],true)) && $userAllData->marriage_type_req==$all['marriageType'] && in_array($userAllData->user_education,json_decode($all['userEducation'],true)) && in_array($userAllData->body_color,json_decode($all['userBodyColor'])) && in_array($userAllData->hair_color,json_decode($all['userHairColor'])) && in_array($userAllData->user_city,json_decode($all['userCity'])) && (array_intersect(json_decode($userAllData->user_hobbies),json_decode($all['userHobbies'])) || in_array($userAllData->user_hobbies,json_decode($all['userHobbies']))) && in_array($userAllData->user_religious,json_decode($all['userReligious'])) && in_array($userAllData->user_income,json_decode($all['userIncomelevel'])) && in_array($userAllData->user_traditional,json_decode($all['traditionalStatus'])) && in_array($userAllData->look_status,json_decode($all['lookStatus'])) && in_array($userAllData->body_status,json_decode($all['bodyStatus'])) && in_array($userAllData->health_status,json_decode($all['healthStatus']))&& (in_array($userAllData->qubelah_name,json_decode($all['qabelahName'],true)) || $qstatus==3) )
			        	  {
                              $json[]=array('id'=>strval($userAllData->user_id),'name'=>$userAllData->username,'gender'=>$userAllData->gender);
					      }	
					
                    else if(
					$profile=='Male' && in_array($userAllData->user_nationality,json_decode($all['userNationality'])) && in_array($userAllData->user_religion,json_decode($all['userReligion']))  && $all['userAgeMin']<=$userAllData->user_age && $all['userAgeMax']>=$userAllData->user_age  && in_array($userAllData->veil_status,json_decode($all['veilBeardStatus'])) && in_array($userAllData->user_profession,json_decode($all['userProfession']))  && in_array($userAllData->married_status,json_decode($all['marriageTypePrefer'],true))  && in_array($userAllData->qubelah_status,json_decode($all['qabelahStatus'],true)) &&$userAllData->marriage_type_req==$all['marriageType']  && (in_array($userAllData->qubelah_name,json_decode($all['qabelahName'],true)) || $qstatus==3) && in_array($userAllData->user_education,json_decode($all['userEducation'],true))  && $all['userHeightMax']>=$userAllData->user_height && $all['userHeightMin']<=$userAllData->user_height && $all['userWeightMin']<=$userAllData->user_weight && $all['userWeightMax']>=$userAllData->user_weight && in_array($userAllData->children_status,json_decode($all['childernStatus'])) && in_array($userAllData->body_color,json_decode($all['userBodyColor']))  && in_array($userAllData->hair_color,json_decode($all['userHairColor'])) && in_array($userAllData->user_city,json_decode($all['userCity']))  && (array_intersect(json_decode($userAllData->user_hobbies),json_decode($all['userHobbies'])) || in_array($userAllData->user_hobbies,json_decode($all['userHobbies']))) && (in_array($userAllData->user_religious,json_decode($all['userReligious'])) || array_intersect(json_decode($userAllData->user_Religious),json_decode($all['userReligious']))) && (in_array($userAllData->user_income,json_decode($all['userIncomelevel'])) ) && in_array($userAllData->user_traditional,json_decode($all['traditionalStatus'])) && in_array($userAllData->look_status,json_decode($all['lookStatus'])) && in_array($userAllData->body_status,json_decode($all['bodyStatus'])) && in_array($userAllData->health_status,json_decode($all['healthStatus'])))
					{
						// $var[]=array('userId'=>$userAllData->user_id,'userName'=>$userAllData->username,'usercity'=>$userAllData->user_city);
						// dd($userAllData);
					    $json[]=array('id'=>strval($userAllData->user_id),'name'=>$userAllData->username,'gender'=>$userAllData->gender);
					}						
                   
				}
				// dd($var);
			   $array=array_pluck($json, 'id');
			return Response::json(array('status'=>'1','idslist'=>array_slice($array,$count,10),'profileOf'=>$profileOf),200);
				
	    }
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>(object)array()]);
		}
}
  
  public function send_notification_ios($deviceToken,$message){


 // Put your private key's passphrase here:
  	    $passphrase = '123456';
		$nottype='123';
 ////////////////////////////////////////////////////////////////////////////
 		$ctx = stream_context_create();
 		stream_context_set_option($ctx, 'ssl', 'local_cert','/var/www/vhosts/wuffiq.com/httpdocs/webs/WuffiqPushCertificate.pem');
 		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
 // dd($ctx);
 // Open a connection to the APNS server
 		$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$fp)
		// exit("Failed to connect: $err $errstr" . PHP_EOL);
		// echo 'Connected to APNS' . PHP_EOL;
		ob_flush();
		flush();
 // Encode the payload as JSON
        $body['aps'] = array(
            // 'alert' => json_encode($message),
			'alert' =>array(
			"title"=>$message['title'],
			"body"=>$message['body']),
            'sound' => 'default',
            'badge' => 1,
			'content-available' => 1,
			'message'=>json_encode($message['messageData']),
        );

 		$payload = json_encode($body);
 // Build the binary notification
 		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
	
 // Send it to the server
 		stream_set_blocking ($fp,0);
 		$result = fwrite($fp, $msg, strlen($msg));
 		if (!$result)
 		{
  		// echo json_encode(array('status'=>'0')) . PHP_EOL;
 		 // echo $fp;
 		}
 		else
 		{
 		// echo json_encode(array('status'=>'1')) .PHP_EOL;
  		  ob_flush();
  		  flush();
 		}
 		// self::checkAppleErrorResponse($fp);
 		fclose($fp);

}

public static $deactivate=array('userId'=>'required|exists:users,id');
public function getDeactivateStatus()
{
	 $all=Input::all();
		$rules=Webservice::$deactivate;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
			$data=DB::table('users')->where('id',$all['userId'])->first();
			if(count($data)>0)
			{
		
				      if($data->preacher==0)
				      {
						  		if($data->user_registration==1)
				{
					        if($data->gender=='Male')
					        {
					             $requestData=DB::table('requests')->where([['user_id',$data->id],['type','marriageRequest'],['status','1'],['deactivaterequest','0']])->first();
					        }
					       else
					        {
					            $requestData=DB::table('requests')->where([['request_id',$data->id],['type','marriageRequest'],['status','1'],['deactivateRequest','0']])->first();
					        }
                             if(count($requestData)>0)
						    {
								$approvedStatus=$requestData->approved;
							$array['marriageRequestTime']=intval($requestData->timestamp);
							if($requestData->payment==0){
								$data->deactivate=3;					// payment is still pending
							}
							elseif($requestData->payment==1){
																			//status 4 means fully cancelled
								$preacherRequest=DB::table('preacher_requests')->where('request_id',$requestData->id)->where('status','!=','3')->orderBy('id','desc')->first();
								if(count($preacherRequest)>0){
									if($preacherRequest->status==0){
											$data->deactivate=4;
									}
									elseif($preacherRequest->status==1){
											$data->deactivate=2;
											DB::table('users')->where('id',$data->id)->update(['deactivate'=>2]);
									}
									elseif($preacherRequest->status==3){
											$data->deactivate=4;
									}
									else{
											$data->deactivate=0;
									}
											
								}
								else{
								$data->deactivate=4;	
								}
													// payment done and approvedd by admin
							}
							elseif($requestData->payment==2){
								$data->deactivate=5;					// payment done but pendng from admin
							}
							elseif($requestData->payment==3){ 
								$data->deactivate=0;					// payment cancel by admin
							}
							$pay=DB::table('payments')->join('bankDetails','bankDetails.id','=','payments.bank_id')->where('payments.request_id',$requestData->id)->orderBy('payments.id','Desc')->first();
							if(count($pay)>0){
									$array['paymentTime']=$pay->created_at;
									$array['selectedBank']=$pay->bankName;
									$array['requestId']=(string)$pay->request_id;
							}
							}
							else
							{
								$approvedStatus='0';
							}
							
								$json=array('status'=>'1','message'=>'fulldata','deactivate'=>(string)$data->deactivate,'approved'=>$approvedStatus );
						}
					else{
						$approvedStatus='0';
						$json=array('status'=>'1','message'=>'halfData','deactivate'=>(string)$data->deactivate,'approved'=>$approvedStatus);
						
					}
					  }
				else
				{
					$arrayData=DB::table('users')->join('preacherDetails','users.id','=','preacherDetails.preacher_id')->join('city_status','city_status.status','=','preacherDetails.city')->where('users.id',$data->id)->first();
					$approvedStatus='0';
                    $json=array('status'=>'1','message'=>'fullData','deactivate'=>(string)$arrayData->deactivate,'approved'=>$approvedStatus);  
				}
			}
			return Response::json($json,200);
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>(object)array()]);
		}
}


}
?>