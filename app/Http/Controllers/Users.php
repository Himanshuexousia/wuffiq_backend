<?php

namespace App\Http\Controllers;

use Twilio;

use illuminate\Http\Request;
 
use App\Http\Requests;

// use Aloha\Twilio\TwilioInterface;
 
use DB;

use app;

use File;

use Hash;

use Auth;

use Response;



// use Facade;

class Users extends Controller
{

public function index(){
	
	$users=DB::table('users')->where('preacher','0')->orderBy('id','DESC')->get();
	return view('all-users',compact('users'));
	
}


public function logout(Request $request){
	$request->session()->flush();
	return Redirect()->back();
}


public function dashboard(){
	
	$totalUsers=DB::table('users')->where('preacher','0')->get();
	
	$totalPreacher=DB::table('users')->where('preacher','1')->get();

	$banUsers=DB::table('users')->where([['preacher','=','0'],['banAdmin','=','1']])->orderBy('id','DESC')->get();

	$vipUsers=DB::table('users')->where([['preacher','=','0'],['vip','=','1']])->orderBy('id','DESC')->get();
	
	$marriageRequests=DB::table('requests')->where('type','marriageRequest')->get();
	
	$array=array('totalUsers'=>$totalUsers,
				'totalPreacher'=>$totalPreacher,
				'banUsers'=>$banUsers,
				'vipUsers'=>$vipUsers,
				'marriageRequests'=>$marriageRequests
	);
	
	return view('dashboard-social',compact('array'));
}


public function dbAdminLogin(Request $request){
	$all=$request->all();
	$user=DB::table('admin')->where('admin_email',$all['email'])->first();
	if(count($user)>0){	
	if(Hash::check($all['password'], $user->admin_password)) {	
	$request->session()->put('wuffiqAdmin',$all['email']);	
    echo json_encode(array('status'=>'1'));
	}
	else
	{
	echo json_encode(array('status'=>'3'));
	}		
	}
	else
	{
		echo json_encode(array('status'=>'2'));
	}
	
	
}


public function viewUser(Request $request,$name){
	// $users=DB::table('users')->where('id',$name)->first();
	// $users->
	$requests=array();
	$users=$this->getUserById($name);
	$request=DB::table('requests')->where('user_id',$name)->get();
	foreach($request as $req){
		
		$data=DB::table('users')->where('id',$req->request_id)->first();
		if($req->type=='poke'){ 
			$type="Poked";
		}
		elseif($req->type=='fav'){
			$type="Favourites";
		}
		elseif($req->type=='marriageRequest'){
			$type="Send Marriage Request to";
		}
		
		
		$requests[]=array(
		'timestamp'=>$req->timestamp,
		'status'=>$req->status,
		'type'=>$req->type,
		'activityType'=>$type,
		'requestedName'=>$data->username
		);
		
	}
	// dd($users);
	// dd(json_decode($users['userHobbies']));
	return view('view_user',compact('users','requests'));
	
	
}


public function getUserById($id){
		
$data=DB::table('users')->where('id',$id)->first();
$data1=DB::table('userDetails')->where('user_id',$id)->first();
// dd($data1);
$json['userId']=$id;
$json['userName']=$data->username;
$json['userEmail']=$data->email;
$json['userPhone']=$data->phone;
$json['userImage']=$data->image;
$json['userGender']=$data->gender;
$json['banAdmin']=$data->banAdmin;
$json['deActive']=$data->deactivate;
$json['deleteStatus']=$data->deleteStatus;
$json['registerType']=$data->registerType;
$json['loginPlatform']=$data->loginPlatform;
$json['regDate']=$data->regDate;
$json['token']=$data->gcmToken;
$json['sessionTime']=$data->sessiontime;
$json['authorization']=$data->authorization;
$json['userRegistration']=$data->user_registration;
$json['marriedStatus']=$this->getCodeById($data1->married_status);
$json['marryReason']=$data1->marry_reason;
$json['qubelahStatus']=$this->getCodeById($data1->qubelah_status);
if($json['qubelahStatus']=='Yes'){
// $json['qubelahStatus']=$this->getCodeById($data1->qubelah_status);	
// dd($json['qubelahName'] =$data1->qubelah_name);
$json['qubelahName']=$data1->qubelah_name;
// dd($json['qubelahName']);
}
// else{	
// $json['qubelahStatus']=$this->getCodeById($data1->qubelah_status);	
// }

// $json['qubelahName']=$this->getCodeById($data1->qubelah_name);
$json['userAge']=$data1->user_age;
$json['userEducation']=$this->getCodeById($data1->user_education);
$json['userHeight']=$data1->user_height;
$json['userWeight']=$data1->user_weight;
$json['userNationality']=$this->getCodeById($data1->user_nationality);
$json['userCity']=$this->getCodeById($data1->user_city);
$json['userReligion']=$this->getCodeById($data1->user_religion);
$json['hairColor']=$this->getCodeById($data1->hair_color);
$json['bodyColor']=$this->getCodeById($data1->body_color);
$json['userProfession']=$this->getCodeById($data1->user_profession);
$json['userRegistration']=$data->user_registration;
if($data1->children_status==0){
$json['childrenStatus']='No';	
}
else{
$json['childrenStatus']=$this->getCodeById($data1->children_status);	
}


if($data->user_registration==1){
$data2=DB::table('userDetailsFull')->where('user_id',$id)->first();
$json['userHobbies']=$data2->user_hobbies;
$json['userIncome']=$this->getCodeById($data2->user_income);
$json['userReligious']=$this->getCodeById($data2->user_religious);
$json['userTraditional']=$this->getCodeById($data2->user_traditional);
$json['lookStatus']=$this->getCodeById($data2->look_status);
$json['bodyStatus']=$this->getCodeById($data2->body_status);
$json['smokeStatus']=$data2->smoke_status;
$json['healthStatus']=$this->getCodeById($data2->health_status);
$json['littleSickType']=$data2->little_sick_type;
$json['disableType']=$data2->disable_type;
$json['otherInformation']=$data2->other_information;
$json['privateInformation']=$data2->private_information;
$json['preatureStatus']=$data2->preature_status;
$json['preatureAccount']=$data2->preature_account;
if($data->gender=='Male'){
$json['beardStatus']=$this->getCodeById($data2->beard_status);
}
else{
$json['hairLookStatus']=$this->getCodeById($data1->hair_look);
$json['veilStatus']=$this->getCodeById($data2->veil_status);
$json['virginStatus']=$data2->virgin_status;
$json['smsNumber']=$data2->sms_number;	
}
$json['arabiaStatus']=$data2->arabia_status;

}
return $json;
}


public function editUser(Request $request,$id){
$city=DB::table('city_status')->where('id','!=',54)->orderBy('id','Asc')->get();	
$data=$this->getIdData($id);
$qubelahName=DB::table('Qabelah_Name')->where('id','!=',169)->get();	
$country=DB::table('country_flag')->where([['country_name','!=',"I don't care"]])->where([['country_name','!=','Kaligi']])->orderBy('flag_id','DESC')->get();
 // dd($data['bodyColor']);
if($data['userGender']=='Male'){
return view('edit_user',compact('country','qubelahName','data','city'));
}
else{
return view('edit_user_female',compact('country','qubelahName','data','city'));
}
}

public function manRequest(Request $request,$id){
$city=DB::table('city_status')->get();	
$data=$this->getIdData($id);
$man=$this->getManData($id);
$qubelahName=DB::table('Qabelah_Name')->get();	
$country=DB::table('country_flag')->where([['country_name','!=','Kaligi']])->orderBy('flag_id','DESC')->get();
$man_request=DB::table('man_request')->where('user_id',$id)->first();
$man_request_full=DB::table('man_request_full')->where('user_id',$id)->first();

if($data['userGender']=='Male'){
return view('man_request_female',compact('country','qubelahName','data','city','man_request','man_request_full','man'));
}
else{
	// dd($country,$qubelahName,$data,$city,$man_request,$man_request_full,$man);
return view('man_request',compact('country','qubelahName','data','city','man_request','man_request_full','man'));
}
}


public function getManData($id)
{
	$man=DB::table('man_request')->where('user_id',$id)->first();
	$json['marriageType']=$man->marriage_type;
	$json['marriageTypePreffered']=$man->marriage_type_prefer;
	$json['qubelahStatus']=$man->qubelah_status;
	$json['qubelahName']=$man->qubelah_name;	
	$json['education']=$man->user_education;
	return $json;
}
public function getIdData($id){
	
$data=DB::table('users')->where('id',$id)->first();
$data1=DB::table('userDetails')->where('user_id',$id)->first();
$json['userId']=$id;
$json['userName']=$data->username;
$json['userEmail']=$data->email;
$json['userPhone']=$data->phone;
$json['userGender']=$data->gender;
$json['banAdmin']=$data->banAdmin;
$json['deActive']=$data->deactivate;
$json['deleteStatus']=$data->deleteStatus;
$json['registerType']=$data->registerType;
$json['loginPlatform']=$data->loginPlatform;
$json['regDate']=$data->regDate;
$json['token']=$data->gcmToken;
$json['sessionTime']=$data->sessiontime;
$json['authorization']=$data->authorization;
$json['userRegistration']=$data->user_registration;
$json['marriedStatus']=$data1->married_status;
$json['marryReason']=$data1->marry_reason;
$json['qubelahStatus']=$data1->qubelah_status;			
$json['qubelahName']=$data1->qubelah_name;	
// $json['qubelahName']=$this->getCodeById($data1->qubelah_name);
$json['userAge']=$data1->user_age;
$json['userEducation']=$data1->user_education;
$json['userHeight']=$data1->user_height;
$json['userWeight']=$data1->user_weight;
$json['userNationality']=$data1->user_nationality;
$json['userCity']=$data1->user_city;
$json['userReligion']=$data1->user_religion;
$json['hairColor']=$data1->hair_color;
$json['bodyColor']=$data1->body_color;
$json['userProfession']=$data1->user_profession;

$json['userRegistration']=$data->user_registration;
if($data1->children_status==0){
$json['childrenStatus']='No';	
}
else{
$json['childrenStatus']=$data1->children_status;	
}


if($data->user_registration==1){
$data2=DB::table('userDetailsFull')->where('user_id',$id)->first();
$json['userHobbies']=$data2->user_hobbies;
$json['userIncome']=$data2->user_income;
$json['userReligion']=$data2->user_religious;
$json['userTraditional']=$data2->user_traditional;
$json['lookStatus']=$data2->look_status;
$json['bodyStatus']=$data2->body_status;
$json['smokeStatus']=$data2->smoke_status;
$json['healthStatus']=$data2->health_status;
$json['littleSickType']=$data2->little_sick_type;
$json['disableType']=$data2->disable_type;
$json['otherInformation']=$data2->other_information;
$json['privateInformation']=$data2->private_information;
$json['preatureStatus']=$data2->preature_status;
$json['preatureAccount']=$data2->preature_account;
// dd($data,$data2);
if($data->gender=='Male'){
$json['beardStatus']=$data2->beard_status;
}	
else{
$json['beardStatus']='';
$json['hairLookStatus']=$data2->hair_look_status;
$json['veilStatus']=$data2->veil_status;
$json['virginStatus']=$data2->virgin_status;
$json['smsNumber']=$data2->sms_number;	
}
$json['arabiaStatus']=$data2->arabia_status;

}
return $json;
}


public function getCodeById($code){
	
	$codeData=DB::table('codes')->where('code',$code)->first();
	if(count($codeData)==0)
	{
	return $codeData=' ';	
	}
	else
	{
	return $codeData->answer;
	}
}


public function updateUser(Request $request){
	$all=$request->all();
	$userHobbies=$request->input('userHobbies');
	DB::table('users')->where('id',$all['userId'])->update(['username'=>$all['userName'],'email'=>$all['userEmail'],'user_registration'=>'1']);
	if($all['userGender']=="Female"){
	DB::table('userDetails')->where('user_id',$all['userId'])->update(['married_status'=>$all['marriedStatus'],'body_color'=>$all['bodycolor'],'qubelah_status'=>$all['qubelahStatus'],'qubelah_name'=>$all['qubelahName'],'user_age'=>$all['userAge'],'user_education'=>$all['userEducation'],'user_height'=>$all['userHeight'],'user_weight'=>$all['userWeight'],'user_nationality'=>$all['userNationality'],'user_city'=>$all['userCity'],'user_religion'=>$all['userReligion'],'hair_color'=>$all['hairColor'],'hair_look'=>$all['hairLook'],'user_profession'=>$all['userProfession'],'children_status'=>$all['childrenStatus']]);
	
	DB::table('userDetailsFull')->where('user_id',$all['userId'])->update(['user_hobbies'=>json_encode($userHobbies),'user_income'=>$all['userIncome'],'user_religious'=>$all['userReligious'],'user_traditional'=>$all['userTraditional'],'look_status'=>$all['userLook'],'body_status'=>$all['userBody'],'smoke_status'=>$all['userSmoke'],'health_status'=>$all['userHealth'],'little_sick_type'=>$all['sickType'],'disable_type'=>$all['disableType'],'other_information'=>$all['otherInformation'],'private_information'=>$all['privateInformation'],'preature_status'=>$all['preacherStatus'],'preature_account'=>$all['preacherAccount'],'arabia_status'=>$all['arabiaStatus'],'veil_status'=>$all['veilStatus'],'virgin_status'=>$all['virginStatus'],'sms_number'=>$all['smsNumber']]);
	echo  json_encode(array('status'=>'1'));
	}
	else{
		
		DB::table('userDetails')->where('user_id',$all['userId'])->update(['married_status'=>$all['marriedStatus'],'marry_reason'=>$all['marryReason'],'qubelah_status'=>$all['qubelahStatus'],'qubelah_name'=>$all['qubelahName'],'user_age'=>$all['userAge'],'user_education'=>$all['userEducation'],'user_height'=>$all['userHeight'],'user_weight'=>$all['userWeight'],'user_nationality'=>$all['userNationality'],'user_city'=>$all['userCity'],'user_religion'=>$all['userReligion'],'hair_color'=>$all['hairColor'],'user_profession'=>$all['userProfession'],'children_status'=>$all['childrenStatus']]);
	
		DB::table('userDetailsFull')->where('user_id',$all['userId'])->update(['user_hobbies'=>json_encode($userHobbies),'user_income'=>$all['userIncome'],'user_religious'=>$all['userReligious'],'user_traditional'=>$all['userTraditional'],'look_status'=>$all['userLook'],'body_status'=>$all['userBody'],'smoke_status'=>$all['userSmoke'],'health_status'=>$all['userHealth'],'little_sick_type'=>$all['sickType'],'disable_type'=>$all['disableType'],'other_information'=>$all['otherInformation'],'private_information'=>$all['privateInformation'],'preature_status'=>$all['preacherStatus'],'preature_account'=>$all['preacherAccount'],'arabia_status'=>$all['arabiaStatus'],'beard_status'=>$all['beardStatus']]);	
		echo  json_encode(array('status'=>'1'));
	}
}



public function suspended(){
	
$users=DB::table('users')->where([['preacher','=','0'],['banAdmin','=','1']])->orderBy('id','DESC')->get();
return view('all-users',compact('users'));	
	
}

public function vip(){
	
$users=DB::table('users')->where([['preacher','=','0'],['vip','=','1']])->orderBy('id','DESC')->get();
return view('all-users',compact('users'));	
	
	
}

public function dbUserAction(Request $request){
	$all=$request->all();
	if($all['type']=='1'){
		DB::table('users')->where('id',$all['id'])->update(['banAdmin'=>'1']);		
	}
	elseif($all['type']=='2'){
		DB::table('users')->where('id',$all['id'])->delete();		
		$data=DB::table('requests')->where([['user_id','=',$all['id']],['request_id','=',$all['id']]])->get();
		foreach($data as $allData){
			DB::table('preacher_requests')->where('id',$allData->id)->delete();	
		}
		DB::table('requests')->where('user_id',$all['id'])->delete();		
		DB::table('requests')->where('request_id',$all['id'])->delete();		
	}
	elseif($all['type']=='3'){
		DB::table('users')->where('id',$all['id'])->update(['vip'=>'1']);		
	}
	elseif($all['type']=='4'){
		DB::table('users')->where('id',$all['id'])->update(['banAdmin'=>'0']);		
	}
	elseif($all['type']=='5'){
		DB::table('users')->where('id',$all['id'])->update(['vip'=>'0']);		
	}
	elseif($all['type']=='6'){
		DB::table('users')->where('id',$all['id'])->update(['image'=>'']);		
	}
	
	
	
}


public function dbReadStatus(Request $request)
{
	$all=$request->all();
	DB::table('users')->where('id',$all['id'])->update(['read_admin'=>'1']);
	
	
}


public function pictures(Request $request)
{
	
	$images=DB::table('users')->where([['image','!=',''],['image','!=','No image']])->orderBy('id','DESC')->get();
	return view('all-pictures',compact('images'));
	
}

public function getNotifications(Request $request)
{
	$array=array();
	$userRegistered=DB::table('notification')->where([['seen','=','0'],['type','=','registered']])->get();
	if(count($userRegistered)>0){
		$array[]='<li><a href="http://www.wuffiq.com/admin/all-users"> <i class="fa fa-user text-aqua"></i>'.count($userRegistered).' New user registered </a></li>';	
	}
	echo json_encode(array('array'=>$array,'count'=>count($array)));
}

public function readNotification(Request $request)
{
	
	DB::table('notification')->update(['seen'=>'1']);	
}

}