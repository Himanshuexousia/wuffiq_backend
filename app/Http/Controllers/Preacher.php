<?php

namespace App\Http\Controllers;

use Twilio;

use illuminate\Http\Request;
 
use App\Http\Requests;

// use Aloha\Twilio\TwilioInterface;
 
use DB;

use app;

use File;

use Hash;

use Auth;

use Response;

use Redirect;



// use Facade;

class Preacher extends Controller
{

public function index(){
	
	$preacher=DB::table('users')->where('preacher','1')->orderBy('id','DESC')->get();
// dd($preacher);
	return view('all-preacher',compact('preacher'));
	
}

public function suspended(){
	$preacher=DB::table('users')->where([['preacher','=','1'],['banAdmin','=','1']])->orderBy('id','DESC')->get();
	return view('all-preacher',compact('preacher'));
	
}

public function addPreacher(){
	
	
	return view('addPreacher');
	
	
}

public function addDbPreacher(Request $request){
	
	$name=$request->input('name');
	$email=$request->input('email');
	$phone=$request->input('phone');
	$phoneCode=$request->input('phoneCode');
	$marriageType=$request->input('marriageType');
	$religion=$request->input('religion');
	$cityLive=$request->input('cityLive');
	$service=json_encode($request->input('service'));
	$bankName=$request->input('bankName');
	$bankAccountName=$request->input('bankAccountName');
	$bankAccount=$request->input('bankAccount');

	
	
	$mobileExist=DB::table('users')->where('phone',$phone)->get();
	if(count($mobileExist)>0){		
	echo  json_encode(array('status'=>'0'));
	die();	
	}
	
	$code=$this->random_string(4);
	
	$preacherId=DB::table('users')->insertGetId(['username'=>$name,'email'=>$email,'phone_code'=>$phoneCode,'phone'=>$phone,'preacher'=>'1','registerType'=>'web','regDate'=>time(),'preacherCode'=>$code]);
	
	$data=DB::table('preacherDetails')->insert(['preacher_id'=>$preacherId,'marriage_type'=>$marriageType,'religion'=>$religion,'city'=>$cityLive,'service'=>$service,'bank_name'=>$bankName,'ac_holder_name'=>$bankAccountName,'account_no'=>$bankAccount]);
	
	echo  json_encode(array('status'=>'1'));
	
}


public function editPreacher($number,Request $request){
	
	$data=DB::table('users')->where('users.id',$number)->join('preacherDetails','users.id','=','preacherDetails.preacher_id')->first();
	return view('editPreacher',compact('data'));
	
}

public function updateDbPreacher(Request $request){
	
	$preacherId=$request->input('preacherId');
	$name=$request->input('name');
	$email=$request->input('email');
	$phone=$request->input('phone');
	$marriageType=$request->input('marriageType');
	$religion=$request->input('religion');
	$cityLive=$request->input('cityLive');
	$service=json_encode($request->input('service'));
	$bankName=$request->input('bankName');
	$bankAccountName=$request->input('bankAccountName');
	$bankAccount=$request->input('bankAccount');	

	$preacher=DB::table('users')->where('id',$preacherId)->update(['username'=>$name,'email'=>$email]);
	
	$data=DB::table('preacherDetails')->where('preacher_id',$preacherId)->update(['marriage_type'=>$marriageType,'religion'=>$religion,'city'=>$cityLive,'service'=>$service,'bank_name'=>$bankName,'ac_holder_name'=>$bankAccountName,'account_no'=>$bankAccount]);
	
	echo  json_encode(array('status'=>'1'));	
	
}

public function deletePreacher($number,Request $request){
	
	DB::table('users')->where('id',$number)->delete();
	return Redirect::back();
}


public function random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range(0, 9));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}

public function preacherRequests($number,Request $request)
{ 
	$select=DB::table('preacher_requests')->select('preacher_requests.time as time','preacher_requests.id as req_id','preacher_requests.preacher_id as preachId','preacher_requests.status as astatus' ,'preacher_requests.id as preachRequestId','requests.*')->where('preacher_id',$number)->where('preacher_requests.status',0)->Join('requests','requests.id','=','preacher_requests.request_id')->get();
return view('view-marriage-requests-preacher',compact('select'));
	// print_R($select);
}


public function allpreacherRequests(Request $request)
{ 
	// $select=DB::table('preacher_requests')->select('preacher_requests.time as time','preacher_requests.id as req_id','preacher_requests.preacher_id as preachId','preacher_requests.status as astatus' ,'preacher_requests.id as preachRequestId','requests.*')->Join('requests','requests.id','=','preacher_requests.request_id')->get();
	$select=DB::table('preacher_requests')->select('preacher_requests.time as time','preacher_requests.id as req_id','requests.user_id as user_id','requests.request_id as request_id','preacher_requests.preacher_id as preachId','preacher_requests.status as astatus' ,'preacher_requests.id as preachRequestId','requests.*')->leftJoin('requests','requests.id','=','preacher_requests.request_id')->where('requests.type','marriageRequest')->where('requests.approved','0')->where('preacher_requests.status','!=','0')->get();
	
	
	return view('view-marriage-requests-preacher',compact('select'));
	// print_R($select);
}

public function approval(Request $request)
{ 
	// $select=DB::table('preacher_requests')->select('preacher_requests.time as time','preacher_requests.id as req_id','preacher_requests.preacher_id as preachId','preacher_requests.status as astatus' ,'preacher_requests.id as preachRequestId','requests.*')->Join('requests','requests.id','=','preacher_requests.request_id')->get();
	$select=DB::table('preacher_requests')->select('preacher_requests.time as time','preacher_requests.id as req_id','requests.user_id as user_id','requests.request_id as request_id','preacher_requests.preacher_id as preachId','preacher_requests.status as astatus' ,'preacher_requests.id as preachRequestId','requests.*')->leftJoin('requests','requests.id','=','preacher_requests.request_id')->where('requests.type','marriageRequest')->where('requests.approved','1')->get();
	
	
	return view('approval-requests',compact('select'));
	// print_R($select);
}

public function acceptPreacherRequest(Request $request)
{ 
	$preachId=$request->input('preachId'); 
	$requestId=$request->input('requestId');
	$check=DB::table('preacher_requests')->where('preacher_id',$preachId)->where('request_id',$requestId)->where('status','0')->get(); 
	if(count($check))
	{
		$query=DB::table('preacher_requests')->where('preacher_id',$preachId)->where('requestId',$requestId)->update(['status'=>'1']);
		// if($query)
		// {
			// $user=DB::table('users')->where('id','requestId')->
		// }
	}
	// return view('view-marriage-requests-preacher',compact('select')); 
	// return Response::json(array('status'=>'1','response'=>$check));
	return 1;
}

public function orders(){
	$json=array();
	$orders=DB::table('requests')->where('type','marriageRequest')->where('deactivateRequest','0')->orderBy('id','DESC')->get();
	// dd($orders);
	foreach($orders as $order){
		$preacherName=DB::table('preacher_requests')->select('users.username','preacher_requests.status')->leftJoin('users','users.id','=','preacher_requests.preacher_id')->where('users.preacher','1')->where('preacher_requests.request_id',$order->id)->first();
		$bank=DB::table('payments')->where('request_id',$order->request_id)->leftJoin('bankDetails','bankDetails.id','=','payments.bank_id')->first();
		
			$orderId=$order->id;
			$boyId=$order->user_id;
			$girlId=$order->request_id;
			$time=$order->timestamp;
			$marriageStatus=$order->status;
			$payment=$order->payment;
		    
			if(empty($preacherName->username ) && empty($preacherName->status))
		{
			$preacher='';
			$preacherStatus='';
		}
		else
		{
			$preacher=$preacherName->username;
			$preacherStatus=$preacherName->status;
		}
		if(empty($bank->bankName))
		{
			$bankName='';
		}
		else
		{
			$bankName=$bank->bankName;
		}
			

		$json[]=array
		(
			'orderId'=>$orderId,
			'boyId'=>$boyId,
			'girlId'=>$girlId,
			'preacher'=>$preacher,
			'time'=>$time,
			'marriageStatus'=>$marriageStatus,
			'bankName'=>$bankName,
			'payment'=>$payment,
			'preacherStatus'=>$preacherStatus
		);
		
	}
	
	
	return view('orders',compact('json'));
	
}


public function requestAction()
{
		$select=DB::table('preacher_requests')->select('preacher_requests.time as time','preacher_requests.id as req_id','requests.user_id as user_id','requests.request_id as request_id','preacher_requests.preacher_id as preachId','preacher_requests.status as astatus' ,'preacher_requests.id as preachRequestId','requests.*')->leftJoin('requests','requests.id','=','preacher_requests.request_id')->where('requests.type','marriageRequest')->where('requests.approved','0')->where('preacher_requests.status','0')->get();
	
	
	return view('request-action',compact('select'));
}

}