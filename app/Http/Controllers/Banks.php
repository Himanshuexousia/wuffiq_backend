<?php

namespace App\Http\Controllers;

use App\User;

use App\Requ;

use Twilio;

use illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use App\Http\Requests;

use Illuminate\Support\Facades\Cache;
 
use DB;

use app;

use File;

use Hash;

use Session;

use Auth;

use Input;

use Route;

use Response;

use Services_Twilio;

use Eloquent;

use Validator;

class Banks extends Controller
{

	public function index()
	{
		
		$json=DB::table('bankDetails')->get();
		
		return view('banks',compact('json'));
		
	}

	
	public function update($bankId)
	{
	
		$bankDetail=DB::table('bankDetails')->where('id',$bankId)->first();
		
		return view('bank-edit',compact('bankDetail'));
	}
	
	public function updateBank(){
		
		$data=Input::all();
		DB::table('bankDetails')->where('id',$data['bankId'])->update(['bankName'=>$data['bankName'],'name'=>$data['name'],'accountNumber'=>$data['accountNumber'],'IBAN'=>$data['IBAN'],'arabic_bankName'=>$data['arabicBankName'],'arabic_name'=>$data['arabicName']]);
		Session::flash('message','Update SuccessFull');
		return redirect()->back();
	}
	
	
	public function addBank(){
		
		$data=Input::all();
		
		DB::table('bankDetails')->insert(['bankName'=>$data['bankName'],'name'=>$data['name'],'accountNumber'=>$data['accountNumber'],'IBAN'=>$data['IBAN'],'arabic_bankName'=>$data['arabicBankName'],'arabic_name'=>$data['arabicName']]);
		Session::flash('message','Added SuccessFull');
		return redirect()->back();
	}


}