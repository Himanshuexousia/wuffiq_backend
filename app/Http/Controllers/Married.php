<?php

namespace App\Http\Controllers;

use App\User;

use App\Requ;

use Twilio;

use illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use App\Http\Requests;

use Illuminate\Support\Facades\Cache;
// use Aloha\Twilio\TwilioInterface;
 
use DB;

use app;

use File;

use Hash;

use Auth;

use Route;

use Response;

use Services_Twilio;

use Eloquent;

class Req extends Eloquent{
	
	protected $table='requests';
	public $timestamps = false;
}

class Married extends Controller
{

public function allMarriedRequests()
{
	$data=DB::table('requests')->where('type','marriageRequest')->orderBy('request_id','DESC')->get();
	$d=array();
	if(count($data)!=0)
	{
		foreach($data as $dat)
		{
		    $check=DB::table('users')->where('id',$dat->user_id)->first();
		    if(count($check)!=0)
		    {
			    $check2=DB::table('users')->where('id',$dat->request_id)->first();
			    if(count($check2)!=0)
			    {
				    $var[]=array('request_id'=>$dat->id,'user_id'=>$dat->user_id,'requested_id'=>$dat->request_id,'username'=>$check->username,'requestedName'=>$check2->username,'status'=>$dat->status,'timestamp'=>$dat->timestamp);
			    }
			
		    }
		
		}
		$d=$var;
		 // return Response::json(['status'=>'1','message'=>'user exists','response'=>$d]);
		return view('all-married-requests',compact('d'));
	}
	else
	{
		$var[]=array('request_id'=>'','user_id'=>'','requested_id'=>'','username'=>'','requestedName'=>'','status'=>'','timestamp'=>'');
		$d=$var;
		 return view('all-married-requests',compact('d'));
	}
	
	
   
}



public function allAcceptedRequests()
{
	$data=DB::table('requests')->where('type','marriageRequest')->where('status','1')->orderBy('request_id','DESC')->get();
	$d=array();
	if(count($data)!=0)
	{
		foreach($data as $dat)
		{
		    $check=DB::table('users')->where('id',$dat->user_id)->first();
		    if(count($check)!=0)
		    {
			    $check2=DB::table('users')->where('id',$dat->request_id)->first();
			    if(count($check2)!=0)
			    {
				    $var[]=array('request_id'=>$dat->id,'user_id'=>$dat->user_id,'requested_id'=>$dat->request_id,'username'=>$check->username,'requestedName'=>$check2->username,'status'=>$dat->status,'timestamp'=>$dat->timestamp);
			    }
			
		    }
		
		}
		$d=$var;
		 // return Response::json(['status'=>'1','message'=>'user exists','response'=>$d]);
		return view('all-married-requests',compact('d'));
	}
	else
	{
		$var[]=array('request_id'=>'','user_id'=>'','requested_id'=>'','username'=>'','requestedName'=>'','status'=>'','timestamp'=>'');
		$d=$var;
		 return view('all-married-requests',compact('d'));
	}
	
	
   
}

public function allRejectedRequests()
{
	$data=DB::table('requests')->where('type','marriageRequest')->where('status','2')->orderBy('request_id','DESC')->get();
	$d=array();
	if(count($data)!=0)
	{
		foreach($data as $dat)
		{
		    $check=DB::table('users')->where('id',$dat->user_id)->first();
		    if(count($check)!=0)
		    {
			    $check2=DB::table('users')->where('id',$dat->request_id)->first();
			    if(count($check2)!=0)
			    {
				    $var[]=array('request_id'=>$dat->id,'user_id'=>$dat->user_id,'requested_id'=>$dat->request_id,'username'=>$check->username,'requestedName'=>$check2->username,'status'=>$dat->status,'timestamp'=>$dat->timestamp);
			    }
			
		    }
		
		}
		$d=$var;
		 // return Response::json(['status'=>'1','message'=>'user exists','response'=>$d]);
		return view('all-married-requests',compact('d'));
	}
	else
	{
		$var[]=array('request_id'=>'','user_id'=>'','requested_id'=>'','username'=>'','requestedName'=>'','status'=>'','timestamp'=>'');
		$d=$var;
		 return view('all-married-requests',compact('d'));
	}
	
	
   
}


public function ignoredAcceptedRequests()
{
	
	
	$date=DB::table('requests')->where('type','marriageRequest')->where('status','1')->where('payment','0')->where('deactivateRequest','0') ->orderBy('request_id','DESC')->groupBy('id')->get();

	$da=array();
	if(count($date)!=0)
	{
		foreach($date as $dat)
		{
		    $check=DB::table('users')->where('id',$dat->user_id)->first();
		    if(count($check)!=0)
		    {
			    $check2=DB::table('users')->where('id',$dat->request_id)->first();
			    if(count($check2)!=0)
			    {
				    $var[]=array('request_id'=>$dat->id,'user_id'=>$dat->user_id,'requested_id'=>$dat->request_id,'username'=>$check->username,'requestedName'=>$check2->username,'status'=>$dat->status,'payment'=>$dat->payment);
			    }
			
		    }
		
		}
		$da=$var;
	
	}
	 return view('ignored-accepted-requests',compact('da'));
	
}


public function pendingRequests()
{
	$data=DB::table('requests')->where('type','marriageRequest')->where('status','0')->orderBy('request_id','DESC')->get();
	$d=array();
	if(count($data)!=0)
	{
		foreach($data as $dat)
		{
		    $check=DB::table('users')->where('id',$dat->user_id)->first();
		    if(count($check)!=0)
		    {
			    $check2=DB::table('users')->where('id',$dat->request_id)->first();
			    if(count($check2)!=0)
			    {
				    $var[]=array('request_id'=>$dat->id,'user_id'=>$dat->user_id,'requested_id'=>$dat->request_id,'username'=>$check->username,'requestedName'=>$check2->username,'status'=>$dat->status,'timestamp'=>$dat->timestamp);
			    }
			
		    }
		
		}
		$d=$var;
		 // return Response::json(['status'=>'1','message'=>'user exists','response'=>$d]);
		return view('all-married-requests',compact('d'));
	}
	else
	{
		$var[]=array('request_id'=>'','user_id'=>'','requested_id'=>'','username'=>'','requestedName'=>'','status'=>'','timestamp'=>'');
		$d=$var;
		 return view('all-married-requests',compact('d'));
}

}

}




?>