<?php

namespace App\Http\Middleware;

use Closure;
use Session;
class loginCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null){

	if (!Session::has('wuffiqAdmin')) {
	return redirect('login');
	}
	return $next($request);	
	}	
       
}

