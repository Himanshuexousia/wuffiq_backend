<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['loginCheck']], function () {

    Route::get('/', [ 'as' => '/','uses' =>'Users@dashboard']);
	
	Route::get('payments-pending', [ 'as' => 'payments-pending','uses' =>'Payments@pendingPayments']);
	
	Route::get('payments-done', [ 'as' => 'payments-done','uses' =>'Payments@donePayments']);
	
	Route::get('payments-cancel', [ 'as' => 'payments-cancel','uses' =>'Payments@cancelPayments']);
	
	Route::get('all-users', [ 'as' => 'all-users','uses' =>'Users@index']);
	
	Route::get('banks', [ 'as' => 'banks','uses' =>'Banks@index']);
	
	Route::get('banks-add', [ 'as' => 'banks-add',function(){
		return view('bank-add');
	}]);
	
	Route::get('banks/edit/{number}', [ 'as' => 'banks/edit','uses' =>'Banks@update']);
	
	Route::get('all-users', [ 'as' => 'all-users','uses' =>'Users@index']);
	
	Route::get('getNotifications', [ 'as' => 'getNotifications','uses' =>'Users@getNotifications']);
	
	Route::get('readNotification', [ 'as' => 'readNotification','uses' =>'Users@readNotification']);
	
	Route::get('all-suspended-user', [ 'as' => 'all-suspended-user','uses' =>'Users@suspended']);
	
	Route::get('all-vip-user', [ 'as' => 'all-vip-user','uses' =>'Users@vip']);
	
	Route::get('preacher-add', [ 'as' => 'preacher-add','uses' =>'Preacher@addPreacher'
    ]);		
	
	Route::get('preacher-all', [ 'as' => 'preacher-all','uses' =>'Preacher@index']);
	
	Route::get('logout', [ 'as' => 'logout','uses' =>'Users@logout']);
	 
	Route::get('pictures', [ 'as' => 'pictures','uses' =>'Users@pictures']);
	
	Route::get('preacher-suspended', [ 'as' => 'preacher-suspended','uses' =>'Preacher@suspended']);
	
	Route::get('preacher-requests', [ 'as' => 'preacher-requests','uses' =>'Preacher@allpreacherRequests']);

	Route::get('request-action', [ 'as' => 'request-action','uses' =>'Preacher@requestAction']);
	
	Route::get('approval', [ 'as' => 'approval','uses' =>'Preacher@approval']);
	
	Route::get('orders', [ 'as' => 'orders','uses' =>'Preacher@orders']);
	
	Route::get('preacher-edit-{number}', [ 'as' => 'preacher-edit-','uses' =>'Preacher@editPreacher'
    ]);
	
	Route::get('preacher-requests-{number}', [ 'as' => 'preacher-requests-','uses' =>'Preacher@preacherRequests'
    ]);
	
	Route::post('addDbPreacher', [ 'as' => 'addDbPreacher','uses' =>'Preacher@addDbPreacher'
    ]);
	
	Route::post('updateDbPreacher', [ 'as' => 'updateDbPreacher','uses' =>'Preacher@updateDbPreacher'
    ]);
	
	Route::get('deletePreacher{number}', [ 'as' => 'deletePreacher','uses' =>'Preacher@deletePreacher'
    ]);
	
	
	
	Route::post('update-bank', [ 'as' => 'update-bank','uses' =>'Banks@updateBank']);
	
	Route::post('add-bank', [ 'as' => 'update-bank','uses' =>'Banks@addBank']);
	
	Route::get('all-marriage-requests', [ 'as' => 'all-married-requests','uses' =>'Married@allMarriedRequests']);
	
	Route::get('pending-marriage-requests', [ 'as' => 'pending-marriage-requests','uses' =>'Married@pendingRequests']);
	
	Route::get('all-accepted-requests', [ 'as' => 'all-accepted-requests','uses' =>'Married@allAcceptedRequests']);
	
	Route::get('ignored-accepted-requests', [ 'as' => 'ignored-accepted-requests','uses' =>'Married@ignoredAcceptedRequests']);
	
	Route::get('all-rejected-requests', [ 'as' => 'all-rejected-requests','uses' =>'Married@allRejectedRequests']);
	
	Route::get('all-users/view-id-{name}', [ 'as' => 'all-users/view','uses' =>'Users@viewUser']);
	
	Route::get('all-users/edit-id-{name}', [ 'as' => 'all-users/edit','uses' =>'Users@editUser']);
	
	Route::get('all-users/man-request-id-{name}', [ 'as' => 'all-users/edit','uses' =>'Users@manRequest']);
	
	
	
    Route::get('responsive', [ 'as' => 'responsive', function () {
		$user='';
		$title='';
        return view('responsive',compact('user,title'));
    }]);
    Route::get('documentation', [ 'as' => 'documentation', function () {
        return view('documentation.infinity');
    }]);
    Route::get('back-end', [ 'as' => 'back-end', function () {
        return view('dashboard-social');
    }]);
    Route::get('home', [ 'as' => 'home', function () {
        return view('dashboard-social');
    }]);
	
	Route::post('sessions/store', ['as' => 'sessions/store', 'uses' => 'AbcController@store123']);

    Route::get('auth/sample', [ 'as' => 'auth-sample', function () {
        return view('auth-sample');
    }]);

		// Dashboards
		Route::group(['prefix' => 'dashboard'], function () {
        Route::get('business', [ 'as' => 'dashboard-business', function () {
            return view('dashboard-business');
        }]);
        Route::get('sales', [ 'as' => 'dashboard-sales', function () {
            return view('dashboard-sales');
        }]);
        Route::get('marketing', [ 'as' => 'dashboard-marketing', function () {
            return view('dashboard-marketing');
        }]);
        Route::get('social', [ 'as' => 'dashboard-social', function () {
            return view('dashboard-social');
        }]);
        Route::get('projects', [ 'as' => 'dashboard-projects', function () {
            return view('dashboard-projects');
        }]);

		});

		// Forms
    Route::group(['prefix' => 'form'], function () {
        Route::get('general', [ 'as' => 'form-general', function () {
            return view('form-general');
        }]);
        Route::get('advanced', [ 'as' => 'form-advanced', function () {
            return view('form-advanced');
        }]);
        Route::get('editor', [ 'as' => 'form-editor', function () {
            return view('form-editor');
        }]);
    });

    // Mailbox
    Route::group(['prefix' => 'mailbox'], function () {
        Route::get('inbox', [ 'as' => 'mail-inbox', function () {
            return view('mail-inbox');
        }]);
        Route::get('message', [ 'as' => 'mail-message', function () {
            return view('mail-message');
        }]);
        Route::get('contacts', [ 'as' => 'contacts-list', function () {
            return view('contacts-list');
        }]);
    });

    // Calendar
    Route::get('calendar', [ 'as' => 'calendar', function () {
        return view('calendar');
    }]);

    // Tables
    Route::group(['prefix' => 'table'], function () {
        Route::get('simple', [ 'as' => 'table-simple', function () {
            return view('table-simple');
        }]);
        Route::get('data', [ 'as' => 'table-data', function () {
            return view('table-data');
        }]);
    });

    // Charts
    Route::group(['prefix' => 'charts'], function () {
        Route::get('google-chart-tools', [ 'as' => 'google-chart-tools', function () {
            return view('google-chart-tools');
        }]);
        Route::get('chart-js', [ 'as' => 'chart-js', function () {
            return view('chart-js');
        }]);
        Route::get('peity', [ 'as' => 'peity', function () {
            return view('peity');
        }]);
    });
	
	// Users
    Route::group(['prefix' => 'users'], function () {
        
    });
    // Ui Elements
    Route::group(['prefix' => 'ui-elements'], function () {
        Route::get('general', [ 'as' => 'general', function () {
            return view('ui-general');
        }]);
        Route::get('grid', [ 'as' => 'grid', function () {
            return view('grid');
        }]);
        Route::get('icons', [ 'as' => 'icons', function () {
            return view('icons');
        }]);
        Route::get('buttons', [ 'as' => 'buttons', function () {
            return view('buttons');
        }]);
        Route::get('sliders', [ 'as' => 'sliders', function () {
            return view('sliders');
        }]);
        Route::get('timeline', [ 'as' => 'timeline', function () {
            return view('timeline');
        }]);
        Route::get('modal', [ 'as' => 'modal', function () {
            return view('modal');
        }]);
        Route::get('cards', [ 'as' => 'cards', function () {
            return view('cards');
        }]);
        Route::get('tabs', [ 'as' => 'tabs', function () {
            return view('tabs');
        }]);
        Route::get('bootstrap-misc', [ 'as' => 'bootstrap-misc', function () {
            return view('bootstrap-misc');
        }]);
        Route::get('alerts', [ 'as' => 'alerts', function () {
            return view('alerts');
        }]);
    });

    // Plugins
    Route::group(['prefix' => 'plugins'], function () {
        Route::get('toastr', [ 'as' => 'plu-toastr', function () {
            return view('plu-toastr');
        }]);
        Route::get('pace', [ 'as' => 'plu-pace', function () {
            return view('plu-pace');
        }]);
    });

    // Pages
    Route::group(['prefix' => 'pages'], function () {
        Route::get('invoice', [ 'as' => 'page-invoice', function () {
            return view('invoice');
        }]);
        Route::get('profile', [ 'as' => 'page-user-profile', function () {
            return view('user-profile');
        }]);

        Route::get('login', [ 'as' => 'page-login', function () {
            return view('login-page');
        }]);
        Route::get('lockscreen', [ 'as' => 'page-lockscreen', function () {
            return view('lockscreen');
        }]);
        Route::get('login2', [ 'as' => 'page-login2', function () {
            return view('login-page-2');
        }]);
        Route::get('lockscreen2', [ 'as' => 'page-lockscreen2', function () {
            return view('lockscreen-2');
        }]);
        Route::get('404', [ 'as' => 'page-404', function () {
            return view('errors.404');
        }]);
        Route::get('500', [ 'as' => 'page-500', function () {
            return view('errors.500');
        }]);
        Route::get('blank', [ 'as' => 'page-blank', function () {
            return view('blank-page');
        }]);
    });

});

//every route in here will be accessed by logged users only.
Route::group(['middleware' => ['web']], function () {
	Route::post('dbAdminLogin', [ 'as' => 'dbAdminLogin','uses' =>'Users@dbAdminLogin']);
});


Route::group(['prefix' => 'API/'], function()
{


	


Route::POST('payNow',['as'=>'payNow' , 'uses' => 'Webservice@payNow']);

Route::POST('paymentAction',['as'=>'paymentAction' , 'uses' => 'Payments@paymentAction']);

Route::POST('paymentNow',['as'=>'paymentNow' , 'uses' => 'Webservice@paymentNow']);

Route::POST('delUserTesting',['as'=>'delUserTesting' , 'uses' => 'Webservice@delUserTesting']);

Route::POST('searchService',['as'=>'searchService' , 'uses' => 'Webservice@search']);

Route::POST('dbReadStatus',['as'=>'dbReadStatus' , 'uses' => 'Users@dbReadStatus']);

Route::POST('dbUserAction',['as'=>'dbUserAction' , 'uses' => 'Users@dbUserAction']);

Route::POST('cityStatus',['as'=>'cityStatus' , 'uses' => 'Webservice@cityStatus']);

Route::POST('registerUser',['as'=>'registerUser' , 'uses' => 'Webservice@registerUser']);

Route::POST('userDetails',['as'=>'userDetails' , 'uses' => 'Webservice@userDetails']);
	
Route::POST('loginUser',['as'=>'loginUser' , 'uses' => 'Webservice@loginUser']);

Route::POST('qabelahName',['as'=>'qabelahName' , 'uses' => 'Webservice@qabelahName']);
	
Route::POST('otp',['as'=>'otp' , 'uses' => 'Webservice@sendOtp']);
	
Route::POST('wuffiqLogin',['as'=>'wuffiqLogin' , 'uses' => 'Webservice@wuffiqLogin']);	

Route::POST('userRegistration',['as'=>'userRegistration' , 'uses' => 'Webservice@userRegister']);	

Route::POST('userRegisterFull',['as'=>'userRegisterFull' , 'uses' => 'Webservice@userRegisterFull']);	

Route::POST('userProfile',['as'=>'userProfile' , 'uses' => 'Webservice@userRegisteredProfile']);
	 
// Route::POST('filterMainData',['as'=>'filterMainData' , 'uses' => 'Webservice@filterMainData']);	

Route::POST('filterMainData',['as'=>'filterMainData' , 'uses' => 'Webservice@filterTest']);	

Route::POST('acceptPreacherRequest',['as'=>'acceptPreacherRequest' , 'uses' => 'Preacher@acceptPreacherRequest']);	


Route::POST('countryPhoneCode',['as'=>'countryPhoneCode' , 'uses' => 'Webservice@countryPhoneCode']);

Route::POST('countryFlags',['as'=>'countryFlags' , 'uses' => 'Webservice@countryFlags']);
	
Route::POST('uploadImage',['as'=>'uploadImage' , 'uses' => 'Webservice@uploadImage']);

Route::POST('uploadProfileImageIos',['as'=>'uploadProfileImageIos' , 'uses' => 'Webservice@uploadProfileImageIos']);

Route::POST('manRequest',['as'=>'manRequest' , 'uses' => 'Webservice@manRequest']);

Route::POST('manRequestFull',['as'=>'manRequestFull' , 'uses' => 'Webservice@manRequestFull']);

Route::POST('manRequestProfile',['as'=>'manRequestProfile' , 'uses' => 'Webservice@manRequestProfile']);

Route::POST('phoneCode',['as'=>'phoneCode' , 'uses' => 'Webservice@phoneCode']);

Route::POST('userRequest',['as'=>'userRequest' , 'uses' => 'Webservice@userRequest']);	

Route::POST('getRequestByType',['as'=>'getRequestByType' , 'uses' => 'Webservice@getRequestByType']);	 

Route::POST('requestAction',['as'=>'requestAction' , 'uses' => 'Webservice@requestAction']);
	
Route::POST('getPreacher',['as'=>'getPreacher' , 'uses' => 'Webservice@getPreacher']);	

Route::POST('preacherProfile',['as'=>'preacherProfile' , 'uses' => 'Webservice@preacherProfile']);	

Route::POST('userfavList',['as'=>'userfavList' , 'uses' => 'Webservice@userfavList']);	

// Route::POST('addBankDetails',['as'=>'addBankDetails' , 'uses' => 'Webservice@addBankDetails']);

Route::GET('bankName',['as'=>'bankName' , 'uses' => 'Webservice@bankName']);

Route::POST('bankDetails',['as'=>'bankDetails' , 'uses' => 'Webservice@bankDetails']);	

Route::POST('marrigeRequests',['as'=>'marrigeRequests' , 'uses' => 'Webservice@marrigeRequests']);	

Route::POST('exchangeRequest',['as'=>'exchangeRequest' , 'uses' => 'Webservice@exchangeRequest']);

Route::any('actionByPreacher',['as'=>'actionByPreacher' , 'uses' => 'Webservice@actionByPreacher']);

Route::any('actionByPreacher2',['as'=>'actionByPreacher2' , 'uses' => 'Webservice@actionByPreacher2']);

Route::POST('regularHit',['as'=>'regularHit' , 'uses' => 'Webservice@regularHit']);

Route::POST('preacherNotificationList',['as'=>'preacherNotificationList' , 'uses' => 'Webservice@preacherNotificationList']);

Route::POST('manMarriageRequestType',['as'=>'manMarriageRequestType' , 'uses' => 'Webservice@manMarriageRequestType']);

Route::POST('cancelRequest',['as'=>'cancelRequest' , 'uses' => 'Webservice@cancelRequest']);

Route::POST('getAllPokeRequests',['as'=>'getAllPokeRequests' , 'uses' => 'Webservice@getAllPokeRequests']);

Route::POST('cancelIgnoredRequest',['as'=>'cancelIgnoredRequest' , 'uses' => 'Webservice@cancelIgnoredRequest']); 

Route::POST('filters',['as'=>'filters' , 'uses' => 'Filters@filterTest2']); 

Route::POST('/apn',['as'=>'apn' , 'uses' => 'Filters@send_notification_pas']);

 Route::GET('d',['as'=>'d' , 'uses' => 'Filters@d']);  
 
 Route::post('filter',['as'=>'filter3' , 'uses' => 'Filters@filter3']); 
 
 Route::post('updateUser', [ 'as' => 'updateUser','uses' =>'Users@updateUser']);

 Route::post('getDeactivateStatus', ['uses' => 'Webservice@getDeactivateStatus']);

Route::get('payments-pending', [ 'as' => 'payments-pending','uses' =>'Payments@pendingPayments']);
 
Route::get('sendMessage/{name}/{name1}', function($number,$message)
{
  // Get form inputs
  // $number = '+919996968968';
  // $message = Input::get('message');

  // Create an authenticated client for the Twilio API
  $client = new Services_Twilio('AC2d33b9dfdf71f2987d4541a0dbb4a2c0', '9812ac0f92962b4665adf79d2477e3f4');

  try {
    // Use the Twilio REST API client to send a text message
    $m = $client->account->messages->sendMessage(
      '+1 765-626-3110', // the text will be sent from your Twilio number
      $number, // the phone number the text will be sent to
      $message // the body of the text message
    );
  } catch(Services_Twilio_RestException $e) {
    // Return and render the exception object, or handle the error as needed
    // return $e;
  };


  // Return the message object to the browser as JSON
  // return $m;
});




	
});

