<?php
use Illuminate\Database\Seeder;

class SentinelRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        Sentinel::getRoleRepository()->createModel()->create(array(
            'name'        => 'Users',
            'slug'        => 'users',
            'permissions' => array(
                'users.list' => true
            )));

        Sentinel::getRoleRepository()->createModel()->create(array(
            'name'        => 'Administrators',
            'slug'        => 'admins',
            'permissions' => array(
                'users.list' => true,
                'users.edit' => true,
                'roles.edit'  => true,
                'guest.list' => true,
                'guest.edit' => true
            )));
    }
}
