(function ($, CheckUi) {

    "use strict";

    /**
    * available themes
    *
    * @type Array
    */
    var themes = [
    "light-blue",
    "aqua",
    "yellow",
    "blue",
    "navy",
    "teal",
    "olive",
    "lime",
    "orange",
    "fuchsia",
    "maroon",
    "light-gray",
    "gray",
    "black",
    "red",
    "purple",
    "green",
    "pink",
    "white",
    "transparent",
    "glass"
    ];

    var backgrounds = [
        "bg-black-laser", "bg-black-velvet", "bg-blue-techno", "bg-dark-wood", "bg-rainbow", "bg-digital-rainbow", "bg-digital-sun", "bg-fractal-ocean",
        "bg-fractal-rainbow", "bg-gray-lights", "bg-pink", "bg-pink-and-blue", "bg-sunset", "bg-gradientdark-blue", "bg-gradient-pink", "bg-gradient-green",
        "bg-gradient-blue", "bg-glow-green", "bg-glow-pink", "bg-glow-blue", "bg-sepia-green", "bg-sepia-blue", "bg-sepia-yellow", "bg-green-yellow", "bg-white-fractal",
        "bg-white-fractal2", "bg-white-laser", "bg-solid-white", "bg-solid-black", "bg-solid-dark", "bg-solid-gray"
    ]

    //Create the new tabs
    var tab_colors_pane = $("<div />", {
    "id": "control-sidebar-theme-demo-options-tab",
    "class": "tab-pane active"
    });

    var tab_options_pane = $("<div />", {
        "id": "control-sidebar-theme-options-tab",
        "class": "tab-pane"
    });

    var tab_background_pane = $("<div />", {
        "id": "control-sidebar-theme-background-tab",
        "class": "tab-pane"
    });

    //Create the tab buttons
    var tab_button_colors = $("<li />", {"class": "nav-item"})
      .html("<a href='#control-sidebar-theme-demo-options-tab' data-toggle='tab' class='nav-link active'>"
      + "<i class='fa fa-paint-brush'></i>"
      + "</a>");

    var tab_button_options = $("<li />", {"class": "nav-item"})
        .html("<a href='#control-sidebar-theme-options-tab' data-toggle='tab' class='nav-link'>"
        + "<i class='fa fa-desktop'></i>"
        + "</a>");

    var tab_button_background = $("<li />", {"class": "nav-item"})
        .html("<a href='#control-sidebar-theme-background-tab' data-toggle='tab' class='nav-link'>"
        + "<i class='fa fa-image'></i>"
        + "</a>");

    //Add the tab buttons
    $("#tab-menu").prepend(tab_button_background);
    $("#tab-menu").prepend(tab_button_options);
    $("#tab-menu").prepend(tab_button_colors);

    //Create the menus
    var color_settings = $("<div />");
    var layout_settings = $("<div />");
    var background_settings = $("<div />");

    //Layout options
    layout_settings.append(
        "<h4 class='control-sidebar-heading'>"
        + "Layout Options (Fixed, Boxed, Rounded)"
        + "</h4>"
        + "<div class='btn-group' data-toggle='buttons'>"
            + "<label class='btn  btn-primary active' id='lo1'><input type='radio' name='options' id='o1' autocomplete='off' checked><b>F</b></label>"
            + "<label class='btn  btn-primary' id='lo2'>       <input type='radio' name='options' id='o2' autocomplete='off'><b>B</b></label>"
            + "<label class='btn  btn-primary' id='lo3'>       <input type='radio' name='options' id='o3' autocomplete='off'><b>R</b></label>"
        + "</div>"
        + "<h4 class='control-sidebar-heading'>"
        + "Mini sidebar"
        + "</h4>"
        + "<div class='btn-group' data-toggle='buttons'>"
        + "<label class='btn  btn-primary active' id='me'><input type='radio' name='options' id='o4' autocomplete='off' checked><b>Enabled</b></label>"
        + "<label class='btn  btn-primary' id='md'>       <input type='radio' name='options' id='o5' autocomplete='off'><b>Disabled</b></label>"
        + "</div>"
        + "<h4 class='control-sidebar-heading'>"
        + "Sidebar fixed or collapsed"
        + "</h4>"
        + "<div class='btn-group' data-toggle='buttons'>"
        + "<label class='btn  btn-primary active' id='sbf'><input type='radio' name='options' id='o4' autocomplete='off' checked><b>Fixed</b></label>"
        + "<label class='btn  btn-primary' id='sbc'>       <input type='radio' name='options' id='o5' autocomplete='off'><b>Collapsed</b></label>"
        + "</div>"
    );

    tab_options_pane.append(layout_settings);
    $("#tab-content").prepend(tab_options_pane);


    //header options
    var theme_list = $("<ul />", {"class": 'list-unstyled clearfix'});
    $.each( themes, function( key, value ) {
        var thistheme =
        $("<li />", {style: "float:left; width: 20%; padding: 5px;"})
            .append("<a href='javascript:void(0);' data-header='header-"+value+"' style='display: block; class='clearfix'>"
            + "<div><span style='display:block; width: 30px; float: left; height: 30px;'><button type='button' class='btn btn-circle btn-sm "+value+"' title='"+value+"'><i class='fa fa-check' style='color:white'></i></button></span></div>"
            + "</a>"
            + "<p class='text-center no-margin'></p>");
        theme_list.append(thistheme);
    });

    color_settings.append("<h4 class='control-sidebar-heading'>Header color</h4>");

    color_settings.append(theme_list);

    //sidebar options
    var theme_list = $("<ul />", {"class": 'list-unstyled clearfix'});
    $.each( themes, function( key, value ) {
        var thistheme =
            $("<li />", {style: "float:left; width: 20%; padding: 5px;"})
                .append("<a href='javascript:void(0);' data-sidebar='sidebar-"+value+"' style='display: block; class='clearfix'>"
                + "<div><span style='display:block; width: 30px; float: left; height: 30px;'><button type='button' class='btn btn-circle btn-sm "+value+"' title='"+value+"'><i class='fa fa-check' style='color:white'></i></button></span></div>"
                + "</a>"
                + "<p class='text-center no-margin'></p>");
        theme_list.append(thistheme);
    });

    color_settings.append("<h4 class='control-sidebar-heading'>Side bar color</h4>");

    color_settings.append(theme_list);

    tab_colors_pane.append(color_settings);
    $("#tab-content").prepend(tab_colors_pane);

    //background options
    var theme_list = $("<ul />", {"class": 'list-unstyled clearfix'});
    var i = 0;
    $.each( backgrounds, function( key, value ) {
        i += 1;
        var thistheme =
            $("<li />", {style: "float:left; width: 20%; padding: 5px;"})
                .append("<a href='javascript:void(0);' data-background='"+value+"' style='display: block; class='clearfix'>"
                + "<div><span style='display:block; width: 30px; float: left; height: 30px;'><button type='button' class='btn btn-circle btn-primary btn-sm' title='"+value+"'><b>"+i+"</b></button></span></div>"
                + "</a>"
                + "<p class='text-center no-margin'></p>");
        theme_list.append(thistheme);
    });

    background_settings.append("<h4 class='control-sidebar-heading'>Background options (best viewed on Boxed or Rounded layouts)</h4>");

    background_settings.append(theme_list);

    tab_background_pane.append(background_settings);
    $("#tab-content").prepend(tab_background_pane);

    setup();

    /**
    * @param String cls the layout class to toggle
    * @returns void
    */
    function change_layout(cls) {
        //clear old class before assign the new
        $("body").removeClass('boxed');
        $("body").removeClass('rounded');
        $("body").removeClass('fixed');

        $("body").toggleClass(cls);
        CheckUi.layout.fixSidebar();

        if ($('body').hasClass('fixed') && cls == 'fixed') {
            CheckUi.pushMenu.expandOnHover();
            CheckUi.layout.activate();
        }
        CheckUi.controlSidebar._fix($(".control-sidebar-bg"));
        CheckUi.controlSidebar._fix($(".control-sidebar"));

    }

    /**
     * @param String cls the layout class to toggle
     * @returns void
     */
    function sidebar_controller(cls) {

        //fixed, collapsed, mini-enabled, mini-disabled
        if(cls == 'fixed'){
            $("body").removeClass('sidebar-collapse');
        }
        if(cls == 'collapsed'){
            $("body").addClass('sidebar-collapse');
        }
        if(cls == 'mini-enabled'){
            $("body").addClass('mini-sidebar');
        }
        if(cls == 'mini-disabled'){
            $("body").removeClass('mini-sidebar');
        }
    }

    /**
    * Replaces the old header theme with the new header theme
    * @param String cls the new theme class
    * @returns Boolean false to prevent link's default action
    */
    function change_header(cls) {
        //console.log(cls);
        $("body").removeClass('undefined');
        $.each(themes, function (i) {
          $("body").removeClass('header-' + themes[i]);
        });

        $("body").addClass(cls);
        store('header-theme', cls);
        return false;
    }

    /**
    * @param String cls the new theme class
    * @returns Boolean false to prevent link's default action
    */
    function change_sidebar(cls) {

        $.each(themes, function (i) {
            $("body").removeClass('sidebar-' + themes[i]);
        });

        $("body").addClass(cls);
        store('sidebar-theme', cls);
        return false;
    }

    /**
     * @param String cls the new theme class
     * @returns Boolean false to prevent link's default action
     */
    function change_background(cls) {
        //console.log(cls);
        $("body").removeClass('undefined');
        $.each(backgrounds, function (i) {
            $("body").removeClass(backgrounds[i]);
        });

        $("body").addClass(cls);
        store('background-theme', cls);
        return false;
    }

    /**
    * Store a new settings in the browser
    * @param String name Name of the setting
    * @param String val Value of the setting
    * @returns void
    */
    function store(name, val) {
    if (typeof (Storage) !== "undefined") {
      localStorage.setItem(name, val);
    } else {
      window.alert('Please use a modern browser to properly view this template!');
    }
    }

    /**
    * Get a prestored setting
    *
    * @param String name Name of of the setting
    * @returns String The value of the setting | null
    */
    function get(name) {
    if (typeof (Storage) !== "undefined") {
      return localStorage.getItem(name);
    } else {
      window.alert('Please use a modern browser to properly view this template!');
    }
    }

    /**
    * Retrieve default settings and apply them to the template
    *
    * @returns void
    */
    function setup() {

        for (var i = 0; i < localStorage.length; i++){
            console.log(localStorage.getItem(localStorage.key(i)));
        }
        //localStorage.clear();

        var tmp = get('header-theme');
        if (tmp){
            change_header(tmp);
        }

        var tmp_sidebar = get('sidebar-theme');
        if (tmp_sidebar && $.inArray(tmp_sidebar, themes))
            change_sidebar(tmp_sidebar);

        var tmp = get('background-theme');
        if (tmp){
            change_background(tmp);
        }


        $("[data-header]").on('click', function (e) {
            e.preventDefault();
            change_header($(this).data('header'));
        });

        $("[data-sidebar]").on('click', function (e) {
            e.preventDefault();
            change_sidebar($(this).data('sidebar'));
        });

        $("[data-background]").on('click', function (e) {
            e.preventDefault();
            change_background($(this).data('background'));
        });

        $('#lo1').on("click",function(){
            change_layout('fixed');
        });

        $('#lo2').on("click",function(){
            change_layout('boxed');
        });

        $('#lo3').on("click",function(){
            change_layout('rounded');
        });

        //$('#lo4').on("click",function(){
        //    change_layout('top-nav');
        //});

        $('#sbf').on("click",function(){
            sidebar_controller('fixed');
        });

        $('#sbc').on("click",function(){
            sidebar_controller('collapsed');
        });

        $('#me').on("click",function(){
            sidebar_controller('mini-enabled');
        });

        $('#md').on("click",function(){
            sidebar_controller('mini-disabled');
        });

        //Add the layout manager
        //$("[data-layout]").on('change', function () {
        //  change_layout($(this).data('layout'));
        //});

        $("[data-controlsidebar]").on('click', function () {
          change_layout($(this).data('controlsidebar'));
          var slide = !CheckUi.options.controlSidebarOptions.slide;
          CheckUi.options.controlSidebarOptions.slide = slide;
          if (!slide)
            $('.control-sidebar').removeClass('control-sidebar-open');
        });


        $("[data-enable='expandOnHover']").on('click', function () {
          $(this).attr('disabled', true);
          CheckUi.pushMenu.expandOnHover();
          if (!$('body').hasClass('sidebar-collapse'))
            $("[data-layout='sidebar-collapse']").click();
        });



    }
})(jQuery, $.CheckUi);