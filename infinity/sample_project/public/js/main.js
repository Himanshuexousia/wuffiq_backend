//Make sure jQuery has been loaded before app.js
if (typeof jQuery === "undefined") {
    throw new Error("CheckUi requires jQuery");
}

$.CheckUi = {};

$.CheckUi.globals = {
    time_start:'1459176992',
    time_tracker: '',
    time_elapsed: ''
}

/* --------------------
 * - CheckUi Options -
 * --------------------
 */
$.CheckUi.options = {
    //Add slimscroll to navbar menus
    //This requires you to load the slimscroll plugin
    //in every page before app.js
    navbarMenuSlimscroll: true,
    navbarMenuSlimscrollWidth: "3px", //The width of the scroll bar
    navbarMenuHeight: "200px", //The height of the inner menu
    //General animation speed for JS animated elements such as card collapse/expand and
    //sidebar treeview slide up/down. This options accepts an integer as milliseconds,
    //'fast', 'normal', or 'slow'
    animationSpeed: 500,
    //Sidebar push menu toggle button selector
    sidebarToggleSelector: "[data-toggle='offcanvas']",
    //Fullscreen toggle button selector
    fullscreenSelector: "[data-toggle='fullscreen']",
    //timetracker button
    timetrackBtn: "[data-toggle='timetrack']",
    //Activate sidebar push menu
    sidebarPushMenu: true,
    //Activate fullscreen push menu
    fullscreenPushMenu: true,
    //Activate sidebar slimscroll if the fixed layout is set (requires SlimScroll Plugin)
    sidebarSlimScroll: true,
    //Enable sidebar expand on hover effect for sidebar mini
    //This option is forced to true if both the fixed layout and sidebar mini
    //are used together
    sidebarExpandOnHover: false,
    //cardRefresh Plugin
    enablecardRefresh: true,
    //Bootstrap.js tooltip
    enableBSToppltip: true,
    BSTooltipSelector: "[data-toggle='tooltip']",
    //Enable Fast Click. Fastclick.js creates a more
    //native touch experience with touch devices. If you
    //choose to enable the plugin, make sure you load the script
    //before CheckUi's app.js
    enableFastclick: true,
    //Control Sidebar Options
    enableControlSidebar: true,
    controlSidebarOptions: {
        //Which button should trigger the open/close event
        toggleBtnSelector: "[data-toggle='control-sidebar']",
        //The sidebar selector
        selector: ".control-sidebar",
        //Enable slide over content
        slide: true
    },

    //card Widget Plugin. Enable this plugin
    //to allow cards to be collapsed and/or removed
    enablecardWidget: true,
    //card Widget plugin options
    cardWidgetOptions: {
        cardWidgetIcons: {
            //Collapse icon
            collapse: 'fa-minus',
            //Open icon
            open: 'fa-plus',
            //Remove icon
            remove: 'fa-times'
        },
        cardWidgetSelectors: {
            //Remove button selector
            remove: '[data-widget="remove"]',
            //Collapse button selector
            collapse: '[data-widget="collapse"]'
        }
    },
    //Direct Chat plugin options
    directChat: {
        //Enable direct chat by default
        enable: true,
        //The button to open and close the chat contacts pane
        contactToggleSelector: '[data-widget="chat-pane-toggle"]'
    },
    //Define the set of colors to use globally around the website
    colors: {
        lightBlue: "#3c8dbc",
        red: "#f56954",
        green: "#00a65a",
        aqua: "#00c0ef",
        yellow: "#f39c12",
        blue: "#0073b7",
        navy: "#001F3F",
        teal: "#39CCCC",
        olive: "#3D9970",
        lime: "#01FF70",
        orange: "#FF851B",
        fuchsia: "#F012BE",
        purple: "#8E24AA",
        maroon: "#D81B60",
        black: "#222222",
        gray: "#d2d6de"
    },
    //The standard screen sizes that bootstrap uses.
    //If you change these in the variables.less file, change
    //them here too.
    screenSizes: {
        xs: 480,
        sm: 768,
        md: 992,
        lg: 1200
    }
};

/* ------------------
 * - Implementation -
 * ------------------
 * The next block of code implements CheckUi's
 * functions and plugins as specified by the
 * options above.
 */
$(function () {
    "use strict";

    //Fix for IE page transitions
    $("body").removeClass("hold-transition");

    //Extend options if external options exist
    if (typeof CheckUiOptions !== "undefined") {
        $.extend(true,
            $.CheckUi.options,
            CheckUiOptions);
    }

    //Easy access to options
    var o = $.CheckUi.options;

    //Set up the object
    _init();

    //Activate the layout maker
    $.CheckUi.layout.activate();

    //Enable sidebar tree view controls
    $.CheckUi.tree('.sidebar');

    //Enable control sidebar
    if (o.enableControlSidebar) {
        $.CheckUi.controlSidebar.activate();
    }

    //Add slimscroll to navbar dropdown
    if (o.navbarMenuSlimscroll && typeof $.fn.slimscroll != 'undefined') {
        $(".navbar .menu").slimscroll({
            height: o.navbarMenuHeight,
            alwaysVisible: false,
            size: o.navbarMenuSlimscrollWidth
        }).css("width", "100%");
    }

    //Activate sidebar push menu
    if (o.sidebarPushMenu) {
        $.CheckUi.pushMenu.activate(o.sidebarToggleSelector);
    }

    //Activate fullscreen
    if (o.fullscreenSelector) {
        $.CheckUi.fullScreen.activate(o.fullscreenSelector);
    }

    //start/stop time tracking
    if (o.timetrackBtn) {
        $.CheckUi.timeTracker.activate(o.timetrackBtn);
    }


    //Activate Bootstrap tooltip
    if (o.enableBSToppltip) {
        $('body').tooltip({
            selector: o.BSTooltipSelector
        });
    }

    //Activate card widget
    if (o.enablecardWidget) {
        $.CheckUi.cardWidget.activate();
    }

    //Activate fast click
    if (o.enableFastclick && typeof FastClick != 'undefined') {
        FastClick.attach(document.body);
    }

    //Activate direct chat widget
    if (o.directChat.enable) {
        $(document).on('click', o.directChat.contactToggleSelector, function () {
            var card = $(this).parents('.direct-chat').first();
            card.toggleClass('direct-chat-contacts-open');
        });
    }

    /*
     * INITIALIZE BUTTON TOGGLE
     * ------------------------
     */
    $('.btn-group[data-toggle="btn-toggle"]').each(function () {
        var group = $(this);
        $(this).find(".btn").on('click', function (e) {
            group.find(".btn.active").removeClass("active");
            $(this).addClass("active");
            e.preventDefault();
        });

    });
});

/* ----------------------------------
 * - Initialize the CheckUi Object -
 * ----------------------------------
 * All CheckUi functions are implemented below.
 */
function _init() {
    'use strict';

    /*Start time tracker on menu header*/
    $.fn.timeTracker('start');

    /* Layout
     * ======
     * Fixes the layout height in case min-height fails.
     *
     * @type Object
     * @usage $.CheckUi.layout.activate()
     *        $.CheckUi.layout.fix()
     *        $.CheckUi.layout.fixSidebar()
     */
    $.CheckUi.layout = {
        activate: function () {
            var _this = this;
            _this.fix();
            _this.fixSidebar();
            $(window, ".wrapper").resize(function () {
                _this.fix();
                _this.fixSidebar();
            });
        },
        fix: function () {
            //Get window height and the wrapper height
            var neg = $('.main-header').outerHeight() + $('.main-footer').outerHeight();
            var window_height = $(window).height();
            var sidebar_height = $(".sidebar").height();
            //Set the min-height of the content and sidebar based on the
            //the height of the document.
            if ($("body").hasClass("fixed")) {
                $(".content-wrapper, .right-side").css('min-height', window_height - $('.main-footer').outerHeight());
            } else {
                var postSetWidth;
                if (window_height >= sidebar_height) {
                    $(".content-wrapper, .right-side").css('min-height', window_height - neg);
                    postSetWidth = window_height - neg;
                } else {
                    $(".content-wrapper, .right-side").css('min-height', sidebar_height);
                    postSetWidth = sidebar_height;
                }

                //Fix for the control sidebar height
                var controlSidebar = $($.CheckUi.options.controlSidebarOptions.selector);
                if (typeof controlSidebar !== "undefined") {
                    if (controlSidebar.height() > postSetWidth)
                        $(".content-wrapper, .right-side").css('min-height', controlSidebar.height());
                }

            }
        },
        fixSidebar: function () {
            //Make sure the body tag has the .fixed class
            if (!$("body").hasClass("fixed")) {
                if (typeof $.fn.slimScroll != 'undefined') {
                    $(".sidebar").slimScroll({destroy: true}).height("auto");
                }
                return;
            } else if (typeof $.fn.slimScroll == 'undefined' && window.console) {
                window.console.error("Error: the fixed layout requires the slimscroll plugin!");
            }
            //Enable slimscroll for fixed layout
            if ($.CheckUi.options.sidebarSlimScroll) {
                if (typeof $.fn.slimScroll != 'undefined') {
                    //Destroy if it exists
                    $(".sidebar").slimScroll({destroy: true}).height("auto");
                    //Add slimscroll
                    $(".sidebar").slimscroll({
                        height: ($(window).height() - $(".main-header").height()) + "px",
                        color: "rgba(0,0,0,0.2)",
                        size: "3px"
                    });
                }
            }
        }
    };

    /* PushMenu()
     * ==========
     * Adds the push menu functionality to the sidebar.
     *
     * @type Function
     * @usage: $.CheckUi.pushMenu("[data-toggle='offcanvas']")
     */
    $.CheckUi.pushMenu = {
        activate: function (toggleBtn) {
            //Get the screen sizes
            var screenSizes = $.CheckUi.options.screenSizes;

            //Enable sidebar toggle
            $(document).on('click', toggleBtn, function (e) {
                e.preventDefault();

                //Enable sidebar push menu
                if ($(window).width() > (screenSizes.sm - 1)) {
                    if ($("body").hasClass('sidebar-collapse')) {
                        $("body").removeClass('sidebar-collapse').trigger('expanded.pushMenu');
                    } else {
                        $("body").addClass('sidebar-collapse').trigger('collapsed.pushMenu');
                    }
                }
                //Handle sidebar push menu for small screens
                else {
                    if ($("body").hasClass('sidebar-open')) {
                        $("body").removeClass('sidebar-open').removeClass('sidebar-collapse').trigger('collapsed.pushMenu');
                    } else {
                        $("body").addClass('sidebar-open').trigger('expanded.pushMenu');
                    }
                }
            });

            $(".content-wrapper").click(function () {
                //Enable hide menu when clicking on the content-wrapper on small screens
                if ($(window).width() <= (screenSizes.sm - 1) && $("body").hasClass("sidebar-open")) {
                    $("body").removeClass('sidebar-open');
                }
            });

            //Enable expand on hover for sidebar mini
            if ($.CheckUi.options.sidebarExpandOnHover
                || ($('body').hasClass('fixed')
                && $('body').hasClass('mini'))) {
                this.expandOnHover();
            }
        },
        expandOnHover: function () {
            var _this = this;
            var screenWidth = $.CheckUi.options.screenSizes.sm - 1;
            //Expand sidebar on hover
            $('.main-sidebar').hover(function () {
                if ($('body').hasClass('mini-sidebar')
                    && $("body").hasClass('sidebar-collapse')
                    && $(window).width() > screenWidth) {
                    _this.expand();
                }
            }, function () {
                if ($('body').hasClass('mini-sidebar')
                    && $('body').hasClass('sidebar-expanded-on-hover')
                    && $(window).width() > screenWidth) {
                    _this.collapse();
                }
            });
        },
        expand: function () {
            $("body").removeClass('sidebar-collapse').addClass('sidebar-expanded-on-hover');
        },
        collapse: function () {
            if ($('body').hasClass('sidebar-expanded-on-hover')) {
                $('body').removeClass('sidebar-expanded-on-hover').addClass('sidebar-collapse');
            }
        }
    };

    /* FullScreen()
     * ======
     * Expand screen to full if supported by browser
     *
     * @type Function
     *  @usage: $.CheckUi.fullScreen("[data-toggle='fullScreen']")
     */
    $.CheckUi.fullScreen = {
        activate: function (toggleBtn) {

            $(document).on('click', toggleBtn, function (e) {
                e.preventDefault();

                var selector = $('html');

                var ua = window.navigator.userAgent;
                var old_ie = ua.indexOf('MSIE ');
                var new_ie = ua.indexOf('Trident/');
                if ((old_ie > -1) || (new_ie > -1)) {
                    selector = $('body');
                }

                // check native support
                var screenCheck = $.fullscreen.isNativelySupported();

                if (screenCheck) {
                    if ($.fullscreen.isFullScreen()) {
                        $.fullscreen.exit();
                    }
                    else {
                        selector.fullscreen({
                            overflow: 'auto'
                        });
                    }
                } else {
                    alert('Your browser does not support fullscreen mode.')
                }
            });
        }
    };

    /* timeTrack()
     * ======
     * start/stops timetracking clock
     *
     * @type Function
     *  @usage: $.CheckUi.timeTrack("[data-toggle='timetrack']")
     */
    $.CheckUi.timeTracker = {
        activate: function (toggleBtn) {

            $(document).on('click', toggleBtn, function (e) {
                e.preventDefault();

                var btn = $("#timeTrackBtn");

                if (btn.hasClass('btn-success')){
                    btn.find('i').removeClass("fa-play");
                    btn.find('i').addClass("fa-stop");
                    btn.removeClass("btn-success");
                    btn.addClass("btn-danger");
                    //$("#currentTimetrack").html('');
                    $.fn.timeTracker('start');


                }else{
                    btn.find('i').removeClass("fa-stop");
                    btn.find('i').addClass("fa-play");
                    btn.removeClass("btn-danger");
                    btn.addClass("btn-success");
                    $.fn.timeTracker('stop');

                }

            });
        }
    };

    /* Tree()
     * ======
     * Converts the sidebar into a multilevel
     * tree view menu.
     *
     * @type Function
     * @Usage: $.CheckUi.tree('.sidebar')
     */
    $.CheckUi.tree = function (menu) {
        var _this = this;
        var animationSpeed = $.CheckUi.options.animationSpeed;
        $(menu).on('click', 'li a', function (e) {
            //Get the clicked link and the next element
            var $this = $(this);
            var checkElement = $this.next();

            //Check if the next element is a menu and is visible
            if ((checkElement.is('.treeview-menu')) && (checkElement.is(':visible')) && (!$('body').hasClass('sidebar-collapse'))) {
                //Close the menu
                checkElement.slideUp(animationSpeed, function () {
                    checkElement.removeClass('menu-open');
                    //Fix the layout in case the sidebar stretches over the height of the window
                    //_this.layout.fix();
                });
                checkElement.parent("li").removeClass("active");
            }
            //If the menu is not visible
            else if ((checkElement.is('.treeview-menu')) && (!checkElement.is(':visible'))) {
                //Get the parent menu
                var parent = $this.parents('ul').first();
                //Close all open menus within the parent
                var ul = parent.find('ul:visible').slideUp(animationSpeed);
                //Remove the menu-open class from the parent
                ul.removeClass('menu-open');
                //Get the parent li
                var parent_li = $this.parent("li");

                //Open the target menu and add the menu-open class
                checkElement.slideDown(animationSpeed, function () {
                    //Add the class active to the parent li
                    checkElement.addClass('menu-open');
                    parent.find('li.active').removeClass('active');
                    parent_li.addClass('active');
                    //Fix the layout in case the sidebar stretches over the height of the window
                    _this.layout.fix();
                });
            }
            //if this isn't a link, prevent the page from being redirected
            if (checkElement.is('.treeview-menu')) {
                e.preventDefault();
            }
        });
    };

    /* ControlSidebar
     * ==============
     * Adds functionality to the right sidebar
     *
     * @type Object
     * @usage $.CheckUi.controlSidebar.activate(options)
     */
    $.CheckUi.controlSidebar = {
        //instantiate the object
        activate: function () {
            //Get the object
            var _this = this;
            //Update options
            var o = $.CheckUi.options.controlSidebarOptions;
            //Get the sidebar
            var sidebar = $(o.selector);
            //The toggle button
            var btn = $(o.toggleBtnSelector);

            //Listen to the click event
            btn.on('click', function (e) {
                e.preventDefault();
                //If the sidebar is not open
                if (!sidebar.hasClass('control-sidebar-open')
                    && !$('body').hasClass('control-sidebar-open')) {
                    //Open the sidebar
                    _this.open(sidebar, o.slide);
                } else {
                    _this.close(sidebar, o.slide);
                }
            });

            //If the body has a carded layout, fix the sidebar bg position
            var bg = $(".control-sidebar-bg");
            _this._fix(bg);

            //If the body has a fixed layout, make the control sidebar fixed
            if ($('body').hasClass('fixed')) {
                _this._fixForFixed(sidebar);
            } else {
                //If the content height is less than the sidebar's height, force max height
                if ($('.content-wrapper, .right-side').height() < sidebar.height()) {
                    _this._fixForContent(sidebar);
                }
            }
        },
        //Open the control sidebar
        open: function (sidebar, slide) {
            //Slide over content
            if (slide) {
                sidebar.addClass('control-sidebar-open');
            } else {
                //Push the content by adding the open class to the body instead
                //of the sidebar itself
                $('body').addClass('control-sidebar-open');
            }
        },
        //Close the control sidebar
        close: function (sidebar, slide) {
            if (slide) {
                sidebar.removeClass('control-sidebar-open');
            } else {
                $('body').removeClass('control-sidebar-open');
            }
        },
        _fix: function (sidebar) {
            var _this = this;
            if ($("body").hasClass('boxed') || $("body").hasClass('rounded')) {
                sidebar.css('position', 'absolute');
                sidebar.height($(".wrapper").height());
                $(window).resize(function () {
                    _this._fix(sidebar);
                });
            } else {
                sidebar.css({
                    'position': 'fixed',
                    'height': 'auto'
                });
            }
        },
        _fixForFixed: function (sidebar) {
            sidebar.css({
                'position': 'fixed',
                'max-height': '100%',
                'overflow': 'auto',
                'padding-bottom': '50px'
            });
        },
        _fixForContent: function (sidebar) {
            $(".content-wrapper, .right-side").css('min-height', sidebar.height());
        }
    };

    /* cardWidget
     * =========
     * cardWidget is a plugin to handle collapsing and
     * removing cards from the screen.
     *
     * @type Object
     * @usage $.CheckUi.cardWidget.activate()
     *        Set all your options in the main $.CheckUi.options object
     */
    $.CheckUi.cardWidget = {
        selectors: $.CheckUi.options.cardWidgetOptions.cardWidgetSelectors,
        icons: $.CheckUi.options.cardWidgetOptions.cardWidgetIcons,
        animationSpeed: $.CheckUi.options.animationSpeed,
        activate: function (_card) {
            var _this = this;
            if (!_card) {
                _card = document; // activate all cards per default
            }
            //Listen for collapse event triggers
            $(_card).on('click', _this.selectors.collapse, function (e) {
                e.preventDefault();
                _this.collapse($(this));
            });

            //Listen for remove event triggers
            $(_card).on('click', _this.selectors.remove, function (e) {
                e.preventDefault();
                _this.remove($(this));
            });
        },
        collapse: function (element) {
            var _this = this;
            //Find the card parent
            var card = element.parents(".card").first();
            //Find the body and the footer
            var card_content = card.find("> .card-block, > .card-footer, > form  >.card-block, > form > .card-footer");
            if (!card.hasClass("collapsed-card")) {
                //Convert minus into plus
                element.children(":first")
                    .removeClass(_this.icons.collapse)
                    .addClass(_this.icons.open);
                //Hide the content
                card_content.slideUp(_this.animationSpeed, function () {
                    card.addClass("collapsed-card");
                });
            } else {
                //Convert plus into minus
                element.children(":first")
                    .removeClass(_this.icons.open)
                    .addClass(_this.icons.collapse);
                //Show the content
                card_content.slideDown(_this.animationSpeed, function () {
                    card.removeClass("collapsed-card");
                });
            }
        },
        remove: function (element) {
            //Find the card parent
            var card = element.parents(".card").first();
            card.slideUp(this.animationSpeed);
        }
    };
}

/* ------------------
 * - Custom Plugins -
 * ------------------
 * All custom plugins are defined below.
 */

/*
 * card REFRESH BUTTON
 * ------------------
 * This is a custom plugin to use with the component card. It allows you to add
 * a refresh button to the card. It converts the card's state to a loading state.
 *
 * @type plugin
 * @usage $("#card-widget").cardRefresh( options );
 */
(function ($) {

    "use strict";

    $.fn.cardRefresh = function (options) {

        // Render options
        var settings = $.extend({
            //Refresh button selector
            trigger: ".refresh-btn",
            //File source to be loaded (e.g: ajax/src.php)
            source: "",
            //Callbacks
            onLoadStart: function (card) {
                return card;
            }, //Right after the button has been clicked
            onLoadDone: function (card) {
                return card;
            } //When the source has been loaded

        }, options);

        //The overlay
        var overlay = $('<div class="overlay"><div class="fa fa-refresh fa-spin"></div></div>');

        return this.each(function () {
            //if a source is specified
            if (settings.source === "") {
                if (window.console) {
                    window.console.log("Please specify a source first - cardRefresh()");
                }
                return;
            }
            //the card
            var card = $(this);
            //the button
            var rBtn = card.find(settings.trigger).first();

            //On trigger click
            rBtn.on('click', function (e) {
                e.preventDefault();
                //Add loading overlay
                start(card);

                //Perform ajax call
                card.find(".card-body").load(settings.source, function () {
                    done(card);
                });
            });
        });

        function start(card) {
            //Add overlay and loading img
            card.append(overlay);

            settings.onLoadStart.call(card);
        }

        function done(card) {
            //Remove overlay and loading img
            card.find(overlay).remove();

            settings.onLoadDone.call(card);
        }

    };

})(jQuery);

/*
 * EXPLICIT card CONTROLS
 * -----------------------
 * This is a custom plugin to use with the component card. It allows you to activate
 * a card inserted in the DOM after the app.js was loaded, toggle and remove card.
 *
 * @type plugin
 * @usage $("#card-widget").activatecard();
 * @usage $("#card-widget").togglecard();
 * @usage $("#card-widget").removecard();
 */
(function ($) {

    'use strict';

    $.fn.activatecard = function () {
        $.CheckUi.cardWidget.activate(this);
    };

    $.fn.togglecard = function(){
        var button = $($.CheckUi.cardWidget.selectors.collapse, this);
        $.CheckUi.cardWidget.collapse(button);
    };

    $.fn.removecard = function(){
        var button = $($.CheckUi.cardWidget.selectors.remove, this);
        $.CheckUi.cardWidget.remove(button);
    };

})(jQuery);


/*
 * TODO LIST CUSTOM PLUGIN
 * -----------------------
 * This plugin depends on iCheck plugin for checkcard and radio inputs
 *
 * @type plugin
 * @usage $("#todo-widget").todolist( options );
 */
(function ($) {

    'use strict';

    $.fn.todolist = function (options) {
        // Render options
        var settings = $.extend({
            //When the user checks the input
            onCheck: function (ele) {
                return ele;
            },
            //When the user unchecks the input
            onUncheck: function (ele) {
                return ele;
            }
        }, options);

        return this.each(function () {

            if (typeof $.fn.iCheck != 'undefined') {
                $('input', this).on('ifChecked', function () {
                    var ele = $(this).parents("li").first();
                    ele.toggleClass("done");
                    settings.onCheck.call(ele);
                });

                $('input', this).on('ifUnchecked', function () {
                    var ele = $(this).parents("li").first();
                    ele.toggleClass("done");
                    settings.onUncheck.call(ele);
                });
            } else {
                $('input', this).on('change', function () {
                    var ele = $(this).parents("li").first();
                    ele.toggleClass("done");
                    if ($('input', ele).is(":checked")) {
                        settings.onCheck.call(ele);
                    } else {
                        settings.onUncheck.call(ele);
                    }
                });
            }
        });
    };
}(jQuery));

/*
 * initTimer
 */
(function ($) {

    'use strict';

    var tracker = $("#currentTimetrack");

    $.fn.timeTracker = function(myState){

        if(myState === 'start'){

            tracker.css("color", "green");

            $.CheckUi.globals.timer = setTimeout(initTimer, 1000); // and continue to run this every 1 sec

            if ($.CheckUi.globals.time_start === -1) return;

            $.CheckUi.globals.time_elapsed = Math.round(new Date().getTime() / 1000) - $.CheckUi.globals.time_start;

            var days=Math.floor($.CheckUi.globals.time_elapsed / 86400);
            var hours = Math.floor(($.CheckUi.globals.time_elapsed - (days * 86400 ))/3600)
            var minutes = Math.floor(($.CheckUi.globals.time_elapsed - (days * 86400 ) - (hours * 3600 ))/60)
            var secs = Math.floor(($.CheckUi.globals.time_elapsed - (days * 86400 ) - (hours * 3600 ) - (minutes * 60)))

            if (days === 0){
                days = '';
            }else if (days === 1){
                days = days + " day, ";
            }else{
                days = days + " days, ";
            }

            if (minutes === 0){
                minutes = '00';
            }else if (minutes < 10){
                minutes = '0' + minutes;
            }

            if (secs < 10){
                secs = '0' + secs;
            }
            secs = ':' + secs;

            if (hours === 0){
                hours = '';
            }else if (hours < 10){
                hours = '0' + hours + ':';
                secs = '';
            }else{
                hours = hours + ":";
                secs = '';
            }

            var total = days + hours + minutes + secs;

            $("#currentTimetrack").html('<i class="fa fa-clock-o"></i> ' + total);

        }else{
            tracker.css("color", "red");
            clearTimeout($.CheckUi.globals.timer);//stop the timer
        }


    };

}(jQuery));