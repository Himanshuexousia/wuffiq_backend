$(function () {

    $('#btnAddField').click(function () {
        var num     = $('.clonedRow').length, // Checks to see how many "duplicatable" input fields we currently have
            newNum  = new Number(num + 1),   // The numeric ID of the new input field being added, increasing by 1 each time
            newElem;
        //console.log(newNum);

        newElem = $('#fieldRow' + num).clone().attr('id', 'fieldRow' + newNum).insertAfter('#fieldRow' + num).fadeIn('slow');

        //erase field values
        //$('#fieldRow' + newNum + '> div > div > input[name="field_name[]"]').val('');
        newElem.find('.input_ln').val('');
        newElem.find('._checkbox').prop( "checked", false );

        // Enable the "remove" button. This only shows once you have a duplicated section.
        $('#btnRemoveField').attr('disabled', false);
    });

    $('#btnRemoveField').click(function () {
        // Confirmation dialog box. Works on all desktop browsers and iPhone.
        if (confirm("Are you sure you wish to remove this field? This cannot be undone."))
        {
            var num = $('.clonedRow').length;
            // how many "duplicatable" input fields we currently have
            $('#fieldRow' + num).slideUp('slow', function () {$(this).remove();
                // if only one element remains, disable the "remove" button
                if (num -1 === 1)
                    $('#btnRemoveField').attr('disabled', true);
                // enable the "add" button. IMPORTANT: only for forms using input type="button" (see older demo). DELETE if using button element.
                $('#btnAddField').attr('disabled', false).prop('value', "Add field");
                // enable the "add" button. IMPORTANT: only for forms using the new button tag (see Bootstrap demo). DELETE if using input type="button" element.
                $('#btnAddField').attr('disabled', false).text("Add field");});
        }
        return false; // Removes the last section you added
    });
    // Enable the "add" button
    $('#btnAddField').attr('disabled', false);
    // Disable the "remove" button
    $('#btnRemoveField').attr('disabled', true);

});