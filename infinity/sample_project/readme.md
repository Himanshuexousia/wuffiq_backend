# Infinity

Administrative Theme built on Laravel using Bootstrap packed with ready to use functionalities that allows you to develop professional web applications in a much faster and structured whey.
 
The theme also comes bundled with CheckAuth,a complete roles authentication system.

Laravel is one of the best PHP MVC framework in the world and Bootstrap is the leading html/Jquery framework. 

## Site

http://infinity.checkmatedigital.com


## Licence

� CheckMateDigital << http://checkmatedigital.com >> 2015-2016, all rights reserved. Read about the license in LICENSE file located at the root of the application.