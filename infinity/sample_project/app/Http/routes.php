<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {

    Route::get('/', [ 'as' => '/', function () {
        return view('infinity');
    }]);
    Route::get('app4', [ 'as' => 'app4', function () {
        return view('front-end');
    }]);
    Route::get('front', [ 'as' => 'front', function () {
        return view('front-end');
    }]);
    Route::get('responsive', [ 'as' => 'responsive', function () {
        return view('responsive');
    }]);
    Route::get('documentation', [ 'as' => 'documentation', function () {
        return view('documentation.infinity');
    }]);
    Route::get('back-end', [ 'as' => 'back-end', function () {
        return view('dashboard-social');
    }]);
    Route::get('home', [ 'as' => 'home', function () {
        return view('dashboard-social');
    }]);


    Route::get('auth/sample', [ 'as' => 'auth-sample', function () {
        return view('auth-sample');
    }]);

    // Dashboards
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('business', [ 'as' => 'dashboard-business', function () {
            return view('dashboard-business');
        }]);
        Route::get('sales', [ 'as' => 'dashboard-sales', function () {
            return view('dashboard-sales');
        }]);
        Route::get('marketing', [ 'as' => 'dashboard-marketing', function () {
            return view('dashboard-marketing');
        }]);
        Route::get('social', [ 'as' => 'dashboard-social', function () {
            return view('dashboard-social');
        }]);
        Route::get('projects', [ 'as' => 'dashboard-projects', function () {
            return view('dashboard-projects');
        }]);

    });

    // Forms
    Route::group(['prefix' => 'form'], function () {
        Route::get('general', [ 'as' => 'form-general', function () {
            return view('form-general');
        }]);
        Route::get('advanced', [ 'as' => 'form-advanced', function () {
            return view('form-advanced');
        }]);
        Route::get('editor', [ 'as' => 'form-editor', function () {
            return view('form-editor');
        }]);
    });

    // Mailbox
    Route::group(['prefix' => 'mailbox'], function () {
        Route::get('inbox', [ 'as' => 'mail-inbox', function () {
            return view('mail-inbox');
        }]);
        Route::get('message', [ 'as' => 'mail-message', function () {
            return view('mail-message');
        }]);
        Route::get('contacts', [ 'as' => 'contacts-list', function () {
            return view('contacts-list');
        }]);
    });

    // Calendar
    Route::get('calendar', [ 'as' => 'calendar', function () {
        return view('calendar');
    }]);

    // Tables
    Route::group(['prefix' => 'table'], function () {
        Route::get('simple', [ 'as' => 'table-simple', function () {
            return view('table-simple');
        }]);
        Route::get('data', [ 'as' => 'table-data', function () {
            return view('table-data');
        }]);
    });

    // Charts
    Route::group(['prefix' => 'charts'], function () {
        Route::get('google-chart-tools', [ 'as' => 'google-chart-tools', function () {
            return view('google-chart-tools');
        }]);
        Route::get('chart-js', [ 'as' => 'chart-js', function () {
            return view('chart-js');
        }]);
        Route::get('peity', [ 'as' => 'peity', function () {
            return view('peity');
        }]);
    });
    // Ui Elements
    Route::group(['prefix' => 'ui-elements'], function () {
        Route::get('general', [ 'as' => 'general', function () {
            return view('ui-general');
        }]);
        Route::get('grid', [ 'as' => 'grid', function () {
            return view('grid');
        }]);
        Route::get('icons', [ 'as' => 'icons', function () {
            return view('icons');
        }]);
        Route::get('buttons', [ 'as' => 'buttons', function () {
            return view('buttons');
        }]);
        Route::get('sliders', [ 'as' => 'sliders', function () {
            return view('sliders');
        }]);
        Route::get('timeline', [ 'as' => 'timeline', function () {
            return view('timeline');
        }]);
        Route::get('modal', [ 'as' => 'modal', function () {
            return view('modal');
        }]);
        Route::get('cards', [ 'as' => 'cards', function () {
            return view('cards');
        }]);
        Route::get('tabs', [ 'as' => 'tabs', function () {
            return view('tabs');
        }]);
        Route::get('bootstrap-misc', [ 'as' => 'bootstrap-misc', function () {
            return view('bootstrap-misc');
        }]);
        Route::get('alerts', [ 'as' => 'alerts', function () {
            return view('alerts');
        }]);
    });

    // Plugins
    Route::group(['prefix' => 'plugins'], function () {
        Route::get('toastr', [ 'as' => 'plu-toastr', function () {
            return view('plu-toastr');
        }]);
        Route::get('pace', [ 'as' => 'plu-pace', function () {
            return view('plu-pace');
        }]);
    });

    // Pages
    Route::group(['prefix' => 'pages'], function () {
        Route::get('invoice', [ 'as' => 'page-invoice', function () {
            return view('invoice');
        }]);
        Route::get('profile', [ 'as' => 'page-user-profile', function () {
            return view('user-profile');
        }]);
        Route::get('login', [ 'as' => 'page-login', function () {
            return view('login-page');
        }]);
        Route::get('lockscreen', [ 'as' => 'page-lockscreen', function () {
            return view('lockscreen');
        }]);
        Route::get('login2', [ 'as' => 'page-login2', function () {
            return view('login-page-2');
        }]);
        Route::get('lockscreen2', [ 'as' => 'page-lockscreen2', function () {
            return view('lockscreen-2');
        }]);
        Route::get('404', [ 'as' => 'page-404', function () {
            return view('errors.404');
        }]);
        Route::get('500', [ 'as' => 'page-500', function () {
            return view('errors.500');
        }]);
        Route::get('blank', [ 'as' => 'page-blank', function () {
            return view('blank-page');
        }]);
    });

});

//every route in here will be accessed by logged users only.
Route::group(['middleware' => ['web', 'sentinel.auth']], function () {


});

