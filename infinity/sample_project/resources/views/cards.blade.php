@extends('app')

@section('page-title')
    Cards
@endsection

@section('page-css')

@endsection
@section('content-header')
    <h1>
        Cards
    </h1>
@endsection

@section('content')

    <div class="card bg-none">
        <div class="card-header">
            Bootstrap
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-4">
                    <h5 class="text-md-center">Basic panel example</h5>
                    <div class="card">
                        <div class="card-block">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text">Card Body Content.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <h5 class="text-md-center">Basic panel with header example</h5>
                    <div class="card">
                        <div class="card-header">
                            Header
                        </div>
                        <div class="card-block">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text">Card Body Content.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <h5 class="text-md-center">Basic panel with footer example</h5>
                    <div class="card">
                        <div class="card-block">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text">Card Body Content.</p>
                        </div>
                        <div class="card-footer">
                            Footer
                        </div>
                    </div>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-md-4">
                    <h5 class="text-md-center">Image Cap example</h5>
                    <div class="card">
                        <img class="card-img-top" data-src="https://placehold.it/307x180/6fe263/ffffff?text=Checkmate+Digital" style="height: 180px; width: 100%; display: block;" src="https://placehold.it/307x180/970202/ffffff?text=Checkmate+Digital" alt="Card image cap">
                        <div class="card-block">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text">Card Body Content.</p>
                            <a href="#" class="btn btn-block btn-danger">Button</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <h5 class="text-md-center">Header and Footer example</h5>
                    <div class="card">
                        <div class="card-header text-xs-center">
                            Advanced
                        </div>
                        <div class="card-block">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text">Card Body Content.</p>
                            <a href="#" class="btn btn-block btn-danger">Button</a>
                        </div>
                        <div class="card-block">
                            <a href="#" class="card-link">Card link</a>
                            <a href="#" class="card-link">Another link</a>
                        </div>
                        <div class="card-footer text-xs-center">
                            Developed by Checkmate Digital
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <h5 class="text-md-center">List example</h5>
                    <div class="card">
                        <div class="card-block">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text">Card Body Content.</p>
                            <a href="#" class="btn btn-block btn-danger">Button</a>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">List Item One</li>
                            <li class="list-group-item">List Item Two</li>
                            <li class="list-group-item">List Item Three</li>
                        </ul>
                    </div>
                </div>
            </div>
            <br />
            <hr />
            <br />
            <h5 class="text-md-center">Colored Cards</h5>
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-primary text-xs-center">
                        <div class="card-block">
                            <div class="card-block">
                                <h4 class="card-title">Card Primary</h4>
                                <p class="card-text">Card Body Content.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <code class="text-md-center">
                        .card .card-primary
                    </code>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-success text-xs-center">
                        <div class="card-block">
                            <div class="card-block">
                                <h4 class="card-title">Card Success</h4>
                                <p class="card-text">Card Body Content.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <code class="text-md-center">
                        .card .card-success
                    </code>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-info text-xs-center">
                        <div class="card-block">
                            <div class="card-block">
                                <h4 class="card-title">Card Info</h4>
                                <p class="card-text">Card Body Content.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <code class="text-md-center">
                        .card .card-info
                    </code>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-warning text-xs-center">
                        <div class="card-block">
                            <div class="card-block">
                                <h4 class="card-title">Card Warning</h4>
                                <p class="card-text">Card Body Content.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <code class="text-md-center">
                        .card .card-warning
                    </code>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-danger text-xs-center">
                        <div class="card-block">
                            <div class="card-block">
                                <h4 class="card-title">Card Danger</h4>
                                <p class="card-text">Card Body Content.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <code class="text-md-center">
                        .card .card-danger
                    </code>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-default text-xs-center">
                        <div class="card-block">
                            <div class="card-block">
                                <h4 class="card-title">Card Default (light-gray)</h4>
                                <p class="card-text">Card Body Content.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <code class="text-md-center">
                        .card .card-default
                    </code>
                </div>
            </div>
            <br />
            <hr />
            <br />
            <h5 class="text-md-center">Group Cards</h5>
            <div class="row">
                <div class="col-md-12">
                    <div class="card-group">
                        <div class="card">
                            <div class="card-header text-xs-center">
                                Header
                            </div>
                            <div class="card-block">
                                <h4 class="card-title">Card title</h4>
                                <p class="card-text">Aenean commodo ligula eget dolor. Duis vel nibh at velit scelerisque suscipit. </p>
                            </div>
                            <div class="card-block">
                                <a href="#" class="card-link">Card link</a>
                                <a href="#" class="card-link">Another link</a>
                            </div>
                            <div class="card-footer text-xs-center">
                                Footer
                            </div>
                        </div>
                        <div class="card">
                            <img class="card-img-top" data-src="https://placehold.it/307x180/6fe263/ffffff?text=Checkmate+Digital" style="height: 180px; width: 100%; display: block;" src="https://placehold.it/307x180/970202/ffffff?text=Checkmate+Digital" alt="Card image cap">
                            <div class="card-block">
                                <h4 class="card-title">Card title</h4>
                                <p class="card-text">Card Body Content.</p>
                            </div>
                        </div>

                        <div class="card card-warning text-xs-center">
                            <div class="card-block">
                                <div class="card-block">
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text">Enim ut auctor augue. Parturient, tincidunt platea tortor tincidunt, vel duis lectus porta tincidunt tempor natoque scelerisque nunc porta, dolor parturient! Montes cum nunc, urna est integer, elit lacus sociis nascetur vel porta? Ridiculus phasellus dignissim? In aliquet turpis pulvinar massa sagittis nunc rhoncus, habitasse elementum non nunc in.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <hr />
            <br />
            <h5 class="text-md-center">Columns - Masonry-like</h5>
            <div class="row">
                <div class="col-md-12">
                    <div class="card-columns">
                        <div class="card">
                            <img class="card-img-top" data-src="https://placehold.it/307x180/6fe263/ffffff?text=Checkmate+Digital" style="height: 180px; width: 100%; display: block;" src="https://placehold.it/307x180/c3c3c3/000000?text=Checkmate+Digital" alt="Card image cap">
                            <div class="card-block">
                                <h4 class="card-title">Card title that wraps to a new line</h4>
                                <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                            </div>
                        </div>
                        <div class="card card-warning card-block">
                            <blockquote class="card-blockquote">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                <footer>
                                    <small class="text-muted">
                                        Someone in <cite title="Checkmate Digital">Checkmate Digital</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                        <div class="card card-block card-inverse navy text-xs-center">
                            <blockquote class="card-blockquote">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat.</p>
                                <footer>
                                    <small>
                                        Someone in <cite title="Checkmate Digital">Checkmate Digital</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                        <div class="card card-danger">
                            <img class="card-img-top" data-src="https://placehold.it/307x180/6fe263/ffffff?text=Checkmate+Digital" style="height: 180px; width: 100%; display: block;" src="https://placehold.it/307x180/c3c3c3/000000?text=Checkmate+Digital" alt="Card image cap">
                            <div class="card-block">
                                <h4 class="card-title">Card title</h4>
                                <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                        <div class="card card-block card-primary text-xs-center">
                            <blockquote class="card-blockquote">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat.</p>
                                <footer>
                                    <small>
                                        Someone in <cite title="Checkmate Digital">Checkmate Digital</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                        <div class="card card-block text-xs-center">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                        <div class="card">
                            <img class="card-img-top" data-src="https://placehold.it/307x180/6fe263/ffffff?text=Checkmate+Digital" style="height: 180px; width: 100%; display: block;" src="https://placehold.it/307x180/c3c3c3/000000?text=Checkmate+Digital" alt="Card image cap">
                        </div>
                        <div class="card card-block text-xs-right">
                            <blockquote class="card-blockquote">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                <footer>
                                    <small class="text-muted">
                                        Someone in <cite title="Checkmate Digital">Checkmate Digital</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                        <div class="card card-block">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>

                        <div class="card card-block card-inverse card-success purple text-xs-center">
                            <blockquote class="card-blockquote">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat.</p>
                                <footer>
                                    <small>
                                        Someone in <cite title="Checkmate Digital">Checkmate Digital</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card bg-none">
        <div class="card-header">
            CheckUI
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <h5 class="text-md-center">Card Colored</h5>
            <div class="row">
                <div class="col-md-3">
                    <div class="card card-block card-inverse light-blue text-xs-center">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Card Body Content.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-block card-inverse aqua text-xs-center">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Card Body Content.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-block card-inverse yellow text-xs-center">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Card Body Content.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-block card-inverse blue text-xs-center">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Card Body Content.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="card card-block card-inverse navy text-xs-center">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Card Body Content.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-block card-inverse teal text-xs-center">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Card Body Content.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-block card-inverse olive text-xs-center">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Card Body Content.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-block card-inverse lime text-xs-center">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Card Body Content.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="card card-block card-inverse orange text-xs-center">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Card Body Content.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-block card-inverse fuchsia text-xs-center">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Card Body Content.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-block card-inverse maroon text-xs-center">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Card Body Content.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-block card-inverse black text-xs-center">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Card Body Content.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="card card-block card-inverse red text-xs-center">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Card Body Content.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-block card-inverse purple text-xs-center">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Card Body Content.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-block card-inverse green text-xs-center">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Card Body Content.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-block card-inverse pink text-xs-center">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Card Body Content.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-xs-center">
                    Card with constant height
                </div>
                <div class="card-block">
                    <div id="scroll-250">
                        <p>Donec rutrum congue leo eget malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tincidunt. Curabitur aliquet quam id dui posuere blandit. Curabitur aliquet quam id dui posuere blandit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada.</p>

                        <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Nulla quis lorem ut libero malesuada feugiat. Curabitur aliquet quam id dui posuere blandit. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla porttitor accumsan tincidunt. Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam id dui posuere blandit.</p>

                        <p>Sed porttitor lectus nibh. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Donec rutrum congue leo eget malesuada. Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Cras ultricies ligula sed magna dictum porta. Proin eget tortor risus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Quisque velit nisi, pretium ut lacinia in, elementum id enim.</p>

                        <p>Donec rutrum congue leo eget malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tincidunt. Curabitur aliquet quam id dui posuere blandit. Curabitur aliquet quam id dui posuere blandit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada.</p>

                        <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Nulla quis lorem ut libero malesuada feugiat. Curabitur aliquet quam id dui posuere blandit. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla porttitor accumsan tincidunt. Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam id dui posuere blandit.</p>

                        <p>Sed porttitor lectus nibh. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Donec rutrum congue leo eget malesuada. Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Cras ultricies ligula sed magna dictum porta. Proin eget tortor risus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Quisque velit nisi, pretium ut lacinia in, elementum id enim.</p>
                    </div>
                </div>
                <div class="card-footer text-xs-center">
                    Card Footer
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-default">
                <div class="box-body">
                    <code>
                        ... <br />
                        &lt;div id=&quot;scroll-250&quot;&gt; <br />
                        &lt;p&gt;...&lt;/p&gt; <br />
                        &lt;/div&gt; <br />
                        ... <br /> <br />
                        &lt;script&gt; <br />
                        $(function(){ <br />
                        $(&#39;#scroll-250&#39;).slimScroll({ <br />
                        height: &#39;250px&#39; <br />
                        }); <br />
                        }); <br />
                        &lt;/script&gt;
                    </code>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script>
        $(function(){
            $('#scroll-250').slimScroll({
                height: '250px'
            });
        });
    </script>
@endsection