@extends('app')

@section('page-title')
    Invoice
@endsection

@section('page-css')
    <link rel="stylesheet" type="text/css" media="print" href="print.css">
@endsection

@section('content-header')
    <h1>
        Invoice
    </h1>
@endsection

@section('content')

    <div class="card invoice-print">
        <div class="card-header">
            <span class="glyphicon glyphicon-print"></span> Printable Invoice</span>
            <div class="pull-sm-right">
                <button type="button" class="btn btn-xs btn-default btn-gradient mr5">
                    <i class="fa fa-plus-square pr5"></i> New Invoice</button>
                <a href="javascript:PrintElem('.invoice-print')" class="btn btn-xs btn-default btn-gradient mr5">
                    <i class="fa fa-print fs13"></i>
                </a>
            </div>
        </div>
        <div class="card-block">
            <div class="row mb30">
                <div class="col-md-4 text-sm-center mt10">
                    <div>
                        <h1 id="invoce-num"> INVOICE #58126332</h1>
                        <h5 class="lead"> Created: Nov 23 2013 </h5>
                        <h5 class="lead"> Status:
                            <b class="text-success">Paid - On Time</b>
                        </h5>
                    </div>
                </div>
                <div class="col-md-4 text-sm-center mt10">
                    <h2 class="logo-name">
                        <a href="{{route('home')}}" class="logo">
                            Infinity
                            <span class="symbol">&infin;</span>
                        </a>
                    </h2>
                </div>
                <div class="col-md-4 text-sm-center mt10">
                    <div>
                        <h5 class="lead"> Sales Rep: <br />
                            <b class="text-primary">Michael Ronny</b>
                        </h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-info">
                        <div class="card-header text-xs-center">
                            Bill To
                        </div>
                        <div class="card-block">
                            <address>
                                <strong>Cannon Camera</strong>
                                <br> 151 Sandy Ave, Suite 200
                                <br> San Jose, CA 91503
                                <br>
                                <abbr title="Phone">P:</abbr> (123) 456-7890
                            </address>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-info">
                        <div class="card-header text-xs-center">
                            Ship To
                        </div>
                        <div class="card-block">
                            <address>
                                <strong>Amazon, Inc.</strong>
                                <br> 795 Folsom Ave, Suite 600
                                <br> San Francisco, CA 94107
                                <br>
                                <abbr title="Phone">P:</abbr> (123) 456-7890
                            </address>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-info">
                        <div class="card-header text-xs-center">
                            Invoice Details
                        </div>
                        <div class="card-block">
                            <ul class="list-unstyled">
                                <li>
                                    <b>Invoice #:</b> 58126332</li>
                                <li>
                                    <b>Invoice Date:</b> 10 Oct 2013</li>
                                <li>
                                    <b>Due Date:</b> 21 Dec 2013</li>
                                <li>
                                    <b>Terms:</b> Ten Forty</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-block">
                            <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Item</th>
                            <th>Description</th>
                            <th style="width: 135px;">#</th>
                            <th>Rate</th>
                            <th class="text-right pr10">Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <b>3</b>
                            </td>
                            <td>Net Code Revamp</td>
                            <td>Worked on Design and Structure (per hour)</td>
                            <td>16</td>
                            <td>$35.00</td>
                            <td class="text-right pr10">$560.00</td>
                        </tr>
                        <tr>
                            <td>
                                <b>1</b>
                            </td>
                            <td>Developer Newsletter </td>
                            <td>Year Subscription X2</td>
                            <td>2</td>
                            <td>$12.99</td>
                            <td class="text-right pr10">$25.98</td>
                        </tr>
                        <tr>
                            <td>
                                <b>3</b>
                            </td>
                            <td>Admin Dashboard</td>
                            <td>Design and implemention(per hour)</td>
                            <td>16</td>
                            <td>$35.00</td>
                            <td class="text-right pr10">$560.00</td>
                        </tr>
                        <tr>
                            <td>
                                <b>3</b>
                            </td>
                            <td>Web Development</td>
                            <td>Worked on Design and Structure (per hour)</td>
                            <td>23</td>
                            <td>$30.00</td>
                            <td class="text-right pr10">$690.00</td>
                        </tr>
                        <tr>
                            <td>
                                <b>1</b>
                            </td>
                            <td>Developer Newsletter </td>
                            <td>Year Subscription X2</td>
                            <td>2</td>
                            <td>$12.99</td>
                            <td class="text-right pr10">$25.98</td>
                        </tr>
                        </tbody>
                    </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="invoice-footer">
                <div class="col-sm-6">
                        <p class="lead">Payment Methods:</p>
                        <img src="{{asset('img/credit/visa.png')}}" alt="Visa">
                        <img src="{{asset('img/credit/mastercard.png')}}" alt="Mastercard">
                        <img src="{{asset('img/credit/american-express.png')}}" alt="American Express">
                        <img src="{{asset('img/credit/paypal2.png')}}" alt="Paypal">

                        <p class="text-muted">Sed, tempor penatibus habitasse, non. Magna pellentesque, urna parturient sed lundium pulvinar et ultricies elementum, etiam egestas aliquam, scelerisque. Aliquam ac in ultrices, tristique, et, vel? Pellentesque nec non, lacus lectus dignissim phasellus tincidunt cursus non nascetur lundium augue porta auctor porttitor, augue? Cras dolor eros duis est aliquet.</p>
                </div>
                <div class="col-sm-6">
                        <table class="table" id="invoice-summary">
                            <thead>
                            <tr>
                                <th>
                                    <b>Sub Total:</b>
                                </th>
                                <th>$1375.98</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <b>Payments</b>
                                </td>
                                <td>(-)0.00</td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Total</b>
                                </td>
                                <td>$230.00</td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Balance Due:</b>
                                </td>
                                <td>$1375.98</td>
                            </tr>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
        <div class="card-footer text-xs-center">
            <div class="pull-sm-left">
                <a href="javascript:PrintElem('.invoice-print')" class="btn btn-default mr10">
                    <i class="fa fa-print pr5"></i> Print Invoice</a>
            </div>
            <div class="pull-sm-right">
                <button class="btn btn-primary btn-gradient" type="button">
                    <i class="fa fa-floppy-o pr5"></i> Save Invoice</button>
            </div>
        </div>
    </div>

@endsection

@section('page-scripts')
    <script src="{{asset('js/printThis.js')}}"></script>
    <script type="text/javascript">

        function PrintElem(elem)
        {
            $(elem).printThis();
        }

    </script>
@endsection