@extends('app')

@section('page-title')
    Projects Dashboard
@endsection

@section('page-css')
    <style>
        .chart-legend ul{
            list-style-type: none;
        }
        .chart-legend ul li { display: inline; }
    </style>

@endsection

@section('content-header')
    <h1>
        Projects Dashboard
    </h1>
@endsection

@section('content')

    <div class="row">
        <div class="col-xl-3 col-lg-6">
            <div class="card-info bg-red">
                <span class="card-info-icon"><i class="ion-ios-flame-outline"></i></span>

                <div class="card-info-content">
                    <span class="card-info-text">Project deadlines</span>
                    <span class="card-info-number">4 delays</span>
                    <span class="progress-description">10 hours behind</span>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6">
            <div class="card-info bg-aqua">
                <span class="card-info-icon"><i class="ion-ios-calendar-outline"></i></span>

                <div class="card-info-content">
                    <span class="card-info-text">Calendar reminder</span>
                    <span class="card-info-number">Client meeting</span>
                    <span class="progress-description">11:30 Grand plaza</span>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6">
            <div class="card-info bg-yellow">
                <span class="card-info-icon"><i class="ion-ios-email-outline"></i></span>

                <div class="card-info-content">
                    <span class="card-info-text">Incomming message</span>
                    <span class="card-info-number">Logo aproved</span>
                    <span class="progress-description">From: Helen Kubrick</span>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6">
            <div class="card-info bg-olive">
                <span class="card-info-icon"><i class="ion-ios-clock-outline"></i></span>

                <div class="card-info-content">
                    <span class="card-info-text">Cronometer</span>
                    <span class="card-info-number">3:14h on task</span>
                    <span class="progress-description">Website interface redesign</span>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <section class="hidden-sm-down col-xl-8 col-lg-12 connectedSortable">
            <div class="card" id="divChart">
                <div class="card-header">
                    <i class="fa fa-clock-o"></i>
                    <h3 class="card-title">Gantt chart</h3>
                    <div class="card-tools pull-right">
                        <div class="btn-group" data-toggle="btn-toggle">
                            <button type="button" class="btn btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-block nav-tabs-custom">
                    <div id="chart"></div>
                </div>
                <div class="card-footer clearfix no-border">
                    <button type="button" class="btn btn-secondary pull-right"><i class="fa fa-plus"></i> Add task</button>
                </div>
            </div>
        </section>
        <section class="col-xl-4 col-lg-12 connectedSortable">
            <!-- TO DO List -->
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-check-circle-o"></i>

                    <h3 class="card-title">My Tasks</h3>

                    <div class="card-tools pull-right">
                        <div class="btn-group" data-toggle="btn-toggle">
                            <button type="button" class="btn btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-block">
                    <ul class="todo-list">
                        <li>
                            <span class="handle">
                            <i class="fa fa-ellipsis-v"></i>
                            <i class="fa fa-ellipsis-v"></i>
                            </span>
                            <input type="checkbox" value="" name="">
                            <span class="text">Design interface</span>
                            <small class="hidden-sm-down label-pill label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
                            <div class="tools">
                                <i class="fa fa-edit"></i>
                                <i class="fa fa-trash-o"></i>
                            </div>
                        </li>
                        <li>
                            <span class="handle">
                            <i class="fa fa-ellipsis-v"></i>
                            <i class="fa fa-ellipsis-v"></i>
                            </span>
                            <input type="checkbox" value="" name="">
                            <span class="text">Develop theme code</span>
                            <small class="hidden-sm-down label-pill label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
                            <div class="tools">
                                <i class="fa fa-edit"></i>
                                <i class="fa fa-trash-o"></i>
                            </div>
                        </li>
                        <li>
                            <span class="handle">
                                <i class="fa fa-ellipsis-v"></i>
                                <i class="fa fa-ellipsis-v"></i>
                            </span>
                            <input type="checkbox" value="" name="">
                            <span class="text">Check responsiveness</span>
                            <small class="hidden-sm-down label-pill label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
                            <div class="tools">
                                <i class="fa fa-edit"></i>
                                <i class="fa fa-trash-o"></i>
                            </div>
                        </li>
                        <li>
                            <span class="handle">
                                <i class="fa fa-ellipsis-v"></i>
                                <i class="fa fa-ellipsis-v"></i>
                            </span>
                            <input type="checkbox" value="" name="">
                            <span class="text">Adjust color pallet</span>
                            <small class="hidden-sm-down label-pill label-success"><i class="fa fa-clock-o"></i> 3 days</small>
                            <div class="tools">
                                <i class="fa fa-edit"></i>
                                <i class="fa fa-trash-o"></i>
                            </div>
                        </li>
                        <li>
                            <span class="handle">
                                <i class="fa fa-ellipsis-v"></i>
                                <i class="fa fa-ellipsis-v"></i>
                            </span>
                            <input type="checkbox" value="" name="">
                            <span class="text">Compile Sass files</span>
                            <small class="hidden-sm-down label-pill label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
                            <div class="tools">
                                <i class="fa fa-edit"></i>
                                <i class="fa fa-trash-o"></i>
                            </div>
                        </li>
                        <li>
                              <span class="handle">
                                <i class="fa fa-ellipsis-v"></i>
                                <i class="fa fa-ellipsis-v"></i>
                              </span>
                            <input type="checkbox" value="" name="">
                            <span class="text">Commit project</span>
                            <small class="hidden-sm-down label-pill label-default"><i class="fa fa-clock-o"></i> 1 month</small>
                            <div class="tools">
                                <i class="fa fa-edit"></i>
                                <i class="fa fa-trash-o"></i>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- /.card-block -->
                <div class="card-footer clearfix no-border">
                    <button type="button" class="btn btn-secondary pull-right"><i class="fa fa-plus"></i> Add item</button>
                </div>
            </div>
            <!-- /.card -->
        </section>
    </div>

    <div class="row">
        <section class="col-xl-4 connectedSortable">
            <!-- Chat box -->
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-comments-o"></i>

                    <h3 class="card-title">Chat</h3>

                    <div class="card-tools pull-right">
                        <div class="btn-group" data-toggle="btn-toggle">
                            <button type="button" class="btn btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-block chat" id="chat-box">
                    <!-- chat item -->
                    <div class="item">
                        <img src="{{asset('img/avatars/user4.jpg')}}" alt="user image" class="online">

                        <p class="message">
                            <a href="#" class="name">
                                <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2:15</small>
                                Eduard Chess
                            </a>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                        </p>
                        <div class="attachment">
                            <h4>Attachments:</h4>

                            <p class="filename">
                                Theme-thumbnail-image.jpg
                            </p>

                            <div class="pull-right">
                                <button type="button" class="btn btn-primary btn-sm btn-flat">Open</button>
                            </div>
                        </div>
                        <!-- /.attachment -->
                    </div>
                    <!-- /.item -->
                    <!-- chat item -->
                    <div class="item">
                        <img src="{{asset('img/avatars/user3.jpg')}}" alt="user image" class="offline">

                        <p class="message">
                            <a href="#" class="name">
                                <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:15</small>
                                Mike Dacascos
                            </a>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                        </p>
                    </div>
                    <!-- /.item -->
                    <!-- chat item -->
                    <div class="item">
                        <img src="{{asset('img/avatars/user2.jpg')}}" alt="user image" class="offline">

                        <p class="message">
                            <a href="#" class="name">
                                <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:30</small>
                                Katerine Lyu
                            </a>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                        </p>
                    </div>
                    <!-- /.item -->
                </div>
                <!-- /.chat -->
                <div class="card-footer">
                    <div class="input-group">
                        <input class="form-control" placeholder="Type message...">

                        <div class="input-group-btn">
                            <button type="button" class="btn btn-secondary">Send <i class="fa fa-arrow-circle-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card (chat box) -->
        </section>
        <section class="col-xl-4 connectedSortable">
            <!-- Calendar -->
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-calendar"></i>

                    <h3 class="card-title">Calendar</h3>
                    <!-- tools card -->
                    <div class="pull-right card-tools">
                        <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-block no-padding">
                    <!--The calendar -->
                    <div id="calendar" style="width: 100%"></div>
                </div>
                <!-- /.card-block -->
                <div class="card-footer text-black">
                    <div class="row">
                        <div class="col-sm-6">
                            <!-- Progress bars -->
                            <div class="clearfix">
                                <span class="pull-left">Task #1</span>
                                <small class="pull-right">90%</small>
                            </div>
                            <progress class="progress progress-success xs" value="15" max="100">15%</progress>

                            <div class="clearfix">
                                <span class="pull-left">Task #2</span>
                                <small class="pull-right">70%</small>
                            </div>
                            <progress class="progress progress-success xs" value="40" max="100">40%</progress>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <div class="clearfix">
                                <span class="pull-left">Task #3</span>
                                <small class="pull-right">60%</small>
                            </div>
                            <progress class="progress progress-success xs" value="34" max="100">34%</progress>

                            <div class="clearfix">
                                <span class="pull-left">Task #4</span>
                                <small class="pull-right">40%</small>
                            </div>
                            <progress class="progress progress-success xs" value="25" max="100">25%</progress>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
            </div>
            <!-- /.card -->
        </section>
        <section class="col-xl-4 connectedSortable">
            <!-- quick email widget -->
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-envelope"></i>

                    <h3 class="card-title">Message</h3>
                    <!-- tools card -->
                    <div class="card-tools pull-right">
                        <div class="btn-group" data-toggle="btn-toggle">
                            <button type="button" class="btn btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /. tools -->
                </div>
                <div class="card-block">
                    <form action="#" method="post">
                        <div class="form-group">
                            <input type="email" class="form-control" name="emailto" placeholder="To:">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="subject" placeholder="Subject">
                        </div>
                        <div>
                            <textarea class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                        </div>
                    </form>
                </div>
                <div class="card-footer clearfix">
                    <button type="button" class="pull-right btn btn-default" id="sendEmail">Send
                        <i class="fa fa-arrow-circle-right"></i></button>
                </div>
            </div>
        </section>
    </div>

@endsection

@section('page-scripts')
    <script src="{{asset('js/bootstrap-datepicker-bs4.js')}}"></script>
    <script src="{{asset('https://www.gstatic.com/charts/loader.js')}}"></script>
    <script>
        $(function () {

            "use strict";

            //Make the dashboard widgets sortable Using jquery UI
            $(".connectedSortable").sortable({
                placeholder: "sort-highlight",
                connectWith: ".connectedSortable",
                handle: ".card-header, .nav-tabs",
                forcePlaceholderSize: true,
                zIndex: 999999
            });
            $(".connectedSortable .card-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");


            google.charts.load('current', {packages: ['gantt']});
            google.charts.setOnLoadCallback(drawGoogleCharts);

            function daysToMilliseconds(days) {
                return days * 24 * 60 * 60 * 1000;
            }

            function drawGoogleCharts() {

                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Task ID');
                data.addColumn('string', 'Task Name');
                data.addColumn('date', 'Start Date');
                data.addColumn('date', 'End Date');
                data.addColumn('number', 'Duration');
                data.addColumn('number', 'Percent Complete');
                data.addColumn('string', 'Dependencies');

                data.addRows([
                    ['Research', 'Research project',
                        new Date(2016, 0, 1), new Date(2016, 0, 5), null,  100,  null],
                    ['Design', 'Design Interface',
                        null, new Date(2016, 0, 10), daysToMilliseconds(6), 100, 'Research'],
                    ['Program', 'UML Design',
                        null, new Date(2016, 0, 10), daysToMilliseconds(3), 100, 'Research'],
                    ['Develop', 'Develop functionalities',
                        null, new Date(2016, 0, 16), daysToMilliseconds(6), 20, 'Design, Program'],
                    ['Debug', 'Test',
                        null, new Date(2016, 0, 22), daysToMilliseconds(1), 0, 'Develop'],
                    ['Present', 'Present to client',
                        null, new Date(2016, 0, 23), daysToMilliseconds(1), 0, 'Debug'],
                    ['Complete', 'Release Alpha',
                        null, new Date(2016, 0, 28), daysToMilliseconds(1), 0, 'Present']

                ]);

                var options = {
                    height: 300
                };

                var chart = new google.visualization.Gantt(document.getElementById('chart'));

                chart.draw(data, options);
            }


            //jQuery UI sortable for the todo list
            $(".todo-list").sortable({
                placeholder: "sort-highlight",
                handle: ".handle",
                forcePlaceholderSize: true,
                zIndex: 999999
            });


            //The Calender
            $("#calendar").datepicker();

            //SLIMSCROLL FOR CHAT WIDGET
            $('#chat-card').slimScroll({
                height: '250px'
            });



            /* The todo list plugin */
            $(".todo-list").todolist({
                onCheck: function (ele) {
                    window.console.log("The element has been checked");
                    return ele;
                },
                onUncheck: function (ele) {
                    window.console.log("The element has been unchecked");
                    return ele;
                }
            });

        });
    </script>
@endsection