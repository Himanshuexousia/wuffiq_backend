@extends('app')

@section('page-css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/crudgen/css/style.css') }}">
    <style>

        #loading {
            position:fixed;
            top:0px;
            left:0px;
            bottom:0px;
            right:0px;
            width:100%;
            height:100%;
            border:none;
            margin:0;
            padding:20% 0 0 50%;
            overflow:hidden;
            text-align: center;
            z-index:999999;
            background: rgba( 0, 0, 0, .8 )
        }
    </style>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Crud Generator
        </div>
        <div class="card-block">
            <p>Fill the form bellow to create controller, class, seeder, menu and routing files. Your database can be updated also. <a href="{{route('documentation').'#crudgen'}}" target="_blank">Check tutorial</a>.</p>
            @if (! empty($status))
                <div class="alert {{$status}}" role="alert">
                    {{$message}}
                </div>
            @endif
            <form action="{{ route('crudgen.generate') }}" method="post" id="form" role="form">
                <legend>Class information</legend>
                <fieldset>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="label_ln control-label" for="class_name">Class name:</label>
                                <input id="class_name" name="class_name" type="text" placeholder="" required="" class="input_ln form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="label_fn control-label" for="route_group">Route Group (prefix):</label>
                                <input id="route_group" name="route_group" type="text" placeholder="" class="input_fn form-control" value="{{ config('crudgen.prefix')  }}">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="label_fn control-label" for="middleware">Middleware:</label>
                                <input id="middleware" name="middleware" type="text" placeholder="" class="input_fn form-control" value="{{ config('crudgen.middleware')  }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3>Fields</h3>
                        </div>
                    </div>
                    <div class="row clonedRow" id="fieldRow1">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label class="label_fn control-label" for="field_name">Field name:</label>
                                <input class="input_ln form-control" id="field_name" name="field_name[]" type="text" placeholder="" class="input_fn form-control" required="">
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label class="label_ttl control-label" for="field_type">Field Type:</label>
                                <select class="select_ttl form-control" name="field_type[]" id="field_type">
                                    <!--<option value="" selected="selected" disabled="disabled">field type</option>-->
                                    <option value="string">string</option>
                                    <option value="number">number</option>
                                    <option value="char">char</option>
                                    <option value="varchar">varchar</option>
                                    <option value="password">password</option>
                                    <option value="email">email</option>
                                    <option value="date">date</option>
                                    <option value="datetime">datetime</option>
                                    <option value="time">time</option>
                                    <option value="timestamp">timestamp</option>
                                    <option value="text">text</option>
                                    <option value="mediumtext">mediumtext</option>
                                    <option value="longtext">longtext</option>
                                    <option value="json">json</option>
                                    <option value="jsonb">jsonb</option>
                                    <option value="bigint">bigint</option>
                                    <option value="mediumint">mediumint</option>
                                    <option value="tinyint">tinyint</option>
                                    <option value="smallint">smallint</option>
                                    <option value="boolean">boolean</option>
                                    <option value="decimal">decimal</option>
                                    <option value="double">double</option>
                                    <option value="float">float</option>
                                    <option value="enum">enum</option>
                                </select> <!-- end .select_ttl -->
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="checkbox">
                                <label>
                                    <input class="_checkbox" type="checkbox" value="true" id="required" name="required[]">
                                    Required
                                </label>
                            </div>
                        </div>
                    </div>

                    <!-- Button (Double) -->
                    <p>
                        <button type="button" id="btnAddField" name="btnAddField" class="btn btn-primary">Add field</button>
                        <button type="button" id="btnRemoveField" name="btnRemoveField" class="btn btn-danger"><span class="ui-button-text">Remove field</span></button>
                    </p>


                    <!-- Button -->
                    <p>
                        <button id="submit_button" name="submit_button" class="btn btn-primary">Generate</button>
                    </p>
                    <input name="_token" value="{{ csrf_token() }}" type="hidden">
                </fieldset>
            </form>
        </div>
    </div>

    <iframe id="loading" src="{{asset('vendor/crudgen/css/ajax-loader.gif')}}" scrolling="no" frameborder="0"></iframe>
@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{ asset('vendor/crudgen/js/clone-form.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#loading').hide();

            $("form").submit(function(){
                $("#loading").show();
            });
        });
    </script>
@endsection