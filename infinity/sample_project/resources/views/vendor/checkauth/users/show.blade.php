@extends('app')
{{-- Web site Title --}}
@section('page-title')
	{{ trans_choice('checkauth::pages.users', 1) }}
@endsection

{{-- Content --}}
@section('content')

    <?php
        // Determine the edit profile route
        if (($user->email == Sentinel::check()->email)) {
            $editAction = route('checkauth.profile.edit');
        } else {
            $editAction =  action('\\Checkmate\CheckAuth\Http\Controllers\UserController@edit', [$user->hash]);
        }
    ?>


	<div class="row">
		<div class="col-xs-8">
			<div class="card">
				<div class="card-header">
					<h3>{{ trans_choice('checkauth::pages.users', 1) }}</h3>
				</div>
				<div class="card-block">
					@if ($user->first_name)
						<p><strong>{{ trans('checkauth::pages.first-name') }}:</strong> {{ $user->first_name }} </p>
					@endif
					@if ($user->last_name)
						<p><strong>{{ trans('checkauth::pages.last-name') }}:</strong> {{ $user->last_name }} </p>
					@endif
					<p><strong>Email:</strong> {{ $user->email }}</p>
					<div class='btn-group'>
						<button class="btn btn-primary" onClick="location.href='{{ $editAction }}'">{{ trans('checkauth::pages.update') }}</button>
					</div>
				</div>
				<div class="card-footer">
					{{ trans('checkauth::pages.created-at') }}: {{ $user->created_at }} <br>{{ trans('checkauth::pages.updated-at') }}: {{ $user->updated_at }}
				</div>
			</div>
		</div>
		<div class="col-xs-4">
			<div class="card">
				<div class="card-header">
					<h3>{{ trans_choice('checkauth::pages.roles',2) }}</h3>
				</div>
				<div class="card-block">
					@foreach($roles as $role)
						<label class="checkbox-inline">
							<input type="checkbox" name="roles[{{ $role->slug }}]" value="1" {{ ($user->inRole($role->slug) ? 'checked' : '') }} disabled> {{ $role->name }}
						</label>
					@endforeach
				</div>
				<div class="card-footer">

				</div>
			</div>
		</div>
	</div>

@stop
