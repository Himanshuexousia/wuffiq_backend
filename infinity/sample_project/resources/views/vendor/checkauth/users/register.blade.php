@extends('checkauth::sessions.logoff')
{{-- Web site Title --}}
@section('checkauth::page-title')
    Register New Account
@endsection

<?php $data = Session::get('data') ?>

{{-- Content --}}
@section('checkauth:grid-class')
    col-md-offset-4 col-md-4 col-md-offset-4
@stop

@section('checkauth:card-title')
    Register New Account
@stop

@section('checkauth::card-block')
    <form method="POST" action="{{ route('checkauth.register.user') }}" accept-charset="UTF-8" id="register-form" enctype="multipart/form-data">

        <div class="form-group {{ ($errors->has('first_name')) ? 'has-error' : '' }}">
            <input class="form-control" placeholder="First name" name="first_name" type="text"  value="{{ !Input::old('first_name')&& isset($data) ? $data['first_name'] : Input::old('first_name')  }}">
            {{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}
        </div>

        <div class="form-group {{ ($errors->has('last_name')) ? 'has-error' : '' }}">
            <input class="form-control" placeholder="Last name" name="last_name" type="text"  value="{{ !Input::old('last_name')&& isset($data) ? $data['last_name'] : Input::old('last_name') }}">
            {{ ($errors->has('last_name') ? $errors->first('last_name') : '') }}
        </div>

        <div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
            <input class="form-control" placeholder="E-mail" name="email" type="text" value="{{ !Input::old('email')&& isset($data) ? $data['email'] : Input::old('email')  }}">
            {{ ($errors->has('email') ? $errors->first('email') : '') }}
        </div>

        <div class="form-group {{ ($errors->has('password')) ? 'has-error' : '' }}">
            <input class="form-control" placeholder="Password" name="password" value="" type="password">
            {{ ($errors->has('password') ?  $errors->first('password') : '') }}
        </div>

        <div class="form-group {{ ($errors->has('password_confirmation')) ? 'has-error' : '' }}">
            <input class="form-control" placeholder="Confirm Password" name="password_confirmation" value="" type="password">
            {{ ($errors->has('password_confirmation') ?  $errors->first('password_confirmation') : '') }}
        </div>

        <div class="form-group {{ ($errors->has('avatar')) ? 'has-error' : '' }}" for="avatar">
            <div class="col-sm-10">
                <label for="{{ 'avatar' }}" class="col-sm-2 control-label">{{ ucwords(str_replace('_',' ','avatar')) }}</label>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
                        <img src="{{ isset($data) ?  $data['avatar'] : url('/vendor/checkauth/img/blank_avatar.png') }}" alt="profile image"/>
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                    <div>
                        <span class="btn btn-secondary btn-file"><span class="fileinput-new">Select image</span>
                        <span class="fileinput-exists">Change</span><input type="file" name="avatar" id="avatar"></span>
                        <a href="#" class="btn btn-secondary fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </div>
        </div>

        <input name="_token" value="{{ csrf_token() }}" type="hidden">
        <input name="avatar_url" value="{{$data['avatar']}}" type="hidden">
        <input name="oauth_id" value="{{ isset($data) ? $data['oauth_id'] : '' }}" type="hidden">
        {{ ($errors->has('oauth_id') ?  $errors->first('oauth_id') : '') }}
        <input class="btn btn-primary" value="Register" type="submit">

    </form>
@stop

@section('checkauth::card-footer')
@stop