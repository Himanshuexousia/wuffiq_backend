@extends('app')
{{-- Web site Title --}}
@section('page-title')
    {{trans('checkauth::pages.create')}} {{trans_choice('checkauth::pages.roles', 2)}}
@endsection

{{-- Content --}}
@section('content-header')

<div class="row">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                <h3>{{trans('checkauth::pages.create')}} {{trans_choice('checkauth::pages.roles', 1)}}</h3>
            </div>
            <form method="POST" action="{{ route('checkauth.roles.store') }}" accept-charset="UTF-8">
            <div class="card-block">
                <div class="form-group row {{ ($errors->has('name')) ? 'has-danger' : '' }}">
                    <label for="Permissions" class="col-sm-2 form-control-label">{{trans('checkauth::pages.name')}}</label>
                    <div class="col-sm-10">
                        <input class="form-control" placeholder="Name" name="name" type="text"  value="{{ Input::old('email') }}">
                        {{ ($errors->has('name') ? $errors->first('name') : '') }}
                    </div>

                </div>
                <div class="form-group row">
                    <label for="Permissions" class="col-sm-2 form-control-label">{{trans('checkauth::pages.permissions')}}</label>
                    <div class="col-sm-10">
                        <?php $defaultPermissions = config('checkauth.default_permissions', []); ?>
                        @foreach ($defaultPermissions as $permission)
                            <label class="checkbox-inline">
                                <input name="permissions[{{ $permission }}]" value="true" type="checkbox"
                                       @if (Input::old('permissions[' . $permission .']'))
                                       checked
                                        @endif
                                        > {{ ucwords($permission) }}
                            </label>
                        @endforeach
                    </div>
                </div>
                <div class='btn-group'>
                    <input name="_token" value="{{ csrf_token() }}" type="hidden">
                    <input class="btn btn-primary" value="{{trans('checkauth::pages.create')}}" type="submit">
                </div>
            </div>
            <div class="card-footer">

            </div>
            </form>
        </div>
    </div>
</div>

@stop