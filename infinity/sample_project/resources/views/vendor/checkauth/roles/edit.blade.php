@extends('app')
{{-- Web site Title --}}
@section('page-title')
    {{trans('checkauth::pages.update')}} {{trans_choice('checkauth::pages.roles', 1)}}
@endsection

{{-- Content --}}
@section('content-header')
    <div class="row">

        <div class="col-xs-12">
            <div class="card">
                <div class="card-header h70">
                    <h3 class="card-title pull-left">{{trans('checkauth::pages.update')}} {{trans_choice('checkauth::pages.roles', 1)}}</h3>
                </div>
                <div class="card-block">

                    <form method="POST" action="{{ route('checkauth.roles.update', $role->hash) }}" accept-charset="UTF-8">

                        <div class="form-group row {{ ($errors->has('name')) ? 'has-danger' : '' }}">
                            <label for="Permissions" class="col-sm-2 form-control-label">{{trans('checkauth::pages.name')}}</label>
                            <div class="col-sm-10">
                                <input class="form-control" placeholder="Name" name="name" value="{{ Input::old('name') ? Input::old('name') : $role->name }}" type="text">
                                {{ ($errors->has('name') ? $errors->first('name') : '') }}
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="Permissions" class="col-sm-2 form-control-label">{{trans('checkauth::pages.permissions')}}</label>
                            <div class="col-sm-10">
                                <?php $defaultPermissions = config('checkauth.default_permissions', []); ?>
                                @foreach ($defaultPermissions as $permission)
                                    <label class="checkbox-inline">
                                        <input name="permissions[{{ $permission }}]" value="true" type="checkbox" {{ (isset($permissions[$permission]) ? 'checked' : '') }}>
                                        {{ ucwords($permission) }}
                                    </label>
                                @endforeach
                            </div>
                        </div>

                        <input name="_method" value="PUT" type="hidden">
                        <input name="_token" value="{{ csrf_token() }}" type="hidden">
                        <input class="btn btn-primary" value="{{trans('checkauth::pages.update')}}" type="submit">

                    </form>
                </div>
                <div class="card-footer">
                </div>
            </div>

@stop