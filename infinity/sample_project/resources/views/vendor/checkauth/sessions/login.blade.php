<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Infinity Admin - Login</title>
    <meta name="robots" content="index, follow">
    <meta name="keywords" content="app, admin, dashboard, laravel, php, html5, theme, template"/>
    <meta name="description" content="Infinity Admin, a fully responsive web app admin theme with charts, dashboards, landing pages and components. Fully customized with more than 140.000 layout combinations." />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="{{asset('vendor/tether/css/tether.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('vendor/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('vendor/ionicons/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/icheck/skins/flat/blue.css')}}" />
    <!-- Main css -->
    <link href="{{asset('css/main.css')}}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        html, body {
            overflow: hidden;
        }
        body {
            min-height: 1200px;
            height: 100%;
        }
        .login-page {
            background: #474747;
        }
        .login-box {
            position: absolute;
            top: 50px;
            left: 50%;
            margin-left: -180px;
        }
        .login-form .card-info {
            border-top: 6px solid #003872;
        }
    </style>
</head>
<body class="content-light login-page">
<div class="login-box white br12 p12">
    <div class="text-sm-center">
        <a href="{{route('home')}}" class="logo">
            <img src="{{asset('img/infinity/infinity_logo.png')}}" height="80px">
        </a>
    </div>
    <div class="login-box-body br24">
        <p class="login-box-msg">Sign in</p>
        @include('checkauth::partials.notifications')
        <form method="POST" action="{{ route('checkauth.session.store') }}" accept-charset="UTF-8" class="form-signin">

            <div class="form-group {{ ($errors->has('email')) ? 'has-danger' : '' }} has-feedback">
                <input type="email" class="form-control {{ ($errors->has('email')) ? 'form-control-danger' : '' }}" placeholder="Email" autofocus="autofocus" type="text" value="{{ (Input::old('email')) ? Input::old('email') : "user@infinity.com"}}"  name="email">
                @if($errors->has('email'))
                    <label class="form-control-label" for="email">{{$errors->first('email')}}</label>
                @endif
            </div>
            <div class="form-group {{ ($errors->has('password')) ? 'has-danger' : '' }} has-feedback">
                <input class="form-control {{ ($errors->has('password')) ? 'form-control-danger' : '' }}" placeholder="Password" type="password"  name="password" value="infinity">
                @if($errors->has('password'))
                    <label class="form-control-label" for="email">{{$errors->first('password')}}</label>
                @endif
            </div>
            @if(config('checkauth.recapcha'))
                <div class="form-group {{ ($errors->has('g-recaptcha-response')) ? 'has-danger' : '' }}">
                    {!! app('captcha')->display() !!}
                    @if($errors->has('g-recaptcha-response'))
                        <label class="form-control-label" for="email">{{$errors->first('g-recaptcha-response')}}</label>
                    @endif
                </div>
            @endif
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> {{ trans('checkauth::pages.remember-me') }}
                        </label>
                        <input name="_token" value="{{ csrf_token() }}" type="hidden">
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block">{{ trans('checkauth::pages.login') }}</button>
                </div>
                <!-- /.col -->
            </div>

        </form>
        <br />
        <br />

        @if(config('checkauth.oauth_providers'))
            <div class="row">
                <div class="col-xs-12">
                    @if(in_array('google', config('checkauth.oauth_providers')))
                        <a class="btn btn-block btn-social btn-google" href="{{route('checkauth.oauth.login', ['provider' => 'google'])}}">
                            <i class="fa fa-google-plus"></i>
                            {{ trans('checkauth::pages.login') }} {{ trans('checkauth::pages.with') }} Google
                        </a>
                    @endif
                    @if(in_array('facebook', config('checkauth.oauth_providers')))
                        <a class="btn btn-block btn-social btn-facebook" href="{{route('checkauth.oauth.login',['provider' => 'facebook'])}}">
                            <i class="fa fa-facebook"></i>
                            {{ trans('checkauth::pages.login') }} {{ trans('checkauth::pages.with') }} Facebook
                        </a>
                    @endif
                    @if(in_array('twitter', config('checkauth.oauth_providers')))
                        <a class="btn btn-block btn-social btn-twitter" href="{{route('checkauth.oauth.login',['provider' => 'twitter'])}}">
                            <i class="fa fa-twitter"></i>
                            {{ trans('checkauth::pages.login') }} {{ trans('checkauth::pages.with') }} Twitter
                        </a>
                    @endif
                    @if(in_array('linkedin', config('checkauth.oauth_providers')))
                        <a class="btn btn-block btn-social btn-linkedin" href="{{route('checkauth.oauth.login',['provider' => 'linkedin'])}}">
                            <i class="fa fa-linkedin"></i>
                            {{ trans('checkauth::pages.login') }} {{ trans('checkauth::pages.with') }} Linkedin
                        </a>
                    @endif
                    @if(in_array('github', config('checkauth.oauth_providers')))
                        <a class="btn btn-block btn-social btn-github" href="{{route('checkauth.oauth.login',['provider' => 'github'])}}">
                            <i class="fa fa-github"></i>
                            {{ trans('checkauth::pages.login') }} {{ trans('checkauth::pages.with') }} Github
                        </a>
                    @endif
                    @if(in_array('bitbucket', config('checkauth.oauth_providers')))
                        <a class="btn btn-block btn-social btn-facebook" href="{{route('checkauth.oauth.login',['provider' => 'bitbucket'])}}">
                            <i class="fa fa-bitbucket"></i>
                            {{ trans('checkauth::pages.login') }} {{ trans('checkauth::pages.with') }} Bitbucket
                        </a>
                    @endif
                </div>
            </div>
        @endif
        <br>
        <a class="btn-block btn btn-secondary" href="{{ route('checkauth.forgot.form') }}">{{ trans('checkauth::pages.forgot-password') }}</a>
        <br>
        <a class="btn-block btn btn-secondary" href="{{ route('checkauth.register.form') }}">{{ trans('checkauth::pages.register') }}</a>

    </div>
</div>

<script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('vendor/tether/js/tether.min.js')}}"></script>
<script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('vendor/icheck/icheck.min.js') }}"></script>
<script src="{{asset('js/jquery.particleground.min.js')}}"></script>
<script>
    $(function () {
        $('.content-light').particleground({
            dotColor: '#6d6d6d',
            lineColor: '#6d6d6d'
        });
        $('input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>
