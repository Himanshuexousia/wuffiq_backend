@extends('app-clean')

@section('page-title')
    Blank Page
@endsection

@section('page-css')

@endsection

@section('content-header')
    <h1>
        Blank Page
    </h1>
@endsection

@section('content')

    <div class="card">
        <div class="card-header text-xs-center">
            Card Title
        </div>
        <div class="card-block">
            <p class="card-text">Adipiscing tristique habitasse a, a non lacus, velit lorem. Placerat tincidunt odio placerat porta! Adipiscing? Pulvinar cum aliquam turpis! Aliquet, tempor mus pulvinar, cras diam magna phasellus porta vel cum platea lundium, sit nunc elementum sed tristique augue tempor! Aliquam, elit, et! Enim est nisi porta lundium ultricies, turpis.</p>
        </div>
        <div class="card-footer"></div>
    </div>

@endsection

@section('page-scripts')

@endsection