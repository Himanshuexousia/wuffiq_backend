@extends('app')

@section('page-title')

@endsection

@section('page-css')

@endsection
@section('content-header')
    <h1>
        Sample page
    </h1>
@endsection

@section('content')
    <div class="container">

        <!-- Your Page Content Here -->
        @section('checkauth::main-content')
            <div class="starter-template">
                <h1>Auth Plugin (free)</h1>
                <p>The advanced Authentication and Authorization plugin comes free on Infinity.</p>
                <p>To know more about this plugin visit: <a href="{{asset('http://auth.checkmatedigital.com/')}}" target="_blank">http://auth.checkmatedigital.com</a> </p>
                <p class="lead">{{ trans('checkauth::pages.helloworld') }}</p>

                @if($user = Sentinel::check())
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>{{ trans('checkauth::pages.flashsuccess') }}! </strong>{{ trans('checkauth::pages.hello') }} {{$user['first_name']}}!
                    </div>
                    <a href="{{route('checkauth.logout')}}"><button type="button" class="btn btn-primary">Click here to logout</button></a>
                @else
                    <div class="alert alert-warning alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Warning!</strong> No user is logged in
                    </div>
                    <p>use e-mail: test@auth.com with password: authtest</p>
                    <a href="{{route('checkauth.login')}}"><button type="button" class="btn btn-primary">Click here to login</button></a>
                @endif

            </div>
        @show

    </div><!-- /.container -->

@endsection

@section('page-scripts')

@endsection