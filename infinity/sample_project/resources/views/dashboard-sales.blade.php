@extends('app')

@section('page-title')
    Sales Dashboard
@endsection

@section('page-css')
    <style>
        .jvectormap-zoomin, .jvectormap-zoomout {
            background: rgba(0,0,0, 0.2);
            padding: 5px;
            width: 20px;
            height: 20px;
        }

        .chart-legend ul{list-style-type: none;}
        .chart-legend ul li { display: inline;}

    </style>
@endsection

@section('content-header')
    <h1>
        Sales Dashboard
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6 col-lg-3">
            <div class="card-info bg-yellow">
                <span class="card-info-icon"><i class="ion-ios-cart-outline"></i></span>
                <div class="card-info-content">
                    <span class="card-info-text">Processing</span>
                    <span class="card-info-number">30</span>
                    <progress class="progress progress-bar-white" value="30" max="30"></progress>
                  <span class="progress-description">
                    of 30
                  </span>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="card-info bg-green">
                <span class="card-info-icon"><i class="ion-ios-checkmark-outline"></i></span>
                <div class="card-info-content">
                    <span class="card-info-text">Delivered</span>
                    <span class="card-info-number">100</span>
                    <progress class="progress progress-bar-white" value="100" max="120"></progress>
                  <span class="progress-description">
                    of 120
                  </span>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="card-info bg-red">
                <span class="card-info-icon"><i class="ion ion-ios-undo-outline"></i></span>
                <div class="card-info-content">
                    <span class="card-info-text">Backordered</span>
                    <span class="card-info-number">4</span>
                    <progress class="progress progress-bar-white" value="4" max="5"></progress>
                  <span class="progress-description">
                    of 5
                  </span>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="card-info bg-aqua">
                <span class="card-info-icon"><i class="ion-ios-paperplane-outline"></i></span>
                <div class="card-info-content">
                    <span class="card-info-text">Shipped</span>
                    <span class="card-info-number">10</span>
                    <progress class="progress progress-bar-white"  value="10" max="20"></progress>
                  <span class="progress-description">
                    of 20
                  </span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-lg-6 connectedSortable">
            <div class="card" id="divSales">
                <div class="card-header with-border">
                    <h3 class="card-title">Sales</h3>

                    <div class="card-tools pull-right">
                        <button type="button" class="btn btn-card-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-card-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="chart-responsive">
                                <canvas id="doughnutChart" height="150"></canvas>
                            </div>
                        </div>
                        <div class="col-sm-12 div-legend">
                            <ul class="chart-legend clearfix">
                                <li><i class="fa fa-square text-red mr5"></i>Product 1</li>
                                <li><i class="fa fa-square text-green mr5"></i>Product 2</li>
                                <li><i class="fa fa-square text-yellow mr5"></i>Product 3</li>
                                <li><i class="fa fa-square text-aqua mr5"></i>Product 4</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-lg-6 connectedSortable">
            <div class="card">
                <div class="card-header with-border">
                    <h3 class="card-title">Sales share</h3>

                    <div class="card-tools pull-right">
                        <button type="button" class="btn btn-card-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-card-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="chart-responsive">
                                <canvas id="pieChart" height="150"></canvas>
                            </div>
                        </div>
                        <div class="col-xl-12 div-legend">
                            <ul class="chart-legend clearfix">
                                <li><i class="fa fa-square text-red mr5"></i>North America</li>
                                <li><i class="fa fa-square text-green mr5"></i>South America</li>
                                <li><i class="fa fa-square text-yellow mr5"></i>Europe</li>
                                <li><i class="fa fa-square text-aqua mr5"></i>Asia</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6 col-lg-12 connectedSortable">
            <div class="card">
                <div class="card-header with-border">
                    <h3 class="card-title">Online vs in-Store Sales</h3>

                    <div class="card-tools pull-right">
                        <button type="button" class="btn btn-card-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-card-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="card-block" style="height: 310px;">
                    <p class="text-center">
                        <strong>Sales: Jan - Jul 2016</strong>
                    </p>

                    <div class="chart">
                        <canvas id="salesChart" style="height: 250px;"></canvas>
                    </div>
                </div>
                <div class="card-footer">
                    <div id="sales-legend" class="chart-legend pull-right"></div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-12 connectedSortable">
            <div class="card">
                <div class="card-header with-border">
                    <h3 class="card-title">World Sales Today</h3>

                    <div class="card-tools pull-right">
                        <button type="button" class="btn btn-card-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-card-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="card-block no-padding">
                    <div id="world-map-markers" style="height: 325px;"></div>
                </div>
                <div class="card-footer">
                    Last updated on 13:43 am
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header with-border">
                    <h3 class="card-title">Latest Orders</h3>

                    <div class="card-tools pull-right">
                        <button type="button" class="btn btn-card-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-card-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Item</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="table-success">
                                <td><a href="{{route('page-invoice')}}">ORDR1234</a></td>
                                <td>Product 3</td>
                                <td><span class="label label-success">Delivered</span></td>
                            </tr>
                            <tr class="table-warning">
                                <td><a href="{{route('page-invoice')}}">ORDR1234</a></td>
                                <td>Product 1</td>
                                <td><span class="label label-warning">Processing</span></td>
                            </tr>
                            <tr class="table-danger">
                                <td><a href="{{route('page-invoice')}}">ORDR1234</a></td>
                                <td>Product 2</td>
                                <td><span class="label label-danger">Backordered</span></td>
                            </tr>
                            <tr class="table-info">
                                <td><a href="{{route('page-invoice')}}">ORDR1234</a></td>
                                <td>Product 1</td>
                                <td><span class="label label-info">Shipped</span></td>
                            </tr>
                            <tr class="table-warning">
                                <td><a href="{{route('page-invoice')}}">ORDR1234</a></td>
                                <td>Product 1</td>
                                <td><span class="label label-warning">Processing</span></td>
                            </tr>
                            <tr class="table-danger">
                                <td><a href="{{route('page-invoice')}}">ORDR1234</a></td>
                                <td>Product 2</td>
                                <td><span class="label label-danger">Backordered</span></td>
                            </tr>
                            <tr class="table-success">
                                <td><a href="{{route('page-invoice')}}">ORDR1234</a></td>
                                <td>Product 3</td>
                                <td><span class="label label-success">Delivered</span></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer clearfix">
                    <a href="javascript::;" class="btn btn-sm btn-secondary btn-flat pull-right">View All</a>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header with-border">
                    <h3 class="card-title">Stock</h3>

                    <div class="card-tools pull-right">
                        <button type="button" class="btn btn-card-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-card-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th>Product ID</th>
                                <th>Description</th>
                                <th>In Stock</th>
                                <th>Stock flow</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><a href="#">PRD1</a></td>
                                <td>Product3</td>
                                <td>10</td>
                                <td>
                                    <div class="sparkbar" data-color="#00a65a" data-height="20">90,80,90,-70,61,-83,63</div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="#">PRD2</a></td>
                                <td>Product 1</td>
                                <td>120</td>
                                <td>
                                    <div class="sparkbar" data-color="#f39c12" data-height="20">90,80,-90,70,61,-83,68</div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="#">PRD3</a></td>
                                <td>Product 2</td>
                                <td>30</td>
                                <td>
                                    <div class="sparkbar" data-color="#f56954" data-height="20">90,-80,90,70,-61,83,63</div>
                                </td>
                            </tr>
                            <tr class="table-danger">
                                <td><a href="#">PRD4</a></td>
                                <td>Product 1</td>
                                <td><span class="label label-danger">Out of stock</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#00c0ef" data-height="20">90,80,-90,70,-61,83,63</div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="#">PRD5</a></td>
                                <td>Product 1</td>
                                <td>125</td>
                                <td>
                                    <div class="sparkbar" data-color="#f39c12" data-height="20">90,80,-90,70,61,-83,68</div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="#">PRD6</a></td>
                                <td>Product 2</td>
                                <td>68</td>
                                <td>
                                    <div class="sparkbar" data-color="#f56954" data-height="20">90,-80,90,70,-61,83,63</div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="#">PRD7</a></td>
                                <td>Product3</td>
                                <td>47</td>
                                <td>
                                    <div class="sparkbar" data-color="#00a65a" data-height="20">90,80,90,-70,61,-83,63</div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer clearfix">
                    <a href="javascript::;" class="btn btn-sm btn-secondary btn-flat pull-right">View All</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

@endsection

@section('page-scripts')
    <script src="{{asset('js/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('vendor/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{asset('vendor/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{asset('/vendor/chart-js/Chart.min.js')}}"></script>
    <script>
        $(function () {

            'use strict';

            $(".connectedSortable").sortable({
                placeholder: "sort-highlight",
                connectWith: ".connectedSortable",
                handle: ".card-header, .nav-tabs",
                forcePlaceholderSize: true,
                zIndex: 999999
            });
            $(".connectedSortable .card-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");
            drawCharts();

            //event listener to div resizing
            var chartDiv = document.getElementById("divSales");
            //css-element-queries library
            new ResizeSensor(chartDiv, function() {
                drawCharts();
            });

            function drawCharts(){
                var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
                var salesChart = new Chart(salesChartCanvas);

                var salesChartData = {
                    labels: ["January", "February", "March", "April", "May", "June", "July"],
                    datasets: [
                        {
                            label: "Online",
                            fillColor: "rgb(60, 141, 188)",
                            strokeColor: "rgb(44, 107, 143)",
                            pointColor: "rgb(44, 107, 143)",
                            pointStrokeColor: "rgb(44, 107, 143)",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgb(32,80,108)",
                            data: [10, 25, 40, 55, 60, 75, 80]
                        },
                        {
                            label: "Store",
                            fillColor: "rgba(0,141,76,0.9)",
                            strokeColor: "rgba(0,113,61,1)",
                            pointColor: "rgba(0,113,61,1)",
                            pointStrokeColor: "rgba(0,113,61,1)",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(60,141,188,1)",
                            data: [30, 15, 60, 50, 40, 20, 10]
                        }
                    ]
                };

                var salesChartOptions = {
                    showScale: true,
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleGridLineWidth: 1,
                    scaleShowHorizontalLines: true,
                    scaleShowVerticalLines: true,
                    bezierCurve: false,
                    pointDot: true,
                    pointDotRadius: 4,
                    pointDotStrokeWidth: 1,
                    pointHitDetectionRadius: 20,
                    datasetStroke: true,
                    datasetStrokeWidth: 2,
                    datasetFill: true,
                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><i class=\"fa fa-square m5\" style=\"color:<%=datasets[i].pointColor%>\"></i><%=datasets[i].label%></li><%}%></ul>",
                    maintainAspectRatio: true,
                    responsive: true
                };

                //Create the line chart
                var salesChart = salesChart.Line(salesChartData, salesChartOptions);
                document.getElementById('sales-legend').innerHTML = salesChart.generateLegend();


                var pieChartCanvas = $("#doughnutChart").get(0).getContext("2d");
                var pieChart = new Chart(pieChartCanvas);
                var PieData = [
                    {
                        value: 700,
                        color: "#f56954",
                        highlight: "#f56954",
                        label: "Product 1"
                    },
                    {
                        value: 500,
                        color: "#00a65a",
                        highlight: "#00a65a",
                        label: "Product 2"
                    },
                    {
                        value: 400,
                        color: "#f39c12",
                        highlight: "#f39c12",
                        label: "Product 3"
                    },
                    {
                        value: 600,
                        color: "#00c0ef",
                        highlight: "#00c0ef",
                        label: "Product 4"
                    }
                ];
                var pieOptions = {
                    segmentShowStroke: true,
                    segmentStrokeColor: "#fff",
                    segmentStrokeWidth: 1,
                    percentageInnerCutout: 50,
                    animationSteps: 100,
                    animationEasing: "easeOutBounce",
                    animateRotate: true,
                    animateScale: false,
                    responsive: true,
                    maintainAspectRatio: false,
                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
                    tooltipTemplate: "<%=value %> <%=label%>"
                };
                pieChart.Doughnut(PieData, pieOptions);


                var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
                var pieChart = new Chart(pieChartCanvas);
                var PieData = [
                    {
                        value: 4500,
                        color: "#f56954",
                        highlight: "#f56954",
                        label: "North America"
                    },
                    {
                        value: 1520,
                        color: "#00a65a",
                        highlight: "#00a65a",
                        label: "South America"
                    },
                    {
                        value: 3650,
                        color: "#f39c12",
                        highlight: "#f39c12",
                        label: "Europe"
                    },
                    {
                        value: 680,
                        color: "#00c0ef",
                        highlight: "#00c0ef",
                        label: "Asia"
                    }
                ];
                var pieOptions = {
                    segmentShowStroke: true,
                    segmentStrokeColor: "#fff",
                    segmentStrokeWidth: 1,
                    percentageInnerCutout: 0,
                    animationSteps: 100,
                    animationEasing: "easeOutBounce",
                    animateRotate: true,
                    animateScale: false,
                    responsive: true,
                    maintainAspectRatio: false,
                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
                    tooltipTemplate: "<%=value %> <%=label%>"
                };
                pieChart.Pie(PieData, pieOptions);


                $('.sparkbar').each(function () {
                    var $this = $(this);
                    $this.sparkline('html', {
                        type: 'bar',
                        height: $this.data('height') ? $this.data('height') : '30',
                        barColor: $this.data('color')
                    });
                });

                $('.sparkpie').each(function () {
                    var $this = $(this);
                    $this.sparkline('html', {
                        type: 'pie',
                        height: $this.data('height') ? $this.data('height') : '90',
                        sliceColors: $this.data('color')
                    });
                });

                $('.sparkline').each(function () {
                    var $this = $(this);
                    $this.sparkline('html', {
                        type: 'line',
                        height: $this.data('height') ? $this.data('height') : '90',
                        width: '100%',
                        lineColor: $this.data('linecolor'),
                        fillColor: $this.data('fillcolor'),
                        spotColor: $this.data('spotcolor')
                    });
                });
            }

            $('#world-map-markers').vectorMap({
                map: 'world_mill_en',
                normalizeFunction: 'polynomial',
                hoverOpacity: 0.7,
                hoverColor: false,
                backgroundColor: 'transparent',
                regionStyle: {
                    initial: {
                        fill: 'rgba(210, 214, 222, 1)',
                        "fill-opacity": 1,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 1
                    },
                    hover: {
                        "fill-opacity": 0.7,
                        cursor: 'pointer'
                    },
                    selected: {
                        fill: 'yellow'
                    },
                    selectedHover: {}
                },
                markerStyle: {
                    initial: {
                        fill: '#00a65a',
                        stroke: '#111'
                    }
                },
                markers: [ //latlng at: http://www.findlatitudeandlongitude.com/
                    {latLng: [29, -95], name: '111 Milam St, Houston, TX, US'},
                    {latLng: [19, -98], name: 'Calle Cerezos 184, La Perla, 57820 Nezahualcoyotl, Mex'},
                    {latLng: [-10, -48], name: 'Q. 1106 Sul Avenida NS 10, 2-288 - Plano Diretor Sul, Palmas - TO, Brasil'},
                    {latLng: [48, 2.35], name: '11 Rue des Bernardins, 75005 Paris, France'},
                    {latLng: [50, 8.7], name: 'Wingertstraße 17, 60316 Frankfurt am Main, Germany'},
                    {latLng: [50, 4.3], name: 'Rue Goffart 77, 1050 Ixelles, Belgium'},
                    {latLng: [35, 139], name: '1 Chome-7-1 Nagatacho, Chiyoda-ku, Tokyo-to 100-0014, Japan'},
                ]
            });


        });

    </script>

@endsection