@extends('app')

@section('page-title')
    Tabs
@endsection

@section('page-css')
    <style>
        .tabs-container .tabs-left .tab-pane .panel-body,
        .tabs-container .tabs-right .tab-pane .panel-body {
            border: 1px solid #e7eaec;
        }
        .tabs-container .nav-tabs > li a:hover {
            background: transparent;
            border-color: transparent;
        }
        .tabs-container .tabs-below > .nav-tabs,
        .tabs-container .tabs-right > .nav-tabs,
        .tabs-container .tabs-left > .nav-tabs {
            border-bottom: 0;
        }
        .tabs-container .tabs-left .panel-body {
            position: static;
        }
        .tabs-container .tabs-left > .nav-tabs,
        .tabs-container .tabs-right > .nav-tabs {
            width: 20%;
        }
        .tabs-container .tabs-left .panel-body {
            width: 80%;
            margin-left: 20%;
        }
        .tabs-container .tabs-right .panel-body {
            width: 80%;
            margin-right: 20%;
        }
        .tabs-container .tab-content > .tab-pane,
        .tabs-container .pill-content > .pill-pane {
            display: none;
        }
        .tabs-container .tab-content > .active,
        .tabs-container .pill-content > .active {
            display: block;
        }
        .tabs-container .tabs-below > .nav-tabs {
            border-top: 1px solid #e7eaec;
        }
        .tabs-container .tabs-below > .nav-tabs > li {
            margin-top: -1px;
            margin-bottom: 0;
        }
        .tabs-container .tabs-below > .nav-tabs > li > a {

        }
        .tabs-container .tabs-below > .nav-tabs > li > a:hover,
        .tabs-container .tabs-below > .nav-tabs > li > a:focus {
            border-top-color: #e7eaec;
            border-bottom-color: transparent;
        }
        .tabs-container .tabs-left > .nav-tabs > li,
        .tabs-container .tabs-right > .nav-tabs > li {
            float: none;
        }
        .tabs-container .tabs-left > .nav-tabs > li > a,
        .tabs-container .tabs-right > .nav-tabs > li > a {
            min-width: 74px;
            margin-right: 0;
            margin-bottom: 3px;
        }
        .tabs-container .tabs-left > .nav-tabs {
            float: left;
            margin-right: 19px;
        }
        .tabs-container .tabs-left > .nav-tabs > li > a {
            margin-right: -1px;
        }
        .tabs-container .tabs-left > .nav-tabs .active > a,
        .tabs-container .tabs-left > .nav-tabs .active > a:hover,
        .tabs-container .tabs-left > .nav-tabs .active > a:focus {
            border-color: #e7eaec transparent #e7eaec #e7eaec;
            *border-right-color: #ffffff;
            border-left: 3px solid #d2d6de;
            z-index: 1;
        }
        .tabs-container .tabs-right > .nav-tabs {
            float: right;
            margin-left: 19px;
        }
        .tabs-container .tabs-right > .nav-tabs > li > a {
        }
        .tabs-container .tabs-right > .nav-tabs .active > a,
        .tabs-container .tabs-right > .nav-tabs .active > a:hover,
        .tabs-container .tabs-right > .nav-tabs .active > a:focus {
            border-color: #e7eaec #e7eaec #e7eaec transparent;
            *border-left-color: #ffffff;
            border-right: 3px solid #d2d6de;
            z-index: 1;
        }
        @media (max-width: 767px) {
            .tabs-container .nav-tabs > li {
                float: none !important;
            }
            .tabs-container .nav-tabs > li.active > a {
                border-bottom: 1px solid #e7eaec !important;
                margin: 0;
            }
        }
    </style>
@endsection
@section('content-header')
    <h1>
        Tabs
    </h1>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Bootstrap
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-6">
                    <h4 class="card-title">Normal Tabs</h4>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Profile</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item">Action</a>
                                <a class="dropdown-item">Another action</a>
                                <a class="dropdown-item">Something else here</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item">Separated link</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="#">Disabled</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
                            <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
                            <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable jean shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero sint qui sapiente accusamus tattooed echo park.</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <h4 class="card-title">Normal Tabs with Icons</h4>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home2" role="tab"><span class="fa fa-home"></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#profile2" role="tab"><span class="fa fa-user"></span></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"> </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item"><span class="fa fa-plane"></span></a>
                                <a class="dropdown-item"><span class="fa fa-gear"></span></a>
                                <a class="dropdown-item"><span class="fa fa-exclamation"></span></a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item"><span class="fa fa-circle"></span></a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="#">Disabled</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="home2">
                            <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="profile2">
                            <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable jean shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero sint qui sapiente accusamus tattooed echo park.</p>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <hr />
            <br />
            <div class="row">
                <div class="col-md-6">
                    <h4 class="card-title">Pill Tabs</h4>
                    <ul class="nav nav-pills" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home4" role="tab"><span class="fa fa-home"></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#profile4" role="tab"><span class="fa fa-user"></span></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"> </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item"><span class="fa fa-plane"></span></a>
                                <a class="dropdown-item"><span class="fa fa-gear"></span></a>
                                <a class="dropdown-item"><span class="fa fa-exclamation"></span></a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item"><span class="fa fa-circle"></span></a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="#">Disabled</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="home4">
                            <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="profile4">
                            <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable jean shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero sint qui sapiente accusamus tattooed echo park.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h4 class="card-title">Pills Stacked</h4>
                    <ul class="nav nav-pills nav-stacked" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home3" role="tab">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#profile3" role="tab">Profile</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item">Action</a>
                                <a class="dropdown-item">Another action</a>
                                <a class="dropdown-item">Something else here</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item">Separated link</a>
                            </div>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="home3" aria-labelledby="home-tab">
                            <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="profile3" aria-labelledby="profile-tab">
                            <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable jean shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero sint qui sapiente accusamus tattooed echo park.</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Vertical Tabs
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-6">
                    <h4 class="card-title">Pills Stacked - Vertical Left</h4>
                    <div class="col-sm-4">
                        <ul class="nav nav-pills nav-stacked" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#homeL" role="tab">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#profileL" role="tab">Profile</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#messageL" role="tab">Message</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#otherL" role="tab">Other</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-8">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="homeL" aria-labelledby="homeL-tab">
                                <p>Integer platea mauris porta! Sit, porta rhoncus, placerat quis platea. Nisi sed tristique cras pellentesque! Ultricies placerat a dolor turpis nec! Urna, mid lectus, rhoncus enim, egestas vel magnis facilisis.</p>

                                <p>A pid odio nec, pid, ac sed dapibus, nascetur diam lacus a nisi eros! Turpis nunc elit! Sed, egestas amet placerat quis elementum a phasellus! Pellentesque et integer ac.</p>

                                <p>Turpis porttitor ut etiam aenean odio mattis, arcu magnis nisi nunc, tristique vel ridiculus phasellus urna porttitor, massa risus nunc ut ultrices tempor. Etiam? Phasellus pid ac? Ut a.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="profileL" aria-labelledby="profileL-tab">
                                <p>Velit sit est facilisis penatibus eu nec, facilisis, a, ac cursus pellentesque. Non pid vut, massa, integer duis dapibus urna mauris, odio porttitor auctor, ac aliquam urna! Dignissim ac elementum.</p>

                                <p>Etiam scelerisque tortor enim, elementum, facilisis velit lacus cras, integer adipiscing. Elementum ac, magna sed, massa lacus nisi in! Risus augue mus parturient. Cum turpis egestas sociis, risus platea.</p>

                                <p>Nunc enim et est sit risus a, ut parturient sociis in. Lectus augue? Natoque urna velit purus tristique lacus et sagittis auctor pulvinar? Pulvinar porttitor pulvinar nec in tincidunt.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="messageL" aria-labelledby="messageL-tab">
                                <p>Mattis et lundium in, augue diam ultrices ultricies eros sagittis? Ac integer elementum duis in montes enim, ac proin ac dignissim cum cum nunc, adipiscing non odio ac risus, lundium, natoque adipiscing elementum sit sed? Porttitor. Non, rhoncus parturient massa arcu natoque lundium integer? In proin auctor sed duis. Pellentesque.</p>

                                <p>Eros cum dignissim? Rhoncus, dolor. Vut vel et ac? Porta magna facilisis nec ac ac. Natoque a massa elit rhoncus risus. Nascetur placerat urna, non aliquam hac pulvinar augue? Sit odio pulvinar aliquam porttitor dapibus in proin aenean a, cursus, cum purus mus sed, pid adipiscing natoque dolor ac.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="otherL" aria-labelledby="otherL-tab">
                                <p>Porttitor pid lectus etiam habitasse placerat adipiscing mauris pulvinar, ac ridiculus integer integer nec quis? A? Mus, quis diam vel aliquet lorem. Phasellus ultricies sit arcu. Urna! Facilisis habitasse et augue, pulvinar augue amet montes lorem mus mattis lorem enim in purus, lacus quis? Ultricies lectus est egestas nunc ultricies.</p>

                                <p>Nunc natoque cras dignissim aenean? Sed, nunc nunc ridiculus! Augue mauris sagittis montes, amet odio sit aliquam nascetur. Cursus nec nascetur. Porta cursus diam nunc, lacus placerat porttitor a magna sit vel in nisi, non urna facilisis tincidunt aliquam facilisis mus odio turpis nunc aenean, hac sed? Odio, est.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h4 class="card-title">Pills Stacked - Vertical Right</h4>
                    <div class="col-sm-8">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="homeR" aria-labelledby="homeR-tab">
                                <p>Integer platea mauris porta! Sit, porta rhoncus, placerat quis platea. Nisi sed tristique cras pellentesque! Ultricies placerat a dolor turpis nec! Urna, mid lectus, rhoncus enim, egestas vel magnis facilisis.</p>

                                <p>A pid odio nec, pid, ac sed dapibus, nascetur diam lacus a nisi eros! Turpis nunc elit! Sed, egestas amet placerat quis elementum a phasellus! Pellentesque et integer ac.</p>

                                <p>Turpis porttitor ut etiam aenean odio mattis, arcu magnis nisi nunc, tristique vel ridiculus phasellus urna porttitor, massa risus nunc ut ultrices tempor. Etiam? Phasellus pid ac? Ut a.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="profileR" aria-labelledby="profileR-tab">
                                <p>Velit sit est facilisis penatibus eu nec, facilisis, a, ac cursus pellentesque. Non pid vut, massa, integer duis dapibus urna mauris, odio porttitor auctor, ac aliquam urna! Dignissim ac elementum.</p>

                                <p>Etiam scelerisque tortor enim, elementum, facilisis velit lacus cras, integer adipiscing. Elementum ac, magna sed, massa lacus nisi in! Risus augue mus parturient. Cum turpis egestas sociis, risus platea.</p>

                                <p>Nunc enim et est sit risus a, ut parturient sociis in. Lectus augue? Natoque urna velit purus tristique lacus et sagittis auctor pulvinar? Pulvinar porttitor pulvinar nec in tincidunt.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="messageR" aria-labelledby="messageR-tab">
                                <p>Mattis et lundium in, augue diam ultrices ultricies eros sagittis? Ac integer elementum duis in montes enim, ac proin ac dignissim cum cum nunc, adipiscing non odio ac risus, lundium, natoque adipiscing elementum sit sed? Porttitor. Non, rhoncus parturient massa arcu natoque lundium integer? In proin auctor sed duis. Pellentesque.</p>

                                <p>Eros cum dignissim? Rhoncus, dolor. Vut vel et ac? Porta magna facilisis nec ac ac. Natoque a massa elit rhoncus risus. Nascetur placerat urna, non aliquam hac pulvinar augue? Sit odio pulvinar aliquam porttitor dapibus in proin aenean a, cursus, cum purus mus sed, pid adipiscing natoque dolor ac.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="otherR" aria-labelledby="otherR-tab">
                                <p>Porttitor pid lectus etiam habitasse placerat adipiscing mauris pulvinar, ac ridiculus integer integer nec quis? A? Mus, quis diam vel aliquet lorem. Phasellus ultricies sit arcu. Urna! Facilisis habitasse et augue, pulvinar augue amet montes lorem mus mattis lorem enim in purus, lacus quis? Ultricies lectus est egestas nunc ultricies.</p>

                                <p>Nunc natoque cras dignissim aenean? Sed, nunc nunc ridiculus! Augue mauris sagittis montes, amet odio sit aliquam nascetur. Cursus nec nascetur. Porta cursus diam nunc, lacus placerat porttitor a magna sit vel in nisi, non urna facilisis tincidunt aliquam facilisis mus odio turpis nunc aenean, hac sed? Odio, est.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <ul class="nav nav-pills nav-stacked" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#homeR" role="tab">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#profileR" role="tab">Profile</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#messageR" role="tab">Message</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#otherR" role="tab">Other</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script>
        $(function(){
            $('#scroll-200').slimScroll({
                height: '200px'
            });
        });
    </script>
@endsection