@extends('app')

@section('page-title')
    User Profile
@endsection

@section('page-css')

@endsection

@section('content-header')
    <h1>
        User Profile
    </h1>
@endsection

@section('content')

    <div class="jumbotron instagram p10">
        <div class="row">
            <div class="col-md-2 text-md-center">
                <img class="mv10 rounded" src="{{asset('img/avatars/user1.jpg')}}" alt="User Image">
                <div class="btn-group mv5">
                    <label class="btn btn-secondary btn-sm btn-circle" data-toggle="tooltip" data-placement="bottom" title="Email">
                        <a href="#"><span class="fa fa-envelope"></span></a>
                    </label>
                    <label class="btn btn-secondary btn-sm btn-circle" data-toggle="tooltip" data-placement="bottom" title="Address">
                        <a href="#" data-toggle="modal" data-target="#modalContact"><span class="fa fa-map-marker"></span></a>
                    </label>
                    <label class="btn btn-secondary btn-sm btn-circle" data-toggle="tooltip" data-placement="bottom" title="Phone">
                        <a href="tel:+552123456789"><span class="fa fa-phone"></span></a>
                    </label>
                </div>
            </div>
            <div class="col-md-10">
                <h2>Checkmate Digital <span class="pull-md-right fs18">Profile</span></h2>
                <blockquote class="blockquote fs14">
                    <p>Penatibus, duis! Ridiculus platea, augue in urna, nunc ultricies. Adipiscing aenean, pellentesque turpis ac magna dignissim parturient cras odio. Massa sit amet et, in, sociis, ac magnis adipiscing! Tortor! Velit porta magna tempor sagittis egestas. Diam phasellus parturient vel, cras porttitor nunc dapibus? Penatibus ut? Pellentesque phasellus non mattis.  ;)</p>
                </blockquote>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" data-src="{{asset('img/photo4.jpg')}}" style="height: 180px; width: 100%; display: block;" src="{{asset('img/photo4.jpg')}}" alt="Card image cap">
                <div class="card-header">
                    About
                </div>
                <div class="card-block">
                    <strong><i class="fa fa-book margin-r-5"></i>  Education</strong>
                    <p class="text-muted">
                        Computer Science course at Harvard
                    </p>

                    <hr>

                    <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                    <p class="text-muted">New York</p>

                    <hr>

                    <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
                    <p>
                        <span class="label label-default">UI Design</span>
                        <span class="label label-primary">Laravel</span>
                        <span class="label label-success">Coding</span>
                        <span class="label label-info">Javascript</span>
                        <span class="label label-warning">PHP</span>
                        <span class="label label-danger">Jquery</span>
                    </p>

                    <hr>

                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
                    <p>Magna lacus nisi parturient tempor vel augue urna velit. Tincidunt non dolor turpis! Hac natoque cursus a lorem magna, cursus et.</p>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-block">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#activity" role="tab">Activity</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#timeline" role="tab">Timeline</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#media" role="tab">Media</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Settings</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane p10 active" id="activity" role="tabpanel">
                            <div class="media">
                                <a class="pull-sm-left" href="#"> <img class="media-object mn thumbnail rounded mw50" src="{{asset('img/avatars/user1.jpg')}}" alt="..."> </a>
                                <div class="media-body">
                                    <h5 class="media-heading ml10 mb20">Checkmate Digital Posted
                                        <small> - 3 hours ago</small>
                                    </h5>
                                    <img src="{{asset('img/photo1.png')}}" class="mw140 mr25 mb20"><img src="{{asset('img/photo2.png')}}" class="mw140 mr25 mb20"> <img src="{{asset('img/photo3.jpg')}}" class="mw140 mb20">
                                    <div class="media-links">
                          <span class="text-light fs12 mr10">
                            <span class="fa fa-thumbs-o-up text-primary mr5"></span> Like </span>
                          <span class="text-light fs12 mr10">
                            <span class="fa fa-share text-primary mr5"></span> Share </span>
                          
                          <span class="text-light fs12 mr10">
                            <span class="fa fa-comment text-primary mr5"></span> Comment </span>
                                    </div>
                                </div>
                            </div>
                            <div class="media mt25">
                                <a class="pull-sm-left" href="#"> <img class="media-object mn thumbnail thumbnail-sm rounded mw40" src="{{asset('img/avatars/user1.jpg')}}" alt="..."> </a>
                                <div class="media-body mb5">
                                    <h5 class="media-heading ml10 mbn">Checkmate Digital Posted
                                        <small> - 3 hours ago</small>
                                    </h5>
                                    <blockquote class="blockquote fs14"> Omg so freaking sweet dude.</blockquote>
                                    <div class="media pb10">
                                        <a class="pull-sm-left" href="#"> <img class="media-object mn thumbnail thumbnail-sm rounded mw40" src="{{asset('img/avatars/user6.jpg')}}" alt="..."> </a>
                                        <div class="media-body mb5">
                                            <h5 class="media-heading ml10 mbn">Sarah Rose
                                                <small> - 3 hours ago</small>
                                            </h5>
                                            <blockquote class="blockquote fs14">Omgosh I'm in love</blockquote>
                                        </div>
                                    </div>
                                    <div class="media mtn">
                                        <a class="pull-sm-left" href="#"> <img class="media-object mn thumbnail thumbnail-sm rounded mw40" src="{{asset('img/avatars/user5.jpg')}}" alt="..."> </a>
                                        <div class="media-body mb5">
                                            <h5 class="media-heading ml10 mbn">Alex Moon
                                                <small> - 3 hours ago</small>
                                            </h5>
                                            <blockquote class="blockquote fs14">Omgosh I'm in love</blockquote>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="media mt25">
                                <a class="pull-sm-left" href="#"> <img class="media-object thumbnail rounded mw50" src="{{asset('img/avatars/user1.jpg')}}" alt="..."> </a>
                                <div class="media-body">
                                    <h5 class="media-heading ml10 mb20">Checkmate Digital Posted
                                        <small> - 3 hours ago</small>
                                    </h5>
                                    <img src="{{asset('img/front-end/portfolio/1.jpg')}}" class="mw140 mr25 mb20"><img src="{{asset('img/front-end/portfolio/2.jpg')}}" class="mw140 mr25 mb20"> <img src="{{asset('img/front-end/portfolio/3.jpg')}}" class="mw140 mb20">
                                    <div class="media-links">
                          <span class="text-light fs12 mr10">
                            <span class="fa fa-thumbs-o-up text-primary mr5"></span> Like </span>
                          <span class="text-light fs12 mr10">
                            <span class="fa fa-share text-primary mr5"></span> Share </span>
                          
                          <span class="text-light fs12 mr10">
                            <span class="fa fa-comment text-primary mr5"></span> Comment </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane p10" id="timeline" role="tabpanel">
                            <div id="timeline" class="mt30">
                                <div class="timeline-divider mtn">
                                    <div class="divider-label">2016</div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6 left-column">
                                        <div class="timeline-item">
                                            <div class="timeline-icon">
                                                <span class="glyphicon glyphicon-user text-primary"></span>
                                            </div>
                                            <div class="card">
                                                <div class="card-header">
                    <span class="card-title">
                      <span class="glyphicon glyphicon-comment"></span> User Posting </span>
                                                    <div class="card-header-menu pull-sm-right mr10 text-muted fs12"> {{date('F')}} {{date('d')}}, {{date('Y')}} </div>
                                                </div>
                                                <div class="card-block">
                                                    <div class="media">
                                                        <a class="pull-sm-left" href="#"> <img class="media-object thumbnail rounded mw50" src="{{asset('img/avatars/user1.jpg')}}" alt="..."> </a>
                                                        <div class="media-body">
                                                            <h5 class="media-heading mb20">Checkmate Digital Posted
                                                                <small> - 3 hours ago</small>
                                                            </h5>
                                                            <img src="{{asset('img/photo1.png')}}" class="mw140 mr25 mb20" /> <img src="{{asset('img/photo2.png')}}" class="mw140 mb20" />
                                                            <div class="media-links">
                          <span class="text-light fs12 mr10">
                            <span class="fa fa-thumbs-o-up text-primary mr5"></span> Like </span>
                          <span class="text-light fs12 mr10">
                            <span class="fa fa-share text-primary mr5"></span> Share </span>
                          <span class="text-light fs12 mr10">
                            <span class="glyphicon glyphicon-floppy-save text-primary mr5"></span> Save </span>
                          <span class="text-light fs12 mr10">
                            <span class="fa fa-comments text-primary mr5"></span> Comment </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="media mt20">
                                                        <a class="pull-sm-left" href="#"> <img class="media-object thumbnail thumbnail-sm rounded mw40" src="{{asset('img/avatars/user6.jpg')}}" alt="..."> </a>
                                                        <div class="media-body mb5">
                                                            <h5 class="media-heading mbn">Sarah Rose
                                                                <small> - 3 hours ago</small>
                                                            </h5>
                                                            <p> Omg so freaking sweet dude.</p>
                                                        </div>
                                                    </div>
                                                    <div class="media mt15">
                                                        <a class="pull-sm-left" href="#"> <img class="media-object thumbnail thumbnail-sm rounded mw40" src="{{asset('img/avatars/user5.jpg')}}" alt="..."> </a>
                                                        <div class="media-body mb5">
                                                            <h5 class="media-heading mbn">Alex Moon
                                                                <small> - 3 hours ago</small>
                                                            </h5>
                                                            <p>Omgosh I'm in love</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-footer p15">
                                                    <div class="admin-form">
                                                        <label for="reply1" class="field prepend-icon">
                                                            <input type="text" name="reply1" id="reply1" class="event-name gui-input" placeholder="Respond with a comment.">
                                                            <label for="reply1" class="field-icon">
                                                                <i class="fa fa-pencil"></i>
                                                            </label>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="timeline-item">
                                            <div class="timeline-icon">
                                                <span class="fa fa-video-camera text-primary"></span>
                                            </div>
                                            <div class="card">
                                                <div class="card-header">
                    <span class="card-title">
                      <span class="glyphicon glyphicon-facetime-video"></span> Timeline Video! </span>
                                                    <div class="card-header-menu pull-sm-right mr10 text-muted fs12"> January 27, 2016 </div>
                                                </div>
                                                <div class="card-block">
                                                    <div class="embed-responsive embed-responsive-16by9">
                                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/eX_iASz1Si8" frameborder="0" allowfullscreen=""></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 right-column">
                                        <div class="timeline-item">
                                            <div class="timeline-icon">
                                                <span class="fa fa-picture-o text-info"></span>
                                            </div>
                                            <div class="card">
                                                <div class="card-block">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <a class="gallery-item" href="{{asset('img/photo4.jpg')}}"><img src="{{asset('img/photo4.jpg')}}" class="img-responsive" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <a class="gallery-item" href="{{asset('img/photo5.jpg')}}"><img src="{{asset('img/photo5.jpg')}}" class="img-responsive" alt="">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="timeline-item">
                                            <div class="timeline-icon">
                                                <span class="fa fa-compass text-success"></span>
                                            </div>
                                            <div class="card">
                                                <div class="card-header">
                    <span class="card-title">
                      <span class="glyphicon glyphicon-camera"></span> Map Posting</span>
                                                    <div class="card-header-menu pull-sm-right mr10 text-muted fs12"> February 2, 2016 </div>
                                                </div>
                                                <div class="card-block">
                                                    <iframe width="100%" height="275" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=Rio%20de%20Janeiro&amp;key=AIzaSyBqf9cjsPAh3rGJPKLCsYL2IeAveGhGJTw&amp;zoom=14"></iframe>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="timeline-item">
                                            <div class="timeline-icon">
                                                <span class="fa fa-paperclip text-danger"></span>
                                            </div>
                                            <div class="card">
                                                <div class="card-block p10">
                                                    <blockquote class="mbn ml10">
                                                        <p>Penatibus, duis! Ridiculus platea, augue in urna, nunc ultricies. Adipiscing aenean, pellentesque turpis ac magna dignissim parturient cras odio. Massa sit amet et, in, sociis, ac magnis adipiscing! Tortor! Velit porta magna tempor sagittis egestas. Diam phasellus parturient vel, cras porttitor nunc dapibus? Penatibus ut? Pellentesque phasellus non mattis. ;)</p>
                                                        <small>Checkmate Digital</small>
                                                    </blockquote>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-divider">
                                    <div class="divider-label">2015</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalContact" tabindex="-1" role="dialog" aria-labelledby="modalContact">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d235190.9986726896!2d-43.183683466015616!3d-22.918557911027275!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spt-BR!2sbr!4v1458066791334" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>

@endsection

@section('page-scripts')
    <script type="text/javascript">
        $(function() {

            "use strict";


        });
    </script>
@endsection