<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{route('home')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><span class="symbol">&infin;</span></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">Infinity <span class="symbol">&infin;</span></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <a href="#" class="fullscreen-toggle hidden-sm-down" data-toggle="fullscreen" role="button">
            <i class="ion-arrow-expand"></i>
        </a>

        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="nav-item">
                    <a href="#" class="nav-link" data-toggle="dropdown">
                        Link
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link" data-toggle="dropdown">
                        Link
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link" data-toggle="dropdown">
                        Link
                    </a>
                </li>



                @if($user = Sentinel::check())
                    <li class="nav-item dropdown user user-menu">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                            <img src="{{asset('img/avatars/'.$user['avatar'])}}" class="user-image" alt="User Image">
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{asset('img/avatars/'.$user['avatar'])}}" alt="User Image">
                                <p>Hello {{$user['first_name']}}</p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="row">
                                    {{$user['first_name']}} {{$user['last_name']}}
                                </div>
                                <!-- /.row -->
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{route('checkauth.profile.show')}}" class="btn btn-secondary btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{route('checkauth.logout')}}" class="btn btn-secondary btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    @endif
                            <!-- Control Sidebar Toggle Button -->
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="control-sidebar"><i class="fa fa-gear"></i></a>
                    </li>
            </ul>
        </div>

    </nav>
</header><!-- /header -->