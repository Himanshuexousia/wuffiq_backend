<!-- Javascript -->
<script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('vendor/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{asset('vendor/moment/js/moment.min.js')}}"></script>
<script src="{{ asset('vendor/tether/js/tether.min.js') }}"></script>
<script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('vendor/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{asset('vendor/toastr/toastr.min.js')}}"></script>
<script src="{{asset('vendor/jq-fullscreen/jquery.fullscreen.min.js')}}"></script>
<script src="{{ asset('vendor/fastclick/fastclick.js') }}"></script>
<script src="{{ asset('vendor/css-element-queries/ResizeSensor.js') }}"></script>
<script src="{{ asset('vendor/css-element-queries/ElementQueries.js') }}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="{{ asset('js/control-sidebar.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/timer.js') }}"></script>