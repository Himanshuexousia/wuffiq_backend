<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-md-right hidden-xs">
        <strong>Copyright &copy; {{date("Y")}} <a href="{{config('app.url')}}">Your company</a>.</strong>
    </div>
    <!-- Default to the left -->
    <a href="#"><b>your link</b></a>. Your text here
</footer>