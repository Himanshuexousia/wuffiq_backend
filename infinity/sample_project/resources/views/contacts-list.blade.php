@extends('app')

@section('page-title')
    Contacts
@endsection

@section('page-css')
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/icheck/skins/flat/blue.css')}}" />
@endsection

@section('content-header')
    <h1>
        Contacts
    </h1>
@endsection

@section('content')

    <ul class="users-list clearfix">
        <li>
            <div class="card">
                <div class="card-block">
                    <img src="{{asset('img/avatars/user1.jpg')}}" alt="User Image">
                    <br />
                    <a class="lead center-block" href="#">Ronald</a>
                    <div class="btn-group">
                        <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Email">
                            <a href="mailto:dev@checkmatedigital.com"><span class="fa fa-envelope"></span></a>
                        </label>
                        <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Address">
                            <a href="#" data-toggle="modal" data-target="#modalContact"><span class="fa fa-map-marker"></span></a>
                        </label>
                        <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Phone">
                            <a href="tel:+552123456789"><span class="fa fa-phone"></span></a>
                        </label>
                        <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Profile">
                            <a href="{{route('page-user-profile')}}"><span class="fa fa-user"></span></a>
                        </label>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="card">
                <div class="card-block">
                    <img src="{{asset('img/avatars/user2.jpg')}}" alt="User Image">
                    <br />
                    <a class="lead center-block" href="#">Norman</a>
                    <div class="btn-group">
                        <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Email">
                            <a href="mailto:dev@checkmatedigital.com"><span class="fa fa-envelope"></span></a>
                        </label>
                        <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Address">
                            <a href="#" data-toggle="modal" data-target="#modalContact"><span class="fa fa-map-marker"></span></a>
                        </label>
                        <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Phone">
                            <a href="tel:+552123456789"><span class="fa fa-phone"></span></a>
                        </label>
                        <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Profile">
                            <a href="{{route('page-user-profile')}}"><span class="fa fa-user"></span></a>
                        </label>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="card">
                <div class="card-block">
            <img src="{{asset('img/avatars/user3.jpg')}}" alt="User Image">
            <br />
            <a class="lead center-block" href="#">Dave</a>
            <div class="btn-group">
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Email">
                    <a href="mailto:dev@checkmatedigital.com"><span class="fa fa-envelope"></span></a>
                </label>
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Address">
                    <a href="#" data-toggle="modal" data-target="#modalContact"><span class="fa fa-map-marker"></span></a>
                </label>
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Phone">
                    <a href="tel:+552123456789"><span class="fa fa-phone"></span></a>
                </label>
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Profile">
                    <a href="{{route('page-user-profile')}}"><span class="fa fa-user"></span></a>
                </label>
            </div>
            </div>
            </div>
        </li>
        <li>
            <div class="card">
                <div class="card-block">
            <img src="{{asset('img/avatars/user4.jpg')}}" alt="User Image">
            <br />
            <a class="lead center-block" href="#">Jack</a>
            <div class="btn-group">
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Email">
                    <a href="mailto:dev@checkmatedigital.com"><span class="fa fa-envelope"></span></a>
                </label>
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Address">
                    <a href="#" data-toggle="modal" data-target="#modalContact"><span class="fa fa-map-marker"></span></a>
                </label>
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Phone">
                    <a href="tel:+552123456789"><span class="fa fa-phone"></span></a>
                </label>
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Profile">
                    <a href="{{route('page-user-profile')}}"><span class="fa fa-user"></span></a>
                </label>
            </div>
            </div>
            </div>
        </li>
        <li>
            <div class="card">
                <div class="card-block">
            <img src="{{asset('img/avatars/user5.jpg')}}" alt="User Image">
            <br />
            <a class="lead center-block" href="#">Sarah</a>
            <div class="btn-group">
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Email">
                    <a href="mailto:dev@checkmatedigital.com"><span class="fa fa-envelope"></span></a>
                </label>
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Address">
                    <a href="#" data-toggle="modal" data-target="#modalContact"><span class="fa fa-map-marker"></span></a>
                </label>
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Phone">
                    <a href="tel:+552123456789"><span class="fa fa-phone"></span></a>
                </label>
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Profile">
                    <a href="{{route('page-user-profile')}}"><span class="fa fa-user"></span></a>
                </label>
            </div>
            </div>
            </div>
        </li>
        <li>
            <div class="card">
                <div class="card-block">
            <img src="{{asset('img/avatars/user6.jpg')}}" alt="User Image">
            <br />
            <a class="lead center-block" href="#">Liv</a>
            <div class="btn-group">
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Email">
                    <a href="mailto:dev@checkmatedigital.com"><span class="fa fa-envelope"></span></a>
                </label>
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Address">
                    <a href="#" data-toggle="modal" data-target="#modalContact"><span class="fa fa-map-marker"></span></a>
                </label>
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Phone">
                    <a href="tel:+552123456789"><span class="fa fa-phone"></span></a>
                </label>
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Profile">
                    <a href="{{route('page-user-profile')}}"><span class="fa fa-user"></span></a>
                </label>
            </div>
            </div>
            </div>
        </li>
        <li>
            <div class="card">
                <div class="card-block">
            <img src="{{asset('img/avatars/user7.jpg')}}" alt="User Image">
            <br />
            <a class="lead center-block" href="#">Maya</a>
            <div class="btn-group">
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Email">
                    <a href="mailto:dev@checkmatedigital.com"><span class="fa fa-envelope"></span></a>
                </label>
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Address">
                    <a href="#" data-toggle="modal" data-target="#modalContact"><span class="fa fa-map-marker"></span></a>
                </label>
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Phone">
                    <a href="tel:+552123456789"><span class="fa fa-phone"></span></a>
                </label>
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Profile">
                    <a href="{{route('page-user-profile')}}"><span class="fa fa-user"></span></a>
                </label>
            </div>
            </div>
            </div>
        </li>
        <li>
            <div class="card">
                <div class="card-block">
            <img src="{{asset('img/avatars/user8.jpg')}}" alt="User Image">
            <br />
            <a class="lead center-block" href="#">Debra</a>
            <div class="btn-group">
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Email">
                    <a href="mailto:dev@checkmatedigital.com"><span class="fa fa-envelope"></span></a>
                </label>
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Address">
                    <a href="#" data-toggle="modal" data-target="#modalContact"><span class="fa fa-map-marker"></span></a>
                </label>
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Phone">
                    <a href="tel:+552123456789"><span class="fa fa-phone"></span></a>
                </label>
                <label class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Profile">
                    <a href="{{route('page-user-profile')}}"><span class="fa fa-user"></span></a>
                </label>
            </div>
            </div>
            </div>
        </li>
    </ul>

    <div class="modal fade" id="modalContact" tabindex="-1" role="dialog" aria-labelledby="modalContact">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d235190.9986726896!2d-43.183683466015616!3d-22.918557911027275!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spt-BR!2sbr!4v1458066791334" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>

@endsection

@section('page-scripts')
    <script src="{{asset('vendor/icheck/icheck.js')}}"></script>
    <script type="text/javascript">
        $(function() {

            "use strict";

            $('input[type=checkbox]').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

        });
    </script>
@endsection