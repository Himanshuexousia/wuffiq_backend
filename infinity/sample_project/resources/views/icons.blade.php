@extends('app')

@section('page-title')
    Icons
@endsection

@section('page-css')
    <style>
        .gly-2 {
            font-size: 2em;
        }
        .gly-3 {
            font-size: 3em;
        }

    </style>
@endsection
@section('content-header')
    <h1>
        Icons
    </h1>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Glyphicons - Not supported in version 4 of Bootstrap, but included in the project manually.
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="boostrap-icons text-md-center">
                <div class="row">
                    <div class="col-md-4">
                        <ul  class="list-unstyled">
                            <li><span class="glyphicon glyphicon-asterisk gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-asterisk</code></li>
                            <li> <span class="glyphicon glyphicon-plus gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-plus</code></li>
                            <li> <span class="glyphicon glyphicon-euro gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-euro</code></li>
                            <li> <span class="glyphicon glyphicon-eur gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-eur</code></li>
                            <li> <span class="glyphicon glyphicon-minus gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-minus</code></li>
                            <li> <span class="glyphicon glyphicon-cloud gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-cloud</code></li>
                            <li> <span class="glyphicon glyphicon-envelope gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-envelope</code></li>
                            <li> <span class="glyphicon glyphicon-pencil gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-pencil</code></li>
                            <li> <span class="glyphicon glyphicon-glass gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-glass</code></li>
                            <li> <span class="glyphicon glyphicon-music gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-music</code></li>
                            <li> <span class="glyphicon glyphicon-search gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-search</code></li>
                            <li> <span class="glyphicon glyphicon-heart gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-heart</code></li>
                            <li> <span class="glyphicon glyphicon-star gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-star</code></li>
                            <li> <span class="glyphicon glyphicon-star-empty gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-star-empty</code></li>
                            <li> <span class="glyphicon glyphicon-user gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-user</code></li>
                            <li> <span class="glyphicon glyphicon-film gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-film</code></li>
                            <li> <span class="glyphicon glyphicon-th-large gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-th-large</code></li>
                            <li> <span class="glyphicon glyphicon-th gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-th</code></li>
                            <li> <span class="glyphicon glyphicon-th-list gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-th-list</code></li>
                            <li> <span class="glyphicon glyphicon-ok gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-ok</code></li>
                            <li> <span class="glyphicon glyphicon-remove gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-remove</code></li>
                            <li> <span class="glyphicon glyphicon-zoom-in gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-zoom-in</code></li>
                            <li> <span class="glyphicon glyphicon-zoom-out gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-zoom-out</code></li>
                            <li> <span class="glyphicon glyphicon-off gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-off</code></li>
                            <li> <span class="glyphicon glyphicon-signal gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-signal</code></li>
                            <li> <span class="glyphicon glyphicon-cog gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-cog</code></li>
                            <li> <span class="glyphicon glyphicon-trash gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-trash</code></li>
                            <li> <span class="glyphicon glyphicon-home gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-home</code></li>
                            <li> <span class="glyphicon glyphicon-file gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-file</code></li>
                            <li> <span class="glyphicon glyphicon-time gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-time</code></li>
                            <li> <span class="glyphicon glyphicon-road gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-road</code></li>
                            <li> <span class="glyphicon glyphicon-download-alt gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-download-alt</code></li>
                            <li> <span class="glyphicon glyphicon-download gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-download</code></li>
                            <li> <span class="glyphicon glyphicon-upload gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-upload</code></li>
                            <li> <span class="glyphicon glyphicon-inbox gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-inbox</code></li>
                            <li> <span class="glyphicon glyphicon-play-circle gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-play-circle</code></li>
                            <li> <span class="glyphicon glyphicon-repeat gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-repeat</code></li>
                            <li> <span class="glyphicon glyphicon-refresh gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-refresh</code></li>
                            <li> <span class="glyphicon glyphicon-list-alt gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-list-alt</code></li>
                            <li> <span class="glyphicon glyphicon-lock gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-lock</code></li>
                            <li> <span class="glyphicon glyphicon-flag gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-flag</code></li>
                            <li> <span class="glyphicon glyphicon-headphones gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-headphones</code></li>
                            <li> <span class="glyphicon glyphicon-volume-off gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-volume-off</code></li>
                            <li> <span class="glyphicon glyphicon-volume-down gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-volume-down</code></li>
                            <li> <span class="glyphicon glyphicon-volume-up gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-volume-up</code></li>
                            <li> <span class="glyphicon glyphicon-qrcode gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-qrcode</code></li>
                            <li> <span class="glyphicon glyphicon-barcode gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-barcode</code></li>
                            <li> <span class="glyphicon glyphicon-tag gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-tag</code></li>
                            <li> <span class="glyphicon glyphicon-tags gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-tags</code></li>
                            <li> <span class="glyphicon glyphicon-book gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-book</code></li>
                            <li> <span class="glyphicon glyphicon-bookmark gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-bookmark</code></li>
                            <li> <span class="glyphicon glyphicon-print gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-print</code></li>
                            <li> <span class="glyphicon glyphicon-camera gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-camera</code></li>
                            <li> <span class="glyphicon glyphicon-font gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-font</code></li>
                            <li> <span class="glyphicon glyphicon-bold gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-bold</code></li>
                            <li> <span class="glyphicon glyphicon-italic gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-italic</code></li>
                            <li> <span class="glyphicon glyphicon-text-height gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-text-height</code></li>
                            <li> <span class="glyphicon glyphicon-text-width gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-text-width</code></li>
                            <li> <span class="glyphicon glyphicon-align-left gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-align-left</code></li>
                            <li> <span class="glyphicon glyphicon-align-center gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-align-center</code></li>
                            <li> <span class="glyphicon glyphicon-align-right gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-align-right</code></li>
                            <li> <span class="glyphicon glyphicon-align-justify gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-align-justify</code></li>
                            <li> <span class="glyphicon glyphicon-list gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-list</code></li>
                            <li> <span class="glyphicon glyphicon-indent-left gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-indent-left</code></li>
                            <li> <span class="glyphicon glyphicon-indent-right gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-indent-right</code></li>
                            <li> <span class="glyphicon glyphicon-facetime-video gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-facetime-video</code></li>
                            <li> <span class="glyphicon glyphicon-picture gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-picture</code></li>
                            <li> <span class="glyphicon glyphicon-map-marker gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-map-marker</code></li>
                            <li> <span class="glyphicon glyphicon-adjust gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-adjust</code></li>
                            <li> <span class="glyphicon glyphicon-tint gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-tint</code></li>
                            <li> <span class="glyphicon glyphicon-edit gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-edit</code></li>
                            <li> <span class="glyphicon glyphicon-share gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-share</code></li>
                            <li> <span class="glyphicon glyphicon-check gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-check</code></li>
                            <li> <span class="glyphicon glyphicon-move gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-move</code></li>
                            <li> <span class="glyphicon glyphicon-step-backward gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-step-backward</code></li>
                            <li> <span class="glyphicon glyphicon-fast-backward gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-fast-backward</code></li>
                            <li> <span class="glyphicon glyphicon-backward gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-backward</code></li>
                            <li> <span class="glyphicon glyphicon-play gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-play</code></li>
                            <li> <span class="glyphicon glyphicon-pause gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-pause</code></li>
                            <li> <span class="glyphicon glyphicon-stop gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-stop</code></li>
                            <li> <span class="glyphicon glyphicon-forward gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-forward</code></li>
                            <li> <span class="glyphicon glyphicon-fast-forward gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-fast-forward</code></li>
                            <li> <span class="glyphicon glyphicon-step-forward gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-step-forward</code></li>
                            <li> <span class="glyphicon glyphicon-eject gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-eject</code></li>
                            <li> <span class="glyphicon glyphicon-chevron-left gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-chevron-left</code></li>
                            <li> <span class="glyphicon glyphicon-chevron-right gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-chevron-right</code></li>
                            <li> <span class="glyphicon glyphicon-plus-sign gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-plus-sign</code></li>
                            <li> <span class="glyphicon glyphicon-minus-sign gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-minus-sign</code></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul  class="list-unstyled">
                            <li> <span class="glyphicon glyphicon-remove-sign gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-remove-sign</code></li>
                            <li> <span class="glyphicon glyphicon-ok-sign gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-ok-sign</code></li>
                            <li> <span class="glyphicon glyphicon-question-sign gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-question-sign</code></li>
                            <li> <span class="glyphicon glyphicon-info-sign gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-info-sign</code></li>
                            <li> <span class="glyphicon glyphicon-screenshot gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-screenshot</code></li>
                            <li> <span class="glyphicon glyphicon-remove-circle gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-remove-circle</code></li>
                            <li> <span class="glyphicon glyphicon-ok-circle gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-ok-circle</code></li>
                            <li> <span class="glyphicon glyphicon-ban-circle gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-ban-circle</code></li>
                            <li> <span class="glyphicon glyphicon-arrow-left gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-arrow-left</code></li>
                            <li> <span class="glyphicon glyphicon-arrow-right gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-arrow-right</code></li>
                            <li> <span class="glyphicon glyphicon-arrow-up gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-arrow-up</code></li>
                            <li> <span class="glyphicon glyphicon-arrow-down gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-arrow-down</code></li>
                            <li> <span class="glyphicon glyphicon-share-alt gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-share-alt</code></li>
                            <li> <span class="glyphicon glyphicon-resize-full gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-resize-full</code></li>
                            <li> <span class="glyphicon glyphicon-resize-small gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-resize-small</code></li>
                            <li> <span class="glyphicon glyphicon-exclamation-sign gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-exclamation-sign</code></li>
                            <li> <span class="glyphicon glyphicon-gift gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-gift</code></li>
                            <li> <span class="glyphicon glyphicon-leaf gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-leaf</code></li>
                            <li> <span class="glyphicon glyphicon-fire gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-fire</code></li>
                            <li> <span class="glyphicon glyphicon-eye-open gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-eye-open</code></li>
                            <li> <span class="glyphicon glyphicon-eye-close gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-eye-close</code></li>
                            <li> <span class="glyphicon glyphicon-warning-sign gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-warning-sign</code></li>
                            <li> <span class="glyphicon glyphicon-plane gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-plane</code></li>
                            <li> <span class="glyphicon glyphicon-calendar gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-calendar</code></li>
                            <li> <span class="glyphicon glyphicon-random gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-random</code></li>
                            <li> <span class="glyphicon glyphicon-comment gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-comment</code></li>
                            <li> <span class="glyphicon glyphicon-magnet gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-magnet</code></li>
                            <li> <span class="glyphicon glyphicon-chevron-up gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-chevron-up</code></li>
                            <li> <span class="glyphicon glyphicon-chevron-down gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-chevron-down</code></li>
                            <li> <span class="glyphicon glyphicon-retweet gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-retweet</code></li>
                            <li> <span class="glyphicon glyphicon-shopping-cart gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-shopping-cart</code></li>
                            <li> <span class="glyphicon glyphicon-folder-close gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-folder-close</code></li>
                            <li> <span class="glyphicon glyphicon-folder-open gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-folder-open</code></li>
                            <li> <span class="glyphicon glyphicon-resize-vertical gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-resize-vertical</code></li>
                            <li> <span class="glyphicon glyphicon-resize-horizontal gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-resize-horizontal</code></li>
                            <li> <span class="glyphicon glyphicon-hdd gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-hdd</code></li>
                            <li> <span class="glyphicon glyphicon-bullhorn gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-bullhorn</code></li>
                            <li> <span class="glyphicon glyphicon-bell gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-bell</code></li>
                            <li> <span class="glyphicon glyphicon-certificate gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-certificate</code></li>
                            <li> <span class="glyphicon glyphicon-thumbs-up gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-thumbs-up</code></li>
                            <li> <span class="glyphicon glyphicon-thumbs-down gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-thumbs-down</code></li>
                            <li> <span class="glyphicon glyphicon-hand-right gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-hand-right</code></li>
                            <li> <span class="glyphicon glyphicon-hand-left gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-hand-left</code></li>
                            <li> <span class="glyphicon glyphicon-hand-up gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-hand-up</code></li>
                            <li> <span class="glyphicon glyphicon-hand-down gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-hand-down</code></li>
                            <li> <span class="glyphicon glyphicon-circle-arrow-right gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-circle-arrow-right</code></li>
                            <li> <span class="glyphicon glyphicon-circle-arrow-left gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-circle-arrow-left</code></li>
                            <li> <span class="glyphicon glyphicon-circle-arrow-up gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-circle-arrow-up</code></li>
                            <li> <span class="glyphicon glyphicon-circle-arrow-down gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-circle-arrow-down</code></li>
                            <li> <span class="glyphicon glyphicon-globe gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-globe</code></li>
                            <li> <span class="glyphicon glyphicon-wrench gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-wrench</code></li>
                            <li> <span class="glyphicon glyphicon-tasks gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-tasks</code></li>
                            <li> <span class="glyphicon glyphicon-filter gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-filter</code></li>
                            <li> <span class="glyphicon glyphicon-briefcase gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-briefcase</code></li>
                            <li> <span class="glyphicon glyphicon-fullscreen gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-fullscreen</code></li>
                            <li> <span class="glyphicon glyphicon-dashboard gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-dashboard</code></li>
                            <li> <span class="glyphicon glyphicon-paperclip gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-paperclip</code></li>
                            <li> <span class="glyphicon glyphicon-heart-empty gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-heart-empty</code></li>
                            <li> <span class="glyphicon glyphicon-link gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-link</code></li>
                            <li> <span class="glyphicon glyphicon-phone gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-phone</code></li>
                            <li> <span class="glyphicon glyphicon-pushpin gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-pushpin</code></li>
                            <li> <span class="glyphicon glyphicon-usd gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-usd</code></li>
                            <li> <span class="glyphicon glyphicon-gbp gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-gbp</code></li>
                            <li> <span class="glyphicon glyphicon-sort gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-sort</code></li>
                            <li> <span class="glyphicon glyphicon-sort-by-alphabet gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-sort-by-alphabet</code></li>
                            <li> <span class="glyphicon glyphicon-sort-by-alphabet-alt gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-sort-by-alphabet-alt</code></li>
                            <li> <span class="glyphicon glyphicon-sort-by-order gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-sort-by-order</code></li>
                            <li> <span class="glyphicon glyphicon-sort-by-order-alt gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-sort-by-order-alt</code></li>
                            <li> <span class="glyphicon glyphicon-sort-by-attributes gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-sort-by-attributes</code></li>
                            <li> <span class="glyphicon glyphicon-sort-by-attributes-alt gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-sort-by-attributes-alt</code></li>
                            <li> <span class="glyphicon glyphicon-unchecked gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-unchecked</code></li>
                            <li> <span class="glyphicon glyphicon-expand gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-expand</code></li>
                            <li> <span class="glyphicon glyphicon-collapse-down gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-collapse-down</code></li>
                            <li> <span class="glyphicon glyphicon-collapse-up gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-collapse-up</code></li>
                            <li> <span class="glyphicon glyphicon-log-in gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-log-in</code></li>
                            <li> <span class="glyphicon glyphicon-flash gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-flash</code></li>
                            <li> <span class="glyphicon glyphicon-log-out gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-log-out</code></li>
                            <li> <span class="glyphicon glyphicon-new-window gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-new-window</code></li>
                            <li> <span class="glyphicon glyphicon-record gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-record</code></li>
                            <li> <span class="glyphicon glyphicon-save gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-save</code></li>
                            <li> <span class="glyphicon glyphicon-open gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-open</code></li>
                            <li> <span class="glyphicon glyphicon-saved gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-saved</code></li>
                            <li> <span class="glyphicon glyphicon-import gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-import</code></li>
                            <li> <span class="glyphicon glyphicon-export gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-export</code></li>
                            <li> <span class="glyphicon glyphicon-send gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-send</code></li>
                            <li> <span class="glyphicon glyphicon-floppy-disk gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-floppy-disk</code></li>
                            <li> <span class="glyphicon glyphicon-floppy-saved gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-floppy-saved</code></li>
                            <li> <span class="glyphicon glyphicon-floppy-remove gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-floppy-remove</code></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul  class="list-unstyled">
                            <li> <span class="glyphicon glyphicon-floppy-save gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-floppy-save</code></li>
                            <li> <span class="glyphicon glyphicon-floppy-open gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-floppy-open</code></li>
                            <li> <span class="glyphicon glyphicon-credit-card gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-credit-card</code></li>
                            <li> <span class="glyphicon glyphicon-transfer gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-transfer</code></li>
                            <li> <span class="glyphicon glyphicon-cutlery gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-cutlery</code></li>
                            <li> <span class="glyphicon glyphicon-header gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-header</code></li>
                            <li> <span class="glyphicon glyphicon-compressed gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-compressed</code></li>
                            <li> <span class="glyphicon glyphicon-earphone gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-earphone</code></li>
                            <li> <span class="glyphicon glyphicon-phone-alt gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-phone-alt</code></li>
                            <li> <span class="glyphicon glyphicon-tower gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-tower</code></li>
                            <li> <span class="glyphicon glyphicon-stats gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-stats</code></li>
                            <li> <span class="glyphicon glyphicon-sd-video gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-sd-video</code></li>
                            <li> <span class="glyphicon glyphicon-hd-video gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-hd-video</code></li>
                            <li> <span class="glyphicon glyphicon-subtitles gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-subtitles</code></li>
                            <li> <span class="glyphicon glyphicon-sound-stereo gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-sound-stereo</code></li>
                            <li> <span class="glyphicon glyphicon-sound-dolby gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-sound-dolby</code></li>
                            <li> <span class="glyphicon glyphicon-sound-5-1 gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-sound-5-1</code></li>
                            <li> <span class="glyphicon glyphicon-sound-6-1 gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-sound-6-1</code></li>
                            <li> <span class="glyphicon glyphicon-sound-7-1 gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-sound-7-1</code></li>
                            <li> <span class="glyphicon glyphicon-copyright-mark gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-copyright-mark</code></li>
                            <li> <span class="glyphicon glyphicon-registration-mark gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-registration-mark</code></li>
                            <li> <span class="glyphicon glyphicon-cloud-download gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-cloud-download</code></li>
                            <li> <span class="glyphicon glyphicon-cloud-upload gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-cloud-upload</code></li>
                            <li> <span class="glyphicon glyphicon-tree-conifer gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-tree-conifer</code></li>
                            <li> <span class="glyphicon glyphicon-tree-deciduous gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-tree-deciduous</code></li>
                            <li> <span class="glyphicon glyphicon-cd gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-cd</code></li>
                            <li> <span class="glyphicon glyphicon-save-file gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-save-file</code></li>
                            <li> <span class="glyphicon glyphicon-open-file gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-open-file</code></li>
                            <li> <span class="glyphicon glyphicon-level-up gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-level-up</code></li>
                            <li> <span class="glyphicon glyphicon-copy gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-copy</code></li>
                            <li> <span class="glyphicon glyphicon-paste gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-paste</code></li>
                            <li> <span class="glyphicon glyphicon-alert gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-alert</code></li>
                            <li> <span class="glyphicon glyphicon-equalizer gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-equalizer</code></li>
                            <li> <span class="glyphicon glyphicon-king gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-king</code></li>
                            <li> <span class="glyphicon glyphicon-queen gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-queen</code></li>
                            <li> <span class="glyphicon glyphicon-pawn gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-pawn</code></li>
                            <li> <span class="glyphicon glyphicon-bishop gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-bishop</code></li>
                            <li> <span class="glyphicon glyphicon-knight gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-knight</code></li>
                            <li> <span class="glyphicon glyphicon-baby-formula gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-baby-formula</code></li>
                            <li> <span class="glyphicon glyphicon-tent gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-tent</code></li>
                            <li> <span class="glyphicon glyphicon-blackboard gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-blackboard</code></li>
                            <li> <span class="glyphicon glyphicon-bed gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-bed</code></li>
                            <li> <span class="glyphicon glyphicon-apple gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-apple</code></li>
                            <li> <span class="glyphicon glyphicon-erase gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-erase</code></li>
                            <li> <span class="glyphicon glyphicon-hourglass gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-hourglass</code></li>
                            <li> <span class="glyphicon glyphicon-lamp gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-lamp</code></li>
                            <li> <span class="glyphicon glyphicon-duplicate gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-duplicate</code></li>
                            <li> <span class="glyphicon glyphicon-piggy-bank gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-piggy-bank</code></li>
                            <li> <span class="glyphicon glyphicon-scissors gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-scissors</code></li>
                            <li> <span class="glyphicon glyphicon-bitcoin gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-bitcoin</code></li>
                            <li> <span class="glyphicon glyphicon-btc gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-btc</code></li>
                            <li> <span class="glyphicon glyphicon-xbt gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-xbt</code></li>
                            <li> <span class="glyphicon glyphicon-yen gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-yen</code></li>
                            <li> <span class="glyphicon glyphicon-jpy gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-jpy</code></li>
                            <li> <span class="glyphicon glyphicon-ruble gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-ruble</code></li>
                            <li> <span class="glyphicon glyphicon-rub gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-rub</code></li>
                            <li> <span class="glyphicon glyphicon-scale gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-scale</code></li>
                            <li> <span class="glyphicon glyphicon-ice-lolly gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-ice-lolly</code></li>
                            <li> <span class="glyphicon glyphicon-ice-lolly-tasted gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-ice-lolly-tasted</code></li>
                            <li> <span class="glyphicon glyphicon-education gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-education</code></li>
                            <li> <span class="glyphicon glyphicon-option-horizontal gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-option-horizontal</code></li>
                            <li> <span class="glyphicon glyphicon-option-vertical gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-option-vertical</code></li>
                            <li> <span class="glyphicon glyphicon-menu-hamburger gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-menu-hamburger</code></li>
                            <li> <span class="glyphicon glyphicon-modal-window gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-modal-window</code></li>
                            <li> <span class="glyphicon glyphicon-oil gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-oil</code></li>
                            <li> <span class="glyphicon glyphicon-grain gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-grain</code></li>
                            <li> <span class="glyphicon glyphicon-sunglasses gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-sunglasses</code></li>
                            <li> <span class="glyphicon glyphicon-text-size gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-text-size</code></li>
                            <li> <span class="glyphicon glyphicon-text-color gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-text-color</code></li>
                            <li> <span class="glyphicon glyphicon-text-background gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-text-background</code></li>
                            <li> <span class="glyphicon glyphicon-object-align-top gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-object-align-top</code></li>
                            <li> <span class="glyphicon glyphicon-object-align-bottom gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-object-align-bottom</code></li>
                            <li> <span class="glyphicon glyphicon-object-align-horizontal gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-object-align-horizontal</code></li>
                            <li> <span class="glyphicon glyphicon-object-align-left gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-object-align-left</code></li>
                            <li> <span class="glyphicon glyphicon-object-align-vertical gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-object-align-vertical</code></li>
                            <li> <span class="glyphicon glyphicon-object-align-right gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-object-align-right</code></li>
                            <li> <span class="glyphicon glyphicon-triangle-right gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-triangle-right</code></li>
                            <li> <span class="glyphicon glyphicon-triangle-left gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-triangle-left</code></li>
                            <li> <span class="glyphicon glyphicon-triangle-bottom gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-triangle-bottom</code></li>
                            <li> <span class="glyphicon glyphicon-triangle-top gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-triangle-top</code></li>
                            <li> <span class="glyphicon glyphicon-console gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-console</code></li>
                            <li> <span class="glyphicon glyphicon-superscript gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-superscript</code></li>
                            <li> <span class="glyphicon glyphicon-subscript gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-subscript</code></li>
                            <li> <span class="glyphicon glyphicon-menu-left gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-menu-left</code></li>
                            <li> <span class="glyphicon glyphicon-menu-right gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-menu-right</code></li>
                            <li> <span class="glyphicon glyphicon-menu-down gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-menu-down</code></li>
                            <li> <span class="glyphicon glyphicon-menu-up gly-2" aria-hidden="true"></span><br /><code>glyphicon glyphicon-menu-up</code></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            Font Awesome 4.5.0
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="font-awesome-icons text-md-center">
                <div class="row">
                    <div class="col-md-4">
                        <ul  class="list-unstyled">
                            <li> <span class="fa fa-500px gly-2" aria-hidden="true"></span><br />
                                <code>fa-500px [ &amp;#xf26e; ]</code></li>
                            <li> <span class="fa fa-adjust gly-2" aria-hidden="true"></span><br />
                                <code>fa-adjust [ &amp;#xf042; ]</code></li>
                            <li> <span class="fa fa-adn gly-2" aria-hidden="true"></span><br />
                                <code>fa-adn [ &amp;#xf170; ]</code></li>
                            <li> <span class="fa fa-align-center gly-2" aria-hidden="true"></span><br />
                                <code>fa-align-center [ &amp;#xf037; ]</code></li>
                            <li> <span class="fa fa-align-justify gly-2" aria-hidden="true"></span><br />
                                <code>fa-align-justify [ &amp;#xf039; ]</code></li>
                            <li> <span class="fa fa-align-left gly-2" aria-hidden="true"></span><br />
                                <code>fa-align-left [ &amp;#xf036; ]</code></li>
                            <li> <span class="fa fa-align-right gly-2" aria-hidden="true"></span><br />
                                <code>fa-align-right [ &amp;#xf038; ]</code></li>
                            <li> <span class="fa fa-amazon gly-2" aria-hidden="true"></span><br />
                                <code>fa-amazon [ &amp;#xf270; ]</code></li>
                            <li> <span class="fa fa-ambulance gly-2" aria-hidden="true"></span><br />
                                <code>fa-ambulance [ &amp;#xf0f9; ]</code></li>
                            <li> <span class="fa fa-anchor gly-2" aria-hidden="true"></span><br />
                                <code>fa-anchor [ &amp;#xf13d; ]</code></li>
                            <li> <span class="fa fa-android gly-2" aria-hidden="true"></span><br />
                                <code>fa-android [ &amp;#xf17b; ]</code></li>
                            <li> <span class="fa fa-angellist gly-2" aria-hidden="true"></span><br />
                                <code>fa-angellist [ &amp;#xf209; ]</code></li>
                            <li> <span class="fa fa-angle-double-down gly-2" aria-hidden="true"></span><br />
                                <code>fa-angle-double-down [ &amp;#xf103; ]</code></li>
                            <li> <span class="fa fa-angle-double-left gly-2" aria-hidden="true"></span><br />
                                <code>fa-angle-double-left [ &amp;#xf100; ]</code></li>
                            <li> <span class="fa fa-angle-double-right gly-2" aria-hidden="true"></span><br />
                                <code>fa-angle-double-right [ &amp;#xf101; ]</code></li>
                            <li> <span class="fa fa-angle-double-up gly-2" aria-hidden="true"></span><br />
                                <code>fa-angle-double-up [ &amp;#xf102; ]</code></li>
                            <li> <span class="fa fa-angle-down gly-2" aria-hidden="true"></span><br />
                                <code>fa-angle-down [ &amp;#xf107; ]</code></li>
                            <li> <span class="fa fa-angle-left gly-2" aria-hidden="true"></span><br />
                                <code>fa-angle-left [ &amp;#xf104; ]</code></li>
                            <li> <span class="fa fa-angle-right gly-2" aria-hidden="true"></span><br />
                                <code>fa-angle-right [ &amp;#xf105; ]</code></li>
                            <li> <span class="fa fa-angle-up gly-2" aria-hidden="true"></span><br />
                                <code>fa-angle-up [ &amp;#xf106; ]</code></li>
                            <li> <span class="fa fa-apple gly-2" aria-hidden="true"></span><br />
                                <code>fa-apple [ &amp;#xf179; ]</code></li>
                            <li> <span class="fa fa-archive gly-2" aria-hidden="true"></span><br />
                                <code>fa-archive [ &amp;#xf187; ]</code></li>
                            <li> <span class="fa fa-area-chart gly-2" aria-hidden="true"></span><br />
                                <code>fa-area-chart [ &amp;#xf1fe; ]</code></li>
                            <li> <span class="fa fa-arrow-circle-down gly-2" aria-hidden="true"></span><br />
                                <code>fa-arrow-circle-down [ &amp;#xf0ab; ]</code></li>
                            <li> <span class="fa fa-arrow-circle-left gly-2" aria-hidden="true"></span><br />
                                <code>fa-arrow-circle-left [ &amp;#xf0a8; ]</code></li>
                            <li> <span class="fa fa-arrow-circle-o-down gly-2" aria-hidden="true"></span><br />
                                <code>fa-arrow-circle-o-down [ &amp;#xf01a; ]</code></li>
                            <li> <span class="fa fa-arrow-circle-o-left gly-2" aria-hidden="true"></span><br />
                                <code>fa-arrow-circle-o-left [ &amp;#xf190; ]</code></li>
                            <li> <span class="fa fa-arrow-circle-o-right gly-2" aria-hidden="true"></span><br />
                                <code>fa-arrow-circle-o-right [ &amp;#xf18e; ]</code></li>
                            <li> <span class="fa fa-arrow-circle-o-up gly-2" aria-hidden="true"></span><br />
                                <code>fa-arrow-circle-o-up [ &amp;#xf01b; ]</code></li>
                            <li> <span class="fa fa-arrow-circle-right gly-2" aria-hidden="true"></span><br />
                                <code>fa-arrow-circle-right [ &amp;#xf0a9; ]</code></li>
                            <li> <span class="fa fa-arrow-circle-up gly-2" aria-hidden="true"></span><br />
                                <code>fa-arrow-circle-up [ &amp;#xf0aa; ]</code></li>
                            <li> <span class="fa fa-arrow-down gly-2" aria-hidden="true"></span><br />
                                <code>fa-arrow-down [ &amp;#xf063; ]</code></li>
                            <li> <span class="fa fa-arrow-left gly-2" aria-hidden="true"></span><br />
                                <code>fa-arrow-left [ &amp;#xf060; ]</code></li>
                            <li> <span class="fa fa-arrow-right gly-2" aria-hidden="true"></span><br />
                                <code>fa-arrow-right [ &amp;#xf061; ]</code></li>
                            <li> <span class="fa fa-arrow-up gly-2" aria-hidden="true"></span><br />
                                <code>fa-arrow-up [ &amp;#xf062; ]</code></li>
                            <li> <span class="fa fa-arrows gly-2" aria-hidden="true"></span><br />
                                <code>fa-arrows [ &amp;#xf047; ]</code></li>
                            <li> <span class="fa fa-arrows-alt gly-2" aria-hidden="true"></span><br />
                                <code>fa-arrows-alt [ &amp;#xf0b2; ]</code></li>
                            <li> <span class="fa fa-arrows-h gly-2" aria-hidden="true"></span><br />
                                <code>fa-arrows-h [ &amp;#xf07e; ]</code></li>
                            <li> <span class="fa fa-arrows-v gly-2" aria-hidden="true"></span><br />
                                <code>fa-arrows-v [ &amp;#xf07d; ]</code></li>
                            <li> <span class="fa fa-asterisk gly-2" aria-hidden="true"></span><br />
                                <code>fa-asterisk [ &amp;#xf069; ]</code></li>
                            <li> <span class="fa fa-at gly-2" aria-hidden="true"></span><br />
                                <code>fa-at [ &amp;#xf1fa; ]</code></li>
                            <li> <span class="fa fa-automobile  gly-2" aria-hidden="true"></span><br />
                                <code>fa-automobile  [ &amp;#xf1b9; ]</code></li>
                            <li> <span class="fa fa-backward gly-2" aria-hidden="true"></span><br />
                                <code>fa-backward [ &amp;#xf04a; ]</code></li>
                            <li> <span class="fa fa-balance-scale gly-2" aria-hidden="true"></span><br />
                                <code>fa-balance-scale [ &amp;#xf24e; ]</code></li>
                            <li> <span class="fa fa-ban gly-2" aria-hidden="true"></span><br />
                                <code>fa-ban [ &amp;#xf05e; ]</code></li>
                            <li> <span class="fa fa-bank  gly-2" aria-hidden="true"></span><br />
                                <code>fa-bank  [ &amp;#xf19c; ]</code></li>
                            <li> <span class="fa fa-bar-chart gly-2" aria-hidden="true"></span><br />
                                <code>fa-bar-chart [ &amp;#xf080; ]</code></li>
                            <li> <span class="fa fa-bar-chart-o  gly-2" aria-hidden="true"></span><br />
                                <code>fa-bar-chart-o  [ &amp;#xf080; ]</code></li>
                            <li> <span class="fa fa-barcode gly-2" aria-hidden="true"></span><br />
                                <code>fa-barcode [ &amp;#xf02a; ]</code></li>
                            <li> <span class="fa fa-bars gly-2" aria-hidden="true"></span><br />
                                <code>fa-bars [ &amp;#xf0c9; ]</code></li>
                            <li> <span class="fa fa-battery-0  gly-2" aria-hidden="true"></span><br />
                                <code>fa-battery-0  [ &amp;#xf244; ]</code></li>
                            <li> <span class="fa fa-battery-1  gly-2" aria-hidden="true"></span><br />
                                <code>fa-battery-1  [ &amp;#xf243; ]</code></li>
                            <li> <span class="fa fa-battery-2  gly-2" aria-hidden="true"></span><br />
                                <code>fa-battery-2  [ &amp;#xf242; ]</code></li>
                            <li> <span class="fa fa-battery-3  gly-2" aria-hidden="true"></span><br />
                                <code>fa-battery-3  [ &amp;#xf241; ]</code></li>
                            <li> <span class="fa fa-battery-4  gly-2" aria-hidden="true"></span><br />
                                <code>fa-battery-4  [ &amp;#xf240; ]</code></li>
                            <li> <span class="fa fa-battery-empty gly-2" aria-hidden="true"></span><br />
                                <code>fa-battery-empty [ &amp;#xf244; ]</code></li>
                            <li> <span class="fa fa-battery-full gly-2" aria-hidden="true"></span><br />
                                <code>fa-battery-full [ &amp;#xf240; ]</code></li>
                            <li> <span class="fa fa-battery-half gly-2" aria-hidden="true"></span><br />
                                <code>fa-battery-half [ &amp;#xf242; ]</code></li>
                            <li> <span class="fa fa-battery-quarter gly-2" aria-hidden="true"></span><br />
                                <code>fa-battery-quarter [ &amp;#xf243; ]</code></li>
                            <li> <span class="fa fa-battery-three-quarters gly-2" aria-hidden="true"></span><br />
                                <code>fa-battery-three-quarters [ &amp;#xf241; ]</code></li>
                            <li> <span class="fa fa-bed gly-2" aria-hidden="true"></span><br />
                                <code>fa-bed [ &amp;#xf236; ]</code></li>
                            <li> <span class="fa fa-beer gly-2" aria-hidden="true"></span><br />
                                <code>fa-beer [ &amp;#xf0fc; ]</code></li>
                            <li> <span class="fa fa-behance gly-2" aria-hidden="true"></span><br />
                                <code>fa-behance [ &amp;#xf1b4; ]</code></li>
                            <li> <span class="fa fa-behance-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-behance-square [ &amp;#xf1b5; ]</code></li>
                            <li> <span class="fa fa-bell gly-2" aria-hidden="true"></span><br />
                                <code>fa-bell [ &amp;#xf0f3; ]</code></li>
                            <li> <span class="fa fa-bell-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-bell-o [ &amp;#xf0a2; ]</code></li>
                            <li> <span class="fa fa-bell-slash gly-2" aria-hidden="true"></span><br />
                                <code>fa-bell-slash [ &amp;#xf1f6; ]</code></li>
                            <li> <span class="fa fa-bell-slash-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-bell-slash-o [ &amp;#xf1f7; ]</code></li>
                            <li> <span class="fa fa-bicycle gly-2" aria-hidden="true"></span><br />
                                <code>fa-bicycle [ &amp;#xf206; ]</code></li>
                            <li> <span class="fa fa-binoculars gly-2" aria-hidden="true"></span><br />
                                <code>fa-binoculars [ &amp;#xf1e5; ]</code></li>
                            <li> <span class="fa fa-birthday-cake gly-2" aria-hidden="true"></span><br />
                                <code>fa-birthday-cake [ &amp;#xf1fd; ]</code></li>
                            <li> <span class="fa fa-bitbucket gly-2" aria-hidden="true"></span><br />
                                <code>fa-bitbucket [ &amp;#xf171; ]</code></li>
                            <li> <span class="fa fa-bitbucket-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-bitbucket-square [ &amp;#xf172; ]</code></li>
                            <li> <span class="fa fa-bitcoin  gly-2" aria-hidden="true"></span><br />
                                <code>fa-bitcoin  [ &amp;#xf15a; ]</code></li>
                            <li> <span class="fa fa-black-tie gly-2" aria-hidden="true"></span><br />
                                <code>fa-black-tie [ &amp;#xf27e; ]</code></li>
                            <li> <span class="fa fa-bluetooth gly-2" aria-hidden="true"></span><br />
                                <code>fa-bluetooth [ &amp;#xf293; ]</code></li>
                            <li> <span class="fa fa-bluetooth-b gly-2" aria-hidden="true"></span><br />
                                <code>fa-bluetooth-b [ &amp;#xf294; ]</code></li>
                            <li> <span class="fa fa-bold gly-2" aria-hidden="true"></span><br />
                                <code>fa-bold [ &amp;#xf032; ]</code></li>
                            <li> <span class="fa fa-bolt gly-2" aria-hidden="true"></span><br />
                                <code>fa-bolt [ &amp;#xf0e7; ]</code></li>
                            <li> <span class="fa fa-bomb gly-2" aria-hidden="true"></span><br />
                                <code>fa-bomb [ &amp;#xf1e2; ]</code></li>
                            <li> <span class="fa fa-book gly-2" aria-hidden="true"></span><br />
                                <code>fa-book [ &amp;#xf02d; ]</code></li>
                            <li> <span class="fa fa-bookmark gly-2" aria-hidden="true"></span><br />
                                <code>fa-bookmark [ &amp;#xf02e; ]</code></li>
                            <li> <span class="fa fa-bookmark-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-bookmark-o [ &amp;#xf097; ]</code></li>
                            <li> <span class="fa fa-briefcase gly-2" aria-hidden="true"></span><br />
                                <code>fa-briefcase [ &amp;#xf0b1; ]</code></li>
                            <li> <span class="fa fa-btc gly-2" aria-hidden="true"></span><br />
                                <code>fa-btc [ &amp;#xf15a; ]</code></li>
                            <li> <span class="fa fa-bug gly-2" aria-hidden="true"></span><br />
                                <code>fa-bug [ &amp;#xf188; ]</code></li>
                            <li> <span class="fa fa-building gly-2" aria-hidden="true"></span><br />
                                <code>fa-building [ &amp;#xf1ad; ]</code></li>
                            <li> <span class="fa fa-building-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-building-o [ &amp;#xf0f7; ]</code></li>
                            <li> <span class="fa fa-bullhorn gly-2" aria-hidden="true"></span><br />
                                <code>fa-bullhorn [ &amp;#xf0a1; ]</code></li>
                            <li> <span class="fa fa-bullseye gly-2" aria-hidden="true"></span><br />
                                <code>fa-bullseye [ &amp;#xf140; ]</code></li>
                            <li> <span class="fa fa-bus gly-2" aria-hidden="true"></span><br />
                                <code>fa-bus [ &amp;#xf207; ]</code></li>
                            <li> <span class="fa fa-buysellads gly-2" aria-hidden="true"></span><br />
                                <code>fa-buysellads [ &amp;#xf20d; ]</code></li>
                            <li> <span class="fa fa-cab  gly-2" aria-hidden="true"></span><br />
                                <code>fa-cab  [ &amp;#xf1ba; ]</code></li>
                            <li> <span class="fa fa-calculator gly-2" aria-hidden="true"></span><br />
                                <code>fa-calculator [ &amp;#xf1ec; ]</code></li>
                            <li> <span class="fa fa-calendar gly-2" aria-hidden="true"></span><br />
                                <code>fa-calendar [ &amp;#xf073; ]</code></li>
                            <li> <span class="fa fa-calendar-check-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-calendar-check-o [ &amp;#xf274; ]</code></li>
                            <li> <span class="fa fa-calendar-minus-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-calendar-minus-o [ &amp;#xf272; ]</code></li>
                            <li> <span class="fa fa-calendar-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-calendar-o [ &amp;#xf133; ]</code></li>
                            <li> <span class="fa fa-calendar-plus-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-calendar-plus-o [ &amp;#xf271; ]</code></li>
                            <li> <span class="fa fa-calendar-times-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-calendar-times-o [ &amp;#xf273; ]</code></li>
                            <li> <span class="fa fa-camera gly-2" aria-hidden="true"></span><br />
                                <code>fa-camera [ &amp;#xf030; ]</code></li>
                            <li> <span class="fa fa-camera-retro gly-2" aria-hidden="true"></span><br />
                                <code>fa-camera-retro [ &amp;#xf083; ]</code></li>
                            <li> <span class="fa fa-car gly-2" aria-hidden="true"></span><br />
                                <code>fa-car [ &amp;#xf1b9; ]</code></li>
                            <li> <span class="fa fa-caret-down gly-2" aria-hidden="true"></span><br />
                                <code>fa-caret-down [ &amp;#xf0d7; ]</code></li>
                            <li> <span class="fa fa-caret-left gly-2" aria-hidden="true"></span><br />
                                <code>fa-caret-left [ &amp;#xf0d9; ]</code></li>
                            <li> <span class="fa fa-caret-right gly-2" aria-hidden="true"></span><br />
                                <code>fa-caret-right [ &amp;#xf0da; ]</code></li>
                            <li> <span class="fa fa-caret-square-o-down gly-2" aria-hidden="true"></span><br />
                                <code>fa-caret-square-o-down [ &amp;#xf150; ]</code></li>
                            <li> <span class="fa fa-caret-square-o-left gly-2" aria-hidden="true"></span><br />
                                <code>fa-caret-square-o-left [ &amp;#xf191; ]</code></li>
                            <li> <span class="fa fa-caret-square-o-right gly-2" aria-hidden="true"></span><br />
                                <code>fa-caret-square-o-right [ &amp;#xf152; ]</code></li>
                            <li> <span class="fa fa-caret-square-o-up gly-2" aria-hidden="true"></span><br />
                                <code>fa-caret-square-o-up [ &amp;#xf151; ]</code></li>
                            <li> <span class="fa fa-caret-up gly-2" aria-hidden="true"></span><br />
                                <code>fa-caret-up [ &amp;#xf0d8; ]</code></li>
                            <li> <span class="fa fa-cart-arrow-down gly-2" aria-hidden="true"></span><br />
                                <code>fa-cart-arrow-down [ &amp;#xf218; ]</code></li>
                            <li> <span class="fa fa-cart-plus gly-2" aria-hidden="true"></span><br />
                                <code>fa-cart-plus [ &amp;#xf217; ]</code></li>
                            <li> <span class="fa fa-cc gly-2" aria-hidden="true"></span><br />
                                <code>fa-cc [ &amp;#xf20a; ]</code></li>
                            <li> <span class="fa fa-cc-amex gly-2" aria-hidden="true"></span><br />
                                <code>fa-cc-amex [ &amp;#xf1f3; ]</code></li>
                            <li> <span class="fa fa-cc-diners-club gly-2" aria-hidden="true"></span><br />
                                <code>fa-cc-diners-club [ &amp;#xf24c; ]</code></li>
                            <li> <span class="fa fa-cc-discover gly-2" aria-hidden="true"></span><br />
                                <code>fa-cc-discover [ &amp;#xf1f2; ]</code></li>
                            <li> <span class="fa fa-cc-jcb gly-2" aria-hidden="true"></span><br />
                                <code>fa-cc-jcb [ &amp;#xf24b; ]</code></li>
                            <li> <span class="fa fa-cc-mastercard gly-2" aria-hidden="true"></span><br />
                                <code>fa-cc-mastercard [ &amp;#xf1f1; ]</code></li>
                            <li> <span class="fa fa-cc-paypal gly-2" aria-hidden="true"></span><br />
                                <code>fa-cc-paypal [ &amp;#xf1f4; ]</code></li>
                            <li> <span class="fa fa-cc-stripe gly-2" aria-hidden="true"></span><br />
                                <code>fa-cc-stripe [ &amp;#xf1f5; ]</code></li>
                            <li> <span class="fa fa-cc-visa gly-2" aria-hidden="true"></span><br />
                                <code>fa-cc-visa [ &amp;#xf1f0; ]</code></li>
                            <li> <span class="fa fa-certificate gly-2" aria-hidden="true"></span><br />
                                <code>fa-certificate [ &amp;#xf0a3; ]</code></li>
                            <li> <span class="fa fa-chain  gly-2" aria-hidden="true"></span><br />
                                <code>fa-chain  [ &amp;#xf0c1; ]</code></li>
                            <li> <span class="fa fa-chain-broken gly-2" aria-hidden="true"></span><br />
                                <code>fa-chain-broken [ &amp;#xf127; ]</code></li>
                            <li> <span class="fa fa-check gly-2" aria-hidden="true"></span><br />
                                <code>fa-check [ &amp;#xf00c; ]</code></li>
                            <li> <span class="fa fa-check-circle gly-2" aria-hidden="true"></span><br />
                                <code>fa-check-circle [ &amp;#xf058; ]</code></li>
                            <li> <span class="fa fa-check-circle-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-check-circle-o [ &amp;#xf05d; ]</code></li>
                            <li> <span class="fa fa-check-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-check-square [ &amp;#xf14a; ]</code></li>
                            <li> <span class="fa fa-check-square-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-check-square-o [ &amp;#xf046; ]</code></li>
                            <li> <span class="fa fa-chevron-circle-down gly-2" aria-hidden="true"></span><br />
                                <code>fa-chevron-circle-down [ &amp;#xf13a; ]</code></li>
                            <li> <span class="fa fa-chevron-circle-left gly-2" aria-hidden="true"></span><br />
                                <code>fa-chevron-circle-left [ &amp;#xf137; ]</code></li>
                            <li> <span class="fa fa-chevron-circle-right gly-2" aria-hidden="true"></span><br />
                                <code>fa-chevron-circle-right [ &amp;#xf138; ]</code></li>
                            <li> <span class="fa fa-chevron-circle-up gly-2" aria-hidden="true"></span><br />
                                <code>fa-chevron-circle-up [ &amp;#xf139; ]</code></li>
                            <li> <span class="fa fa-chevron-down gly-2" aria-hidden="true"></span><br />
                                <code>fa-chevron-down [ &amp;#xf078; ]</code></li>
                            <li> <span class="fa fa-chevron-left gly-2" aria-hidden="true"></span><br />
                                <code>fa-chevron-left [ &amp;#xf053; ]</code></li>
                            <li> <span class="fa fa-chevron-right gly-2" aria-hidden="true"></span><br />
                                <code>fa-chevron-right [ &amp;#xf054; ]</code></li>
                            <li> <span class="fa fa-chevron-up gly-2" aria-hidden="true"></span><br />
                                <code>fa-chevron-up [ &amp;#xf077; ]</code></li>
                            <li> <span class="fa fa-child gly-2" aria-hidden="true"></span><br />
                                <code>fa-child [ &amp;#xf1ae; ]</code></li>
                            <li> <span class="fa fa-chrome gly-2" aria-hidden="true"></span><br />
                                <code>fa-chrome [ &amp;#xf268; ]</code></li>
                            <li> <span class="fa fa-circle gly-2" aria-hidden="true"></span><br />
                                <code>fa-circle [ &amp;#xf111; ]</code></li>
                            <li> <span class="fa fa-circle-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-circle-o [ &amp;#xf10c; ]</code></li>
                            <li> <span class="fa fa-circle-o-notch gly-2" aria-hidden="true"></span><br />
                                <code>fa-circle-o-notch [ &amp;#xf1ce; ]</code></li>
                            <li> <span class="fa fa-circle-thin gly-2" aria-hidden="true"></span><br />
                                <code>fa-circle-thin [ &amp;#xf1db; ]</code></li>
                            <li> <span class="fa fa-clipboard gly-2" aria-hidden="true"></span><br />
                                <code>fa-clipboard [ &amp;#xf0ea; ]</code></li>
                            <li> <span class="fa fa-clock-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-clock-o [ &amp;#xf017; ]</code></li>
                            <li> <span class="fa fa-clone gly-2" aria-hidden="true"></span><br />
                                <code>fa-clone [ &amp;#xf24d; ]</code></li>
                            <li> <span class="fa fa-close  gly-2" aria-hidden="true"></span><br />
                                <code>fa-close  [ &amp;#xf00d; ]</code></li>
                            <li> <span class="fa fa-cloud gly-2" aria-hidden="true"></span><br />
                                <code>fa-cloud [ &amp;#xf0c2; ]</code></li>
                            <li> <span class="fa fa-cloud-download gly-2" aria-hidden="true"></span><br />
                                <code>fa-cloud-download [ &amp;#xf0ed; ]</code></li>
                            <li> <span class="fa fa-cloud-upload gly-2" aria-hidden="true"></span><br />
                                <code>fa-cloud-upload [ &amp;#xf0ee; ]</code></li>
                            <li> <span class="fa fa-cny  gly-2" aria-hidden="true"></span><br />
                                <code>fa-cny  [ &amp;#xf157; ]</code></li>
                            <li> <span class="fa fa-code gly-2" aria-hidden="true"></span><br />
                                <code>fa-code [ &amp;#xf121; ]</code></li>
                            <li> <span class="fa fa-code-fork gly-2" aria-hidden="true"></span><br />
                                <code>fa-code-fork [ &amp;#xf126; ]</code></li>
                            <li> <span class="fa fa-codepen gly-2" aria-hidden="true"></span><br />
                                <code>fa-codepen [ &amp;#xf1cb; ]</code></li>
                            <li> <span class="fa fa-codiepie gly-2" aria-hidden="true"></span><br />
                                <code>fa-codiepie [ &amp;#xf284; ]</code></li>
                            <li> <span class="fa fa-coffee gly-2" aria-hidden="true"></span><br />
                                <code>fa-coffee [ &amp;#xf0f4; ]</code></li>
                            <li> <span class="fa fa-cog gly-2" aria-hidden="true"></span><br />
                                <code>fa-cog [ &amp;#xf013; ]</code></li>
                            <li> <span class="fa fa-cogs gly-2" aria-hidden="true"></span><br />
                                <code>fa-cogs [ &amp;#xf085; ]</code></li>
                            <li> <span class="fa fa-columns gly-2" aria-hidden="true"></span><br />
                                <code>fa-columns [ &amp;#xf0db; ]</code></li>
                            <li> <span class="fa fa-comment gly-2" aria-hidden="true"></span><br />
                                <code>fa-comment [ &amp;#xf075; ]</code></li>
                            <li> <span class="fa fa-comment-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-comment-o [ &amp;#xf0e5; ]</code></li>
                            <li> <span class="fa fa-commenting gly-2" aria-hidden="true"></span><br />
                                <code>fa-commenting [ &amp;#xf27a; ]</code></li>
                            <li> <span class="fa fa-commenting-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-commenting-o [ &amp;#xf27b; ]</code></li>
                            <li> <span class="fa fa-comments gly-2" aria-hidden="true"></span><br />
                                <code>fa-comments [ &amp;#xf086; ]</code></li>
                            <li> <span class="fa fa-comments-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-comments-o [ &amp;#xf0e6; ]</code></li>
                            <li> <span class="fa fa-compass gly-2" aria-hidden="true"></span><br />
                                <code>fa-compass [ &amp;#xf14e; ]</code></li>
                            <li> <span class="fa fa-compress gly-2" aria-hidden="true"></span><br />
                                <code>fa-compress [ &amp;#xf066; ]</code></li>
                            <li> <span class="fa fa-connectdevelop gly-2" aria-hidden="true"></span><br />
                                <code>fa-connectdevelop [ &amp;#xf20e; ]</code></li>
                            <li> <span class="fa fa-contao gly-2" aria-hidden="true"></span><br />
                                <code>fa-contao [ &amp;#xf26d; ]</code></li>
                            <li> <span class="fa fa-copy  gly-2" aria-hidden="true"></span><br />
                                <code>fa-copy  [ &amp;#xf0c5; ]</code></li>
                            <li> <span class="fa fa-copyright gly-2" aria-hidden="true"></span><br />
                                <code>fa-copyright [ &amp;#xf1f9; ]</code></li>
                            <li> <span class="fa fa-creative-commons gly-2" aria-hidden="true"></span><br />
                                <code>fa-creative-commons [ &amp;#xf25e; ]</code></li>
                            <li> <span class="fa fa-credit-card gly-2" aria-hidden="true"></span><br />
                                <code>fa-credit-card [ &amp;#xf09d; ]</code></li>
                            <li> <span class="fa fa-credit-card-alt gly-2" aria-hidden="true"></span><br />
                                <code>fa-credit-card-alt [ &amp;#xf283; ]</code></li>
                            <li> <span class="fa fa-crop gly-2" aria-hidden="true"></span><br />
                                <code>fa-crop [ &amp;#xf125; ]</code></li>
                            <li> <span class="fa fa-crosshairs gly-2" aria-hidden="true"></span><br />
                                <code>fa-crosshairs [ &amp;#xf05b; ]</code></li>
                            <li> <span class="fa fa-css3 gly-2" aria-hidden="true"></span><br />
                                <code>fa-css3 [ &amp;#xf13c; ]</code></li>
                            <li> <span class="fa fa-cube gly-2" aria-hidden="true"></span><br />
                                <code>fa-cube [ &amp;#xf1b2; ]</code></li>
                            <li> <span class="fa fa-cubes gly-2" aria-hidden="true"></span><br />
                                <code>fa-cubes [ &amp;#xf1b3; ]</code></li>
                            <li> <span class="fa fa-cut  gly-2" aria-hidden="true"></span><br />
                                <code>fa-cut  [ &amp;#xf0c4; ]</code></li>
                            <li> <span class="fa fa-cutlery gly-2" aria-hidden="true"></span><br />
                                <code>fa-cutlery [ &amp;#xf0f5; ]</code></li>
                            <li> <span class="fa fa-dashboard  gly-2" aria-hidden="true"></span><br />
                                <code>fa-dashboard  [ &amp;#xf0e4; ]</code></li>
                            <li> <span class="fa fa-dashcube gly-2" aria-hidden="true"></span><br />
                                <code>fa-dashcube [ &amp;#xf210; ]</code></li>
                            <li> <span class="fa fa-database gly-2" aria-hidden="true"></span><br />
                                <code>fa-database [ &amp;#xf1c0; ]</code></li>
                            <li> <span class="fa fa-dedent  gly-2" aria-hidden="true"></span><br />
                                <code>fa-dedent  [ &amp;#xf03b; ]</code></li>
                            <li> <span class="fa fa-delicious gly-2" aria-hidden="true"></span><br />
                                <code>fa-delicious [ &amp;#xf1a5; ]</code></li>
                            <li> <span class="fa fa-desktop gly-2" aria-hidden="true"></span><br />
                                <code>fa-desktop [ &amp;#xf108; ]</code></li>
                            <li> <span class="fa fa-deviantart gly-2" aria-hidden="true"></span><br />
                                <code>fa-deviantart [ &amp;#xf1bd; ]</code></li>
                            <li> <span class="fa fa-diamond gly-2" aria-hidden="true"></span><br />
                                <code>fa-diamond [ &amp;#xf219; ]</code></li>
                            <li> <span class="fa fa-digg gly-2" aria-hidden="true"></span><br />
                                <code>fa-digg [ &amp;#xf1a6; ]</code></li>
                            <li> <span class="fa fa-dollar  gly-2" aria-hidden="true"></span><br />
                                <code>fa-dollar  [ &amp;#xf155; ]</code></li>
                            <li> <span class="fa fa-dot-circle-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-dot-circle-o [ &amp;#xf192; ]</code></li>
                            <li> <span class="fa fa-download gly-2" aria-hidden="true"></span><br />
                                <code>fa-download [ &amp;#xf019; ]</code></li>
                            <li> <span class="fa fa-dribbble gly-2" aria-hidden="true"></span><br />
                                <code>fa-dribbble [ &amp;#xf17d; ]</code></li>
                            <li> <span class="fa fa-dropbox gly-2" aria-hidden="true"></span><br />
                                <code>fa-dropbox [ &amp;#xf16b; ]</code></li>
                            <li> <span class="fa fa-drupal gly-2" aria-hidden="true"></span><br />
                                <code>fa-drupal [ &amp;#xf1a9; ]</code></li>
                            <li> <span class="fa fa-edge gly-2" aria-hidden="true"></span><br />
                                <code>fa-edge [ &amp;#xf282; ]</code></li>
                            <li> <span class="fa fa-edit  gly-2" aria-hidden="true"></span><br />
                                <code>fa-edit  [ &amp;#xf044; ]</code></li>
                            <li> <span class="fa fa-eject gly-2" aria-hidden="true"></span><br />
                                <code>fa-eject [ &amp;#xf052; ]</code></li>
                            <li> <span class="fa fa-ellipsis-h gly-2" aria-hidden="true"></span><br />
                                <code>fa-ellipsis-h [ &amp;#xf141; ]</code></li>
                            <li> <span class="fa fa-ellipsis-v gly-2" aria-hidden="true"></span><br />
                                <code>fa-ellipsis-v [ &amp;#xf142; ]</code></li>
                            <li> <span class="fa fa-empire gly-2" aria-hidden="true"></span><br />
                                <code>fa-empire [ &amp;#xf1d1; ]</code></li>
                            <li> <span class="fa fa-envelope gly-2" aria-hidden="true"></span><br />
                                <code>fa-envelope [ &amp;#xf0e0; ]</code></li>
                            <li> <span class="fa fa-envelope-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-envelope-o [ &amp;#xf003; ]</code></li>
                            <li> <span class="fa fa-envelope-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-envelope-square [ &amp;#xf199; ]</code></li>
                            <li> <span class="fa fa-eraser gly-2" aria-hidden="true"></span><br />
                                <code>fa-eraser [ &amp;#xf12d; ]</code></li>
                            <li> <span class="fa fa-eur gly-2" aria-hidden="true"></span><br />
                                <code>fa-eur [ &amp;#xf153; ]</code></li>
                            <li> <span class="fa fa-euro  gly-2" aria-hidden="true"></span><br />
                                <code>fa-euro  [ &amp;#xf153; ]</code></li>
                            <li> <span class="fa fa-exchange gly-2" aria-hidden="true"></span><br />
                                <code>fa-exchange [ &amp;#xf0ec; ]</code></li>
                            <li> <span class="fa fa-exclamation gly-2" aria-hidden="true"></span><br />
                                <code>fa-exclamation [ &amp;#xf12a; ]</code></li>
                            <li> <span class="fa fa-exclamation-circle gly-2" aria-hidden="true"></span><br />
                                <code>fa-exclamation-circle [ &amp;#xf06a; ]</code></li>
                            <li> <span class="fa fa-exclamation-triangle gly-2" aria-hidden="true"></span><br />
                                <code>fa-exclamation-triangle [ &amp;#xf071; ]</code></li>
                            <li> <span class="fa fa-expand gly-2" aria-hidden="true"></span><br />
                                <code>fa-expand [ &amp;#xf065; ]</code></li>
                            <li> <span class="fa fa-expeditedssl gly-2" aria-hidden="true"></span><br />
                                <code>fa-expeditedssl [ &amp;#xf23e; ]</code></li>
                            <li> <span class="fa fa-external-link gly-2" aria-hidden="true"></span><br />
                                <code>fa-external-link [ &amp;#xf08e; ]</code></li>
                            <li> <span class="fa fa-external-link-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-external-link-square [ &amp;#xf14c; ]</code></li>
                            <li> <span class="fa fa-eye gly-2" aria-hidden="true"></span><br />
                                <code>fa-eye [ &amp;#xf06e; ]</code></li>
                            <li> <span class="fa fa-eye-slash gly-2" aria-hidden="true"></span><br />
                                <code>fa-eye-slash [ &amp;#xf070; ]</code></li>
                            <li> <span class="fa fa-eyedropper gly-2" aria-hidden="true"></span><br />
                                <code>fa-eyedropper [ &amp;#xf1fb; ]</code></li>
                            <li> <span class="fa fa-facebook gly-2" aria-hidden="true"></span><br />
                                <code>fa-facebook [ &amp;#xf09a; ]</code></li>
                            <li> <span class="fa fa-facebook-f  gly-2" aria-hidden="true"></span><br />
                                <code>fa-facebook-f  [ &amp;#xf09a; ]</code></li>
                            <li> <span class="fa fa-facebook-official gly-2" aria-hidden="true"></span><br />
                                <code>fa-facebook-official [ &amp;#xf230; ]</code></li>
                            <li> <span class="fa fa-facebook-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-facebook-square [ &amp;#xf082; ]</code></li>
                            <li> <span class="fa fa-fast-backward gly-2" aria-hidden="true"></span><br />
                                <code>fa-fast-backward [ &amp;#xf049; ]</code></li>
                            <li> <span class="fa fa-fast-forward gly-2" aria-hidden="true"></span><br />
                                <code>fa-fast-forward [ &amp;#xf050; ]</code></li>
                            <li> <span class="fa fa-fax gly-2" aria-hidden="true"></span><br />
                                <code>fa-fax [ &amp;#xf1ac; ]</code></li>
                            <li> <span class="fa fa-feed  gly-2" aria-hidden="true"></span><br />
                                <code>fa-feed  [ &amp;#xf09e; ]</code></li>
                            <li> <span class="fa fa-female gly-2" aria-hidden="true"></span><br />
                                <code>fa-female [ &amp;#xf182; ]</code></li>
                            <li> <span class="fa fa-fighter-jet gly-2" aria-hidden="true"></span><br />
                                <code>fa-fighter-jet [ &amp;#xf0fb; ]</code></li>
                            <li> <span class="fa fa-file gly-2" aria-hidden="true"></span><br />
                                <code>fa-file [ &amp;#xf15b; ]</code></li>
                            <li> <span class="fa fa-file-archive-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-file-archive-o [ &amp;#xf1c6; ]</code></li>
                        <ul>
                    </div>
                    <div class="col-md-4">
                        <ul  class="list-unstyled">
                            <li> <span class="fa fa-file-audio-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-file-audio-o [ &amp;#xf1c7; ]</code></li>
                            <li> <span class="fa fa-file-code-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-file-code-o [ &amp;#xf1c9; ]</code></li>
                            <li> <span class="fa fa-file-excel-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-file-excel-o [ &amp;#xf1c3; ]</code></li>
                            <li> <span class="fa fa-file-image-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-file-image-o [ &amp;#xf1c5; ]</code></li>
                            <li> <span class="fa fa-file-movie-o  gly-2" aria-hidden="true"></span><br />
                                <code>fa-file-movie-o  [ &amp;#xf1c8; ]</code></li>
                            <li> <span class="fa fa-file-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-file-o [ &amp;#xf016; ]</code></li>
                            <li> <span class="fa fa-file-pdf-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-file-pdf-o [ &amp;#xf1c1; ]</code></li>
                            <li> <span class="fa fa-file-photo-o  gly-2" aria-hidden="true"></span><br />
                                <code>fa-file-photo-o  [ &amp;#xf1c5; ]</code></li>
                            <li> <span class="fa fa-file-picture-o  gly-2" aria-hidden="true"></span><br />
                                <code>fa-file-picture-o  [ &amp;#xf1c5; ]</code></li>
                            <li> <span class="fa fa-file-powerpoint-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-file-powerpoint-o [ &amp;#xf1c4; ]</code></li>
                            <li> <span class="fa fa-file-sound-o  gly-2" aria-hidden="true"></span><br />
                                <code>fa-file-sound-o  [ &amp;#xf1c7; ]</code></li>
                            <li> <span class="fa fa-file-text gly-2" aria-hidden="true"></span><br />
                                <code>fa-file-text [ &amp;#xf15c; ]</code></li>
                            <li> <span class="fa fa-file-text-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-file-text-o [ &amp;#xf0f6; ]</code></li>
                            <li> <span class="fa fa-file-video-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-file-video-o [ &amp;#xf1c8; ]</code></li>
                            <li> <span class="fa fa-file-word-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-file-word-o [ &amp;#xf1c2; ]</code></li>
                            <li> <span class="fa fa-file-zip-o  gly-2" aria-hidden="true"></span><br />
                                <code>fa-file-zip-o  [ &amp;#xf1c6; ]</code></li>
                            <li> <span class="fa fa-files-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-files-o [ &amp;#xf0c5; ]</code></li>
                            <li> <span class="fa fa-film gly-2" aria-hidden="true"></span><br />
                                <code>fa-film [ &amp;#xf008; ]</code></li>
                            <li> <span class="fa fa-filter gly-2" aria-hidden="true"></span><br />
                                <code>fa-filter [ &amp;#xf0b0; ]</code></li>
                            <li> <span class="fa fa-fire gly-2" aria-hidden="true"></span><br />
                                <code>fa-fire [ &amp;#xf06d; ]</code></li>
                            <li> <span class="fa fa-fire-extinguisher gly-2" aria-hidden="true"></span><br />
                                <code>fa-fire-extinguisher [ &amp;#xf134; ]</code></li>
                            <li> <span class="fa fa-firefox gly-2" aria-hidden="true"></span><br />
                                <code>fa-firefox [ &amp;#xf269; ]</code></li>
                            <li> <span class="fa fa-flag gly-2" aria-hidden="true"></span><br />
                                <code>fa-flag [ &amp;#xf024; ]</code></li>
                            <li> <span class="fa fa-flag-checkered gly-2" aria-hidden="true"></span><br />
                                <code>fa-flag-checkered [ &amp;#xf11e; ]</code></li>
                            <li> <span class="fa fa-flag-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-flag-o [ &amp;#xf11d; ]</code></li>
                            <li> <span class="fa fa-flash  gly-2" aria-hidden="true"></span><br />
                                <code>fa-flash  [ &amp;#xf0e7; ]</code></li>
                            <li> <span class="fa fa-flask gly-2" aria-hidden="true"></span><br />
                                <code>fa-flask [ &amp;#xf0c3; ]</code></li>
                            <li> <span class="fa fa-flickr gly-2" aria-hidden="true"></span><br />
                                <code>fa-flickr [ &amp;#xf16e; ]</code></li>
                            <li> <span class="fa fa-floppy-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-floppy-o [ &amp;#xf0c7; ]</code></li>
                            <li> <span class="fa fa-folder gly-2" aria-hidden="true"></span><br />
                                <code>fa-folder [ &amp;#xf07b; ]</code></li>
                            <li> <span class="fa fa-folder-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-folder-o [ &amp;#xf114; ]</code></li>
                            <li> <span class="fa fa-folder-open gly-2" aria-hidden="true"></span><br />
                                <code>fa-folder-open [ &amp;#xf07c; ]</code></li>
                            <li> <span class="fa fa-folder-open-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-folder-open-o [ &amp;#xf115; ]</code></li>
                            <li> <span class="fa fa-font gly-2" aria-hidden="true"></span><br />
                                <code>fa-font [ &amp;#xf031; ]</code></li>
                            <li> <span class="fa fa-fonticons gly-2" aria-hidden="true"></span><br />
                                <code>fa-fonticons [ &amp;#xf280; ]</code></li>
                            <li> <span class="fa fa-fort-awesome gly-2" aria-hidden="true"></span><br />
                                <code>fa-fort-awesome [ &amp;#xf286; ]</code></li>
                            <li> <span class="fa fa-forumbee gly-2" aria-hidden="true"></span><br />
                                <code>fa-forumbee [ &amp;#xf211; ]</code></li>
                            <li> <span class="fa fa-forward gly-2" aria-hidden="true"></span><br />
                                <code>fa-forward [ &amp;#xf04e; ]</code></li>
                            <li> <span class="fa fa-foursquare gly-2" aria-hidden="true"></span><br />
                                <code>fa-foursquare [ &amp;#xf180; ]</code></li>
                            <li> <span class="fa fa-frown-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-frown-o [ &amp;#xf119; ]</code></li>
                            <li> <span class="fa fa-futbol-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-futbol-o [ &amp;#xf1e3; ]</code></li>
                            <li> <span class="fa fa-gamepad gly-2" aria-hidden="true"></span><br />
                                <code>fa-gamepad [ &amp;#xf11b; ]</code></li>
                            <li> <span class="fa fa-gavel gly-2" aria-hidden="true"></span><br />
                                <code>fa-gavel [ &amp;#xf0e3; ]</code></li>
                            <li> <span class="fa fa-gbp gly-2" aria-hidden="true"></span><br />
                                <code>fa-gbp [ &amp;#xf154; ]</code></li>
                            <li> <span class="fa fa-ge  gly-2" aria-hidden="true"></span><br />
                                <code>fa-ge  [ &amp;#xf1d1; ]</code></li>
                            <li> <span class="fa fa-gear  gly-2" aria-hidden="true"></span><br />
                                <code>fa-gear  [ &amp;#xf013; ]</code></li>
                            <li> <span class="fa fa-gears  gly-2" aria-hidden="true"></span><br />
                                <code>fa-gears  [ &amp;#xf085; ]</code></li>
                            <li> <span class="fa fa-genderless gly-2" aria-hidden="true"></span><br />
                                <code>fa-genderless [ &amp;#xf22d; ]</code></li>
                            <li> <span class="fa fa-get-pocket gly-2" aria-hidden="true"></span><br />
                                <code>fa-get-pocket [ &amp;#xf265; ]</code></li>
                            <li> <span class="fa fa-gg gly-2" aria-hidden="true"></span><br />
                                <code>fa-gg [ &amp;#xf260; ]</code></li>
                            <li> <span class="fa fa-gg-circle gly-2" aria-hidden="true"></span><br />
                                <code>fa-gg-circle [ &amp;#xf261; ]</code></li>
                            <li> <span class="fa fa-gift gly-2" aria-hidden="true"></span><br />
                                <code>fa-gift [ &amp;#xf06b; ]</code></li>
                            <li> <span class="fa fa-git gly-2" aria-hidden="true"></span><br />
                                <code>fa-git [ &amp;#xf1d3; ]</code></li>
                            <li> <span class="fa fa-git-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-git-square [ &amp;#xf1d2; ]</code></li>
                            <li> <span class="fa fa-github gly-2" aria-hidden="true"></span><br />
                                <code>fa-github [ &amp;#xf09b; ]</code></li>
                            <li> <span class="fa fa-github-alt gly-2" aria-hidden="true"></span><br />
                                <code>fa-github-alt [ &amp;#xf113; ]</code></li>
                            <li> <span class="fa fa-github-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-github-square [ &amp;#xf092; ]</code></li>
                            <li> <span class="fa fa-gittip  gly-2" aria-hidden="true"></span><br />
                                <code>fa-gittip  [ &amp;#xf184; ]</code></li>
                            <li> <span class="fa fa-glass gly-2" aria-hidden="true"></span><br />
                                <code>fa-glass [ &amp;#xf000; ]</code></li>
                            <li> <span class="fa fa-globe gly-2" aria-hidden="true"></span><br />
                                <code>fa-globe [ &amp;#xf0ac; ]</code></li>
                            <li> <span class="fa fa-google gly-2" aria-hidden="true"></span><br />
                                <code>fa-google [ &amp;#xf1a0; ]</code></li>
                            <li> <span class="fa fa-google-plus gly-2" aria-hidden="true"></span><br />
                                <code>fa-google-plus [ &amp;#xf0d5; ]</code></li>
                            <li> <span class="fa fa-google-plus-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-google-plus-square [ &amp;#xf0d4; ]</code></li>
                            <li> <span class="fa fa-google-wallet gly-2" aria-hidden="true"></span><br />
                                <code>fa-google-wallet [ &amp;#xf1ee; ]</code></li>
                            <li> <span class="fa fa-graduation-cap gly-2" aria-hidden="true"></span><br />
                                <code>fa-graduation-cap [ &amp;#xf19d; ]</code></li>
                            <li> <span class="fa fa-gratipay gly-2" aria-hidden="true"></span><br />
                                <code>fa-gratipay [ &amp;#xf184; ]</code></li>
                            <li> <span class="fa fa-group  gly-2" aria-hidden="true"></span><br />
                                <code>fa-group  [ &amp;#xf0c0; ]</code></li>
                            <li> <span class="fa fa-h-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-h-square [ &amp;#xf0fd; ]</code></li>
                            <li> <span class="fa fa-hacker-news gly-2" aria-hidden="true"></span><br />
                                <code>fa-hacker-news [ &amp;#xf1d4; ]</code></li>
                            <li> <span class="fa fa-hand-grab-o  gly-2" aria-hidden="true"></span><br />
                                <code>fa-hand-grab-o  [ &amp;#xf255; ]</code></li>
                            <li> <span class="fa fa-hand-lizard-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-hand-lizard-o [ &amp;#xf258; ]</code></li>
                            <li> <span class="fa fa-hand-o-down gly-2" aria-hidden="true"></span><br />
                                <code>fa-hand-o-down [ &amp;#xf0a7; ]</code></li>
                            <li> <span class="fa fa-hand-o-left gly-2" aria-hidden="true"></span><br />
                                <code>fa-hand-o-left [ &amp;#xf0a5; ]</code></li>
                            <li> <span class="fa fa-hand-o-right gly-2" aria-hidden="true"></span><br />
                                <code>fa-hand-o-right [ &amp;#xf0a4; ]</code></li>
                            <li> <span class="fa fa-hand-o-up gly-2" aria-hidden="true"></span><br />
                                <code>fa-hand-o-up [ &amp;#xf0a6; ]</code></li>
                            <li> <span class="fa fa-hand-paper-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-hand-paper-o [ &amp;#xf256; ]</code></li>
                            <li> <span class="fa fa-hand-peace-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-hand-peace-o [ &amp;#xf25b; ]</code></li>
                            <li> <span class="fa fa-hand-pointer-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-hand-pointer-o [ &amp;#xf25a; ]</code></li>
                            <li> <span class="fa fa-hand-rock-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-hand-rock-o [ &amp;#xf255; ]</code></li>
                            <li> <span class="fa fa-hand-scissors-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-hand-scissors-o [ &amp;#xf257; ]</code></li>
                            <li> <span class="fa fa-hand-spock-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-hand-spock-o [ &amp;#xf259; ]</code></li>
                            <li> <span class="fa fa-hand-stop-o  gly-2" aria-hidden="true"></span><br />
                                <code>fa-hand-stop-o  [ &amp;#xf256; ]</code></li>
                            <li> <span class="fa fa-hashtag gly-2" aria-hidden="true"></span><br />
                                <code>fa-hashtag [ &amp;#xf292; ]</code></li>
                            <li> <span class="fa fa-hdd-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-hdd-o [ &amp;#xf0a0; ]</code></li>
                            <li> <span class="fa fa-header gly-2" aria-hidden="true"></span><br />
                                <code>fa-header [ &amp;#xf1dc; ]</code></li>
                            <li> <span class="fa fa-headphones gly-2" aria-hidden="true"></span><br />
                                <code>fa-headphones [ &amp;#xf025; ]</code></li>
                            <li> <span class="fa fa-heart gly-2" aria-hidden="true"></span><br />
                                <code>fa-heart [ &amp;#xf004; ]</code></li>
                            <li> <span class="fa fa-heart-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-heart-o [ &amp;#xf08a; ]</code></li>
                            <li> <span class="fa fa-heartbeat gly-2" aria-hidden="true"></span><br />
                                <code>fa-heartbeat [ &amp;#xf21e; ]</code></li>
                            <li> <span class="fa fa-history gly-2" aria-hidden="true"></span><br />
                                <code>fa-history [ &amp;#xf1da; ]</code></li>
                            <li> <span class="fa fa-home gly-2" aria-hidden="true"></span><br />
                                <code>fa-home [ &amp;#xf015; ]</code></li>
                            <li> <span class="fa fa-hospital-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-hospital-o [ &amp;#xf0f8; ]</code></li>
                            <li> <span class="fa fa-hotel  gly-2" aria-hidden="true"></span><br />
                                <code>fa-hotel  [ &amp;#xf236; ]</code></li>
                            <li> <span class="fa fa-hourglass gly-2" aria-hidden="true"></span><br />
                                <code>fa-hourglass [ &amp;#xf254; ]</code></li>
                            <li> <span class="fa fa-hourglass-1  gly-2" aria-hidden="true"></span><br />
                                <code>fa-hourglass-1  [ &amp;#xf251; ]</code></li>
                            <li> <span class="fa fa-hourglass-2  gly-2" aria-hidden="true"></span><br />
                                <code>fa-hourglass-2  [ &amp;#xf252; ]</code></li>
                            <li> <span class="fa fa-hourglass-3  gly-2" aria-hidden="true"></span><br />
                                <code>fa-hourglass-3  [ &amp;#xf253; ]</code></li>
                            <li> <span class="fa fa-hourglass-end gly-2" aria-hidden="true"></span><br />
                                <code>fa-hourglass-end [ &amp;#xf253; ]</code></li>
                            <li> <span class="fa fa-hourglass-half gly-2" aria-hidden="true"></span><br />
                                <code>fa-hourglass-half [ &amp;#xf252; ]</code></li>
                            <li> <span class="fa fa-hourglass-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-hourglass-o [ &amp;#xf250; ]</code></li>
                            <li> <span class="fa fa-hourglass-start gly-2" aria-hidden="true"></span><br />
                                <code>fa-hourglass-start [ &amp;#xf251; ]</code></li>
                            <li> <span class="fa fa-houzz gly-2" aria-hidden="true"></span><br />
                                <code>fa-houzz [ &amp;#xf27c; ]</code></li>
                            <li> <span class="fa fa-html5 gly-2" aria-hidden="true"></span><br />
                                <code>fa-html5 [ &amp;#xf13b; ]</code></li>
                            <li> <span class="fa fa-i-cursor gly-2" aria-hidden="true"></span><br />
                                <code>fa-i-cursor [ &amp;#xf246; ]</code></li>
                            <li> <span class="fa fa-ils gly-2" aria-hidden="true"></span><br />
                                <code>fa-ils [ &amp;#xf20b; ]</code></li>
                            <li> <span class="fa fa-image  gly-2" aria-hidden="true"></span><br />
                                <code>fa-image  [ &amp;#xf03e; ]</code></li>
                            <li> <span class="fa fa-inbox gly-2" aria-hidden="true"></span><br />
                                <code>fa-inbox [ &amp;#xf01c; ]</code></li>
                            <li> <span class="fa fa-indent gly-2" aria-hidden="true"></span><br />
                                <code>fa-indent [ &amp;#xf03c; ]</code></li>
                            <li> <span class="fa fa-industry gly-2" aria-hidden="true"></span><br />
                                <code>fa-industry [ &amp;#xf275; ]</code></li>
                            <li> <span class="fa fa-info gly-2" aria-hidden="true"></span><br />
                                <code>fa-info [ &amp;#xf129; ]</code></li>
                            <li> <span class="fa fa-info-circle gly-2" aria-hidden="true"></span><br />
                                <code>fa-info-circle [ &amp;#xf05a; ]</code></li>
                            <li> <span class="fa fa-inr gly-2" aria-hidden="true"></span><br />
                                <code>fa-inr [ &amp;#xf156; ]</code></li>
                            <li> <span class="fa fa-instagram gly-2" aria-hidden="true"></span><br />
                                <code>fa-instagram [ &amp;#xf16d; ]</code></li>
                            <li> <span class="fa fa-institution  gly-2" aria-hidden="true"></span><br />
                                <code>fa-institution  [ &amp;#xf19c; ]</code></li>
                            <li> <span class="fa fa-internet-explorer gly-2" aria-hidden="true"></span><br />
                                <code>fa-internet-explorer [ &amp;#xf26b; ]</code></li>
                            <li> <span class="fa fa-intersex  gly-2" aria-hidden="true"></span><br />
                                <code>fa-intersex  [ &amp;#xf224; ]</code></li>
                            <li> <span class="fa fa-ioxhost gly-2" aria-hidden="true"></span><br />
                                <code>fa-ioxhost [ &amp;#xf208; ]</code></li>
                            <li> <span class="fa fa-italic gly-2" aria-hidden="true"></span><br />
                                <code>fa-italic [ &amp;#xf033; ]</code></li>
                            <li> <span class="fa fa-joomla gly-2" aria-hidden="true"></span><br />
                                <code>fa-joomla [ &amp;#xf1aa; ]</code></li>
                            <li> <span class="fa fa-jpy gly-2" aria-hidden="true"></span><br />
                                <code>fa-jpy [ &amp;#xf157; ]</code></li>
                            <li> <span class="fa fa-jsfiddle gly-2" aria-hidden="true"></span><br />
                                <code>fa-jsfiddle [ &amp;#xf1cc; ]</code></li>
                            <li> <span class="fa fa-key gly-2" aria-hidden="true"></span><br />
                                <code>fa-key [ &amp;#xf084; ]</code></li>
                            <li> <span class="fa fa-keyboard-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-keyboard-o [ &amp;#xf11c; ]</code></li>
                            <li> <span class="fa fa-krw gly-2" aria-hidden="true"></span><br />
                                <code>fa-krw [ &amp;#xf159; ]</code></li>
                            <li> <span class="fa fa-language gly-2" aria-hidden="true"></span><br />
                                <code>fa-language [ &amp;#xf1ab; ]</code></li>
                            <li> <span class="fa fa-laptop gly-2" aria-hidden="true"></span><br />
                                <code>fa-laptop [ &amp;#xf109; ]</code></li>
                            <li> <span class="fa fa-lastfm gly-2" aria-hidden="true"></span><br />
                                <code>fa-lastfm [ &amp;#xf202; ]</code></li>
                            <li> <span class="fa fa-lastfm-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-lastfm-square [ &amp;#xf203; ]</code></li>
                            <li> <span class="fa fa-leaf gly-2" aria-hidden="true"></span><br />
                                <code>fa-leaf [ &amp;#xf06c; ]</code></li>
                            <li> <span class="fa fa-leanpub gly-2" aria-hidden="true"></span><br />
                                <code>fa-leanpub [ &amp;#xf212; ]</code></li>
                            <li> <span class="fa fa-legal  gly-2" aria-hidden="true"></span><br />
                                <code>fa-legal  [ &amp;#xf0e3; ]</code></li>
                            <li> <span class="fa fa-lemon-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-lemon-o [ &amp;#xf094; ]</code></li>
                            <li> <span class="fa fa-level-down gly-2" aria-hidden="true"></span><br />
                                <code>fa-level-down [ &amp;#xf149; ]</code></li>
                            <li> <span class="fa fa-level-up gly-2" aria-hidden="true"></span><br />
                                <code>fa-level-up [ &amp;#xf148; ]</code></li>
                            <li> <span class="fa fa-life-bouy  gly-2" aria-hidden="true"></span><br />
                                <code>fa-life-bouy  [ &amp;#xf1cd; ]</code></li>
                            <li> <span class="fa fa-life-buoy  gly-2" aria-hidden="true"></span><br />
                                <code>fa-life-buoy  [ &amp;#xf1cd; ]</code></li>
                            <li> <span class="fa fa-life-ring gly-2" aria-hidden="true"></span><br />
                                <code>fa-life-ring [ &amp;#xf1cd; ]</code></li>
                            <li> <span class="fa fa-life-saver  gly-2" aria-hidden="true"></span><br />
                                <code>fa-life-saver  [ &amp;#xf1cd; ]</code></li>
                            <li> <span class="fa fa-lightbulb-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-lightbulb-o [ &amp;#xf0eb; ]</code></li>
                            <li> <span class="fa fa-line-chart gly-2" aria-hidden="true"></span><br />
                                <code>fa-line-chart [ &amp;#xf201; ]</code></li>
                            <li> <span class="fa fa-link gly-2" aria-hidden="true"></span><br />
                                <code>fa-link [ &amp;#xf0c1; ]</code></li>
                            <li> <span class="fa fa-linkedin gly-2" aria-hidden="true"></span><br />
                                <code>fa-linkedin [ &amp;#xf0e1; ]</code></li>
                            <li> <span class="fa fa-linkedin-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-linkedin-square [ &amp;#xf08c; ]</code></li>
                            <li> <span class="fa fa-linux gly-2" aria-hidden="true"></span><br />
                                <code>fa-linux [ &amp;#xf17c; ]</code></li>
                            <li> <span class="fa fa-list gly-2" aria-hidden="true"></span><br />
                                <code>fa-list [ &amp;#xf03a; ]</code></li>
                            <li> <span class="fa fa-list-alt gly-2" aria-hidden="true"></span><br />
                                <code>fa-list-alt [ &amp;#xf022; ]</code></li>
                            <li> <span class="fa fa-list-ol gly-2" aria-hidden="true"></span><br />
                                <code>fa-list-ol [ &amp;#xf0cb; ]</code></li>
                            <li> <span class="fa fa-list-ul gly-2" aria-hidden="true"></span><br />
                                <code>fa-list-ul [ &amp;#xf0ca; ]</code></li>
                            <li> <span class="fa fa-location-arrow gly-2" aria-hidden="true"></span><br />
                                <code>fa-location-arrow [ &amp;#xf124; ]</code></li>
                            <li> <span class="fa fa-lock gly-2" aria-hidden="true"></span><br />
                                <code>fa-lock [ &amp;#xf023; ]</code></li>
                            <li> <span class="fa fa-long-arrow-down gly-2" aria-hidden="true"></span><br />
                                <code>fa-long-arrow-down [ &amp;#xf175; ]</code></li>
                            <li> <span class="fa fa-long-arrow-left gly-2" aria-hidden="true"></span><br />
                                <code>fa-long-arrow-left [ &amp;#xf177; ]</code></li>
                            <li> <span class="fa fa-long-arrow-right gly-2" aria-hidden="true"></span><br />
                                <code>fa-long-arrow-right [ &amp;#xf178; ]</code></li>
                            <li> <span class="fa fa-long-arrow-up gly-2" aria-hidden="true"></span><br />
                                <code>fa-long-arrow-up [ &amp;#xf176; ]</code></li>
                            <li> <span class="fa fa-magic gly-2" aria-hidden="true"></span><br />
                                <code>fa-magic [ &amp;#xf0d0; ]</code></li>
                            <li> <span class="fa fa-magnet gly-2" aria-hidden="true"></span><br />
                                <code>fa-magnet [ &amp;#xf076; ]</code></li>
                            <li> <span class="fa fa-mail-forward  gly-2" aria-hidden="true"></span><br />
                                <code>fa-mail-forward  [ &amp;#xf064; ]</code></li>
                            <li> <span class="fa fa-mail-reply  gly-2" aria-hidden="true"></span><br />
                                <code>fa-mail-reply  [ &amp;#xf112; ]</code></li>
                            <li> <span class="fa fa-mail-reply-all  gly-2" aria-hidden="true"></span><br />
                                <code>fa-mail-reply-all  [ &amp;#xf122; ]</code></li>
                            <li> <span class="fa fa-male gly-2" aria-hidden="true"></span><br />
                                <code>fa-male [ &amp;#xf183; ]</code></li>
                            <li> <span class="fa fa-map gly-2" aria-hidden="true"></span><br />
                                <code>fa-map [ &amp;#xf279; ]</code></li>
                            <li> <span class="fa fa-map-marker gly-2" aria-hidden="true"></span><br />
                                <code>fa-map-marker [ &amp;#xf041; ]</code></li>
                            <li> <span class="fa fa-map-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-map-o [ &amp;#xf278; ]</code></li>
                            <li> <span class="fa fa-map-pin gly-2" aria-hidden="true"></span><br />
                                <code>fa-map-pin [ &amp;#xf276; ]</code></li>
                            <li> <span class="fa fa-map-signs gly-2" aria-hidden="true"></span><br />
                                <code>fa-map-signs [ &amp;#xf277; ]</code></li>
                            <li> <span class="fa fa-mars gly-2" aria-hidden="true"></span><br />
                                <code>fa-mars [ &amp;#xf222; ]</code></li>
                            <li> <span class="fa fa-mars-double gly-2" aria-hidden="true"></span><br />
                                <code>fa-mars-double [ &amp;#xf227; ]</code></li>
                            <li> <span class="fa fa-mars-stroke gly-2" aria-hidden="true"></span><br />
                                <code>fa-mars-stroke [ &amp;#xf229; ]</code></li>
                            <li> <span class="fa fa-mars-stroke-h gly-2" aria-hidden="true"></span><br />
                                <code>fa-mars-stroke-h [ &amp;#xf22b; ]</code></li>
                            <li> <span class="fa fa-mars-stroke-v gly-2" aria-hidden="true"></span><br />
                                <code>fa-mars-stroke-v [ &amp;#xf22a; ]</code></li>
                            <li> <span class="fa fa-maxcdn gly-2" aria-hidden="true"></span><br />
                                <code>fa-maxcdn [ &amp;#xf136; ]</code></li>
                            <li> <span class="fa fa-meanpath gly-2" aria-hidden="true"></span><br />
                                <code>fa-meanpath [ &amp;#xf20c; ]</code></li>
                            <li> <span class="fa fa-medium gly-2" aria-hidden="true"></span><br />
                                <code>fa-medium [ &amp;#xf23a; ]</code></li>
                            <li> <span class="fa fa-medkit gly-2" aria-hidden="true"></span><br />
                                <code>fa-medkit [ &amp;#xf0fa; ]</code></li>
                            <li> <span class="fa fa-meh-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-meh-o [ &amp;#xf11a; ]</code></li>
                            <li> <span class="fa fa-mercury gly-2" aria-hidden="true"></span><br />
                                <code>fa-mercury [ &amp;#xf223; ]</code></li>
                            <li> <span class="fa fa-microphone gly-2" aria-hidden="true"></span><br />
                                <code>fa-microphone [ &amp;#xf130; ]</code></li>
                            <li> <span class="fa fa-microphone-slash gly-2" aria-hidden="true"></span><br />
                                <code>fa-microphone-slash [ &amp;#xf131; ]</code></li>
                            <li> <span class="fa fa-minus gly-2" aria-hidden="true"></span><br />
                                <code>fa-minus [ &amp;#xf068; ]</code></li>
                            <li> <span class="fa fa-minus-circle gly-2" aria-hidden="true"></span><br />
                                <code>fa-minus-circle [ &amp;#xf056; ]</code></li>
                            <li> <span class="fa fa-minus-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-minus-square [ &amp;#xf146; ]</code></li>
                            <li> <span class="fa fa-minus-square-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-minus-square-o [ &amp;#xf147; ]</code></li>
                            <li> <span class="fa fa-mixcloud gly-2" aria-hidden="true"></span><br />
                                <code>fa-mixcloud [ &amp;#xf289; ]</code></li>
                            <li> <span class="fa fa-mobile gly-2" aria-hidden="true"></span><br />
                                <code>fa-mobile [ &amp;#xf10b; ]</code></li>
                            <li> <span class="fa fa-mobile-phone  gly-2" aria-hidden="true"></span><br />
                                <code>fa-mobile-phone  [ &amp;#xf10b; ]</code></li>
                            <li> <span class="fa fa-modx gly-2" aria-hidden="true"></span><br />
                                <code>fa-modx [ &amp;#xf285; ]</code></li>
                            <li> <span class="fa fa-money gly-2" aria-hidden="true"></span><br />
                                <code>fa-money [ &amp;#xf0d6; ]</code></li>
                            <li> <span class="fa fa-moon-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-moon-o [ &amp;#xf186; ]</code></li>
                            <li> <span class="fa fa-mortar-board  gly-2" aria-hidden="true"></span><br />
                                <code>fa-mortar-board  [ &amp;#xf19d; ]</code></li>
                            <li> <span class="fa fa-motorcycle gly-2" aria-hidden="true"></span><br />
                                <code>fa-motorcycle [ &amp;#xf21c; ]</code></li>
                            <li> <span class="fa fa-mouse-pointer gly-2" aria-hidden="true"></span><br />
                                <code>fa-mouse-pointer [ &amp;#xf245; ]</code></li>
                            <li> <span class="fa fa-music gly-2" aria-hidden="true"></span><br />
                                <code>fa-music [ &amp;#xf001; ]</code></li>
                            <li> <span class="fa fa-navicon  gly-2" aria-hidden="true"></span><br />
                                <code>fa-navicon  [ &amp;#xf0c9; ]</code></li>
                            <li> <span class="fa fa-neuter gly-2" aria-hidden="true"></span><br />
                                <code>fa-neuter [ &amp;#xf22c; ]</code></li>
                            <li> <span class="fa fa-newspaper-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-newspaper-o [ &amp;#xf1ea; ]</code></li>
                            <li> <span class="fa fa-object-group gly-2" aria-hidden="true"></span><br />
                                <code>fa-object-group [ &amp;#xf247; ]</code></li>
                            <li> <span class="fa fa-object-ungroup gly-2" aria-hidden="true"></span><br />
                                <code>fa-object-ungroup [ &amp;#xf248; ]</code></li>
                            <li> <span class="fa fa-odnoklassniki gly-2" aria-hidden="true"></span><br />
                                <code>fa-odnoklassniki [ &amp;#xf263; ]</code></li>
                            <li> <span class="fa fa-odnoklassniki-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-odnoklassniki-square [ &amp;#xf264; ]</code></li>
                            <li> <span class="fa fa-opencart gly-2" aria-hidden="true"></span><br />
                                <code>fa-opencart [ &amp;#xf23d; ]</code></li>
                            <li> <span class="fa fa-openid gly-2" aria-hidden="true"></span><br />
                                <code>fa-openid [ &amp;#xf19b; ]</code></li>
                            <li> <span class="fa fa-opera gly-2" aria-hidden="true"></span><br />
                                <code>fa-opera [ &amp;#xf26a; ]</code></li>
                            <li> <span class="fa fa-optin-monster gly-2" aria-hidden="true"></span><br />
                                <code>fa-optin-monster [ &amp;#xf23c; ]</code></li>
                            <li> <span class="fa fa-outdent gly-2" aria-hidden="true"></span><br />
                                <code>fa-outdent [ &amp;#xf03b; ]</code></li>
                            <li> <span class="fa fa-pagelines gly-2" aria-hidden="true"></span><br />
                                <code>fa-pagelines [ &amp;#xf18c; ]</code></li>
                            <li> <span class="fa fa-paint-brush gly-2" aria-hidden="true"></span><br />
                                <code>fa-paint-brush [ &amp;#xf1fc; ]</code></li>
                            <li> <span class="fa fa-paper-plane gly-2" aria-hidden="true"></span><br />
                                <code>fa-paper-plane [ &amp;#xf1d8; ]</code></li>
                            <li> <span class="fa fa-paper-plane-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-paper-plane-o [ &amp;#xf1d9; ]</code></li>
                            <li> <span class="fa fa-paperclip gly-2" aria-hidden="true"></span><br />
                                <code>fa-paperclip [ &amp;#xf0c6; ]</code></li>
                            <li> <span class="fa fa-paragraph gly-2" aria-hidden="true"></span><br />
                                <code>fa-paragraph [ &amp;#xf1dd; ]</code></li>
                            <li> <span class="fa fa-paste  gly-2" aria-hidden="true"></span><br />
                                <code>fa-paste  [ &amp;#xf0ea; ]</code></li>
                            <li> <span class="fa fa-pause gly-2" aria-hidden="true"></span><br />
                                <code>fa-pause [ &amp;#xf04c; ]</code></li>
                            <li> <span class="fa fa-pause-circle gly-2" aria-hidden="true"></span><br />
                                <code>fa-pause-circle [ &amp;#xf28b; ]</code></li>
                            <li> <span class="fa fa-pause-circle-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-pause-circle-o [ &amp;#xf28c; ]</code></li>
                            <li> <span class="fa fa-paw gly-2" aria-hidden="true"></span><br />
                                <code>fa-paw [ &amp;#xf1b0; ]</code></li>
                            <li> <span class="fa fa-paypal gly-2" aria-hidden="true"></span><br />
                                <code>fa-paypal [ &amp;#xf1ed; ]</code></li>
                            <li> <span class="fa fa-pencil gly-2" aria-hidden="true"></span><br />
                                <code>fa-pencil [ &amp;#xf040; ]</code></li>
                            <li> <span class="fa fa-pencil-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-pencil-square [ &amp;#xf14b; ]</code></li>
                            <li> <span class="fa fa-pencil-square-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-pencil-square-o [ &amp;#xf044; ]</code></li>
                            <li> <span class="fa fa-percent gly-2" aria-hidden="true"></span><br />
                                <code>fa-percent [ &amp;#xf295; ]</code></li>
                            <li> <span class="fa fa-phone gly-2" aria-hidden="true"></span><br />
                                <code>fa-phone [ &amp;#xf095; ]</code></li>
                            <li> <span class="fa fa-phone-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-phone-square [ &amp;#xf098; ]</code></li>
                            <li> <span class="fa fa-photo  gly-2" aria-hidden="true"></span><br />
                                <code>fa-photo  [ &amp;#xf03e; ]</code></li>
                            <li> <span class="fa fa-picture-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-picture-o [ &amp;#xf03e; ]</code></li>
                            <li> <span class="fa fa-pie-chart gly-2" aria-hidden="true"></span><br />
                                <code>fa-pie-chart [ &amp;#xf200; ]</code></li>
                            <li> <span class="fa fa-pied-piper gly-2" aria-hidden="true"></span><br />
                                <code>fa-pied-piper [ &amp;#xf1a7; ]</code></li>
                            <li> <span class="fa fa-pied-piper-alt gly-2" aria-hidden="true"></span><br />
                                <code>fa-pied-piper-alt [ &amp;#xf1a8; ]</code></li>
                            <li> <span class="fa fa-pinterest gly-2" aria-hidden="true"></span><br />
                                <code>fa-pinterest [ &amp;#xf0d2; ]</code></li>
                            <li> <span class="fa fa-pinterest-p gly-2" aria-hidden="true"></span><br />
                                <code>fa-pinterest-p [ &amp;#xf231; ]</code></li>
                            <li> <span class="fa fa-pinterest-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-pinterest-square [ &amp;#xf0d3; ]</code></li>
                            <li> <span class="fa fa-plane gly-2" aria-hidden="true"></span><br />
                                <code>fa-plane [ &amp;#xf072; ]</code></li>
                            <li> <span class="fa fa-play gly-2" aria-hidden="true"></span><br />
                                <code>fa-play [ &amp;#xf04b; ]</code></li>
                        <ul>
                    </div>
                    <div class="col-md-4">
                        <ul  class="list-unstyled">
                            <li> <span class="fa fa-play-circle gly-2" aria-hidden="true"></span><br />
                                <code>fa-play-circle [ &amp;#xf144; ]</code></li>
                            <li> <span class="fa fa-play-circle-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-play-circle-o [ &amp;#xf01d; ]</code></li>
                            <li> <span class="fa fa-plug gly-2" aria-hidden="true"></span><br />
                                <code>fa-plug [ &amp;#xf1e6; ]</code></li>
                            <li> <span class="fa fa-plus gly-2" aria-hidden="true"></span><br />
                                <code>fa-plus [ &amp;#xf067; ]</code></li>
                            <li> <span class="fa fa-plus-circle gly-2" aria-hidden="true"></span><br />
                                <code>fa-plus-circle [ &amp;#xf055; ]</code></li>
                            <li> <span class="fa fa-plus-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-plus-square [ &amp;#xf0fe; ]</code></li>
                            <li> <span class="fa fa-plus-square-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-plus-square-o [ &amp;#xf196; ]</code></li>
                            <li> <span class="fa fa-power-off gly-2" aria-hidden="true"></span><br />
                                <code>fa-power-off [ &amp;#xf011; ]</code></li>
                            <li> <span class="fa fa-print gly-2" aria-hidden="true"></span><br />
                                <code>fa-print [ &amp;#xf02f; ]</code></li>
                            <li> <span class="fa fa-product-hunt gly-2" aria-hidden="true"></span><br />
                                <code>fa-product-hunt [ &amp;#xf288; ]</code></li>
                            <li> <span class="fa fa-puzzle-piece gly-2" aria-hidden="true"></span><br />
                                <code>fa-puzzle-piece [ &amp;#xf12e; ]</code></li>
                            <li> <span class="fa fa-qq gly-2" aria-hidden="true"></span><br />
                                <code>fa-qq [ &amp;#xf1d6; ]</code></li>
                            <li> <span class="fa fa-qrcode gly-2" aria-hidden="true"></span><br />
                                <code>fa-qrcode [ &amp;#xf029; ]</code></li>
                            <li> <span class="fa fa-question gly-2" aria-hidden="true"></span><br />
                                <code>fa-question [ &amp;#xf128; ]</code></li>
                            <li> <span class="fa fa-question-circle gly-2" aria-hidden="true"></span><br />
                                <code>fa-question-circle [ &amp;#xf059; ]</code></li>
                            <li> <span class="fa fa-quote-left gly-2" aria-hidden="true"></span><br />
                                <code>fa-quote-left [ &amp;#xf10d; ]</code></li>
                            <li> <span class="fa fa-quote-right gly-2" aria-hidden="true"></span><br />
                                <code>fa-quote-right [ &amp;#xf10e; ]</code></li>
                            <li> <span class="fa fa-ra  gly-2" aria-hidden="true"></span><br />
                                <code>fa-ra  [ &amp;#xf1d0; ]</code></li>
                            <li> <span class="fa fa-random gly-2" aria-hidden="true"></span><br />
                                <code>fa-random [ &amp;#xf074; ]</code></li>
                            <li> <span class="fa fa-rebel gly-2" aria-hidden="true"></span><br />
                                <code>fa-rebel [ &amp;#xf1d0; ]</code></li>
                            <li> <span class="fa fa-recycle gly-2" aria-hidden="true"></span><br />
                                <code>fa-recycle [ &amp;#xf1b8; ]</code></li>
                            <li> <span class="fa fa-reddit gly-2" aria-hidden="true"></span><br />
                                <code>fa-reddit [ &amp;#xf1a1; ]</code></li>
                            <li> <span class="fa fa-reddit-alien gly-2" aria-hidden="true"></span><br />
                                <code>fa-reddit-alien [ &amp;#xf281; ]</code></li>
                            <li> <span class="fa fa-reddit-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-reddit-square [ &amp;#xf1a2; ]</code></li>
                            <li> <span class="fa fa-refresh gly-2" aria-hidden="true"></span><br />
                                <code>fa-refresh [ &amp;#xf021; ]</code></li>
                            <li> <span class="fa fa-registered gly-2" aria-hidden="true"></span><br />
                                <code>fa-registered [ &amp;#xf25d; ]</code></li>
                            <li> <span class="fa fa-remove  gly-2" aria-hidden="true"></span><br />
                                <code>fa-remove  [ &amp;#xf00d; ]</code></li>
                            <li> <span class="fa fa-renren gly-2" aria-hidden="true"></span><br />
                                <code>fa-renren [ &amp;#xf18b; ]</code></li>
                            <li> <span class="fa fa-reorder  gly-2" aria-hidden="true"></span><br />
                                <code>fa-reorder  [ &amp;#xf0c9; ]</code></li>
                            <li> <span class="fa fa-repeat gly-2" aria-hidden="true"></span><br />
                                <code>fa-repeat [ &amp;#xf01e; ]</code></li>
                            <li> <span class="fa fa-reply gly-2" aria-hidden="true"></span><br />
                                <code>fa-reply [ &amp;#xf112; ]</code></li>
                            <li> <span class="fa fa-reply-all gly-2" aria-hidden="true"></span><br />
                                <code>fa-reply-all [ &amp;#xf122; ]</code></li>
                            <li> <span class="fa fa-retweet gly-2" aria-hidden="true"></span><br />
                                <code>fa-retweet [ &amp;#xf079; ]</code></li>
                            <li> <span class="fa fa-rmb  gly-2" aria-hidden="true"></span><br />
                                <code>fa-rmb  [ &amp;#xf157; ]</code></li>
                            <li> <span class="fa fa-road gly-2" aria-hidden="true"></span><br />
                                <code>fa-road [ &amp;#xf018; ]</code></li>
                            <li> <span class="fa fa-rocket gly-2" aria-hidden="true"></span><br />
                                <code>fa-rocket [ &amp;#xf135; ]</code></li>
                            <li> <span class="fa fa-rotate-left  gly-2" aria-hidden="true"></span><br />
                                <code>fa-rotate-left  [ &amp;#xf0e2; ]</code></li>
                            <li> <span class="fa fa-rotate-right  gly-2" aria-hidden="true"></span><br />
                                <code>fa-rotate-right  [ &amp;#xf01e; ]</code></li>
                            <li> <span class="fa fa-rouble  gly-2" aria-hidden="true"></span><br />
                                <code>fa-rouble  [ &amp;#xf158; ]</code></li>
                            <li> <span class="fa fa-rss gly-2" aria-hidden="true"></span><br />
                                <code>fa-rss [ &amp;#xf09e; ]</code></li>
                            <li> <span class="fa fa-rss-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-rss-square [ &amp;#xf143; ]</code></li>
                            <li> <span class="fa fa-rub gly-2" aria-hidden="true"></span><br />
                                <code>fa-rub [ &amp;#xf158; ]</code></li>
                            <li> <span class="fa fa-ruble  gly-2" aria-hidden="true"></span><br />
                                <code>fa-ruble  [ &amp;#xf158; ]</code></li>
                            <li> <span class="fa fa-rupee  gly-2" aria-hidden="true"></span><br />
                                <code>fa-rupee  [ &amp;#xf156; ]</code></li>
                            <li> <span class="fa fa-safari gly-2" aria-hidden="true"></span><br />
                                <code>fa-safari [ &amp;#xf267; ]</code></li>
                            <li> <span class="fa fa-save  gly-2" aria-hidden="true"></span><br />
                                <code>fa-save  [ &amp;#xf0c7; ]</code></li>
                            <li> <span class="fa fa-scissors gly-2" aria-hidden="true"></span><br />
                                <code>fa-scissors [ &amp;#xf0c4; ]</code></li>
                            <li> <span class="fa fa-scribd gly-2" aria-hidden="true"></span><br />
                                <code>fa-scribd [ &amp;#xf28a; ]</code></li>
                            <li> <span class="fa fa-search gly-2" aria-hidden="true"></span><br />
                                <code>fa-search [ &amp;#xf002; ]</code></li>
                            <li> <span class="fa fa-search-minus gly-2" aria-hidden="true"></span><br />
                                <code>fa-search-minus [ &amp;#xf010; ]</code></li>
                            <li> <span class="fa fa-search-plus gly-2" aria-hidden="true"></span><br />
                                <code>fa-search-plus [ &amp;#xf00e; ]</code></li>
                            <li> <span class="fa fa-sellsy gly-2" aria-hidden="true"></span><br />
                                <code>fa-sellsy [ &amp;#xf213; ]</code></li>
                            <li> <span class="fa fa-send  gly-2" aria-hidden="true"></span><br />
                                <code>fa-send  [ &amp;#xf1d8; ]</code></li>
                            <li> <span class="fa fa-send-o  gly-2" aria-hidden="true"></span><br />
                                <code>fa-send-o  [ &amp;#xf1d9; ]</code></li>
                            <li> <span class="fa fa-server gly-2" aria-hidden="true"></span><br />
                                <code>fa-server [ &amp;#xf233; ]</code></li>
                            <li> <span class="fa fa-share gly-2" aria-hidden="true"></span><br />
                                <code>fa-share [ &amp;#xf064; ]</code></li>
                            <li> <span class="fa fa-share-alt gly-2" aria-hidden="true"></span><br />
                                <code>fa-share-alt [ &amp;#xf1e0; ]</code></li>
                            <li> <span class="fa fa-share-alt-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-share-alt-square [ &amp;#xf1e1; ]</code></li>
                            <li> <span class="fa fa-share-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-share-square [ &amp;#xf14d; ]</code></li>
                            <li> <span class="fa fa-share-square-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-share-square-o [ &amp;#xf045; ]</code></li>
                            <li> <span class="fa fa-shekel  gly-2" aria-hidden="true"></span><br />
                                <code>fa-shekel  [ &amp;#xf20b; ]</code></li>
                            <li> <span class="fa fa-sheqel  gly-2" aria-hidden="true"></span><br />
                                <code>fa-sheqel  [ &amp;#xf20b; ]</code></li>
                            <li> <span class="fa fa-shield gly-2" aria-hidden="true"></span><br />
                                <code>fa-shield [ &amp;#xf132; ]</code></li>
                            <li> <span class="fa fa-ship gly-2" aria-hidden="true"></span><br />
                                <code>fa-ship [ &amp;#xf21a; ]</code></li>
                            <li> <span class="fa fa-shirtsinbulk gly-2" aria-hidden="true"></span><br />
                                <code>fa-shirtsinbulk [ &amp;#xf214; ]</code></li>
                            <li> <span class="fa fa-shopping-bag gly-2" aria-hidden="true"></span><br />
                                <code>fa-shopping-bag [ &amp;#xf290; ]</code></li>
                            <li> <span class="fa fa-shopping-basket gly-2" aria-hidden="true"></span><br />
                                <code>fa-shopping-basket [ &amp;#xf291; ]</code></li>
                            <li> <span class="fa fa-shopping-cart gly-2" aria-hidden="true"></span><br />
                                <code>fa-shopping-cart [ &amp;#xf07a; ]</code></li>
                            <li> <span class="fa fa-sign-in gly-2" aria-hidden="true"></span><br />
                                <code>fa-sign-in [ &amp;#xf090; ]</code></li>
                            <li> <span class="fa fa-sign-out gly-2" aria-hidden="true"></span><br />
                                <code>fa-sign-out [ &amp;#xf08b; ]</code></li>
                            <li> <span class="fa fa-signal gly-2" aria-hidden="true"></span><br />
                                <code>fa-signal [ &amp;#xf012; ]</code></li>
                            <li> <span class="fa fa-simplybuilt gly-2" aria-hidden="true"></span><br />
                                <code>fa-simplybuilt [ &amp;#xf215; ]</code></li>
                            <li> <span class="fa fa-sitemap gly-2" aria-hidden="true"></span><br />
                                <code>fa-sitemap [ &amp;#xf0e8; ]</code></li>
                            <li> <span class="fa fa-skyatlas gly-2" aria-hidden="true"></span><br />
                                <code>fa-skyatlas [ &amp;#xf216; ]</code></li>
                            <li> <span class="fa fa-skype gly-2" aria-hidden="true"></span><br />
                                <code>fa-skype [ &amp;#xf17e; ]</code></li>
                            <li> <span class="fa fa-slack gly-2" aria-hidden="true"></span><br />
                                <code>fa-slack [ &amp;#xf198; ]</code></li>
                            <li> <span class="fa fa-sliders gly-2" aria-hidden="true"></span><br />
                                <code>fa-sliders [ &amp;#xf1de; ]</code></li>
                            <li> <span class="fa fa-slideshare gly-2" aria-hidden="true"></span><br />
                                <code>fa-slideshare [ &amp;#xf1e7; ]</code></li>
                            <li> <span class="fa fa-smile-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-smile-o [ &amp;#xf118; ]</code></li>
                            <li> <span class="fa fa-soccer-ball-o  gly-2" aria-hidden="true"></span><br />
                                <code>fa-soccer-ball-o  [ &amp;#xf1e3; ]</code></li>
                            <li> <span class="fa fa-sort gly-2" aria-hidden="true"></span><br />
                                <code>fa-sort [ &amp;#xf0dc; ]</code></li>
                            <li> <span class="fa fa-sort-alpha-asc gly-2" aria-hidden="true"></span><br />
                                <code>fa-sort-alpha-asc [ &amp;#xf15d; ]</code></li>
                            <li> <span class="fa fa-sort-alpha-desc gly-2" aria-hidden="true"></span><br />
                                <code>fa-sort-alpha-desc [ &amp;#xf15e; ]</code></li>
                            <li> <span class="fa fa-sort-amount-asc gly-2" aria-hidden="true"></span><br />
                                <code>fa-sort-amount-asc [ &amp;#xf160; ]</code></li>
                            <li> <span class="fa fa-sort-amount-desc gly-2" aria-hidden="true"></span><br />
                                <code>fa-sort-amount-desc [ &amp;#xf161; ]</code></li>
                            <li> <span class="fa fa-sort-asc gly-2" aria-hidden="true"></span><br />
                                <code>fa-sort-asc [ &amp;#xf0de; ]</code></li>
                            <li> <span class="fa fa-sort-desc gly-2" aria-hidden="true"></span><br />
                                <code>fa-sort-desc [ &amp;#xf0dd; ]</code></li>
                            <li> <span class="fa fa-sort-down  gly-2" aria-hidden="true"></span><br />
                                <code>fa-sort-down  [ &amp;#xf0dd; ]</code></li>
                            <li> <span class="fa fa-sort-numeric-asc gly-2" aria-hidden="true"></span><br />
                                <code>fa-sort-numeric-asc [ &amp;#xf162; ]</code></li>
                            <li> <span class="fa fa-sort-numeric-desc gly-2" aria-hidden="true"></span><br />
                                <code>fa-sort-numeric-desc [ &amp;#xf163; ]</code></li>
                            <li> <span class="fa fa-sort-up  gly-2" aria-hidden="true"></span><br />
                                <code>fa-sort-up  [ &amp;#xf0de; ]</code></li>
                            <li> <span class="fa fa-soundcloud gly-2" aria-hidden="true"></span><br />
                                <code>fa-soundcloud [ &amp;#xf1be; ]</code></li>
                            <li> <span class="fa fa-space-shuttle gly-2" aria-hidden="true"></span><br />
                                <code>fa-space-shuttle [ &amp;#xf197; ]</code></li>
                            <li> <span class="fa fa-spinner gly-2" aria-hidden="true"></span><br />
                                <code>fa-spinner [ &amp;#xf110; ]</code></li>
                            <li> <span class="fa fa-spoon gly-2" aria-hidden="true"></span><br />
                                <code>fa-spoon [ &amp;#xf1b1; ]</code></li>
                            <li> <span class="fa fa-spotify gly-2" aria-hidden="true"></span><br />
                                <code>fa-spotify [ &amp;#xf1bc; ]</code></li>
                            <li> <span class="fa fa-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-square [ &amp;#xf0c8; ]</code></li>
                            <li> <span class="fa fa-square-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-square-o [ &amp;#xf096; ]</code></li>
                            <li> <span class="fa fa-stack-exchange gly-2" aria-hidden="true"></span><br />
                                <code>fa-stack-exchange [ &amp;#xf18d; ]</code></li>
                            <li> <span class="fa fa-stack-overflow gly-2" aria-hidden="true"></span><br />
                                <code>fa-stack-overflow [ &amp;#xf16c; ]</code></li>
                            <li> <span class="fa fa-star gly-2" aria-hidden="true"></span><br />
                                <code>fa-star [ &amp;#xf005; ]</code></li>
                            <li> <span class="fa fa-star-half gly-2" aria-hidden="true"></span><br />
                                <code>fa-star-half [ &amp;#xf089; ]</code></li>
                            <li> <span class="fa fa-star-half-empty  gly-2" aria-hidden="true"></span><br />
                                <code>fa-star-half-empty  [ &amp;#xf123; ]</code></li>
                            <li> <span class="fa fa-star-half-full  gly-2" aria-hidden="true"></span><br />
                                <code>fa-star-half-full  [ &amp;#xf123; ]</code></li>
                            <li> <span class="fa fa-star-half-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-star-half-o [ &amp;#xf123; ]</code></li>
                            <li> <span class="fa fa-star-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-star-o [ &amp;#xf006; ]</code></li>
                            <li> <span class="fa fa-steam gly-2" aria-hidden="true"></span><br />
                                <code>fa-steam [ &amp;#xf1b6; ]</code></li>
                            <li> <span class="fa fa-steam-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-steam-square [ &amp;#xf1b7; ]</code></li>
                            <li> <span class="fa fa-step-backward gly-2" aria-hidden="true"></span><br />
                                <code>fa-step-backward [ &amp;#xf048; ]</code></li>
                            <li> <span class="fa fa-step-forward gly-2" aria-hidden="true"></span><br />
                                <code>fa-step-forward [ &amp;#xf051; ]</code></li>
                            <li> <span class="fa fa-stethoscope gly-2" aria-hidden="true"></span><br />
                                <code>fa-stethoscope [ &amp;#xf0f1; ]</code></li>
                            <li> <span class="fa fa-sticky-note gly-2" aria-hidden="true"></span><br />
                                <code>fa-sticky-note [ &amp;#xf249; ]</code></li>
                            <li> <span class="fa fa-sticky-note-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-sticky-note-o [ &amp;#xf24a; ]</code></li>
                            <li> <span class="fa fa-stop gly-2" aria-hidden="true"></span><br />
                                <code>fa-stop [ &amp;#xf04d; ]</code></li>
                            <li> <span class="fa fa-stop-circle gly-2" aria-hidden="true"></span><br />
                                <code>fa-stop-circle [ &amp;#xf28d; ]</code></li>
                            <li> <span class="fa fa-stop-circle-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-stop-circle-o [ &amp;#xf28e; ]</code></li>
                            <li> <span class="fa fa-street-view gly-2" aria-hidden="true"></span><br />
                                <code>fa-street-view [ &amp;#xf21d; ]</code></li>
                            <li> <span class="fa fa-strikethrough gly-2" aria-hidden="true"></span><br />
                                <code>fa-strikethrough [ &amp;#xf0cc; ]</code></li>
                            <li> <span class="fa fa-stumbleupon gly-2" aria-hidden="true"></span><br />
                                <code>fa-stumbleupon [ &amp;#xf1a4; ]</code></li>
                            <li> <span class="fa fa-stumbleupon-circle gly-2" aria-hidden="true"></span><br />
                                <code>fa-stumbleupon-circle [ &amp;#xf1a3; ]</code></li>
                            <li> <span class="fa fa-subscript gly-2" aria-hidden="true"></span><br />
                                <code>fa-subscript [ &amp;#xf12c; ]</code></li>
                            <li> <span class="fa fa-subway gly-2" aria-hidden="true"></span><br />
                                <code>fa-subway [ &amp;#xf239; ]</code></li>
                            <li> <span class="fa fa-suitcase gly-2" aria-hidden="true"></span><br />
                                <code>fa-suitcase [ &amp;#xf0f2; ]</code></li>
                            <li> <span class="fa fa-sun-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-sun-o [ &amp;#xf185; ]</code></li>
                            <li> <span class="fa fa-superscript gly-2" aria-hidden="true"></span><br />
                                <code>fa-superscript [ &amp;#xf12b; ]</code></li>
                            <li> <span class="fa fa-support  gly-2" aria-hidden="true"></span><br />
                                <code>fa-support  [ &amp;#xf1cd; ]</code></li>
                            <li> <span class="fa fa-table gly-2" aria-hidden="true"></span><br />
                                <code>fa-table [ &amp;#xf0ce; ]</code></li>
                            <li> <span class="fa fa-tablet gly-2" aria-hidden="true"></span><br />
                                <code>fa-tablet [ &amp;#xf10a; ]</code></li>
                            <li> <span class="fa fa-tachometer gly-2" aria-hidden="true"></span><br />
                                <code>fa-tachometer [ &amp;#xf0e4; ]</code></li>
                            <li> <span class="fa fa-tag gly-2" aria-hidden="true"></span><br />
                                <code>fa-tag [ &amp;#xf02b; ]</code></li>
                            <li> <span class="fa fa-tags gly-2" aria-hidden="true"></span><br />
                                <code>fa-tags [ &amp;#xf02c; ]</code></li>
                            <li> <span class="fa fa-tasks gly-2" aria-hidden="true"></span><br />
                                <code>fa-tasks [ &amp;#xf0ae; ]</code></li>
                            <li> <span class="fa fa-taxi gly-2" aria-hidden="true"></span><br />
                                <code>fa-taxi [ &amp;#xf1ba; ]</code></li>
                            <li> <span class="fa fa-television gly-2" aria-hidden="true"></span><br />
                                <code>fa-television [ &amp;#xf26c; ]</code></li>
                            <li> <span class="fa fa-tencent-weibo gly-2" aria-hidden="true"></span><br />
                                <code>fa-tencent-weibo [ &amp;#xf1d5; ]</code></li>
                            <li> <span class="fa fa-terminal gly-2" aria-hidden="true"></span><br />
                                <code>fa-terminal [ &amp;#xf120; ]</code></li>
                            <li> <span class="fa fa-text-height gly-2" aria-hidden="true"></span><br />
                                <code>fa-text-height [ &amp;#xf034; ]</code></li>
                            <li> <span class="fa fa-text-width gly-2" aria-hidden="true"></span><br />
                                <code>fa-text-width [ &amp;#xf035; ]</code></li>
                            <li> <span class="fa fa-th gly-2" aria-hidden="true"></span><br />
                                <code>fa-th [ &amp;#xf00a; ]</code></li>
                            <li> <span class="fa fa-th-large gly-2" aria-hidden="true"></span><br />
                                <code>fa-th-large [ &amp;#xf009; ]</code></li>
                            <li> <span class="fa fa-th-list gly-2" aria-hidden="true"></span><br />
                                <code>fa-th-list [ &amp;#xf00b; ]</code></li>
                            <li> <span class="fa fa-thumb-tack gly-2" aria-hidden="true"></span><br />
                                <code>fa-thumb-tack [ &amp;#xf08d; ]</code></li>
                            <li> <span class="fa fa-thumbs-down gly-2" aria-hidden="true"></span><br />
                                <code>fa-thumbs-down [ &amp;#xf165; ]</code></li>
                            <li> <span class="fa fa-thumbs-o-down gly-2" aria-hidden="true"></span><br />
                                <code>fa-thumbs-o-down [ &amp;#xf088; ]</code></li>
                            <li> <span class="fa fa-thumbs-o-up gly-2" aria-hidden="true"></span><br />
                                <code>fa-thumbs-o-up [ &amp;#xf087; ]</code></li>
                            <li> <span class="fa fa-thumbs-up gly-2" aria-hidden="true"></span><br />
                                <code>fa-thumbs-up [ &amp;#xf164; ]</code></li>
                            <li> <span class="fa fa-ticket gly-2" aria-hidden="true"></span><br />
                                <code>fa-ticket [ &amp;#xf145; ]</code></li>
                            <li> <span class="fa fa-times gly-2" aria-hidden="true"></span><br />
                                <code>fa-times [ &amp;#xf00d; ]</code></li>
                            <li> <span class="fa fa-times-circle gly-2" aria-hidden="true"></span><br />
                                <code>fa-times-circle [ &amp;#xf057; ]</code></li>
                            <li> <span class="fa fa-times-circle-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-times-circle-o [ &amp;#xf05c; ]</code></li>
                            <li> <span class="fa fa-tint gly-2" aria-hidden="true"></span><br />
                                <code>fa-tint [ &amp;#xf043; ]</code></li>
                            <li> <span class="fa fa-toggle-down  gly-2" aria-hidden="true"></span><br />
                                <code>fa-toggle-down  [ &amp;#xf150; ]</code></li>
                            <li> <span class="fa fa-toggle-left  gly-2" aria-hidden="true"></span><br />
                                <code>fa-toggle-left  [ &amp;#xf191; ]</code></li>
                            <li> <span class="fa fa-toggle-off gly-2" aria-hidden="true"></span><br />
                                <code>fa-toggle-off [ &amp;#xf204; ]</code></li>
                            <li> <span class="fa fa-toggle-on gly-2" aria-hidden="true"></span><br />
                                <code>fa-toggle-on [ &amp;#xf205; ]</code></li>
                            <li> <span class="fa fa-toggle-right  gly-2" aria-hidden="true"></span><br />
                                <code>fa-toggle-right  [ &amp;#xf152; ]</code></li>
                            <li> <span class="fa fa-toggle-up  gly-2" aria-hidden="true"></span><br />
                                <code>fa-toggle-up  [ &amp;#xf151; ]</code></li>
                            <li> <span class="fa fa-trademark gly-2" aria-hidden="true"></span><br />
                                <code>fa-trademark [ &amp;#xf25c; ]</code></li>
                            <li> <span class="fa fa-train gly-2" aria-hidden="true"></span><br />
                                <code>fa-train [ &amp;#xf238; ]</code></li>
                            <li> <span class="fa fa-transgender gly-2" aria-hidden="true"></span><br />
                                <code>fa-transgender [ &amp;#xf224; ]</code></li>
                            <li> <span class="fa fa-transgender-alt gly-2" aria-hidden="true"></span><br />
                                <code>fa-transgender-alt [ &amp;#xf225; ]</code></li>
                            <li> <span class="fa fa-trash gly-2" aria-hidden="true"></span><br />
                                <code>fa-trash [ &amp;#xf1f8; ]</code></li>
                            <li> <span class="fa fa-trash-o gly-2" aria-hidden="true"></span><br />
                                <code>fa-trash-o [ &amp;#xf014; ]</code></li>
                            <li> <span class="fa fa-tree gly-2" aria-hidden="true"></span><br />
                                <code>fa-tree [ &amp;#xf1bb; ]</code></li>
                            <li> <span class="fa fa-trello gly-2" aria-hidden="true"></span><br />
                                <code>fa-trello [ &amp;#xf181; ]</code></li>
                            <li> <span class="fa fa-tripadvisor gly-2" aria-hidden="true"></span><br />
                                <code>fa-tripadvisor [ &amp;#xf262; ]</code></li>
                            <li> <span class="fa fa-trophy gly-2" aria-hidden="true"></span><br />
                                <code>fa-trophy [ &amp;#xf091; ]</code></li>
                            <li> <span class="fa fa-truck gly-2" aria-hidden="true"></span><br />
                                <code>fa-truck [ &amp;#xf0d1; ]</code></li>
                            <li> <span class="fa fa-try gly-2" aria-hidden="true"></span><br />
                                <code>fa-try [ &amp;#xf195; ]</code></li>
                            <li> <span class="fa fa-tty gly-2" aria-hidden="true"></span><br />
                                <code>fa-tty [ &amp;#xf1e4; ]</code></li>
                            <li> <span class="fa fa-tumblr gly-2" aria-hidden="true"></span><br />
                                <code>fa-tumblr [ &amp;#xf173; ]</code></li>
                            <li> <span class="fa fa-tumblr-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-tumblr-square [ &amp;#xf174; ]</code></li>
                            <li> <span class="fa fa-turkish-lira  gly-2" aria-hidden="true"></span><br />
                                <code>fa-turkish-lira  [ &amp;#xf195; ]</code></li>
                            <li> <span class="fa fa-tv  gly-2" aria-hidden="true"></span><br />
                                <code>fa-tv  [ &amp;#xf26c; ]</code></li>
                            <li> <span class="fa fa-twitch gly-2" aria-hidden="true"></span><br />
                                <code>fa-twitch [ &amp;#xf1e8; ]</code></li>
                            <li> <span class="fa fa-twitter gly-2" aria-hidden="true"></span><br />
                                <code>fa-twitter [ &amp;#xf099; ]</code></li>
                            <li> <span class="fa fa-twitter-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-twitter-square [ &amp;#xf081; ]</code></li>
                            <li> <span class="fa fa-umbrella gly-2" aria-hidden="true"></span><br />
                                <code>fa-umbrella [ &amp;#xf0e9; ]</code></li>
                            <li> <span class="fa fa-underline gly-2" aria-hidden="true"></span><br />
                                <code>fa-underline [ &amp;#xf0cd; ]</code></li>
                            <li> <span class="fa fa-undo gly-2" aria-hidden="true"></span><br />
                                <code>fa-undo [ &amp;#xf0e2; ]</code></li>
                            <li> <span class="fa fa-university gly-2" aria-hidden="true"></span><br />
                                <code>fa-university [ &amp;#xf19c; ]</code></li>
                            <li> <span class="fa fa-unlink  gly-2" aria-hidden="true"></span><br />
                                <code>fa-unlink  [ &amp;#xf127; ]</code></li>
                            <li> <span class="fa fa-unlock gly-2" aria-hidden="true"></span><br />
                                <code>fa-unlock [ &amp;#xf09c; ]</code></li>
                            <li> <span class="fa fa-unlock-alt gly-2" aria-hidden="true"></span><br />
                                <code>fa-unlock-alt [ &amp;#xf13e; ]</code></li>
                            <li> <span class="fa fa-unsorted  gly-2" aria-hidden="true"></span><br />
                                <code>fa-unsorted  [ &amp;#xf0dc; ]</code></li>
                            <li> <span class="fa fa-upload gly-2" aria-hidden="true"></span><br />
                                <code>fa-upload [ &amp;#xf093; ]</code></li>
                            <li> <span class="fa fa-usb gly-2" aria-hidden="true"></span><br />
                                <code>fa-usb [ &amp;#xf287; ]</code></li>
                            <li> <span class="fa fa-usd gly-2" aria-hidden="true"></span><br />
                                <code>fa-usd [ &amp;#xf155; ]</code></li>
                            <li> <span class="fa fa-user gly-2" aria-hidden="true"></span><br />
                                <code>fa-user [ &amp;#xf007; ]</code></li>
                            <li> <span class="fa fa-user-md gly-2" aria-hidden="true"></span><br />
                                <code>fa-user-md [ &amp;#xf0f0; ]</code></li>
                            <li> <span class="fa fa-user-plus gly-2" aria-hidden="true"></span><br />
                                <code>fa-user-plus [ &amp;#xf234; ]</code></li>
                            <li> <span class="fa fa-user-secret gly-2" aria-hidden="true"></span><br />
                                <code>fa-user-secret [ &amp;#xf21b; ]</code></li>
                            <li> <span class="fa fa-user-times gly-2" aria-hidden="true"></span><br />
                                <code>fa-user-times [ &amp;#xf235; ]</code></li>
                            <li> <span class="fa fa-users gly-2" aria-hidden="true"></span><br />
                                <code>fa-users [ &amp;#xf0c0; ]</code></li>
                            <li> <span class="fa fa-venus gly-2" aria-hidden="true"></span><br />
                                <code>fa-venus [ &amp;#xf221; ]</code></li>
                            <li> <span class="fa fa-venus-double gly-2" aria-hidden="true"></span><br />
                                <code>fa-venus-double [ &amp;#xf226; ]</code></li>
                            <li> <span class="fa fa-venus-mars gly-2" aria-hidden="true"></span><br />
                                <code>fa-venus-mars [ &amp;#xf228; ]</code></li>
                            <li> <span class="fa fa-viacoin gly-2" aria-hidden="true"></span><br />
                                <code>fa-viacoin [ &amp;#xf237; ]</code></li>
                            <li> <span class="fa fa-video-camera gly-2" aria-hidden="true"></span><br />
                                <code>fa-video-camera [ &amp;#xf03d; ]</code></li>
                            <li> <span class="fa fa-vimeo gly-2" aria-hidden="true"></span><br />
                                <code>fa-vimeo [ &amp;#xf27d; ]</code></li>
                            <li> <span class="fa fa-vimeo-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-vimeo-square [ &amp;#xf194; ]</code></li>
                            <li> <span class="fa fa-vine gly-2" aria-hidden="true"></span><br />
                                <code>fa-vine [ &amp;#xf1ca; ]</code></li>
                            <li> <span class="fa fa-vk gly-2" aria-hidden="true"></span><br />
                                <code>fa-vk [ &amp;#xf189; ]</code></li>
                            <li> <span class="fa fa-volume-down gly-2" aria-hidden="true"></span><br />
                                <code>fa-volume-down [ &amp;#xf027; ]</code></li>
                            <li> <span class="fa fa-volume-off gly-2" aria-hidden="true"></span><br />
                                <code>fa-volume-off [ &amp;#xf026; ]</code></li>
                            <li> <span class="fa fa-volume-up gly-2" aria-hidden="true"></span><br />
                                <code>fa-volume-up [ &amp;#xf028; ]</code></li>
                            <li> <span class="fa fa-warning  gly-2" aria-hidden="true"></span><br />
                                <code>fa-warning  [ &amp;#xf071; ]</code></li>
                            <li> <span class="fa fa-wechat  gly-2" aria-hidden="true"></span><br />
                                <code>fa-wechat  [ &amp;#xf1d7; ]</code></li>
                            <li> <span class="fa fa-weibo gly-2" aria-hidden="true"></span><br />
                                <code>fa-weibo [ &amp;#xf18a; ]</code></li>
                            <li> <span class="fa fa-weixin gly-2" aria-hidden="true"></span><br />
                                <code>fa-weixin [ &amp;#xf1d7; ]</code></li>
                            <li> <span class="fa fa-whatsapp gly-2" aria-hidden="true"></span><br />
                                <code>fa-whatsapp [ &amp;#xf232; ]</code></li>
                            <li> <span class="fa fa-wheelchair gly-2" aria-hidden="true"></span><br />
                                <code>fa-wheelchair [ &amp;#xf193; ]</code></li>
                            <li> <span class="fa fa-wifi gly-2" aria-hidden="true"></span><br />
                                <code>fa-wifi [ &amp;#xf1eb; ]</code></li>
                            <li> <span class="fa fa-wikipedia-w gly-2" aria-hidden="true"></span><br />
                                <code>fa-wikipedia-w [ &amp;#xf266; ]</code></li>
                            <li> <span class="fa fa-windows gly-2" aria-hidden="true"></span><br />
                                <code>fa-windows [ &amp;#xf17a; ]</code></li>
                            <li> <span class="fa fa-won  gly-2" aria-hidden="true"></span><br />
                                <code>fa-won  [ &amp;#xf159; ]</code></li>
                            <li> <span class="fa fa-wordpress gly-2" aria-hidden="true"></span><br />
                                <code>fa-wordpress [ &amp;#xf19a; ]</code></li>
                            <li> <span class="fa fa-wrench gly-2" aria-hidden="true"></span><br />
                                <code>fa-wrench [ &amp;#xf0ad; ]</code></li>
                            <li> <span class="fa fa-xing gly-2" aria-hidden="true"></span><br />
                                <code>fa-xing [ &amp;#xf168; ]</code></li>
                            <li> <span class="fa fa-xing-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-xing-square [ &amp;#xf169; ]</code></li>
                            <li> <span class="fa fa-y-combinator gly-2" aria-hidden="true"></span><br />
                                <code>fa-y-combinator [ &amp;#xf23b; ]</code></li>
                            <li> <span class="fa fa-y-combinator-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-y-combinator-square [ &amp;#xf1d4; ]</code></li>
                            <li> <span class="fa fa-yahoo gly-2" aria-hidden="true"></span><br />
                                <code>fa-yahoo [ &amp;#xf19e; ]</code></li>
                            <li> <span class="fa fa-yc  gly-2" aria-hidden="true"></span><br />
                                <code>fa-yc  [ &amp;#xf23b; ]</code></li>
                            <li> <span class="fa fa-yc-square  gly-2" aria-hidden="true"></span><br />
                                <code>fa-yc-square  [ &amp;#xf1d4; ]</code></li>
                            <li> <span class="fa fa-yelp gly-2" aria-hidden="true"></span><br />
                                <code>fa-yelp [ &amp;#xf1e9; ]</code></li>
                            <li> <span class="fa fa-yen  gly-2" aria-hidden="true"></span><br />
                                <code>fa-yen  [ &amp;#xf157; ]</code></li>
                            <li> <span class="fa fa-youtube gly-2" aria-hidden="true"></span><br />
                                <code>fa-youtube [ &amp;#xf167; ]</code></li>
                            <li> <span class="fa fa-youtube-play gly-2" aria-hidden="true"></span><br />
                                <code>fa-youtube-play [ &amp;#xf16a; ]</code></li>
                            <li> <span class="fa fa-youtube-square gly-2" aria-hidden="true"></span><br />
                                <code>fa-youtube-square [ &amp;#xf166; ]</code></li>
                        <ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            Ionicons 2.0.1
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="font-awesome-icons text-md-center">
                <div class="row">
                    <div class="col-md-4">
                        <ul  class="list-unstyled">
                            <li> <span class="fa ion-ionic gly-2" aria-hidden="true"></span><br /> <code> ion-ionic </code></li>
                            <li> <span class="fa ion-arrow-up-a gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-up-a </code></li>
                            <li> <span class="fa ion-arrow-right-a gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-right-a </code></li>
                            <li> <span class="fa ion-arrow-down-a gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-down-a </code></li>
                            <li> <span class="fa ion-arrow-left-a gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-left-a </code></li>
                            <li> <span class="fa ion-arrow-up-b gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-up-b </code></li>
                            <li> <span class="fa ion-arrow-right-b gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-right-b </code></li>
                            <li> <span class="fa ion-arrow-down-b gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-down-b </code></li>
                            <li> <span class="fa ion-arrow-left-b gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-left-b </code></li>
                            <li> <span class="fa ion-arrow-up-c gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-up-c </code></li>
                            <li> <span class="fa ion-arrow-right-c gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-right-c </code></li>
                            <li> <span class="fa ion-arrow-down-c gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-down-c </code></li>
                            <li> <span class="fa ion-arrow-left-c gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-left-c </code></li>
                            <li> <span class="fa ion-arrow-return-right gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-return-right </code></li>
                            <li> <span class="fa ion-arrow-return-left gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-return-left </code></li>
                            <li> <span class="fa ion-arrow-swap gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-swap </code></li>
                            <li> <span class="fa ion-arrow-shrink gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-shrink </code></li>
                            <li> <span class="fa ion-arrow-expand gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-expand </code></li>
                            <li> <span class="fa ion-arrow-move gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-move </code></li>
                            <li> <span class="fa ion-arrow-resize gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-resize </code></li>
                            <li> <span class="fa ion-chevron-up gly-2" aria-hidden="true"></span><br /> <code> ion-chevron-up </code></li>
                            <li> <span class="fa ion-chevron-right gly-2" aria-hidden="true"></span><br /> <code> ion-chevron-right </code></li>
                            <li> <span class="fa ion-chevron-down gly-2" aria-hidden="true"></span><br /> <code> ion-chevron-down </code></li>
                            <li> <span class="fa ion-chevron-left gly-2" aria-hidden="true"></span><br /> <code> ion-chevron-left </code></li>
                            <li> <span class="fa ion-navicon-round gly-2" aria-hidden="true"></span><br /> <code> ion-navicon-round </code></li>
                            <li> <span class="fa ion-navicon gly-2" aria-hidden="true"></span><br /> <code> ion-navicon </code></li>
                            <li> <span class="fa ion-drag gly-2" aria-hidden="true"></span><br /> <code> ion-drag </code></li>
                            <li> <span class="fa ion-log-in gly-2" aria-hidden="true"></span><br /> <code> ion-log-in </code></li>
                            <li> <span class="fa ion-log-out gly-2" aria-hidden="true"></span><br /> <code> ion-log-out </code></li>
                            <li> <span class="fa ion-checkmark-round gly-2" aria-hidden="true"></span><br /> <code> ion-checkmark-round </code></li>
                            <li> <span class="fa ion-checkmark gly-2" aria-hidden="true"></span><br /> <code> ion-checkmark </code></li>
                            <li> <span class="fa ion-checkmark-circled gly-2" aria-hidden="true"></span><br /> <code> ion-checkmark-circled </code></li>
                            <li> <span class="fa ion-close-round gly-2" aria-hidden="true"></span><br /> <code> ion-close-round </code></li>
                            <li> <span class="fa ion-close gly-2" aria-hidden="true"></span><br /> <code> ion-close </code></li>
                            <li> <span class="fa ion-close-circled gly-2" aria-hidden="true"></span><br /> <code> ion-close-circled </code></li>
                            <li> <span class="fa ion-plus-round gly-2" aria-hidden="true"></span><br /> <code> ion-plus-round </code></li>
                            <li> <span class="fa ion-plus gly-2" aria-hidden="true"></span><br /> <code> ion-plus </code></li>
                            <li> <span class="fa ion-plus-circled gly-2" aria-hidden="true"></span><br /> <code> ion-plus-circled </code></li>
                            <li> <span class="fa ion-minus-round gly-2" aria-hidden="true"></span><br /> <code> ion-minus-round </code></li>
                            <li> <span class="fa ion-minus gly-2" aria-hidden="true"></span><br /> <code> ion-minus </code></li>
                            <li> <span class="fa ion-minus-circled gly-2" aria-hidden="true"></span><br /> <code> ion-minus-circled </code></li>
                            <li> <span class="fa ion-information gly-2" aria-hidden="true"></span><br /> <code> ion-information </code></li>
                            <li> <span class="fa ion-information-circled gly-2" aria-hidden="true"></span><br /> <code> ion-information-circled </code></li>
                            <li> <span class="fa ion-help gly-2" aria-hidden="true"></span><br /> <code> ion-help </code></li>
                            <li> <span class="fa ion-help-circled gly-2" aria-hidden="true"></span><br /> <code> ion-help-circled </code></li>
                            <li> <span class="fa ion-backspace-outline gly-2" aria-hidden="true"></span><br /> <code> ion-backspace-outline </code></li>
                            <li> <span class="fa ion-backspace gly-2" aria-hidden="true"></span><br /> <code> ion-backspace </code></li>
                            <li> <span class="fa ion-help-buoy gly-2" aria-hidden="true"></span><br /> <code> ion-help-buoy </code></li>
                            <li> <span class="fa ion-asterisk gly-2" aria-hidden="true"></span><br /> <code> ion-asterisk </code></li>
                            <li> <span class="fa ion-alert gly-2" aria-hidden="true"></span><br /> <code> ion-alert </code></li>
                            <li> <span class="fa ion-alert-circled gly-2" aria-hidden="true"></span><br /> <code> ion-alert-circled </code></li>
                            <li> <span class="fa ion-refresh gly-2" aria-hidden="true"></span><br /> <code> ion-refresh </code></li>
                            <li> <span class="fa ion-loop gly-2" aria-hidden="true"></span><br /> <code> ion-loop </code></li>
                            <li> <span class="fa ion-shuffle gly-2" aria-hidden="true"></span><br /> <code> ion-shuffle </code></li>
                            <li> <span class="fa ion-home gly-2" aria-hidden="true"></span><br /> <code> ion-home </code></li>
                            <li> <span class="fa ion-search gly-2" aria-hidden="true"></span><br /> <code> ion-search </code></li>
                            <li> <span class="fa ion-flag gly-2" aria-hidden="true"></span><br /> <code> ion-flag </code></li>
                            <li> <span class="fa ion-star gly-2" aria-hidden="true"></span><br /> <code> ion-star </code></li>
                            <li> <span class="fa ion-heart gly-2" aria-hidden="true"></span><br /> <code> ion-heart </code></li>
                            <li> <span class="fa ion-heart-broken gly-2" aria-hidden="true"></span><br /> <code> ion-heart-broken </code></li>
                            <li> <span class="fa ion-gear-a gly-2" aria-hidden="true"></span><br /> <code> ion-gear-a </code></li>
                            <li> <span class="fa ion-gear-b gly-2" aria-hidden="true"></span><br /> <code> ion-gear-b </code></li>
                            <li> <span class="fa ion-toggle-filled gly-2" aria-hidden="true"></span><br /> <code> ion-toggle-filled </code></li>
                            <li> <span class="fa ion-toggle gly-2" aria-hidden="true"></span><br /> <code> ion-toggle </code></li>
                            <li> <span class="fa ion-settings gly-2" aria-hidden="true"></span><br /> <code> ion-settings </code></li>
                            <li> <span class="fa ion-wrench gly-2" aria-hidden="true"></span><br /> <code> ion-wrench </code></li>
                            <li> <span class="fa ion-hammer gly-2" aria-hidden="true"></span><br /> <code> ion-hammer </code></li>
                            <li> <span class="fa ion-edit gly-2" aria-hidden="true"></span><br /> <code> ion-edit </code></li>
                            <li> <span class="fa ion-trash-a gly-2" aria-hidden="true"></span><br /> <code> ion-trash-a </code></li>
                            <li> <span class="fa ion-trash-b gly-2" aria-hidden="true"></span><br /> <code> ion-trash-b </code></li>
                            <li> <span class="fa ion-document gly-2" aria-hidden="true"></span><br /> <code> ion-document </code></li>
                            <li> <span class="fa ion-document-text gly-2" aria-hidden="true"></span><br /> <code> ion-document-text </code></li>
                            <li> <span class="fa ion-clipboard gly-2" aria-hidden="true"></span><br /> <code> ion-clipboard </code></li>
                            <li> <span class="fa ion-scissors gly-2" aria-hidden="true"></span><br /> <code> ion-scissors </code></li>
                            <li> <span class="fa ion-funnel gly-2" aria-hidden="true"></span><br /> <code> ion-funnel </code></li>
                            <li> <span class="fa ion-bookmark gly-2" aria-hidden="true"></span><br /> <code> ion-bookmark </code></li>
                            <li> <span class="fa ion-email gly-2" aria-hidden="true"></span><br /> <code> ion-email </code></li>
                            <li> <span class="fa ion-email-unread gly-2" aria-hidden="true"></span><br /> <code> ion-email-unread </code></li>
                            <li> <span class="fa ion-folder gly-2" aria-hidden="true"></span><br /> <code> ion-folder </code></li>
                            <li> <span class="fa ion-filing gly-2" aria-hidden="true"></span><br /> <code> ion-filing </code></li>
                            <li> <span class="fa ion-archive gly-2" aria-hidden="true"></span><br /> <code> ion-archive </code></li>
                            <li> <span class="fa ion-reply gly-2" aria-hidden="true"></span><br /> <code> ion-reply </code></li>
                            <li> <span class="fa ion-reply-all gly-2" aria-hidden="true"></span><br /> <code> ion-reply-all </code></li>
                            <li> <span class="fa ion-forward gly-2" aria-hidden="true"></span><br /> <code> ion-forward </code></li>
                            <li> <span class="fa ion-share gly-2" aria-hidden="true"></span><br /> <code> ion-share </code></li>
                            <li> <span class="fa ion-paper-airplane gly-2" aria-hidden="true"></span><br /> <code> ion-paper-airplane </code></li>
                            <li> <span class="fa ion-link gly-2" aria-hidden="true"></span><br /> <code> ion-link </code></li>
                            <li> <span class="fa ion-paperclip gly-2" aria-hidden="true"></span><br /> <code> ion-paperclip </code></li>
                            <li> <span class="fa ion-compose gly-2" aria-hidden="true"></span><br /> <code> ion-compose </code></li>
                            <li> <span class="fa ion-briefcase gly-2" aria-hidden="true"></span><br /> <code> ion-briefcase </code></li>
                            <li> <span class="fa ion-medkit gly-2" aria-hidden="true"></span><br /> <code> ion-medkit </code></li>
                            <li> <span class="fa ion-at gly-2" aria-hidden="true"></span><br /> <code> ion-at </code></li>
                            <li> <span class="fa ion-pound gly-2" aria-hidden="true"></span><br /> <code> ion-pound </code></li>
                            <li> <span class="fa ion-quote gly-2" aria-hidden="true"></span><br /> <code> ion-quote </code></li>
                            <li> <span class="fa ion-cloud gly-2" aria-hidden="true"></span><br /> <code> ion-cloud </code></li>
                            <li> <span class="fa ion-upload gly-2" aria-hidden="true"></span><br /> <code> ion-upload </code></li>
                            <li> <span class="fa ion-more gly-2" aria-hidden="true"></span><br /> <code> ion-more </code></li>
                            <li> <span class="fa ion-grid gly-2" aria-hidden="true"></span><br /> <code> ion-grid </code></li>
                            <li> <span class="fa ion-calendar gly-2" aria-hidden="true"></span><br /> <code> ion-calendar </code></li>
                            <li> <span class="fa ion-clock gly-2" aria-hidden="true"></span><br /> <code> ion-clock </code></li>
                            <li> <span class="fa ion-compass gly-2" aria-hidden="true"></span><br /> <code> ion-compass </code></li>
                            <li> <span class="fa ion-pinpoint gly-2" aria-hidden="true"></span><br /> <code> ion-pinpoint </code></li>
                            <li> <span class="fa ion-pin gly-2" aria-hidden="true"></span><br /> <code> ion-pin </code></li>
                            <li> <span class="fa ion-navigate gly-2" aria-hidden="true"></span><br /> <code> ion-navigate </code></li>
                            <li> <span class="fa ion-location gly-2" aria-hidden="true"></span><br /> <code> ion-location </code></li>
                            <li> <span class="fa ion-map gly-2" aria-hidden="true"></span><br /> <code> ion-map </code></li>
                            <li> <span class="fa ion-lock-combination gly-2" aria-hidden="true"></span><br /> <code> ion-lock-combination </code></li>
                            <li> <span class="fa ion-locked gly-2" aria-hidden="true"></span><br /> <code> ion-locked </code></li>
                            <li> <span class="fa ion-unlocked gly-2" aria-hidden="true"></span><br /> <code> ion-unlocked </code></li>
                            <li> <span class="fa ion-key gly-2" aria-hidden="true"></span><br /> <code> ion-key </code></li>
                            <li> <span class="fa ion-arrow-graph-up-right gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-graph-up-right </code></li>
                            <li> <span class="fa ion-arrow-graph-down-right gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-graph-down-right </code></li>
                            <li> <span class="fa ion-arrow-graph-up-left gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-graph-up-left </code></li>
                            <li> <span class="fa ion-arrow-graph-down-left gly-2" aria-hidden="true"></span><br /> <code> ion-arrow-graph-down-left </code></li>
                            <li> <span class="fa ion-stats-bars gly-2" aria-hidden="true"></span><br /> <code> ion-stats-bars </code></li>
                            <li> <span class="fa ion-connection-bars gly-2" aria-hidden="true"></span><br /> <code> ion-connection-bars </code></li>
                            <li> <span class="fa ion-pie-graph gly-2" aria-hidden="true"></span><br /> <code> ion-pie-graph </code></li>
                            <li> <span class="fa ion-chatbubble gly-2" aria-hidden="true"></span><br /> <code> ion-chatbubble </code></li>
                            <li> <span class="fa ion-chatbubble-working gly-2" aria-hidden="true"></span><br /> <code> ion-chatbubble-working </code></li>
                            <li> <span class="fa ion-chatbubbles gly-2" aria-hidden="true"></span><br /> <code> ion-chatbubbles </code></li>
                            <li> <span class="fa ion-chatbox gly-2" aria-hidden="true"></span><br /> <code> ion-chatbox </code></li>
                            <li> <span class="fa ion-chatbox-working gly-2" aria-hidden="true"></span><br /> <code> ion-chatbox-working </code></li>
                            <li> <span class="fa ion-chatboxes gly-2" aria-hidden="true"></span><br /> <code> ion-chatboxes </code></li>
                            <li> <span class="fa ion-person gly-2" aria-hidden="true"></span><br /> <code> ion-person </code></li>
                            <li> <span class="fa ion-person-add gly-2" aria-hidden="true"></span><br /> <code> ion-person-add </code></li>
                            <li> <span class="fa ion-person-stalker gly-2" aria-hidden="true"></span><br /> <code> ion-person-stalker </code></li>
                            <li> <span class="fa ion-woman gly-2" aria-hidden="true"></span><br /> <code> ion-woman </code></li>
                            <li> <span class="fa ion-man gly-2" aria-hidden="true"></span><br /> <code> ion-man </code></li>
                            <li> <span class="fa ion-female gly-2" aria-hidden="true"></span><br /> <code> ion-female </code></li>
                            <li> <span class="fa ion-male gly-2" aria-hidden="true"></span><br /> <code> ion-male </code></li>
                            <li> <span class="fa ion-transgender gly-2" aria-hidden="true"></span><br /> <code> ion-transgender </code></li>
                            <li> <span class="fa ion-fork gly-2" aria-hidden="true"></span><br /> <code> ion-fork </code></li>
                            <li> <span class="fa ion-knife gly-2" aria-hidden="true"></span><br /> <code> ion-knife </code></li>
                            <li> <span class="fa ion-spoon gly-2" aria-hidden="true"></span><br /> <code> ion-spoon </code></li>
                            <li> <span class="fa ion-soup-can-outline gly-2" aria-hidden="true"></span><br /> <code> ion-soup-can-outline </code></li>
                            <li> <span class="fa ion-soup-can gly-2" aria-hidden="true"></span><br /> <code> ion-soup-can </code></li>
                            <li> <span class="fa ion-beer gly-2" aria-hidden="true"></span><br /> <code> ion-beer </code></li>
                            <li> <span class="fa ion-wineglass gly-2" aria-hidden="true"></span><br /> <code> ion-wineglass </code></li>
                            <li> <span class="fa ion-coffee gly-2" aria-hidden="true"></span><br /> <code> ion-coffee </code></li>
                            <li> <span class="fa ion-icecream gly-2" aria-hidden="true"></span><br /> <code> ion-icecream </code></li>
                            <li> <span class="fa ion-pizza gly-2" aria-hidden="true"></span><br /> <code> ion-pizza </code></li>
                            <li> <span class="fa ion-power gly-2" aria-hidden="true"></span><br /> <code> ion-power </code></li>
                            <li> <span class="fa ion-mouse gly-2" aria-hidden="true"></span><br /> <code> ion-mouse </code></li>
                            <li> <span class="fa ion-battery-full gly-2" aria-hidden="true"></span><br /> <code> ion-battery-full </code></li>
                            <li> <span class="fa ion-battery-half gly-2" aria-hidden="true"></span><br /> <code> ion-battery-half </code></li>
                            <li> <span class="fa ion-battery-low gly-2" aria-hidden="true"></span><br /> <code> ion-battery-low </code></li>
                            <li> <span class="fa ion-battery-empty gly-2" aria-hidden="true"></span><br /> <code> ion-battery-empty </code></li>
                            <li> <span class="fa ion-battery-charging gly-2" aria-hidden="true"></span><br /> <code> ion-battery-charging </code></li>
                            <li> <span class="fa ion-wifi gly-2" aria-hidden="true"></span><br /> <code> ion-wifi </code></li>
                            <li> <span class="fa ion-bluetooth gly-2" aria-hidden="true"></span><br /> <code> ion-bluetooth </code></li>
                            <li> <span class="fa ion-calculator gly-2" aria-hidden="true"></span><br /> <code> ion-calculator </code></li>
                            <li> <span class="fa ion-camera gly-2" aria-hidden="true"></span><br /> <code> ion-camera </code></li>
                            <li> <span class="fa ion-eye gly-2" aria-hidden="true"></span><br /> <code> ion-eye </code></li>
                            <li> <span class="fa ion-eye-disabled gly-2" aria-hidden="true"></span><br /> <code> ion-eye-disabled </code></li>
                            <li> <span class="fa ion-flash gly-2" aria-hidden="true"></span><br /> <code> ion-flash </code></li>
                            <li> <span class="fa ion-flash-off gly-2" aria-hidden="true"></span><br /> <code> ion-flash-off </code></li>
                            <li> <span class="fa ion-qr-scanner gly-2" aria-hidden="true"></span><br /> <code> ion-qr-scanner </code></li>
                            <li> <span class="fa ion-image gly-2" aria-hidden="true"></span><br /> <code> ion-image </code></li>
                            <li> <span class="fa ion-images gly-2" aria-hidden="true"></span><br /> <code> ion-images </code></li>
                            <li> <span class="fa ion-wand gly-2" aria-hidden="true"></span><br /> <code> ion-wand </code></li>
                            <li> <span class="fa ion-contrast gly-2" aria-hidden="true"></span><br /> <code> ion-contrast </code></li>
                            <li> <span class="fa ion-aperture gly-2" aria-hidden="true"></span><br /> <code> ion-aperture </code></li>
                            <li> <span class="fa ion-crop gly-2" aria-hidden="true"></span><br /> <code> ion-crop </code></li>
                            <li> <span class="fa ion-easel gly-2" aria-hidden="true"></span><br /> <code> ion-easel </code></li>
                            <li> <span class="fa ion-paintbrush gly-2" aria-hidden="true"></span><br /> <code> ion-paintbrush </code></li>
                            <li> <span class="fa ion-paintbucket gly-2" aria-hidden="true"></span><br /> <code> ion-paintbucket </code></li>
                            <li> <span class="fa ion-monitor gly-2" aria-hidden="true"></span><br /> <code> ion-monitor </code></li>
                            <li> <span class="fa ion-laptop gly-2" aria-hidden="true"></span><br /> <code> ion-laptop </code></li>
                            <li> <span class="fa ion-ipad gly-2" aria-hidden="true"></span><br /> <code> ion-ipad </code></li>
                            <li> <span class="fa ion-iphone gly-2" aria-hidden="true"></span><br /> <code> ion-iphone </code></li>
                            <li> <span class="fa ion-ipod gly-2" aria-hidden="true"></span><br /> <code> ion-ipod </code></li>
                            <li> <span class="fa ion-printer gly-2" aria-hidden="true"></span><br /> <code> ion-printer </code></li>
                            <li> <span class="fa ion-usb gly-2" aria-hidden="true"></span><br /> <code> ion-usb </code></li>
                            <li> <span class="fa ion-outlet gly-2" aria-hidden="true"></span><br /> <code> ion-outlet </code></li>
                            <li> <span class="fa ion-bug gly-2" aria-hidden="true"></span><br /> <code> ion-bug </code></li>
                            <li> <span class="fa ion-code gly-2" aria-hidden="true"></span><br /> <code> ion-code </code></li>
                            <li> <span class="fa ion-code-working gly-2" aria-hidden="true"></span><br /> <code> ion-code-working </code></li>
                            <li> <span class="fa ion-code-download gly-2" aria-hidden="true"></span><br /> <code> ion-code-download </code></li>
                            <li> <span class="fa ion-fork-repo gly-2" aria-hidden="true"></span><br /> <code> ion-fork-repo </code></li>
                            <li> <span class="fa ion-network gly-2" aria-hidden="true"></span><br /> <code> ion-network </code></li>
                            <li> <span class="fa ion-pull-request gly-2" aria-hidden="true"></span><br /> <code> ion-pull-request </code></li>
                            <li> <span class="fa ion-merge gly-2" aria-hidden="true"></span><br /> <code> ion-merge </code></li>
                            <li> <span class="fa ion-xbox gly-2" aria-hidden="true"></span><br /> <code> ion-xbox </code></li>
                            <li> <span class="fa ion-playstation gly-2" aria-hidden="true"></span><br /> <code> ion-playstation </code></li>
                            <li> <span class="fa ion-steam gly-2" aria-hidden="true"></span><br /> <code> ion-steam </code></li>
                            <li> <span class="fa ion-closed-captioning gly-2" aria-hidden="true"></span><br /> <code> ion-closed-captioning </code></li>
                            <li> <span class="fa ion-videocamera gly-2" aria-hidden="true"></span><br /> <code> ion-videocamera </code></li>
                            <li> <span class="fa ion-film-marker gly-2" aria-hidden="true"></span><br /> <code> ion-film-marker </code></li>
                            <li> <span class="fa ion-disc gly-2" aria-hidden="true"></span><br /> <code> ion-disc </code></li>
                            <li> <span class="fa ion-headphone gly-2" aria-hidden="true"></span><br /> <code> ion-headphone </code></li>
                            <li> <span class="fa ion-music-note gly-2" aria-hidden="true"></span><br /> <code> ion-music-note </code></li>
                            <li> <span class="fa ion-radio-waves gly-2" aria-hidden="true"></span><br /> <code> ion-radio-waves </code></li>
                            <li> <span class="fa ion-speakerphone gly-2" aria-hidden="true"></span><br /> <code> ion-speakerphone </code></li>
                            <li> <span class="fa ion-mic-a gly-2" aria-hidden="true"></span><br /> <code> ion-mic-a </code></li>
                            <li> <span class="fa ion-mic-b gly-2" aria-hidden="true"></span><br /> <code> ion-mic-b </code></li>
                            <li> <span class="fa ion-mic-c gly-2" aria-hidden="true"></span><br /> <code> ion-mic-c </code></li>
                            <li> <span class="fa ion-volume-high gly-2" aria-hidden="true"></span><br /> <code> ion-volume-high </code></li>
                            <li> <span class="fa ion-volume-medium gly-2" aria-hidden="true"></span><br /> <code> ion-volume-medium </code></li>
                            <li> <span class="fa ion-volume-low gly-2" aria-hidden="true"></span><br /> <code> ion-volume-low </code></li>
                            <li> <span class="fa ion-volume-mute gly-2" aria-hidden="true"></span><br /> <code> ion-volume-mute </code></li>
                            <li> <span class="fa ion-levels gly-2" aria-hidden="true"></span><br /> <code> ion-levels </code></li>
                            <li> <span class="fa ion-play gly-2" aria-hidden="true"></span><br /> <code> ion-play </code></li>
                            <li> <span class="fa ion-pause gly-2" aria-hidden="true"></span><br /> <code> ion-pause </code></li>
                            <li> <span class="fa ion-stop gly-2" aria-hidden="true"></span><br /> <code> ion-stop </code></li>
                            <li> <span class="fa ion-record gly-2" aria-hidden="true"></span><br /> <code> ion-record </code></li>
                            <li> <span class="fa ion-skip-forward gly-2" aria-hidden="true"></span><br /> <code> ion-skip-forward </code></li>
                            <li> <span class="fa ion-skip-backward gly-2" aria-hidden="true"></span><br /> <code> ion-skip-backward </code></li>
                            <li> <span class="fa ion-eject gly-2" aria-hidden="true"></span><br /> <code> ion-eject </code></li>
                            <li> <span class="fa ion-bag gly-2" aria-hidden="true"></span><br /> <code> ion-bag </code></li>
                            <li> <span class="fa ion-card gly-2" aria-hidden="true"></span><br /> <code> ion-card </code></li>
                            <li> <span class="fa ion-cash gly-2" aria-hidden="true"></span><br /> <code> ion-cash </code></li>
                            <li> <span class="fa ion-pricetag gly-2" aria-hidden="true"></span><br /> <code> ion-pricetag </code></li>
                            <li> <span class="fa ion-pricetags gly-2" aria-hidden="true"></span><br /> <code> ion-pricetags </code></li>
                            <li> <span class="fa ion-thumbsup gly-2" aria-hidden="true"></span><br /> <code> ion-thumbsup </code></li>
                            <li> <span class="fa ion-thumbsdown gly-2" aria-hidden="true"></span><br /> <code> ion-thumbsdown </code></li>
                            <li> <span class="fa ion-happy-outline gly-2" aria-hidden="true"></span><br /> <code> ion-happy-outline </code></li>
                            <li> <span class="fa ion-happy gly-2" aria-hidden="true"></span><br /> <code> ion-happy </code></li>
                            <li> <span class="fa ion-sad-outline gly-2" aria-hidden="true"></span><br /> <code> ion-sad-outline </code></li>
                            <li> <span class="fa ion-sad gly-2" aria-hidden="true"></span><br /> <code> ion-sad </code></li>
                            <li> <span class="fa ion-bowtie gly-2" aria-hidden="true"></span><br /> <code> ion-bowtie </code></li>
                            <li> <span class="fa ion-tshirt-outline gly-2" aria-hidden="true"></span><br /> <code> ion-tshirt-outline </code></li>
                            <li> <span class="fa ion-tshirt gly-2" aria-hidden="true"></span><br /> <code> ion-tshirt </code></li>
                            <li> <span class="fa ion-trophy gly-2" aria-hidden="true"></span><br /> <code> ion-trophy </code></li>
                            <li> <span class="fa ion-podium gly-2" aria-hidden="true"></span><br /> <code> ion-podium </code></li>
                            <li> <span class="fa ion-ribbon-a gly-2" aria-hidden="true"></span><br /> <code> ion-ribbon-a </code></li>
                            <li> <span class="fa ion-ribbon-b gly-2" aria-hidden="true"></span><br /> <code> ion-ribbon-b </code></li>
                            <li> <span class="fa ion-university gly-2" aria-hidden="true"></span><br /> <code> ion-university </code></li>
                            <li> <span class="fa ion-magnet gly-2" aria-hidden="true"></span><br /> <code> ion-magnet </code></li>
                            <li> <span class="fa ion-beaker gly-2" aria-hidden="true"></span><br /> <code> ion-beaker </code></li>
                            <li> <span class="fa ion-erlenmeyer-flask gly-2" aria-hidden="true"></span><br /> <code> ion-erlenmeyer-flask </code></li>
                            <li> <span class="fa ion-egg gly-2" aria-hidden="true"></span><br /> <code> ion-egg </code></li>
                            <li> <span class="fa ion-earth gly-2" aria-hidden="true"></span><br /> <code> ion-earth </code></li>
                            <li> <span class="fa ion-planet gly-2" aria-hidden="true"></span><br /> <code> ion-planet </code></li>
                            <li> <span class="fa ion-lightbulb gly-2" aria-hidden="true"></span><br /> <code> ion-lightbulb </code></li>
                            <li> <span class="fa ion-cube gly-2" aria-hidden="true"></span><br /> <code> ion-cube </code></li>
                            <li> <span class="fa ion-leaf gly-2" aria-hidden="true"></span><br /> <code> ion-leaf </code></li>
                            <li> <span class="fa ion-waterdrop gly-2" aria-hidden="true"></span><br /> <code> ion-waterdrop </code></li>
                            <li> <span class="fa ion-flame gly-2" aria-hidden="true"></span><br /> <code> ion-flame </code></li>
                            <li> <span class="fa ion-fireball gly-2" aria-hidden="true"></span><br /> <code> ion-fireball </code></li>
                            <li> <span class="fa ion-bonfire gly-2" aria-hidden="true"></span><br /> <code> ion-bonfire </code></li>
                            <li> <span class="fa ion-umbrella gly-2" aria-hidden="true"></span><br /> <code> ion-umbrella </code></li>
                            <li> <span class="fa ion-nuclear gly-2" aria-hidden="true"></span><br /> <code> ion-nuclear </code></li>
                            <li> <span class="fa ion-no-smoking gly-2" aria-hidden="true"></span><br /> <code> ion-no-smoking </code></li>
                            <li> <span class="fa ion-thermometer gly-2" aria-hidden="true"></span><br /> <code> ion-thermometer </code></li>
                        <ul>
                    </div>
                    <div class="col-md-4">
                        <ul  class="list-unstyled">
                            <li> <span class="fa ion-speedometer gly-2" aria-hidden="true"></span><br /> <code> ion-speedometer </code></li>
                            <li> <span class="fa ion-model-s gly-2" aria-hidden="true"></span><br /> <code> ion-model-s </code></li>
                            <li> <span class="fa ion-plane gly-2" aria-hidden="true"></span><br /> <code> ion-plane </code></li>
                            <li> <span class="fa ion-jet gly-2" aria-hidden="true"></span><br /> <code> ion-jet </code></li>
                            <li> <span class="fa ion-load-a gly-2" aria-hidden="true"></span><br /> <code> ion-load-a </code></li>
                            <li> <span class="fa ion-load-b gly-2" aria-hidden="true"></span><br /> <code> ion-load-b </code></li>
                            <li> <span class="fa ion-load-c gly-2" aria-hidden="true"></span><br /> <code> ion-load-c </code></li>
                            <li> <span class="fa ion-load-d gly-2" aria-hidden="true"></span><br /> <code> ion-load-d </code></li>
                            <li> <span class="fa ion-ios-ionic-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-ionic-outline </code></li>
                            <li> <span class="fa ion-ios-arrow-back gly-2" aria-hidden="true"></span><br /> <code> ion-ios-arrow-back </code></li>
                            <li> <span class="fa ion-ios-arrow-forward gly-2" aria-hidden="true"></span><br /> <code> ion-ios-arrow-forward </code></li>
                            <li> <span class="fa ion-ios-arrow-up gly-2" aria-hidden="true"></span><br /> <code> ion-ios-arrow-up </code></li>
                            <li> <span class="fa ion-ios-arrow-right gly-2" aria-hidden="true"></span><br /> <code> ion-ios-arrow-right </code></li>
                            <li> <span class="fa ion-ios-arrow-down gly-2" aria-hidden="true"></span><br /> <code> ion-ios-arrow-down </code></li>
                            <li> <span class="fa ion-ios-arrow-left gly-2" aria-hidden="true"></span><br /> <code> ion-ios-arrow-left </code></li>
                            <li> <span class="fa ion-ios-arrow-thin-up gly-2" aria-hidden="true"></span><br /> <code> ion-ios-arrow-thin-up </code></li>
                            <li> <span class="fa ion-ios-arrow-thin-right gly-2" aria-hidden="true"></span><br /> <code> ion-ios-arrow-thin-right </code></li>
                            <li> <span class="fa ion-ios-arrow-thin-down gly-2" aria-hidden="true"></span><br /> <code> ion-ios-arrow-thin-down </code></li>
                            <li> <span class="fa ion-ios-arrow-thin-left gly-2" aria-hidden="true"></span><br /> <code> ion-ios-arrow-thin-left </code></li>
                            <li> <span class="fa ion-ios-circle-filled gly-2" aria-hidden="true"></span><br /> <code> ion-ios-circle-filled </code></li>
                            <li> <span class="fa ion-ios-circle-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-circle-outline </code></li>
                            <li> <span class="fa ion-ios-checkmark-empty gly-2" aria-hidden="true"></span><br /> <code> ion-ios-checkmark-empty </code></li>
                            <li> <span class="fa ion-ios-checkmark-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-checkmark-outline </code></li>
                            <li> <span class="fa ion-ios-checkmark gly-2" aria-hidden="true"></span><br /> <code> ion-ios-checkmark </code></li>
                            <li> <span class="fa ion-ios-plus-empty gly-2" aria-hidden="true"></span><br /> <code> ion-ios-plus-empty </code></li>
                            <li> <span class="fa ion-ios-plus-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-plus-outline </code></li>
                            <li> <span class="fa ion-ios-plus gly-2" aria-hidden="true"></span><br /> <code> ion-ios-plus </code></li>
                            <li> <span class="fa ion-ios-close-empty gly-2" aria-hidden="true"></span><br /> <code> ion-ios-close-empty </code></li>
                            <li> <span class="fa ion-ios-close-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-close-outline </code></li>
                            <li> <span class="fa ion-ios-close gly-2" aria-hidden="true"></span><br /> <code> ion-ios-close </code></li>
                            <li> <span class="fa ion-ios-minus-empty gly-2" aria-hidden="true"></span><br /> <code> ion-ios-minus-empty </code></li>
                            <li> <span class="fa ion-ios-minus-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-minus-outline </code></li>
                            <li> <span class="fa ion-ios-minus gly-2" aria-hidden="true"></span><br /> <code> ion-ios-minus </code></li>
                            <li> <span class="fa ion-ios-information-empty gly-2" aria-hidden="true"></span><br /> <code> ion-ios-information-empty </code></li>
                            <li> <span class="fa ion-ios-information-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-information-outline </code></li>
                            <li> <span class="fa ion-ios-information gly-2" aria-hidden="true"></span><br /> <code> ion-ios-information </code></li>
                            <li> <span class="fa ion-ios-help-empty gly-2" aria-hidden="true"></span><br /> <code> ion-ios-help-empty </code></li>
                            <li> <span class="fa ion-ios-help-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-help-outline </code></li>
                            <li> <span class="fa ion-ios-help gly-2" aria-hidden="true"></span><br /> <code> ion-ios-help </code></li>
                            <li> <span class="fa ion-ios-search gly-2" aria-hidden="true"></span><br /> <code> ion-ios-search </code></li>
                            <li> <span class="fa ion-ios-search-strong gly-2" aria-hidden="true"></span><br /> <code> ion-ios-search-strong </code></li>
                            <li> <span class="fa ion-ios-star gly-2" aria-hidden="true"></span><br /> <code> ion-ios-star </code></li>
                            <li> <span class="fa ion-ios-star-half gly-2" aria-hidden="true"></span><br /> <code> ion-ios-star-half </code></li>
                            <li> <span class="fa ion-ios-star-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-star-outline </code></li>
                            <li> <span class="fa ion-ios-heart gly-2" aria-hidden="true"></span><br /> <code> ion-ios-heart </code></li>
                            <li> <span class="fa ion-ios-heart-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-heart-outline </code></li>
                            <li> <span class="fa ion-ios-more gly-2" aria-hidden="true"></span><br /> <code> ion-ios-more </code></li>
                            <li> <span class="fa ion-ios-more-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-more-outline </code></li>
                            <li> <span class="fa ion-ios-home gly-2" aria-hidden="true"></span><br /> <code> ion-ios-home </code></li>
                            <li> <span class="fa ion-ios-home-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-home-outline </code></li>
                            <li> <span class="fa ion-ios-cloud gly-2" aria-hidden="true"></span><br /> <code> ion-ios-cloud </code></li>
                            <li> <span class="fa ion-ios-cloud-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-cloud-outline </code></li>
                            <li> <span class="fa ion-ios-cloud-upload gly-2" aria-hidden="true"></span><br /> <code> ion-ios-cloud-upload </code></li>
                            <li> <span class="fa ion-ios-cloud-upload-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-cloud-upload-outline </code></li>
                            <li> <span class="fa ion-ios-cloud-download gly-2" aria-hidden="true"></span><br /> <code> ion-ios-cloud-download </code></li>
                            <li> <span class="fa ion-ios-cloud-download-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-cloud-download-outline </code></li>
                            <li> <span class="fa ion-ios-upload gly-2" aria-hidden="true"></span><br /> <code> ion-ios-upload </code></li>
                            <li> <span class="fa ion-ios-upload-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-upload-outline </code></li>
                            <li> <span class="fa ion-ios-download gly-2" aria-hidden="true"></span><br /> <code> ion-ios-download </code></li>
                            <li> <span class="fa ion-ios-download-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-download-outline </code></li>
                            <li> <span class="fa ion-ios-refresh gly-2" aria-hidden="true"></span><br /> <code> ion-ios-refresh </code></li>
                            <li> <span class="fa ion-ios-refresh-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-refresh-outline </code></li>
                            <li> <span class="fa ion-ios-refresh-empty gly-2" aria-hidden="true"></span><br /> <code> ion-ios-refresh-empty </code></li>
                            <li> <span class="fa ion-ios-reload gly-2" aria-hidden="true"></span><br /> <code> ion-ios-reload </code></li>
                            <li> <span class="fa ion-ios-loop-strong gly-2" aria-hidden="true"></span><br /> <code> ion-ios-loop-strong </code></li>
                            <li> <span class="fa ion-ios-loop gly-2" aria-hidden="true"></span><br /> <code> ion-ios-loop </code></li>
                            <li> <span class="fa ion-ios-bookmarks gly-2" aria-hidden="true"></span><br /> <code> ion-ios-bookmarks </code></li>
                            <li> <span class="fa ion-ios-bookmarks-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-bookmarks-outline </code></li>
                            <li> <span class="fa ion-ios-book gly-2" aria-hidden="true"></span><br /> <code> ion-ios-book </code></li>
                            <li> <span class="fa ion-ios-book-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-book-outline </code></li>
                            <li> <span class="fa ion-ios-flag gly-2" aria-hidden="true"></span><br /> <code> ion-ios-flag </code></li>
                            <li> <span class="fa ion-ios-flag-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-flag-outline </code></li>
                            <li> <span class="fa ion-ios-glasses gly-2" aria-hidden="true"></span><br /> <code> ion-ios-glasses </code></li>
                            <li> <span class="fa ion-ios-glasses-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-glasses-outline </code></li>
                            <li> <span class="fa ion-ios-browsers gly-2" aria-hidden="true"></span><br /> <code> ion-ios-browsers </code></li>
                            <li> <span class="fa ion-ios-browsers-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-browsers-outline </code></li>
                            <li> <span class="fa ion-ios-at gly-2" aria-hidden="true"></span><br /> <code> ion-ios-at </code></li>
                            <li> <span class="fa ion-ios-at-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-at-outline </code></li>
                            <li> <span class="fa ion-ios-pricetag gly-2" aria-hidden="true"></span><br /> <code> ion-ios-pricetag </code></li>
                            <li> <span class="fa ion-ios-pricetag-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-pricetag-outline </code></li>
                            <li> <span class="fa ion-ios-pricetags gly-2" aria-hidden="true"></span><br /> <code> ion-ios-pricetags </code></li>
                            <li> <span class="fa ion-ios-pricetags-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-pricetags-outline </code></li>
                            <li> <span class="fa ion-ios-cart gly-2" aria-hidden="true"></span><br /> <code> ion-ios-cart </code></li>
                            <li> <span class="fa ion-ios-cart-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-cart-outline </code></li>
                            <li> <span class="fa ion-ios-chatboxes gly-2" aria-hidden="true"></span><br /> <code> ion-ios-chatboxes </code></li>
                            <li> <span class="fa ion-ios-chatboxes-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-chatboxes-outline </code></li>
                            <li> <span class="fa ion-ios-chatbubble gly-2" aria-hidden="true"></span><br /> <code> ion-ios-chatbubble </code></li>
                            <li> <span class="fa ion-ios-chatbubble-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-chatbubble-outline </code></li>
                            <li> <span class="fa ion-ios-cog gly-2" aria-hidden="true"></span><br /> <code> ion-ios-cog </code></li>
                            <li> <span class="fa ion-ios-cog-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-cog-outline </code></li>
                            <li> <span class="fa ion-ios-gear gly-2" aria-hidden="true"></span><br /> <code> ion-ios-gear </code></li>
                            <li> <span class="fa ion-ios-gear-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-gear-outline </code></li>
                            <li> <span class="fa ion-ios-settings gly-2" aria-hidden="true"></span><br /> <code> ion-ios-settings </code></li>
                            <li> <span class="fa ion-ios-settings-strong gly-2" aria-hidden="true"></span><br /> <code> ion-ios-settings-strong </code></li>
                            <li> <span class="fa ion-ios-toggle gly-2" aria-hidden="true"></span><br /> <code> ion-ios-toggle </code></li>
                            <li> <span class="fa ion-ios-toggle-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-toggle-outline </code></li>
                            <li> <span class="fa ion-ios-analytics gly-2" aria-hidden="true"></span><br /> <code> ion-ios-analytics </code></li>
                            <li> <span class="fa ion-ios-analytics-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-analytics-outline </code></li>
                            <li> <span class="fa ion-ios-pie gly-2" aria-hidden="true"></span><br /> <code> ion-ios-pie </code></li>
                            <li> <span class="fa ion-ios-pie-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-pie-outline </code></li>
                            <li> <span class="fa ion-ios-pulse gly-2" aria-hidden="true"></span><br /> <code> ion-ios-pulse </code></li>
                            <li> <span class="fa ion-ios-pulse-strong gly-2" aria-hidden="true"></span><br /> <code> ion-ios-pulse-strong </code></li>
                            <li> <span class="fa ion-ios-filing gly-2" aria-hidden="true"></span><br /> <code> ion-ios-filing </code></li>
                            <li> <span class="fa ion-ios-filing-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-filing-outline </code></li>
                            <li> <span class="fa ion-ios-box gly-2" aria-hidden="true"></span><br /> <code> ion-ios-box </code></li>
                            <li> <span class="fa ion-ios-box-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-box-outline </code></li>
                            <li> <span class="fa ion-ios-compose gly-2" aria-hidden="true"></span><br /> <code> ion-ios-compose </code></li>
                            <li> <span class="fa ion-ios-compose-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-compose-outline </code></li>
                            <li> <span class="fa ion-ios-trash gly-2" aria-hidden="true"></span><br /> <code> ion-ios-trash </code></li>
                            <li> <span class="fa ion-ios-trash-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-trash-outline </code></li>
                            <li> <span class="fa ion-ios-copy gly-2" aria-hidden="true"></span><br /> <code> ion-ios-copy </code></li>
                            <li> <span class="fa ion-ios-copy-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-copy-outline </code></li>
                            <li> <span class="fa ion-ios-email gly-2" aria-hidden="true"></span><br /> <code> ion-ios-email </code></li>
                            <li> <span class="fa ion-ios-email-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-email-outline </code></li>
                            <li> <span class="fa ion-ios-undo gly-2" aria-hidden="true"></span><br /> <code> ion-ios-undo </code></li>
                            <li> <span class="fa ion-ios-undo-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-undo-outline </code></li>
                            <li> <span class="fa ion-ios-redo gly-2" aria-hidden="true"></span><br /> <code> ion-ios-redo </code></li>
                            <li> <span class="fa ion-ios-redo-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-redo-outline </code></li>
                            <li> <span class="fa ion-ios-paperplane gly-2" aria-hidden="true"></span><br /> <code> ion-ios-paperplane </code></li>
                            <li> <span class="fa ion-ios-paperplane-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-paperplane-outline </code></li>
                            <li> <span class="fa ion-ios-folder gly-2" aria-hidden="true"></span><br /> <code> ion-ios-folder </code></li>
                            <li> <span class="fa ion-ios-folder-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-folder-outline </code></li>
                            <li> <span class="fa ion-ios-paper gly-2" aria-hidden="true"></span><br /> <code> ion-ios-paper </code></li>
                            <li> <span class="fa ion-ios-paper-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-paper-outline </code></li>
                            <li> <span class="fa ion-ios-list gly-2" aria-hidden="true"></span><br /> <code> ion-ios-list </code></li>
                            <li> <span class="fa ion-ios-list-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-list-outline </code></li>
                            <li> <span class="fa ion-ios-world gly-2" aria-hidden="true"></span><br /> <code> ion-ios-world </code></li>
                            <li> <span class="fa ion-ios-world-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-world-outline </code></li>
                            <li> <span class="fa ion-ios-alarm gly-2" aria-hidden="true"></span><br /> <code> ion-ios-alarm </code></li>
                            <li> <span class="fa ion-ios-alarm-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-alarm-outline </code></li>
                            <li> <span class="fa ion-ios-speedometer gly-2" aria-hidden="true"></span><br /> <code> ion-ios-speedometer </code></li>
                            <li> <span class="fa ion-ios-speedometer-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-speedometer-outline </code></li>
                            <li> <span class="fa ion-ios-stopwatch gly-2" aria-hidden="true"></span><br /> <code> ion-ios-stopwatch </code></li>
                            <li> <span class="fa ion-ios-stopwatch-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-stopwatch-outline </code></li>
                            <li> <span class="fa ion-ios-timer gly-2" aria-hidden="true"></span><br /> <code> ion-ios-timer </code></li>
                            <li> <span class="fa ion-ios-timer-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-timer-outline </code></li>
                            <li> <span class="fa ion-ios-clock gly-2" aria-hidden="true"></span><br /> <code> ion-ios-clock </code></li>
                            <li> <span class="fa ion-ios-clock-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-clock-outline </code></li>
                            <li> <span class="fa ion-ios-time gly-2" aria-hidden="true"></span><br /> <code> ion-ios-time </code></li>
                            <li> <span class="fa ion-ios-time-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-time-outline </code></li>
                            <li> <span class="fa ion-ios-calendar gly-2" aria-hidden="true"></span><br /> <code> ion-ios-calendar </code></li>
                            <li> <span class="fa ion-ios-calendar-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-calendar-outline </code></li>
                            <li> <span class="fa ion-ios-photos gly-2" aria-hidden="true"></span><br /> <code> ion-ios-photos </code></li>
                            <li> <span class="fa ion-ios-photos-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-photos-outline </code></li>
                            <li> <span class="fa ion-ios-albums gly-2" aria-hidden="true"></span><br /> <code> ion-ios-albums </code></li>
                            <li> <span class="fa ion-ios-albums-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-albums-outline </code></li>
                            <li> <span class="fa ion-ios-camera gly-2" aria-hidden="true"></span><br /> <code> ion-ios-camera </code></li>
                            <li> <span class="fa ion-ios-camera-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-camera-outline </code></li>
                            <li> <span class="fa ion-ios-reverse-camera gly-2" aria-hidden="true"></span><br /> <code> ion-ios-reverse-camera </code></li>
                            <li> <span class="fa ion-ios-reverse-camera-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-reverse-camera-outline </code></li>
                            <li> <span class="fa ion-ios-eye gly-2" aria-hidden="true"></span><br /> <code> ion-ios-eye </code></li>
                            <li> <span class="fa ion-ios-eye-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-eye-outline </code></li>
                            <li> <span class="fa ion-ios-bolt gly-2" aria-hidden="true"></span><br /> <code> ion-ios-bolt </code></li>
                            <li> <span class="fa ion-ios-bolt-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-bolt-outline </code></li>
                            <li> <span class="fa ion-ios-color-wand gly-2" aria-hidden="true"></span><br /> <code> ion-ios-color-wand </code></li>
                            <li> <span class="fa ion-ios-color-wand-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-color-wand-outline </code></li>
                            <li> <span class="fa ion-ios-color-filter gly-2" aria-hidden="true"></span><br /> <code> ion-ios-color-filter </code></li>
                            <li> <span class="fa ion-ios-color-filter-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-color-filter-outline </code></li>
                            <li> <span class="fa ion-ios-grid-view gly-2" aria-hidden="true"></span><br /> <code> ion-ios-grid-view </code></li>
                            <li> <span class="fa ion-ios-grid-view-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-grid-view-outline </code></li>
                            <li> <span class="fa ion-ios-crop-strong gly-2" aria-hidden="true"></span><br /> <code> ion-ios-crop-strong </code></li>
                            <li> <span class="fa ion-ios-crop gly-2" aria-hidden="true"></span><br /> <code> ion-ios-crop </code></li>
                            <li> <span class="fa ion-ios-barcode gly-2" aria-hidden="true"></span><br /> <code> ion-ios-barcode </code></li>
                            <li> <span class="fa ion-ios-barcode-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-barcode-outline </code></li>
                            <li> <span class="fa ion-ios-briefcase gly-2" aria-hidden="true"></span><br /> <code> ion-ios-briefcase </code></li>
                            <li> <span class="fa ion-ios-briefcase-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-briefcase-outline </code></li>
                            <li> <span class="fa ion-ios-medkit gly-2" aria-hidden="true"></span><br /> <code> ion-ios-medkit </code></li>
                            <li> <span class="fa ion-ios-medkit-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-medkit-outline </code></li>
                            <li> <span class="fa ion-ios-medical gly-2" aria-hidden="true"></span><br /> <code> ion-ios-medical </code></li>
                            <li> <span class="fa ion-ios-medical-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-medical-outline </code></li>
                            <li> <span class="fa ion-ios-infinite gly-2" aria-hidden="true"></span><br /> <code> ion-ios-infinite </code></li>
                            <li> <span class="fa ion-ios-infinite-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-infinite-outline </code></li>
                            <li> <span class="fa ion-ios-calculator gly-2" aria-hidden="true"></span><br /> <code> ion-ios-calculator </code></li>
                            <li> <span class="fa ion-ios-calculator-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-calculator-outline </code></li>
                            <li> <span class="fa ion-ios-keypad gly-2" aria-hidden="true"></span><br /> <code> ion-ios-keypad </code></li>
                            <li> <span class="fa ion-ios-keypad-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-keypad-outline </code></li>
                            <li> <span class="fa ion-ios-telephone gly-2" aria-hidden="true"></span><br /> <code> ion-ios-telephone </code></li>
                            <li> <span class="fa ion-ios-telephone-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-telephone-outline </code></li>
                            <li> <span class="fa ion-ios-drag gly-2" aria-hidden="true"></span><br /> <code> ion-ios-drag </code></li>
                            <li> <span class="fa ion-ios-location gly-2" aria-hidden="true"></span><br /> <code> ion-ios-location </code></li>
                            <li> <span class="fa ion-ios-location-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-location-outline </code></li>
                            <li> <span class="fa ion-ios-navigate gly-2" aria-hidden="true"></span><br /> <code> ion-ios-navigate </code></li>
                            <li> <span class="fa ion-ios-navigate-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-navigate-outline </code></li>
                            <li> <span class="fa ion-ios-locked gly-2" aria-hidden="true"></span><br /> <code> ion-ios-locked </code></li>
                            <li> <span class="fa ion-ios-locked-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-locked-outline </code></li>
                            <li> <span class="fa ion-ios-unlocked gly-2" aria-hidden="true"></span><br /> <code> ion-ios-unlocked </code></li>
                            <li> <span class="fa ion-ios-unlocked-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-unlocked-outline </code></li>
                            <li> <span class="fa ion-ios-monitor gly-2" aria-hidden="true"></span><br /> <code> ion-ios-monitor </code></li>
                            <li> <span class="fa ion-ios-monitor-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-monitor-outline </code></li>
                            <li> <span class="fa ion-ios-printer gly-2" aria-hidden="true"></span><br /> <code> ion-ios-printer </code></li>
                            <li> <span class="fa ion-ios-printer-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-printer-outline </code></li>
                            <li> <span class="fa ion-ios-game-controller-a gly-2" aria-hidden="true"></span><br /> <code> ion-ios-game-controller-a </code></li>
                            <li> <span class="fa ion-ios-game-controller-a-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-game-controller-a-outline </code></li>
                            <li> <span class="fa ion-ios-game-controller-b gly-2" aria-hidden="true"></span><br /> <code> ion-ios-game-controller-b </code></li>
                            <li> <span class="fa ion-ios-game-controller-b-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-game-controller-b-outline </code></li>
                            <li> <span class="fa ion-ios-americanfootball gly-2" aria-hidden="true"></span><br /> <code> ion-ios-americanfootball </code></li>
                            <li> <span class="fa ion-ios-americanfootball-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-americanfootball-outline </code></li>
                            <li> <span class="fa ion-ios-baseball gly-2" aria-hidden="true"></span><br /> <code> ion-ios-baseball </code></li>
                            <li> <span class="fa ion-ios-baseball-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-baseball-outline </code></li>
                            <li> <span class="fa ion-ios-basketball gly-2" aria-hidden="true"></span><br /> <code> ion-ios-basketball </code></li>
                            <li> <span class="fa ion-ios-basketball-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-basketball-outline </code></li>
                            <li> <span class="fa ion-ios-tennisball gly-2" aria-hidden="true"></span><br /> <code> ion-ios-tennisball </code></li>
                            <li> <span class="fa ion-ios-tennisball-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-tennisball-outline </code></li>
                            <li> <span class="fa ion-ios-football gly-2" aria-hidden="true"></span><br /> <code> ion-ios-football </code></li>
                            <li> <span class="fa ion-ios-football-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-football-outline </code></li>
                            <li> <span class="fa ion-ios-body gly-2" aria-hidden="true"></span><br /> <code> ion-ios-body </code></li>
                            <li> <span class="fa ion-ios-body-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-body-outline </code></li>
                            <li> <span class="fa ion-ios-person gly-2" aria-hidden="true"></span><br /> <code> ion-ios-person </code></li>
                            <li> <span class="fa ion-ios-person-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-person-outline </code></li>
                            <li> <span class="fa ion-ios-personadd gly-2" aria-hidden="true"></span><br /> <code> ion-ios-personadd </code></li>
                            <li> <span class="fa ion-ios-personadd-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-personadd-outline </code></li>
                            <li> <span class="fa ion-ios-people gly-2" aria-hidden="true"></span><br /> <code> ion-ios-people </code></li>
                            <li> <span class="fa ion-ios-people-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-people-outline </code></li>
                            <li> <span class="fa ion-ios-musical-notes gly-2" aria-hidden="true"></span><br /> <code> ion-ios-musical-notes </code></li>
                            <li> <span class="fa ion-ios-musical-note gly-2" aria-hidden="true"></span><br /> <code> ion-ios-musical-note </code></li>
                            <li> <span class="fa ion-ios-bell gly-2" aria-hidden="true"></span><br /> <code> ion-ios-bell </code></li>
                            <li> <span class="fa ion-ios-bell-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-bell-outline </code></li>
                            <li> <span class="fa ion-ios-mic gly-2" aria-hidden="true"></span><br /> <code> ion-ios-mic </code></li>
                            <li> <span class="fa ion-ios-mic-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-mic-outline </code></li>
                            <li> <span class="fa ion-ios-mic-off gly-2" aria-hidden="true"></span><br /> <code> ion-ios-mic-off </code></li>
                            <li> <span class="fa ion-ios-volume-high gly-2" aria-hidden="true"></span><br /> <code> ion-ios-volume-high </code></li>
                            <li> <span class="fa ion-ios-volume-low gly-2" aria-hidden="true"></span><br /> <code> ion-ios-volume-low </code></li>
                            <li> <span class="fa ion-ios-play gly-2" aria-hidden="true"></span><br /> <code> ion-ios-play </code></li>
                            <li> <span class="fa ion-ios-play-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-play-outline </code></li>
                            <li> <span class="fa ion-ios-pause gly-2" aria-hidden="true"></span><br /> <code> ion-ios-pause </code></li>
                            <li> <span class="fa ion-ios-pause-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-pause-outline </code></li>
                            <li> <span class="fa ion-ios-recording gly-2" aria-hidden="true"></span><br /> <code> ion-ios-recording </code></li>
                            <li> <span class="fa ion-ios-recording-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-recording-outline </code></li>
                            <li> <span class="fa ion-ios-fastforward gly-2" aria-hidden="true"></span><br /> <code> ion-ios-fastforward </code></li>
                            <li> <span class="fa ion-ios-fastforward-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-fastforward-outline </code></li>
                            <li> <span class="fa ion-ios-rewind gly-2" aria-hidden="true"></span><br /> <code> ion-ios-rewind </code></li>
                            <li> <span class="fa ion-ios-rewind-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-rewind-outline </code></li>
                            <li> <span class="fa ion-ios-skipbackward gly-2" aria-hidden="true"></span><br /> <code> ion-ios-skipbackward </code></li>
                            <li> <span class="fa ion-ios-skipbackward-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-skipbackward-outline </code></li>
                            <li> <span class="fa ion-ios-skipforward gly-2" aria-hidden="true"></span><br /> <code> ion-ios-skipforward </code></li>
                            <li> <span class="fa ion-ios-skipforward-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-skipforward-outline </code></li>
                            <li> <span class="fa ion-ios-shuffle-strong gly-2" aria-hidden="true"></span><br /> <code> ion-ios-shuffle-strong </code></li>
                            <li> <span class="fa ion-ios-shuffle gly-2" aria-hidden="true"></span><br /> <code> ion-ios-shuffle </code></li>
                            <li> <span class="fa ion-ios-videocam gly-2" aria-hidden="true"></span><br /> <code> ion-ios-videocam </code></li>
                            <li> <span class="fa ion-ios-videocam-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-videocam-outline </code></li>
                            <li> <span class="fa ion-ios-film gly-2" aria-hidden="true"></span><br /> <code> ion-ios-film </code></li>
                            <li> <span class="fa ion-ios-film-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-film-outline </code></li>
                            <li> <span class="fa ion-ios-flask gly-2" aria-hidden="true"></span><br /> <code> ion-ios-flask </code></li>
                        <ul>
                    </div>
                    <div class="col-md-4">
                        <ul  class="list-unstyled">
                            <li> <span class="fa ion-ios-flask-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-flask-outline </code></li>
                            <li> <span class="fa ion-ios-lightbulb gly-2" aria-hidden="true"></span><br /> <code> ion-ios-lightbulb </code></li>
                            <li> <span class="fa ion-ios-lightbulb-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-lightbulb-outline </code></li>
                            <li> <span class="fa ion-ios-wineglass gly-2" aria-hidden="true"></span><br /> <code> ion-ios-wineglass </code></li>
                            <li> <span class="fa ion-ios-wineglass-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-wineglass-outline </code></li>
                            <li> <span class="fa ion-ios-pint gly-2" aria-hidden="true"></span><br /> <code> ion-ios-pint </code></li>
                            <li> <span class="fa ion-ios-pint-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-pint-outline </code></li>
                            <li> <span class="fa ion-ios-nutrition gly-2" aria-hidden="true"></span><br /> <code> ion-ios-nutrition </code></li>
                            <li> <span class="fa ion-ios-nutrition-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-nutrition-outline </code></li>
                            <li> <span class="fa ion-ios-flower gly-2" aria-hidden="true"></span><br /> <code> ion-ios-flower </code></li>
                            <li> <span class="fa ion-ios-flower-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-flower-outline </code></li>
                            <li> <span class="fa ion-ios-rose gly-2" aria-hidden="true"></span><br /> <code> ion-ios-rose </code></li>
                            <li> <span class="fa ion-ios-rose-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-rose-outline </code></li>
                            <li> <span class="fa ion-ios-paw gly-2" aria-hidden="true"></span><br /> <code> ion-ios-paw </code></li>
                            <li> <span class="fa ion-ios-paw-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-paw-outline </code></li>
                            <li> <span class="fa ion-ios-flame gly-2" aria-hidden="true"></span><br /> <code> ion-ios-flame </code></li>
                            <li> <span class="fa ion-ios-flame-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-flame-outline </code></li>
                            <li> <span class="fa ion-ios-sunny gly-2" aria-hidden="true"></span><br /> <code> ion-ios-sunny </code></li>
                            <li> <span class="fa ion-ios-sunny-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-sunny-outline </code></li>
                            <li> <span class="fa ion-ios-partlysunny gly-2" aria-hidden="true"></span><br /> <code> ion-ios-partlysunny </code></li>
                            <li> <span class="fa ion-ios-partlysunny-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-partlysunny-outline </code></li>
                            <li> <span class="fa ion-ios-cloudy gly-2" aria-hidden="true"></span><br /> <code> ion-ios-cloudy </code></li>
                            <li> <span class="fa ion-ios-cloudy-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-cloudy-outline </code></li>
                            <li> <span class="fa ion-ios-rainy gly-2" aria-hidden="true"></span><br /> <code> ion-ios-rainy </code></li>
                            <li> <span class="fa ion-ios-rainy-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-rainy-outline </code></li>
                            <li> <span class="fa ion-ios-thunderstorm gly-2" aria-hidden="true"></span><br /> <code> ion-ios-thunderstorm </code></li>
                            <li> <span class="fa ion-ios-thunderstorm-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-thunderstorm-outline </code></li>
                            <li> <span class="fa ion-ios-snowy gly-2" aria-hidden="true"></span><br /> <code> ion-ios-snowy </code></li>
                            <li> <span class="fa ion-ios-moon gly-2" aria-hidden="true"></span><br /> <code> ion-ios-moon </code></li>
                            <li> <span class="fa ion-ios-moon-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-moon-outline </code></li>
                            <li> <span class="fa ion-ios-cloudy-night gly-2" aria-hidden="true"></span><br /> <code> ion-ios-cloudy-night </code></li>
                            <li> <span class="fa ion-ios-cloudy-night-outline gly-2" aria-hidden="true"></span><br /> <code> ion-ios-cloudy-night-outline </code></li>
                            <li> <span class="fa ion-android-arrow-up gly-2" aria-hidden="true"></span><br /> <code> ion-android-arrow-up </code></li>
                            <li> <span class="fa ion-android-arrow-forward gly-2" aria-hidden="true"></span><br /> <code> ion-android-arrow-forward </code></li>
                            <li> <span class="fa ion-android-arrow-down gly-2" aria-hidden="true"></span><br /> <code> ion-android-arrow-down </code></li>
                            <li> <span class="fa ion-android-arrow-back gly-2" aria-hidden="true"></span><br /> <code> ion-android-arrow-back </code></li>
                            <li> <span class="fa ion-android-arrow-dropup gly-2" aria-hidden="true"></span><br /> <code> ion-android-arrow-dropup </code></li>
                            <li> <span class="fa ion-android-arrow-dropup-circle gly-2" aria-hidden="true"></span><br /> <code> ion-android-arrow-dropup-circle </code></li>
                            <li> <span class="fa ion-android-arrow-dropright gly-2" aria-hidden="true"></span><br /> <code> ion-android-arrow-dropright </code></li>
                            <li> <span class="fa ion-android-arrow-dropright-circle gly-2" aria-hidden="true"></span><br /> <code> ion-android-arrow-dropright-circle </code></li>
                            <li> <span class="fa ion-android-arrow-dropdown gly-2" aria-hidden="true"></span><br /> <code> ion-android-arrow-dropdown </code></li>
                            <li> <span class="fa ion-android-arrow-dropdown-circle gly-2" aria-hidden="true"></span><br /> <code> ion-android-arrow-dropdown-circle </code></li>
                            <li> <span class="fa ion-android-arrow-dropleft gly-2" aria-hidden="true"></span><br /> <code> ion-android-arrow-dropleft </code></li>
                            <li> <span class="fa ion-android-arrow-dropleft-circle gly-2" aria-hidden="true"></span><br /> <code> ion-android-arrow-dropleft-circle </code></li>
                            <li> <span class="fa ion-android-add gly-2" aria-hidden="true"></span><br /> <code> ion-android-add </code></li>
                            <li> <span class="fa ion-android-add-circle gly-2" aria-hidden="true"></span><br /> <code> ion-android-add-circle </code></li>
                            <li> <span class="fa ion-android-remove gly-2" aria-hidden="true"></span><br /> <code> ion-android-remove </code></li>
                            <li> <span class="fa ion-android-remove-circle gly-2" aria-hidden="true"></span><br /> <code> ion-android-remove-circle </code></li>
                            <li> <span class="fa ion-android-close gly-2" aria-hidden="true"></span><br /> <code> ion-android-close </code></li>
                            <li> <span class="fa ion-android-cancel gly-2" aria-hidden="true"></span><br /> <code> ion-android-cancel </code></li>
                            <li> <span class="fa ion-android-radio-button-off gly-2" aria-hidden="true"></span><br /> <code> ion-android-radio-button-off </code></li>
                            <li> <span class="fa ion-android-radio-button-on gly-2" aria-hidden="true"></span><br /> <code> ion-android-radio-button-on </code></li>
                            <li> <span class="fa ion-android-checkmark-circle gly-2" aria-hidden="true"></span><br /> <code> ion-android-checkmark-circle </code></li>
                            <li> <span class="fa ion-android-checkbox-outline-blank gly-2" aria-hidden="true"></span><br /> <code> ion-android-checkbox-outline-blank </code></li>
                            <li> <span class="fa ion-android-checkbox-outline gly-2" aria-hidden="true"></span><br /> <code> ion-android-checkbox-outline </code></li>
                            <li> <span class="fa ion-android-checkbox-blank gly-2" aria-hidden="true"></span><br /> <code> ion-android-checkbox-blank </code></li>
                            <li> <span class="fa ion-android-checkbox gly-2" aria-hidden="true"></span><br /> <code> ion-android-checkbox </code></li>
                            <li> <span class="fa ion-android-done gly-2" aria-hidden="true"></span><br /> <code> ion-android-done </code></li>
                            <li> <span class="fa ion-android-done-all gly-2" aria-hidden="true"></span><br /> <code> ion-android-done-all </code></li>
                            <li> <span class="fa ion-android-menu gly-2" aria-hidden="true"></span><br /> <code> ion-android-menu </code></li>
                            <li> <span class="fa ion-android-more-horizontal gly-2" aria-hidden="true"></span><br /> <code> ion-android-more-horizontal </code></li>
                            <li> <span class="fa ion-android-more-vertical gly-2" aria-hidden="true"></span><br /> <code> ion-android-more-vertical </code></li>
                            <li> <span class="fa ion-android-refresh gly-2" aria-hidden="true"></span><br /> <code> ion-android-refresh </code></li>
                            <li> <span class="fa ion-android-sync gly-2" aria-hidden="true"></span><br /> <code> ion-android-sync </code></li>
                            <li> <span class="fa ion-android-wifi gly-2" aria-hidden="true"></span><br /> <code> ion-android-wifi </code></li>
                            <li> <span class="fa ion-android-call gly-2" aria-hidden="true"></span><br /> <code> ion-android-call </code></li>
                            <li> <span class="fa ion-android-apps gly-2" aria-hidden="true"></span><br /> <code> ion-android-apps </code></li>
                            <li> <span class="fa ion-android-settings gly-2" aria-hidden="true"></span><br /> <code> ion-android-settings </code></li>
                            <li> <span class="fa ion-android-options gly-2" aria-hidden="true"></span><br /> <code> ion-android-options </code></li>
                            <li> <span class="fa ion-android-funnel gly-2" aria-hidden="true"></span><br /> <code> ion-android-funnel </code></li>
                            <li> <span class="fa ion-android-search gly-2" aria-hidden="true"></span><br /> <code> ion-android-search </code></li>
                            <li> <span class="fa ion-android-home gly-2" aria-hidden="true"></span><br /> <code> ion-android-home </code></li>
                            <li> <span class="fa ion-android-cloud-outline gly-2" aria-hidden="true"></span><br /> <code> ion-android-cloud-outline </code></li>
                            <li> <span class="fa ion-android-cloud gly-2" aria-hidden="true"></span><br /> <code> ion-android-cloud </code></li>
                            <li> <span class="fa ion-android-download gly-2" aria-hidden="true"></span><br /> <code> ion-android-download </code></li>
                            <li> <span class="fa ion-android-upload gly-2" aria-hidden="true"></span><br /> <code> ion-android-upload </code></li>
                            <li> <span class="fa ion-android-cloud-done gly-2" aria-hidden="true"></span><br /> <code> ion-android-cloud-done </code></li>
                            <li> <span class="fa ion-android-cloud-circle gly-2" aria-hidden="true"></span><br /> <code> ion-android-cloud-circle </code></li>
                            <li> <span class="fa ion-android-favorite-outline gly-2" aria-hidden="true"></span><br /> <code> ion-android-favorite-outline </code></li>
                            <li> <span class="fa ion-android-favorite gly-2" aria-hidden="true"></span><br /> <code> ion-android-favorite </code></li>
                            <li> <span class="fa ion-android-star-outline gly-2" aria-hidden="true"></span><br /> <code> ion-android-star-outline </code></li>
                            <li> <span class="fa ion-android-star-half gly-2" aria-hidden="true"></span><br /> <code> ion-android-star-half </code></li>
                            <li> <span class="fa ion-android-star gly-2" aria-hidden="true"></span><br /> <code> ion-android-star </code></li>
                            <li> <span class="fa ion-android-calendar gly-2" aria-hidden="true"></span><br /> <code> ion-android-calendar </code></li>
                            <li> <span class="fa ion-android-alarm-clock gly-2" aria-hidden="true"></span><br /> <code> ion-android-alarm-clock </code></li>
                            <li> <span class="fa ion-android-time gly-2" aria-hidden="true"></span><br /> <code> ion-android-time </code></li>
                            <li> <span class="fa ion-android-stopwatch gly-2" aria-hidden="true"></span><br /> <code> ion-android-stopwatch </code></li>
                            <li> <span class="fa ion-android-watch gly-2" aria-hidden="true"></span><br /> <code> ion-android-watch </code></li>
                            <li> <span class="fa ion-android-locate gly-2" aria-hidden="true"></span><br /> <code> ion-android-locate </code></li>
                            <li> <span class="fa ion-android-navigate gly-2" aria-hidden="true"></span><br /> <code> ion-android-navigate </code></li>
                            <li> <span class="fa ion-android-pin gly-2" aria-hidden="true"></span><br /> <code> ion-android-pin </code></li>
                            <li> <span class="fa ion-android-compass gly-2" aria-hidden="true"></span><br /> <code> ion-android-compass </code></li>
                            <li> <span class="fa ion-android-map gly-2" aria-hidden="true"></span><br /> <code> ion-android-map </code></li>
                            <li> <span class="fa ion-android-walk gly-2" aria-hidden="true"></span><br /> <code> ion-android-walk </code></li>
                            <li> <span class="fa ion-android-bicycle gly-2" aria-hidden="true"></span><br /> <code> ion-android-bicycle </code></li>
                            <li> <span class="fa ion-android-car gly-2" aria-hidden="true"></span><br /> <code> ion-android-car </code></li>
                            <li> <span class="fa ion-android-bus gly-2" aria-hidden="true"></span><br /> <code> ion-android-bus </code></li>
                            <li> <span class="fa ion-android-subway gly-2" aria-hidden="true"></span><br /> <code> ion-android-subway </code></li>
                            <li> <span class="fa ion-android-train gly-2" aria-hidden="true"></span><br /> <code> ion-android-train </code></li>
                            <li> <span class="fa ion-android-boat gly-2" aria-hidden="true"></span><br /> <code> ion-android-boat </code></li>
                            <li> <span class="fa ion-android-plane gly-2" aria-hidden="true"></span><br /> <code> ion-android-plane </code></li>
                            <li> <span class="fa ion-android-restaurant gly-2" aria-hidden="true"></span><br /> <code> ion-android-restaurant </code></li>
                            <li> <span class="fa ion-android-bar gly-2" aria-hidden="true"></span><br /> <code> ion-android-bar </code></li>
                            <li> <span class="fa ion-android-cart gly-2" aria-hidden="true"></span><br /> <code> ion-android-cart </code></li>
                            <li> <span class="fa ion-android-camera gly-2" aria-hidden="true"></span><br /> <code> ion-android-camera </code></li>
                            <li> <span class="fa ion-android-image gly-2" aria-hidden="true"></span><br /> <code> ion-android-image </code></li>
                            <li> <span class="fa ion-android-film gly-2" aria-hidden="true"></span><br /> <code> ion-android-film </code></li>
                            <li> <span class="fa ion-android-color-palette gly-2" aria-hidden="true"></span><br /> <code> ion-android-color-palette </code></li>
                            <li> <span class="fa ion-android-create gly-2" aria-hidden="true"></span><br /> <code> ion-android-create </code></li>
                            <li> <span class="fa ion-android-mail gly-2" aria-hidden="true"></span><br /> <code> ion-android-mail </code></li>
                            <li> <span class="fa ion-android-drafts gly-2" aria-hidden="true"></span><br /> <code> ion-android-drafts </code></li>
                            <li> <span class="fa ion-android-send gly-2" aria-hidden="true"></span><br /> <code> ion-android-send </code></li>
                            <li> <span class="fa ion-android-archive gly-2" aria-hidden="true"></span><br /> <code> ion-android-archive </code></li>
                            <li> <span class="fa ion-android-delete gly-2" aria-hidden="true"></span><br /> <code> ion-android-delete </code></li>
                            <li> <span class="fa ion-android-attach gly-2" aria-hidden="true"></span><br /> <code> ion-android-attach </code></li>
                            <li> <span class="fa ion-android-share gly-2" aria-hidden="true"></span><br /> <code> ion-android-share </code></li>
                            <li> <span class="fa ion-android-share-alt gly-2" aria-hidden="true"></span><br /> <code> ion-android-share-alt </code></li>
                            <li> <span class="fa ion-android-bookmark gly-2" aria-hidden="true"></span><br /> <code> ion-android-bookmark </code></li>
                            <li> <span class="fa ion-android-document gly-2" aria-hidden="true"></span><br /> <code> ion-android-document </code></li>
                            <li> <span class="fa ion-android-clipboard gly-2" aria-hidden="true"></span><br /> <code> ion-android-clipboard </code></li>
                            <li> <span class="fa ion-android-list gly-2" aria-hidden="true"></span><br /> <code> ion-android-list </code></li>
                            <li> <span class="fa ion-android-folder-open gly-2" aria-hidden="true"></span><br /> <code> ion-android-folder-open </code></li>
                            <li> <span class="fa ion-android-folder gly-2" aria-hidden="true"></span><br /> <code> ion-android-folder </code></li>
                            <li> <span class="fa ion-android-print gly-2" aria-hidden="true"></span><br /> <code> ion-android-print </code></li>
                            <li> <span class="fa ion-android-open gly-2" aria-hidden="true"></span><br /> <code> ion-android-open </code></li>
                            <li> <span class="fa ion-android-exit gly-2" aria-hidden="true"></span><br /> <code> ion-android-exit </code></li>
                            <li> <span class="fa ion-android-contract gly-2" aria-hidden="true"></span><br /> <code> ion-android-contract </code></li>
                            <li> <span class="fa ion-android-expand gly-2" aria-hidden="true"></span><br /> <code> ion-android-expand </code></li>
                            <li> <span class="fa ion-android-globe gly-2" aria-hidden="true"></span><br /> <code> ion-android-globe </code></li>
                            <li> <span class="fa ion-android-chat gly-2" aria-hidden="true"></span><br /> <code> ion-android-chat </code></li>
                            <li> <span class="fa ion-android-textsms gly-2" aria-hidden="true"></span><br /> <code> ion-android-textsms </code></li>
                            <li> <span class="fa ion-android-hangout gly-2" aria-hidden="true"></span><br /> <code> ion-android-hangout </code></li>
                            <li> <span class="fa ion-android-happy gly-2" aria-hidden="true"></span><br /> <code> ion-android-happy </code></li>
                            <li> <span class="fa ion-android-sad gly-2" aria-hidden="true"></span><br /> <code> ion-android-sad </code></li>
                            <li> <span class="fa ion-android-person gly-2" aria-hidden="true"></span><br /> <code> ion-android-person </code></li>
                            <li> <span class="fa ion-android-people gly-2" aria-hidden="true"></span><br /> <code> ion-android-people </code></li>
                            <li> <span class="fa ion-android-person-add gly-2" aria-hidden="true"></span><br /> <code> ion-android-person-add </code></li>
                            <li> <span class="fa ion-android-contact gly-2" aria-hidden="true"></span><br /> <code> ion-android-contact </code></li>
                            <li> <span class="fa ion-android-contacts gly-2" aria-hidden="true"></span><br /> <code> ion-android-contacts </code></li>
                            <li> <span class="fa ion-android-playstore gly-2" aria-hidden="true"></span><br /> <code> ion-android-playstore </code></li>
                            <li> <span class="fa ion-android-lock gly-2" aria-hidden="true"></span><br /> <code> ion-android-lock </code></li>
                            <li> <span class="fa ion-android-unlock gly-2" aria-hidden="true"></span><br /> <code> ion-android-unlock </code></li>
                            <li> <span class="fa ion-android-microphone gly-2" aria-hidden="true"></span><br /> <code> ion-android-microphone </code></li>
                            <li> <span class="fa ion-android-microphone-off gly-2" aria-hidden="true"></span><br /> <code> ion-android-microphone-off </code></li>
                            <li> <span class="fa ion-android-notifications-none gly-2" aria-hidden="true"></span><br /> <code> ion-android-notifications-none </code></li>
                            <li> <span class="fa ion-android-notifications gly-2" aria-hidden="true"></span><br /> <code> ion-android-notifications </code></li>
                            <li> <span class="fa ion-android-notifications-off gly-2" aria-hidden="true"></span><br /> <code> ion-android-notifications-off </code></li>
                            <li> <span class="fa ion-android-volume-mute gly-2" aria-hidden="true"></span><br /> <code> ion-android-volume-mute </code></li>
                            <li> <span class="fa ion-android-volume-down gly-2" aria-hidden="true"></span><br /> <code> ion-android-volume-down </code></li>
                            <li> <span class="fa ion-android-volume-up gly-2" aria-hidden="true"></span><br /> <code> ion-android-volume-up </code></li>
                            <li> <span class="fa ion-android-volume-off gly-2" aria-hidden="true"></span><br /> <code> ion-android-volume-off </code></li>
                            <li> <span class="fa ion-android-hand gly-2" aria-hidden="true"></span><br /> <code> ion-android-hand </code></li>
                            <li> <span class="fa ion-android-desktop gly-2" aria-hidden="true"></span><br /> <code> ion-android-desktop </code></li>
                            <li> <span class="fa ion-android-laptop gly-2" aria-hidden="true"></span><br /> <code> ion-android-laptop </code></li>
                            <li> <span class="fa ion-android-phone-portrait gly-2" aria-hidden="true"></span><br /> <code> ion-android-phone-portrait </code></li>
                            <li> <span class="fa ion-android-phone-landscape gly-2" aria-hidden="true"></span><br /> <code> ion-android-phone-landscape </code></li>
                            <li> <span class="fa ion-android-bulb gly-2" aria-hidden="true"></span><br /> <code> ion-android-bulb </code></li>
                            <li> <span class="fa ion-android-sunny gly-2" aria-hidden="true"></span><br /> <code> ion-android-sunny </code></li>
                            <li> <span class="fa ion-android-alert gly-2" aria-hidden="true"></span><br /> <code> ion-android-alert </code></li>
                            <li> <span class="fa ion-android-warning gly-2" aria-hidden="true"></span><br /> <code> ion-android-warning </code></li>
                            <li> <span class="fa ion-social-twitter gly-2" aria-hidden="true"></span><br /> <code> ion-social-twitter </code></li>
                            <li> <span class="fa ion-social-twitter-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-twitter-outline </code></li>
                            <li> <span class="fa ion-social-facebook gly-2" aria-hidden="true"></span><br /> <code> ion-social-facebook </code></li>
                            <li> <span class="fa ion-social-facebook-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-facebook-outline </code></li>
                            <li> <span class="fa ion-social-googleplus gly-2" aria-hidden="true"></span><br /> <code> ion-social-googleplus </code></li>
                            <li> <span class="fa ion-social-googleplus-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-googleplus-outline </code></li>
                            <li> <span class="fa ion-social-google gly-2" aria-hidden="true"></span><br /> <code> ion-social-google </code></li>
                            <li> <span class="fa ion-social-google-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-google-outline </code></li>
                            <li> <span class="fa ion-social-dribbble gly-2" aria-hidden="true"></span><br /> <code> ion-social-dribbble </code></li>
                            <li> <span class="fa ion-social-dribbble-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-dribbble-outline </code></li>
                            <li> <span class="fa ion-social-octocat gly-2" aria-hidden="true"></span><br /> <code> ion-social-octocat </code></li>
                            <li> <span class="fa ion-social-github gly-2" aria-hidden="true"></span><br /> <code> ion-social-github </code></li>
                            <li> <span class="fa ion-social-github-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-github-outline </code></li>
                            <li> <span class="fa ion-social-instagram gly-2" aria-hidden="true"></span><br /> <code> ion-social-instagram </code></li>
                            <li> <span class="fa ion-social-instagram-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-instagram-outline </code></li>
                            <li> <span class="fa ion-social-whatsapp gly-2" aria-hidden="true"></span><br /> <code> ion-social-whatsapp </code></li>
                            <li> <span class="fa ion-social-whatsapp-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-whatsapp-outline </code></li>
                            <li> <span class="fa ion-social-snapchat gly-2" aria-hidden="true"></span><br /> <code> ion-social-snapchat </code></li>
                            <li> <span class="fa ion-social-snapchat-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-snapchat-outline </code></li>
                            <li> <span class="fa ion-social-foursquare gly-2" aria-hidden="true"></span><br /> <code> ion-social-foursquare </code></li>
                            <li> <span class="fa ion-social-foursquare-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-foursquare-outline </code></li>
                            <li> <span class="fa ion-social-pinterest gly-2" aria-hidden="true"></span><br /> <code> ion-social-pinterest </code></li>
                            <li> <span class="fa ion-social-pinterest-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-pinterest-outline </code></li>
                            <li> <span class="fa ion-social-rss gly-2" aria-hidden="true"></span><br /> <code> ion-social-rss </code></li>
                            <li> <span class="fa ion-social-rss-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-rss-outline </code></li>
                            <li> <span class="fa ion-social-tumblr gly-2" aria-hidden="true"></span><br /> <code> ion-social-tumblr </code></li>
                            <li> <span class="fa ion-social-tumblr-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-tumblr-outline </code></li>
                            <li> <span class="fa ion-social-wordpress gly-2" aria-hidden="true"></span><br /> <code> ion-social-wordpress </code></li>
                            <li> <span class="fa ion-social-wordpress-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-wordpress-outline </code></li>
                            <li> <span class="fa ion-social-reddit gly-2" aria-hidden="true"></span><br /> <code> ion-social-reddit </code></li>
                            <li> <span class="fa ion-social-reddit-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-reddit-outline </code></li>
                            <li> <span class="fa ion-social-hackernews gly-2" aria-hidden="true"></span><br /> <code> ion-social-hackernews </code></li>
                            <li> <span class="fa ion-social-hackernews-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-hackernews-outline </code></li>
                            <li> <span class="fa ion-social-designernews gly-2" aria-hidden="true"></span><br /> <code> ion-social-designernews </code></li>
                            <li> <span class="fa ion-social-designernews-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-designernews-outline </code></li>
                            <li> <span class="fa ion-social-yahoo gly-2" aria-hidden="true"></span><br /> <code> ion-social-yahoo </code></li>
                            <li> <span class="fa ion-social-yahoo-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-yahoo-outline </code></li>
                            <li> <span class="fa ion-social-buffer gly-2" aria-hidden="true"></span><br /> <code> ion-social-buffer </code></li>
                            <li> <span class="fa ion-social-buffer-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-buffer-outline </code></li>
                            <li> <span class="fa ion-social-skype gly-2" aria-hidden="true"></span><br /> <code> ion-social-skype </code></li>
                            <li> <span class="fa ion-social-skype-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-skype-outline </code></li>
                            <li> <span class="fa ion-social-linkedin gly-2" aria-hidden="true"></span><br /> <code> ion-social-linkedin </code></li>
                            <li> <span class="fa ion-social-linkedin-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-linkedin-outline </code></li>
                            <li> <span class="fa ion-social-vimeo gly-2" aria-hidden="true"></span><br /> <code> ion-social-vimeo </code></li>
                            <li> <span class="fa ion-social-vimeo-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-vimeo-outline </code></li>
                            <li> <span class="fa ion-social-twitch gly-2" aria-hidden="true"></span><br /> <code> ion-social-twitch </code></li>
                            <li> <span class="fa ion-social-twitch-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-twitch-outline </code></li>
                            <li> <span class="fa ion-social-youtube gly-2" aria-hidden="true"></span><br /> <code> ion-social-youtube </code></li>
                            <li> <span class="fa ion-social-youtube-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-youtube-outline </code></li>
                            <li> <span class="fa ion-social-dropbox gly-2" aria-hidden="true"></span><br /> <code> ion-social-dropbox </code></li>
                            <li> <span class="fa ion-social-dropbox-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-dropbox-outline </code></li>
                            <li> <span class="fa ion-social-apple gly-2" aria-hidden="true"></span><br /> <code> ion-social-apple </code></li>
                            <li> <span class="fa ion-social-apple-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-apple-outline </code></li>
                            <li> <span class="fa ion-social-android gly-2" aria-hidden="true"></span><br /> <code> ion-social-android </code></li>
                            <li> <span class="fa ion-social-android-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-android-outline </code></li>
                            <li> <span class="fa ion-social-windows gly-2" aria-hidden="true"></span><br /> <code> ion-social-windows </code></li>
                            <li> <span class="fa ion-social-windows-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-windows-outline </code></li>
                            <li> <span class="fa ion-social-html5 gly-2" aria-hidden="true"></span><br /> <code> ion-social-html5 </code></li>
                            <li> <span class="fa ion-social-html5-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-html5-outline </code></li>
                            <li> <span class="fa ion-social-css3 gly-2" aria-hidden="true"></span><br /> <code> ion-social-css3 </code></li>
                            <li> <span class="fa ion-social-css3-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-css3-outline </code></li>
                            <li> <span class="fa ion-social-javascript gly-2" aria-hidden="true"></span><br /> <code> ion-social-javascript </code></li>
                            <li> <span class="fa ion-social-javascript-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-javascript-outline </code></li>
                            <li> <span class="fa ion-social-angular gly-2" aria-hidden="true"></span><br /> <code> ion-social-angular </code></li>
                            <li> <span class="fa ion-social-angular-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-angular-outline </code></li>
                            <li> <span class="fa ion-social-nodejs gly-2" aria-hidden="true"></span><br /> <code> ion-social-nodejs </code></li>
                            <li> <span class="fa ion-social-sass gly-2" aria-hidden="true"></span><br /> <code> ion-social-sass </code></li>
                            <li> <span class="fa ion-social-python gly-2" aria-hidden="true"></span><br /> <code> ion-social-python </code></li>
                            <li> <span class="fa ion-social-chrome gly-2" aria-hidden="true"></span><br /> <code> ion-social-chrome </code></li>
                            <li> <span class="fa ion-social-chrome-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-chrome-outline </code></li>
                            <li> <span class="fa ion-social-codepen gly-2" aria-hidden="true"></span><br /> <code> ion-social-codepen </code></li>
                            <li> <span class="fa ion-social-codepen-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-codepen-outline </code></li>
                            <li> <span class="fa ion-social-markdown gly-2" aria-hidden="true"></span><br /> <code> ion-social-markdown </code></li>
                            <li> <span class="fa ion-social-tux gly-2" aria-hidden="true"></span><br /> <code> ion-social-tux </code></li>
                            <li> <span class="fa ion-social-freebsd-devil gly-2" aria-hidden="true"></span><br /> <code> ion-social-freebsd-devil </code></li>
                            <li> <span class="fa ion-social-usd gly-2" aria-hidden="true"></span><br /> <code> ion-social-usd </code></li>
                            <li> <span class="fa ion-social-usd-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-usd-outline </code></li>
                            <li> <span class="fa ion-social-bitcoin gly-2" aria-hidden="true"></span><br /> <code> ion-social-bitcoin </code></li>
                            <li> <span class="fa ion-social-bitcoin-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-bitcoin-outline </code></li>
                            <li> <span class="fa ion-social-yen gly-2" aria-hidden="true"></span><br /> <code> ion-social-yen </code></li>
                            <li> <span class="fa ion-social-yen-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-yen-outline </code></li>
                            <li> <span class="fa ion-social-euro gly-2" aria-hidden="true"></span><br /> <code> ion-social-euro </code></li>
                            <li> <span class="fa ion-social-euro-outline gly-2" aria-hidden="true"></span><br /> <code> ion-social-euro-outline </code></li>
                        <ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')

@endsection