@extends('app')

@section('page-title')
    Business Dashboard
@endsection

@section('page-css')
<style>
    .mh400{min-height: 400px;}
    .mh300{min-height: 300px;}
    .sravatar{
        border-radius: 50%;
        height: 50px;
        width: 50px;
        margin: 10px 0 0 20px;
        border: 2px solid #008d4c;
    }

</style>
@endsection

@section('content-header')
    <h4>
        Business Dashboard
    </h4>
@endsection

@section('content')

    <div class="row">
        <section class="col-xl-6 col-lg-12 connectedSortable">
            <div class="card mh400" id="divSales">
                <div class="card-header">
                    <i class="fa fa-money"></i>
                    <h3 class="card-title">Sales Graph</h3>
                    <div class="card-tools pull-right">
                        <button type="button" class="btn btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="card-block border-radius-none">
                    <canvas id="barChart" style="height: 280px;"></canvas>
                </div>
            </div>
        </section>
        <section class="col-xl-6 col-lg-12 connectedSortable">
            <div class="card mh400">
                <div class="card-header">
                    <i class="fa fa-line-chart"></i>

                    <h3 class="card-title">Return and debt on equity</h3>

                    <div class="card-tools pull-right">
                        <div class="btn-group" data-toggle="btn-toggle">
                            <button type="button" class="btn btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-block">
                    <div id="equity" style="height: 320px;"></div>
                </div>
            </div>
        </section>
        <section class="col-xl-6 col-lg-12 connectedSortable">
            <div class="card mh300">
                <div class="card-header">
                    <i class="fa fa-line-chart"></i>

                    <h3 class="card-title">Product sales goals</h3>

                    <div class="card-tools pull-right">
                        <div class="btn-group" data-toggle="btn-toggle">
                            <button type="button" class="btn btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-3 col-xs-6 text-xs-center">
                            <div>
                                <canvas id="facebookDs"></canvas>
                            </div>
                            <div class="knob-label">Product 1</div>
                        </div>
                        <div class="col-md-3 col-xs-6 text-xs-center">
                            <div>
                                <canvas id="googleDs"></canvas>
                            </div>
                            <div class="knob-label">Product 2</div>
                        </div>
                        <div class="col-md-3 col-xs-6 text-xs-center">
                            <div>
                                <canvas id="twitterDs"></canvas>
                            </div>
                            <div class="knob-label">Product 3</div>
                        </div>
                        <div class="col-md-3 col-xs-6 text-xs-center">
                            <div>
                                <canvas id="linkedinDs"></canvas>
                            </div>
                            <div class="knob-label">Product 4</div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-xs-2">
                            <img class="sravatar" alt="user image" src="{{asset('img/avatars/user4.jpg')}}">
                            <div class="text-xs-center">Mark Walberg</div>
                        </div>
                        <div class="col-xs-10">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="col-xl-6 col-lg-12 connectedSortable">
            <div class="card mh300">
                <div class="card-header">
                    <i class="fa fa-bar-chart"></i>

                    <h3 class="card-title">Return on equity (ROE)</h3>

                    <div class="card-tools pull-right">
                        <div class="btn-group" data-toggle="btn-toggle">
                            <button type="button" class="btn btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-block">
                    <div id="roe"></div>
                </div>
            </div>
        </section>
        <!--./roe-->


    </div>
    <div class="row">

        <section class="col-xl-4 col-lg-12 connectedSortable">
            <div class="card card-graphic">
                <div class="card-block">
                    <div class="inner">
                        <h4 class="mt5 mbn pull-left">Income</h4>
                        <h4 class="text-system pull-right pr10">$68,653</h4>
                    </div>
                    <div class="text-xs-center graphic">
                        <canvas id="lineChart1" height="100" width="800"></canvas>
                    </div>

                </div>
                <div class="card-footer text-xs-center br-t">
                    <span>
                        <i class="fa fa-arrow-up" style="color: palegreen;"></i>
                        25% INCREASE
                        <b>vs $54,923</b>
                    </span>
                </div>
            </div>
        </section>
        <section class="col-xl-4  col-lg-12 connectedSortable">
            <div class="card card-graphic">
                <div class="card-block">
                    <div class="inner">
                        <h4 class="mt5 mbn pull-left">Expenses</h4>
                        <h4 class="text-system pull-right pr10">$18,095</h4>
                    </div>
                    <div class="text-xs-center graphic">
                        <canvas id="lineChart2" height="100" width="800"></canvas>
                    </div>

                </div>
                <div class="card-footer text-xs-center br-t">
                    <span>
                    <i class="fa fa-arrow-up" style="color: palevioletred;"></i>
                    10% INCREASE
                    <b>vs 16,450</b>
                    </span>
                </div>
            </div>
        </section>
        <section class="col-xl-4 col-lg-12 connectedSortable">
            <div class="card card-graphic">
                <div class="card-block">
                    <div class="inner">
                        <h4 class="mt5 mbn pull-left">Proffit</h4>
                        <h4 class="text-system pull-right pr10">$50,558</h4>
                    </div>
                    <div class="text-xs-center graphic">
                        <canvas id="lineChart3" height="100" width="800"></canvas>
                    </div>
                </div>
                <div class="card-footer text-xs-center br-t">
                    <span>
                        <i class="fa fa-arrow-up" style="color: palegreen;"></i>
                        31% INCREASE
                        <b>vs $38,473</b>
                    </span>
                </div>
            </div>
        </section>

    </div>


@endsection

@section('page-scripts')
    <script src="{{asset('vendor/moment/js/moment.min.js')}}"></script>
    <script src="{{asset('/vendor/chart-js/Chart.min.js')}}"></script>
    <script src="{{asset('https://www.gstatic.com/charts/loader.js')}}"></script>

    <script>
        $(function () {

            "use strict";

            //Make the dashboard widgets sortable Using jquery UI
            $(".connectedSortable").sortable({
                placeholder: "sort-highlight",
                connectWith: ".connectedSortable",
                handle: ".card-header, .nav-tabs",
                forcePlaceholderSize: true,
                zIndex: 999999
            });
            $(".connectedSortable .card-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");

            google.charts.load('current', {packages: ['corechart', 'bar']});
            google.charts.setOnLoadCallback(googleCharstDraw);
            drawCharts();

            //event listener to div resizing
            var chartDiv = document.getElementById("divSales");
            //css-element-queries library
            new ResizeSensor(chartDiv, function() {
                drawCharts();
                googleCharstDraw();
            });

            function drawCharts(){
                var data = {
                    labels: ["01", "02", "03", "04", "05", "06", "07"],
                    datasets: [
                        {

                            fillColor: "rgba(209,231,255,1)",
                            strokeColor: "rgba(97,154,234,1)",
                            pointColor: "rgba(97,154,234,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: [65, 59, 120, 81, 15, 55, 40]
                        }
                    ]
                };

                var lineChart = document.getElementById("lineChart1").getContext("2d");
                new Chart(lineChart).Line(data, {
                    bezierCurve: false,
                    showScale: false
                });

                var data = {
                    labels: ["01", "02", "03", "04", "05", "06", "07"],
                    datasets: [
                        {

                            fillColor: "rgba(209,231,255,1)",
                            strokeColor: "rgba(97,154,234,1)",
                            pointColor: "rgba(97,154,234,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: [45, 90, 120, 15, 100, 55, 40]
                        }
                    ]
                };

                var lineChart = document.getElementById("lineChart2").getContext("2d");
                new Chart(lineChart).Line(data, {
                    bezierCurve: false,
                    showScale: false
                });

                var data = {
                    labels: ["01", "02", "03", "04", "05", "06", "07"],
                    datasets: [
                        {

                            fillColor: "rgba(209,231,255,1)",
                            strokeColor: "rgba(97,154,234,1)",
                            pointColor: "rgba(97,154,234,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: [32, 67, 10, 20, 90, 110, 20]
                        }
                    ]
                };

                var lineChart = document.getElementById("lineChart3").getContext("2d");
                new Chart(lineChart).Line(data, {
                    bezierCurve: false,
                    showScale: false
                });


                var barChartData = {
                    labels : ["January","February","March","April"],
                    datasets : [
                        {
                            label: "Product 1",
                            fillColor: "rgba(59,86,152,0.5)",
                            strokeColor: "rgba(59,86,152,0.8)",
                            highlightFill: "rgba(59,86,152,0.75)",
                            highlightStroke: "rgba(59,86,152,1)",
                            data: [65, 59, 80, 81]
                        },
                        {
                            label: "Product 2",
                            fillColor: "rgba(221,75,57,0.5)",
                            strokeColor: "rgba(221,75,57,0.8)",
                            highlightFill: "rgba(221,75,57,0.75)",
                            highlightStroke: "rgba(221,75,57,1)",
                            data: [28, 48, 40, 19]
                        },
                        {
                            label: "Product 3",
                            fillColor: "rgba(85,172,238,0.5)",
                            strokeColor: "rgba(85,172,238,0.8)",
                            highlightFill: "rgba(85,172,238,0.75)",
                            highlightStroke: "rgba(85,172,238,1)",
                            data: [52, 22, 67, 78]
                        },
                        {
                            label: "Product 4",
                            fillColor: "rgba(0,123,182,0.5)",
                            strokeColor: "rgba(0,123,182,0.8)",
                            highlightFill: "rgba(0,123,182,0.75)",
                            highlightStroke: "rgba(0,123,182,1)",
                            data: [32, 43, 65, 12]
                        }
                    ]

                };


                var barChart = document.getElementById("barChart").getContext("2d");
                window.myBar = new Chart(barChart).Bar(barChartData, {
                    responsive : true,
                    maintainAspectRatio: false,
                    //Number - Spacing between each of the X value sets
                    barValueSpacing : 2,
                    scaleShowHorizontalLines: false,
                    scaleShowVerticalLines: false,
                    multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"

                });

                //week goals
                var dougnutData = [
                    {
                        value: 60,
                        color:"#3B5998",
                        highlight: "#324C81",
                        label: ""
                    },
                    {
                        value: 40,
                        color:"#eee",
                        highlight: "#f5f5f5",
                        label: ""
                    },
                ];

                var doughnutChart = document.getElementById("facebookDs").getContext("2d");
                window.myDoughnut = new Chart(doughnutChart).Doughnut(dougnutData, {responsive : true});

                //week goals
                var dougnutData = [
                    {
                        value: 80,
                        color:"#DD4B39",
                        highlight: "#BC4031",
                        label: ""
                    },
                    {
                        value: 20,
                        color:"#eee",
                        highlight: "#f5f5f5",
                        label: ""
                    },
                ];

                var doughnutChart = document.getElementById("googleDs").getContext("2d");
                window.myDoughnut = new Chart(doughnutChart).Doughnut(dougnutData, {responsive : true});

                //week goals
                var dougnutData = [
                    {
                        value: 95,
                        color:"#55ACEE",
                        highlight: "#4892CB",
                        label: ""
                    },
                    {
                        value: 5,
                        color:"#eee",
                        highlight: "#f5f5f5",
                        label: ""
                    },
                ];

                var doughnutChart = document.getElementById("twitterDs").getContext("2d");
                window.myDoughnut = new Chart(doughnutChart).Doughnut(dougnutData, {responsive : true});

                //week goals
                var dougnutData = [
                    {
                        value: 20,
                        color:"#007BB6",
                        highlight: "#00699B",
                        label: ""
                    },
                    {
                        value: 80,
                        color:"#eee",
                        highlight: "#f5f5f5",
                        label: ""
                    },
                ];

                var doughnutChart = document.getElementById("linkedinDs").getContext("2d");
                window.myDoughnut = new Chart(doughnutChart).Doughnut(dougnutData, {responsive : true});

                //Make the dashboard widgets sortable Using jquery UI
                $(".connectedSortable").sortable({
                    placeholder: "sort-highlight",
                    connectWith: ".connectedSortable",
                    handle: ".card-header, .nav-tabs",
                    forcePlaceholderSize: true,
                    zIndex: 999999
                });


            }

            function googleCharstDraw(){
                var data = google.visualization.arrayToDataTable([
                    ['Year', 'Return', 'Debt'],
                    ['2010', 54260, 23522],
                    ['2012', 61254, 32544],
                    ['2013', 85221, 42588],
                    ['2014', 75422, 42566],
                    ['2015', 85322, 35877]
                ]);

                var options = {
                    isStacked: false,
                    hAxis: {
                        title: 'Year',
                        minValue: 0
                    },
                    vAxis: {format: 'decimal'},
                    bars: 'horizontal',
                    colors: ['#267BBF', '#DF6D69'],
                };
                var chart = new google.charts.Bar(document.getElementById('equity'));
                chart.draw(data, google.charts.Bar.convertOptions(options));

                var data = google.visualization.arrayToDataTable([
                    ['Year', 'ROE'],
                    ['2010', -10],
                    ['2012', 5],
                    ['2013', 10],
                    ['2014', 9],
                    ['2015', 14]
                ]);

                var options = {
                    hAxis: {
                        title: 'Year',
                        minValue: 0,
                    },
                    vAxis: {
                        title: '$'
                    },
                    bars: 'vertical',
                    colors: ['#267BBF']
                };
                var material = new google.charts.Bar(document.getElementById('roe'));
                material.draw(data, options);
            }

        });
    </script>
@endsection