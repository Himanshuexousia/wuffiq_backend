<!DOCTYPE html>
<html>
<head>
    @include('section.head')
    @yield('page-css')
</head>

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Backgrounds   | bg-black-laser, bg-black-velvet, bg-blue-techno, bg-dark-wood, bg-rainbow, bg-digital-rainbow, bg-digital-sun, bg-fractal-ocean
|               | bg-fractal-rainbow, bg-gray-lights, bg-pink, bg-pink-and-blue, bg-sunset, bg-gradientdark-blue, bg-gradient-pink, bg-gradient-green
|               | bg-gradient-blue, bg-glow-green, bg-glow-pink, bg-glow-blue, bg-sepia-green, bg-sepia-blue, bg-sepia-yellow, bg-green-yellow, bg-white-fractal
|               | bg-white-fractal2, bg-white-laser, bg-solid-white, bg-solid-black, bg-solid-dark, bg-solid-gray
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Layouts       | fixed, boxed, rounded, top-nav
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Sidebar       | mini-sidebar and or sidebar-collapse
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Headers       | header-black, header-blue, header-glass, header-green, header-pink, header-purple, header-red, header-transparent, header-white, header-yellow
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Sidebars      | sidebar-black, sidebar-blue, sidebar-glass, sidebar-green, sidebar-pink, sidebar-purple, sidebar-red, sidebar-transparent, sidebar-white, sidebar-yellow
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Content       | content-dark, content-light, content-glass | for now just content-light is implemented, wait for the next releases
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

ex: <body class="bg-black-laser fixed mini-sidebar header-black sidebar-black content-clear">
-->
<body class="hold-transition mini-sidebar  header-black bg-rainbow sidebar-black content-light {{--@yield('checkui::skin', Config::get('checkui.skin')) @yield('checkui::layout', Config::get('checkui.layout'))--}}">

<div class="wrapper">
    @include('section.sample-header'){{--change to section.header to remove sample menu--}}
    @include('section.sample-sidebar'){{--change to section.sidebar to remove sample menu--}}

    <!-- Content Wrapper -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @yield('content-header')
            @include('section.notifications')
        </section>

        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('section.controls')
    @include('section.footer')

</div><!-- ./wrapper -->

@include('section.scripts')
@yield('page-scripts')

</body>
</html>