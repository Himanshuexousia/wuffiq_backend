@extends('app')

@section('page-title')
    Mailbox - Inbox
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{asset('vendor/animate.css/animate.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/icheck/skins/flat/blue.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/bootstrap3-wysihtml5/bootstrap3-wysihtml5.min.css')}}" />
    <link rel="stylesheet" href="{{asset('vendor/select2/css/select2.min.css')}}">
    <style>
        .content-light .card .card-header .btn {
            background-color: inherit;
            border-color: inherit;
            color: inherit;
        }
    </style>
@endsection

@section('content-header')
    <h1>
        Mailbox - Inbox
    </h1>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-block">
                    <h4 class="lead text-md-center"> Menu </h4>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <a href="#">
                                <i class="fa fa-gear"></i>
                                Email Settings
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{route('mail-inbox')}}">
                                <i class="fa fa-envelope"></i>
                                Inbox
                                <span class="label badge-warning">2</span>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <i class="fa fa-user"></i>
                                Friends
                                <span class="label badge-warning">6</span>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <i class="fa fa-trash-o"></i>
                                Spam
                                <span class="label badge-muted">13</span>
                            </a>
                        </li>
                    </ul>
                    <br />
                    <br />

                    <h4 class="lead text-md-center"> Labels </h4>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <a href="#">
                                Danger
                                <span class="pull-md-right label label-pill label-danger">7</span>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                Success
                                <span class="pull-md-right label label-pill label-success">9</span>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                Primary
                                <span class="pull-md-right label label-pill label-primary">13</span>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                Warning
                                <span class="pull-md-right label label-pill label-warning">15</span>
                            </a>
                        </li>
                    </ul>
                    <br />
                    <br />

                    <button type="button" class="btn btn-block btn-secondary" data-toggle="modal" data-target="#modalCompose">Quick Compose</button>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="hidden-xs hidden-sm col-md-3">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default ">
                                    <i class="fa fa-refresh"></i>
                                </button>
                                <button type="button" class="btn btn-default ">
                                    <i class="fa fa-pencil"></i>
                                </button>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-9 text-md-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default hidden-xs">
                                    <i class="fa fa-star"></i>
                                </button>
                                <button type="button" class="btn btn-default hidden-xs">
                                    <i class="fa fa-calendar"></i>
                                </button>
                                <button type="button" class="btn btn-default">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                            <div class="btn-group">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default  dropdown-toggle" data-toggle="dropdown">
                                        <span class="fa fa-tags"></span>
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="#">Work</a>
                                        </li>
                                        <li>
                                            <a href="#">Important</a>
                                        </li>
                                        <li>
                                            <a href="#">Favorites</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-plus"></span> Create New</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default  dropdown-toggle br-tp-left" data-toggle="dropdown">
                                        <span class="fa fa-folder"></span>
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="#">Work</a>
                                        </li>
                                        <li>
                                            <a href="#">Important</a>
                                        </li>
                                        <li>
                                            <a href="#">Favorites</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-plus"></span> Create New</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default ">
                                    <i class="fa fa-chevron-left"></i>
                                </button>
                                <button type="button" class="btn btn-default ">
                                    <i class="fa fa-chevron-right"></i>
                                </button>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-block">
                    <table class="table table-hover">
                        <thead class="thead-inverse">
                            <tr>
                                <th><input type="checkbox" id="checkAll" /></th>
                                <th><i class="fa fa-star text-yellow" data-toggle="tooltip" data-placement="top" title="Important"></i></th>
                                <th>Contact</th>
                                <th>Subject</th>
                                <th><i class="fa fa-paperclip" data-toggle="tooltip" data-placement="top" title="Attachment"></i></th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><input type="checkbox" class="check" /></td>
                            <td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a></td>
                            <td class="mailbox-name"><a href="{{route('contacts-list')}}">Checkmate Digital</a></td>
                            <td class="mailbox-subject"><a href="{{route('mail-message')}}">Mail Subject</a></td>
                            <td class="mailbox-attachment"></td>
                            <td class="mailbox-date">5 mins ago</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" class="check" /></td>
                            <td class="mailbox-star"><a href="#"><i class="fa fa-star-o text-yellow"></i></a></td>
                            <td class="mailbox-name"><a href="{{route('contacts-list')}}">Checkmate Digital</a></td>
                            <td class="mailbox-subject"><a href="{{route('mail-message')}}">Mail Subject</a></td>
                            <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                            <td class="mailbox-date">28 mins ago</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" class="check" /></td>
                            <td class="mailbox-star"><a href="#"><i class="fa fa-star-o text-yellow"></i></a></td>
                            <td class="mailbox-name"><a href="{{route('contacts-list')}}">Checkmate Digital</a></td>
                            <td class="mailbox-subject"><a href="{{route('mail-message')}}">Mail Subject</a></td>
                            <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                            <td class="mailbox-date">11 hours ago</td>
                        </tr>
                        <tr class="light-gray">
                            <td><input type="checkbox" class="check" /></td>
                            <td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a></td>
                            <td class="mailbox-name"><a href="{{route('contacts-list')}}">Checkmate Digital</a></td>
                            <td class="mailbox-subject"><a href="{{route('mail-message')}}">Mail Subject</a></td>
                            <td class="mailbox-attachment"></td>
                            <td class="mailbox-date">15 hours ago</td>
                        </tr>
                        <tr class="light-gray">
                            <td><input type="checkbox" class="check" /></td>
                            <td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a></td>
                            <td class="mailbox-name"><a href="{{route('contacts-list')}}">Checkmate Digital</a></td>
                            <td class="mailbox-subject"><a href="{{route('mail-message')}}">Mail Subject</a></td>
                            <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                            <td class="mailbox-date">Yesterday</td>
                        </tr>
                        <tr class="light-gray">
                            <td><input type="checkbox" class="check" /></td>
                            <td class="mailbox-star"><a href="#"><i class="fa fa-star-o text-yellow"></i></a></td>
                            <td class="mailbox-name"><a href="{{route('contacts-list')}}">Checkmate Digital</a></td>
                            <td class="mailbox-subject"><a href="{{route('mail-message')}}">Mail Subject</a></td>
                            <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                            <td class="mailbox-date">2 days ago</td>
                        </tr>
                        <tr class="light-gray">
                            <td><input type="checkbox" class="check" /></td>
                            <td class="mailbox-star"><a href="#"><i class="fa fa-star-o text-yellow"></i></a></td>
                            <td class="mailbox-name"><a href="{{route('contacts-list')}}">Checkmate Digital</a></td>
                            <td class="mailbox-subject"><a href="{{route('mail-message')}}">Mail Subject</a></td>
                            <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                            <td class="mailbox-date">2 days ago</td>
                        </tr>
                        <tr class="light-gray">
                            <td><input type="checkbox" class="check" /></td>
                            <td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a></td>
                            <td class="mailbox-name"><a href="{{route('contacts-list')}}">Checkmate Digital</a></td>
                            <td class="mailbox-subject"><a href="{{route('mail-message')}}">Mail Subject</a></td>
                            <td class="mailbox-attachment"></td>
                            <td class="mailbox-date">2 days ago</td>
                        </tr>
                        <tr class="light-gray">
                            <td><input type="checkbox" class="check" /></td>
                            <td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a></td>
                            <td class="mailbox-name"><a href="{{route('contacts-list')}}">Checkmate Digital</a></td>
                            <td class="mailbox-subject"><a href="{{route('mail-message')}}">Mail Subject</a></td>
                            <td class="mailbox-attachment"></td>
                            <td class="mailbox-date">2 days ago</td>
                        </tr>
                        <tr class="light-gray">
                            <td><input type="checkbox" class="check" /></td>
                            <td class="mailbox-star"><a href="#"><i class="fa fa-star-o text-yellow"></i></a></td>
                            <td class="mailbox-name"><a href="{{route('contacts-list')}}">Checkmate Digital</a></td>
                            <td class="mailbox-subject"><a href="{{route('mail-message')}}">Mail Subject</a></td>
                            <td class="mailbox-attachment"></td>
                            <td class="mailbox-date">2 days ago</td>
                        </tr>
                        <tr class="light-gray">
                            <td><input type="checkbox" class="check" /></td>
                            <td class="mailbox-star"><a href="#"><i class="fa fa-star-o text-yellow"></i></a></td>
                            <td class="mailbox-name"><a href="{{route('contacts-list')}}">Checkmate Digital</a></td>
                            <td class="mailbox-subject"><a href="{{route('mail-message')}}">Mail Subject</a></td>
                            <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                            <td class="mailbox-date">4 days ago</td>
                        </tr>
                        <tr class="light-gray">
                            <td><input type="checkbox" class="check" /></td>
                            <td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a></td>
                            <td class="mailbox-name"><a href="{{route('contacts-list')}}">Checkmate Digital</a></td>
                            <td class="mailbox-subject"><a href="{{route('mail-message')}}">Mail Subject</a></td>
                            <td class="mailbox-attachment"></td>
                            <td class="mailbox-date">12 days ago</td>
                        </tr>
                        <tr class="light-gray">
                            <td><input type="checkbox" class="check" /></td>
                            <td class="mailbox-star"><a href="#"><i class="fa fa-star-o text-yellow"></i></a></td>
                            <td class="mailbox-name"><a href="{{route('contacts-list')}}">Checkmate Digital</a></td>
                            <td class="mailbox-subject"><a href="{{route('mail-message')}}">Mail Subject</a></td>
                            <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                            <td class="mailbox-date">12 days ago</td>
                        </tr>
                        <tr class="light-gray">
                            <td><input type="checkbox" class="check" /></td>
                            <td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a></td>
                            <td class="mailbox-name"><a href="{{route('contacts-list')}}">Checkmate Digital</a></td>
                            <td class="mailbox-subject"><a href="{{route('mail-message')}}">Mail Subject</a></td>
                            <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                            <td class="mailbox-date">14 days ago</td>
                        </tr>
                        <tr class="light-gray">
                            <td><input type="checkbox" class="check" /></td>
                            <td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a></td>
                            <td class="mailbox-name"><a href="{{route('contacts-list')}}">Checkmate Digital</a></td>
                            <td class="mailbox-subject"><a href="{{route('mail-message')}}">Mail Subject</a></td>
                            <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                            <td class="mailbox-date">15 days ago</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalCompose" tabindex="-1" role="dialog" aria-labelledby="modalCompose">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Quick Compose</h4>
                </div>
                <div class="modal-body animate slideUp">
                    <form>
                        <div class="form-group row">
                            <label for="title" class="col-sm-2 form-control-label">Title</label>
                            <div class="col-sm-10">
                                <input type="title" class="form-control" id="title" placeholder="Title">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="to" class="col-sm-2 form-control-label">To</label>
                            <div class="col-sm-10">
                                <select id="to" class="form-control" style="width: 100%">
                                    <option value="Checkmate Digital">Checkmate Digital</option>
                                    <option value="Checkmate Digital2">Checkmate Digital2</option>
                                    <option value="Checkmate Digital3">Checkmate Digital3</option>
                                    <option value="Checkmate Digital4">Checkmate Digital4</option>
                                    <option value="Other Checkmate Digital">Other Checkmate Digital</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="message" class="col-sm-2 form-control-label">Message</label>
                            <div class="col-sm-10">
                                <textarea type="message" class="form-control" id="message" placeholder="Message"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Send</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('page-scripts')
    <script src="{{asset('vendor/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap3-wysihtml5/templates.js')}}"></script>
    <script src="{{asset('vendor/bootstrap3-wysihtml5/commands.js')}}"></script>
    <script src="{{asset('vendor/select2/js/select2.full.min.js')}}"></script>
    <script src="{{asset('vendor/icheck/icheck.js')}}"></script>
    <script type="text/javascript">
        $(function() {

            "use strict";

            $('input[type=checkbox]').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
            $('#message').wysihtml5();
            $("#to").select2();

        });

        $('.mailbox-star a i').on('click', function(e){
            e.preventDefault();
            if($( this ).hasClass('fa-star-o'))
            {
                $( this ).removeClass('fa-star-o');
                $( this ).addClass('fa-star');
            }
            else
            {
                $( this ).removeClass('fa-star');
                $( this ).addClass('fa-star-o');
            }
        });

        $('#checkAll').on('ifChecked', function (event) {
            $('.check').iCheck('check');
        });

        $('.check').on('ifUnchecked', function (event) {
            $('#checkAll').iCheck('uncheck');
        });

        // Make "All" checked if all checkboxes are checked
        $('.check').on('ifChecked', function (event) {
            if ($('.check').filter(':checked').length == $('.check').length) {
                $('#checkAll').iCheck('check');
            }
        });

        /*
        $('#checkAll').on('change', function(e){
            console.log(e);
            $('input[type=checkbox]').each(function() {
                $( this ).prop('checked', true);
            });
        });  */
    </script>
@endsection