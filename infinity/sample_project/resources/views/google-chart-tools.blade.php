@extends('app')

@section('page-title')
    Google Chart Tools
@endsection

@section('page-css')
    <style>
    .google-visualization-orgchart-noderow-medium, .google-visualization-orgchart-table {
        border-collapse: initial;
    }
    </style>
@endsection
@section('content-header')
    <h1>
        Google Chart Tools
    </h1>
@endsection

@section('content')

    <div class="card-columns">
        <div class="card">
            <div class="card-header">
                Pie Chart
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="pieChart"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Geo Chart
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="geoChart"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Line Chart
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="lineChart"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Area Chart
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="areaChart"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Bar Chart
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="barChart"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Column Chart
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="columnChart"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Gauge Chart
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="gauge"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Histogram Chart
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="histogram"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Org Chart
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="orgChart"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Scatter Chart
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="scatterchart"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Scatter Chart
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="steppedareachart"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Table
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="table"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Treemap
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="treemap"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Candlestick Chart
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="candlestickchart"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Combo Chart
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="combochart"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Timeline Chart
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="timelinechart"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Bubble Chart
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="bubblechart"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Donnuts Chart
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="donutchart"></div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="{{asset('js/custom-google-charts-tools.js')}}"></script>
@endsection