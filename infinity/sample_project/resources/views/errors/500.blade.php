@extends('app')

@section('page-title')
    500 Error
@endsection

@section('page-css')
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/animate.css/animate.min.css')}}" />
@endsection

@section('content-header')

@endsection

@section('content')

    <div class="error-page text-sm-center">
        <h2 class="display-1 text-danger animated shake">500 Error</h2>
        <br />
        <br />
        <div class="row">
        <div class="col-md-12">
            <h3><i class="fa fa-warning  text-danger"></i> Oops! Something went wrong. <i class="fa fa-warning text-danger"></i> </h3>
            <br />
            <br />
            <p class="lead">
                We will work on fixing that right away.
                <br />
                <br />
                <a href="{{route('home')}}"><button class="btn btn-circle btn-lg red"><i class="fa fa-home"></i></button></a>
            </p>

            <form class="search-form">
                <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="or Search for another Page">

                    <div class="input-group-btn">
                        <button type="submit" name="submit" class="btn btn-danger btn-flat"><i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        </div>
    </div>

@endsection

@section('page-scripts')

@endsection