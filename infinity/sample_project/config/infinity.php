<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Use minified js and css files
    |--------------------------------------------------------------------------
    |
    | Here you can specify your custom template path for the generator.
    |
     */
    'use_minified' => true,


];
