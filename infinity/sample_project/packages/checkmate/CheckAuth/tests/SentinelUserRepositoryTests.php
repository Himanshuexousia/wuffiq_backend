<?php

require_once 'CheckAuthTestCase.php';

use Illuminate\Support\Facades\DB;

class SentinelUserRepositoryTests extends CheckAuthTestCase
{
    /**
     * Setup the test environment.
     */
    public function setUp()
    {
        parent::setUp();

        $this->repo = app()->make('Checkmate\CheckAuth\Repositories\User\CheckAuthUserRepositoryInterface');

        //Mock class
/*        $mock = $this->getMockBuilder('Checkmate\CheckAuth\Repositories\User\CheckAuthUserRepositoryInterface')
            ->disableOriginalConstructor()
            ->getMock();*/

        //$mock->method('doSomething')->will($this->returnSelf());

        //$this->repo = app()->make($mock);

        include __DIR__ . '/../src/Http/routes.php';
    }

    /**
     * @group user
     * Test the instantiation of the CheckAuth SentinelUser repository
     */
    public function testRepoInstantiation()
    {
        // Test that we are able to properly instantiate the SentinelUser object for testing
        $this->assertInstanceOf('Checkmate\CheckAuth\Repositories\User\SentinelUserRepository', $this->repo);
    }

    /**
     * @group user
     * Test that the seed data exists and is reachable
     */
    public function testDatabaseSeeds()
    {
        // Check that the test data is present and correctly seeded
        $user = DB::table('users')->where('email', 'user@auth.com')->first();
        $this->assertEquals('user@auth.com', $user->email);
    }

    /**
     * @group user
     * Test the creation of a user using the default configuration options
     */
    public function testSavingUser()
    {

        // Explicily disable additional user fields
        //app()['config']->set('checkauth.additional_user_fields', []);

        // This is the code we are testing
        $result = $this->repo->store([
            'first_name' => 'Michael',
            'last_name'  => 'Jackson',
            'email'      => 'michael@jackson.net',
            'password'   => 'jackson'
        ]);

        // Grab the "Users" role object for assertions
        $usersRole = Sentinel::findRoleByName('Users');

        $testUser = DB::table('users')
            ->where('email', 'michael@jackson.net')
            ->where('first_name', 'Michael')
            ->where('last_name', 'Jackson')
            ->first();

        // Assertions
        $this->assertTrue($result->isSuccessful());
        $this->assertFalse($result->getPayload()['activated']);
        //$this->assertTrue($result->getPayload()['user']->inRole($usersRole));
        $this->assertEquals('michael@jackson.net', $testUser->email);
    }



    /**
     * @group user
     * Test the creation of a user that should be activated upon creation
     */
    public function testSavingActivatedUser()
    {
        // This is the code we are testing
        $result = $this->repo->store([
            'first_name' => 'Silvester',
            'last_name'  => 'Stallone',
            'email'      => 'silvester@stallone.net',
            'password'   => 'stallone',
            'activate'   => true
        ]);

        // Assertions
        $this->assertTrue($result->isSuccessful());
        $this->assertTrue($result->getPayload()['activated']);
        $testUser = DB::table('users')
            ->where('email', 'silvester@stallone.net')
            ->first();

        $activation = DB::table('activations')
            ->where('user_id', $testUser->id)
            ->where('completed', '1')
            ->first();
        $this->assertEquals('silvester@stallone.net', $testUser->email);

    }


    /**
     * @group user2
     * Test the creation of users with additional user fields
     */
    public function testSavingUserWithAdditionalData()
    {
        // Explicitly set the 'allow usernames' config option to false
        //app()['config']->set('checkauth.allow_usernames', false);

        // Explicitly set the 'additional user fields'
        app()['config']->set('checkauth.additional_user_fields', [
                'first_name' => 'alpha_spaces',
                'last_name'  => 'alpha_spaces'
        ]);

        // This is the code we are testing
        $result = $this->repo->store([
            'first_name' => 'Michael',
            'last_name'  => 'Jackson',
            'email'      => 'michael@jackson.net',
            'password'   => 'jackson',
        ]);

        // Assertions
        $this->assertTrue($result->isSuccessful());
        $this->assertFalse($result->getPayload()['activated']);
        $testUser = DB::table('users')
            ->where('email', 'michael@jackson.net')
            ->where('first_name', 'Michael')
            ->where('last_name', 'Jackson')
            ->first();
        $this->assertEquals('michael@jackson.net', $testUser->email);
    }


    /**
     * @group user
     * Test deleting a user from storage
     */
    public function testDestroyUser()
    {
        $credentials = [
            'email'    => 'user@auth.com',
        ];
        // Find the user we are going to delete
        $user = Sentinel::findByCredentials($credentials);

        // This is the code we are testing
        $this->repo->destroy($user->id);

        // Assertions
        $this->assertTrue(DB::table('users')->where('email', 'user@auth.com')->count() == 0);
    }

    /**
     * @group user
     * Test user activation
     */
    public function testActivatingUser()
    {
        // Explicily disable additional user fields
        //app()['config']->set('checkauth.additional_user_fields', []);

        // Create a new user that is not activated
        $userResponse = $this->repo->store([
            'first_name' => 'Michael',
            'last_name'  => 'Jackson',
            'email'      => 'michael@jackson.net',
            'password'   => 'jackson',
        ]);

        $user = $userResponse->getPayload()['user'];
        $activation = Activation::exists($user);

        // This is the code we are testing
        $result = $this->repo->activate($user->id, $activation->code);

        // Assertions
        $this->assertTrue($result->isSuccessful());
        $this->assertTrue(DB::table('activations')->where('user_id', '3')->where('completed', 1)->count() == 1);
    }


    /**
     * @group hash
     * Test change user password
     */
    public function testHash()
    {
        $userResponse = $this->repo->store([
            'first_name' => 'Michael',
            'last_name'  => 'Jackson',
            'email'      => 'michael@jackson.net',
            'password'   => 'jackson',
        ]);

        $user = $userResponse->getPayload()['user'];

        $this->assertInstanceOf('Checkmate\CheckAuth\Models\User', $user);
        $this->assertTrue(password_verify('jackson', $user->password));
    }

    /**
     * @group user
     * Test change user password
     */
    public function testChangeUserPassword()
    {
        // Create a new user that is not activated
        $userResponse = $this->repo->store([
            'first_name' => 'Michael',
            'last_name'  => 'Jackson',
            'email'      => 'michael@jackson.net',
            'password'   => 'jackson',
        ]);

        $user = $userResponse->getPayload()['user'];

        // This is the code we are testing
        $result = $this->repo->changePassword([
            'id'          => $user->id,
            'oldPassword' => 'jackson',
            'newPassword' => 'jackson2'
        ]);

        $testUser = DB::table('users')
            ->where('id', $user->id)
            ->where('email', 'michael@jackson.net')
            ->first();

        // Assertions
        $this->assertTrue($result->isSuccessful());
        $this->assertTrue(password_verify('jackson2', $testUser->password));
    }


    /**
     * @group user
     * Test retrieve user by id
     */
    public function testRetrieveUserById()
    {
        // This is the code we are testing
        $user = $this->repo->retrieveById(1);

        $this->assertInstanceOf('Checkmate\CheckAuth\Models\User', $user);
        $this->assertEquals('admin@auth.com', $user->email);
    }

    /**
     * @group user
     * Test retrieve user by credentials
     */
    public function testRetrieveByCredentials()
    {
        // This is the code we are testing
        $user = $this->repo->retrieveByCredentials(['email' => 'admin@auth.com']);

        // Assertions
        $this->assertEquals(1, $user->id);
    }

    /**
     * @group user
     * Test retrieving all users
     */
    public function testRetrieveAllUsers()
    {
        // This is the code we are testing
        $users = $this->repo->all();
        echo count($users);

        // Assertions
        $this->assertTrue(is_object($users));
        $this->assertEquals(2, count($users));
    }
}