<?php

namespace Checkmate\CheckAuth\DataTransferObjects;

/**
 * Inherits BaseResponse
 *
 * @license http://www.checkmate.com/licenses/checkauth
 *
 * @version 1.0.0
 */
class ExceptionResponse extends BaseResponse
{
    public function __construct($message, array $payload = null)
    {
        parent::__construct($message, $payload);

        $this->success = false;
        $this->error = true;
    }
}
