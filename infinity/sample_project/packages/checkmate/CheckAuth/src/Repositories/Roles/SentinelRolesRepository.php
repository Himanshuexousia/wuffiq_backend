<?php

namespace Checkmate\CheckAuth\Repositories\Roles;

use Cartalyst\Sentinel\Roles\EloquentRole;
use Cartalyst\Sentinel\Sentinel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Events\Dispatcher;
use Illuminate\Http\Response;
use Checkmate\CheckAuth\Models\Role;
use Checkmate\CheckAuth\DataTransferObjects\BaseResponse;
use Checkmate\CheckAuth\DataTransferObjects\SuccessResponse;
use Checkmate\CheckAuth\DataTransferObjects\FailureResponse;
use Checkmate\CheckAuth\DataTransferObjects\ExceptionResponse;

/**
 * separate the logic that retrieves the data and maps it to the entity model from the business logic that acts on the model
 * @license http://www.checkmate.com/licenses/checkauth
 * @version 1.0.0
 */
class SentinelRolesRepository implements CheckAuthRolesRepositoryInterface
{
    protected $sentry;

    /**
     * Constructor
     * * @param Sentinel $sentinel
     * * @param Dispatcher $dispatcher
     */
    public function __construct(Sentinel $sentinel, Dispatcher $dispatcher)
    {
        // Sentinel Singleton Object
        $this->sentinel = $sentinel;
        
        $this->dispatcher = $dispatcher;
    }

    /**
     * Store a newly created resource in storage.
     * @param $data $data
     * @return BaseResponse
     */
    public function store($data)
    {
        try {
            //check if role exists
            if($this->sentinel->findRoleBySlug($data['name']))
            {
                $message = trans('checkauth::roles.roleexists');
                return new ExceptionResponse($message);

            }else{
                // Assemble permissions
                $permissions = (isset($data['permissions']) ? $data['permissions'] : []);

                $role = $this->sentinel->getRoleRepository()->createModel()->create([
                    'name' => e($data['name']),
                    'slug' => e(strtolower($data['name'])),
                    'permissions' => $permissions,
                ]);

                // Fire the 'role created' event (not implemented)
                //$this->dispatcher->fire('checkauth.role.created', ['role' => $role]);

                return new SuccessResponse(trans('checkauth::roles.created'), ['role' => $role]);
            }

        } catch (Exception $e) {
            return new ExceptionResponse($e);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  $data $data
     * @return BaseResponse
     */
    public function update($data)
    {
        try {
            // Assemble permissions
            $permissions = (isset($data['permissions']) ? $data['permissions'] : []);

            // Find the role using the role id
            $role = $this->sentinel->findRoleById($data['id']);

            if($role){
                // Grab the current (pre-edit) permissions and nullify appropriately
                $existingPermissions = $role->getPermissions();
                $nulledPermissions   = array_diff_key($existingPermissions, $permissions);
                foreach ($nulledPermissions as $key => $value) {
                    // Set the nulled permissions to 0
                    $permissions[$key] = 0;
                }

                // Update the role details
                $role->name        = e($data['name']);
                $role->permissions = $permissions;

                // Update the role
                if ($role->save()) {
                    // Fire the 'role updated' event not implemented yet
                    //$this->dispatcher->fire('checkauth.role.updated', ['role' => $role]);

                    return new SuccessResponse(trans('checkauth::roles.updated'), ['role' => $role]);
                } else {
                    // There was a problem
                    return new FailureResponse(trans('checkauth::roles.updateproblem'), ['role' => $role]);
                }
            }else{
                $message = trans('checkauth::roles.notfound');
                return new ExceptionResponse($message);
            }

        } catch (Exception $e) {
            return new ExceptionResponse($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return BaseResponse
     */
    public function destroy($id)
    {
        try {
            // Find the role using the role id
            $role = $this->sentinel->findRoleById($id);

            if($role){
                // Delete the role
                $role->delete();
                // Fire the 'role destroyed' event not implemented yet
                //$this->dispatcher->fire('checkauth.role.destroyed', ['role' => $role]);
                return new SuccessResponse(trans('checkauth::roles.destroyed'), ['role' => $role]);
            }else{
                $message = trans('checkauth::roles.notfound');
                return new ExceptionResponse($message);
            }

        } catch (Exception $e) {
            return new ExceptionResponse($e);
        }
    }

    /**
     * Return a specific role by a given id
     *
     * @param  integer $id
     * @return Role
     */
    public function retrieveById($id)
    {
        return $this->sentinel->findRoleById($id);
    }

    /**
     * Return a specific role by a given name
     *
     * @param  string $name
     * @return Role
     */
    public function retrieveByName($name)
    {
        return $this->sentinel->findRoleByName($name);
    }

    /**
     * Return all the registered roles
     *
     * @return Array
     */
    public function all()
    {
        $roles = EloquentRole::all();
        return $roles;
    }
}
