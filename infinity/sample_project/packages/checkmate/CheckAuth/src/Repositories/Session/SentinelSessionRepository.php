<?php

namespace Checkmate\CheckAuth\Repositories\Session;

use Config;
use Illuminate\Events\Dispatcher;
use Cartalyst\Sentinel\Sentinel;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Checkmate\CheckAuth\DataTransferObjects\BaseResponse;
use Checkmate\CheckAuth\DataTransferObjects\ExceptionResponse;
use Checkmate\CheckAuth\DataTransferObjects\SuccessResponse;


/**
 * separate the logic that retrieves the data and maps it to the entity model from the business logic that acts on the model
 * @license http://www.checkmate.com/licenses/checkauth
 * @version 1.0.0
 */
class SentinelSessionRepository implements CheckAuthSessionRepositoryInterface
{
    private $sentinel;
    private $sentinelUserProvider;
    private $dispatcher;

    public function __construct(Sentinel $sentinel, Dispatcher $dispatcher)
    {

        // Sentinel Singleton Object
        $this->sentinel = $sentinel;

        $this->dispatcher = $dispatcher;

        // Get the user repo
        $this->sentinelUserProvider = $this->sentinel->getUserRepository();
    }

    /**
     * Store a newly created resource in storage.
     * @param array $data
     * @return BaseResponse
     */
    public function store($data)
    {
        try {
            // Check for 'rememberMe' in POST data
            $rememberMe = isset($data['rememberMe']);


            // Set login credentials
            $credentials['password'] = e($data['password']);//e function sanitizes the input string.
            $credentials['email']    = isset($data['email']) ? e($data['email']) : '';

            // Try to authenticate the user
            if($user = $this->sentinel->authenticate($credentials, $rememberMe)){

                // Login was successful. Fire the CheckAuth.user.login event
                $this->dispatcher->fire('checkauth.user.login', ['user' => $user]);

                // Return Response Object
                return new SuccessResponse('');

            }else{

                $message = trans('checkauth::sessions.invalid');
                return new ExceptionResponse($message);

            }

        }catch (ThrottlingException $e) {

            $delay = $e->getDelay();
            $message = "Your account is blocked for {$delay} second(s).";
            return new ExceptionResponse($message);

        }catch (NotActivatedException $e) {

            $url = route('checkauth.reactivate.form');
            $message = trans('checkauth::sessions.notactive', array('url' => $url));
            return new ExceptionResponse($message);

        }
    }

    /**
     * Store a newly authenticated user in session.
     *
     * @param array $data
     *
     * @return BaseResponse
     */
    public function oauthStore($data)
    {
        try {

            // Set login credentials
            $credentials['email']    = e($data['email']);

            // Try to find the user
            if($user = $this->sentinel->findByCredentials($credentials)) {

                //oath id did not exist, register provider
                if (strlen($user['oauth_id']) == 0) {
                    $url = route('checkauth.provider', ['hash' => $user->hash, 'provider' => $data['provider'], 'oauth_id' => $data['oauth_id']]);
                    $message = trans('checkauth::sessions.add_provider', array('provider' => $data['provider'], 'url' => $url));
                    return new ExceptionResponse($message, array('route' => 'checkauth.login', 'data' => $data, 'message_type' => 'warning'));
                }

                //oath id is different, change provider...
                if ($user['oauth_id'] != $data['oauth_id']) {
                    $url = route('checkauth.provider', ['hash' => $user->hash, 'provider' => $data['provider'], 'oauth_id' => $data['oauth_id']]);
                    $message = trans('checkauth::sessions.change_providers', array('provider' => $data['provider'], 'url' => $url));
                    return new ExceptionResponse($message, array('route' => 'checkauth.login', 'data' => $data, 'message_type' => 'warning'));
                }

                //oauth_id and provider match?
                if($user['oauth_id'] === $data['oauth_id']){

                    //check if user is active
                    if(Activation::completed($user)){

                        //user exists... log him in
                        $this->sentinel->login($user);
                        // Login was successful. Fire the CheckAuth.user.login event
                        $this->dispatcher->fire('checkauth.user.login', ['user' => $user]);
                        // Return Response Object
                        return new SuccessResponse('');

                    }else{
                        //user not activated, redirect to login and warn him.
                        $url = route('checkauth.reactivate.form');
                        //$this->recordLoginAttempt($credentials);
                        $message = trans('checkauth::sessions.notactive', array('url' => $url));
                        return new ExceptionResponse($message, array('route' => 'checkauth.login', 'message_type' => 'warning'));
                    }
                }

            }else{
                //user not found, register it
                $message = trans('checkauth::sessions.register_provider', array('provider' => $data['provider']));
                return new ExceptionResponse($message, array('route' => 'checkauth.register.form', 'data' => $data, 'message_type' => 'info'));
            }

        } catch (ThrottlingException $e) {

            $delay = $e->getDelay();
            //$errors = "Your account is blocked for {$delay} second(s).";
            $url = route('checkauth.reactivate.form');
            //$this->recordLoginAttempt($credentials);
            $message = trans('checkauth::sessions.notactive', array('url' => $url));
            return new ExceptionResponse($message, array('message_type' => 'error'));

        } catch (Exception  $e) {
            return $e->getMessage();
        }
    }

    /**
     * Log the current user out and destroy their session
     * @return BaseResponse
     */
    public function destroy()
    {
        // Fire the CheckAuth User Logout event
        $user = $this->sentinel->getUser();
        $this->dispatcher->fire('checkauth.user.logout', ['user' => $user]);
        //\Event::fire(new checkauthEvents('testing!!!'));

        // Destroy the user's session and log them out
        $this->sentinel->logout();
        return new SuccessResponse('');
    }


    /**
     * Record a login attempt to the throttle table.  This only works if the login attempt was
     * made against a valid user object.
     *
     * @param $credentials
     */

/*    private function recordLoginAttempt($credentials)
    {
        if (array_key_exists('email', $credentials)) {
            $throttle = $this->sentinel->findThrottlerByUserLogin(
                $credentials['email'],
                \Request::ip()
            );
        }

        if (array_key_exists('username', $credentials)) {
            $this->sentinelUserProvider->getEmptyUser()->setLoginAttributeName('username');
            $throttle = $this->sentinel->findThrottlerByUserLogin(
                $credentials['username'],
                \Request::ip()
            );
        }

        if (isset($throttle)) {
            $throttle->ip_address = \Request::ip();

            $throttle->addLoginAttempt();
        }
    }*/

    /**
     * Validate an email address
     * http://stackoverflow.com/questions/12026842/how-to-validate-an-email-address-in-php
     */
    private function validEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        return true;
    }
}
