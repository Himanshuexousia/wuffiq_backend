<?php

namespace Checkmate\CheckAuth\Repositories\User;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Collection;
use Checkmate\CheckAuth\DataTransferObjects\BaseResponse;
use Checkmate\CheckAuth\Models\User;

interface CheckAuthUserRepositoryInterface
{
    /**
     * Store or update a newly created resource in storage.
     *
     * @param $data
     *
     * @return BaseResponse
     */
    public function store($data);

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return BaseResponse
     */
    public function destroy($id);

    /**
     * Return all the registered users
     *
     * @return Collection
     */
    public function all();

    /**
     * Return the currently active user
     *
     * @return User
     */
    public function getUser();

    /**
     * Attempt activation for the specified user
     *
     * @param  int $id
     * @param  string $code
     *
     * @return Bool
     */
    public function activate($id, $code);

    /**
     * Attempt to change user oauth provider
     *
     * @param  int $id
     * @param  string $provider
     * @param  string $oauth_id
     *
     * @return Bool
     */
    public function provider($id, $provider, $oauth_id);

    /**
     * Resend the activation email to the specified email address
     *
     * @param  Array $data
     *
     * @return BaseResponse
     */
    public function resend($data);

    /**
     * The user has requested a password reset
     *
     * @param $email
     *
     * @return Bool
     *
     */
    public function triggerPasswordReset($email);

    /**
     * Validate a password reset link
     *
     * @param $id
     * @param $code
     *
     * @return BaseResponse
     */
    public function validateResetCode($id, $code);

    /**
     * Process the password reset request
     *
     * @param  int $id
     * @param  string $code
     *
     * @return BaseResponse
     */
    public function resetPassword($id, $code, $password);

    /**
     * Process a change password request.
     *
     * @return BaseResponse
     */
    public function changePassword($data);

    /**
     * Change a user's password without checking their old password first
     *
     * @param $data
     *
     * @return BaseResponse
     */
    public function changePasswordWithoutCheck($data);

    /**
     * Change user roles memberships
     *
     * @return BaseResponse
     */
    public function changeRoleMemberships($userId, $roles);

    /**
     * Change user permissions
     *
     * @return BaseResponse
     */
    public function changePermissions($userId, $permissions);

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     *
     * @return \Illuminate\Auth\UserInterface|null
    */
    public function retrieveById($identifier);

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     *
     * @return \Illuminate\Auth\UserInterface|null
    */
    public function retrieveByCredentials(array $credentials);

    /**
     * Validate a user against the given credentials.
     *
     * @param  $user
     * @param  array, $credentials
     *
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials);

    /**
     * Check if current user is in role
     *
     * @param  string $role
     *
     * @return user
     */
    public function inRole($role);

    /**
     * Check if current user is in role
     *
     * @param  array $permissions
     *
     * @return user
     */
    public function hasAccess(array $permissions);

    /**
     * Check if current user is in role
     *
     * @param  array $permissions
     *
     * @return user
     */
    public function hasAnyAccess(array $permissions);


}
