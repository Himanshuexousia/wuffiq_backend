<?php

namespace Checkmate\CheckAuth\Repositories\User;

use Cartalyst\Sentinel\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Checkmate\CheckAuth\Models\Role;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Config\Repository;
use Illuminate\Events\Dispatcher;
use Checkmate\CheckAuth\Models\User;
use Checkmate\CheckAuth\DataTransferObjects\BaseResponse;
use Checkmate\CheckAuth\DataTransferObjects\SuccessResponse;
use Checkmate\CheckAuth\DataTransferObjects\FailureResponse;
use Checkmate\CheckAuth\DataTransferObjects\ExceptionResponse;

use Input;
use Image;
use Storage;

/**
 * separate the logic that retrieves the data and maps it to the entity model from the business logic that acts on the model
 * @license http://www.checkmate.com/licenses/checkauth
 * @version 1.0.0
 */
class SentinelUserRepository implements CheckAuthUserRepositoryInterface, UserProvider
{
    protected $config;
    protected $dispatcher;

    /**
     * Construct a new SentinelUser Object
     * @param Sentinel $sentinel
     * @param Repository $config
     * @param Dispatcher $dispatcher
     */
    public function __construct(Sentinel $sentinel, Repository $config, Dispatcher $dispatcher)
    {
        $this->sentinel = $sentinel;// Sentinel Singleton Object
        $this->config = $config;
        $this->dispatcher = $dispatcher;
    }


    /**
     * saves or updates user in repository.
     *
     * @param  array $data
     *
     * @return BaseResponse
     */
    public function store($data)
    {
        try {

            //create or update user?
            if(isset($data['id'])){
                //update
                $user = $this->sentinel->findById($data['id']);
            }else{
                //create
                // Should we automatically activate this user?
                if (array_key_exists('activate', $data)) {
                    $activateUser = (bool)$data['activate'];
                } else {
                    $activateUser = !$this->config->get('checkauth.require_activation', true);
                }

                //Prepare the user credentials
                $credentials = [
                    'email' => e($data['email']),
                    'password' => e($data['password'])
                ];

                // Attempt user registration
                $user = $this->sentinel->register($credentials, $activateUser);

                // If no role memberships were specified, use the default roles from config
                if (array_key_exists('roles', $data)) {
                    $roles = $data['roles'];
                } else {
                    $roles = $this->config->get('checkauth.default_user_roles', []);
                }

                // Assign roles to this user
                foreach ($roles as $name) {
                    $role = $this->sentinel->findRoleByName($name);
                    //if user inrole skip to avoid sql integrity errors
                    if(!$user->inRole($role))
                    $role->users()->attach($user);
                }
            }

            //save or edit fields
            if(isset($data['first_name'])){$user->first_name = e($data['first_name']);}
            if(isset($data['last_name'])){$user->last_name = e($data['last_name']);}
            if(isset($data['oauth_id'])){$user->oauth_id = e($data['oauth_id']);}
            if(isset($data['token'])){$user->token = e($data['token']);}
            if(isset($data['provider'])){$user->provider = e($data['provider']);}

            if(Input::file('avatar')){
                //save avatar
                $path = $this->config->get('checkauth.imagePath');
                //  Before uploading a new image we will check if one already exists and delete it first.
                if ($user->avatar != null) {
                    $old_image = $user->avatar;
                    Storage::delete($path . $old_image);
                }

                //  Next we will get the image to be uploaded, rename it so as to be unique.
                $image = Input::file('avatar');

                $image_name = time() . '-' . $image->getClientOriginalName();
                $newPath = public_path($path . $image_name);

                Image::make($image->getRealPath())->resize(200, 200)->save($newPath);

                $user->avatar = $image_name;
            }else{
                //check if user was created with oauth data, and if avatar was loaded through avatar_url
                if(!empty($data['avatar_url'])){

                    $path = $this->config->get('checkauth.imagePath');
                    $pathInfo = pathinfo($data['avatar_url']);
                    //copy file from url to server
                    $image_name = time() . '-' . strtok($pathInfo['basename'],'?');
                    $newPath = public_path($path . $image_name);

                    Image::make($pathInfo['dirname'].'/'.strtok($pathInfo['basename'],'?'))->save($newPath);

                    $user->avatar = $image_name;
                }
            }

            // If there are additional fields in config file...
            if(is_array($this->config->get('checkauth.additional_user_fields'))){
                foreach ($this->config->get('checkauth.additional_user_fields', []) as $field => $rules) {
                    $user->$field = (isset($data[$field]) ? e($data[$field]) : $user->$field);
                }
            }
            $user->save();

            if(isset($data['id'])){

                return new SuccessResponse(trans('checkauth::users.updated'), ['user' => $user]);

            }else{
                // User registration was successful.  Determine response message
                if ($activateUser) {
                    $message = trans('checkauth::users.createdactive');
                } else {
                    $message = trans('checkauth::users.created');
                }

                // Response Payload
                $payload = [
                    'user' => $user,
                    'activated' => $activateUser
                ];

                // Fire the 'user registered' event
                $this->dispatcher->fire('checkauth.user.registered', $payload);

                // Return a response
                return new SuccessResponse($message, $payload);
            }


        }catch (Exception $e) {
            $message = trans('checkauth::sessions.invalid');
            return new ExceptionResponse($message);
        }
    }

    /**
     * saves user in repository.
     *
     * @param  array $data
     *
     * @return BaseResponse
     */
    public function oauth($data)
    {
        try {
            // Should we automatically activate this user?
            if (array_key_exists('activate', $data)) {
                $activateUser = (bool)$data['activate'];
            } else {
                $activateUser = !$this->config->get('checkauth.require_activation', true);
            }

            //Prepare the user credentials
            $credentials = [
                'email' => e($data['email']),
                'password' => e($data['password'])
            ];

            // Attempt user registration
            $user = $this->sentinel->register($credentials, $activateUser);

            // If no role memberships were specified, use the default roles from config
            if (array_key_exists('roles', $data)) {
                $roles = $data['roles'];
            } else {
                $roles = $this->config->get('checkauth.default_user_roles', []);
            }

            // Assign roles to this user
            foreach ($roles as $name) {
                $role = $this->sentinel->findRoleByName($name);
                //if user inrole skip to avoid sql integrity errors
                if(!$user->inRole($role))
                    $role->users()->attach($user);
            }

            //save or edit fields
            if(isset($data['first_name'])){$user->first_name = e($data['first_name']);}
            if(isset($data['last_name'])){$user->last_name = e($data['last_name']);}
            if(isset($data['oauth_id'])){$user->oauth_id = e($data['oauth_id']);}
            if(isset($data['provider'])){$user->provider = e($data['provider']);}

            if(Input::file('avatar')){
                //save avatar
                $path = $this->config->get('checkauth.imagePath');
                //  Before uploading a new image we will check if one already exists and delete it first.
                if ($user->avatar != null) {
                    $old_image = $user->avatar;
                    Storage::delete($path . $old_image);
                }

                //  Next we will get the image to be uploaded, rename it so as to be unique.
                $image = Input::file('avatar');

                $image_name = time() . '-' . $image->getClientOriginalName();
                $newPath = public_path($path . $image_name);

                Image::make($image->getRealPath())->resize(200, 200)->save($newPath);

                $user->avatar = $image_name;
            }else{
                //check if user was created with oauth data, and if avatar was loaded through avatar_url
                if(!empty($data['avatar_url'])){

                    $path = $this->config->get('checkauth.imagePath');
                    $pathInfo = pathinfo($data['avatar_url']);
                    //copy file from url to server
                    $image_name = time() . '-' . strtok($pathInfo['basename'],'?');
                    $newPath = public_path($path . $image_name);

                    Image::make($pathInfo['dirname'].'/'.strtok($pathInfo['basename'],'?'))->save($newPath);

                    $user->avatar = $image_name;
                }
            }

            // If there are additional fields in config file...
            if(is_array($this->config->get('checkauth.additional_user_fields'))){
                foreach ($this->config->get('checkauth.additional_user_fields', []) as $field => $rules) {
                    $user->$field = (isset($data[$field]) ? e($data[$field]) : $user->$field);
                }
            }
            $user->save();

            if(isset($data['id'])){

                return new SuccessResponse(trans('checkauth::users.updated'), ['user' => $user]);

            }else{
                // User registration was successful.  Determine response message
                if ($activateUser) {
                    $message = trans('checkauth::users.createdactive');
                } else {
                    $message = trans('checkauth::users.created');
                }

                // Response Payload
                $payload = [
                    'user' => $user,
                    'activated' => $activateUser
                ];

                // Fire the 'user registered' event
                $this->dispatcher->fire('checkauth.user.registered', $payload);

                // Return a response
                return new SuccessResponse($message, $payload);
            }

        }catch (Exception $e) {
            $message = trans('checkauth::sessions.invalid');
            return new ExceptionResponse($message);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return BaseResponse
     */
    public function destroy($id)
    {
        try {
            // Find the user using the user id
            $user = $this->sentinel->findById($id);

            // Delete the user
            if ($user->delete()) {

                return new SuccessResponse(trans('checkauth::users.destroyed'), ['user' => $user]);
            }

            // Unable to delete the user
            return new FailureResponse(trans('checkauth::users.notdestroyed'), ['user' => $user]);
        } catch (Exception $e) {
            $message = trans('checkauth::sessions.invalid');

            return new ExceptionResponse($message);
        }
    }

    /**
     * Attempt activation for the specified user
     *
     * @param  int $id
     * @param  string $code
     *
     * @return bool
     */
    public function activate($id, $code)
    {
        try {
            // Find the user using the user id
            $user = $this->sentinel->findById($id);

            if (Activation::complete($user, $code))
            {
                $payload = [
                    'user' => $user,
                    'activated' => true
                ];
                // Activation was successfull
                $this->dispatcher->fire('checkauth.user.registered', $payload);
                // Generate login url
                $url = route('checkauth.login');
                return new SuccessResponse(trans('checkauth::users.activated', array('url' => $url)), ['user' => $user]);
            }
            else
            {
                // Activation not found or not completed.
                return new FailureResponse(trans('checkauth::users.notactivated'), ['user' => $user]);
            }

        } catch (Exception  $e) {

            $message = trans('checkauth::sessions.invalid');
            return new ExceptionResponse($message);

        }
    }

    /**
     * Attempt to change user oauth provider
     *
     * @param  int $id
     * @param  string $provider
     * @param  string $oauth_id
     *
     * @return Bool
     */
    public function provider($id, $provider, $oauth_id){
        try {
            // Find the user using the user id
            $user = $this->sentinel->findById($id);

            $user['provider'] = $provider;
            $user['oauth_id'] = $oauth_id;
            $user->save();

            return new SuccessResponse(trans('checkauth::users.updated'), ['user' => $user]);

        } catch (Exception  $e) {

            $message = trans('checkauth::sessions.invalid');
            return new ExceptionResponse($message);

        }
    }

    /**
     * Resend the activation email to the specified email address
     *
     * @param  Array $data
     *
     * @return BaseResponse
     */
    public function resend($data)
    {
        try {

            $email = e($data['email']);
            $credentials = [
                'login' => $email,
            ];


            if($user = $this->sentinel->findByCredentials($credentials)){

                if(!Activation::completed($user)){
                    $this->dispatcher->fire('checkauth.user.resend', [
                        'user' => $user,
                        'activated' => false,
                    ]);
                    return new SuccessResponse(trans('checkauth::users.emailconfirm'), ['user' => $user]);
                }else{
                    // The user is already activated
                    return new FailureResponse(trans('checkauth::users.alreadyactive'), ['user' => $user]);
                }

            }else{

                $message = 'Email is not registered';
                return new FailureResponse($message, []);

            }

        } catch (Exception $e) {
            $message = $e->getMessage();
            return new FailureResponse($message, []);
        }
    }

    /**
     * The user has requested a password reset
     *
     * @param  string $email
     *
     * @return Bool
     */
    public function triggerPasswordReset($email)
    {
        $email = e($email);

        try {
            $credentials = [
                'login' => $email,
            ];

            if($user = $this->sentinel->findByCredentials($credentials)){

                $reminder = Reminder::exists($user) ?: Reminder::create($user);

                $this->dispatcher->fire('checkauth.user.reset', [
                    'user' => $user,
                    'code' => $reminder->code
                ]);

                return new SuccessResponse(trans('checkauth::users.emailinfo'), ['user' => $user]);

            }else{

                $message = 'Email is not registered';
                return new FailureResponse($message, []);

            }

        } catch (Exception $e) {
            $message = $e->getMessage();
            return new FailureResponse($message, []);
        }
    }


    /**
     * Validate a password reset link
     *
     * @param $id
     * @param $code
     *
     * @return FailureResponse
     */
    public function validateResetCode($id, $code)
    {
        try {
            $user = $this->sentinel->findById($id);

            if($reminder = Reminder::exists($user)){
                if($reminder->code == $code){
                    return new SuccessResponse(null);
                }else{$codeError = true;}
            }else{
                $codeError = true;
            }

            if ($codeError) {
                return new FailureResponse(trans('checkauth::users.invalidreset'), ['user' => $user]);
            }


        } catch (Exception $e) {
            $message = trans('checkauth::sessions.invalid');

            return new ExceptionResponse($message);
        }
    }

    /**
     * Process the password reset request
     *
     * @param  int $id
     * @param  string $code
     *
     * @return Array
     */
    public function resetPassword($id, $code, $password)
    {
        try {
            // Grab the user
            $user = $this->sentinel->findById($id);

            if ($reminder = Reminder::complete($user, $code, $password))
            {
                // Reminder was successfull
                // Fire the 'password reset' event
                $this->dispatcher->fire('checkauth.password.reset', ['user' => $user]);

                return new SuccessResponse(trans('checkauth::users.passwordchg'), ['user' => $user]);
            }
            else
            {
                // Reminder not found or not completed.
                return new FailureResponse(trans('checkauth::users.problem'), ['user' => $user]);
            }


        } catch (Exception $e) {

            $message = trans('checkauth::sessions.invalid');

            return new ExceptionResponse($message);
        }
    }

    /**
     * Process a password change request
     *
     * @param $data
     *
     * @return FailureResponse|SuccessResponse
     */
    public function changePassword($data)
    {
        try {
            $user = $this->sentinel->findById($data['id']);

            // Does the old password input match the user's existing password?
            if (password_verify(e($data['oldPassword']), $user->password)){
                // Set the new password
                //$user->password = e($data['newPassword']);
                $user->password = password_hash(e($data['newPassword']),PASSWORD_BCRYPT);

                if ($user->save()) {

                    // User saved - event listener not implemented yet
                    //$this->dispatcher->fire('checkauth.user.passwordchange', ['user' => $user]);
                    return new SuccessResponse(trans('checkauth::users.passwordchg'), ['user' => $user]);
                }

                // User not Saved
                return new FailureResponse(trans('checkauth::users.passwordprob'), ['user' => $user]);
            }

            // Password mismatch. Abort.
            return new FailureResponse(trans('checkauth::users.oldpassword'), ['user' => $user]);

        } catch (Exception $e) {
            $message = trans('checkauth::sessions.invalid');

            return new ExceptionResponse($message);
        }
    }

    /**
     * Change a user's password without checking their old password first
     *
     * @param $data
     *
     * @return FailureResponse|SuccessResponse
     */
    public function changePasswordWithoutCheck($data)
    {
        try {
            $user = $this->sentinel->findById($data['id']);

            // Set the new password (Sentinel will hash it behind the scenes)
            $user->password = e($data['newPassword']);

            if ($user->save()) {

                // User saved - event listener not implemented yet.
                //$this->dispatcher->fire('checkauth.user.passwordchange', ['user' => $user]);
                return new SuccessResponse(trans('checkauth::users.passwordchg'), ['user' => $user]);
            }

            // User not Saved
            return new FailureResponse(trans('checkauth::users.passwordprob'), ['user' => $user]);
        } catch (Exception $e) {
            $message = trans('checkauth::sessions.invalid');

            return new ExceptionResponse($message);
        }
    }

    /**
     * Change user roles memberships.
     *
     * @param int $userId
     * @param array $selections
     *
     * @return BaseResponse
     */
    public function changeRoleMemberships($userId, $selections)
    {
        try {
            $user = $this->sentinel->findById(e($userId));

            // Gather all available roles
            //$roles = $this->sentinel->getRoleRepository();
            $roles = Role::all();

            // Update role memberships
            foreach ($roles as $role) {
                if (isset($selections[$role->slug])) {
                    //The user should be added to this role
                    if(!$user->inRole($role->slug))//otherwise will get a duplicate key error
                    $role->users()->attach($user);

                } else {
                    // The user should be removed from this role
                    $role->users()->detach($user);
                }
            }

            return new SuccessResponse(trans('checkauth::users.memberships'), ['user' => $user]);


        } catch (Exception $e) {
            $message = trans('checkauth::sessions.invalid');
            return new ExceptionResponse($message);
        }
    }

    /**
     * Change user permissions
     *
     * @param int $userId
     * @param array $selections
     *
     * @return BaseResponse
     */
    public function changePermissions($userId, $selections)
    {
        try {
            $user = $this->sentinel->findById(e($userId));

            // Gather all available permissions
            $permissions = config('checkauth.default_permissions');

            // Update role memberships
            foreach ($permissions as $permission) {
                if (isset($selections[$permission])) {
                    //The user should have this access
                    if(!$user->hasAccess($permission))//otherwise will get a duplicate key error
                        $user->addPermission($permission);

                } else {
                    // The user should be removed from this role
                    $user->removePermission($permission);
                }
            }

            $user->save();

            return new SuccessResponse(trans('checkauth::users.memberships'), ['user' => $user]);


        } catch (Exception $e) {
            $message = trans('checkauth::sessions.invalid');
            return new ExceptionResponse($message);
        }
    }

    /**
     * turn an user inactive
     *
     * @param  int $id
     *
     * @return Array
     */
    public function deactivateUser($id)
    {
        try {
            $user = $this->sentinel->findById($id);

            Activation::remove($user);

            // Fire the 'banned user' event
            //$this->dispatcher->fire('checkauth.user.banned', ['user' => $user]);

            return new SuccessResponse(trans('checkauth::users.deactivation'), ['userId' => $id]);

        } catch (Exception $e) {
            $message = trans('checkauth::sessions.invalid');

            return new ExceptionResponse($message);
        }
    }

    /**
     * tur an user active
     *
     * @param  int $id
     *
     * @return Array
     */
    public function activateUser($id)
    {
        try {

            $user = $this->sentinel->findById($id);

            //Check if an activation record exists for the user.
            $activation = Activation::exists($user);
            If(!$activation){
                $activation = Activation::create($user);
            }
            If($activation){
                //complete process
                if (Activation::complete($user, $activation->code))
                {
                    // Activation was successful, fire activated event
                    //$this->dispatcher->fire('checkauth.user.activated', ['userId' => $id]);
                    return new SuccessResponse(trans('checkauth::users.activation'), ['userId' => $id]);
                }
                else
                {
                    // Activation not found or not completed.
                }
            }


        } catch (Exception $e) {
            $message = trans('checkauth::sessions.invalid');

            return new ExceptionResponse($message);
        }
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     *
     * @return \Illuminate\Auth\UserInterface|null
     */
    public function retrieveById($identifier)
    {
        //$model = $this->sentinel->createModel();
        //return $model->find($identifier);

        //$user = $this->sentinel->findById($identifier);//sentinel model
        $user = User::findOrNew($identifier);
        return $user;
    }

    /**
     * Retrieve a user by by their unique identifier and "remember me" token.
     *
     * @param  mixed $identifier
     * @param  string $token
     *
     * @return \Illuminate\Auth\UserInterface|null
     */
    public function retrieveByToken($identifier, $token)
    {
        $model = $this->sentinel->createModel();

        return $model->where('id', $identifier)->where('persist_code', $token)->first();
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  Authenticatable $user
     * @param  string $token
     *
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        $model = $this->sentinel->createModel();

        $model->where('id', $user->id)->update('persist_code', $token);
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     *
     * @return \Illuminate\Auth\UserInterface|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        try {
            return $this->sentinel->findUserByCredentials($credentials);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Return all the registered users
     *
     * @return Collection
     */
    public function all()
    {

        $users = User::all();

        return $users;
    }

    /**
     * Return the current active user
     *
     * @return user object
     */
    public function getUser()
    {
        return $this->sentinel->getUser();
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  Authenticatable $user
     * @param  array $credentials
     *
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        if (isset($credentials['email']) && $credentials['email'] != $user->email) {
            return false;
        }

        return $user->checkPassword($credentials['password']);
    }

    /**
     * Check if current user is in role
     *
     * @param  string $role
     *
     * @return user
     */
    public function inRole($role){
        return $this->sentinel->inRole($role);
    }

    /**
     * Check if current user is in role
     *
     * @param  array $permissions
     *
     * @return role
     */
    public function hasAccess(array $permissions){
        return $this->sentinel->hasAccess($permissions);
    }

    /**
     * Check if current user is in role
     *
     * @param  array $permissions
     *
     * @return user
     */
    public function hasAnyAccess(array $permissions){
        return $this->sentinel->hasAnyAccess($permissions);
    }


}
