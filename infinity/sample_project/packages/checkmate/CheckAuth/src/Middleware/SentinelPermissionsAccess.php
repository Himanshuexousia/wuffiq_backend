<?php

namespace Checkmate\CheckAuth\Middleware;

use Checkmate\CheckAuth\Repositories\User\CheckAuthUserRepositoryInterface;
use Closure;
use Session;

/**
 * middleware provide a convenient mechanism for filtering HTTP requests entering your application.
 * SentinelPermissionsAccess middleware allows only authenticated users with a certain permission to access.
 *
 * @license http://www.checkmate.com/licenses/checkauth
 *
 * @version 1.0.0
 */
class SentinelPermissionsAccess
{
    /**
     * Constructor
     * @param CheckAuthUserRepositoryInterface $userManager
     */
    public function __construct(CheckAuthUserRepositoryInterface $userManager)
    {
        $this->user = $userManager;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param string $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        $user = $this->user->getUser();
        if(!$user){
        //if (!Sentinel::check()) {//changed to repository call
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest(route('checkauth.login'));
            }
        }

        // user has permission
        if(!$user->hasAccess($permission)){
        //if (!Sentinel::hasAccess($permission)){//changed to repository call
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                Session::flash('error', trans('checkauth::users.noaccess'));
                return redirect()->back();
            }
        }

        return $next($request);
    }
}