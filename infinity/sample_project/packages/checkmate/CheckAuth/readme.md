## CheckAuth

Authentication package for Laravel

## Installation

1 - add dependencies to your composer.json file inside your app root folder.

    "require": {
        ...
        "cartalyst/sentinel": "2.0.*",
        "vinkla/hashids": "~2.0",
        "laravel/socialite": "~2.0",
        "intervention/image": "~2.3.5",
        "anhskohbo/no-captcha": "2.*"
    },
    
and in psr-4 add:

    "psr-4": {
        "App\\": "app/",
        "Checkmate\\CheckAuth\\": "packages/checkmate/CheckAuth/src"
    }        

Go to: {{url('https://www.google.com/recaptcha/intro/index.html')}} , get reCaptcha secret and key codes and add them to your .env file

    NOCAPTCHA_SECRET= <<your-secret>>
    NOCAPTCHA_SITEKEY= <<your-key>>

2 - Run 'composer update' on your command prompt
3 - Open file app.php in the folder 'web/config', and change 'http://localhost' to your domain URL (ex: http://www.mydomain.com) 
4 - Setup database: Open 'config/database.php' and choose your database connection, for example: 'sqlite' or 'mysql' 
5 - Open 'config/mail.php' and configure your e-mail provider parameters. CheckAuth uses this for send registration confirmation, resend passwords, etc.
6 - In 'config/app.php' include Checkauth service provider at the end of the providers list:
    
    'providers' => [
            ...
            Checkmate\CheckAuth\Providers\CheckAuthServiceProvider::class,
            
    
7- Publish Checkauth's views, assets, config files and migrations into your laravels app. In your shell type:

    php artisan checkauth:publish

8 - file permissions:  You may need to configure some permissions. Directories within the storage and the bootstrap/cache directories should be writable by your web server or Laravel will not run.
    
9 - Security: The next thing you should do after installing checkauth is set your application key to a random string. 
Typically, this string should be 32 characters long. The key can be set in the .env environment file located in 'web' folder. 
If your not familiar with Artisan command line 'php artisan key:generate', you can go on this site and generate a key here. Important! 
Character set has to be 0-9, A-Z, a-z (ASCII 48-57, 65-90, 97-122), Lenght 32. 
Then click generate button, then copy it to your file. If the application key is not set, your user sessions and other encrypted data will not be secure! 

## Documentation and Support

http://checkauth.checkmatedigital.com/documentation

http://support.checkmatedigital.com/checkauth

## License

&copy; CheckMateDigital << http://checkmatedigital.com >> 2015-2016, all rights reserved. 

Read about the license in LICENSE file located at the root of the application or visit << <http://checkauth.checkmatedigital.com/license >>