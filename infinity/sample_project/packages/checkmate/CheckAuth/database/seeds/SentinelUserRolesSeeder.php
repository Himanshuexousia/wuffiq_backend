<?php
use Illuminate\Database\Seeder;

class SentinelUserRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_users')->delete();

        $user = [
            'email' => 'user@auth.com',
        ];

        $admin = [
            'email' => 'admin@auth.com',
        ];


        $userUser = \Sentinel::findByCredentials($user);
        $adminUser = \Sentinel::findByCredentials($admin);

        $userRole = \Sentinel::findRoleByName('Users');
        $adminRole = \Sentinel::findRoleByName('Administrators');

        // Assign the groups to the users
        $userRole->users()->attach($userUser);
        $userRole->users()->attach($adminUser);
        $adminRole->users()->attach($adminUser);

    }
}
