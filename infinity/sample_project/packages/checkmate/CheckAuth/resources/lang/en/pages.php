<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Language strings for views
    |--------------------------------------------------------------------------
    */

    'remember-me' => 'Remember me',

    'with' => 'with',

    'forgot-password' => 'Forgot my password',

    'create' => 'Create',

    'update' => 'Update',

    'delete' => 'Delete',

    'account' => 'Conta',

    'first-name' => 'Name',

    'last-name' => 'Last Name',

    'created-at' => 'Created at',

    'updated-at' => 'Updated at',

    'permissions' => 'Permissions',

    'change-password' => 'Change password',

    'update-your-account' => 'Update your account',

    'update-account' => 'Update account',

    'select' => 'Select',

    'image' => 'Image',

    'profile' => 'Profile',

    'change' => 'Change',

    'remove' => 'Remove',

    'old-password' => 'Old password',

    'new-password' => 'New password',

    'confirm-password' => 'Confirm passeord',

    'hello' => 'Hello',

    'flashsuccess'    => 'Success',

    'flasherror'    => 'Error',

    'flashwarning'    => 'Warning',

    'flashinfo'        => 'FYI',

    'register'        => 'Register',

    'login'            => "Login",

    'logout'        => "Logout",

    'home'            => "Home",

    'name' => 'Name',

    'documentation' => 'Documentation',

    'users'        => 'User|Users',

    'roles'        => 'Roles|Roles',

    'helloworld'    => "This is a sample app homepage",

    'description'    => "This is an example of <a href=\"https://github.com/laravel/laravel\">Laravel 4.1</a> running with <a href=\"https://github.com/cartalyst/sentry\">Sentry 2.0</a> and <a href=\"http://getbootstrap.com/\">Bootstrap 3.0</a>.",

    'loginstatus'    => "You are currently logged in.",

    'sessiondata'    => "Session Data",

    'currentusers'    => "Current Users",

    'options'        => 'Options',

    'status'        => "Status",

    'active'        => "Active",

    'notactive'        => "Inactive",

    'suspended'        => "Suspended",

    'banned'        => "Banned",

    'actionedit'    => "Edit",

    'actionsuspend'    => "Suspend",

    'actionunsuspend' => "Un-Suspend",

    'actionban'        => "Ban",

    'actionunban'    => "Un-Ban",

    'actiondelete'    => "Delete",

    'savingDisabled' => "Saving is disabled in this application",

);
