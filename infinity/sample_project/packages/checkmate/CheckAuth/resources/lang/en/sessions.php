<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Session Repository Messages
    |--------------------------------------------------------------------------
    */

    "invalid"   => "Invalid username or password.",

    "notactive" =>  "You have not yet activated this account. <a href=':url' class='alert-link'>Resend Activation Email?</a>",

    "suspended" => "Your account has been temporarily suspended.",

    "banned"    => "You have been banned.",

    "oathRegister" => "Confirm your information and provide a password. You can login with :provider or with your e-mail and password in the future",

    "change_providers" => "This e-mail is already associated with another oauth provider. Do you want to change to :provider? <a href=':url' class='alert-link'>Click here to proceed</a>",

    "add_provider" => "Do you want to login with ':provider'? <a href=':url' class='alert-link'>Click here to proceed</a>",

    "register_provider" => "Confirm your information and provide a password. You can login with :provider or with your e-mail and password in the future",

    "provider_changed" => "Oauth Provider was changed to :provider! You can now login pressing 'Login with :provider' button",

);
