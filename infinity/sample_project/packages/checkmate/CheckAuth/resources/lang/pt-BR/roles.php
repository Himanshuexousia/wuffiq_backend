<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Roles Repositiory Messages
    |--------------------------------------------------------------------------
    */

    'created'        => "Grupo Criado.",

    'loginreq'        => "O campo Login é obrigatório.",

    'userexists'    => "Usuário existente.",

    'updated'        => "O Grupo foi atualizado.",

    'updateproblem' => "Ocorreu um problema ao atualizar o grupo.",

    'namereq'        => "Você deve informar um nome para o grupo.",

    'groupexists'    => "Este grupo já existe.",

    'notfound'        => "Grupo não encontrado.",

);
