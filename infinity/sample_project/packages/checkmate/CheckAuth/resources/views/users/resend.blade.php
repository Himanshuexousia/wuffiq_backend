@extends('checkauth::sessions.logoff')
    {{-- Web site Title --}}
@section('htmlHeaderTitle')
    Resend Activation link
@endsection

{{-- Content --}}
@section('checkauth:grid-class')
    col-md-offset-4 col-md-4 col-md-offset-4
@stop

@section('checkauth:card-title')
    Resend Activation link
@stop

@section('checkauth::card-block')

    @include('checkauth::partials.notifications')
    <form method="POST" action="{{ route('checkauth.reactivate.send') }}" accept-charset="UTF-8">
        <div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
            <input class="form-control" placeholder="E-mail" autofocus="autofocus" name="email" type="text" value="{{ Input::old('name') }}">
            {{ ($errors->has('email') ? $errors->first('email') : '') }}
        </div>
        <div class="form-group">
            {!! app('captcha')->display() !!}
            {{ ($errors->has('g-recaptcha-response') ? $errors->first('g-recaptcha-response') : '') }}
        </div>

        <input name="_token" value="{{ csrf_token() }}" type="hidden">
        <input class="btn btn-primary" value="Resend" type="submit">

    </form>

@stop

@section('checkauth::card-footer')
@stop


@include('checkauth::partials.scripts')
@section('checkauth::scripts')
@stop
