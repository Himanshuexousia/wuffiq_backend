@extends('checkauth::app')
{{-- Web site Title --}}
@section('htmlHeaderTitle')
    {{ trans('checkauth::pages.create') }} {{ trans_choice('checkauth::pages.users', 1) }}
@endsection

{{-- Content --}}
@section('checkauth::main-content')

    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('checkauth::pages.create') }} {{ trans_choice('checkauth::pages.users', 1) }}</h3>
                </div>
                <form method="POST" action="{{ route('checkauth.users.insert') }}" accept-charset="UTF-8">
                    <div class="card-block">

                        <div class="form-group {{ ($errors->has('first_name')) ? 'has-error' : '' }}">
                            <input class="form-control" placeholder="First name" name="first_name" type="text"  value="{{ Input::old('first_name') }}">
                            {{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}
                        </div>

                        <div class="form-group {{ ($errors->has('last_name')) ? 'has-error' : '' }}">
                            <input class="form-control" placeholder="Last name" name="last_name" type="text"  value="{{ Input::old('last_name') }}">
                            {{ ($errors->has('last_name') ? $errors->first('last_name') : '') }}
                        </div>

                        <div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
                            <input class="form-control" placeholder="E-mail" name="email" type="text"  value="{{ Input::old('email') }}">
                            {{ ($errors->has('email') ? $errors->first('email') : '') }}
                        </div>

                        <div class="form-group {{ ($errors->has('password')) ? 'has-error' : '' }}">
                            <input class="form-control" placeholder="Password" name="password" value="" type="password">
                            {{ ($errors->has('password') ?  $errors->first('password') : '') }}
                        </div>

                        <div class="form-group {{ ($errors->has('password_confirmation')) ? 'has-error' : '' }}">
                            <input class="form-control" placeholder="Confirm Password" name="password_confirmation" value="" type="password">
                            {{ ($errors->has('password_confirmation') ?  $errors->first('password_confirmation') : '') }}
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="activate" value="activate" type="checkbox">
                                    Activate
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <input name="_token" value="{{ csrf_token() }}" type="hidden">

                            <div class='btn-group'>
                                <input class="btn btn-primary" value="Create" type="submit">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">

                    </div>
                </form>
            </div>
        </div>
    </div>

@stop