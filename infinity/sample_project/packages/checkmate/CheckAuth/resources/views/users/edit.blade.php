@extends('checkauth::app')
{{-- Web site Title --}}
@section('htmlHeaderTitle')
    {{trans('checkauth::pages.update')}} {{ trans_choice('checkauth::pages.users', 1) }}
@endsection

{{-- Content --}}
@section('checkauth::main-content')

    <?php
    // Pull the custom fields from config
    $userLogged = CheckAuth::getUser();
    $isProfileUpdate = ($user->email == $userLogged->email);
    //$customFields = config('checkauth.additional_user_fields');
    $customFields = $userFields;

    // Determine the form post route
    if ($isProfileUpdate) {
        $profileFormAction = route('checkauth.profile.update');
        $passwordFormAction = route('checkauth.profile.password');
    } else {
        $profileFormAction =  route('checkauth.users.update', $user->hash);
        $passwordFormAction = route('checkauth.password.change', $user->hash);
    }
    ?>


    <div class="row">
        <div class="col-xs-8">
            <div class="card">
                <div class="card-header">
                    <h3>
                        @if($isProfileUpdate)
                            {{trans('checkauth::pages.update-your-account')}}
                        @else
                            {{trans('checkauth::pages.update-account')}} {{ $user->first_name }} {{  $user->last_name }}'s
                        @endif
                        </h3>
                </div>
                @if(!empty($customFields))
                    <form method="POST" action="{{ $profileFormAction }}" accept-charset="UTF-8" role="form" enctype="multipart/form-data">
                        <div class="card-block">
                            <input name="_method" value="PUT" type="hidden">
                            <input name="_token" value="{{ csrf_token() }}" type="hidden">

                            @foreach($customFields as $field => $rules)
                                @if($rules === 'image')

                                    <div class="form-group row {{ ($errors->has($field)) ? 'has-danger' : '' }}" for="{{ $field }}">
                                        <label for="{{ $field }}" class="col-sm-2 form-control-danger">{{ ucwords(str_replace('_',' ',$field)) }}</label>
                                        <div class="col-sm-10">

                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
                                                    @if($user->$field)
                                                        <img src="{!! url('/img/avatars/'.$user->$field) !!}" alt="{{trans('checkauth::pages.profile')}} {{trans('checkauth::pages.image')}}" />
                                                    @else
                                                        <img src="{!! url('/vendor/checkauth/img/blank_avatar.png') !!}" alt="profile image"/>
                                                    @endif
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                                <div>
                                            <span class="btn btn-secondary btn-file"><span class="fileinput-new">{{trans('checkauth::pages.select')}} {{trans('checkauth::pages.image')}}</span>
                                            <span class="fileinput-exists">{{trans('checkauth::pages.change')}}</span><input type="file" name="{{ $field }}" id="{{ $field }}"></span>
                                                    <a href="#" class="btn btn-secondary fileinput-exists" data-dismiss="fileinput">{{trans('checkauth::pages.remove')}}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="form-group row {{ ($errors->has($field)) ? 'has-danger' : '' }}" for="{{ $field }}">
                                        <label for="{{ $field }}" class="col-sm-2">{{trans('checkauth::pages.' . str_replace('_','-',$field))}}{{-- ucwords(str_replace('_',' ',$field)) --}}</label>
                                        <div class="col-sm-10">
                                            <input class="form-control form-control-danger" name="{{ $field }}" type="text" value="{{ Input::old($field) ? Input::old($field) : $user->$field }}">
                                            @if($errors->has($field))<span style="color:red">{{$errors->first($field)}}</span>@endif
                                        </div>
                                    </div>
                                @endif

                            @endforeach
                            <input class="btn btn-primary" value="{{trans('checkauth::pages.update')}}" type="submit">
                        </div>
                        <div class="card-footer"></div>
                    </form>
                @endif

            </div>


        </div>

        <div class="col-xs-4">
            @if(CheckAuth::inRole('admins'))
                <div class="card">
                    <div class="card-header">
                        <h3>{{trans_choice('checkauth::pages.roles',2)}}</h3>
                    </div>

                    <form method="POST" action="{{ route('checkauth.users.memberships', $user->hash) }}" accept-charset="UTF-8" class="form-horizontal" role="form">
                        <div class="card-block">


                            <div class="form-group row">
                                <div class="col-sm-offset-2 col-sm-10">
                                    @foreach($roles as $role)
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="roles[{{ $role->slug }}]" value="1" {{ ($user->inRole($role->slug) ? 'checked' : '') }}> {{ $role->name }}
                                        </label>
                                    @endforeach
                                </div>
                            </div>

                            <input name="_token" value="{{ csrf_token() }}" type="hidden">
                            <input class="btn btn-primary" value="{{trans('checkauth::pages.update')}} {{trans_choice('checkauth::pages.roles',2)}}" type="submit">

                        </div>
                        <div class="card-footer"></div>
                    </form>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h3>{{trans('checkauth::pages.permissions')}}</h3>
                    </div>

                    <form method="POST" action="{{ route('checkauth.users.permissions', $user->hash) }}" accept-charset="UTF-8" class="form-horizontal" role="form">
                        <div class="card-block">

                            <div class="form-group row">
                                <div class="col-sm-offset-2 col-sm-10">
                                    @foreach($permissions as $permission)
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="permissions[{{ $permission }}]" value="1" {{ ($user->hasAccess($permission) ? 'checked' : '') }}> {{ $permission }}
                                        </label>
                                    @endforeach
                                </div>
                            </div>

                            <input name="_token" value="{{ csrf_token() }}" type="hidden">
                            <input class="btn btn-primary" value="{{trans('checkauth::pages.update')}} {{trans('checkauth::pages.permissions')}}" type="submit">

                        </div>
                        <div class="card-footer"></div>
                    </form>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3>{{trans('checkauth::pages.change-password')}}</h3>
                </div>
                <form method="POST" action="{{ $passwordFormAction }}" accept-charset="UTF-8" role="form">
                    <div class="card-block">
                        @if(! $user->inRole(['admin']))
                            <div class="form-group row @if ($errors->has('oldPassword')) has-danger @endif">
                                <label for="oldPassword" class="sr-only">{{trans('checkauth::pages.old-password')}}</label>
                                <input class="form-control" placeholder="{{trans('checkauth::pages.old-password')}}" name="oldPassword" value="" id="oldPassword" type="password">
                            </div>
                        @endif

                        <div class="form-group row @if ($errors->has('newPassword')) has-danger @endif">
                            <label for="newPassword" class="sr-only">{{trans('checkauth::pages.new-password')}}</label>
                            <input class="form-control" placeholder="{{trans('checkauth::pages.new-password')}}" name="newPassword" value="" id="newPassword" type="password">
                        </div>

                        <div class="form-group row @if ($errors->has('newPassword_confirmation')) has-danger @endif">
                            <label for="newPassword_confirmation" class="sr-only">{{trans('checkauth::pages.confirm-password')}}</label>
                            <input class="form-control" placeholder="{{trans('checkauth::pages.confirm-password')}}" name="newPassword_confirmation" value="" id="newPassword_confirmation" type="password">
                        </div>

                        <input name="_token" value="{{ csrf_token() }}" type="hidden">
                        <input class="btn btn-primary" value="{{trans('checkauth::pages.change-password')}}" type="submit">

                        {{ ($errors->has('oldPassword') ? '<br />' . $errors->first('oldPassword') : '') }}
                        {{ ($errors->has('newPassword') ?  '<br />' . $errors->first('newPassword') : '') }}
                        {{ ($errors->has('newPassword_confirmation') ? '<br />' . $errors->first('newPassword_confirmation') : '') }}

                        @foreach ($errors->all() as $error)
                            {{-- $error --}}<br>
                        @endforeach

                    </div>
                    <div class="card-footer"></div>
                </form>
            </div>

        </div>


    </div>

@stop
