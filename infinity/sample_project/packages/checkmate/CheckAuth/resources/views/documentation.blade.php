<!doctype html>
<html lang="en-us">
<head>
    <meta charset="utf-8">
    <title>Auth - Advanced role based authentication with social login and auth webservice for Laravel 5</title>

    <meta name="description" content="checkauth - Advanced interface and role based authentication for Laravel 5">
    <meta name="author" content="checkmate digital">
    <meta name="copyright" content="checkmatedigital.com">

    <link rel="stylesheet" href="{{ asset('vendor/documenter/css/documenter_style.css') }}" media="all">

    <script src="{{ asset('vendor/jquery/jquery.js') }}"></script>
    <script src="{{ asset('vendor/jquery.scrollTo/jquery.scrollTo.min.js') }}"></script>

    <script src="{{ asset('vendor/documenter/js/documenter_script.js') }}"></script>
    <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js?autoload=true&amp;skin=sons-of-obsidian&amp;lang=html" defer="defer"></script>
</head>
<body>
<div id="documenter_sidebar">
    <a href="#start" id="logo"><img src="{{ asset('vendor/checkauth/img/CheckMateLogo.png') }}"></a>
    <ul id="documenter_nav">
        <li><a class="current" href="#start" title="Start">Start</a></li>
        <li><a href="#introduction" title="Introduction">What's in the box</a></li>
        <li><a href="#features" title="Features">Features</a></li>
        <li><a href="#server" title="Server requirements">Server requirements</a></li>
        <li><a href="#installation" title="Installation">Installation</a></li>
        <li><a href="#configuration" title="Configuration">Configuration</a></li>
        <li><a href="#authentication" title="Authentication">Authentication</a></li>
        <li><a href="#social" title="Social authentication">Social authentication</a></li>
        <li><a href="#roles" title="Roles and permissions">Roles and permissions</a></li>
        <li><a href="#routing" title="Routing">Route access</a></li>
        <li><a href="#views" title="Views access">Views access</a></li>
        <li><a href="#changelog" title="Changelog">Changelog</a></li>
        <li><a href="#support" title="Support">Support</a></li>
        <li><a href="#license" title="License">License</a></li>
    </ul>
    <div id="documenter_copyright">Copyright<br><a href="http://www.checkmatedigital.com">Checkmate Digital</a>
    </div>
</div>
<div id="documenter_content">
    <section id="start">
        <img src="{{asset('vendor/checkauth/img/authLogo.png')}}">
        <h1>Auth (CheckAuth)</h1>
        <h2>Advanced role based authentication with social login and auth webservice for Laravel 5</h2>
        <hr>
        <ul>
            <li>Created: 01/01/2016</li>
            <li>latest Update: 01/05/2016</li>
            <li>Developer: <a href="{{asset('http://www.checkmatedigital.com')}}" target="_blank">Checkmate Digital</a></li>
        </ul>
        <p></p>
    </section>

    <section id="introduction">
        <div class="page-header"><h3>What's in the box</h3><hr class="notop"></div>
        <p>Auth is a complete roles authentication package for Laravel 5. It can be used inside your interface or as a webservice. Auth uses top notch technologies, best practices and code testing, providing you with reliable and professional tools. Saving you lots of time, money and letting you focus on your project core objectives.</b><br>
        <p>Our team expend more then one thousand hours developing checkauth and we will expend much more supporting and upgrading it!</p>
    </section>
    <section id="features">
        <div class="page-header"><h3>Features</h3><hr class="notop"></div>
        <ul>
            <li>Authentication and Authorization solution</li>
            <li>Users & Roles Management.</li>
            <li>Webservice authentication</li>
            <li>Working sample views made with blade templating and Bootstrap 4</li>
            <li>Social Authentication (Oauth) ready with: Facebook, Twitter, Google, LinkedIn, GitHub and Bitbucket</li>
            <li>Recapcha security implemented</li>
            <li>Roles and permission based access</li>
            <li>Flexible activation scenarios.</li>
            <li>Reminders (password reset).</li>
            <li>Avatar image upload</li>
            <li>User authentication by e-mail</li>
            <li>E-mail password reset</li>
            <li>Multilanguage support</li>
        </ul>
    </section>
    <section id="server">
        <div class="page-header"><h3>Server requirements</h3><hr class="notop"></div>
        <ul>
            <li>PHP >= 5.5.9</li>
            <li>OpenSSL PHP Extension</li>
            <li>PDO PHP Extension</li>
            <li>Mbstring PHP Extension</li>
            <li>Tokenizer PHP Extension</li>
            <li>FileInfo PHP Extension</li>
            <li>MySql or SQLite database</li>
        </ul>
    </section>
    <section id="installation">
        <div class="page-header"><h3>Installation</h3><hr class="notop"></div>
        <p>It is strongly recommended that you have intermediary knowledge in PHP and Laravel framework</p>
        <p>The zip file comes with a sample laravel project, you can use it as your base project or you can install Auth as a package in your Laravel project (Package requires Laravel 5 to work).</p>
        <p><strong>Option 1 -> Sample project:</strong></p>
        <p>To Use the sample project just unzip SampleProject.zip file in your server. It comes with all required files to work and it is already configured with sqlite as a default database.</p>
        <p>If you prefere mySql you can change the 'Default Database Connection' in config/database.php to mysql and configure your connection on the .env file at projects root folder.</p>
        <p>Run the sql script file 'checkauth_laravel_sample.sql' located in 'database' folder to populate your database.</p>
        <p><strong>Option 2 -> Package installation:</strong></p>
        <p>	<strong>Step 1, extract files:&nbsp;</strong>Extract 'packages' folder from the checkauth.zip file to your Laravel's app root folder. Add dependencies to your composer.json file in your app root folder.</p>
        <pre class="prettyprint linenums:14">
"require": {
    ...
    "cartalyst/sentinel": "2.0.*",
    "vinkla/hashids": "~2.0",
    "laravel/socialite": "~2.0",
    "intervention/image": "~2.3.5",
    "anhskohbo/no-captcha": "2.*"
},
      </pre>
        <p>and in psr-4 add:</p>
        <pre class="prettyprint linenums:36">
"psr-4": {
    "App\\": "app/",
    "Checkmate\\CheckAuth\\": "packages/checkmate/CheckAuth/src"
}
        </pre>
        <p>Run the command above in your shell command prompt and wait untill required libraries are installed:</p>
        <pre class="prettyprint">
composer update
        </pre>

        <p>Go to: {{url('https://www.google.com/recaptcha/intro/index.html')}} and Get reCaptcha secret and site keys, this will allow recapcha to protect your login form</p>
        <p>Then you just need to add to your .env file the following parameters (dont forget to replace your secret-key and site-key from the website above.</p>
        <pre class="prettyprint">
NOCAPTCHA_SECRET=[secret-key]
NOCAPTCHA_SITEKEY=[site-key]
        </pre>
        <p><strong>Important:</strong> After any change on config or env file you must use the command above to clear Laravel's cache</p>
        <pre class="prettyprint">
php artisan config:cache
        </pre>

        <p>	<strong>Step 2, Configure environment:&nbsp;</strong>Open file app.php in the folder 'web/config', and change 'http://localhost' to your domain URL (ex: http://www.mydomain.com) </p>
        <!--cannot use indentation inside pre because it will mass with code-prettify-->
        <pre class="prettyprint linenums:31">
/*
|--------------------------------------------------------------------------
| Application URL
|--------------------------------------------------------------------------
|
| This URL is used by the console to properly generate URLs when using
| the Artisan command line tool. You should set this to the root of
| your application so that it is used when running Artisan tasks.
|
*/

'url' => 'http://localhost',
        </pre>
        <p>	<strong>Step 3, Setup database:&nbsp;</strong>Open 'config/database.php' and choose your database connection, for example: 'sqlite' or 'mysql'
        <pre class="prettyprint linenums::18">
/*
|--------------------------------------------------------------------------
| Default Database Connection Name
|--------------------------------------------------------------------------
|
| Here you may specify which of the database connections below you wish
| to use as your default connection for all database work. Of course
| you may use many connections at once using the Database library.
|
*/

'default' => env('DB_CONNECTION', 'sqlite'),
        </pre>

        <p>Change database parameters to mach your database environment. For example:</p>

        <pre class="prettyprint linenums:55">
'mysql' => [
    'driver'    => 'mysql',
    'host'      => env('DB_HOST', 'localhost'),
    'database'  => env('DB_DATABASE', 'forge'),
    'username'  => env('DB_USERNAME', 'forge'),
    'password'  => env('DB_PASSWORD', ''),
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
    'strict'    => false,
],
        </pre>
        <p>if you use mySql and have Cpanel you can check this tutorial on <a href="{{asset('https://www.youtube.com/watch?v=nfM0xNwkAMA')}}" target="_blank"> how to create a MySql database</a></p>
        <p>	<strong>Step 4:&nbsp;</strong> Open 'config/mail.php' and configure your e-mail provider parameters. Auth uses this to send registration confirmation, resend passwords, etc.</p>
        <p>	<strong>Step 5:&nbsp;</strong>In 'config/app.php' include Checkauth service provider at the end of the providers list</p>
        <pre class="prettyprint linenums:124">
/*
|--------------------------------------------------------------------------
| Autoloaded Service Providers
|--------------------------------------------------------------------------
|
| The service providers listed here will be automatically loaded on the
| request to your application. Feel free to add your own services to
| this array to grant expanded functionality to your applications.
|
*/

'providers' => [
        ...
        Checkmate\CheckAuth\Providers\CheckAuthServiceProvider::class,
        </pre>
        <p>	<strong>Step 6, publish checkauth:</strong>Publish Checkauth's views, assets, config files and migrations into your laravels app. In your shell type:</p>
        <pre class="prettyprint">
php artisan checkauth:publish
        </pre>
        <p>	<strong>Step 7, optional database update (if you skip this on step 6) :</strong>If you chose to not update your database during step 6, you can update ir now using the above command:</p>
        <pre class="prettyprint">
php artisan migrate:refresh --seed
        </pre>
        <p>Follow the installation instructions untill the end.</p>
        <p>	<strong>Step 8, file permissions:&nbsp;</strong> You may need to configure some permissions. Directories within the storage and the bootstrap/cache directories should be writable by your web server or Laravel will not run.</p>
        <p>	<strong>Last step, Security: </strong> The next thing you should do after installing checkauth is set your application key to a random string. Typically, this string should be 32 characters long. The key can be set in the .env environment file located in 'web' folder. If your not familiar with Artisan command line 'php artisan key:generate', you can go on this site and generate a key <a href="{{asset('https://darkvoice.dyndns.org/wlankeygen/')}}" target="_blank">here.</a> Important! Character set has to be 0-9, A-Z, a-z (ASCII 48-57, 65-90, 97-122), Lenght 32. Then click generate button, then copy it to your file. <strong>If the application key is not set, your user sessions and other encrypted data will not be secure!</strong> </p>

    </section>

    <section id="configuration">
        <div class="page-header"><h3>Configuration</h3><hr class="notop"></div>
        <p>Auth have a config file located at 'config/checkauth.php', that allow tou to customize authentication options to suit your needs. You can see the options above:</p>

        <pre class="prettyprint linenums:5">
/*
|--------------------------------------------------------------------------
| Registration
|--------------------------------------------------------------------------
|
| Users can register on your app. If false, users will only be created for another logged user
|
*/

'registration' => true,

/*
|--------------------------------------------------------------------------
| Recapcha
|--------------------------------------------------------------------------
|
| Use recapcha on login form
|
*/

'recapcha' => true,

/*
|--------------------------------------------------------------------------
| Activation
|--------------------------------------------------------------------------
|
| By default, new accounts must be activated via email (exception made for
| oauth with facebook, google+, etc).  Setting this to
| false will allow users to login immediately after signing up.
|
*/

'require_activation' => true,


/*
|--------------------------------------------------------------------------
| Default User Roles
|--------------------------------------------------------------------------
|
| When a new user is created, they will automatically be added to the
| roles in this array.
|
*/

'default_user_roles' => ['Users'],


/*
|--------------------------------------------------------------------------
| Default Roles Permissions
|--------------------------------------------------------------------------
|
| Permission types allowed in you application
| roles in this array.
|
*/

'default_permissions' => ['users.list', 'users.edit', 'roles.edit'],

/*
|--------------------------------------------------------------------------
| Oath providers
|--------------------------------------------------------------------------
|
| Oauth authentication is not allowed in your application by default
| You can add an array of the providers you'll want to use.
| Auth supports: facebook, twitter, linkedin, google, github or bitbucket
| You can specify an array of the providers you want like that:
| ['facebook', 'twitter', 'linkedin', 'google', 'github', 'bitbucket']
*/

'oauth_providers' => ['facebook', 'google'],

/*
|--------------------------------------------------------------------------
| Default Oauth User Roles
|--------------------------------------------------------------------------
|
| If the client was registered by Oauth, they will automatically be added to the
| roles in this array.
|
*/

'default_oauth_user_roles' => null, //user will just be logged in, treated as a guest

/*
|--------------------------------------------------------------------------
| Remember oauth users after authenticate
|--------------------------------------------------------------------------
|
| By default, users authenticated through Oauth (social authentication like google) will be remembered.
|
*/

'remember_oauth' => true,

/*
|--------------------------------------------------------------------------
| Default oauth Roles Permissions
|--------------------------------------------------------------------------
|
| Permission types allowed in you application through Oauth (social authentication like google) will be remembered.
| roles in this array.
|
*/

'default_oauth_permissions' => null,

/*
|--------------------------------------------------------------------------
| Custom User Fields
|--------------------------------------------------------------------------
|
| If you want to add additional fields to your user model you can specify
| their validation needs here (can be alpha spaces or image).  You must update your db tables and add
| the fields to your 'create' and 'edit' views before this will work.
| value must be a key pair like: 'middle_name' => 'alpha'
|
*/

'additional_user_fields' => null,

/*
|--------------------------------------------------------------------------
| path for user profile images inside public folder
|--------------------------------------------------------------------------
|
*/

'imagePath' => 'img/avatars/',


/*
|--------------------------------------------------------------------------
| E-Mail Subject Lines
|--------------------------------------------------------------------------
|
| When using the "Eloquent" authentication driver, we need to know which
| Eloquent model should be used to retrieve your users. Of course, it
| is often just the "User" model but you may use whatever you like.
|
*/

'subjects' => [
'activate'        => 'Account Registration Confirmation',
'welcome'        => 'Account Registration Confirmed',
'reset_password' => 'Password Reset Confirmation'
],

/*
|--------------------------------------------------------------------------
| Default Routing
|--------------------------------------------------------------------------
|
| Auth provides default routes for its sessions, users and roles.
| You can use them as is, or you can disable them entirely.
|
*/
'routes_enabled' => true,

/*
|--------------------------------------------------------------------------
| URL Redirection for Method Completion
|--------------------------------------------------------------------------
|
| Upon completion of their tasks, controller methods will look-up their
| return destination here. You can specify a route, action or URL.
| If no action is specified a JSON response will be returned.
|
*/

'routing' => [
'session_store'                => ['route' => 'checkauth.app'],
'session_destroy'              => ['action' => '\\Checkmate\CheckAuth\Http\Controllers\SessionController@create'],
//'session_destroy'                => ['route' => 'checkauth.login'],
'registration_complete'        => ['route' => 'checkauth.app'],
'registration_activated'       => ['route' => 'checkauth.app'],
'registration_resend'          => ['route' => 'checkauth.app'],
'registration_reset_triggered' => ['route' => 'checkauth.app'],
'registration_reset_invalid'   => ['route' => 'checkauth.app'],
'registration_reset_complete'  => ['route' => 'checkauth.app'],
'users_invalid'                => ['route' => 'checkauth.app'],
'users_store'                  => ['route' => 'checkauth.users.index'],
'users_update'                 => ['route' => 'checkauth.users.show', 'parameters' => ['user' => 'hash']],
'users_destroy'                => ['route' => 'checkauth.users.index'],
'users_change_password'        => ['route' => 'checkauth.users.show', 'parameters' => ['user' => 'hash']],
'users_change_memberships'     => ['route' => 'checkauth.users.show', 'parameters' => ['user' => 'hash']],
'users_change_permissions'     => ['route' => 'checkauth.users.show', 'parameters' => ['user' => 'hash']],
'users_deactivated'             => ['route' => 'checkauth.users.index'],
'users_activated'               => ['route' => 'checkauth.users.index'],
'roles_store'                 => ['route' => 'checkauth.roles.index'],
'roles_update'                => ['route' => 'checkauth.roles.index'],
'roles_destroy'               => ['route' => 'checkauth.roles.index'],
'profile_change_password'      => ['route' => 'checkauth.profile.show'],
'profile_update'               => ['route' => 'checkauth.profile.show'],
],

/*
|--------------------------------------------------------------------------
| Guest Middleware Redirection
|--------------------------------------------------------------------------
|
| The SentinelGuest middleware will redirect users with active sessions to
| the route you specify here.  If left blank, the user will be taken
| to the home route.
|
*/

'redirect_if_authenticated' => 'checkauth.app',

/*
|--------------------------------------------------------------------------
| Enable HTML Views
|--------------------------------------------------------------------------
|
| There are situations in which you may not want to display any views
| when interacting with Auth.  To return JSON instead of HTML,
| turn this setting off. This cannot be done selectively.
|
*/
'views_enabled' => true,

/*
|--------------------------------------------------------------------------
| Email Views
|--------------------------------------------------------------------------
|
| String or array of views to use for emails
|
*/

'emails' => [
'views' => [
'welcome' => 'checkauth::emails.welcome',
'activate' => 'checkauth::emails.activate',
'reset' => 'checkauth::emails.reset'
]
]
    </pre>
    </section>
    <section id="authentication">
        <div class="page-header"><h3>Authentication</h3><hr class="notop"></div>
        <p>Your installation comes with the following default users:</p>
        <ul>
            <li><strong>Admin: </strong>login: admin@auth.com password:authadmin </li>
            <li><strong>User: </strong>login: user@auth.com password:authuser</li>
        </ul>
    </section>
    <section id="social">
        <div class="page-header"><h3>Social Authentication (Oauth)</h3><hr class="notop"></div>
        <p>To enable Oauth, the first thing you'll need to do is change in 'config/checkauth.php' and include one or more providers like this:</p>
        <pre class="prettyprint linenums:67">
/*
|--------------------------------------------------------------------------
| Oath providers
|--------------------------------------------------------------------------
|
| Oauth authentication is not allowed in your application by default
| You can add an array of the providers you'll want to use.
| CheckAuth supports: facebook, twitter, linkedin, google, github or bitbucket
| You can specify an array of the providers you want like that:
| ['facebook', 'twitter', 'linkedin', 'google', 'github', 'bitbucket']
*/

'oauth_providers' => ['facebook', 'google'],
        </pre>
        <p>You will also need to add credentials for the OAuth services your application utilizes. These credentials should be placed in your config/services.php configuration file, and should use the key facebook, twitter, linkedin, google, github or bitbucket, depending on the providers your application requires. For example:</p>
        <pre class="prettyprint">
'name-of-the-provider' => [
    'client_id' => 'your-github-app-id',
    'client_secret' => 'your-github-app-secret',
    'redirect' => '/oauth/callback/name-of-the-provider',
],
        </pre>
        <p>Google sample:</p>
        <pre class="prettyprint">
'google' => [
    'client_id' => 'your-github-app-id',
    'client_secret' => 'your-github-app-secret',
    'redirect' => '/oauth/callback/google',
],
        </pre>
        <h3>Oauth routes</h3>
        <p>Auth comes ready for Oauth, the routes above will do tue authentication work for you:</p>
        <pre class="prettyprint">
//Oauth Session Routes
Route::get('oauth/callback/{provider}', ['as' => 'checkauth.oauth.callback',
            'uses' => 'SessionController@handleProviderCallback']);
Route::get('oauth/{provider}', ['as' => 'checkauth.oauth.login',
            'uses' => 'SessionController@redirectToProvider']);
        </pre>
        <p><strong>Important:</strong> This new user will have no roles, if you need any role you'll have to provide-it via roles administrator card. </p>

    </section>
    <section id="roles">
        <div class="page-header"><h3>Roles and permissions</h3><hr class="notop"></div>
        <h3>Permissions</h3>
        <p>Permissions can be broken down into two types and two implementations. Depending on the used implementation, these permission types will behave differently.</p>
        <ul>
            <li>Role Permissions</li>
            <li>User Permissions</li>
        </ul>
        <p>
            <em>Standard</em>
            - This implementation will give the user-based permissions a higher priority and will override role-based permissions. Any permissions granted/rejected on the user will always take precendece over any role-based permissions assigned.
        </p>
        <p>
            <em>Strict</em>
            - This implementation will reject a permission as soon as one rejected permission is found on either the user or any of the assigned roles. Granting a user a permission that is rejected on a role he is assigned to will not grant that user this permission.
        </p>
        <p>Role-based permissions that define the same permission with different access rights will be rejected in case of any rejections on any role.</p>
        <p>If a user is not assigned a permission, the user will inherit permissions from the role. If a user is assigned a permission of false or true, then the user's permission will override the role permission.</p>
        <blockquote>
            <p>
                <strong>Important:</strong> The permission type is set to <strong>StandardPermissions</strong> by default. It can be changed on the <strong>config</strong>
                file located at 'config/checkauth.php'.
            </p>
        </blockquote>
    </section>
    <section id="routing">
        <div class="page-header"><h3>Route access</h3><hr class="notop"></div>
        <p>Auth uses <a href="{{asset('https://laravel.com/docs/5.2/middleware')}}" target="_blank">Laravel's Midleware</a> to handle route access. Routes are located in app/Http/routes.php. You can read more about routes in <a href="{{asset('https://laravel.com/docs/5.2/routing')}}" target="_blank">here</a>.</p>
        <p>You can grant access to some areas of your site by including inside a route::group a midleware directive like this for example:</p>
        <pre class="prettyprint">
//user authenticated and with roles.list permission
Route::group(['middleware' => ['sentinel.permission:roles.list']], function () {
    Route::get('users', ['as' => 'checkauth.users.index',
            'uses' => 'UserController@index']);
});
        </pre>
        <p>In the example above, you're telling laravel that everyone with roles.list permission can acess the route users (ex: www.yourdomain/users) and list all the users.</p>
        <h3>Availlable midleware options:</h3>
        <p>sentinel.auth -> all user logged in have access to it</p>
        <pre class="prettyprint">
//user authenticated
Route::group(['middleware' => ['sentinel.auth']], function () {
    //every route in here will be accessed by logged users only.
});
        </pre>
        <p>sentinel.role:role-name -> all user logged that got this role will have access to it.</p>
        <pre class="prettyprint">
//user authenticated with admin role
Route::group(['middleware' => ['sentinel.role:admin']], function () {
    //every route in here will be accessed by logged users
    //with administrator role only.
});
        </pre>
        <p>sentinel.permission:permission-name -> all user logged with that permission will have access to it..</p>
        <pre class="prettyprint">
//user authenticated and with roles.list permission
Route::group(['middleware' => ['sentinel.permission:user.edit']], function () {
    //every route in here will be accessed by users with permission 'user.edit'
});
        </pre>
        <h3>Authentication routes:</h3>
        <p>Auth comes with the predetermined routes above:</p>
        <pre class="prettyprint linenums:11">
Route::group(['namespace' => 'Checkmate\CheckAuth\Http\Controllers', 'middleware' => ['web']], function () {

    //sample app homepage
    Route::get('checkauth-app', ['as' => 'checkauth.app', function () {
        return view('checkauth::app');
    }]);

    Route::get('documentation/checkauth', ['as' => 'checkauth.documentation', function () {
        return view('checkauth::documentation');
    }]);

    //Oauth Session Routes
    Route::get('oauth/callback/{provider}', ['as' => 'checkauth.oauth.callback', 'uses' => 'SessionController@handleProviderCallback']);
    Route::get('oauth/{provider}', ['as' => 'checkauth.oauth.login', 'uses' => 'SessionController@redirectToProvider']);

    //Session Routes
    Route::get('login', ['as' => 'checkauth.login', 'uses' => 'SessionController@create']);
    Route::get('logout', ['as' => 'checkauth.logout', 'uses' => 'SessionController@destroy']);
    Route::get('sessions/create', ['as' => 'checkauth.session.create', 'uses' => 'SessionController@create']);
    Route::post('sessions/store', ['as' => 'checkauth.session.store', 'uses' => 'SessionController@store']);
    Route::delete('sessions/destroy', ['as' => 'checkauth.session.destroy', 'uses' => 'SessionController@destroy']);

    //Registration
    Route::get('register', ['as' => 'checkauth.register.form', 'uses' => 'RegistrationController@registration']);
    Route::post('register', ['as' => 'checkauth.register.user', 'uses' => 'RegistrationController@register']);
    Route::get('users/activate/{hash}/{code}', ['as' => 'checkauth.activate', 'uses' => 'RegistrationController@activate']);
    Route::get('reactivate', ['as' => 'checkauth.reactivate.form', 'uses' => 'RegistrationController@resendActivationForm']);
    Route::post('reactivate', ['as' => 'checkauth.reactivate.send', 'uses' => 'RegistrationController@resendActivation']);
    Route::get('forgot', ['as' => 'checkauth.forgot.form', 'uses' => 'RegistrationController@forgotPasswordForm']);
    Route::post('forgot', ['as' => 'checkauth.reset.request', 'uses' => 'RegistrationController@sendResetPasswordEmail']);
    Route::get('reset/{hash}/{code}', ['as' => 'checkauth.reset.form', 'uses' => 'RegistrationController@passwordResetForm']);
    Route::post('reset/{hash}/{code}', ['as' => 'checkauth.reset.password', 'uses' => 'RegistrationController@resetPassword']);


    Route::group(['middleware' => ['sentinel.auth']], function () {//user authenticated
        //Profile
        Route::get('profile', ['as' => 'checkauth.profile.show', 'uses' => 'ProfileController@show']);
        Route::get('profile/edit', ['as' => 'checkauth.profile.edit', 'uses' => 'ProfileController@edit']);
        Route::put('profile', ['as' => 'checkauth.profile.update', 'uses' => 'ProfileController@update']);
        Route::post('profile/password', ['as' => 'checkauth.profile.password', 'uses' => 'ProfileController@changePassword']);
    });

    Route::group(['middleware' => ['sentinel.permission:users.list']], function () {//user authenticated and with user.list permission
        //List Users
        Route::get('users', ['as' => 'checkauth.users.index', 'uses' => 'UserController@index']);
    });


    Route::group(['middleware' => ['sentinel.permission:users.edit']], function () {//user authenticated and with user.edit permission

        //Edit Users
        Route::get('users/create', ['as' => 'checkauth.users.create', 'uses' => 'UserController@create']);
        Route::post('users', ['as' => 'checkauth.users.insert', 'uses' => 'UserController@insert']);
        Route::get('users/{hash}', ['as' => 'checkauth.users.show', 'uses' => 'UserController@show']);
        Route::get('users/{hash}/edit', ['as' => 'checkauth.users.edit', 'uses' => 'UserController@edit']);
        Route::post('users/{hash}/permissions', ['as' => 'checkauth.users.permissions', 'uses' => 'UserController@updatePermissions']);
        Route::post('users/{hash}/password', ['as' => 'checkauth.password.change', 'uses' => 'UserController@changePassword']);
        Route::post('users/{hash}/memberships', ['as' => 'checkauth.users.memberships', 'uses' => 'UserController@updateRoleMemberships']);
        Route::put('users/{hash}', ['as' => 'checkauth.users.update', 'uses' => 'UserController@update']);
        Route::delete('users/{hash}', ['as' => 'checkauth.users.destroy', 'uses' => 'UserController@destroy']);
        Route::get('users/{hash}/deactivate', ['as' => 'checkauth.users.deactivate', 'uses' => 'UserController@deactivateUser']);
        Route::get('users/{hash}/activate', ['as' => 'checkauth.users.activate', 'uses' => 'UserController@activateUser']);

    });

    Route::group(['middleware' => ['sentinel.permission:roles.edit']], function () {//user authenticated and with roles.edit permission
        // Sentinel Roles
        Route::get('roles', ['as' => 'checkauth.roles.index', 'uses' => 'RoleController@index']);
        Route::get('roles/create', ['as' => 'checkauth.roles.create', 'uses' => 'RoleController@create']);
        Route::post('roles', ['as' => 'checkauth.roles.store', 'uses' => 'RoleController@store']);
        Route::get('roles/{hash}', ['as' => 'checkauth.roles.show', 'uses' => 'RoleController@show']);
        Route::get('roles/{hash}/edit', ['as' => 'checkauth.roles.edit', 'uses' => 'RoleController@edit']);
        Route::put('roles/{hash}', ['as' => 'checkauth.roles.update', 'uses' => 'RoleController@update']);
        Route::delete('roles/{hash}', ['as' => 'checkauth.roles.destroy', 'uses' => 'RoleController@destroy']);
    });

});
        </pre>

    </section>
    <section id="views">
        <div class="page-header"><h3>Views access</h3><hr class="notop"></div>
        <p>You can choose what to show in your views depending on session status, roles and permissions.</p>
        <p>Check examples bellow:</p>
        <h3>CheckAuth::getUser()</h3>
        <p>This method will check if user is logged in.</p>
        <p>It can return the user object so you can retrieve user information.</p>
        <pre class="prettyprint">
if($user = CheckAuth::getUser()){
    //only authenticated users can view this
    //you can use $user to retrieve any fields in user table
}else{
    //unauthenticated access code
}
        </pre>
        <h3>CheckAuth::inRole</h3>
        <p>Check if the current user belongs to the given role.</p>
        <pre class="prettyprint">
if($user = CheckAuth::inRole('admins')){
    //only users with 'admins' role can view this
}
        </pre>
        <h3>hasAccess</h3>
        <p>This method will strictly require all passed permissions to be true in order to grant access.</p>
        <p>This test will require both user.create and user.update to be true in order for permissions to be granted.</p>
        <pre class="prettyprint">
$user = CheckAuth::getUser();

if ($user->hasAccess(['user.create', 'user.update']))
{
    // Execute this code if the user has permission
}
else
{
    // Execute this code if the permission check failed
}
        </pre>
        <h3>CheckAuth::hasAnyAccess</h3>
        <p>This method will grant access if any permission passes the check.</p>
        <p>This test will require only one permission of user.admin and user.create to be true in order for permissions to be granted.</p>
        <pre class="prettyprint">
if (CheckAuth::hasAnyAccess(['user.admin', 'user.update']))
{
    // Execute this code if the user has permission
}
else
{
    // Execute this code if the permission check failed
}
        </pre>
        <h3>Wildcard Checks</h3>
        <p>Permissions can be checked based on wildcards using the * character to match any of a set of permissions.</p>
        <pre class="prettyprint">
if (CheckAuth::hasAnyAccess(['user.admin', 'user.update']))
{
    // Execute this code if the user has permission
}
else
{
    // Execute this code if the permission check failed
}
        </pre>
    </section>
    <section id="changelog">
        <div class="page-header"><h3>Changelog</h3><hr class="notop"></div>
        <p>
            <strong>v1.0 (01/05/2016) :</strong></p>
        <ul>
            <li>
                initial release</li>
        </ul>
    </section>
    <section id="support">
        <div class="page-header"><h3>Support</h3><hr class="notop"></div>
        <p>	If you need any help regarding the installation of the script please create a support&nbsp; ticket from here with help topic &quot;CodeCanyon Support &quot;</p>
        <p>	<a href="{{asset('http://support.checkmatedigital.com/checkauth')}}" target="_blank">Checkmate Support Center</a></p>
        <p>	Our tech support team will get back to you within the nex 48 hours.</p>
    </section>

    <section id="license">
        <div class="license_list">
            <div class="page-header"><h3>License</h3><hr class="notop"></div>
            <p>When you buy this project (or item) you're agreeing with the license above:</p>
            <h3>General terms</h3>
            <ul>
                <li>
                    <p><span >1.</span>This license grants you, the purchaser, an ongoing, non-exclusive, worldwide license to make use of the digital work you have purchased. Read the rest of this license for the details that apply to your use of the Item.</p>
                </li>
                <li>
                    <p><span>2.</span>You are licensed to use the Item to create one single End Product for yourself or for one client (a "single application"), and the End Product can be distributed for Free.</p>
                </li>
                <li>
                    <p><span>3.</span>An End Product is one of the following things, both requiring an application of skill and effort.</p>

                    <ul>
                        <li>
                            <p><span>(a)</span>the End Product is a customised implementation of this project.</p>
                        </li>
                        <li>
                            <p><span>(b)</span> the End Product is a work that incorporates the Item as well as other things, so that it is larger in scope and different in nature than the Item.</p>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <h3>Things you can do with this item</h3>

        <ul>
            <li>
                <p><span>4.</span>You can create one End Product for a client, and you can transfer that single End Product to your client for any fee. This license is then transferred to your client.</p>
            </li>
            <li>
                <p><span>5.</span>You can modify or manipulate the Item. You can combine the Item with other works and make a derivative work from it. The resulting works are subject to the terms of this license. You can do these things as long as the End Product you then create is one that's permitted under clause 3.</p>
            </li>
        </ul>

        <h3>Things you can&#39;t do with the item</h3>

        <ul>
            <li>
                <p><span>6.</span>You can't Sell the End Product, except to one client. (If you or your client want to Sell the End Product, you will need the Extended License.)</p>
            </li>
            <li>
                <p><span>7.</span>You can't re-distribute the Item as stock, in a tool or template, or with source files. You can't do this with an Item either on its own or bundled with other items, and even if you modify the Item. You can't re-distribute or make available the Item as-is or with superficial modifications. These things are not allowed even if the re-distribution is for Free.</p>
                <p class="license-bubble--below">For example: You can't purchase an HTML template, convert it to a WordPress theme and sell or give it to more than one client. You can't license an item and then make it available as-is on your website for your users to download.</p>
            </li>
            <li>
                <p><span>8.</span>You can't use the Item in any application allowing an end user to customise a digital or physical product to their specific needs, such as an "on demand", "made to order" or "build it yourself" application. You can use the Item in this way only if you purchase a separate license for each final product incorporating the Item that is created using the application.</p>

                <p class="license-bubble--below">Examples of "on demand", "made to order" or "build it yourself" applications: website builders, "create your own" slideshow apps, and e-card generators.</p>
            </li>
            <li>
                <p><span>9.</span>Although you can modify the Item and therefore delete unwanted components before creating your single End Product, you can't extract and use a single component of an Item on a stand-alone basis.</p>

                <p class="license-bubble--below">For example: You license a website theme containing icons. You can delete unwanted icons from the theme. But you can&#39;t extract an icon to use outside of the theme.</p>
            </li>
            <li>
                <p><span>10.</span>You must not permit an end user of the End Product to extract the Item and use it separately from the End Product.</p>
            </li>
            <li>
                <p><span>11.</span>You can't use an Item in a logo, trademark, or service mark.</p>
            </li>
        </ul>

        <h3>Other license terms</h3>

        <ul>

            <li>
                <p><span>12.</span>You can only use the Item for lawful purposes. Also, if an Item contains an image of a person, even if the Item is model-released you can't use it in a way that creates a fake identity, implies personal endorsement of a product by the person, or in a way that is defamatory, obscene or demeaning, or in connection with sensitive subjects.</p>
            </li>
            <li>
                <p><span>16.</span>Items that contain digital versions of real products, trademarks or other intellectual property owned by others have not been property released. These Items are licensed on the basis of editorial use only. It is your responsibility to consider whether your use of these Items requires a clearance and if so, to obtain that clearance from the intellectual property rights owner.</p>
            </li>
            <li>
                <p><span>17.</span>This license applies in conjunction with the <a href="http://codecanyon.net/legal/market">Envato Market Terms</a> for your use of Envato Market. If there is an inconsistency between this license and the Envato Market Terms, this license will apply to the extent necessary to resolve the inconsistency.</p>
            </li>
            <li>
                <p><span>18.</span>This license can be terminated if you breach it. If that happens, you must stop making copies of or distributing the End Product until you remove the Item from it.</p>
            </li>
            <li>
                <p><span>19.</span>The author of the Item retains ownership of the Item but grants you the license on these terms. This license is between the author of the Item and you.</p>
            </li>

        </ul>

        <h3>Credits</h3>

        <p>We built Checkauth using the great rydurham/Sentinel project as a reference and with the help of other great gitHub projects above (Thanks for all!):</p>

        <h3>MIT License:</h3>

        <ul>
            <li>laravel/framework</li>
            <li>bootstrap/bootstrap</li>
            <li>vinkla/hashids</li>
            <li>laravel/socialite</li>
            <li>intervention/image</li>
            <li>anhskohbo/no-captcha</li>
            <li>fzaninotto/faker</li>
            <li>mockery/mockery</li>
            <li>phpunit/phpunit</li>
            <li>symfony/css-selector</li>
            <li>symfony/dom-crawler</li>
            <li>orchestra/testbench</li>
            <li>FortAwesome/Font-Awesome</li>
            <li>1000hz/bootstrap-validator</li>
            <li>driftyco/ionicons</li>
            <li>jasny/bootstrap</li>
        </ul>

        <h3>BSD3 License:</h3>
        <ul>
            <li>cartalyst/sentinel &copy;  2011-2015 Cartalyst LLC, All rights reserved.</li>
        </ul>

    </section>

</div>

</body>
</html>