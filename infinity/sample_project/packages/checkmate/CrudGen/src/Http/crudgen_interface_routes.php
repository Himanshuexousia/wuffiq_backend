<?php

/*
 * Important!!!
 * Laravel 5.2 notice:
 * Any routes not placed within the web middleware group will not have access to sessions and CSRF protection,
 * so make sure any routes that need these features are placed within the group.
 * Typically, you will place most of your routes within this group:
 */

Route::group(['namespace' => 'Checkmate\CrudGen\Http\Controllers', 'middleware' => ['web', config('crudgen.middleware')]], function () {

    Route::get(config('crudgen.prefix').'/crudgen/generator', ['as' => 'crudgen.show', 'uses' => 'GeneratorController@show']);
    Route::post(config('crudgen.prefix').'/crudgen/generator', ['as' => 'crudgen.generate', 'uses' => 'GeneratorController@generate']);

});

Route::group(['namespace' => 'Checkmate\CrudGen\Http\Controllers', 'middleware' => ['web']], function () {

    Route::get('documentation/crudgen', ['as' => 'crudgen.documentation', function()
    {
        return view('crudgen::documentation');
    }]);

});