<?php namespace Checkmate\CrudGen\Commands;

use File;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class RouteCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature  = 'crud:route
                        {name : The name of the migration.}
                        {--middleware= : Middleware of the controller.}
                        {--namespace= : Namespace of the controller.}
                        {--route-group= : Prefix of the route group.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Append crud name to crud_routes.php file';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $crudName = strtolower($this->argument('name'));
        $crudNamePlural = str_plural($crudName);
        $crudNamePluralCap = ucwords($crudNamePlural);
        //$crudController = $crudNamePluralCap."Controller";
        $crudController = $name."Controller";
        $path = app_path('Http/crud_routes.php');
        $content = "";

        if($this->option('middleware')){
            $middleware = $this->option('middleware');
        }else{
            $middleware = config('crudgen.middleware');
        }

        $namespace = $this->option('namespace');

        if($this->option('route-group')){
            $prefix = $this->option('route-group');
        }else{
            $prefix = config('crudgen.prefix');
        }


        //determine laravel version to add web middleware for 5.2+
        if (\App::VERSION() >= '5.2') {
            $content .= "Route::group(['prefix' => '".$prefix."', 'namespace' => '".$namespace."', 'middleware' => ['web','".$middleware."']], function () {\n";
        } else {
            $content .= "Route::group([('prefix' => '".$prefix."', 'namespace' => '".$namespace."', 'middleware' => '".$middleware."'], function () {\n";
        }

        $content .= "\tRoute::resource('$crudName', '$crudController');\n";

        if($prefix){
            $content .= "\tRoute::get('$crudName/{id}/delete', array('as' => '$prefix.$crudName.delete', 'uses' => '$crudController@getDelete'));\n";
            $content .= "\tRoute::get('$crudName/{id}/confirm-delete', array('as' => '$prefix.$crudName.confirm-delete', 'uses' => '$crudController@getModalDelete'));\n";
        }else{
            $content .= "\tRoute::get('$crudName/{id}/delete', array('as' => '$prefix.$crudName.delete', 'uses' => '$crudController@getDelete'));\n";
            $content .= "\tRoute::get('$crudName/{id}/confirm-delete', array('as' => '$prefix.$crudName.confirm-delete', 'uses' => '$crudController@getModalDelete'));\n";
        }


        $content .= "});";

        $bytesWritten = File::append($path, $content);
        if ($bytesWritten === false)
        {
            die("Couldn't write to the file.");
        }

        $this->info('Routes added to crud_routes.php successfully.');

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'Name of the Crud.'],
        ];
    }

    /*
     * Get the console command options.
     *
     * @return array
     */

    protected function getOptions()
    {
        return [
            ['fields', null, InputOption::VALUE_OPTIONAL, 'The fields of the form.', null],
        ];
    }

}
