<?php
namespace Checkmate\CrudGen\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Filesystem\Filesystem;


/**
 * Command class for CrudGen
 *
 * @license http://www.checkmatedigital.com/licenses/crudgen
 *
 * @version 1.0.0
 */
class PublishCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crudgen:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish views, routes, config file and public asset for CrudGen';

    /**
     * The base path of the parent application
     *
     * @var string
     */
    private $appPath;

    /**
     * The path to the CrudGen's src directory
     *
     * @var string
     */
    private $packagePath;

    /**
     * Create a new command instance.
     *
     * @param Filesystem $file
     */
    public function __construct(Filesystem $file)
    {
        parent::__construct();

        // DI Member Assignment
        $this->file = $file;

        // Set Application Path
        $this->appPath = app_path();

        // Set the path to the  Package namespace root
        $this->packagePath = __DIR__ . '/../..';
    }

    /**
     * This trait allows us to easily check the current environment
     */
    use ConfirmableTrait;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // warn the user if the app is in production!
        if (!$this->confirmToProceed('Application In Production!')) {
            return;
        }

        $this->publishConfig();

        $this->publishViews();

        $this->publishAssets();

        $this->publishResources();

        $this->publishPublic();


        $this->info('Crudgen is now ready to use!');
    }



    /**
     * Publish the Config file
     */
    private function publishConfig()
    {

        $source      = $this->packagePath . '/src/config/crudgen.php';
        $destination = config_path('crudgen.php');

        if ($this->file->isFile($destination)) {
            $answer = $this->confirm('CrudGen config has already been published. Do you want to overwrite?');

            if (!$answer) {
                return;
            }
        }

        $this->file->copy($source, $destination);

        $this->info('CrudGen configuration file published.');
    }


    /**
     * Publish the Views
     */
    private function publishViews()
    {

        $source      = $this->packagePath . '/resources/views/';
        $destination = base_path() . '/resources/views/vendor/crudgen';

        if ($this->file->isDirectory($destination)) {
            $answer = $this->confirm('Viewa have already been published. Do you want to overwrite?');

            if (!$answer) {
                return;
            }
        }

        $this->file->copyDirectory($source, $destination);

        $this->info('Views published.');
    }

    /**
     * Publish the assets needed for a specified theme.
     */
    private function publishAssets()
    {

        $source      = $this->packagePath . '/resources/EnumTrait.php';
        $destination = app_path('EnumTrait.php');

        if ($this->file->isFile($destination)) {
            $answer = $this->confirm('EnumTrait.php have already been published at laravel app folder. File will be overwrited, Do you want to proceed?');

            if (!$answer) {
                return;
            }
        }

        $this->file->copy($source, $destination);

        $this->info('Assets published.');
    }

    /**
     * Publish language files
     */
    private function publishResources()
    {

        $source      = $this->packagePath . '/src/stubs/';
        $destination = base_path('resources/vendor/crudgen/stubs/');

        if ($this->file->isDirectory($destination)) {
            $answer = $this->confirm('Resources have already been published in resources/vendor/crudgen folder. Do you want to overwrite?');

            if (!$answer) {
                return;
            }
        }

        $this->file->copyDirectory($source, $destination);

        $this->info('CheckAuth language files published.');
    }

    /**
     * Publish Public files
     */
    private function publishPublic()
    {

        $source      = $this->packagePath . '/resources/css/';
        $destination = public_path('vendor/crudgen/css/');
        if ($this->file->isDirectory($destination)) {
            $answer = $this->confirm('css folder have already been published in public/vendor/crudgen/css folder. Do you want to overwrite?');

            if (!$answer) {
                return;
            }
        }
        $this->file->copyDirectory($source, $destination);

        $source      = $this->packagePath . '/resources/js/';
        $destination = public_path('vendor/crudgen/js/');
        if ($this->file->isDirectory($destination)) {
            $answer = $this->confirm('js folder have already been published in public/vendor/crudgen/js folder. Do you want to overwrite?');

            if (!$answer) {
                return;
            }
        }
        $this->file->copyDirectory($source, $destination);

        $source      = $this->packagePath . '/resources/images/';
        $destination = public_path('vendor/crudgen/images/');
        if ($this->file->isDirectory($destination)) {
            $answer = $this->confirm('images folder have already been published in public/vendor/crudgen/images folder. Do you want to overwrite?');

            if (!$answer) {
                return;
            }
        }
        $this->file->copyDirectory($source, $destination);

        $this->info('CheckAuth language files published.');
    }


}