var elixir = require('laravel-elixir');
var gulp = require('gulp');
var lessToScss = require('gulp-less-to-scss');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify-css');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    //mix.scriptsIn("public/js/some/directory")
    mix.sass('front-end/front-end.scss').sass('infinity/infinity.scss');
    //mix.sass('infinity/infinity.scss');
    //mix.sass('main.scss');
});


gulp.task('dist-js-front', function(){
    return gulp.src([
        'public/vendor/jquery/jquery.min.js',
        'public/vendor/jquery-ui/jquery-ui.min.js',
        'public/vendor/tether/js/tether.min.js',
        'public/vendor/bootstrap/js/bootstrap.min.js',
        'public/vendor/responsive-img.js/responsive-img.min.js',
        'public/js/jquery.easing.min.js',
        'public/vendor/superslides/jquery.superslides.min.js',
        'public/vendor/magnific-popup/jquery.magnific-popup.min.js',
        'public/js/jquery.fittext.js',
        'public/js/scrollPosStyler.js',
        'public/js/jquery.scrollie.min.js',
        'public/js/front-end.js'
    ]).pipe(concat('concat.js'))
      .pipe(gulp.dest('public/dist'))
      .pipe(rename('main-front.js'))
      .pipe(uglify())
      .pipe(gulp.dest('public/dist'));
});

gulp.task('dist-css-front', function(){
    return gulp.src([
        'public/vendor/normalize-css/normalize.css',
        'public/vendor/bootstrap/css/bootstrap.min.css',
        'public/vendor/font-awesome/css/font-awesome.min.css',
        'public/vendor/animate.css/animate.min.css',
        'public/vendor/superslides/stylesheets/superslides.css',
        'public/vendor/magnific-popup/magnific-popup.css',
        'public/css/front-end.css'
    ]).pipe(concat('concat.css'))
        .pipe(gulp.dest('public/dist'))
        .pipe(rename('main-front.css'))
        .pipe(minify())
        .pipe(gulp.dest('public/dist'));
});

gulp.task('dist-js-infinity', function(){
    return gulp.src([
        'public/vendor/jquery/jquery.min.js',
        'public/vendor/jquery-ui/jquery-ui.min.js',
        'public/vendor/tether/js/tether.min.js',
        'public/vendor/bootstrap/js/bootstrap.min.js',
        'public/vendor/responsive-img.js/responsive-img.min.js',
        'public/js/jquery.easing.min.js',
        'public/vendor/superslides/jquery.superslides.min.js',
        'public/vendor/magnific-popup/jquery.magnific-popup.min.js',
        'public/js/jquery.fittext.js',
        'public/js/scrollPosStyler.js',
        'public/js/jquery.scrollie.min.js',
        'public/js/infinity.js'
    ]).pipe(concat('concat.js'))
        .pipe(gulp.dest('public/dist'))
        .pipe(rename('main-infinity.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/dist'));
});

gulp.task('dist-css-infinity', function(){
    return gulp.src([
        'public/vendor/normalize-css/normalize.css',
        'public/vendor/bootstrap/css/bootstrap.min.css',
        'public/vendor/font-awesome/css/font-awesome.min.css',
        'public/vendor/animate.css/animate.min.css',
        'public/vendor/superslides/stylesheets/superslides.css',
        'public/vendor/magnific-popup/magnific-popup.css',
        'public/css/infinity.css'
    ]).pipe(concat('concat.css'))
        .pipe(gulp.dest('public/dist'))
        .pipe(rename('main-infinity.css'))
        .pipe(minify())
        .pipe(gulp.dest('public/dist'));
});

//run> gulp copy-dist
gulp.task('copy-dist', function() {
    gulp.src(['resources/vendor/bootstrap/dist/**/*']).pipe(gulp.dest('public/vendor/bootstrap'));//**/* will not flatten folder structure
    gulp.src(['resources/vendor/jquery/dist/**/*']).pipe(gulp.dest('public/vendor/jquery'));
    gulp.src(['resources/vendor/tether/dist/**/*']).pipe(gulp.dest('public/vendor/tether'));
    gulp.src(['resources/vendor/jquery-ui/jquery-ui.min.js']).pipe(gulp.dest('public/vendor/jquery-ui'));
    gulp.src(['resources/vendor/jquery-ui/jquery-ui.js']).pipe(gulp.dest('public/vendor/jquery-ui'));
    gulp.src(['resources/vendor/font-awesome/css/*']).pipe(gulp.dest('public/vendor/font-awesome/css'));//* will copy all files in the folder
    gulp.src(['resources/vendor/font-awesome/fonts/*']).pipe(gulp.dest('public/vendor/font-awesome/fonts'));
    gulp.src(['resources/vendor/slimscroll/*.js']).pipe(gulp.dest('public/vendor/slimscroll'));
    gulp.src(['resources/vendor/fastclick/lib/*.js']).pipe(gulp.dest('public/vendor/fastclick'));
    gulp.src(['resources/vendor/uikit/css/**/*']).pipe(gulp.dest('public/vendor/uikit/css'));
    gulp.src(['resources/vendor/uikit/fonts/**/*']).pipe(gulp.dest('public/vendor/uikit/fonts'));
    gulp.src(['resources/vendor/uikit/js/**/*']).pipe(gulp.dest('public/vendor/uikit/js'));
    gulp.src(['resources/vendor/Ionicons/css/**/*']).pipe(gulp.dest('public/vendor/ionicons/css'));
    gulp.src(['resources/vendor/Ionicons/fonts/**/*']).pipe(gulp.dest('public/vendor/ionicons/fonts'));
    gulp.src(['resources/vendor/Ionicons/png/**/*']).pipe(gulp.dest('public/vendor/ionicons/png'));
    gulp.src(['resources/vendor/Ionicons/src/**/*']).pipe(gulp.dest('public/vendor/ionicons/src'));
    gulp.src(['resources/vendor/seiyria-bootstrap-slider/dist/*.js']).pipe(gulp.dest('public/vendor/bootstrap-slider/js'));
    gulp.src(['resources/vendor/seiyria-bootstrap-slider/dist/css/**/*']).pipe(gulp.dest('public/vendor/bootstrap-slider/css'));
    gulp.src(['resources/vendor/animate.css/*']).pipe(gulp.dest('public/vendor/animate.css'));
    gulp.src(['resources/assets/fonts/bootstrap/**/*']).pipe(gulp.dest('public/fonts/bootstrap'));
    gulp.src(['resources/vendor/Chart.js/Chart.js']).pipe(gulp.dest('public/vendor/chart-js'));
    gulp.src(['resources/vendor/Chart.js/Chart.min.js']).pipe(gulp.dest('public/vendor/chart-js'));
    gulp.src(['resources/vendor/peity/jquery.peity.js']).pipe(gulp.dest('public/vendor/peity'));
    gulp.src(['resources/vendor/peity/jquery.peity.min.js']).pipe(gulp.dest('public/vendor/peity'));
    gulp.src(['resources/vendor/datatables.net/js/*']).pipe(gulp.dest('public/vendor/datatable/js')); // fazer todos os includes do datatable
    gulp.src(['resources/vendor/datatables.net-dt/css/*']).pipe(gulp.dest('public/vendor/datatable/css'));
    gulp.src(['resources/vendor/datatables.net-dt/images/*']).pipe(gulp.dest('public/vendor/datatable/img'));
    gulp.src(['resources/vendor/datatables.net-responsive/js/*']).pipe(gulp.dest('public/vendor/datatable/plugins/responsive/js/'));
    gulp.src(['resources/vendor/datatables.net-responsive-dt/css/*']).pipe(gulp.dest('public/vendor/datatable/plugins/responsive/css'));
    gulp.src(['resources/vendor/bootstrap3-wysihtml5-bower/dist/**/*']).pipe(gulp.dest('public/vendor/bootstrap3-wysihtml5'));
    gulp.src(['resources/vendor/jquery-knob/dist/**/*']).pipe(gulp.dest('public/vendor/jquery-knob'));
    gulp.src(['resources/vendor/bower-jvectormap/**/*']).pipe(gulp.dest('public/vendor/jvectormap'));
    gulp.src(['resources/vendor/morris.js/morris.min.js']).pipe(gulp.dest('public/vendor/morris.js'));
    gulp.src(['resources/vendor/morris.js/morris.css']).pipe(gulp.dest('public/vendor/morris.js'));
    gulp.src(['resources/vendor/bootstrap-datepicker/dist/**/*']).pipe(gulp.dest('public/vendor/bootstrap-datepicker'));
    gulp.src(['resources/vendor/raphael/raphael-min.js']).pipe(gulp.dest('public/vendor/raphael'));
    gulp.src(['resources/vendor/bootstrap-daterangepicker/daterangepicker.js']).pipe(gulp.dest('public/vendor/bootstrap-daterangepicker'));
    gulp.src(['resources/vendor/bootstrap-daterangepicker/daterangepicker.css']).pipe(gulp.dest('public/vendor/bootstrap-daterangepicker'));
    gulp.src(['resources/vendor/jq-fullscreen/release/jquery.fullscreen.min.js']).pipe(gulp.dest('public/vendor/jq-fullscreen'));
    gulp.src(['resources/vendor/jquery-countTo/jquery.countTo.js']).pipe(gulp.dest('public/vendor/jquery-countTo'));
    gulp.src(['resources/vendor/css-element-queries/src/**/*']).pipe(gulp.dest('public/vendor/css-element-queries'));
    gulp.src(['resources/vendor/superslides/dist/**/*']).pipe(gulp.dest('public/vendor/superslides'));
    gulp.src(['resources/vendor/normalize-css/normalize.css']).pipe(gulp.dest('public/vendor/normalize-css'));
    gulp.src(['resources/vendor/iCheck/skins/**/*']).pipe(gulp.dest('public/vendor/icheck'));
    gulp.src(['resources/vendor/iCheck/**/*.js']).pipe(gulp.dest('public/vendor/icheck'));
    gulp.src(['resources/vendor/jasny-bootstrap/dist/**/*']).pipe(gulp.dest('public/vendor/jasny-bootstrap'));
    gulp.src(['resources/vendor/jquery.scrollTo/jquery.scrollTo.min.js']).pipe(gulp.dest('public/vendor/jquery.scrollTo'));
    gulp.src(['resources/vendor/jquery.easing/js/jquery.easing.min.js']).pipe(gulp.dest('public/vendor/jquery.easing'));
    gulp.src(['resources/vendor/jquery.stellar/src/**/*']).pipe(gulp.dest('public/vendor/jquery.stellar'));
    gulp.src(['resources/vendor/magnific-popup/dist/**/*']).pipe(gulp.dest('public/vendor/magnific-popup'));
    gulp.src(['resources/vendor/responsive-img.js/responsive-img.min.js']).pipe(gulp.dest('public/vendor/responsive-img.js'));
});

//run> gulp copy-assets
gulp.task('copy-assets', function() {
    gulp.src(['resources/vendor/bootstrap/scss/**/*']).pipe(gulp.dest('resources/assets/sass/vendor/bootstrap'));//**/* will not flatten folder structure
});


gulp.task('lessToScss',function(){
    gulp.src('resources/assets/less/**/*')
        .pipe(lessToScss())
        .pipe(gulp.dest('resources/assets/sass/front-end'));
});