<?php

/*
 * Checkauth advanced authentication routes (do not remove this file)
 */

Route::group(['namespace' => 'Checkmate\CheckAuth\Http\Controllers', 'middleware' => ['web']], function () {

    //sample app homepage
    Route::get('checkauth-app', ['as' => 'checkauth.app', function () {
        return view('checkauth::app');
    }]);

    Route::get('documentation/checkauth', ['as' => 'checkauth.documentation', function () {
        return view('checkauth::documentation');
    }]);

    //Oauth Session Routes
    Route::get('oauth/callback/{provider}', ['as' => 'checkauth.oauth.callback', 'uses' => 'SessionController@handleProviderCallback']);
    Route::get('oauth/{provider}', ['as' => 'checkauth.oauth.login', 'uses' => 'SessionController@redirectToProvider']);

    //Session Routes
    Route::get('login', ['as' => 'checkauth.login', 'uses' => 'SessionController@create']);
    Route::get('logout', ['as' => 'checkauth.logout', 'uses' => 'SessionController@destroy']);
    Route::get('sessions/create', ['as' => 'checkauth.session.create', 'uses' => 'SessionController@create']);
    Route::post('sessions/store', ['as' => 'checkauth.session.store', 'uses' => 'SessionController@store']);
    Route::delete('sessions/destroy', ['as' => 'checkauth.session.destroy', 'uses' => 'SessionController@destroy']);

    //Registration
    Route::get('register', ['as' => 'checkauth.register.form', 'uses' => 'RegistrationController@registration']);
    Route::post('register', ['as' => 'checkauth.register.user', 'uses' => 'RegistrationController@register']);
    Route::get('users/activate/{hash}/{code}', ['as' => 'checkauth.activate', 'uses' => 'RegistrationController@activate']);
    Route::get('reactivate', ['as' => 'checkauth.reactivate.form', 'uses' => 'RegistrationController@resendActivationForm']);
    Route::post('reactivate', ['as' => 'checkauth.reactivate.send', 'uses' => 'RegistrationController@resendActivation']);
    Route::get('forgot', ['as' => 'checkauth.forgot.form', 'uses' => 'RegistrationController@forgotPasswordForm']);
    Route::post('forgot', ['as' => 'checkauth.reset.request', 'uses' => 'RegistrationController@sendResetPasswordEmail']);
    Route::get('reset/{hash}/{code}', ['as' => 'checkauth.reset.form', 'uses' => 'RegistrationController@passwordResetForm']);
    Route::post('reset/{hash}/{code}', ['as' => 'checkauth.reset.password', 'uses' => 'RegistrationController@resetPassword']);
    Route::get('provider/{hash}/{provider}/{oauth_id}', ['as' => 'checkauth.provider', 'uses' => 'RegistrationController@provider']);

    Route::group(['middleware' => ['sentinel.auth']], function () {//user authenticated
        //Profile
        Route::get('profile', ['as' => 'checkauth.profile.show', 'uses' => 'ProfileController@show']);
        Route::get('profile/edit', ['as' => 'checkauth.profile.edit', 'uses' => 'ProfileController@edit']);
        Route::put('profile', ['as' => 'checkauth.profile.update', 'uses' => 'ProfileController@update']);
        Route::post('profile/password', ['as' => 'checkauth.profile.password', 'uses' => 'ProfileController@changePassword']);
    });

    Route::group(['middleware' => ['sentinel.permission:users.list']], function () {//user authenticated and with user.list permission
        //List Users
        Route::get('users', ['as' => 'checkauth.users.index', 'uses' => 'UserController@index']);
    });


    Route::group(['middleware' => ['sentinel.permission:users.edit']], function () {//user authenticated and with user.edit permission

        //Edit Users
        Route::get('users/create', ['as' => 'checkauth.users.create', 'uses' => 'UserController@create']);
        Route::get('users/oauth/create', ['as' => 'checkauth.users.oauth.create', 'uses' => 'UserController@oauth']);
        Route::post('users', ['as' => 'checkauth.users.insert', 'uses' => 'UserController@insert']);
        Route::get('users/{hash}', ['as' => 'checkauth.users.show', 'uses' => 'UserController@show']);
        Route::get('users/{hash}/edit', ['as' => 'checkauth.users.edit', 'uses' => 'UserController@edit']);
        Route::post('users/{hash}/permissions', ['as' => 'checkauth.users.permissions', 'uses' => 'UserController@updatePermissions']);
        Route::post('users/{hash}/password', ['as' => 'checkauth.password.change', 'uses' => 'UserController@changePassword']);
        Route::post('users/{hash}/memberships', ['as' => 'checkauth.users.memberships', 'uses' => 'UserController@updateRoleMemberships']);
        Route::put('users/{hash}', ['as' => 'checkauth.users.update', 'uses' => 'UserController@update']);
        Route::delete('users/{hash}', ['as' => 'checkauth.users.destroy', 'uses' => 'UserController@destroy']);
        Route::get('users/deactivate/{hash}', ['as' => 'checkauth.users.deactivate', 'uses' => 'UserController@deactivateUser']);
        Route::get('users/activate/{hash}', ['as' => 'checkauth.users.activate', 'uses' => 'UserController@activateUser']);

    });

    Route::group(['middleware' => ['sentinel.permission:roles.list']], function () {//user authenticated and with roles.edit permission
        // Sentinel Roles
        Route::get('roles', ['as' => 'checkauth.roles.index', 'uses' => 'RoleController@index']);
        Route::get('roles/{hash}', ['as' => 'checkauth.roles.show', 'uses' => 'RoleController@show']);
    });

    Route::group(['middleware' => ['sentinel.permission:roles.edit']], function () {//user authenticated and with roles.edit permission
        // Sentinel Roles
        Route::get('roles/create', ['as' => 'checkauth.roles.create', 'uses' => 'RoleController@create']);
        Route::post('roles', ['as' => 'checkauth.roles.store', 'uses' => 'RoleController@store']);
        Route::get('roles/{hash}/edit', ['as' => 'checkauth.roles.edit', 'uses' => 'RoleController@edit']);
        Route::put('roles/{hash}', ['as' => 'checkauth.roles.update', 'uses' => 'RoleController@update']);
        Route::delete('roles/{hash}', ['as' => 'checkauth.roles.destroy', 'uses' => 'RoleController@destroy']);
    });

});