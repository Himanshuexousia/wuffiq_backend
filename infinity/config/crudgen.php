<?php

return [

    'custom_template' => true,

    /*
    |--------------------------------------------------------------------------
    | Crud Generator Template Stubs Storage Path
    |--------------------------------------------------------------------------
    |
    | Here you can specify your custom template path for the generator.
    |
     */
    'path' => base_path('resources/vendor/crudgen/stubs/'),


    /*
    |--------------------------------------------------------------------------
    | Middleware
    |--------------------------------------------------------------------------
    |
    | Here you can specify a middleware to filter crud requests (for example, verify if user has admin rights to access the cruds)
    |
     */
    'middleware' => 'sentinel.auth', //if you're using checkauth. If you're using native Laravel Auth change it to 'middleware' => 'auth'

    /*
    |--------------------------------------------------------------------------
    | Prefix
    |--------------------------------------------------------------------------
    |
    | Here you can specify the route name prefix on witch will reside the cruds
    |
     */
    'prefix' => 'admin',

    /*
    |--------------------------------------------------------------------------
    | Columns number to show in view's table.
    |--------------------------------------------------------------------------
    |
    | Here you can specify how many columns your list page is going to show
    |
     */
    'view_columns_number' => 3,

];
