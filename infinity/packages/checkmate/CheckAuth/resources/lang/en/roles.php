<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Roles Repositiory Messages
    |--------------------------------------------------------------------------
    */

    'created'        => "Roles Created.",

    'loginreq'        => "Login field required.",

    'userexists'    => "User already exists.",

    'updated'        => "Roles has been updated.",

    'updateproblem' => "There was a problem updating role.",

    'namereq'        => "You must provide a role name.",

    'roleexists'    => "That role already exists.",

    'notfound'        => "Roles not found.",

);
