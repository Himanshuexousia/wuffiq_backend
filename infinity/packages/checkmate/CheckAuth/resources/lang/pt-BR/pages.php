<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Language strings for views
    |--------------------------------------------------------------------------
    */

    'remember-me' => 'Lembrar de mim',

    'with' => 'com',

    'forgot-password' => 'Esqueci minha senha',

    'create' => 'Criar',

    'update' => 'Editar',

    'delete' => 'Deletar',

    'account' => 'Conta',

    'first-name' => 'Nome',

    'last-name' => 'Sobrenome',
    
    'created-at' => 'Criado em',

    'updated-at' => 'Atualizado em',

    'permissions' => 'Permissões',

    'change-password' => 'Alterar senha',

    'old-password' => 'Senha antiga',

    'new-password' => 'Senha nova',

    'confirm-password' => 'Confirmar senha',

    'hello' => 'Olá',

    'update-your-account' => 'Atualizar sua conta',

    'update-account' => 'Atualizar conta',

    'select' => 'Selecionar',

    'image' => 'Imagem',

    'profile' => 'Perfil',

    'change' => 'Alterar',

    'remove' => 'Remover',

    'flashsuccess'    => 'Sucesso',

    'flasherror'    => 'Erro',

    'flashwarning'    => 'Atenção',

    'flashinfo'        => 'Obs.:',

    'register'        => 'Registrar-se',

    'login'            => "Acessar",

    'logout'        => "Sair",

    'home'            => "Página Principal",

    'name' => 'Nome',

    'documentation' => 'Documentação',

    'users'        => 'Usuário|Usuários',

    'roles'        => 'Grupo|Grupos',

    'helloworld'    => 'Essa é uma homepage de exemplo',

    'description'    => "Este é um exemplo do <a href=\"https://github.com/laravel/laravel\">Laravel 4.1</a> rodando com <a href=\"https://github.com/cartalyst/sentry\">Sentry 2.0</a> e <a href=\"http://getbootstrap.com/\">Bootstrap 3.0</a>.",

    'loginstatus'    => "Você está logado.",

    'sessiondata'    => "Dados da Sessão",

    'currentusers'    => "Usuários Atuais",

    'options'        => 'Opções',

    'status'        => "Status",

    'active'        => "Ativo",

    'notactive'        => "Inativo",

    'suspended'        => "Suspenso",

    'banned'        => "Banir",

    'actionedit'    => "Editar",

    'actionsuspend'    => "Suspender",

    'actionunsuspend' => "Reativar",

    'actionban'        => "Banir",

    'actionunban'    => "Reabilitar",

    'actiondelete'    => "Deletar",

    'savingDisabled' => "Salvar está desabilitado nesta aplicação",
);
