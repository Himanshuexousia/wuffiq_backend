@extends('checkauth::sessions.logoff')
{{-- Web site Title --}}
@section('checkauth::htmlHeaderTitle')
    Login
@endsection

{{-- Content --}}
@section('checkauth:grid-class')
    col-md-offset-4 col-md-4 col-md-offset-4
@stop

@section('checkauth:card-title')
    {{ trans('checkauth::pages.login') }}
@stop

@section('checkauth::card-block')
    <form method="POST" action="{{ route('checkauth.session.store') }}" accept-charset="UTF-8" class="form-signin">
        <div class="form-group {{ ($errors->has('email')) ? 'has-danger' : '' }} has-feedback">
            <input type="email" class="form-control {{ ($errors->has('email')) ? 'form-control-danger' : '' }}" placeholder="Email" autofocus="autofocus" type="text" value="{{ Input::old('email') }}"  name="email">
            @if($errors->has('email'))
                <label class="form-control-label" for="email">{{$errors->first('email')}}</label>
            @endif
        </div>
        <div class="form-group {{ ($errors->has('password')) ? 'has-danger' : '' }} has-feedback">
            <input class="form-control {{ ($errors->has('password')) ? 'form-control-danger' : '' }}" placeholder="Password" type="password"  name="password">
            @if($errors->has('password'))
                <label class="form-control-label" for="email">{{$errors->first('password')}}</label>
            @endif
        </div>
        @if(config('checkauth.recapcha'))
        <div class="form-group {{ ($errors->has('g-recaptcha-response')) ? 'has-danger' : '' }}">
            {!! app('captcha')->display() !!}
            @if($errors->has('g-recaptcha-response'))
                <label class="form-control-label" for="email">{{$errors->first('g-recaptcha-response')}}</label>
            @endif
        </div>
        @endif
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <label>
                        <input type="checkbox"> {{ trans('checkauth::pages.remember-me') }}
                    </label>
                    <input name="_token" value="{{ csrf_token() }}" type="hidden">
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('checkauth::pages.login') }}</button>
            </div>
            <!-- /.col -->
        </div>

    </form>
    @if(config('checkauth.oauth_providers'))
    <div class="row">
        <div class="col-xs-12">
            @if(in_array('google', config('checkauth.oauth_providers')))
            <a class="btn btn-block btn-social btn-google" href="{{route('checkauth.oauth.login', ['provider' => 'google'])}}">
                <i class="fa fa-google-plus"></i>
                {{ trans('checkauth::pages.login') }} {{ trans('checkauth::pages.with') }} Google
            </a>
            @endif
            @if(in_array('facebook', config('checkauth.oauth_providers')))
                <a class="btn btn-block btn-social btn-facebook" href="{{route('checkauth.oauth.login',['provider' => 'facebook'])}}">
                    <i class="fa fa-facebook"></i>
                    {{ trans('checkauth::pages.login') }} {{ trans('checkauth::pages.with') }} Facebook
                </a>
            @endif
            @if(in_array('twitter', config('checkauth.oauth_providers')))
                <a class="btn btn-block btn-social btn-twitter" href="{{route('checkauth.oauth.login',['provider' => 'twitter'])}}">
                    <i class="fa fa-twitter"></i>
                    {{ trans('checkauth::pages.login') }} {{ trans('checkauth::pages.with') }} Twitter
                </a>
            @endif
            @if(in_array('linkedin', config('checkauth.oauth_providers')))
                <a class="btn btn-block btn-social btn-linkedin" href="{{route('checkauth.oauth.login',['provider' => 'linkedin'])}}">
                    <i class="fa fa-linkedin"></i>
                    {{ trans('checkauth::pages.login') }} {{ trans('checkauth::pages.with') }} Linkedin
                </a>
            @endif
            @if(in_array('github', config('checkauth.oauth_providers')))
                <a class="btn btn-block btn-social btn-github" href="{{route('checkauth.oauth.login',['provider' => 'github'])}}">
                    <i class="fa fa-github"></i>
                    {{ trans('checkauth::pages.login') }} {{ trans('checkauth::pages.with') }} Github
                </a>
            @endif
            @if(in_array('bitbucket', config('checkauth.oauth_providers')))
                <a class="btn btn-block btn-social btn-facebook" href="{{route('checkauth.oauth.login',['provider' => 'bitbucket'])}}">
                    <i class="fa fa-bitbucket"></i>
                    {{ trans('checkauth::pages.login') }} {{ trans('checkauth::pages.with') }} Bitbucket
                </a>
            @endif
            <a class="btn-block btn btn-secondary" href="{{ route('checkauth.forgot.form') }}">{{ trans('checkauth::pages.forgot-password') }}</a>

            <a class="btn-block btn btn-secondary" href="{{ route('checkauth.register.form') }}">{{ trans('checkauth::pages.register') }}</a>
        </div>
    </div>
    @endif
@stop

@section('checkauth::card-footer')
@stop

<!-- /.login-box -->
@section('checkauth::scripts')
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
@stop