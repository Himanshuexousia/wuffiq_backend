<?php

require_once 'CheckAuthTestCase.php';

class SentinelRoleRepositoryTests extends CheckAuthTestCase
{
    /**
     * Setup the test environment.
     */
    public function setUp()
    {
        parent::setUp();
        $this->repo = app()->make('Checkmate\CheckAuth\Repositories\Roles\CheckAuthRolesRepositoryInterface');
    }
    
    /**
     * @group roles
     * Test the instantiation of the CheckAuth SentinelUser repository
     */
    public function testRepoInstantiation()
    {
        // Test that we are able to properly instantiate the SentinelUser object for testing
        $this->assertInstanceOf('Checkmate\CheckAuth\Repositories\Roles\SentinelRolesRepository', $this->repo);
    }

    /**
     * @group roles
     * Test that the seed data exists and is reachable
     */
    public function testDatabaseSeeds()
    {
        // Check that the test data is present and correctly seeded
        $user = \DB::table('users')->where('email', 'user@user.com')->first();
        $this->assertEquals('user@user.com', $user->email);
    }

    /**
     * @group roles
     * Test the creation of a user using the default configuration options
     */
    public function testSavingRole()
    {
        // This is the code we are testing
        $result = $this->repo->store([
            'name' => 'Managers',
            'permissions' => ['user' => 1, 'admin' => 0]
        ]);

        //print_r($result->getPayload()['role']);

        // Assertions
        $this->assertTrue($result->isSuccessful());
        //$this->assertInstanceOf('Checkmate\CheckAuth\Models\Role', $result->getPayload()['role']);
        $this->assertArrayHasKey('user', $result->getPayload()['role']->getPermissions());
        //$this->assertArrayNotHasKey('admin', $result->getPayload()['role']->getPermissions());

        $role = \DB::table('roles')->where('name', 'Managers')->first();
        $this->assertEquals('Managers', $role->name);
    }

    /**
     * @group roles
     * Test role update
     */
    public function testUpdatingRole()
    {
        // Find the role we will edit
        $role = $this->repo->retrieveByName('Users');

        // This is the code we are testing
        $result = $this->repo->update([
            'id' => $role->id,
            'name' => 'Managers',
            'permissions' => ['admin' => 1]
        ]);

        //print_r($result->getPayload()['role']);

        // Assertions
        $this->assertTrue($result->isSuccessful());
        $this->assertEquals('Managers', $result->getPayload()['role']->name);
        $this->assertArrayHasKey('admin', $result->getPayload()['role']->getPermissions());

    }

    /**
     * @group roles
     * Test role destroy
     */
    public function testDestroyRole()
    {
        // Find the role we will remove from storage
        $role = $this->repo->retrieveByName('Users');

        // This is the code we are testing
        $result = $this->repo->destroy($role->id);

        // Assertions
        $this->assertTrue($result->isSuccessful());
        $this->assertFalse(\DB::table('roles')->where('name', 'Users')->count() > 0);
    }

    /**
     * @group roles
     * Test retrieve role by id
     */
    public function testRetrieveRoleById()
    {
        // Find the role we will use for reference
        $reference = $this->repo->retrieveByName('Users');

        // This is the code we are testing
        $role = $this->repo->retrieveById($reference->id);

        // Assertions
        //$this->assertInstanceOf('Checkmate\CheckAuth\Models\Role', $role);
        $this->assertEquals('Users', $role->name);
    }

    /**
     * @group roles
     * Test retrieve role by name
     */
    public function testRetrieveRoleByName()
    {
        // Find the role we will use for reference
        $reference = $this->repo->retrieveById(1);

        // This is the code we are testing
        $role = $this->repo->retrieveByName($reference->name);

        // Assertions
        //$this->assertInstanceOf('Checkmate\CheckAuth\Models\Role', $role);
        $this->assertEquals(1, $role->id);
    }

    /**
     * @group roles
     * Test retrieve all roles
     */
    public function testRetrieveAllRoles()
    {
        // This is the code we are testing
        $roles = $this->repo->all();
        $this->assertEquals(2, count($roles));
    }
}