<?php

require_once 'CheckAuthTestCase.php';

class SentinelSessionTests extends CheckAuthTestCase
{
    /**
     * Setup the test environment.
     */
    public function setUp()
    {
        parent::setUp();

        $this->repo = app()->make('Checkmate\CheckAuth\Repositories\Session\CheckAuthSessionRepositoryInterface');
        include __DIR__ . '/../src/Http/routes.php';
    }

    /**
     * @group session
     * Test the instantiation of the CheckAuth SentinelUser repository
     */
    public function testRepoInstantiation()
    {
        // Test that we are able to properly instantiate the SentinelUser object for testing
        $this->assertInstanceOf('Checkmate\CheckAuth\Repositories\Session\SentinelSessionRepository', $this->repo);
    }

    /**
     * @group session
     * Test that the seed data exists and is reachable
     */
    public function testDatabaseSeeds()
    {
        // Check that the test data is present and correctly seeded
        $user = \DB::table('users')->where('email', 'user@user.com')->first();
        $this->assertEquals('user@user.com', $user->email);
    }

    /**
     * @group session
     * Test the creation of a user using the default configuration options
     */
    public function testCreatingSession()
    {

        //create user
        $credentials = [
            'email'    => 'john.doe@example.com',
            'password' => 'password',
        ];

        $user = Sentinel::registerAndActivate($credentials);

        // This is the code we are testing
        $result = $this->repo->store([
            'email' => 'john.doe@example.com',
            'password' => 'password'
        ]);

        // Assertions
        $this->assertTrue($result->isSuccessful());

    }

    /**
     * @group session
     * Test if user can have access given an admin role
     */
    public function testAdminSession()
    {
        $user = Sentinel::findById(1);
        Sentinel::login($user);

        $role = Sentinel::findRoleById(2);

        // Assertions
        $this->assertTrue($role->hasAccess(['admin']));
        $this->assertTrue($user->hasAccess(['admin']));

        $loggedUser = Sentinel::check();
        $this->assertTrue($loggedUser->hasAccess(['admin']));
    }

    /**
     * @group session
     * Test if user can have access given an admin role
     */
    public function testAddingPermission()
    {
        $user = Sentinel::findById(1);
        $user->addPermission('test');
        $user->save();
        // Assertions
        $this->assertTrue($user->hasAccess(['test']));
    }

}