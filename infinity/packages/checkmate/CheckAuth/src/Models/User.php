<?php

namespace Checkmate\CheckAuth\Models;

use Hashids;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;



/**
 * User Class
 *
 * @license http://www.checkmate.com/licenses/checkauth
 *
 * @property $first_name
 * @property $last_name
 * @property $email
 * @property $avatar
 * @property $oauth
 * @property $id
 *
 * @version 1.0.0
 */
class User extends EloquentUser implements UserContract
{
    /**
     * Set the Sentinel User Model Hasher to be the same as the configured Sentinel Hasher
     */
    public static function boot()
    {
        parent::boot();
        //static::setHasher(app()->make('sentinel.hasher'));

    }

    public function __construct()
    {
        $this->hash = $this->getHashAttribute();
    }

    private $hash;

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get auth identifyer name
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return $this->getPersistCode();
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string $value
     *
     * @return void
     */
    public function setRememberToken($value)
    {
        $this->persist_code = $value;

        $this->save();
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return "persist_code";
    }

    /**
     * Use a mutator to derive the appropriate hash for this user
     *
     * @return mixed
     */
    public function getHashAttribute()
    {
        return Hashids::encode($this->getKey());
    }
}