<?php

namespace Checkmate\CheckAuth\Facades;

use Illuminate\Support\Facades\Facade;

class CheckAuth extends Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'checkauth';
    }
}