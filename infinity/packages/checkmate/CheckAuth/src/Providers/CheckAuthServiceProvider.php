<?php
namespace Checkmate\CheckAuth\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Console\AppNamespaceDetectorTrait;
use Illuminate\Routing\Router;
use Illuminate\Foundation\AliasLoader;
use Artisan;
use ReflectionClass;
use Checkmate\CheckAuth\Command\CheckAuthPublishCommand;
use Checkmate\CheckAuth\Repositories\Session\SentinelSessionRepository;
use Checkmate\CheckAuth\Repositories\Roles\SentinelRolesRepository;
use Checkmate\CheckAuth\Repositories\User\SentinelUserRepository;


/**
 * CheckAuth registration class into Laravel
 *
 * @license http://checkauth.checkmate.com/licenses/
 *
 * @version 1.0.0
 */
class CheckAuthServiceProvider extends ServiceProvider
{
    use AppNamespaceDetectorTrait;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application services.
     *
     * @param $router Router
     *
     * @return void
     */
    public function boot(Router $router)
    {
        // Find path to the package
        $providerFilename = with(new ReflectionClass('Checkmate\CheckAuth\Providers\CheckAuthServiceProvider'))->getFileName();
        $srcPath = dirname($providerFilename)  . '/../';

        // Register Artisan Commands
        $this->registerArtisanCommands();

        // Establish Fallback Config settings
        $this->mergeConfigFrom($srcPath.'/Config/checkauth.php', 'checkauth');

        // Register Routes Middleware
        $router->middleware('sentinel.auth', '\Checkmate\CheckAuth\Middleware\SentinelAuth');
        $router->middleware('sentinel.role', '\Checkmate\CheckAuth\Middleware\SentinelRolesAccess');
        $router->middleware('sentinel.permission', '\Checkmate\CheckAuth\Middleware\SentinelPermissionsAccess');

        // Establish Views Namespace
        if (is_dir(base_path() . '/resources/views/checkauth')) {
            // The package views have been published - use those views.
            $this->loadViewsFrom(base_path() . '/resources/views/vendor/checkauth', 'checkauth');
        } else {
            // The package views have not been published. Use the defaults.
            $this->loadViewsFrom($srcPath . '/../resources/views', 'checkauth');
        }

        // Establish Translator Namespace
        if (is_dir(base_path() . '/resources/lang/checkauth')) {
            // The package language files have been published - use those views.
            $this->loadTranslationsFrom(base_path() . '/resources/lang', 'checkauth');
        } else {
            // The package language files have not been published. Use the defaults.
            $this->loadTranslationsFrom($srcPath . '/../resources/lang', 'checkauth');
        }

        // Include custom validation rules
        require $srcPath . 'Http/validators.php';

        // Should we register the default routes?
        if (config('checkauth.routes_enabled')) {

            if (config('checkauth.publish_routes') && \File::exists(app_path() . '/Http/authentication_routes.php')) {
                require app_path() . '/Http/authentication_routes.php';
            }else{
                require $srcPath . '/Http/routes.php';
            }

        }

        // Set up event listeners
        $dispatcher = $this->app->make('events');
        $dispatcher->subscribe('Checkmate\CheckAuth\Listeners\UserEventListener');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Register the Sentinel Service Provider (overriding original vendors service provider
        $this->app->register('Checkmate\CheckAuth\Providers\SentinelServiceProvider');

        // Register the Vinkla/Hashids Service Provider
        $this->app->register('Vinkla\Hashids\HashidsServiceProvider');

        // Register socialite for social logins
        $this->app->register('Laravel\Socialite\SocialiteServiceProvider');

        //Register noCapcha
        $this->app->register('Anhskohbo\NoCaptcha\NoCaptchaServiceProvider');

        // Register intervention Service Provider (file management package)
        $this->app->register('Intervention\Image\ImageServiceProvider');

        // Load Facade Aliases
        $loader = AliasLoader::getInstance();

        $loader->alias('CheckAuth', 'Checkmate\CheckAuth\Facades\CheckAuth');
        $loader->alias('Hashids', 'Vinkla\Hashids\Facades\Hashids');
        $loader->alias('Sentinel', 'Cartalyst\Sentinel\Laravel\Facades\Sentinel');
        $loader->alias('Activation', 'Cartalyst\Sentinel\Laravel\Facades\Activation');
        $loader->alias('Reminder', 'Cartalyst\Sentinel\Laravel\Facades\Reminder');
        $loader->alias('Sentinel', 'Cartalyst\Sentinel\Laravel\Facades\Sentinel');
        $loader->alias('Input', 'Illuminate\Support\Facades\Input');
        $loader->alias('Socialite', 'Laravel\Socialite\Facades\Socialite');
        $loader->alias('Image', 'Intervention\Image\Facades\Image');

        $this->app->singleton('checkauth', function ($app) {
            return new SentinelUserRepository(
                $app['sentinel'],
                $app['config'],
                $app['events']
            );
        });

        //register class alias
        //$this->app->alias('checkauth', 'Checkmate\CheckAuth\Repositories\User\CheckAuthUserRepositoryInterface');


        // Bind the User Repository
        $this->app->bind('Checkmate\CheckAuth\Repositories\User\CheckAuthUserRepositoryInterface', function ($app) {
            return new SentinelUserRepository(
                $app['sentinel'],
                $app['config'],
                $app['events']
            );
        });


        // Bind the Roles Repository
        $this->app->bind('Checkmate\CheckAuth\Repositories\Roles\CheckAuthRolesRepositoryInterface', function ($app) {
            return new SentinelRolesRepository(
                $app['sentinel'],
                $app['events']
            );
        });

        //register class alias
        //$this->app->alias('checkauth.role', 'Checkmate\CheckAuth\Repositories\Roles\CheckAuthRolesRepositoryInterface');

        // Bind the Session Manager
        $this->app->bind('Checkmate\CheckAuth\Repositories\Session\CheckAuthSessionRepositoryInterface', function ($app) {
            return new SentinelSessionRepository(
                $app['sentinel'],
                $app['events']
            );
        });

        //register class alias
        //$this->app->alias('checkauth.session', 'Checkmate\CheckAuth\Repositories\Session\CheckAuthSessionRepositoryInterface');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        //return array('auth', 'sentinel');
        return [
            'checkauth',
            'sentinel'
            //'checkauth.role',
            //'checkauth.session',
        ];
    }

    /**
     * Register the Artisan Commands
     */
    private function registerArtisanCommands()
    {
        $this->app['checkauth.publisher'] = $this->app->share(function ($app) {
            return new CheckAuthPublishCommand(
                $app->make('files')
            );
        });

        $this->commands('checkauth.publisher');
    }
}