<?php

namespace Checkmate\CheckAuth\Traits;

use Redirect;
use Response;
use View;

/**
 * Redirect, Response and View methods to be used in other CheckAuth Classes
 *
 * @license http://www.checkmate.com/licenses/checkauth
 *
 * @version 1.0.0
 */
trait CheckAuthViewfinderTrait
{
    /**
     * Before returning an HTML view, we need to make sure the developer has not
     * specified that we only use JSON responses.
     *
     * @param       $view
     * @param array $payload
     *
     * @return Response
     */
    public function viewFinder($view, $payload = [])
    {

        // Check the config for enabled views
        if (config('checkauth.views_enabled')) {
            // Views are enabled.
            return View::make($view)->with($payload);
        } else {
            // Check the payload for paginator instances.
            foreach ($payload as $name => $item) {
                if ($item instanceof \Illuminate\Pagination\Paginator) {
                    $payload[$name] = $item->getCollection();
                }
            }

            return Response::json($payload);
        }
    }
}
