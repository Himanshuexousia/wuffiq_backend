<?php

namespace Checkmate\CheckAuth\Middleware;

use Checkmate\CheckAuth\Repositories\User\CheckAuthUserRepositoryInterface;
use Closure;

/**
 * middleware provide a convenient mechanism for filtering HTTP requests entering your application.
 * SentinelAuth middleware allows only authenticated users to access.
 *
 * @license http://www.checkmate.com/licenses/checkauth
 *
 * @version 1.0.0
 */
class SentinelAuth
{
    /**
     * Constructor
     * @param CheckAuthUserRepositoryInterface $userManager
     */
    public function __construct(CheckAuthUserRepositoryInterface $userManager)
    {
        $this->user = $userManager;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $this->user->getUser();
        if (!$user) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                $request->session()->reflash();
                return redirect()->guest(route('checkauth.login'));
            }
        }

        return $next($request);
    }
}
