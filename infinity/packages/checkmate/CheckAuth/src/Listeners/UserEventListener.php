<?php

namespace Checkmate\CheckAuth\Listeners;

use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Config\Repository;
use Cartalyst\Sentinel\Laravel\Facades\Activation;

/**
 * Event listener for user actions
 *
 * @license http://www.checkmate.com/licenses/checkauth
 *
 * @version 1.0.0
 */
class UserEventListener
{
    public function __construct(Repository $config)
    {
        $this->config = $config;
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher $events
     * @return array
     */
    public function subscribe($events)
    {
        $events->listen('checkauth.user.login', 'Checkmate\CheckAuth\Listeners\UserEventListener@onUserLogin', 10);
        $events->listen('checkauth.user.logout', 'Checkmate\CheckAuth\Listeners\UserEventListener@onUserLogout', 10);
        $events->listen('checkauth.user.registered', 'Checkmate\CheckAuth\Listeners\UserEventListener@welcome', 10);
        $events->listen('checkauth.user.resend', 'Checkmate\CheckAuth\Listeners\UserEventListener@welcome', 10);
        $events->listen('checkauth.user.reset', 'Checkmate\CheckAuth\Listeners\UserEventListener@passwordReset', 10);
    }

    /**
     * Handle user login events.
     */
    public function onUserLogin($user)
    {
    }

    /**
     * Handle user logout events.
     */
    public function onUserLogout()
    {
    }

    /**
     * Send a welcome email to a new user.
     *
     * @param $user
     * @param $activated
     *
     * @internal param string $email
     * @internal param int $userId
     * @internal param string $activationCode
     */
    public function welcome($user, $activated)
    {

        $data['email'] = $user->email;
        $data['first_name'] = $user->first_name;
        $data['last_name'] = $user->last_name;

        if($user->avatar != ''){$data['avatar'] = asset('img/avatars' . $user->avatar);}else{$data['avatar'] = asset('vendor/chkauth/img/blank_avatar.png');}

        if ($activated) {
            $subject = $this->config->get('checkauth.subjects.welcome');
            $view = $this->config->get('checkauth.email.views.welcome', 'checkauth::emails.welcome');
            $this->sendTo($user->email, $subject, $view, $data);
        }else{
            $subject = $this->config->get('checkauth.subjects.activate');
            $view = $this->config->get('checkauth.email.views.activate', 'checkauth::emails.activate');

            $data['hash'] = $user->hash;

            //$data['code'] = $user->getActivationCode();

            //get activation code
            if($activation = Activation::exists($user)){
                $data['code'] = $activation['code'];
            }else{
                $activation = Activation::create($user);
                $data['code'] = $activation['code'];
            }

            $this->sendTo($user->email, $subject, $view, $data);

        }
    }

    /**
     * Email Password Reset info to a user.
     *
     * @param $user
     * @param $code
     *
     * @internal param string $email
     * @internal param int $userId
     * @internal param string $resetCode
     */
    public function passwordReset($user, $code)
    {
        $data['email'] = $user->email;
        $data['first_name'] = $user->first_name;
        $data['last_name'] = $user->last_name;

        $subject = $this->config->get('checkauth.subjects.reset_password');
        $view = $this->config->get('checkauth.email.views.reset', 'checkauth::emails.reset');
        $data['hash'] = $user->hash;
        $data['code'] = $code;
        $data['email'] = $user->email;

        $this->sendTo($user->email, $subject, $view, $data);

    }

    /**
     * Convenience function for sending mail
     *
     * @param $email
     * @param $subject
     * @param $view
     * @param array $data
     */
    private function sendTo($email, $subject, $view, $data = array())
    {
        $sender = $this->gatherSenderAddress();

        Mail::queue($view, $data, function ($message) use ($email, $subject, $sender) {
            $message->to($email)
                ->from($sender['address'], $sender['name'])
                ->subject($subject);
        });


    }

    /**
     * If the application does not have a valid "from" address configured, we should stub in
     * a temporary alternative so we have something to pass to the Mailer
     *
     * @return array|mixed
     */
    private function gatherSenderAddress()
    {
        $sender = config('mail.from', []);

        if (!array_key_exists('address', $sender) || is_null($sender['address'])) {
            return ['address' => 'noreply@example.com', 'name' => ''];
        }

        if (is_null($sender['name'])) {
            $sender['name'] = '';
        }

        return $sender;
    }
}
