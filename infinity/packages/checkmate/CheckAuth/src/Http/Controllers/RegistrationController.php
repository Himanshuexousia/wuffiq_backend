<?php

namespace Checkmate\CheckAuth\Http\Controllers;

use Vinkla\Hashids\HashidsManager;
use Illuminate\Routing\Controller as BaseController;
use Sentinel;
use View;
use Input;
use Event;
use Redirect;
use Session;
use Config;
use CheckAuth;

use Checkmate\CheckAuth\FormRequests\UserCreateRequest;
use Checkmate\CheckAuth\FormRequests\EmailRequest;
use Checkmate\CheckAuth\FormRequests\ResetPasswordRequest;
use Checkmate\CheckAuth\Repositories\Roles\CheckAuthRolesRepositoryInterface;
use Checkmate\CheckAuth\Repositories\User\CheckAuthUserRepositoryInterface;
use Checkmate\CheckAuth\Traits\CheckAuthRedirectionTrait;
use Checkmate\CheckAuth\Traits\CheckAuthViewfinderTrait;

/**
 * User Profile controller
 *
 * @license http://www.checkmate.com/licenses/checkauth
 *
 * @version 1.0.0
 */
class RegistrationController extends BaseController
{
    /**
     * Traits
     */
    use CheckAuthRedirectionTrait;
    use CheckAuthViewfinderTrait;

    /**
     * Constructor
     * @param CheckAuthUserRepositoryInterface $userRepository
     * @param CheckAuthRolesRepositoryInterface $groupRepository
     * @param HashidsManager $hashids
     */
    public function __construct(
        CheckAuthUserRepositoryInterface $userRepository,
        CheckAuthRolesRepositoryInterface $groupRepository,
        HashidsManager $hashids
    ) {
        $this->userRepository       = $userRepository;
        $this->groupRepository      = $groupRepository;
        $this->hashids              = $hashids;

        //Check CSRF token on POST
        //$this->beforeFilter('Sentinel\csrf', array('on' => array('post', 'put', 'delete')));//deprecated on laravel 5.2
    }

    /**
     * Show the registration form, if registration is allowed
     *
     * @return Response
     */
    public function registration()
    {
        // Is this user already signed in? If so redirect to the post login route
        if (CheckAuth::getUser()) {
            return $this->redirectTo('session_store');
        }

        //If registration is currently disabled, show a message and redirect home.
        if (! config('checkauth.registration', false)) {
            return $this->redirectTo(['route' => 'home'], ['error' => trans('checkauth::users.inactive_reg')]);
        }

        // All clear - show the registration form.
        return $this->viewFinder('checkauth::users.register');
    }

    /**
     * Process a registration request
     *
     * @param UserCreateRequest $request
     *
     * @return $this->redirectViaResponse
     */
    public function register(UserCreateRequest $request)
    {

        $data = $request->all();

        $result = $this->userRepository->store($data);

        return $this->redirectViaResponse('registration_complete', $result);
    }

    /**
     * change provider
     *
     * @param string $hash
     *
     * @param string $provider
     *
     * @param string $oauth_id
     *
     * @return $this->redirectViaResponse
     */
    public function provider($hash, $provider, $oauth_id)
    {
        // Decode the hashid
        $id = $this->hashids->decode($hash)[0];

        $result = $this->userRepository->provider($id, $provider, $oauth_id);

        if($result->isSuccessful()){
            Session::flash('success', trans('checkauth::sessions.provider_changed', array('provider'=>$provider)));
            return Redirect::route('checkauth.login');
        }

    }


    /**
     * Activate a new user
     *
     * @param string $hash
     * @param string $code
     *
     * @return $this->redirectViaResponse
     */
    public function activate($hash, $code)
    {
        // Decode the hashid
        $id = $this->hashids->decode($hash)[0];

        // Attempt the activation
        $result = $this->userRepository->activate($id, $code);

        // It worked!  Use config to determine where we should go.
        return $this->redirectViaResponse('registration_activated', $result);
    }

    /**
     * Show the 'Resend Activation' form
     *
     * @return $this->viewFinder
     */
    public function resendActivationForm()
    {
        return $this->viewFinder('checkauth::users.resend');
    }

    /**
     * Process resend activation request
     *
     * @param EmailRequest $request
     *
     * @return Response
     */
    public function resendActivation(EmailRequest $request)
    {
        // Resend the activation email
        $result = $this->userRepository->resend(['email' => e($request->get('email'))]);

        // It worked!  Use config to determine where we should go.
        if($result->isSuccessful()){
            return $this->redirectViaResponse('registration_resend', $result);
        }else{
            return $this->redirectViaResponse('registration_resend', $result);
        }

    }

    /**
     * Display the "Forgot Password" form
     *
     * @return $this->viewFinder
     */
    public function forgotPasswordForm()
    {
        return $this->viewFinder('checkauth::users.forgot');
    }


    /**
     * Process Forgot Password request
     *
     * @param EmailRequest $request
     *
     * @return $this->redirectViaResponse
     */
    public function sendResetPasswordEmail(EmailRequest $request)
    {

        // Send Password Reset Email
        $result = $this->userRepository->triggerPasswordReset(e($request->get('email')));

        if($result->isSuccessful()){
            // It worked!  Use config to determine where we should go.
            return $this->redirectViaResponse('registration_reset_triggered', $result);
        }else{
            return $this->redirectViaResponse('registration_reset', $result);
        }

    }

    /**
     * A user is attempting to reset their password
     *
     * @param string $hash
     * @param string $code
     *
     * @return Redirect|View
     */
    public function passwordResetForm($hash, $code)
    {
        // Decode the hashid
        $id = $this->hashids->decode($hash)[0];

        // Validate Reset Code
        $result = $this->userRepository->validateResetCode($id, $code);

        if (! $result->isSuccessful()) {
            return $this->redirectViaResponse('registration_reset_invalid', $result);
        }

        return $this->viewFinder('checkauth::users.reset', [
            'hash' => $hash,
            'code' => $code
        ]);
    }

    /**
     * Process a password reset form submission
     *
     * @param ResetPasswordRequest $request
     * @param string $hash
     * @param string $code
     * @return $this->redirectViaResponse
     */
    public function resetPassword(ResetPasswordRequest $request, $hash, $code)
    {
        // Decode the hashid
        $id = $this->hashids->decode($hash)[0];

        // Gather input data
        $data = $request->only('password', 'password_confirmation');

        // Change the user's password
        $result = $this->userRepository->resetPassword($id, $code, e($data['password']));

        // It worked!  Use config to determine where we should go.
        return $this->redirectViaResponse('registration_reset_complete', $result);
    }
}