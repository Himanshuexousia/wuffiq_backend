<?php

namespace Checkmate\CheckAuth\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

use Checkmate\CheckAuth\FormRequests\LoginRequest;
use Checkmate\CheckAuth\Repositories\Session\CheckAuthSessionRepositoryInterface;
use Checkmate\CheckAuth\Traits\CheckAuthRedirectionTrait;
use Checkmate\CheckAuth\Traits\CheckAuthViewfinderTrait;
use Checkmate\CheckAuth\Models\User;

use Sentinel;
use View;
use Input;
use Event;
use Redirect;
use Session;
use Config;

use Socialite;//oauth2 package

use CheckAuth;

class SessionController extends BaseController
{
    /**
     * Traits
     */
    use CheckAuthRedirectionTrait;
    use CheckAuthViewfinderTrait;

    /**
     * Constructor
     * @param CheckAuthSessionRepositoryInterface $sessionManager
     */
    public function __construct(CheckAuthSessionRepositoryInterface $sessionManager)
    {
        $this->session = $sessionManager;
    }

    /**
     * Show the login form
     */
    public function create()
    {
        // Is this user already signed in?
        if (CheckAuth::getUser()) {
            return $this->redirectTo('session_store');
        }

        // No - they are not signed in.  Show the login form.
        return $this->viewFinder('checkauth::sessions.login');

    }

    /**
     * Attempt authenticate a user.
     *
     * @param  LoginRequest $request
     *
     * @return Response
     */
    public function store(LoginRequest $request)
    {
        $data = $request->all();

        $result = $this->session->store($data);

        // Did it work?
        if ($result->isSuccessful()) {
            // Login was successful.  Determine where we should go now.
            if (!config('checkauth.views_enabled')) {
                // Views are disabled - return json instead
                return Response::json('success', 200);
            }
            // Views are enabled, so go to the determined route
            $redirect_route = config('checkauth.routing.session_store');
            $this->generateUrl($redirect_route);
            return Redirect::intended($this->generateUrl($redirect_route));

        } else {
            // There was a problem - unrelated to Form validation.
            if (!config('checkauth.views_enabled')) {
                // Views are disabled - return json instead
                return Response::json($result->getMessage(), 400);
            }

            Session::flash('error', $result->getMessage());
            return Redirect::route('checkauth.session.create')->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy()
    {
        $this->session->destroy();
        $logoutMsg = 'You were logged out!';
        return $this->redirectTo('session_destroy', ['success' => $logoutMsg]);
    }

    /**
     * Redirect the user providers authentication page.
     * @param $provider string
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.
     *
     * @param Request $request
     *
     * @param string $provider
     *
     * @return Response
     */
    public function handleProviderCallback($provider, Request $request)
    {

        $oauthUser = Socialite::with($provider)->user();//retrieve callback from oauth provider

        /**
         * workaround for socialite bug in:
         * https://laracasts.com/discuss/channels/laravel/socialite-invalidstateexception-in-abstractproviderphp
         **/
        $state = $request->get('state');
        $request->session()->put('state',$state);
        if(!CheckAuth::getUser()){
            session()->regenerate();
        }

        if($oauthUser){

            $data['name'] = $oauthUser->getName();

            list($fName, $lName) = explode(' ', $data['name'],2);

            $data['first_name'] = $fName;
            $data['last_name'] = $lName;
            $data['oauth_id'] = $oauthUser->getId();
            $data['provider'] = $provider;
            $data['token'] = $oauthUser->token;
            $data['email'] = $oauthUser->getEmail();
            $data['avatar'] = $oauthUser->getAvatar();

            $result = $this->session->oauthStore($data);

            // Did it work?
            if ($result->isSuccessful()) {
                // Login was successful.  Determine where we should go now.
                if (!config('checkauth.views_enabled')) {
                    // Views are disabled - return json instead
                    return Response::json('success', 200);
                }
                // Views are enabled, so go to the determined route
                $redirect_route = config('checkauth.routing.session_store');
                $this->generateUrl($redirect_route);
                return Redirect::intended($this->generateUrl($redirect_route));
            } else {

                if (!config('checkauth.views_enabled')) {
                    //Views are disabled - return json instead
                    //will have to implement this later....
                    //return Response::json($result->getMessage(), 400);
                }

                // do what repository commands...
                Session::flash($result->getPayload()['message_type'], $result->getMessage());
                return Redirect::route($result->getPayload()['route'])->with('data',$data);

            }
        }

    }

}