<?php namespace Checkmate\CrudGen\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use File;

class MenuCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crud:menu
        {name : The name of the migration.}
        {--route-group= : Prefix of the route group.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add required menu items to sub view';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->viewDirectoryPath = config('crudgen.custom_template')
            ? config('crudgen.path')
            : __DIR__ . '/../stubs/';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $crudName = strtolower($this->argument('name'));
        $prefix = $this->option('route-group');

        $crudNameCap = ucwords($crudName);
        $crudNamePlural = str_plural($crudName);
        $crudNamePluralCap = ucwords($crudNamePlural);
        $crudNameSingularCap = ucwords(str_singular($crudName));

        $path = $this->viewDirectoryPath . 'menu.blade.stub';
        $menu = File::get($path);

        $destination = config('view.paths')[0] . '/crud_menu.blade.php';

        $menu = str_replace('%%prefix%%',$prefix,$menu);
        $menu = str_replace('%%$crudName%%',$crudName,$menu);
        $menu = str_replace('%%crudNamePlural%%',$crudNamePlural,$menu);
        $menu = str_replace('%%crudNamePluralCap%%',$crudNamePluralCap,$menu);
        $menu = str_replace('%%crudNameSingularCap%%',$crudNameSingularCap,$menu);
        $menu = str_replace('%%crudNameCap%%',$crudNameCap,$menu);

        $bytesWritten = File::append($destination, $menu);
        if ($bytesWritten === false)
        {
            die("Couldn't write to the file.");
        }

        $this->info('Crud added to menu successfully.');

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'Name of the Crud.'],
        ];
    }

    /*
     * Get the console command options.
     *
     * @return array
     */

    protected function getOptions()
    {
        return [
            ['fields', null, InputOption::VALUE_OPTIONAL, 'The fields of the form.', null],
        ];
    }

}
