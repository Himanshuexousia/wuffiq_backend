<?php

namespace Checkmate\CrudGen\Commands;

use File;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

class ConsoleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crud:generate
                            {name : The name of the Crud.}
                            {--fields= : Fields name for the form & model.}
                            {--route=yes : Include Crud route to routes.php? yes|no.}
                            {--pk=id : The name of the primary key.}
                            {--view-path= : The name of the view path.}
                            {--namespace= : Namespace of the controller.}
                            {--middleware= : Middleware of the controller.}
                            {--route-group= : Prefix of the route group.}
                            {--localize=no : Localize the generated files? yes|no. }
                            {--locales=en : Locales to create lang files for.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Crud including controller, model, views, routes, menu & migrations.';

    /** @var string  */
    protected $routeName = '';

    /** @var string  */
    protected $controller = '';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');

        if (!Schema::hasTable($name))
        {
            $modelName = str_singular($name);
            $migrationName = str_plural(snake_case($name));
            $tableName = $migrationName;

            $routeGroup = $this->option('route-group');
            $this->routeName = ($routeGroup) ? $routeGroup . '/' . snake_case($name, '-') : snake_case($name, '-');

            $controllerNamespace = ($this->option('namespace')) ? $this->option('namespace')  : '';

            $middleware = ($this->option('middleware')) ? $this->option('middleware') : '';

            $fields = $this->option('fields');
            $primaryKey = $this->option('pk');
            $viewPath = $this->option('view-path');

            $fieldsArray = explode(',', $fields);
            $requiredFieldsStr = '';
            $fillableArray = [];

            foreach ($fieldsArray as $item) {
                $spareParts = explode('#', trim($item));
                $fillableArray[] = $spareParts[0];

                $currentField = trim($spareParts[0]);
                $requiredFieldsStr .= (isset($spareParts[2]))
                    ? "'$currentField' => '{$spareParts[2]}', " : '';
            }

            $commaSeparetedString = implode("', '", $fillableArray);
            $fillable = "['" . $commaSeparetedString . "']";

            $requiredFields = ($requiredFieldsStr != '') ? "[" . $requiredFieldsStr . "]" : '';

            $localize = $this->option('localize');
            $locales = $this->option('locales');

            $this->call('crud:controller', ['name' => $controllerNamespace . '\\' . $name . 'Controller', '--crud-name' => $name, '--model-name' => $modelName, '--view-path' => $viewPath, '--required-fields' => $requiredFields, '--route-group' => $routeGroup]);
            $this->call('crud:model', ['name' => $modelName, '--fillable' => $fillable, '--table' => $tableName, '--pk' => $primaryKey]);
            $this->call('crud:migration', ['name' => $migrationName, '--schema' => $fields, '--pk' => $primaryKey]);
            $this->call('crud:view', ['name' => $name, '--fields' => $fields, '--view-path' => $viewPath, '--route-group' => $routeGroup, '--localize' => $localize, '--pk' => $primaryKey]);
            if ($localize == 'yes') {
                $this->call('crud:lang', ['name' => $name, '--fields' => $fields, '--locales' => $locales]);
            }
            //$this->call('crud:menu', ['name' => $modelName]);
            $this->call('crud:menu', ['name' => $name, '--route-group' => $this->option('route-group')]);
            $this->call('crud:route',['name' => $name, '--middleware' => $middleware, '--namespace' => $controllerNamespace, '--route-group' => $this->option('route-group')]);

            //check if user whants to publish migrations
            $this->publishDb();

            // For optimizing the class loader
            $this->callSilent('optimize');

            //final message
            $this->info('Crud Generator process finished');

            return 1;

            // Updating the Http/routes.php file
            /*        $routeFile = app_path('Http/routes.php');
                    if (file_exists($routeFile) && (strtolower($this->option('route')) === 'yes')) {
                        $this->controller = ($controllerNamespace != '') ? $controllerNamespace . '\\' . $name . 'Controller' : $name . 'Controller';

                        $isAdded = File::append($routeFile, "\n" . implode("\n", $this->addRoutes()));

                        if ($isAdded) {
                            $this->info('Crud/Resource route added to ' . $routeFile);
                        } else {
                            $this->info('Unable to add the route to ' . $routeFile);
                        }
                    }*/
        }else{
            $this->info('Table ' . $name . ' already exists on database. Try a different class name');
            return 2;
        }


    }

    /**
     * Add routes.
     *
     * @return  array
     */
    protected function addRoutes()
    {
        return ["Route::resource('" . $this->routeName . "', '" . $this->controller . "');"];
    }

    /**
     * Run migration and seeder files
     */
    private function publishDb()
    {

/*        if ($this->confirm('Would you like to update your database with crud tables?')) {
            //$this->call('migrate:install');
            //$this->call('migrate:reset');
            //$this->call('migrate', ['--seed' => true]);
            $this->call('migrate', ['--path' => 'database/migrations/crudgen']); //migrate the migrations file from crud migrations folder
            //$this->call('migrate:refresh'); //reset and re-run all your migrations
            //$this->call('db:seed');

            // Notify action completion
            $this->info('database updated.');
        }*/

        $this->call('migrate', ['--path' => 'database/migrations/crudgen']);

    }
}