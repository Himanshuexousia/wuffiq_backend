<?php

namespace Checkmate\CrudGen\Providers;

use Illuminate\Support\ServiceProvider;
use Artisan;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Routing\Router;


/**
 * CrudGen registration class into Laravel
 *
 * @license http://crudgen.checkmate.com/licenses/
 *
 * @version 1.0.0
 */
class CrudGenServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Bootstrap the application services.
     *
     * @param  \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function boot(Router $router)
    {
        // Set the path to the  Package namespace root
        $packagePath = __DIR__ . '/../..';


        if (\File::exists(app_path() . '/Http/crud_routes.php')) {
            require app_path('Http/crud_routes.php');//attach route file to project
        }

        require $packagePath . '/src/Http/crudgen_interface_routes.php';//attach routes for the crud creator interface.

        // Establish Views Namespace
        if (is_dir(base_path() . '/resources/views/crudgen')) {
            // The package views have been published - use those views.
            $this->loadViewsFrom(base_path() . '/resources/views/vendor/crudgen', 'crudgen');
        } else {
            // The package views have not been published. Use the defaults.
            $this->loadViewsFrom($packagePath . '/resources/views', 'crudgen');
        }

    }


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register LaravelCollective
        $this->app->register('Collective\Html\HtmlServiceProvider');

        // Load Facade Aliases
        $loader = AliasLoader::getInstance();

        // Register LaravelCollective Aliases
        $loader->alias('Form', 'Collective\Html\FormFacade');
        $loader->alias('HTML', 'Collective\Html\HtmlFacade');

        $this->commands(
            'Checkmate\CrudGen\Commands\ConsoleCommand',
            'Checkmate\CrudGen\Commands\PublishCommand',
            'Checkmate\CrudGen\Commands\ControllerCommand',
            'Checkmate\CrudGen\Commands\ModelCommand',
            'Checkmate\CrudGen\Commands\MigrationCommand',
            'Checkmate\CrudGen\Commands\ViewCommand',
            'Checkmate\CrudGen\Commands\LangCommand',
            'Checkmate\CrudGen\Commands\RouteCommand',
            'Checkmate\CrudGen\Commands\MenuCommand'
        );
    }
}