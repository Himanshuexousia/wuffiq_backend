<?php

namespace Checkmate\Crudgen\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Controller;
use App\Console\Commands;
use Session;

/**
 * Crud Generator Interface controller
 *
 * @license http://www.checkmate.com/licenses/crudgen
 *
 * @version 1.0.0
 */
class GeneratorController extends Controller
{

    /**
     * Display the form for crud generation.
     *
     * @return $this->view
     */
    public function show(Request $request)
    {
        if(!$request){
            return view('crudgen::generator');
        }else{
            return view('crudgen::generator', ['message' => $request->input('message'), 'status' => $request->input('status')]);
        }

    }

    /**
     * Generate Crud
     *
     * @return $this->view
     */
    public function generate(Request $request)
    {
        //echo print_r(Input::all());
        $fieldName = $request->input('field_name');
        $fieldType = $request->input('field_type');
        $className = $request->input('class_name');
        $routeGroup = $request->input('route_group');
        $middleware = $request->input('middleware');
        $required = $request->input('required');
        $fields = '';
        $requiredField = '';
        $namespace = 'App\Http\Controllers';

        $i = 0;
        $len = count($fieldName);
        foreach( $fieldName as $key => $field ) {
            if(is_array ($required)) {
                if (array_key_exists($key, $required)) {
                    $requiredField = '#required';
                };
            }

            if ($i == $len - 1) {
                // last
                $fields .= $field . '#' . $fieldType[$key] . $requiredField;
            }else{
                $fields .= $field . '#' . $fieldType[$key] . $requiredField . ',';
            }
            $requiredField = '';
            $i++;
        }

        /*
        --fields : Fields name for the form & model.
        --route : Include Crud route to routes.php? yes or no.
        --pk : The name of the primary key.
        --view-path : The name of the view path.
        --namespace : Namespace of the controller.
        --route-group : Prefix of the route group.
        */

        $exitCode = Artisan::call('crud:generate', [
            'name' => $className,
            '--fields' => $fields,
            '--route-group' => $routeGroup,
            '--namespace' => $namespace,
            '--middleware' => $middleware
        ]);

        //echo 'classname:' .  $className . 'fields:' . $fields . 'route group:' . $routeGroup . 'namespace:' . $namespace . 'middleware:' . $middleware;

        $message = '';
        $status = '';

        if($exitCode==1){//success
            $message = 'Crud Generator process finished';
            $status = 'alert-success';
        }elseif($exitCode==2){
            $message = 'Table ' . $className . ' already exists on database. Try a different class name';
            $status = 'alert-danger';
        }

        //return view('crudgen::generator', ['message' => $message, 'status' => $status]);//routes need to be refreshed by reloading page
        return redirect()->route('crudgen.show', ['message' => $message, 'status' => $status]);

    }
}
