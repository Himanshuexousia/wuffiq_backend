@extends('app')

@section('page-title')
    Buttons
@endsection

@section('page-css')
    <style>

    </style>
@endsection
@section('content-header')
    <h1>
        Buttons
    </h1>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Basic
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <h2 class="page-header with-border">Colored Buttons</h2>
            <div class="row text-md-center">
                <div class="col-md-2">
                    <button type="button" class="btn btn-block btn-primary">Primary</button>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-block btn-secondary">Secondary</button>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-block btn-success">Success</button>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-block btn-info">Info</button>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-block btn-danger">Danger</button>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-block btn-warning">Warning</button>
                </div>
            </div>
            <br />
            <br />
            <h2 class="page-header">Outline Buttons</h2>
            <div class="row text-md-center">
                <div class="col-md-2">
                    <button type="button" class="btn btn-block btn-primary-outline">Primary</button>
                    <br />
                    <code>btn btn-primary-outline</code>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-block btn-secondary-outline">Secondary</button>
                    <br />
                    <code>btn btn-secondary-outline</code>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-block btn-success-outline">Success</button>
                    <br />
                    <code>btn btn-success-outline</code>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-block btn-info-outline">Info</button>
                    <br />
                    <code>btn btn-info-outline</code>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-block btn-warning-outline">Warning</button>
                    <br />
                    <code>btn btn-warning-outline</code>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-block btn-danger-outline">Danger</button>
                    <br />
                    <code>btn btn-danger-outline</code>
                </div>
                <div class="col-md-2">
                </div>
            </div>
            <br />
            <br />
            <h2 class="page-header">Type Buttons</h2>
            <div class="row text-md-center">
                <div class="col-md-2">
                    <button type="button" class="btn btn-block btn-link">Link</button>
                    <br />
                    <code>btn btn-link</code>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-block btn-secondary">Normal</button>
                    <br />
                    <code>btn btn-secondary</code>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-block btn-secondary btn-lg">Large</button>
                    <br />
                    <code>.btn-lg</code>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-block btn-secondary btn-sm">Small</button>
                    <br />
                    <code>.btn-sm</code>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-block btn-secondary btn-flat">Flat</button>
                    <br />
                    <code>.btn-flat</code>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-block btn-secondary disabled">Disabled</button>
                    <br />
                    <code>.disabled</code>
                </div>
            </div>
            <br />
            <br />
            <h2 class="page-header">Circle Buttons</h2>
            <div class="row text-md-center">
                <div class="col-md-4">
                    <button type="button" class="btn btn-secondary btn-circle btn-sm"><i class="glyphicon glyphicon-ok"></i></button>
                    <button type="button" class="btn btn-primary btn-circle btn-sm"><i class="glyphicon glyphicon-list"></i></button>
                    <button type="button" class="btn btn-success btn-circle btn-sm"><i class="glyphicon glyphicon-link"></i></button>
                    <button type="button" class="btn btn-info btn-circle btn-sm"><i class="glyphicon glyphicon-ok"></i></button>
                    <button type="button" class="btn btn-warning btn-circle btn-sm"><i class="glyphicon glyphicon-remove"></i></button>
                    <button type="button" class="btn btn-danger btn-circle btn-sm"><i class="glyphicon glyphicon-heart"></i></button>
                </div>
                <div class="col-md-4">
                    <button type="button" class="btn btn-secondary btn-circle"><i class="glyphicon glyphicon-ok"></i></button>
                    <button type="button" class="btn btn-primary btn-circle"><i class="glyphicon glyphicon-list"></i></button>
                    <button type="button" class="btn btn-success btn-circle"><i class="glyphicon glyphicon-link"></i></button>
                    <button type="button" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-ok"></i></button>
                    <button type="button" class="btn btn-warning btn-circle"><i class="glyphicon glyphicon-remove"></i></button>
                    <button type="button" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-heart"></i></button>
                </div>
                <div class="col-md-4">
                    <button type="button" class="btn btn-secondary btn-circle btn-lg"><i class="glyphicon glyphicon-ok"></i></button>
                    <button type="button" class="btn btn-primary btn-circle btn-lg"><i class="glyphicon glyphicon-list"></i></button>
                    <button type="button" class="btn btn-success btn-circle btn-lg"><i class="glyphicon glyphicon-link"></i></button>
                    <button type="button" class="btn btn-info btn-circle btn-lg"><i class="glyphicon glyphicon-ok"></i></button>
                    <button type="button" class="btn btn-warning btn-circle btn-lg"><i class="glyphicon glyphicon-remove"></i></button>
                    <button type="button" class="btn btn-danger btn-circle btn-lg"><i class="glyphicon glyphicon-heart"></i></button>
                </div>
            </div>
            <br />
            <br />
            <h2 class="page-header">Checkbox and radio buttons</h2>
            <div class="row text-md-center">
                <div class="col-md-4">
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-primary active">
                            <input type="checkbox" checked autocomplete="off"> Check 1
                        </label>
                        <label class="btn btn-primary">
                            <input type="checkbox" autocomplete="off"> Check 2
                        </label>
                        <label class="btn btn-primary">
                            <input type="checkbox" autocomplete="off"> Check 3
                        </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-primary active">
                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Radio 1
                        </label>
                        <label class="btn btn-primary">
                            <input type="radio" name="options" id="option2" autocomplete="off"> Radio 2
                        </label>
                        <label class="btn btn-primary">
                            <input type="radio" name="options" id="option3" autocomplete="off"> Radio 3
                        </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-primary btn-circle active">
                            <input type="radio" name="options" id="option1" autocomplete="off" checked> <i class="glyphicon glyphicon-ok"></i>
                        </label>
                        <label class="btn btn-primary btn-circle">
                            <input type="radio" name="options" id="option2" autocomplete="off"> <i class="glyphicon glyphicon-align-left
"></i>
                        </label>
                        <label class="btn btn-primary btn-circle">
                            <input type="radio" name="options" id="option3" autocomplete="off"> <i class="glyphicon glyphicon-align-center"></i>
                        </label>
                        <label class="btn btn-primary btn-circle">
                            <input type="radio" name="options" id="option4" autocomplete="off"> <i class="glyphicon glyphicon-align-right"></i>
                        </label>
                        <label class="btn btn-primary btn-circle">
                            <input type="radio" name="options" id="option5" autocomplete="off"> <i class="glyphicon glyphicon-align-justify"></i>
                        </label>
                        <label class="btn btn-primary btn-circle">
                            <input type="radio" name="options" id="option6" autocomplete="off"> <i class="glyphicon glyphicon-picture"></i>
                        </label>
                        <label class="btn btn-primary btn-circle">
                            <input type="radio" name="options" id="option7" autocomplete="off"> <i class="glyphicon glyphicon-remove"></i>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Advanced
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            Block Button
                        </div>
                        <div class="card-block">
                            <button type="button" class="btn btn-secondary btn-block"><code>.btn-block</code></button>
                            <button type="button" class="btn btn-secondary btn-block active"><code>.btn-block .active</code></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            Icon Buttons<small>   <code>.btn-app</code></small>
                        </div>
                        <div class="card-block">
                            <button type="button" class="btn btn-secondary btn-app"><i class="fa fa-home"></i> Home </button>
                            <button type="button" class="btn btn-secondary btn-app"><i class="fa fa-list"></i> List </button>
                            <button type="button" class="btn btn-secondary btn-app"><i class="fa fa-play"></i>  Play</button>
                            <button type="button" class="btn btn-secondary btn-app"><i class="fa fa-edit"></i>  Edit</button>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-header">
                            Horizontal Grouped Buttons<small>   <code>.btn-group</code></small>
                        </div>
                        <div class="card-block">
                            <div class="btn-group">
                                <button type="button" class="btn btn-secondary">Home</button>
                                <button type="button" class="btn btn-secondary">About</button>
                                <button type="button" class="btn btn-secondary">Contact</button>
                                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    Other Link
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="#">Dropdown link</a></li>
                                    <li><a href="#">Dropdown link</a></li>
                                </ul>
                            </div>
                            <br />
                            <br />
                            <div class="btn-group">
                                <button type="button" class="btn btn-secondary"><i class="fa fa-font"></i></button>
                                <button type="button" class="btn btn-secondary"><i class="fa fa-bold"></i></button>
                                <button type="button" class="btn btn-secondary"><i class="fa fa-italic"></i></button>
                                <button type="button" class="btn btn-secondary"><i class="fa fa-align-left"></i></button>
                                <button type="button" class="btn btn-secondary"><i class="fa fa-align-center"></i></button>
                                <button type="button" class="btn btn-secondary"><i class="fa fa-align-right"></i></button>
                                <button type="button" class="btn btn-secondary"><i class="fa fa-picture-o"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-header">
                            Vertical Grouped Buttons<small>   <code>.btn-group-vertical</code></small>
                        </div>
                        <div class="card-block">
                            <div class="btn-group-vertical">
                                <button type="button" class="btn btn-secondary">Home</button>
                                <button type="button" class="btn btn-secondary">Contact</button>
                                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    Other Link
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="#">Dropdown link</a></li>
                                    <li><a href="#">Dropdown link</a></li>
                                </ul>
                            </div>
                            <div class="btn-group-vertical pull-md-right">
                                <button type="button" class="btn btn-secondary"><i class="fa fa-font"></i></button>
                                <button type="button" class="btn btn-secondary"><i class="fa fa-bold"></i></button>
                                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="#">Dropdown link</a></li>
                                    <li><a href="#">Dropdown link</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            Action Buttons with Dropdown
                        </div>
                        <div class="card-block text-md-center">
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-secondary">Action</button>
                                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-secondary"><i class="fa fa-home"></i></button>
                                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary"><i class="fa fa-home"></i>  Home</button>
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            Action Buttons
                        </div>
                        <div class="card-block">
                            <div class="input-group margin">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action</button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                                <input type="text" class="form-control" placeholder="With dropdown" />
                            </div>
                            <p></p>
                            <div class="input-group margin">
                                <input type="text" class="form-control" placeholder="Normal" />
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-danger">Action</button>
                                </div>
                            </div>
                            <div class="input-group margin">
                                <input type="text" class="form-control" placeholder="Flat" />
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-secondary btn-flat">Go!</button>
                                </span>
                            </div>
                            <div class="input-group margin">
                                <input type="text" class="form-control" placeholder="Search" />
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-secondary"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                            <div class="input-group margin">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-secondary"><i class="fa fa-dollar"></i></button>
                                </span>
                                <input type="text" class="form-control" placeholder="Money" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            Social Buttons
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-md-2">
                                    <a class="btn btn-block btn-social btn-bitbucket">
                                        <i class="fa fa-bitbucket"></i> Bitbucket
                                    </a>
                                    <a class="btn btn-block btn-social btn-dropbox">
                                        <i class="fa fa-dropbox"></i> Dropbox
                                    </a>
                                    <a class="btn btn-block btn-social btn-facebook">
                                        <i class="fa fa-facebook"></i> Facebook
                                    </a>
                                    <a class="btn btn-block btn-social btn-flickr">
                                        <i class="fa fa-flickr"></i> Flickr
                                    </a>
                                </div>
                                <div class="col-md-2">
                                    <a class="btn btn-block btn-social btn-foursquare">
                                        <i class="fa fa-foursquare"></i> Foursquare
                                    </a>
                                    <a class="btn btn-block btn-social btn-github">
                                        <i class="fa fa-github"></i> GitHub
                                    </a>
                                    <a class="btn btn-block btn-social btn-google">
                                        <i class="fa fa-google-plus"></i> Google
                                    </a>
                                    <a class="btn btn-block btn-social btn-instagram">
                                        <i class="fa fa-instagram"></i> Instagram
                                    </a>
                                </div>
                                <div class="col-md-2">
                                    <a class="btn btn-block btn-social btn-linkedin">
                                        <i class="fa fa-linkedin"></i> LinkedIn
                                    </a>
                                    <a class="btn btn-block btn-social btn-tumblr">
                                        <i class="fa fa-tumblr"></i> Tumblr
                                    </a>
                                    <a class="btn btn-block btn-social btn-twitter">
                                        <i class="fa fa-twitter"></i> Twitter
                                    </a>
                                    <a class="btn btn-block btn-social btn-vk">
                                        <i class="fa fa-vk"></i> VK
                                    </a>
                                </div>
                                <div class="col-md-2">
                                    <a class="btn btn-block btn-social btn-adn">
                                        <i class="fa fa-adn"></i> Adn
                                    </a>
                                    <a class="btn btn-block btn-social btn-microsoft">
                                        <i class="fa fa-windows"></i> Microsoft
                                    </a>
                                    <a class="btn btn-block btn-social btn-openid">
                                        <i class="fa fa-openid"></i> Open ID
                                    </a>
                                    <a class="btn btn-block btn-social btn-reddit">
                                        <i class="fa fa-reddit"></i> Reddit
                                    </a>
                                </div>
                                <div class="col-md-2">
                                    <a class="btn btn-block btn-social btn-soundcloud">
                                        <i class="fa fa-soundcloud"></i> Sound Cloud
                                    </a>
                                    <a class="btn btn-block btn-social btn-vimeo">
                                        <i class="fa fa-vimeo"></i> Vimeo
                                    </a>
                                    <a class="btn btn-block btn-social btn-yahoo">
                                        <i class="fa fa-yahoo"></i> Yahoo
                                    </a>
                                    <a class="btn btn-block btn-social btn-pinterest">
                                        <i class="fa fa-pinterest"></i> Pinterest
                                    </a>
                                </div>
                            </div>
                            <br>
                            <div class="text-md-center">
                                <a class="btn btn-circle btn-social-icon btn-bitbucket"><i class="fa fa-bitbucket"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-dropbox"><i class="fa fa-dropbox"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-flickr"><i class="fa fa-flickr"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-foursquare"><i class="fa fa-foursquare"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-github"><i class="fa fa-github"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-google"><i class="fa fa-google-plus"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-instagram"><i class="fa fa-instagram"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-linkedin"><i class="fa fa-linkedin"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-tumblr"><i class="fa fa-tumblr"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-vk"><i class="fa fa-vk"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-adn"><i class="fa fa-adn"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-microsoft"><i class="fa fa-windows"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-openid"><i class="fa fa-openid"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-reddit"><i class="fa fa-reddit"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-soundcloud"><i class="fa fa-soundcloud"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-vimeo"><i class="fa fa-vimeo"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-yahoo"><i class="fa fa-yahoo"></i></a>
                                <a class="btn btn-circle btn-social-icon btn-pinterest"><i class="fa fa-pinterest"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('page-scripts')

@endsection