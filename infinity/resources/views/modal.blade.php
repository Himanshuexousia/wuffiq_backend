@extends('app')

@section('page-title')
    Modals
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{asset('vendor/animate.css/animate.min.css')}}">
    <style>
        .modal-content.bg-modal-img {
            background: url('{{asset('img/image_example.jpg')}}');
            background-repeat: no-repeat;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
        .bg-modal-img > .modal-header, .bg-modal-img > .modal-footer {
            border: none;
        }
    </style>
@endsection
@section('content-header')
    <h1>
        Modals
    </h1>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Animated Modals
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-3 text-md-center">
                    <h5>Selected Modal Content</h5>
                    <div class="row">
                        <div class="col-xs-3 col-sm-6">
                            <button type="button" class="btn btn-secondary btn-app animatedStyle active" data-style="image">
                                <span class="fa fa-picture-o"></span>
                                Image
                            </button>
                        </div>
                        <div class="col-xs-3 col-sm-6">
                            <button type="button" class="btn btn-secondary btn-app animatedStyle" data-style="card">
                                <span class="fa fa-list-alt"></span>
                                Card
                            </button>
                        </div>
                        <div class="col-xs-3 col-sm-6">
                            <button type="button" class="btn btn-secondary btn-app animatedStyle" data-style="form">
                                <span class="fa fa-pencil-square-o"></span>
                                Form
                            </button>
                        </div>
                        <div class="col-xs-3 col-sm-6">
                            <button type="button" class="btn btn-secondary btn-app animatedStyle" data-style="normal">
                                <span class="fa fa-text-height"></span>
                                Normal
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 text-md-center">
                    <h5>Selected Modal Animation</h5>
                    <div class="row">
                        <div class="col-md-3">
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="bounce">	Bounce	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="flash">	Flash	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="pulse">	Pulse	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="rubberBand">	Rubberband	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="shake">	Shake	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="headShake">	Headshake	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="swing">	Swing	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="tada">	Tada	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="wobble">	Wobble	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="jello">	Jello	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="bounceIn">	Bouncein	</button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="bounceInDown">	Bounceindown	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="bounceInLeft">	Bounceinleft	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="bounceInRight">	Bounceinright	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="bounceInUp">	Bounceinup	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="fadeIn">	Fadein	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="fadeInDown">	Fadeindown	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="fadeInDownBig">	Fadeindownbig	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="fadeInLeft">	Fadeinleft	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="fadeInLeftBig">	Fadeinleftbig	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="fadeInRight">	Fadeinright	</button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="fadeInRightBig">	Fadeinrightbig	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="fadeInUp">	Fadeinup	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="fadeInUpBig">	Fadeinupbig	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="flipInX">	Flipinx	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="flipInY">	Flipiny	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="lightSpeedIn">	Lightspeedin	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="rotateIn">	Rotatein	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="rotateInDownLeft">	Rotateindownleft	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="rotateInDownRight">	Rotateindownright	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="rotateInUpLeft">	Rotateinupleft	</button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="rotateInUpRight">	Rotateinupright	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="rollIn">	Rollin	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="zoomIn">	Zoomin	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="zoomInDown">	Zoomindown	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="zoomInLeft">	Zoominleft	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="zoomInRight">	Zoominright	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="zoomInUp">	Zoominup	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="slideInDown">	Slideindown	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="slideInLeft">	Slideinleft	</button><button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="slideInRight">	Slideinright	</button>
                            <button type="button" class="animatedButton btn btn-block btn-secondary" data-toggle="modal" data-target="#modalAnimated" data-effect="slideInUp">	Slideinup	</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Bootstrap Modals
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-12 text-md-center">
                    <button id="defaultButton" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modalDefault">
                        Modal Default
                    </button>
                    <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modalSmall">
                        Modal Small
                    </button>
                    <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modalLarge">
                        Modal Large
                    </button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalPrimary">
                        Modal Primary
                    </button>
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalSuccess">
                        Modal Success
                    </button>
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalInfo">
                        Modal Info
                    </button>
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modalWarning">
                        Modal Warning
                    </button>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalDanger">
                        Modal Danger
                    </button>
                </div>
            </div>
            <br />
            <div class="bg-gray">
                <br />
                <div>
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Modal Title</h4>
                            </div>
                            <div class="modal-body">
                                This is an example of Modal.
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-labelledby="modalDefault">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal Title</h4>
                </div>
                <div class="modal-body">
                    This is an example of Modal.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal typeImage" id="modalAnimated" tabindex="-1" role="dialog" aria-labelledby="modalAnimated">
        <div class="modal-dialog" role="document">
            <div id="aniClass" class="modal-content bg-modal-img">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div style="height:400px;"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal typeCard" tabindex="-1" role="dialog" aria-labelledby="modalAnimated">
        <div class="modal-dialog" role="document">
            <div id="aniClass" class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <img class="card-img-top" data-src="https://placehold.it/307x180/6fe263/ffffff?text=Checkmate+Digital" style="height: 180px; width: 100%; display: block;" src="https://placehold.it/307x180/970202/ffffff?text=Checkmate+Digital" alt="Card image cap">
                        <div class="card-header text-xs-center">
                            Advanced
                        </div>
                        <div class="card-block">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text">Card Body Content.</p>
                            <a href="#" class="btn btn-block btn-danger">Button</a>
                        </div>
                        <div class="card-block">
                            <a href="#" class="card-link">Card link</a>
                            <a href="#" class="card-link">Another link</a>
                        </div>
                        <div class="card-footer text-xs-center">
                            Developed by Checkmate Digital
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal typeForm" tabindex="-1" role="dialog" aria-labelledby="modalAnimated">
        <div class="modal-dialog" role="document">
            <div id="aniClass" class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Modal Form</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 form-control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-2 form-control-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2">Radios</label>
                            <div class="col-sm-10">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
                                        Option one is this and that&mdash;be sure to include why it's great
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="gridRadios" id="gridRadios2" value="option2">
                                        Option two can be something else and selecting it will deselect option one
                                    </label>
                                </div>
                                <div class="radio disabled">
                                    <label>
                                        <input type="radio" name="gridRadios" id="gridRadios3" value="option3" disabled>
                                        Option three is disabled
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2">Checkbox</label>
                            <div class="col-sm-10">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> Check me out
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal">Send</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal typeNormal" tabindex="-1" role="dialog" aria-labelledby="modalAnimated">
        <div class="modal-dialog" role="document">
            <div id="aniClass" class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Modal Form</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <p>Integer magnis dolor, turpis dictumst, duis cursus aenean enim nec nunc lorem sit amet, scelerisque, vel, ridiculus sed elementum cum ac auctor augue vut. Dignissim elit, velit nunc sagittis tristique? Et ut augue odio duis eros etiam facilisis! Vel enim? Aliquet enim odio quis, phasellus porta natoque integer scelerisque nec.</p>

                    <p>Natoque lorem quis? Amet porttitor, magna! Ultrices phasellus ultrices egestas aliquet elit eu habitasse adipiscing lectus lorem, dictumst. Odio in turpis! Nascetur massa massa pellentesque cum mauris amet cum aenean? Vel montes integer! Rhoncus. Odio, natoque dapibus penatibus habitasse. Augue dictumst, dignissim, urna, ut ultrices, lacus placerat in. Pulvinar.</p>

                    <p>Turpis? Pid augue. Vel nisi a rhoncus aliquam ac dolor turpis in! Augue! Tristique nascetur duis sociis tincidunt natoque augue nec vel dignissim phasellus amet! Porttitor scelerisque? A parturient et natoque magnis mid sed diam, hac rhoncus nunc tincidunt odio! Integer. Ac et! Nec eu et in mus eros.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalSmall" tabindex="-1" role="dialog" aria-labelledby="modalSmall">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal Title</h4>
                </div>
                <div class="modal-body">
                    This is an example of Modal.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalLarge" tabindex="-1" role="dialog" aria-labelledby="modalLarge">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal Title</h4>
                </div>
                <div class="modal-body">
                    This is an example of Modal.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-primary fade" id="modalPrimary" tabindex="-1" role="dialog" aria-labelledby="modalPrimary">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal Title</h4>
                </div>
                <div class="modal-body">
                    This is an example of Modal.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-success fade" id="modalSuccess" tabindex="-1" role="dialog" aria-labelledby="modalPrimary">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal Title</h4>
                </div>
                <div class="modal-body">
                    This is an example of Modal.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-info fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="modalPrimary">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal Title</h4>
                </div>
                <div class="modal-body">
                    This is an example of Modal.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-warning fade" id="modalWarning" tabindex="-1" role="dialog" aria-labelledby="modalPrimary">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal Title</h4>
                </div>
                <div class="modal-body">
                    This is an example of Modal.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" id="modalDanger" tabindex="-1" role="dialog" aria-labelledby="modalPrimary">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal Title</h4>
                </div>
                <div class="modal-body">
                    This is an example of Modal.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script>
        $(function() {
            $( ".animatedButton" ).click(function(e) {
                var effect = e.currentTarget.dataset.effect;
                var classModal = $( "#aniClass").attr('class');
                var classModal = classModal.split("animated");
                $( "#aniClass" ).removeClass();
                $( "#aniClass" ).addClass(classModal[0]);
                $( "#aniClass" ).addClass('animated '+effect);
            });
            $( ".animatedStyle" ).click(function(e) {
                var style = e.currentTarget.dataset.style;
                $( ".animatedStyle" ).each(function( index ) {
                    $( this ).removeClass('active');
                });
                $( this).addClass('active');
                console.log(style);
                switch(style)
                {
                    case 'image':
                        $( ".typeImage" ).attr('id', 'modalAnimated');
                        $( ".typeCard" ).attr('id', '');
                        $( ".typeForm" ).attr('id', '');
                        $( ".typeNormal" ).attr('id', '');
                        $( ".typeImage > div > div" ).attr('id', 'aniClass');
                        $( ".typeCard > div > div" ).attr('id', '');
                        $( ".typeForm > div > div" ).attr('id', '');
                        $( ".typeNormal > div > div" ).attr('id', '');
                    break;
                    case 'normal':
                        $( ".typeImage" ).attr('id', '');
                        $( ".typeCard" ).attr('id', '');
                        $( ".typeForm" ).attr('id', '');
                        $( ".typeNormal" ).attr('id', 'modalAnimated');
                        $( ".typeImage > div > div" ).attr('id', '');
                        $( ".typeCard > div > div" ).attr('id', '');
                        $( ".typeForm > div > div" ).attr('id', '');
                        $( ".typeNormal > div > div" ).attr('id', 'aniClass');
                    break;
                    case 'card':
                        $( ".typeImage" ).attr('id', '');
                        $( ".typeCard" ).attr('id', 'modalAnimated');
                        $( ".typeForm" ).attr('id', '');
                        $( ".typeNormal" ).attr('id', '');
                        $( ".typeImage > div > div" ).attr('id', '');
                        $( ".typeCard > div > div" ).attr('id', 'aniClass');
                        $( ".typeForm > div > div" ).attr('id', '');
                        $( ".typeNormal > div > div" ).attr('id', '');
                    break;
                    case 'form':
                        $( ".typeImage" ).attr('id', '');
                        $( ".typeCard" ).attr('id', '');
                        $( ".typeForm" ).attr('id', 'modalAnimated');
                        $( ".typeNormal" ).attr('id', '');
                        $( ".typeImage > div > div" ).attr('id', '');
                        $( ".typeCard > div > div" ).attr('id', '');
                        $( ".typeForm > div > div" ).attr('id', 'aniClass');
                        $( ".typeNormal > div > div" ).attr('id', '');
                    break;
                    default:
                        $( ".typeImage" ).attr('id', 'modalAnimated');
                        $( ".typeCard" ).attr('id', '');
                        $( ".typeForm" ).attr('id', '');
                        $( ".typeNormal" ).attr('id', '');
                        $( ".typeImage > div > div" ).attr('id', 'aniClass');
                        $( ".typeCard > div > div" ).attr('id', '');
                        $( ".typeForm > div > div" ).attr('id', '');
                        $( ".typeNormal > div > div" ).attr('id', '');
                    break;
                }
            });
        });
    </script>
@endsection