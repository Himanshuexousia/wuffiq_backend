@extends('app')

@section('page-title')
    Peity
@endsection

@section('page-css')

@endsection
@section('content-header')
    <h1>
        Peity
    </h1>
@endsection

@section('content')

    <div class="card-columns">
        <div class="card">
            <div class="card-header">
                Bar
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <span class="bar">5,3,9,6,5,9,7,3,5,2</span> <code>5,3,9,6,5,9,7,3,5,2</code> <br />
                <span class="bar">5,3,2,-1,-3,-2,2,3,5,2</span> <code>5,3,2,-1,-3,-2,2,3,5,2</code> <br />
                <span class="bar">0,-3,-6,-4,-5,-4,-7,-3,-5,-2</span> <code>0,-3,-6,-4,-5,-4,-7,-3,-5,-2</code> <br />
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Doughnut
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="canvas-holder">
                    <span class="donut">1/5</span> <code>1/5</code> <br />
                    <span class="donut">226/360</span> <code>226/360</code> <br />
                    <span class="donut">0.52/1.561</span> <code>0.52/1.561</code> <br />
                    <span class="donut">1,4</span> <code>1,4</code> <br />
                    <span class="donut">226,134</span> <code>226,134</code> <br />
                    <span class="donut">0.52,1.041</span> <code>0.52,1.041</code> <br />
                    <span class="donut">1,2,3,2,2</span> <code>1,2,3,2,2</code> <br />
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Line
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="canvas-holder">
                    <span class="line">5,3,9,6,5,9,7,3,5,2</span> <code>5,3,9,6,5,9,7,3,5,2</code> <br />
                    <span class="line">5,3,2,-1,-3,-2,2,3,5,2</span> <code>5,3,2,-1,-3,-2,2,3,5,2</code> <br />
                    <span class="line">0,-3,-6,-4,-5,-4,-7,-3,-5,-2</span> <code>0,-3,-6,-4,-5,-4,-7,-3,-5,-2</code> <br />
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Pie
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="canvas-holder">
                    <span class="pie">1/5</span> <code>1/5</code> <br />
                    <span class="pie">226/360</span> <code>226/360</code> <br />
                    <span class="pie">0.52/1.561</span> <code>0.52/1.561</code> <br />
                    <span class="pie">1,4</span> <code>1,4</code> <br />
                    <span class="pie">226,134</span> <code>226,134</code> <br />
                    <span class="pie">0.52,1.041</span> <code>0.52,1.041</code> <br />
                    <span class="pie">1,2,3,2,2</span> <code>1,2,3,2,2</code> <br />
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Setting Colours
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div class="text-md-center">
                    <span class="bar-colours-1">5,3,9,6,5,9,7,3,5,2</span>
                    <span class="bar-colours-2">5,3,2,-1,-3,-2,2,3,5,2</span>
                    <span class="bar-colours-3">0,-3,-6,-4,-5,-4,-7,-3,-5,-2</span>
                    <span class="pie-colours-1">4,7,6,5</span>
                    <span class="pie-colours-2">5,3,9,6,5</span>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Custom Charts
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div class="text-md-center">
                    <h4 class="card-title">Data Attributes</h4>
                    <p class="data-attributes">
                        <span data-peity='{ "fill": ["red", "#eeeeee"],    "innerRadius": 10, "radius": 32 }'>1/7</span>
                        <span data-peity='{ "fill": ["orange", "#eeeeee"], "innerRadius": 14, "radius": 32 }'>2/7</span>
                        <span data-peity='{ "fill": ["yellow", "#eeeeee"], "innerRadius": 16, "radius": 32 }'>3/7</span>
                    </p>
                    <p class="data-attributes2">
                        <span data-peity='{ "fill": ["green", "#eeeeee"], "radius": 28 }'>4/7</span>
                        <span data-peity='{ "fill": ["blue", "#eeeeee"], "radius": 24 }'>5/7</span>
                        <span data-peity='{ "fill": ["indigo", "#eeeeee"], "radius": 20 }'>6/7</span>
                        <span data-peity='{ "fill": ["violet", "#eeeeee"], "radius": 16 }'>7/7</span>
                    </p>
                </div>
                <div class="text-md-center">
                    <h4 class="card-title">Updating Chart</h4>
                    <span class="updating-chart">5,3,9,6,5,9,7,3,5,2,5,3,9,6,5,9,7,3,5,2</span>
                </div>
                <div class="text-md-center">
                    <h4 class="card-title">Event</h4>
                    <ul class="list-unstyled">
                        <li>
                            <span class="graph"></span>
                            <select>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4" selected>4</option>
                                <option value="5">5</option>
                            </select>
                        </li>
                        <li>
                            <span class="graph"></span>
                            <select>
                                <option value="0">0</option>
                                <option value="1" selected>1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </li>
                        <li>
                            <span class="graph"></span>
                            <select>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3" selected>3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </li>
                    </ul>
                    <p id="notice">Nothing's happened yet.</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script src="{{asset('/vendor/peity/jquery.peity.min.js')}}"></script>
    <script>
        $(function() {

            $(".bar").peity("bar");
            $('.donut').peity('donut');
            $(".line").peity("line");
            $(".pie").peity("pie");
            $(".bar-colours-1").peity("bar", {
                fill: ["red", "green", "blue"]
            });

            $(".bar-colours-2").peity("bar", {
                fill: function(value) {
                    return value > 0 ? "green" : "red";
                }
            });

            $(".bar-colours-3").peity("bar", {
                fill: function(_, i, all) {
                    var g = parseInt((i / all.length) * 255);
                    return "rgb(255, " + g + ", 0)";
                }
            });

            $(".pie-colours-1").peity("pie", {
                fill: ["cyan", "magenta", "yellow", "black"]
            });

            $(".pie-colours-2").peity("pie", {
                fill: function(_, i, all) {
                    var g = parseInt((i / all.length) * 255);
                    return "rgb(255, " + g + ", 0)"
                }
            });
            $(".data-attributes span").peity("donut");
            $(".data-attributes2 span").peity("pie");
            var updatingChart = $(".updating-chart").peity("line", { width: 64 });

            setInterval(function() {
                var random = Math.round(Math.random() * 10);
                var values = updatingChart.text().split(",");
                values.shift();
                values.push(random);

                updatingChart
                        .text(values.join(","))
                        .change();
            }, 1000);
            $('select').change(function() {
                var text = $(this).val() + '/' + 5;

                $(this)
                        .siblings('span.graph')
                        .text(text)
                        .change();

                $('#notice').text('Chart updated: ' + text)
            }).change();

            $('span.graph').peity('pie');
            
        });
    </script>
@endsection