<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Infinity Admin - Login</title>
    <meta name="robots" content="index, follow">
    <meta name="keywords" content="app, admin, dashboard, laravel, php, html5, theme, template"/>
    <meta name="description" content="Infinity Admin, a fully responsive web app admin theme with charts, dashboards, landing pages and components. Fully customized with more than 140.000 layout combinations." />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="{{asset('vendor/tether/css/tether.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/icheck/skins/flat/blue.css')}}" />
    <!-- Main css -->
    <link href="{{asset('css/main.css')}}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        html, body {
            overflow: hidden;
        }
        body {
            min-height: 1200px;
            height: 100%;
        }
        .login-page {
            background: #474747;
        }
        .login-form {
            position: absolute;
            top: 200px;
            left: 50%;
            margin-left: -400px;
            width: 800px;
        }
        .login-form .card-info {
            border-top: 6px solid #003872;
        }
    </style>
</head>
<body class="content-light login-page">
<div class="login-form p10">
    <div class="row mb15 table-layout">
        <div class="col-xs-6 va-m pln">
            <a href="{{route('home')}}" class="logo">
                <img src="{{asset('img/infinity/infinity_logo_white.png')}}" height="80px">
            </a>
        </div>
        <div class="col-xs-6 text-sm-right va-b pr5">
            <div class="login-links">
                <ul class="nav nav-inline">
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#login" role="tab" aria-controls="login">Sign In</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#register" role="tab" aria-controls="register">Register</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card card-info p10">
        <div class="tab-content">
            <div class="tab-pane active" id="login" role="tabpanel">

                @if(config('checkauth.oauth_providers'))
                    <div class="row">
                            @if(in_array('google', config('checkauth.oauth_providers')))
                            <div class="col-md-4">
                                <a class="btn btn-block btn-social btn-flat btn-google" href="{{route('checkauth.oauth.login', ['provider' => 'google'])}}">
                                    <i class="fa fa-google-plus"></i>
                                    Google
                                </a>
                            </div>
                            @endif
                            @if(in_array('facebook', config('checkauth.oauth_providers')))
                                <div class="col-md-4">
                                    <a class="btn btn-block btn-social btn-flat btn-facebook" href="{{route('checkauth.oauth.login',['provider' => 'facebook'])}}">
                                        <i class="fa fa-facebook"></i> Facebook
                                    </a>
                                </div>
                            @endif
                            <div class="col-md-4">
                                <a class="btn btn-block btn-social btn-flat btn-twitter @if(!in_array('twitter', config('checkauth.oauth_providers'))) disabled @endif" href="#">
                                    <i class="fa fa-twitter"></i> Twitter
                                </a>
                            </div>
                    </div>
                @endif

                <hr />
                    <div class="card-block">
                        <div class="row">
                           @include('checkauth::partials.notifications')
                        </div>
                        <div class="row">
                            <div class="col-sm-7 pr30">
                                <p class="lead">
                                    Sign in
                                </p>
                                <form method="POST" action="#" accept-charset="UTF-8">
                                    <div class="form-group {{ ($errors->has('email')) ? 'has-danger' : '' }} has-feedback">
                                        <div class="row">
                                            <div class="col-xs-2 text-sm-right">
                                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                            </div>
                                            <div class="col-xs-10">
                                                <input type="email" class="form-control {{ ($errors->has('email')) ? 'form-control-danger' : '' }}" placeholder="Email" autofocus="autofocus" type="text" value="{{ (Input::old('email')) ? Input::old('email') : "user@infinity.com"}}"  name="email">
                                                @if($errors->has('email'))
                                                    <label class="form-control-label" for="email">{{$errors->first('email')}}</label>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group {{ ($errors->has('password')) ? 'has-danger' : '' }} has-feedback">
                                        <div class="row">
                                            <div class="col-xs-2 text-sm-right">
                                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                            </div>
                                            <div class="col-xs-10">
                                                <input class="form-control {{ ($errors->has('password')) ? 'form-control-danger' : '' }}" placeholder="Password" type="password"  name="password" value="infinity">
                                                @if($errors->has('password'))
                                                    <label class="form-control-label" for="email">{{$errors->first('password')}}</label>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <input type="checkbox"> Remember me
                                            <input name="_token" value="{{ csrf_token() }}" type="hidden">
                                        </div>

                                        <div class="col-xs-4">
                                            <button type="submit" class="btn btn-primary btn-block disabled">{{ trans('checkauth::pages.login') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-5 br-l br-grey pl30">
                                <h3 class="mb25"> Access To Your:</h3>
                                <p class="mb15">
                                    <span class="fa fa-check text-success pr5"></span> Unlimited Email Storage</p>
                                <p class="mb15">
                                    <span class="fa fa-check text-success pr5"></span> Unlimited Photo Sharing/Storage</p>
                                <p class="mb15">
                                    <span class="fa fa-check text-success pr5"></span> Unlimited Downloads</p>
                                <p class="mb15">
                                    <span class="fa fa-check text-success pr5"></span> Unlimited Service Tickets</p>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="alert alert-info" role="alert">
                                <strong>Heads up!</strong> This is just the interface design, a working sample will be presented in the next release.
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-pane" id="register" role="tabpanel">
                @if(config('checkauth.oauth_providers'))
                    <div class="row">
                        @if(in_array('google', config('checkauth.oauth_providers')))
                            <div class="col-md-4">
                                <a class="btn btn-block btn-social btn-flat btn-google" href="{{route('checkauth.oauth.login', ['provider' => 'google'])}}">
                                    <i class="fa fa-google-plus"></i>
                                    Google
                                </a>
                            </div>
                        @endif
                        @if(in_array('facebook', config('checkauth.oauth_providers')))
                            <div class="col-md-4">
                                <a class="btn btn-block btn-social btn-flat btn-facebook" href="{{route('checkauth.oauth.login',['provider' => 'facebook'])}}">
                                    <i class="fa fa-facebook"></i> Facebook
                                </a>
                            </div>
                        @endif
                        <div class="col-md-4">
                            <a class="btn btn-block btn-social btn-flat btn-twitter @if(!in_array('twitter', config('checkauth.oauth_providers'))) disabled @endif" href="#">
                                <i class="fa fa-twitter"></i> Twitter
                            </a>
                        </div>
                    </div>
                @endif
                <hr />
                <form method="post">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-7 pr30">
                                <div class="form-group has-feedback">
                                    <div class="row">
                                        <div class="col-xs-2 text-sm-right">
                                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                        </div>
                                        <div class="col-xs-10">
                                            <input type="text" class="form-control" placeholder="Full name">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <div class="row">
                                        <div class="col-xs-2 text-sm-right">
                                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                        </div>
                                        <div class="col-xs-10">
                                            <input type="email" class="form-control" placeholder="Email">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <div class="row">
                                        <div class="col-xs-2 text-sm-right">
                                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                        </div>
                                        <div class="col-xs-10">
                                            <input type="password" class="form-control" placeholder="Password">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <div class="row">
                                        <div class="col-xs-2 text-sm-right">
                                            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                                        </div>
                                        <div class="col-xs-10">
                                            <input type="password" class="form-control" placeholder="Retype password">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-8">
                                        <div class="checkbox icheck">
                                            <label>
                                                <input type="checkbox"> I agree to the <a href="#">terms</a>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <button type="submit" class="btn btn-primary btn-block disabled">Register</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 br-l br-grey pl30">
                                <h3 class="mb25"> Register to Have Access to:</h3>
                                <p class="mb15">
                                    <span class="fa fa-check text-success pr5"></span> Unlimited Email Storage</p>
                                <p class="mb15">
                                    <span class="fa fa-check text-success pr5"></span> Unlimited Photo Sharing/Storage</p>
                                <p class="mb15">
                                    <span class="fa fa-check text-success pr5"></span> Unlimited Downloads</p>
                                <p class="mb15">
                                    <span class="fa fa-check text-success pr5"></span> Unlimited Service Tickets</p>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="alert alert-info" role="alert">
                                <strong>Heads up!</strong> This is just the interface design, a working sample will be presented in the next release.
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('vendor/tether/js/tether.min.js')}}"></script>
<script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('vendor/icheck/icheck.min.js') }}"></script>
<script src="{{asset('js/jquery.particleground.min.js')}}"></script>
<script>
    $(function () {
        $('.content-light').particleground({
            dotColor: '#6d6d6d',
            lineColor: '#6d6d6d'
        });
        $('input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>