@extends('app')

@section('page-title')
    404 Error
@endsection

@section('page-css')
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/animate.css/animate.min.css')}}" />
@endsection

@section('content-header')

@endsection

@section('content')

    <div class="error-page text-sm-center">
        <h2 class="display-1 text-warning animated bounce">404 Error</h2>
        <br />
        <br />
        <div class="row">
            <div class="col-md-12">
                <h3>The page you requested was not found!</h3>
                <br />
                <br />
                <p class="lead">
                    It may have been moved to other place. Also check the URL accessed.
                </p>

                <form class="search-form">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Search for another Page">

                        <div class="input-group-btn">
                            <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('page-scripts')

@endsection