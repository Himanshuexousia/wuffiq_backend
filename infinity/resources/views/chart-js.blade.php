@extends('app')

@section('page-title')
    Chart.js
@endsection

@section('page-css')
    <style>

    </style>
@endsection
@section('content-header')
    <h1>
        Chart.js
    </h1>
@endsection

@section('content')

    <div class="card-columns">
        <div class="card">
            <div class="card-header">
                Bar
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="canvas-holder">
                <canvas id="chart-bar"></canvas>
                    </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Doughnut
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="canvas-holder">
                    <canvas id="chart-area"></canvas>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Line
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="canvas-holder">
                    <canvas id="chart-line"></canvas>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Pie
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="canvas-holder">
                    <canvas id="chart-pie"></canvas>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Polar Area Chart
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="canvas-holder">
                    <canvas id="chart-polar"></canvas>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Radar Chart
                <div class="pull-md-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-block">
                <div id="canvas-holder">
                    <canvas id="chart-radar"></canvas>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script src="{{asset('/vendor/chart-js/Chart.min.js')}}"></script>
    <script src="{{asset('/js/custom-chartjs.js')}}"></script>
@endsection