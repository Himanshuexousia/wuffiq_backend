@extends('app')

@section('content')
<div class="card">
    <div class="card-header">
        Product {{ $product->id }}
                <a href="{{ url('admin/products/' . $product->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Product"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['admin/products', $product->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete Product',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ));!!}
                {!! Form::close() !!}
    </div>
    <div class="card-block">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>ID</th><td>{{ $product->id }}</td>
                </tr>
                <tr><th> Name </th><td> {{ $product->Name }} </td></tr><tr><th> Description </th><td> {{ $product->Description }} </td></tr><tr><th> Price </th><td> {{ $product->Price }} </td></tr>
            </tbody>
        </table>
    </div>
</div>

@endsection
