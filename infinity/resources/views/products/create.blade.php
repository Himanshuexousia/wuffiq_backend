@extends('app')

@section('content')
<div class="card">
    <div class="card-header">
       Create New Product
    </div>
    <div class="card-block">
        {!! Form::open(['url' => '/admin/products', 'class' => 'form-horizontal']) !!}

                    <div class="form-group row {{ $errors->has('Name') ? 'has-error' : ''}}">
                {!! Form::label('Name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('Name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('Name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group row {{ $errors->has('Description') ? 'has-error' : ''}}">
                {!! Form::label('Description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('Description', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('Description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group row {{ $errors->has('Price') ? 'has-error' : ''}}">
                {!! Form::label('Price', 'Price', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('Price', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('Price', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


        <div class="form-group row">
            <div class="col-sm-offset-3 col-sm-3">
                {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
            </div>
        </div>
        {!! Form::close() !!}

        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
    </div>
</div>

@endsection