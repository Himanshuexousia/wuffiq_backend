@extends('app')

@section('page-title')
    Mailbox - Message
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{asset('vendor/animate.css/animate.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/icheck/skins/flat/blue.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/bootstrap3-wysihtml5/bootstrap3-wysihtml5.min.css')}}" />
    <link rel="stylesheet" href="{{asset('vendor/select2/css/select2.min.css')}}">
    <style>
        .content-light .card .card-header .btn {
            background-color: inherit;
            border-color: inherit;
            color: inherit;
        }
        .attachment-icon, .attachment-name {
            margin-right: 25px;
        }
        .attachment-icon img {
            max-height: 100px;
        }
        .read-info {
            padding-bottom: 15px;
            border-bottom: 2px inset gray;
            margin-bottom: 15px;
        }
    </style>
@endsection

@section('content-header')
    <h1>
        Mailbox - Message
    </h1>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-block">
                    <h4 class="lead text-md-center"> Menu </h4>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <a href="#">
                                <i class="fa fa-gear"></i>
                                Email Settings
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{route('mail-inbox')}}">
                                <i class="fa fa-envelope"></i>
                                Inbox
                                <span class="label badge-warning">2</span>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <i class="fa fa-user"></i>
                                Friends
                                <span class="label badge-warning">6</span>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <i class="fa fa-trash-o"></i>
                                Spam
                                <span class="label badge-muted">13</span>
                            </a>
                        </li>
                    </ul>
                    <br />
                    <br />

                    <h4 class="lead text-md-center"> Labels </h4>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <a href="#">
                                Danger
                                <span class="pull-md-right label label-pill label-danger">7</span>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                Success
                                <span class="pull-md-right label label-pill label-success">9</span>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                Primary
                                <span class="pull-md-right label label-pill label-primary">13</span>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                Warning
                                <span class="pull-md-right label label-pill label-warning">15</span>
                            </a>
                        </li>
                    </ul>
                    <br />
                    <br />

                    <button type="button" class="btn btn-block btn-secondary" data-toggle="modal" data-target="#modalCompose">Quick Compose</button>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Read Mail</h3>
                    <div class="pull-md-right">
                        <div class="btn-group" data-toggle="buttons">
                            <button class="btn btn-default btn-sm btn-circle" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></button>
                            <button class="btn btn-default btn-sm btn-circle" data-toggle="tooltip" title="Print"><i class="fa fa-print"></i></button>
                        </div>
                        <div class="btn-group" data-toggle="buttons">
                            <a href="#" class="btn btn-default" data-toggle="tooltip" title="Previous"><i class="fa fa-chevron-left"></i></a>
                            <a href="#" class="btn btn-default" data-toggle="tooltip" title="Next"><i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="card-block">
                            <div class="read-info">
                                <h3>Message Subject</h3>
                                <h5>From: <a href="mailto:dev@checkmatedigital.com">dev@checkmatedigital.com</a> <span class="read-time pull-md-right">15 Feb. 2015 11:03 PM</span></h5>
                            </div>
                            <div class="read-message">
                                <p>Hello Renato,</p>
                                <p>Sit phasellus pellentesque! Integer odio vel! Magna turpis. Urna aliquam in penatibus, ridiculus hac magna ridiculus lorem ac? Pulvinar, amet, integer, nec, ac nec egestas lorem a integer! Montes porttitor, etiam et elementum a ac porttitor? Facilisis aliquam tortor, nec. Integer porta, ultricies natoque rhoncus, mauris elementum quis? Adipiscing eros.</p>

                                <p>Scelerisque amet non turpis? Porta! Phasellus vel. Duis nunc porttitor, pid risus ut ultricies ac, habitasse augue tortor. Magna, ultricies aenean cras cursus sit! Platea, integer platea tristique eu phasellus ut urna tristique! Mus adipiscing ac integer etiam in, dignissim nisi rhoncus scelerisque adipiscing, dis vel urna, diam porttitor.</p>

                                <p>Magnis! Ut. Dis, tristique platea, cras, eu, velit dapibus! Nec vel! Turpis nunc? Vel nisi! Rhoncus sit et placerat, tincidunt et est dignissim ultrices! Magna et turpis integer, eros etiam placerat! Tempor tortor vel, enim quis augue? Enim! Amet aenean tincidunt cursus in tristique cum rhoncus porta aenean in.</p>

                                <p>Quis! In, egestas magnis ultrices! Pid ac! Lacus, etiam cras magnis! Non pellentesque vel? Scelerisque in pulvinar, nec mattis ac etiam, odio natoque proin parturient nec cum est eu massa purus dignissim! Diam, sociis tortor, porttitor ut! Ut, pulvinar magna duis. Elit arcu, nisi, non purus enim dolor sed.</p>

                                <p>Non in elementum turpis adipiscing tristique? Pellentesque vel adipiscing, nisi porta dis in? Integer egestas nisi dapibus sociis et habitasse integer eu mid! Augue! Porta diam odio, dapibus dis cras urna dis vut purus! Etiam sit in sagittis mid, porttitor lundium magnis, cras, odio porttitor, cras pid, ac et.</p>

                                <p>Thanks,
                                <br />
                                Daniel</p>
                            </div>
                        </div>
                        <div class="card-footer">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <div class="attachment-info">
                                        <span class="attachment-icon"><i class="fa fa-file-pdf-o"></i></span>
                                        <a href="#" class="attachment-name">
                                            <i class="fa fa-paperclip"></i> report.pdf
                                        </a>
                                        <span class="attachment-size">
                                            1,245 KB
                                            <a href="#" class="btn btn-default btn-xs pull-md-right">
                                                <i class="fa fa-cloud-download"></i>
                                            </a>
                                        </span>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="attachment-info">
                                        <span class="attachment-icon"><i class="fa fa-file-word-o"></i></span>
                                        <a href="#" class="attachment-name">
                                            <i class="fa fa-paperclip"></i> resume.doc
                                        </a>
                                        <span class="attachment-size">
                                            745 B
                                            <a href="#" class="btn btn-default btn-xs pull-md-right">
                                                <i class="fa fa-cloud-download"></i>
                                            </a>
                                        </span>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="attachment-info">
                                        <span class="attachment-icon"><img src="{{asset('img/photo1.png')}}" alt="Attachment"></span>
                                        <a href="#" class="attachment-name">
                                            <i class="fa fa-camera"></i> photo1.png
                                        </a>
                                        <span class="attachment-size">
                                            2.67 MB
                                            <a href="#" class="btn btn-default btn-xs pull-md-right">
                                                <i class="fa fa-cloud-download"></i>
                                            </a>
                                        </span>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="attachment-info">
                                        <span class="attachment-icon"><img src="{{asset('img/photo2.png')}}" alt="Attachment"></span>
                                        <a href="#" class="attachment-name">
                                            <i class="fa fa-camera"></i> photo2.png
                                        </a>
                                        <span class="attachment-size">
                                            1.9 MB
                                            <a href="#" class="btn btn-default btn-xs pull-md-right">
                                                <i class="fa fa-cloud-download"></i>
                                            </a>
                                        </span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

    <div class="modal fade" id="modalCompose" tabindex="-1" role="dialog" aria-labelledby="modalCompose">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Quick Compose</h4>
                </div>
                <div class="modal-body animate slideUp">
                    <form>
                        <div class="form-group row">
                            <label for="title" class="col-sm-2 form-control-label">Title</label>
                            <div class="col-sm-10">
                                <input type="title" class="form-control" id="title" placeholder="Title">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="to" class="col-sm-2 form-control-label">To</label>
                            <div class="col-sm-10">
                                <select id="to" class="form-control" style="width: 100%">
                                    <option value="Checkmate Digital">Checkmate Digital</option>
                                    <option value="Checkmate Digital2">Checkmate Digital2</option>
                                    <option value="Checkmate Digital3">Checkmate Digital3</option>
                                    <option value="Checkmate Digital4">Checkmate Digital4</option>
                                    <option value="Other Checkmate Digital">Other Checkmate Digital</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="message" class="col-sm-2 form-control-label">Message</label>
                            <div class="col-sm-10">
                                <textarea type="message" class="form-control" id="message" placeholder="Message"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Send</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('page-scripts')
    <script src="{{asset('vendor/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap3-wysihtml5/templates.js')}}"></script>
    <script src="{{asset('vendor/bootstrap3-wysihtml5/commands.js')}}"></script>
    <script src="{{asset('vendor/select2/js/select2.full.min.js')}}"></script>
    <script src="{{asset('vendor/icheck/icheck.js')}}"></script>
    <script type="text/javascript">
        $(function() {

            "use strict";

            $('input[type=checkbox]').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
            $('#message').wysihtml5();
            $("#to").select2();

        });

        $('.star a i').on('click', function(e){
            e.preventDefault();
            if($( this ).hasClass('fa-star-o'))
            {
                $( this ).removeClass('fa-star-o');
                $( this ).addClass('fa-star');
            }
            else
            {
                $( this ).removeClass('fa-star');
                $( this ).addClass('fa-star-o');
            }
        });
    </script>
@endsection