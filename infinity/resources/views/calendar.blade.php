@extends('app')

@section('page-title')
   Calendar
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{asset('vendor/fullcalendar/css/fullcalendar.css')}}">
    <style>
        .pallete-item {
            width: 140px;
            float: left;
            margin: 0 0 20px 20px
        }

        .palette {
            font-size: 14px;
            line-height: 1.214;
            color: #fff;
            margin: 0;
            padding: 15px;
            text-transform: uppercase
        }

        .palette dt, .palette dd {
            line-height: 1.429
        }

        .palette dt {
            display: block;
            font-weight: 700;
            opacity: .8;
            filter: alpha(opacity=80)
        }

        .palette dd {
            font-weight: 300;
            margin-left: 0;
            opacity: .8;
            filter: alpha(opacity=80);
            -webkit-font-smoothing: subpixel-antialiased
        }

        .palette-white {
            background-color: #FFF !important
        }

        .palette-primary {
            background-color: #418BCA !important
        }

        .palette-success {
            background-color: #00bc8c !important
        }

        .palette-info {
            background-color: #5bc0de !important
        }

        .palette-warning {
            background-color: #F89A14 !important
        }

        .palette-danger {
            background-color: #EF6F6C !important
        }

        .palette-default {
            background-color: #A9B6BC !important
        }

        .external-event {
            margin: 10px 0;
            padding: 3px 5px;
            border-radius: 2px;
            cursor: pointer;
            display: inline-block;
            margin: 0 5px 5px 0;
        }

        .panel .panel-body .fc-header-title h2 {
            font-size: 15px;
            line-height: 1.6em;
            color: #666;
            margin-left: 10px;
        }
    </style>
@endsection

@section('content-header')
    <h1>
       Calendar
    </h1>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">
                    Draggable Events
                    <div class="pull-md-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-block">
                    <div id='external-events'>
                        <div class='external-event palette-warning'>Team Out</div>
                        <div class='external-event palette-primary'>Product Seminar</div>
                        <div class='external-event palette-danger'>Client Meeting</div>
                        <div class='external-event palette-info'>Repeating Event</div>
                        <div class='external-event palette-success'>Anniversary Celebrations</div>
                        <p class="well no-border no-radius">
                            <input type='checkbox' id='drop-remove' style="opacity:1 !important" />
                            <label for='drop-remove'>remove after drop</label>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    FullCalendar
                    <div class="pull-md-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-block">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('page-scripts')
    <script src="{{asset('vendor/fullcalendar/fullcalendar.min.js')}}"></script>
    <script src="{{asset('vendor/fullcalendar/calendarcustom.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            /* initialize the external events
             -----------------------------------------------------------------*/
            function ini_events(ele) {
                ele.each(function() {

                    // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                    // it doesn't need to have a start or end
                    var eventObject = {
                        title: $.trim($(this).text()) // use the element's text as the event title
                    };

                    // store the Event Object in the DOM element so we can get to it later
                    $(this).data('eventObject', eventObject);

                    // make the event draggable using jQuery UI
                    $(this).draggable({
                        zIndex: 1070,
                        revert: true, // will cause the event to go back to its
                        revertDuration: 0 //  original position after the drag
                    });

                });
            }
            ini_events($('#external-events div.external-event'));

            /* initialize the calendar
             -----------------------------------------------------------------*/
            //Date for the calendar events (dummy data)
            var date = new Date();
            var d = date.getDate(),
                    m = date.getMonth(),
                    y = date.getFullYear();
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                buttonText: {
                    prev: "<span class='fa fa-caret-left'></span>",
                    next: "<span class='fa fa-caret-right'></span>",
                    today: 'today',
                    month: 'month',
                    week: 'week',
                    day: 'day'
                },
                //Random events
                events: [{
                    title: 'Team Out',
                    start: new Date(y, m, 2),
                    backgroundColor: "#F89A14"
                }, {
                    title: 'Client Meeting',
                    start: new Date(y, m, d - 2),
                    end: new Date(y, m, d - 5),
                    backgroundColor: "#418BCA"
                }, {
                    title: 'Repeating Event',
                    start: new Date(y, m, 6)
                }, {
                    title: 'Birthday Party',
                    start: new Date(y, m, 12),
                    backgroundColor: "#00bc8c"
                }, {
                    title: 'Product Seminar',
                    start: new Date(y, m, 16),
                    backgroundColor: "#A9B6BC"
                }, {
                    title: 'Anniversary Celebrations',
                    start: new Date(y, m, 26),
                    backgroundColor: "#F89A14"
                }, {
                    title: 'Client Meeting',
                    start: new Date(y, m, 10),
                    backgroundColor: "#5bc0de"
                }],
                editable: true,
                droppable: true, // this allows things to be dropped onto the calendar !!!
                drop: function(date, allDay) { // this function is called when something is dropped

                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $(this).data('eventObject');

                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);

                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.allDay = allDay;
                    copiedEventObject.backgroundColor = $(this).css("background-color");
                    copiedEventObject.borderColor = $(this).css("border-color");

                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove();
                    }

                }
            });

            /* ADDING EVENTS */
            var currColor = "#418BCA"; //default
            //Color chooser button
            var colorChooser = $("#color-chooser-btn");
            $("#color-chooser > li > a").click(function(e) {
                e.preventDefault();
                //Save color
                currColor = $(this).css("background-color");
                //Add color effect to button
                colorChooser
                        .css({
                            "background-color": currColor,
                            "border-color": currColor
                        })
                        .html($(this).text() + ' <span class="caret"></span>');
            });
            $("#add-new-event").click(function(e) {
                e.preventDefault();
                //Get value and make sure it is not null
                var val = $("#new-event").val();
                if (val.length == 0) {
                    return;
                }

                //Create event
                var event = $("<div />");
                event.css({
                    "background-color": currColor,
                    "border-color": currColor,
                    "color": "#fff"
                }).addClass("external-event");
                event.html(val);
                $('#external-events').prepend(event);

                //Add draggable funtionality
                ini_events(event);

                //Remove event from text input
                $("#new-event").val("");
            });
        });
    </script>
@endsection