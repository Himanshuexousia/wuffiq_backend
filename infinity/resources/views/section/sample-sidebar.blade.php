<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="treeview {{ ( Request::is( 'dashboard/*')  ? ' active' : '') }}">
                <a href="{{ route('home')  }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-md-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ (Request::is( 'dashboard/business') ? 'class="active"' : '') }}><a href="{{route('dashboard-business')}}"><i class="fa fa-circle-o{{ (Request::is( 'dashboard/business') ? ' text-yellow' : '') }}"></i>Business</a></li>
                    <li {{ (Request::is( 'dashboard/sales') ? 'class="active"' : '') }}><a href="{{route('dashboard-sales')}}"><i class="fa fa-circle-o{{ (Request::is( 'dashboard/sales') ? ' text-yellow' : '') }}"></i>Sales</a></li>
                    <li {{ (Request::is( 'dashboard/marketing') ? 'class="active"' : '') }}><a href="{{route('dashboard-marketing')}}"><i class="fa fa-circle-o{{ (Request::is( 'dashboard/marketing') ? ' text-yellow' : '') }}"></i>Marketing</a></li>
                    <li {{ (Request::is( 'dashboard/social') ? 'class="active"' : '') }}><a href="{{route('dashboard-social')}}"><i class="fa fa-circle-o{{ (Request::is( 'dashboard/social') ? ' text-yellow' : '') }}"></i>Social Media</a></li>
                    <li {{ (Request::is( 'dashboard/projects') ? 'class="active"' : '') }}><a href="{{route('dashboard-projects')}}"><i class="fa fa-circle-o{{ (Request::is( 'dashboard/projects') ? ' text-yellow' : '') }}"></i>Projects</a></li>
                </ul>
            </li>
            <li class="treeview {{ ( Request::is( 'front')  ? ' active' : '') }}">
                <a href="{{ route('front')  }}" target="_blank">
                    <i class="fa fa-paint-brush"></i>
                    <span>Front-end</span>
                    <span class="label label-default fire pull-md-right">HOT</span>
                </a>
            </li>
            <li class="treeview {{ ( Request::is( 'admin/crudgen/generator')  ? ' active' : '') }}">
                <a href="{{ route('crudgen.show')  }}">
                    <i class="fa fa-gear"></i>
                    <span>Crud Generator</span>
                    <span class="label label-default fire pull-md-right">HOT</span>
                </a>
            </li>
            @include('crud_menu')<!--crud generator classes menu-->
            <li class="treeview {{ ( Request::is( 'auth/*') || Request::is( 'users') || Request::is( 'roles')    ? 'active' : '') }}">
                <a href="{{ route('home')  }}">
                    <i class="fa fa-lock"></i> <span>Authentication</span> <i class="fa fa-angle-left pull-md-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ (Request::is( 'auth/sample') ? 'class="active"' : '') }}><a href="{{route('auth-sample')}}"><i class="fa fa-circle-o{{ (Request::is( 'auth/sample') ? ' text-yellow' : '') }}"></i>Sample page</a></li>
                    @if($user = Sentinel::check())
                        <li {{ (Request::is( 'users') ? 'class="active"' : '') }}><a href="{{route('checkauth.users.index')}}"><i class="fa fa-circle-o{{ (Request::is( 'users') ? ' text-yellow' : '') }}"></i>List Users</a></li>
                        <li {{ (Request::is( 'roles') ? 'class="active"' : '') }}><a href="{{route('checkauth.roles.index')}}"><i class="fa fa-circle-o{{ (Request::is( 'roles') ? ' text-yellow' : '') }}"></i>List Roles</a></li>
                        <li {{ (Request::is( 'logout') ? 'class="active"' : '') }}><a href="{{route('checkauth.logout')}}"><i class="fa fa-circle-o{{ (Request::is( 'logout') ? ' text-yellow' : '') }}"></i>Logout</a></li>
                    @else
                        <li {{ (Request::is( 'login') ? 'class="active"' : '') }}><a href="{{route('checkauth.login')}}"><i class="fa fa-circle-o{{ (Request::is( 'login') ? ' text-yellow' : '') }}"></i>Login</a></li>
                        <li {{ (Request::is( 'register') ? 'class="active"' : '') }}><a href="{{route('checkauth.register.form')}}"><i class="fa fa-circle-o{{ (Request::is( 'register') ? ' text-yellow' : '') }}"></i>Register</a></li>
                    @endif
                </ul>
            </li>
            <li class="treeview">
                <a href="#" data-toggle="control-sidebar">
                    <i class="fa fa-paint-brush"></i>
                    <span>Layout Options</span>
                    <span class="label label-default fire pull-md-right">HOT</span>
                </a>
            </li>
            <!-- will be on next release
            <li>
                <a href="../widgets.html">
                    <i class="fa fa-th"></i> <span>Widgets</span>
                    <small class="label pull-md-right bg-green">new</small>
                </a>
            </li>
            -->
            <li class="treeview{{ (Request::is( 'charts/*') ? ' active' : '') }}">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>Charts</span>
                    <i class="fa fa-angle-left pull-md-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ (Request::is( 'charts/google-chart-tools') ? 'class="active"' : '') }}><a href="{{route('google-chart-tools')}}"><i class="fa fa-circle-o{{ (Request::is( 'charts/google-chart-tools') ? ' text-yellow' : '') }}"></i> Google Chart Tools</a></li>
                    <li {{ (Request::is( 'charts/chart-js') ? 'class="active"' : '') }}><a href="{{route('chart-js')}}"><i class="fa fa-circle-o{{ (Request::is( 'charts/chart-js') ? ' text-yellow' : '') }}"></i> ChartJS</a></li>
                    <li {{ (Request::is( 'charts/peity') ? 'class="active"' : '') }}><a href="{{route('peity')}}"><i class="fa fa-circle-o{{ (Request::is( 'charts/peity') ? ' text-yellow' : '') }}"></i> Peity</a></li>
                </ul>
            </li>
            <li class="treeview{{ (Request::is( 'ui-elements/*') ? ' active' : '') }}">
                <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>UI Elements</span>
                    <i class="fa fa-angle-left pull-md-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ (Request::is( 'ui-elements/general') ? 'class="active"' : '') }}><a href="{{route('general')}}"><i class="fa fa-circle-o{{ (Request::is( 'ui-elements/general') ? ' text-yellow' : '') }}"></i> General</a></li>
                    <li {{ (Request::is( 'ui-elements/grid') ? 'class="active"' : '') }}><a href="{{route('grid')}}"><i class="fa fa-circle-o{{ (Request::is( 'ui-elements/grid') ? ' text-yellow' : '') }}"></i> Grid System</a></li>
                    <li {{ (Request::is( 'ui-elements/icons') ? 'class="active"' : '') }}><a href="{{route('icons')}}"><i class="fa fa-circle-o{{ (Request::is( 'ui-elements/icons') ? ' text-yellow' : '') }}"></i> Icons</a></li>
                    <li {{ (Request::is( 'ui-elements/buttons') ? 'class="active"' : '') }}><a href="{{route('buttons')}}"><i class="fa fa-circle-o{{ (Request::is( 'ui-elements/buttons') ? ' text-yellow' : '') }}"></i> Buttons</a></li>
                    <li {{ (Request::is( 'ui-elements/sliders') ? 'class="active"' : '') }}><a href="{{route('sliders')}}"><i class="fa fa-circle-o{{ (Request::is( 'ui-elements/sliders') ? ' text-yellow' : '') }}"></i> Sliders</a></li>
                    <li {{ (Request::is( 'ui-elements/timeline') ? 'class="active"' : '') }}><a href="{{route('timeline')}}"><i class="fa fa-circle-o{{ (Request::is( 'ui-elements/timeline') ? ' text-yellow' : '') }}"></i> Timeline</a></li>
                    <li {{ (Request::is( 'ui-elements/modal') ? 'class="active"' : '') }}><a href="{{route('modal')}}"><i class="fa fa-circle-o{{ (Request::is( 'ui-elements/modal') ? ' text-yellow' : '') }}"></i> Modals</a></li>
                    <li {{ (Request::is( 'ui-elements/tabs') ? 'class="active"' : '') }}><a href="{{route('tabs')}}"><i class="fa fa-circle-o{{ (Request::is( 'ui-elements/tabs') ? ' text-yellow' : '') }}"></i> Tabs</a></li>
                    <li {{ (Request::is( 'ui-elements/cards') ? 'class="active"' : '') }}><a href="{{route('cards')}}"><i class="fa fa-circle-o{{ (Request::is( 'ui-elements/cards') ? ' text-yellow' : '') }}"></i> Cards (Old "Panels")</a></li>
                    <li {{ (Request::is( 'ui-elements/alerts') ? 'class="active"' : '') }}><a href="{{route('alerts')}}"><i class="fa fa-circle-o{{ (Request::is( 'ui-elements/alerts') ? ' text-yellow' : '') }}"></i> Alerts, Tooltips, Popovers</a></li>
                    <li {{ (Request::is( 'ui-elements/bootstrap-misc') ? 'class="active"' : '') }}><a href="{{route('bootstrap-misc')}}"><i class="fa fa-circle-o{{ (Request::is( 'ui-elements/bootstrap-misc') ? ' text-yellow' : '') }}"></i> Bootstrap Miscellaneous</a></li>
                </ul>
            </li>
            <li class="treeview{{ (Request::is( 'form/*') ? ' active' : '') }}">
                <a href="#">
                    <i class="fa fa-edit"></i> <span>Forms</span>
                    <i class="fa fa-angle-left pull-md-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ (Request::is( 'form/general') ? 'class="active"' : '') }}><a href="{{route('form-general')}}"><i class="fa fa-circle-o{{ (Request::is( 'form/general') ? ' text-yellow' : '') }}"></i> General Elements</a></li>
                    <li {{ (Request::is( 'form/advanced') ? 'class="active"' : '') }}><a href="{{route('form-advanced')}}"><i class="fa fa-circle-o{{ (Request::is( 'form/advanced') ? ' text-yellow' : '') }}"></i> Advanced Elements</a></li>
                    <li {{ (Request::is( 'form/editor') ? 'class="active"' : '') }}><a href="{{route('form-editor')}}"><i class="fa fa-circle-o{{ (Request::is( 'form/advanced') ? ' text-yellow' : '') }}"></i> Editor</a></li>
                </ul>
            </li>
            <li class="treeview{{ (Request::is( 'table/*')  ? ' active' : '') }}">
                <a href="#">
                    <i class="fa fa-table"></i> <span>Tables</span>
                    <i class="fa fa-angle-left pull-md-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ (Request::is( 'table/simple') ? 'class="active"' : '') }}><a href="{{route('table-simple')}}"><i class="fa fa-circle-o{{ (Request::is( 'table/simple') ? ' text-yellow' : '') }}"></i> Simple Tables</a></li>
                    <li {{ (Request::is( 'table/data') ? 'class="active"' : '') }}><a href="{{route('table-data')}}"><i class="fa fa-circle-o{{ (Request::is( 'table/data') ? ' text-yellow' : '') }}"></i> Data tables</a></li>
                </ul>
            </li>
            <li {{ (Request::is( 'calendar') ? 'class="active"' : '') }}>
                <a href="{{route('calendar')}}">
                    <i class="fa fa-calendar{{ (Request::is( 'table/data') ? ' text-yellow' : '') }}"></i> <span>Calendar</span>
                </a>
            </li>
            <li  class="treeview{{ (Request::is( 'mailbox/*') ? ' active' : '') }}">
                <a href="#">
                    <i class="fa fa-envelope"></i> <span>Mailbox</span>
                    <i class="fa fa-angle-left pull-md-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ (Request::is( 'mailbox/inbox') ? 'class="active"' : '') }}><a href="{{route('mail-inbox')}}"><i class="fa fa-circle-o{{ (Request::is( 'mailbox/inbox') ? ' text-yellow' : '') }}"></i> Inbox</a></li>
                    <li {{ (Request::is( 'mailbox/message') ? 'class="active"' : '') }}><a href="{{route('mail-message')}}"><i class="fa fa-circle-o{{ (Request::is( 'mailbox/message') ? ' text-yellow' : '') }}"></i> Read Message</a></li>
                    <li {{ (Request::is( 'mailbox/contacts') ? 'class="active"' : '') }}><a href="{{route('contacts-list')}}"><i class="fa fa-circle-o{{ (Request::is( 'mailbox/contacts') ? ' text-yellow' : '') }}"></i> Contact List</a></li>
                </ul>
            </li>
            <li  class="treeview{{ (Request::is( 'plugins/*') ? ' active' : '') }}">
                <a href="#">
                    <i class="fa fa-gear"></i> <span>Plugins</span>
                    <i class="fa fa-angle-left pull-md-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ (Request::is( 'plugins/toastr') ? 'class="active"' : '') }}><a href="{{route('plu-toastr')}}"><i class="fa fa-circle-o{{ (Request::is( 'plugins/toastr') ? ' text-yellow' : '') }}"></i> Toastr</a></li>
                    <li {{ (Request::is( 'plugins/pace') ? 'class="active"' : '') }}><a href="{{route('plu-pace')}}"><i class="fa fa-circle-o{{ (Request::is( 'plugins/pace') ? ' text-yellow' : '') }}"></i> Pace</a></li>
                </ul>
            </li>
            <li class="treeview{{ (Request::is( 'pages/*') ? ' active' : '') }}">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Pages Examples</span>
                    <i class="fa fa-angle-left pull-md-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ (Request::is( 'pages/invoice') ? 'class="active"' : '') }}><a href="{{route('page-invoice')}}"><i class="fa fa-circle-o{{ (Request::is( 'pages/invoice') ? ' text-yellow' : '') }}"></i> Invoice</a></li>
                    <li {{ (Request::is( 'pages/profile') ? 'class="active"' : '') }}><a href="{{route('page-user-profile')}}"><i class="fa fa-circle-o{{ (Request::is( 'pages/profile') ? ' text-yellow' : '') }}"></i> Profile</a></li>
                    <li {{ (Request::is( 'pages/login') ? 'class="active"' : '') }}><a href="{{route('page-login')}}"><i class="fa fa-circle-o{{ (Request::is( 'pages/login') ? ' text-yellow' : '') }}"></i> Login</a></li>
                    <li {{ (Request::is( 'pages/login2') ? 'class="active"' : '') }}><a href="{{route('page-login2')}}"><i class="fa fa-circle-o{{ (Request::is( 'pages/login2') ? ' text-yellow' : '') }}"></i> Login 2</a></li>
                    <li {{ (Request::is( 'pages/lockscreen') ? 'class="active"' : '') }}><a href="{{route('page-lockscreen')}}"><i class="fa fa-circle-o{{ (Request::is( 'pages/lockscreen') ? ' text-yellow' : '') }}"></i> Lockscreen</a></li>
                    <li {{ (Request::is( 'pages/lockscreen2') ? 'class="active"' : '') }}><a href="{{route('page-lockscreen2')}}"><i class="fa fa-circle-o{{ (Request::is( 'pages/lockscreen2') ? ' text-yellow' : '') }}"></i> Lockscreen 2</a></li>
                    <li {{ (Request::is( 'pages/404') ? 'class="active"' : '') }}><a href="{{route('page-404')}}"><i class="fa fa-circle-o{{ (Request::is( 'pages/404') ? ' text-yellow' : '') }}"></i> 404 Error</a></li>
                    <li {{ (Request::is( 'pages/500') ? 'class="active"' : '') }}><a href="{{route('page-500')}}"><i class="fa fa-circle-o{{ (Request::is( 'pages/500') ? ' text-yellow' : '') }}"></i> 500 Error</a></li>
                    <li {{ (Request::is( 'pages/blank') ? 'class="active"' : '') }}><a href="{{route('page-blank')}}"><i class="fa fa-circle-o{{ (Request::is( 'pages/blank') ? ' text-yellow' : '') }}"></i> Blank Page</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-share"></i> <span>Multilevel</span>
                    <i class="fa fa-angle-left pull-md-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                    <li>
                        <a href="#"><i class="fa fa-circle-o"></i> Level One <i class="fa fa-angle-left pull-md-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                            <li>
                                <a href="#"><i class="fa fa-circle-o"></i> Level Two <i class="fa fa-angle-left pull-md-right"></i></a>
                                <ul class="treeview-menu">
                                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                </ul>
            </li>
            <!--<li><a href="../../documentation/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>-->
            <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>