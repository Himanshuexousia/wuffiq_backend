<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{route('home')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><span class="symbol">&infin;</span></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">Infinity <span class="symbol">&infin;</span></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <a href="#" class="fullscreen-toggle hidden-sm-down" data-toggle="fullscreen" role="button">
            <i class="ion-arrow-expand"></i>
        </a>

        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="nav-item dropdown tasks-menu">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-hourglass-o"></i>
                        <span class="label label-danger">9</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 9 tasks</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <li><!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            UI project design
                                            <small class="pull-right">25%</small>
                                        </h3>
                                        <progress class="progress progress-info xs" value="25" max="100">25%</progress>
                                    </a>
                                </li>
                                <!-- end task item -->
                                <li><!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            SEO testing
                                            <small class="pull-right">25%</small>
                                        </h3>
                                        <progress class="progress progress-success xs" value="25" max="100">25%</progress>
                                    </a>
                                </li>
                                <!-- end task item -->
                                <li><!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            Refactoring
                                            <small class="pull-right">75%</small>
                                        </h3>
                                        <progress class="progress progress-warning xs" value="75" max="100">75%</progress>
                                    </a>
                                </li>
                                <!-- end task item -->
                                <li><!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            Animate css
                                            <small class="pull-right">100%</small>
                                        </h3>
                                        <progress class="progress progress-danger xs" value="100" max="100">100%</progress>
                                    </a>
                                </li>
                                <!-- end task item -->
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="#">View all tasks</a>
                        </li>
                    </ul>
                </li>


                <li class="nav-item dropdown messages-menu">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success">5</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 5 messages</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <li><!-- start message -->
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="{{asset('img/avatars/user1.jpg')}}" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            Ruppert Jonhson
                                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                        </h4>
                                        <p>Infinity, awesome boostrap theme!</p>
                                    </a>
                                </li>
                                <!-- end message -->
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="{{asset('img/avatars/user2.jpg')}}" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            Mary Jane
                                            <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                        </h4>
                                        <p>Infinity, awesome boostrap theme!</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="{{asset('img/avatars/user3.jpg')}}" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            Bob Sinclair
                                            <small><i class="fa fa-clock-o"></i> Today</small>
                                        </h4>
                                        <p>Infinity, awesome boostrap theme!</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="{{asset('img/avatars/user4.jpg')}}" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            Carl Sagan
                                            <small><i class="fa fa-clock-o"></i> Yesterday</small>
                                        </h4>
                                        <p>Infinity, awesome boostrap theme!</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="{{asset('img/avatars/user5.jpg')}}" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            May Lou
                                            <small><i class="fa fa-clock-o"></i> 2 days</small>
                                        </h4>
                                        <p>Infinity, awesome boostrap theme!</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">See All Messages</a></li>
                    </ul>
                </li>

                <li class="nav-item dropdown notifications-menu">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bullhorn"></i>
                        <span class="label label-warning">4</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 4 notifications</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-shopping-cart text-green"></i>New order placed
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-aqua"></i>New user registered
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-warning text-red"></i>Disk quota at 95%
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-aqua"></i>New user registered
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">View all</a></li>
                    </ul>
                </li>

                <li class="nav-item dropdown timetracker hidden-xs-down">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                        <span>UI Development: <span class="label-pill white" id="currentTimetrack"></span></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="timetracker-header">
                            <p>Current task: UI Development</p>
                            <p>Estimated time: 5:30</p>
                            <p>Task cost: $ 60.00</p>
                        </li>
                        <!-- Menu Body -->
                        <li class="timetracker-body">
                            <div class="row">
                                <div class="col-xs-9 text-md-center">
                                    <button class="btn btn-block btn-danger" type="button" id="timeTrackBtn" data-toggle="timetrack"><i class="fa fa-stop"></i></button>
                                </div>
                                <div class="col-xs-3 text-md-center">
                                    <a href="#" class="btn btn-secondary"><i class="fa fa-gear"></i></a>
                                </div>
                            </div>
                            <!-- /.row -->
                        </li>
                        <!-- Menu Footer-->
                        <li class="timetracker-footer">

                        </li>
                    </ul>
                </li>

                @if($user = Sentinel::check())
                <li class="nav-item dropdown user user-menu">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                        <img src="{{asset('img/avatars/'.$user['avatar'])}}" class="user-image" alt="User Image">
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{asset('img/avatars/'.$user['avatar'])}}" alt="User Image">
                            <p>{{$user['first_name']}} {{$user['last_name']}}</p>

                        </li>

                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="row">
                                <p class="text-xs-center"><i class="fa fa-circle text-success"></i> Online</p>
                            </div>
                            <!-- /.row -->
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{route('checkauth.profile.show')}}" class="btn btn-secondary btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{route('checkauth.logout')}}" class="btn btn-secondary btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                @endif
                <!-- Control Sidebar Toggle Button -->
                <li class="nav-item hidden-sm-down">
                    <a class="nav-link" href="#" data-toggle="control-sidebar"><i class="fa fa-gear"></i></a>
                </li>
            </ul>
        </div>

    </nav>
</header><!-- /header -->