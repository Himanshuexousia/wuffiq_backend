@extends('app')

@section('page-title')
    General Form
@endsection

@section('page-css')

@endsection

@section('content-header')
    <h1>
        General Form
    </h1>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Form Groups
                    <div class="pull-md-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-block">
                    <form>
                        <fieldset class="form-group">
                            <label for="formGroupExampleInput">Example label</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="formGroupExampleInput2">Another label</label>
                            <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Another input">
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    Inline Form
                    <div class="pull-md-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-block">
                    <form class="form-inline">
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputName2">Name</label>
                            <input type="text" class="form-control" id="exampleInputName2" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputEmail2">Email</label>
                            <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Email">
                        </div>
                        <button type="submit" class="btn btn-primary">Send</button>
                    </form>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    Grid Form
                    <div class="pull-md-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-block">
                    <form>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 form-control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-2 form-control-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2">Number</label>
                            <div class="col-sm-10">
                                <div class="number">
                                    <label>
                                        <input class="form-control" type="number" name="quantity" min="1" max="5">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2">Radios</label>
                            <div class="col-sm-10">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
                                        Option one is this and that&mdash;be sure to include why it's great
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="gridRadios" id="gridRadios2" value="option2">
                                        Option two can be something else and selecting it will deselect option one
                                    </label>
                                </div>
                                <div class="radio disabled">
                                    <label>
                                        <input type="radio" name="gridRadios" id="gridRadios3" value="option3" disabled>
                                        Option three is disabled
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2">Checkbox</label>
                            <div class="col-sm-10">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> Check me out
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">Sign in</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Basic Example
                    <div class="pull-md-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-block">
                    <form>
                        <fieldset class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                            <small class="text-muted">We'll never share your email with anyone else.</small>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleSelect1">Example select</label>
                            <select class="form-control" id="exampleSelect1">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleSelect2">Example multiple select</label>
                            <select multiple class="form-control" id="exampleSelect2">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleTextarea">Example textarea</label>
                            <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleInputFile">File input</label>
                            <input type="file" class="form-control-file" id="exampleInputFile">
                            <small class="text-muted">This is some placeholder block-level help text for the above input. It's a bit lighter and easily wraps to a new line.</small>
                        </fieldset>
                        <div class="radio">
                            <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                Option one is this and that&mdash;be sure to include why it's great
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                Option two can be something else and selecting it will deselect option one
                            </label>
                        </div>
                        <div class="radio disabled">
                            <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled>
                                Option three is disabled
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Check me out
                            </label>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Input Addon
                    <div class="pull-md-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-block">
                    <div class="input-group">
                        <span class="input-group-addon">@</span>
                        <input type="text" class="form-control" placeholder="Username">
                    </div>
                    <br>

                    <div class="input-group">
                        <input type="text" class="form-control" />
                        <span class="input-group-addon">.00</span>
                    </div>
                    <br>

                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text" class="form-control" />
                        <span class="input-group-addon">.00</span>
                    </div>
                    <hr />
                    <h4 class="card-title">With icons</h4>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="email" class="form-control" placeholder="Email" />
                    </div>
                    <br>

                    <div class="input-group">
                        <input type="text" class="form-control" />
                        <span class="input-group-addon"><i class="fa fa-check"></i></span>
                    </div>
                    <br>

                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                        <input type="text" class="form-control" />
                        <span class="input-group-addon"><i class="fa fa-ambulance"></i></span>
                    </div>
                    <hr />
                    <h4 class="card-title">With checkbox and radio inputs</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox" />
                        </span>
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                      <input type="radio" />
                                    </span>
                                    <input type="text" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <h4 class="card-title">With buttons</h4>
                    <br />
                    <h5 class="card-title">Large</h5>
                    <div class="input-group input-group-lg">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"><span class="fa fa-list"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <input type="text" class="form-control" />
                    </div>
                    <br />
                    <h5 class="card-title">Normal</h5>
                    <div class="input-group">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-secondary"><span class="fa fa-exclamation"></span></button>
                        </div>
                        <input type="text" class="form-control" />
                    </div>
                    <br />
                    <h5 class="card-title">Small</h5>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputAmount">Search Input</label>
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" />
                            <span class="input-group-btn">
                              <button type="button" class="btn btn-secondary btn-flat"><span class="fa fa-search"></span></button>
                            </span>
                        </div>
                    </div>
                    <br />
                    <h5 class="card-title">Circle Button</h5>
                    <div class="input-group">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-circle btn-secondary"><span class="fa fa-check"></span></button>
                        </div>
                        <input id="p12" type="text" class="form-control" />
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-circle btn-secondary"><span class="fa fa-remove"></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Control Layout
                    <div class="pull-md-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-block">
                    <h4 class="card-title">Input Sizing</h4>
                    <input class="form-control form-control-lg" type="text" placeholder=".form-control-lg">
                    <input class="form-control" type="text" placeholder="Default input">
                    <input class="form-control form-control-sm" type="text" placeholder=".form-control-sm">
                    <hr />
                    <h4 class="card-title">Column Sizing</h4>
                    <div class="row">
                        <div class="col-xs-2">
                            <input type="text" class="form-control" placeholder=".col-xs-2">
                        </div>
                        <div class="col-xs-3">
                            <input type="text" class="form-control" placeholder=".col-xs-3">
                        </div>
                        <div class="col-xs-4">
                            <input type="text" class="form-control" placeholder=".col-xs-4">
                        </div>
                    </div>
                    <hr />
                    <h4 class="card-title">Input Colored</h4>
                    <div class="form-group has-success">
                        <label class="form-control-label" for="inputSuccess1">Input with success</label>
                        <input type="text" class="form-control form-control-success" id="inputSuccess1">
                    </div>
                    <div class="form-group has-warning">
                        <label class="form-control-label" for="inputWarning1">Input with warning</label>
                        <input type="text" class="form-control form-control-warning" id="inputWarning1">
                    </div>
                    <div class="form-group has-danger">
                        <label class="form-control-label" for="inputDanger1">Input with danger</label>
                        <input type="text" class="form-control form-control-danger" id="inputDanger1">
                    </div>

                    <div class="checkbox has-success">
                        <label>
                            <input type="checkbox" id="checkboxSuccess" value="option1">
                            Checkbox with success
                        </label>
                    </div>
                    <div class="checkbox has-warning">
                        <label>
                            <input type="checkbox" id="checkboxWarning" value="option1">
                            Checkbox with warning
                        </label>
                    </div>
                    <div class="checkbox has-danger">
                        <label>
                            <input type="checkbox" id="checkboxDanger" value="option1">
                            Checkbox with danger
                        </label>
                    </div>
                    <hr />
                    <h4 class="card-title">Other Inputs</h4>
                    <h5 class="card-title">Select</h5>
                    <select class="c-select">
                        <option selected>Open this select menu</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                    <br />
                    <br />
                    <h5 class="card-title">File</h5>
                    <label class="file">
                        <input type="file" id="file">
                        <span class="file-custom"></span>
                    </label>
                    <br />
                    <br />
                    <h5 class="card-title">Checkboxes and Raios</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="c-input c-checkbox">
                                <input type="checkbox">
                                <span class="c-indicator"></span>
                                Check this custom checkbox
                            </label>
                        </div>
                        <div class="col-md-6">
                            <label class="c-input c-radio">
                                <input id="radio1" name="radio" type="radio">
                                <span class="c-indicator"></span>
                                Toggle this custom radio
                            </label>
                            <br />
                            <label class="c-input c-radio">
                                <input id="radio2" name="radio" type="radio">
                                <span class="c-indicator"></span>
                                Or toggle this other radio
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')

@endsection