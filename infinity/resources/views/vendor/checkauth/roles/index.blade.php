@extends('app')
{{-- Web site Title --}}
@section('page-title')
    {{trans_choice('checkauth::pages.roles', 2)}}
@endsection

{{-- Content --}}
@section('content-header')

    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header h70">
                    <h3 class="card-title pull-left">{{trans_choice('checkauth::pages.roles', 2)}}</h3>
                    <a class='btn btn-primary pull-right' href="{{ route('checkauth.roles.create') }}">{{trans('checkauth::pages.create')}} {{trans_choice('checkauth::pages.roles', 1)}}</a>
                </div>
                <div class="card-block">
                    <table class="table table-striped table-hover">
                        <thead>
                        <th>{{trans_choice('checkauth::pages.users', 2)}}</th>
                        <th>{{trans('checkauth::pages.permissions')}}</th>
                        <th>{{trans('checkauth::pages.options')}}</th>
                        </thead>
                        <tbody>
                        @foreach ($roles as $role)
                            <tr>
                                <td><a href="{{ route('checkauth.roles.show', $role->hash) }}">{{ $role->name }}</a></td>
                                <td>
                                    <?php
                                    $permissions = $role->getPermissions();
                                    $keys = array_keys($permissions);
                                    $last_key = end($keys);
                                    ?>
                                    @foreach ($permissions as $key => $value)
                                        {{ ucfirst($key) . ($key == $last_key ? '' : ', ') }}
                                    @endforeach
                                </td>
                                <td>
                                    <button class="btn btn-secondary" onClick="location.href='{{ route('checkauth.roles.edit', [$role->hash]) }}'"><i class="fa fa-edit fa" alt="Edit" title="Edit"></i></button>
                                    <button class="btn btn-danger action_confirm {{ ($role->name == 'Admins') ? 'disabled' : '' }}" type="button" data-token="{{ Session::getToken() }}" data-method="delete" href="{{ route('checkauth.roles.destroy', [$role->hash]) }}"><i class="fa fa-trash fa" alt="Delete" title="Delete"></i></button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                </div>
            </div>
        </div>
    </div>

<div class="row">
    <div class="table-responsive">

    </div>
</div>
<div class="row">
    {!! $roles->render() !!}
</div>
<!--
	The delete button uses Resftulizer.js to restfully submit with "Delete".  The "action_confirm" class triggers an optional confirm dialog.
	Also, I have hardcoded adding the "disabled" class to the Admin role - deleting your own admin access causes problems.
-->
@section('checkauth::page-scripts')
    <script src="{{ asset('vendor/checkauth/js/restfulizer.js') }}"></script>
@endsection
@stop

