@extends('app')

@section('page-title')
    Bootstrap Miscellaneous
@endsection

@section('page-css')

    <link rel="stylesheet" type="text/css" href="{{asset('vendor/jquery-star-rating/rating.css')}}">

@endsection
@section('content-header')
    <h1>
        Bootstrap Miscellaneous
    </h1>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Labels and Dropdowns
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-8 text-md-center">
                    <div class="row">
                        <div class="col-md-6 text-md-center">
                            <h4>Labels</h4>
                            <span class="label label-default">Default</span> <code>.label-default</code> <br />
                            <span class="label label-primary">Primary</span> <code>.label-primary</code> <br />
                            <span class="label label-success">Success</span> <code>.label-success</code> <br />
                            <span class="label label-info">Info</span> <code>.label-info</code> <br />
                            <span class="label label-warning">Warning</span> <code>.label-warning</code> <br />
                            <span class="label label-danger">Danger</span> <code>.label-danger</code>
                        </div>
                        <div class="col-md-6 text-md-center">
                            <h4>Pill labels</h4>
                            <span class="label label-pill label-default">Default</span>  <code>.label-pill .label-default</code> <br />
                            <span class="label label-pill label-primary">Primary</span> <code>.label-pill .label-primary</code> <br />
                            <span class="label label-pill label-success">Success</span> <code>.label-pill .label-success</code> <br />
                            <span class="label label-pill label-info">Info</span> <code>.label-pill .label-info</code> <br />
                            <span class="label label-pill label-warning">Warning</span> <code>.label-pill .label-warning</code> <br />
                            <span class="label label-pill label-danger">Danger</span> <code>.label-pill .label-danger</code>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 text-md-center">
                    <h4>Dropdonws</h4>
                    <div class="col-md-6">
                        <h6>To Down</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Click me
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6">
                        <h6>To Up</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="dropup">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Click me
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6">
                        <h6>Other Options</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Click me
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li class="dropdown-header">Primary</li>
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li class="disabled"><a href="#">Disabled link</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li class="dropdown-header">Secondary</li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                    </div>
                    <br />
                    <h4>Rating</h4>
                    <div class="col-sm-6">
                        <div id="rating">
                            <input type="radio" name="example" class="rating" value="1" />
                            <input type="radio" name="example" class="rating" value="2" />
                            <input type="radio" name="example" class="rating" value="3" />
                            <input type="radio" name="example" class="rating" value="4" />
                            <input type="radio" name="example" class="rating" value="5" />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        Rate this page!
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Progress Bars
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-6">
                    <h4>Bootstrap Style</h4>
                    <progress class="progress" value="80" max="100">80%</progress>
                    <progress class="progress progress-info" value="60" max="100">60%</progress>
                    <progress class="progress progress-warning" value="50" max="100">50%</progress>
                    <progress class="progress progress-danger" value="28" max="100">28%</progress>
                    <progress class="progress progress-success" value="100" max="100">100%</progress>
                    <progress class="progress progress-striped" value="10" max="100">10%</progress>
                    <progress class="progress progress-striped progress-info" value="50" max="100">50%</progress>
                </div>
                <div class="col-md-6">
                    <h4>Animated</h4>
                    <progress class="progress progress-striped progress-animated" value="100" max="100">100%</progress>
                    <progress class="progress progress-striped progress-danger progress-animated" value="60" max="100">60%</progress>
                    <div class="row">
                        <div class="col-md-2">
                            <button class="btn btn-sm btn-primary" onclick="loadProgressBar('#loading',0)">Animate</button>
                        </div>
                        <div class="col-md-10">
                            <progress id="loading" class="progress progress-striped progress-animated" value="0" max="100">0%</progress>
                        </div>
                    </div>
                    <br />
                    <progress id="loading2" class="progress progress-success" value="0" max="100">0%</progress>
                    <progress id="loading3" class="progress progress-striped progress-info" value="0" max="100">0%</progress>
                    <progress id="loading4" class="progress progress-animated progress-striped progress-warning" value="0" max="100">0%</progress>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script src="{{asset('vendor/jquery-star-rating/rating.js')}}" type="application/javascript"></script>
    <script>
        $(function() {
            $('#rating').rating();
            loadProgressBar2('#loading2', 0);
            loadProgressBar3('#loading3', 0);
            loadProgressBar4('#loading4', 0);
        });
        function loadProgressBar(element, contador, tipo)
        {
            var cont = contador;
            if(cont < 11) {
                if(!$(element).hasClass('progress-animated'))
                {
                    $(element).addClass('progress-animated');
                }
                var valor = cont*10;
                $(element).attr('value',valor);
                cont++;
                setTimeout(function(){
                    loadProgressBar(element, cont);
                }, 1500);
            } else {
                $(element).removeClass('progress-animated');
            }
        }
        function loadProgressBar2(element, contador, tipo)
        {
            var cont = contador;
            if(cont < 21) {
                if(!$(element).hasClass('progress-animated'))
                {
                    $(element).addClass('progress-animated');
                }
                var valor = cont*5;
                $(element).attr('value',valor);
                cont++;
                setTimeout(function(){
                    loadProgressBar2(element, cont);
                }, 500);
            } else {
                $(element).removeClass('progress-animated');
            }
        }
        function loadProgressBar3(element, contador, tipo)
        {
            var cont = contador;
            if(cont < 101) {
                if(!$(element).hasClass('progress-animated'))
                {
                    $(element).addClass('progress-animated');
                }
                var valor = cont*1;
                $(element).attr('value',valor);
                cont++;
                setTimeout(function(){
                    loadProgressBar3(element, cont);
                }, 300);
            } else {
                $(element).removeClass('progress-animated');
            }
        }
        function loadProgressBar4(element, contador, tipo)
        {
            var cont = contador;
            if(cont < 21) {
                if(!$(element).hasClass('progress-animated'))
                {
                    $(element).addClass('progress-animated');
                }
                var valor = cont*5;
                $(element).attr('value',valor);
                cont++;
                setTimeout(function(){
                    loadProgressBar4(element, cont);
                }, 800);
            } else {
                $(element).removeClass('progress-animated');
            }
        }

    </script>
@endsection