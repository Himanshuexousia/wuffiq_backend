<li class="treeview {{ (Request::is( 'admin/posts') || (Request::is( 'admin/posts/*'))  ? ' active' : '') }}">
    <a href="#">
        <i class="fa fa-table"></i> <span>Posts</span>
        <i class="fa fa-angle-left pull-md-right"></i>
    </a>
    <ul class="treeview-menu">
        <li {{ (Request::is( 'admin/posts') ? 'class="active"' : '') }}><a href="{{route('admin.posts.index')}}"><i class="fa fa-circle-o{{ (Request::is( 'admin/posts') ? ' text-yellow' : '') }}"></i>List posts</a></li>
        <li {{ (Request::is( 'admin/posts/create') ? 'class="active"' : '') }}><a href="{{route('admin.posts.create')}}"><i class="fa fa-circle-o{{ (Request::is( 'admin/posts/create') ? ' text-yellow' : '') }}"></i>Add New post</a></li>
    </ul>
</li>
<li class="treeview{{ (Request::is( 'admin/products/*') || (Request::is( 'admin/products'))   ? ' active' : '') }}">
    <a href="#">
        <i class="fa fa-table"></i> <span>Products</span>
        <i class="fa fa-angle-left pull-md-right"></i>
    </a>
    <ul class="treeview-menu">
        <li {{ (Request::is( 'admin/products') ? 'class="active"' : '') }}><a href="{{route('admin.products.index')}}"><i class="fa fa-circle-o{{ (Request::is( 'admin/products') ? ' text-yellow' : '') }}"></i>List Products</a></li>
        <li {{ (Request::is( 'admin/products/create') ? 'class="active"' : '') }}><a href="{{route('admin.products.create')}}"><i class="fa fa-circle-o{{ (Request::is( 'admin/products/create') ? ' text-yellow' : '') }}"></i>Add New Product</a></li>
    </ul>
</li>
