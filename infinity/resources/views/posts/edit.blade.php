@extends('app')

@section('content')
    <div class="card">
        <div class="card-header">
            Edit Post {{ $post->id }}
        </div>
        <div class="card-block">
            {!! Form::model($post, [
                    'method' => 'PATCH',
                    'url' => ['/admin/posts', $post->id],
                    'class' => 'form-horizontal'
                ]) !!}

            <div class="form-group row {{ $errors->has('title') ? 'has-error' : ''}}">
                {!! Form::label('title', 'Title', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group row {{ $errors->has('text') ? 'has-error' : ''}}">
                {!! Form::label('text', 'Text', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('text', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('text', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


            <div class="form-group row">
                <div class="col-sm-offset-3 col-sm-3">
                    {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
                </div>
            </div>
            {!! Form::close() !!}

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>

@endsection