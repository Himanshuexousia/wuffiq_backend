@extends('app')

@section('page-title')
    Toastr
@endsection

@section('page-css')
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/icheck/skins/flat/blue.css')}}" />
@endsection

@section('content-header')
    <h1>
        Toastr
    </h1>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Basic
            <div class="pull-md-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-6">
                    <div class="control-group">
                        <div class="controls">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title" placeholder="Enter a title ...">
                            <br />
                            <label for="message">Message</label>
                            <textarea class="form-control" id="message" rows="3" placeholder="Enter a message ..."></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <label class="checkbox" for="closeButton">
                                <input id="closeButton" type="checkbox" value="checked" class="input-mini">Close Button
                            </label>
                        </div>
                        <div class="controls">
                            <label class="checkbox" for="addBehaviorOnToastClick">
                                <input id="addBehaviorOnToastClick" type="checkbox" value="checked" class="input-mini">Add behavior on toast click
                            </label>
                        </div>
                        <div class="controls">
                            <label class="checkbox" for="debugInfo">
                                <input id="debugInfo" type="checkbox" value="checked" class="input-mini">Debug
                            </label>
                        </div>
                        <div class="controls">
                            <label class="checkbox" for="progressBar">
                                <input id="progressBar" type="checkbox" value="checked" class="input-mini">Progress Bar
                            </label>
                        </div>
                        <div class="controls">
                            <label class="checkbox" for="preventDuplicates">
                                <input id="preventDuplicates" type="checkbox" value="checked" class="input-mini">Prevent Duplicates
                            </label>
                        </div>
                        <div class="controls">
                            <label class="checkbox" for="addClear">
                                <input id="addClear" type="checkbox" value="checked" class="input-mini">Add button to force clearing a toast, ignoring focus
                            </label>
                        </div>
                        <div class="controls">
                            <label class="checkbox" for="newestOnTop">
                                <input id="newestOnTop" type="checkbox" value="checked" class="input-mini">Newest on top
                            </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="control-group" id="toastTypeGroup">
                        <div class="controls">
                            <label>Toast Type</label>
                            <label class="radio">
                                <input type="radio" name="toasts" value="success" checked="">Success
                            </label>
                            <label class="radio">
                                <input type="radio" name="toasts" value="info">Info
                            </label>
                            <label class="radio">
                                <input type="radio" name="toasts" value="warning">Warning
                            </label>
                            <label class="radio">
                                <input type="radio" name="toasts" value="error">Error
                            </label>
                        </div>
                    </div>
                    <div class="control-group" id="positionGroup">
                        <div class="controls">
                            <label>Position</label>
                            <label class="radio">
                                <input type="radio" name="positions" value="toast-top-right" checked="">Top Right
                            </label>
                            <label class="radio">
                                <input type="radio" name="positions" value="toast-bottom-right">Bottom Right
                            </label>
                            <label class="radio">
                                <input type="radio" name="positions" value="toast-bottom-left">Bottom Left
                            </label>
                            <label class="radio">
                                <input type="radio" name="positions" value="toast-top-left">Top Left
                            </label>
                            <label class="radio">
                                <input type="radio" name="positions" value="toast-top-full-width">Top Full Width
                            </label>
                            <label class="radio">
                                <input type="radio" name="positions" value="toast-bottom-full-width">Bottom Full Width
                            </label>
                            <label class="radio">
                                <input type="radio" name="positions" value="toast-top-center">Top Center
                            </label>
                            <label class="radio">
                                <input type="radio" name="positions" value="toast-bottom-center">Bottom Center
                            </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="control-group">
                        <div class="controls">
                            <label class="control-label" for="showEasing">Show Easing</label>
                            <input id="showEasing" type="text" placeholder="swing, linear" class="input-mini" value="swing">

                            <label class="control-label" for="hideEasing">Hide Easing</label>
                            <input id="hideEasing" type="text" placeholder="swing, linear" class="input-mini" value="linear">

                            <label class="control-label" for="showMethod">Show Method</label>
                            <input id="showMethod" type="text" placeholder="show, fadeIn, slideDown" class="input-mini" value="fadeIn">

                            <label class="control-label" for="hideMethod">Hide Method</label>
                            <input id="hideMethod" type="text" placeholder="hide, fadeOut, slideUp" class="input-mini" value="fadeOut">
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="control-group">
                        <div class="controls">
                            <label class="control-label" for="showDuration">Show Duration</label>
                            <input id="showDuration" type="text" placeholder="ms" class="input-mini" value="300">

                            <label class="control-label" for="hideDuration">Hide Duration</label>
                            <input id="hideDuration" type="text" placeholder="ms" class="input-mini" value="1000">

                            <label class="control-label" for="timeOut">Time out</label>
                            <input id="timeOut" type="text" placeholder="ms" class="input-mini" value="5000">

                            <label class="control-label" for="extendedTimeOut">Extended time out</label>
                            <input id="extendedTimeOut" type="text" placeholder="ms" class="input-mini" value="1000">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                <button type="button" class="btn btn-primary" id="showtoast">Show Toast</button>
                <button type="button" class="btn btn-danger" id="cleartoasts">Clear Toasts</button>
                <button type="button" class="btn btn-danger" id="clearlasttoast">Clear Last Toast</button>
            </div>
            </div>

            <div class="row m25">
                <div class="col-md-2">
                </div>
                <div class="col-md-8 bw1 br-dashed br-danger">
                    <code>
                        <strong class="text-danger">DEBUG: </strong>
                        <pre id="toastrOptions"></pre>
                    </code>
                </div>
                <div class="col-md-2">
                </div>
            </div>
        </div>
    </div>


@endsection

@section('page-scripts')
    <script src="{{asset('vendor/icheck/icheck.js')}}"></script>
    <script type="text/javascript">
        $(function () {

            "use strict";

            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

            var i = -1;
            var toastCount = 0;
            var $toastlast;

            var getMessage = function () {
                var msgs = ['Neo: Hello?',
                    'Morpheus: Hello, Neo. Do you know who this is?',
                    'Neo: Morpheus?',
                    'Morpheus: Yes. I\'ve been looking for you, Neo. I don\'t know if you\'re ready to see what I want to show you, but unfortunately, you and I have run out of time. They\'re coming for you, Neo, and I don\'t know what they\'re going to do.',
                    'Neo: Who\'s coming for me? ',
                    'Morpheus: Stand up and see for yourself. ',
                    'Neo: What, right now? ',
                    'Morpheus: Yes, now. ',
                ];
                i++;
                if (i === msgs.length) {
                    i = 0;
                }

                return msgs[i];
            };

            var getMessageWithClearButton = function (msg) {
                msg = msg ? msg : 'Clear itself?';
                msg += '<br /><br /><button type="button" class="btn clear">Yes</button>';
                return msg;
            };

            $('#showtoast').click(function () {
                var shortCutFunction = $("#toastTypeGroup input:radio:checked").val();
                var msg = $('#message').val();
                var title = $('#title').val() || '';
                var $showDuration = $('#showDuration');
                var $hideDuration = $('#hideDuration');
                var $timeOut = $('#timeOut');
                var $extendedTimeOut = $('#extendedTimeOut');
                var $showEasing = $('#showEasing');
                var $hideEasing = $('#hideEasing');
                var $showMethod = $('#showMethod');
                var $hideMethod = $('#hideMethod');
                var toastIndex = toastCount++;
                var addClear = $('#addClear').prop('checked');

                toastr.options = {
                    closeButton: $('#closeButton').prop('checked'),
                    debug: $('#debugInfo').prop('checked'),
                    newestOnTop: $('#newestOnTop').prop('checked'),
                    progressBar: $('#progressBar').prop('checked'),
                    positionClass: $('#positionGroup input:radio:checked').val() || 'toast-top-right',
                    preventDuplicates: $('#preventDuplicates').prop('checked'),
                    onclick: null
                };

                if ($('#addBehaviorOnToastClick').prop('checked')) {
                    toastr.options.onclick = function () {
                        alert('You can perform some custom action after a toast goes away');
                    };
                }

                if ($showDuration.val().length) {
                    toastr.options.showDuration = $showDuration.val();
                }

                if ($hideDuration.val().length) {
                    toastr.options.hideDuration = $hideDuration.val();
                }

                if ($timeOut.val().length) {
                    toastr.options.timeOut = addClear ? 0 : $timeOut.val();
                }

                if ($extendedTimeOut.val().length) {
                    toastr.options.extendedTimeOut = addClear ? 0 : $extendedTimeOut.val();
                }

                if ($showEasing.val().length) {
                    toastr.options.showEasing = $showEasing.val();
                }

                if ($hideEasing.val().length) {
                    toastr.options.hideEasing = $hideEasing.val();
                }

                if ($showMethod.val().length) {
                    toastr.options.showMethod = $showMethod.val();
                }

                if ($hideMethod.val().length) {
                    toastr.options.hideMethod = $hideMethod.val();
                }

                if (addClear) {
                    msg = getMessageWithClearButton(msg);
                    toastr.options.tapToDismiss = false;
                }
                if (!msg) {
                    msg = getMessage();
                }

                $('#toastrOptions').text('Command: toastr["'
                        + shortCutFunction
                        + '"]("'
                        + msg
                        + (title ? '", "' + title : '')
                        + '")\n\ntoastr.options = '
                        + JSON.stringify(toastr.options, null, 2)
                );

                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                $toastlast = $toast;

                if(typeof $toast === 'undefined'){
                    return;
                }

                if ($toast.find('#okBtn').length) {
                    $toast.delegate('#okBtn', 'click', function () {
                        alert('you clicked me. i was toast #' + toastIndex + '. goodbye!');
                        $toast.remove();
                    });
                }
                if ($toast.find('#surpriseBtn').length) {
                    $toast.delegate('#surpriseBtn', 'click', function () {
                        alert('Morpheus: This is your last chance. After this, there is no turning back. You take the blue pill - the story ends, you wake up in your bed and believe whatever you want to believe. You take the red pill - you stay in Wonderland and I show you how deep the rabbit-hole goes. #' + toastIndex + '.');
                    });
                }
                if ($toast.find('.clear').length) {
                    $toast.delegate('.clear', 'click', function () {
                        toastr.clear($toast, { force: true });
                    });
                }
            });

            function getLastToast(){
                return $toastlast;
            }
            $('#clearlasttoast').click(function () {
                toastr.clear(getLastToast());
            });
            $('#cleartoasts').click(function () {
                toastr.clear();
            });
        })
    </script>
@endsection