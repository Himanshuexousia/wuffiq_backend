@extends('app')

@section('page-title')
    Social Media Dashboard
@endsection

@section('page-css')

    <!-- jvectormap -->
    <link rel="stylesheet" href="{{asset('vendor/jvectormap/jquery-jvectormap-1.2.2.css')}}">

    <style>
        .jvectormap-zoomin, .jvectormap-zoomout {
            background: rgba(0,0,0, 0.2);
            padding: 5px;
            width: 20px;
            height: 20px;
        }
    </style>
@endsection

@section('content-header')
    <h1>
        Social Media Dashboard
    </h1>
@endsection

@section('content')

<!--social cards row-->
<div class="row">
    <div class="col-lg-2 col-md-4">
        <div class="card  facebook small-card">
            <div class="inner">
                <h3>
                    <span class="timer" data-from="0" data-to="1450" data-speed="3000" data-refresh-interval="100"></span>
                    <i class="fa fa-arrow-up" style="color: palegreen;"></i>
                </h3>
                <p>1200 Last week</p>
            </div>
            <div class="icon">
                <i class="fa fa-facebook"></i>
            </div>
            <a href="#" class="small-card-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-2 col-md-4">
        <div class="card  google small-card">
            <div class="inner">
                <h3>
                    <span class="timer" data-from="0" data-to="180" data-speed="3000" data-refresh-interval="100"></span>
                    <i class="fa fa-arrow-up" style="color: palegreen;"></i>
                </h3>
                <p>20 Last week</p>
            </div>
            <div class="icon">
                <i class="fa fa-google-plus"></i>
            </div>
            <a href="#" class="small-card-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-2 col-md-4">
        <div class="card  twitter small-card">
            <div class="inner">
                <h3>
                    <span class="timer" data-from="0" data-to="1020" data-speed="3000" data-refresh-interval="100"></span>
                    <i class="fa fa-arrow-down" style="color: palevioletred;"></i>
                </h3>
                <p>1900 Last week</p>
            </div>
            <div class="icon">
                <i class="fa fa-twitter"></i>
            </div>
            <a href="#" class="small-card-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-2 col-md-4">
        <div class="card  instagram small-card">
            <div class="inner">
                <h3>
                    <span class="timer" data-from="0" data-to="800" data-speed="3000" data-refresh-interval="100"></span>
                    <i class="fa fa-arrow-up" style="color: palegreen;"></i>
                </h3>
                <p>420 Last week</p>
            </div>
            <div class="icon">
                <i class="fa fa-instagram"></i>
            </div>
            <a href="#" class="small-card-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-2 col-md-4">
        <div class="card  linkedin small-card">
            <div class="inner">
                <h3>
                    <span class="timer" data-from="0" data-to="302" data-speed="3000" data-refresh-interval="100"></span>
                    <i class="fa fa-arrow-up" style="color: palegreen;"></i>
                </h3>
                <p>110 Last week</p>
            </div>
            <div class="icon">
                <i class="fa fa-linkedin"></i>
            </div>
            <a href="#" class="small-card-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-2 col-md-4">
        <div class="card  foursquare small-card">
            <div class="inner">
                <h3>
                    <span class="timer" data-from="0" data-to="211" data-speed="3000" data-refresh-interval="100"></span>
                    <i class="fa fa-arrow-up" style="color: palegreen;"></i></h3>
                <p>152 Last week</p>
            </div>
            <div class="icon">
                <i class="fa fa-foursquare"></i>
            </div>
            <a href="#" class="small-card-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>
<!-- /.social cards row -->
<div class="row">

    <section class="col-lg-6 connectedSortable">
        <div class="card card-primary">
            <div class="card-header">
                <i class="fa fa-line-chart"></i>

                <h3 class="card-title">Social Media Metrics</h3>

                <div class="card-tools pull-right">
                    <div class="btn-group" data-toggle="btn-toggle">
                        <button type="button" class="btn btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
            </div>
            <div class="card-block nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="nav-item pull-right"><a href="#tabFollowers" class="nav-link active" data-toggle="tab" id="followers">Followers</a></li>
                    <li class="nav-item pull-right"><a href="#tabShare" class="nav-link" data-toggle="tab" id="share">Social Media Share</a></li>
                </ul>
                <div class="tab-content no-padding">
                    <div class="chart tab-pane active" id="tabFollowers">
                            <canvas id="barChart"></canvas>
                    </div>
                    <div class="chart tab-pane" id="tabShare">
                            <canvas id="doughnutChart" height="100"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <!-- Chat box -->
        <div class="card card-primary">
            <div class="card-header">
                <i class="fa fa-comments-o"></i>

                <h3 class="card-title">Facebook page comments</h3>

                <div class="card-tools pull-right">
                    <div class="btn-group" data-toggle="btn-toggle">
                        <button type="button" class="btn btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
            </div>
            <div class="card-block chat" id="chat-box">
                <!-- comment -->
                <div class="item">
                    <img src="{{asset('img/avatars/user4.jpg')}}" alt="user image" class="online">

                    <p class="message">
                        <a href="#" class="name">
                            <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2:15</small>
                            Jeremy Stuart
                        </a>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                    </p>
                </div>
                <!-- /.comment -->
                <!-- comment -->
                <div class="item">
                    <img src="{{asset('img/avatars/user3.jpg')}}" alt="user image" class="offline">

                    <p class="message">
                        <a href="#" class="name">
                            <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:15</small>
                            Bob Sinclair
                        </a>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                    </p>
                </div>
                <!-- /.comment -->
                <!-- comment -->
                <div class="item">
                    <img src="{{asset('img/avatars/user2.jpg')}}" alt="user image" class="offline">

                    <p class="message">
                        <a href="#" class="name">
                            <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:30</small>
                            Suki nakamoto
                        </a>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                    </p>
                </div>
                <!-- /.comment -->
            </div>
            <!-- /.facebook page comments -->
            <div class="card-footer">
            </div>
        </div>
        <!-- /.card (chat box) -->
    </section>

    <!-- /.Left col -->
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <section class="col-lg-6 connectedSortable">
        <!-- Map card -->
        <div class="card bg-blue-gradient">
            <div class="card-header">
                <!-- tools card -->
                <div class="pull-right card-tools">
                    <button type="button" class="btn btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                </div>
                <!-- /. tools -->

                <i class="fa fa-map-marker"></i>

                <h3 class="card-title">
                    Social media Demographics
                </h3>
            </div>
            <div class="card-block">
                <div id="world-map" style="height: 250px; width: 100%;"></div>
            </div>
        </div>
        <!-- /.card -->
        <div class="card" id="divGoals">
            <div class="card-header">
                <!-- tools card -->
                <div class="pull-right card-tools">
                    <button type="button" class="btn btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                </div>
                <!-- /. tools -->

                <i class="fa fa-dot-circle-o"></i>

                <h3 class="card-title">
                    This week goals
                </h3>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-xl-3 col-xs-6 text-xs-center">
                        <div>
                            <canvas id="facebookDs"></canvas>
                        </div>
                        <div class="knob-label">Facebook</div>
                    </div>
                    <div class="col-xl-3 col-xs-6 text-xs-center">
                        <div>
                            <canvas id="googleDs"></canvas>
                        </div>
                        <div class="knob-label">Google+</div>
                    </div>
                    <div class="col-xl-3 col-xs-6 text-xs-center">
                        <div>
                            <canvas id="twitterDs"></canvas>
                        </div>
                        <div class="knob-label">Twitter</div>
                    </div>
                    <div class="col-xl-3 col-xs-6 text-xs-center">
                        <div>
                            <canvas id="linkedinDs"></canvas>
                        </div>
                        <div class="knob-label">Linkedin</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card bg-red-gradient">
            <div class="card-header">
                <!-- tools card -->
                <div class="pull-right card-tools">
                    <button type="button" class="btn btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                </div>
                <!-- /. tools -->

                <i class="fa fa-line-chart"></i>

                <h3 class="card-title">
                    Google+ Statistics
                </h3>
            </div>
            <div class="card-block">
                <div class="col-md-6">
                    <div class="card card-graphic">
                        <div class="card-block">
                            <div class="inner">
                                <h1 class="fs30 mt5 mbn">400</h1>
                                <h6 class="text-system">Calls this week</h6>
                            </div>
                            <div class="text-xs-center graphic">
                                <canvas id="lineChart1" height="150"></canvas>
                            </div>

                        </div>
                        <div class="card-footer text-xs-center br-t">
                    <span>
                        <i class="fa fa-arrow-up"></i>
                        5% INCREASE
                        <b>last week</b>
                    </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-graphic">
                        <div class="card-block">
                            <div class="inner">
                                <h1 class="fs30 mt5 mbn">512</h1>
                                <h6 class="text-system">Badges this week</h6>
                            </div>
                            <div class="text-xs-center graphic">
                                <canvas id="lineChart2" height="150"></canvas>
                            </div>

                        </div>
                        <div class="card-footer text-xs-center br-t">
                    <span>
                    <i class="fa fa-arrow-up"></i>
                    4% INCREASE
                    <b>last week</b>
                    </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-graphic">
                        <div class="card-block">
                            <div class="inner">
                                <h1 class="fs30 mt5 mbn">610</h1>
                                <h6 class="text-system">Shares</h6>
                            </div>
                            <div class="text-xs-center graphic">
                                <canvas id="lineChart3" height="150"></canvas>
                            </div>
                        </div>
                        <div class="card-footer text-xs-center br-t">
                    <span>
                        <i class="fa fa-arrow-up"></i>
                        30% INCREASE
                        <b>last week</b>
                    </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-graphic">
                        <div class="card-block">
                            <div class="inner">
                                <h1 class="fs30 mt5 mbn">1820</h1>
                                <h6 class="text-system">Page views</h6>
                            </div>
                            <div class="text-xs-center graphic">
                                <canvas id="lineChart4" height="150"></canvas>
                            </div>

                        </div>
                        <div class="card-footer text-xs-center br-t">
                            <span>
                            <i class="fa fa-arrow-up"></i>
                            3% INCREASE
                            <b>last week</b>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
    <!-- right col -->
</div>
<!-- /.row (main row) -->

@endsection

@section('page-scripts')
    <script src="{{asset('vendor/raphael/raphael-min.js')}}"></script>
    <script src="{{asset('js/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('vendor/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{asset('vendor/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{asset('vendor/chart-js/Chart.min.js')}}"></script>
    <script src="{{asset('vendor/slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('vendor/moment/js/moment.min.js')}}"></script>
    <script src="{{asset('vendor/jquery-countTo/jquery.countTo.js')}}"></script>

    <script>

        $('.timer').countTo();

        $(function () {

            "use strict";

            $(".connectedSortable .card-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");

            drawTabChart('followers');
            drawCharts();

            //event listener to div resizing
            var chartDiv = document.getElementById("divGoals");
            //css-element-queries library
            new ResizeSensor(chartDiv, function() {
                drawCharts();
                drawTabChart('followers');
            });

            function drawTabChart($id){
                if ($id == 'followers'){

                    var barChartData = {
                        labels : ["January","February","March","April"],
                        datasets : [
                            {
                                label: "Facebook",
                                fillColor: "rgba(59,86,152,0.5)",
                                strokeColor: "rgba(59,86,152,0.8)",
                                highlightFill: "rgba(59,86,152,0.75)",
                                highlightStroke: "rgba(59,86,152,1)",
                                data: [65, 59, 80, 81]
                            },
                            {
                                label: "Google+",
                                fillColor: "rgba(221,75,57,0.5)",
                                strokeColor: "rgba(221,75,57,0.8)",
                                highlightFill: "rgba(221,75,57,0.75)",
                                highlightStroke: "rgba(221,75,57,1)",
                                data: [28, 48, 40, 19]
                            },
                            {
                                label: "Twitter",
                                fillColor: "rgba(85,172,238,0.5)",
                                strokeColor: "rgba(85,172,238,0.8)",
                                highlightFill: "rgba(85,172,238,0.75)",
                                highlightStroke: "rgba(85,172,238,1)",
                                data: [52, 22, 67, 78]
                            },
                            {
                                label: "Linkedin",
                                fillColor: "rgba(0,123,182,0.5)",
                                strokeColor: "rgba(0,123,182,0.8)",
                                highlightFill: "rgba(0,123,182,0.75)",
                                highlightStroke: "rgba(0,123,182,1)",
                                data: [32, 43, 65, 12]
                            }
                        ]

                    };

                    var barChart = document.getElementById("barChart").getContext("2d");
                    window.myBar = new Chart(barChart).Bar(barChartData, {
                        responsive : true,
                        maintainAspectRatio: true,
                        //Number - Spacing between each of the X value sets
                        barValueSpacing : 2,
                        multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"

                    });

                }else{
                    var dougnutData = [
                        {
                            value: 60,
                            color:"#3B5998",
                            highlight: "#324C81",
                            label: "Facebook"
                        },
                        {
                            value: 22,
                            color: "#DD4B39",
                            highlight: "#BC4031",
                            label: "Google+"
                        },
                        {
                            value: 8,
                            color: "#55ACEE",
                            highlight: "#4892CB",
                            label: "Twitter"
                        },
                        {
                            value: 10,
                            color: "#007BB6",
                            highlight: "#00699B",
                            label: "Linkedin"
                        }

                    ];

                    var doughnutChart = document.getElementById("doughnutChart").getContext("2d");
                    window.myDoughnut = new Chart(doughnutChart).Pie(dougnutData, {responsive : true, maintainAspectRatio: true});
                }

            }

            //Fix for charts under tabs
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                console.log($(e.target).attr('id'));
                drawTabChart($(e.target).attr('id'));
            });

            function drawCharts() {
                //week goals
                var dougnutData = [
                    {
                        value: 60,
                        color:"#3B5998",
                        highlight: "#324C81",
                        label: ""
                    },
                    {
                        value: 40,
                        color:"#eee",
                        highlight: "#f5f5f5",
                        label: ""
                    },
                ];

                var doughnutChart = document.getElementById("facebookDs").getContext("2d");
                window.myDoughnut = new Chart(doughnutChart).Doughnut(dougnutData, {responsive : true});

                //week goals
                var dougnutData = [
                    {
                        value: 80,
                        color:"#DD4B39",
                        highlight: "#BC4031",
                        label: ""
                    },
                    {
                        value: 20,
                        color:"#eee",
                        highlight: "#f5f5f5",
                        label: ""
                    },
                ];

                var doughnutChart = document.getElementById("googleDs").getContext("2d");
                window.myDoughnut = new Chart(doughnutChart).Doughnut(dougnutData, {responsive : true});

                //week goals
                var dougnutData = [
                    {
                        value: 95,
                        color:"#55ACEE",
                        highlight: "#4892CB",
                        label: ""
                    },
                    {
                        value: 5,
                        color:"#eee",
                        highlight: "#f5f5f5",
                        label: ""
                    },
                ];

                var doughnutChart = document.getElementById("twitterDs").getContext("2d");
                window.myDoughnut = new Chart(doughnutChart).Doughnut(dougnutData, {responsive : true});

                //week goals
                var dougnutData = [
                    {
                        value: 20,
                        color:"#007BB6",
                        highlight: "#00699B",
                        label: ""
                    },
                    {
                        value: 80,
                        color:"#eee",
                        highlight: "#f5f5f5",
                        label: ""
                    },
                ];

                var doughnutChart = document.getElementById("linkedinDs").getContext("2d");
                window.myDoughnut = new Chart(doughnutChart).Doughnut(dougnutData, {responsive : true});

                //Make the dashboard widgets sortable Using jquery UI
                $(".connectedSortable").sortable({
                    placeholder: "sort-highlight",
                    connectWith: ".connectedSortable",
                    handle: ".card-header, .nav-tabs",
                    forcePlaceholderSize: true,
                    zIndex: 999999
                });



                var data = {
                    labels: ["01", "02", "03", "04", "05", "06", "07"],
                    datasets: [
                        {
                            fillColor: "rgba(220,220,220,0.2)",
                            strokeColor: "rgba(220,220,220,1)",
                            pointColor: "rgba(220,220,220,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: [65, 59, 120, 81, 15, 55, 40]
                        }
                    ]
                };

                var lineChart = document.getElementById("lineChart1").getContext("2d");
                new Chart(lineChart).Line(data, {
                    bezierCurve: false,
                    showScale: false,
                    showTooltips: false,
                    responsive: true
                });

                var data = {
                    labels: ["01", "02", "03", "04", "05", "06", "07"],
                    datasets: [
                        {
                            fillColor: "rgba(220,220,220,0.2)",
                            strokeColor: "rgba(220,220,220,1)",
                            pointColor: "rgba(220,220,220,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: [45, 90, 120, 15, 100, 55, 40]
                        }
                    ]
                };

                var lineChart = document.getElementById("lineChart2").getContext("2d");
                new Chart(lineChart).Line(data, {
                    bezierCurve: false,
                    showScale: false,
                    showTooltips: false,
                    responsive: true
                });

                var data = {
                    labels: ["01", "02", "03", "04", "05", "06", "07"],
                    datasets: [
                        {
                            fillColor: "rgba(220,220,220,0.2)",
                            strokeColor: "rgba(220,220,220,1)",
                            pointColor: "rgba(220,220,220,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: [32, 67, 10, 20, 90, 110, 20]
                        }
                    ]
                };

                var lineChart = document.getElementById("lineChart3").getContext("2d");
                new Chart(lineChart).Line(data, {
                    bezierCurve: false,
                    showScale: false,
                    showTooltips: false,
                    responsive: true
                });

                var data = {
                    labels: ["01", "02", "03", "04", "05", "06", "07"],
                    datasets: [
                        {
                            fillColor: "rgba(220,220,220,0.2)",
                            strokeColor: "rgba(220,220,220,1)",
                            pointColor: "rgba(220,220,220,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: [12, 32, 42, 124, 10, 65, 40]
                        }
                    ]
                };

                var lineChart = document.getElementById("lineChart4").getContext("2d");
                new Chart(lineChart).Line(data, {
                    bezierCurve: false,
                    showScale: false,
                    showTooltips: false,
                    responsive: true
                });
            }

            //jvectormap data
            var visitorsData = {
                "US": 398, //USA
                "SA": 400, //Saudi Arabia
                "CA": 1000, //Canada
                "DE": 500, //Germany
                "FR": 760, //France
                "CN": 300, //China
                "AU": 700, //Australia
                "BR": 600, //Brazil
                "IN": 800, //India
                "GB": 320, //Great Britain
                "RU": 3000 //Russia
            };
            //World map by jvectormap
            $('#world-map').vectorMap({
                map: 'world_mill_en',
                backgroundColor: "transparent",
                regionStyle: {
                    initial: {
                        fill: '#e4e4e4',
                        "fill-opacity": 1,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 1
                    }
                },
                series: {
                    regions: [{
                        values: visitorsData,
                        scale: ["#92c1dc", "#ebf4f9"],
                        normalizeFunction: 'polynomial'
                    }]
                },
                onRegionLabelShow: function (e, el, code) {
                    if (typeof visitorsData[code] != "undefined")
                        el.html(el.html() + ': ' + visitorsData[code] + ' new visitors');
                }
            });

        });
    </script>
@endsection