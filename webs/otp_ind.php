<?php
include('config.php');
include('sanitizeValid.php');
include('twilio/Services/Twilio.php');
$userPhone=validError(sanitizeInput($_POST['userPhone']),'userPhone');
$phoneCode=validError(sanitizeInput($_POST['phoneCode']),'phoneCode');
$token=validError(sanitizeInput($_POST['token']),'token');
$otp=validError(sanitizeInput($_POST['otp']),'otp');

function sendOtp($conn,$phoneCode,$userPhone,$otp,$token){
	

	header ('Content-Type: text/json; charset=UTF-8'); 

  // Declare encoding META tag, it causes browser to load the UTF-8 charset 
  // before displaying the page. 
'<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />'; 	
$account_sid = "AC2d33b9dfdf71f2987d4541a0dbb4a2c0"; // Your Twilio account sid
$auth_token = "9812ac0f92962b4665adf79d2477e3f4"; // Your Twilio auth token
$client = new Services_Twilio($account_sid, $auth_token);
$select=$conn->prepare("SELECT * from users where phone=?");
$select->execute(array($userPhone));
if($select->rowCount()>0){
$message = $client->account->messages->sendMessage(
  '+1 765-626-3110', // From a Twilio number in your account
  $phoneCode.$userPhone, // Text any number
 "الرمز السري".$otp."www.wuffiq.com"
);
$sel=$select->fetch(PDO::FETCH_ASSOC);

$updToken=$conn->prepare("update users set gcmToken=? where id=?");	
$updToken->execute(array($token,$sel['id']));
if($sel['preacher']==0){

$data=$conn->prepare("select * from users INNER JOIN userDetails on users.id=userDetails.user_id  where users.id=?");	
$data->execute(array($sel['id']));
$row=$data->fetch(PDO::FETCH_ASSOC);	
// print_r($row);
// dd($row);	
$array=array(
			'userId'=>$row['user_id'],
			'userName'=>$row['username'],
			'email'=>$row['email'],
			'userPhone'=>$row['phone'],
			'userGender'=>$row['gender'],
			'userImage'=>$row['image'],
			'marriedStatus'=>$row['married_status'],
			'marryReason'=>$row['marry_reason'],
			'qubelahStatus'=>$row['qubelah_status'],	
			'qubelahName'=>$row['qubelah_name'],
		
			'userAge'=>$row['user_age'],	
			'userEducation'=>$row['user_education'],	
			'userHeight'=>$row['user_height'],
			'userWeight'=>$row['user_weight'],
			'userNationality'=>$row['user_nationality'],
			'userCity'=>$row['user_city'],
			'userReligion'=>$row['user_religion'],
			'hairColor'=>$row['hair_color'],
			'hairLook'=>$row['hair_look'],
			'bodyColor'=>$row['body_color'],
			'userProfession'=>$row['user_profession'],
			'childrenStatus'=>$row['children_status'],
			);


if($sel['user_registration']==1){

$manRequest=$conn->prepare("select * from man_request where user_id=?");	
$manRequest->execute(array($row['user_id']));
$row1=$manRequest->fetch(PDO::FETCH_ASSOC);
if($manRequest->rowCount()>0){
if($row1['man_request_status']==1){
$manRequestStatus='0';	
$manRequestData=(object)array();		
}
else{
$manRequestStatus='1';	
$manRequestData=array(
'marriageType'=>$row1['marriage_type'],
'marriageTypePrefer'=>json_decode($row1['marriage_type_prefer']),
'qubelahStatus'=>$row1['qubelah_status'],
'minAge'=>$row1['user_age_min'],
'maxAge'=>$row1['user_age_max'],
'lookWomenEducationList'=>json_decode($row1['user_education']),
'heightMin'=>$row1['user_height_min'],
'heightMax'=>$row1['user_height_max'],
'weightMin'=>$row1['user_weight_min'],
'weightMax'=>$row1['user_weight_max'],
'nationalityLogin'=>json_decode($row1['user_nationality']),
'city'=>json_decode($row1['user_city']),
'religion'=>json_decode($row1['user_religion']),
'hairColor'=>json_decode($row1['user_hair_color']),
'hairLook'=>json_decode($row1['user_hair_look']),
'bodyColor'=>json_decode($row1['user_body_color']),
'job'=>json_decode($row1['user_profession'])
);
// if($row1['qubelah_status']=="GQ1" || $row1['qubelah_status']=="MQ1"){
$manRequestData['qubelahName']=json_decode($row1['qubelah_name']);	
// }
// else{	
// $manRequestData['qubelahName']=(object)array();	
// }
		
}
}
else{
$manRequestStatus='2';	
$manRequestData=(object)array();		
}
if($row['gender']=='Male')
{
	$requestData=$conn->prepare("select * from requests where user_id=? && type='marriageRequest' && status=1 order by id desc");
}	
else
{
	$requestData=$conn->prepare("select * from requests where request_id=? && type='marriageRequest' && status=1 order by id desc");	
}
$requestData->execute(array($row['user_id']));
if($requestData->rowCount()>0){

	$reqData=$requestData->fetch(PDO::FETCH_ASSOC);
	
	$array['marriageRequestTime']=intval($reqData['timestamp']);
	
	if($reqData['payment']==0)
	{
		$row['deactivate']=3;	// payment is still pending
	}
	elseif($reqData['payment']==1)
	{
		$row['deactivate']=4;	// payment done and approvedd by admin
	}
	elseif($reqData['payment']==2)
	{
		$row['deactivate']=5;	// payment done but pendng from admin
	}
	elseif($reqData['payment']==3)
	{
		$row['deactivate']=0;	// payment cancel by admin
	}
	$payStatus=$conn->prepare("select * from payments INNER JOIN bankDetails on bankDetails.id=payments.bank_id where payments.request_id=? order by payments.id desc");	
	$payStatus->execute(array($reqData['id']));
	if($payStatus->rowCount()>0)
	{
	$paymentStatus=$payStatus->fetch(PDO::FETCH_ASSOC);
	$array['paymentTime']=$paymentStatus['created_at'];
	$array['selectedBank']=$paymentStatus['bankName'];
	}
}

	
echo json_encode(array('status'=>'1','message'=>'fullData','banAdmin'=>$row['banAdmin'],'deactivate'=>$row['deactivate'],'preacher'=>$sel['preacher'],'manRequestStatus'=>$manRequestStatus,'preferenceResponse'=>$manRequestData,'responsePreacher'=>(object)array(),'response'=>$array),200);
}
else{
echo json_encode(array('status'=>'1','message'=>'halfData','banAdmin'=>$row['banAdmin'],'deactivate'=>$row['deactivate'],'preacher'=>$sel['preacher'],'manRequestStatus'=>'2','preferenceResponse'=>(object)array(),'response'=>$array,'responsePreacher'=>(object)array()),200);		
}
}
else{


$data=$conn->prepare("select * from users INNER JOIN preacherDetails on users.id=preacherDetails.preacher_id  where users.id=?");
$data->execute(array($sel['id']));
$row=$data->fetch(PDO::FETCH_ASSOC);	
$array=array(
			'preacherId'=>$row['preacher_id'],
			'preacherName'=>$row['username'],
			'preacherPhone'=>$row['phone'],
			'preacherEmail'=>$row['email'],
			'preacherCode'=>$row['loginPlatform'],
			'preacherReligion'=>$row['religion'],
			'preacherCity'=>$row['city'],
			'preacherService'=>json_decode($row['service']),
			'bankName'=>$row['bank_name'],
			'acHolderName'=>$row['ac_holder_name'],
			'accountNo'=>$row['account_no'],
			'marriageType'=>json_decode($row['marriage_type'])
);
			
echo json_encode(array('status'=>'1','message'=>'fullData','banAdmin'=>$row['banAdmin'],'deactivate'=>$row['deactivate'],'preacher'=>$sel['preacher'],'manRequestStatus'=>'2','preferenceResponse'=>(object)array(),'response'=>(object)array(),'responsePreacher'=>$array),200);	
	
}
}
else{
	
$message = $client->account->messages->sendMessage(
  '+1 765-626-3110 ', // From a Twilio number in your account
  $phoneCode.$userPhone, // Text any number
  "الرمز السري".$otp."www.wuffiq.com"
);
$sel=$select->fetch(PDO::FETCH_ASSOC);
echo json_encode(array('status'=>'0','message'=>'notExist','banAdmin'=>0,'deactivate'=>0,'response'=>(object)array()),200);
	
}	
}

sendOtp($conn,$phoneCode,$userPhone,$otp,$token);

?>