<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    //to test locally: set in google developers, facebook developers, etc the callback to http://localhost:8000/oauth/google
    'google' => [
        'client_id' => 'your-id',
        'client_secret' => 'your-secret',
        'redirect' => config('app.url').'/oauth/callback/google',
    ],
    'facebook' => [
        'client_id' => 'your-id',
        'client_secret' => 'your-secret',
        'redirect' => config('app.url').'/oauth/callback/facebook',
    ],
	'twilio' => [
		'sid' => 'AC2d33b9dfdf71f2987d4541a0dbb4a2c0',
		'token' => '9812ac0f92962b4665adf79d2477e3f4',
		'from' => '+1 765-626-3110',
		'ssl_verify' => true, // Development switch to bypass API SSL certificate verfication
	],

];
